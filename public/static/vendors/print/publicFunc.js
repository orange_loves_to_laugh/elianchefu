function getsNowDateTime() {
    var date = new Date();
    //年
    var year = date.getFullYear();
    //月
    var month = date.getMonth() + 1;
    //日
    var day = date.getDate();
    //时
    var hh = date.getHours();
    //分
    var mm = date.getMinutes();
    //秒
    var ss = date.getSeconds();
    var rq = year + "年" + month + "月" + day + "日" + hh + ":" + mm + ":" + ss;
    return rq;
}

/**
 * 清空时间控件的值
 * @param name
 */
function cleanTime(name){
    $("input[name="+name+"]").val("")

}