let obj=''
let min=0;
let sec=0;
let ms=0;
let time_show=null;
let time_calling=null;
let count=0;

/**
 * [showtime 显示时间]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 */
function showtime(){
    ms++;
    if(sec==60){
        min++;sec=0;
    }
    if(ms==100){
        sec++;ms=0;
    }
    var msStr=ms;
    if(ms<10){
        msStr="0"+ms;
    }
    var secStr=sec;
    if(sec<10){
        secStr="0"+sec;
    }
    var minStr=min;
    if(min<10){
        minStr="0"+min;
    }
    $('#showtime span:eq(0)').html(minStr);
    $('#showtime span:eq(2)').html(secStr);
    //$('#showtime span:eq(4)').html(msStr);
}

function starttime(obj){
    if($(obj).html()=='启动'){
        $(obj).html('停止');
        //$('.bnt button:eq(0)').html('记次');
        //clearInterval(timer);
        timer=setInterval(showtime,10)
    }else{
        $(obj).html('启动');
       // $('.bnt button:eq(0)').html('复位');
        clearInterval(timer);
    }
}

$('.bnt button:eq(1)').click(function(){
    if($(this).html()=='启动'){
        $(this).html('停止');
        $('.bnt button:eq(0)').html('记次');
        clearInterval(timer);
        timer=setInterval(showtime,10)
    }else{
        $(this).html('启动');
        $('.bnt button:eq(0)').html('复位');
        clearInterval(timer);
    }
});

/**
 * [checkthumb 查看头像]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[object]}    obj [jquery对象]
 */
function checkavatar(obj) {

    let aa={
        "title": '用户头像', //相册标题
        "id": 1, //相册id
        "start": 0, //初始显示的图片序号，默认0
        "data": [   //相册包含的图片，数组格式
            {
                "alt": '用户头像',
                "pid": 2, //图片id
                "src": $(obj).attr('data-img'), //原图地址
                "thumb": $(obj).attr('data-img') //缩略图地址
            }
        ]
    }
    layer.photos({
        photos: aa
        ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
    });

}
/*车辆保养记录*/
function checkMaintainInfo(carLicences){
    layer.open({
        type: 2,
        title: '车辆保养记录',
        shadeClose: true,
        shade: false,
        area: ['100%', '100%'],
        content: '/admin/Member/maintainInfo?car='+carLicences
    });
}

/*用户各种记录*/
function checkMemberInfo(memberId,type,title){

    layer.open({
        type: 2,
        title: title,
        shadeClose: true,
        shade: false,
        area: ['100%', '100%'],
        content: '/calling/Index/'+type+'?memberId='+memberId
    });
}
/**
 * [showimg 获取图片]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[string]}    type [图片类型 按钮对象]
 * @param    {[int]}    member_id [用户id]
 */
function showimg(member_id,type) {
    let obj={
        handle_card:'办理会员',
        upgrade:'升级充值',
    }
    $.ajax({
        url: "/calling/Index/MemberQrcode",
        type: 'post',
        data:{
            member_id: member_id,
            img_tag:type
        },
        async: true,
        dataType: 'json',
        success: function (res) {
            layer.open({
                type: 1,
                title: obj[type],
                shadeClose: true,
                shade: false,
                area: ['30%', '100%'],
                content: '<img width="100%" height="100%" src="'+res.data.src+'">',
            });
        },
        error: function(re) {
            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    });
}
/**
 * [stopcall 挂断电话]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[obj]}    obj [jquery 按钮对象]
 */
function stopcall(obj){
    $.ajax({
        url: "/calling/Yuncall/Stopcall",
        type: 'post',
        async: true,
        dataType: 'json',
        success: function (res) {
            if (res.status)
            {
                layer.msg('<span style="color:#fff">挂断成功</span>')
                $('#showtime').siblings('span').html('')


                $(obj).attr('class','btn btn-info')
                $(obj).html('立即呼叫')
                $(obj).attr('data-action','docall')
                clearInterval(time_show);

            }

        },
        error: function(re) {
            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    });
}
/*
拨打电话
 */
$(".log .btn-info[data-action='docall']").on('click',function(){
    if ($(this).attr('data-action') == 'stopcall')
    {
        console.log(123);
        stopcall(this)
    }else{
        YunCall(this)
    }


})



/**
 * [YunCall 发起呼叫]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[obj]}    obj [jquery 按钮对象]
 */
function YunCall(obj) {


    let p_call =new Promise(function (resolve, reject) {
        $.ajax({
            url: "/calling/Yuncall/Docall?member_id="+$(obj).attr('data-id'),
            type: 'post',
            async: true,
            dataType: 'json',
            success: function (res) {
                resolve(res)
            },
            error: function(re) {
                reject(re)
            }
        });
    })
    p_call.then(
        function (res) {

            console.log(res);
            if (!res.status) {
               layer.msg(res.msg,{icon:2,time:2000})
                return
            }else{
                if (!res.data.status) {
                    layer.msg(res.data.msg,{icon:2,time:2000})
                    return
                }
              let uid=res.data.uniquid
                  min=0;
                  sec=0;
                $('#showtime span:eq(0)').html('00');
                $('#showtime span:eq(2)').html('00');
                $('#showtime').siblings('span').html('呼叫中……')
                $(obj).attr('class','btn btn-danger')
                $(obj).html('挂断')
                $(obj).attr('data-action','stopcall')

                getcallinfo(uid,obj);
            }

        },
        function (re) {
            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    )


}



/**
 * [showUnreadNews 根据uid获取呼叫信息]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[string]}    uid [通话记录中的 unqueid 或是弹屏中的 uid]
 * @param    {[obj]}    obj [jquery 按钮对象]
 */
function getcallinfo(uid,obj) {
    let billsec=$(obj).attr('data-billsec')
    let p_call =new Promise(function (resolve, reject) {
        $.ajax({
            url: "/calling/Yuncall/GetPhoneInfoByUid?uniqueid="+uid,
            type: 'post',
            async: true,
            dataType: 'json',
            success: function (res) {
                resolve(res)
            },
            error: function(re) {
                reject(re)
            }
        });
    })

    p_call.then(
        function (res) {

            if (res.status)
            {

                if (typeof res.data.disposition == 'string')//挂断
                {

                    if (res.data.disposition == 'ANSWERED') //已接听
                        {

                            if (res.data.billsec > billsec) {
                                $('#showtime').siblings('span').html('完成通话')
                                /*
                                $(obj).html('完成通话')
                                $(obj).attr('disabled',true)
                                $(obj).attr('class','btn btn-success')
                                */

                                //赠送积分
                                givingintegral(obj)

                            }else{
                                $('#showtime').siblings('span').html('未完成')
                                /*
                                $(obj).html('未完成')
                                $(obj).attr('disabled',true)
                                $(obj).attr('class','btn btn-danger')
                                */
                            }
                            $(obj).html('立即呼叫')
                            $(obj).attr('class','btn btn-info')
                            $(obj).attr('data-action','docall')
                            clearInterval(time_calling);
                            clearInterval(time_show);
                        }

                    if (res.data.disposition == 'NO ANSWER') //未接听
                        {
                            /*
                            $(obj).html('未接听')
                            $(obj).attr('disabled',true)
                            $(obj).attr('class','btn btn-danger')
                            */
                            $('#showtime').siblings('span').html('未接听')
                            $(obj).html('立即呼叫')
                            $(obj).attr('class','btn btn-info')
                            $(obj).attr('data-action','docall')
                            clearInterval(time_calling);

                        }
                        if (res.data.disposition == 'FAILED ') //呼叫失败
                        {
                            /*
                            $(obj).html('呼叫失败')
                            $(obj).attr('disabled',true)
                            $(obj).attr('class','btn btn-warning')
                            */
                            $('#showtime').siblings('span').html('呼叫失败')
                            $(obj).html('立即呼叫')
                            $(obj).attr('class','btn btn-info')
                            $(obj).attr('data-action','docall')
                            clearInterval(time_calling);

                        }

                }

                if (typeof res.data.activation == 'string')//接听
                {

                    if (res.data.activation.length>0)
                    {
                        if ($('#showtime').siblings('span').html() == '通话中……') {

                        }else{
                            $('#showtime').siblings('span').html('通话中……')
                            time_show=setInterval(showtime,10)
                        }
                        /*
                        $(obj).html('通话中……')
                        $(obj).attr('disabled',true)
                        $(obj).attr('class','btn btn-primary')
                        */
                        time_calling=setInterval(getcallinfo(uid,obj),5000);
                    }
                }




            }else{
                time_calling=setInterval(getcallinfo(uid,obj),50000);
            }

        },
        function (re) {
            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    )

}
/**
 * [givingintegral 赠送用户积分]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月12日18:58:45
 * @param    {[obj]}    obj [jquery 按钮对象]
 */
function givingintegral(obj)
{
    let p_call =new Promise(function (resolve, reject) {
        $.ajax({
            url: "/calling/Index/MemberGivingIntegral?member_id="+$(obj).attr('data-id'),
            type: 'post',
            async: true,
            dataType: 'json',
            success: function (res) {
                resolve(res)
            },
            error: function(re) {
                reject(re)
            }
        });
    })
    p_call.then(
        function (res) {

            if (!res.status)
            {
                //layer.msg(res.msg,{icon:2,time:2000})

            }else{

                layer.msg('<span style="color: #fff">赠送积分成功</span>')
            }

        },
        function (re) {
            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    )
}