layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    //初始化member_id
    let member_id=$.getQueryVariable('memberId')
    $('input[name="member_id"]').val(member_id)

    table.render({
        elem: '#myTable',
        url: '/calling/Index/MemberServiceLog?action=ajax&member_id='+member_id,
        method: 'post',

        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据

            //服务总数
            $('table tbody tr td').eq(0).html(res.count)
            //洗车美容
            $('table tbody tr td').eq(1).html(res.other_total.mr)
            //装饰
            $('table tbody tr td').eq(2).html(res.other_total.zs)
            //养护
            $('table tbody tr td').eq(3).html(res.other_total.yh)
            //喷漆
            $('table tbody tr td').eq(4).html(res.other_total.pq)

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            {field: 'pay_create', title: '日期时间'},
            {field: 'biz_title', title: '门店名称'},
            {field: 'car_liences', title: '服务车辆'},
            {field: 'service_title', title: '服务名称'},
            {field: 'order_price', title: '消费金额'},

            {field: 'voucher_title', title: '是否使用卡券'},
            {field: 'pay_price', title: '实际支付'},
            {field: 'pay_type_title', title: '支付方式'},
            {field: 'score_title', title: '用户评价'},
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    /*
    搜索事件
     */
    $("select[name='comment_level']").on('change',function (re) {

        console.log(123);
        $.submitInfo(table);
    })

    $("select[name='use_voucher']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='cash_type']").on('change',function (re) {
        $.submitInfo(table);
    })



});