layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    let member_id=$.getQueryVariable('memberId')
    $("input[name='memberId']").val(member_id)

    let flatpickr_register_time=$("#create_time").flatpickr({
        enableTime: false,
        mode:'range',

        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (re){
            if ($("#create_time").val().split(' to ').length > 1) {
                $.submitInfo(table);
                flatpickr_register_time.close()

            }
            //console.log($("#register_time").val().split(' to '));
            //flatpickr_register_time.close()
        }
    })
/*
时间 时长 分机号 状态（接通 未接弄 有效 无效）操作 是否有操作：办理会员（办理的什么会员） 充值（充了多少钱） 升级（生了多少级）
 */

    table.render({
        elem: '#myTable',
        url: '/calling/Index/MemberCallLog?action=ajax&member_id='+member_id,
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res);
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[

            {field: 'create_time', title: '时间'},
            {field: 'duration', title: '时长(秒)'},
            {field: 'phone_code', title: '分机号'},
            {field: 'is_connect_title', title: '接通状态'},

            {field: 'action', title: '相关操作'},
            {field: 'action_num', title: '操作数值'},


        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });



});