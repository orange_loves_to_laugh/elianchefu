layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    //初始化member_id
    let member_id=$.getQueryVariable('memberId')
    $('input[name="member_id"]').val(member_id)

    let flatpickr=$("#consump_time").flatpickr({
        enableTime: true,
        mode:"range",
        defaultDate: [$("#consump_time").attr('data-value')],
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (re) {
            console.log(re);
            if (re.length > 1) {
                $.submitInfo(table);
                flatpickr.close()
            }

        }
    })

    table.render({
        elem: '#myTable',
        url: '/calling/Index/MemberRechargeLog?action=ajax&member_id='+member_id,
        method: 'post',

        toolbar: '#toolbarDemo',
        defaultToolbar: [],

        cols: [[
            //日期时间 类型（办理会员等级 升级会员等级 会员充值等级）金额 支付方式
            {field: 'consump_time', title: '日期时间'},
            {field: 'income_type_title', title: '充值类型'},
            {field: 'consump_price', title: '金额'},
            {field: 'consump_pay_type_title', title: '支付方式'},
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    /*
    搜索事件
     */
    $("select[name='comment_level']").on('change',function (re) {

        $.submitInfo(table);
    })




});