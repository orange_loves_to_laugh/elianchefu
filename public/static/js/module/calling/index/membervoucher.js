layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    let member_id=$.getQueryVariable('memberId')
    $("input[name='memberId']").val(member_id)

    //卡券名称 卡券类型 卡券金额 消费金额 到期时间 领取方式

    table.render({
        elem: '#myTable',
        url: '/calling/Index/MemberVoucher?action=ajax&member_id='+member_id,
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[

            {field: 'card_title', title: '名称'},
            {field: 'card_type_title', title: '类型'},
            {field: 'voucher_cost', title: '金额'},
            {field: 'voucher_source_title', title: '来源'},
            {
                field: 'card_time', title: '有效期(天)', sort: true, align: 'center', event: 'cardTime', style: 'cursor: pointer;',
                templet: function (d) {
                    return '<a href="#" style="color: #1e9fff;text-decoration: underline;">' + d.card_time + '</a>';
                }
            },
            {field: 'create', title: '到期时间'},



        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });



    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        //修改升级价格
        if(obj.event === 'cardTime'){
            layer.prompt({
                formType: 0,
                value:obj.data.card_time,
                title: '修改有效期',
                area: ['800px', '350px'] //自定义文本域宽高
            }, function(value, index, elem){

                $.ajax({
                    url: '/admin/member/mVoucherWrite',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'card_time':value,
                        'id':obj.data.id,
                        'start_time':obj.data.start_time,
                    },
                    success: function (res) {
                        if (res.status == true) {
                            location.reload();


                        }else{
                            layer.msg(res.msg, {icon: 5, time: 1000} )
                        }
                    },
                    error: function (err) {
                        layer.msg(err);
                    }
                });

            });
        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/Member/DelMemberVoucher',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });
    var memberId = {$memberId}

    /**
     * toolbar监听事件
     */
    table.on('toolbar(currentTableFilter)', function (obj) {


        if (obj.event === 'add') {  // 监听添加操作
            $.ajax({
                url: '/admin/member/activeTypeService',
                type: 'post',
                dataType: 'json',
                data: {
                    'type':'service',
                    'mark':'member_voucher',
                    'mtype':'member_voucher',

                },
                success: function (res) {
                    console.log(111111111);
                    if (res.status == true) {
                        a= layer.open({
                            type: 1,
                            title:res.title,
                            skin: 'layui-layer-custom', //加上边框
                            area: ['1200px', '600px'], //宽高
                            content: res.data
                        });


                    }else{
                        layer.msg(res.msg, {icon: 5, time: 1000} )

                    }


                },
                error: function (err) {
                    layer.msg(err);
                }
            });


            /*let index = layer.open({
                title: '赠送卡卷',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['60%', '70%'],
                content: '/admin/Member/SaveMemberVoucher?member_id='+member_id
            });
            $(window).on("resize", function () {
                layer.full(index);
            });*/
        }

    });
    //积分 余额 卡券 保存
    form.on('submit(saveBtnPrize)', function (data) {
        var id =data.field.id
        var card_title =data.field.card_title
        var voucher_cost =data.field.voucher_cost
        var card_time =data.field.card_time
        var card_num =data.field.card_num


        if( card_title == ''){
            layer.msg('卡券名称为空', {icon: 5, time: 1000} )
            return
        }
        if( voucher_cost == ''){
            layer.msg('卡券价值为空', {icon: 5, time: 1000} )
            return
        }
        if( card_time == ''){
            layer.msg('有效期为空', {icon: 5, time: 1000} )
            return
        }
        if( card_num == ''){
            layer.msg('赠送数量为空', {icon: 5, time: 1000} )
            return
        }


        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/member/memberVoucherSession',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            location.reload();
                        })


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });





    });


});