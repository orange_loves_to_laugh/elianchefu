layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form=layui.form,
        table = layui.table;
    /*
    选择搜索条件
     */
    $("select[name='search_field']").on('change',function (re) {

        let title=$(this).find('option:checked').attr('data-title')

        $.each( $('select[name="search_field"] option'),function(index,val){


            if ($(val).attr('data-title') == title)
            {
                $('input[name="' + title + '"]').parents('.template').attr('class','form-group input-group-lg notemplate')
                $('select[name="' + title + '"]').parents('.template').attr('class','form-group input-group-lg notemplate')
                // if (input_len > 0) {
                //
                //     $('input[name="' + title + '"]').parents('.template').attr('class','form-group input-group-lg notemplate')
                //     $('select[name="' + title + '"]').parents('.template').attr('class','form-group input-group-lg notemplate')
                // }
                // if (select_len > 0) {
                //     $('select[name="' + title + '"]').parents('.template').attr('class','form-group input-group-lg notemplate')
                // }
            }else{
                $('input[name="' + $(val).attr('data-title') + '"]').parents('.notemplate').attr('class','form-group input-group-lg template')
                $('select[name="' + $(val).attr('data-title') + '"]').parents('.notemplate').attr('class','form-group input-group-lg template')
                // if (input_len > 0) {
                //     $('input[name="' + title + '"]').parents('.notemplate').attr('class','form-group input-group-lg template')
                // }
                // if (select_len > 0) {
                //     $('select[name="' + title + '"]').parents('.notemplate').attr('class','form-group input-group-lg template')
                // }
            }

        })



    })
    /*
    初始化 时间控件
     */
    let flatpickr_register_time=$("#register_time").flatpickr({
        enableTime: false,
        mode:'range',

        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (re){
            if ($("#register_time").val().split(' to ').length > 1)
            {
                let that="#register_time"
                searchdata(that)
                //$.submitInfo(table);
            }

            //flatpickr_register_time.close()
        }
    })
    let flatpickr_member_create=$("#member_create").flatpickr({
        enableTime: false,
        mode:'range',
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (){
            if ($("#member_create").val().split(' to ').length > 1)
            {
                let that="#member_create"
                searchdata(that)

            }
        }
    })

    //一键清空
    $('button[lay-event="clear"]').on('click',function(re){
        $('.notemplate').attr('class','form-group input-group-lg template')
        let t = $('#formInfo [name]');
        let html=$("#html").html()


        $("select[name='search_field']").html(html)

        $.each(t, function (index,val) {
            $(val).val('')
        });
    })
    table.render({
        elem: '#myTable',
        url: '/calling/Index/Index?action=ajax',
        method: 'post',
        defaultToolbar: ['filter', 'exports', 'print',],
        toolbar: '#toolbarDemo' ,
        parseData: function (res) {
            $.each(res.count,function(index,val)
            {
                $("#calc tr:eq(0)").find('th[data-title="'+index+'"] span').html(val)
            })
            console.log();
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            {field: 'headimage', title: '头像',templet:'#thumb'},
            {field: 'nickname', title: '昵称'},
            {field: 'total_address', title: '所在地区'},
            {field: 'member_sex_title', title: '性别'},
            {field: 'member_phone', title: '手机号'},
            {field: 'member_level_title', title: '会员等级'},
            {field: 'register_time', title: '注册时间',width:250},
            {field: 'arrive_times', title: '到店次数'},
            {field: 'experience_card_status_title', title: '是否激活体验卡'},
            {field: 'calling_time', title: '呼叫次数'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });

    /*
      公共搜索方法
     */
    function searchdata(obj){
        $.each($('#formInfo [name]'), function (index,val) {

            if ($(val).attr('name') != $(obj).attr('name'))
            {
                $(val).val('')
            }

        });
        $.submitInfo(table);
    }
    /*
    搜索事件
     */
    $("input[name='keywords']").on('keydown',function (re) {
        if (re.keyCode == 13) {
            let that=this
            searchdata(that)
        }

        // $.each($('#formInfo [name]'), function (index,val) {
        //
        //     if ($(val).attr('name') != $(that).attr('name'))
        //     {
        //         $(val).val('')
        //     }
        //
        // });
        // $.submitInfo(table);

    })
    // $("input[name='register_time']").on('blur',function (re) {
    //     $.submitInfo(table);
    // })
    // $("input[name='member_create']").on('blur',function (re) {
    //     $.submitInfo(table);
    // })


    $("select[name='member_level']").on('change',function (re) {
        let that=this
        searchdata(that)

    })
    $("select[name='member_sex']").on('change',function (re) {
        let that=this
        searchdata(that)
    })
    $("select[name='calling_time']").on('change',function (re) {
        let that=this
        searchdata(that)
    })

    $("select[name='experience_card_status']").on('change',function (re) {
        let that=this
        searchdata(that)

    })

    $("select[name='play_game_status']").on('change',function (re) {
        let that=this
        searchdata(that)

    })
    $("select[name='member_balance']").on('change',function (re) {
        let that=this
        searchdata(that)

    })
    $("select[name='position']").on('change',function (re) {
        let that=this
        searchdata(that)

    })

    $("select[name='cumulative_amount']").on('change',function (re) {
        let that=this
        searchdata(that)

    })
    $("select[name='insurance_status']").on('change',function (re) {
        let that=this
        searchdata(that)

    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {

            var index = layer.open({
                title: '查看信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/calling/Index/MemberDetail?id='+data.member_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }



        if (obj.event === 'headimage')
        {

            let json={
                "title": data.nickname+'头像', //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": data.nickname+'头像',
                        "pid": 666, //图片id
                        "src": data.headimage, //原图地址
                        "thumb": data.headimage //缩略图地址
                    }
                ]
            }

            layer.photos({
                photos: json
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
    });


});




