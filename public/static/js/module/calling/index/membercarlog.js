layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    //初始化member_id
    let member_id=$.getQueryVariable('memberId')

    table.render({
        elem: '#myTable',
        url: '/calling/Index/MemberCarLog?action=ajax&member_id='+member_id,
        method: 'post',

        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res);

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            //车牌号 品牌型号 车型 公里数 是否免排队 服务次数
            {field: 'car_licens', title: '车牌号'},
            {field: 'logoTitle', title: '车辆品牌'},
            {field: 'sortTitle', title: '车型'},
            {field: 'mileage', title: '公里数'},
            {field: 'queue_status_title', title: '是否免排队'},
            {field: 'order_num', title: '服务次数'},

        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    /*
    搜索事件
     */
    $("select[name='comment_level']").on('change',function (re) {

        console.log(123);
        $.submitInfo(table);
    })

    $("select[name='use_voucher']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='cash_type']").on('change',function (re) {
        $.submitInfo(table);
    })



});