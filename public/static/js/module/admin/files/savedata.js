layui.use(['layer', 'form', 'laydate', 'upload'], function () {
    var layer = layui.layer
        , form = layui.form
        , $ = layui.$;

    //初始化生效时间
    $("#work_time").flatpickr({
        enableTime: false,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',

        time_24hr:true,
        defaultHour:'00',
    })


    //监听提交
    form.on('submit(saveBtn)', function (data) {
        var obj = {
            id: data.field.id,
            base_salary: data.field.base_salary,
            fullattendance_salary: data.field.fullattendance_salary,
            station_salary: data.field.station_salary,
            employee_department_id: data.field.employee_department_id,
            employee_station_id: data.field.employee_station_id,
            work_time: data.field.work_time,
        };
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Files/SaveDispatch',
            type: 'post',
            dataType: 'json',
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function () {
                            parent.location.reload();
                        });
                } else {
                    layer.alert(res.msg);
                }
            },
            error: function (re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">' + re.responseJSON.errorCode + ':</span>' + re.responseJSON.msg,
                });
            }
        });
    });


    // 监听一级部门联动
    $("select[name='department_id_after']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // let option = '<option value="">请选择二级部门</option>'
                //
                // for(let i =0; i in res.data;i++){
                //     option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                // }
                //
                // $("select[name='department_secid_after']").html('')
                // $("select[name='department_secid_after']").html(option)


                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='department_secid_after']").html('')
                $("select[name='department_secid_after']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='station_id_after']").html('')
                $("select[name='station_id_after']").html(option_station)
                form.render();

                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })
    //监听二级部门联动
    $("select[name='department_secid_after']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                        department_secid:re.currentTarget.value,
                        department_id:$('select[name="department_id_after"]') .val()
                    }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }


                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='station_id_after']").html('')
                $("select[name='station_id_after']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

});




