layui.use(['layer', 'form', 'laydate', 'upload'], function () {
    var layer = layui.layer
        , form = layui.form
        , $ = layui.$,
        upload = layui.upload;

    //身份证
    $.singleupload(upload,'headImg_idcard','imgurlHidden_idcard','demoText_idcard','imgurl_idcard');

    //学历
    $.singleupload(upload,'headImg_education','imgurlHidden_education','demoText_education','imgurl_education');

    //劳动合同
    $.singleupload(upload,'headImg_contract','imgurlHidden_contract','demoText_contract','imgurl_contract');

    //社保
    $.singleupload(upload,'headImg_social_security','imgurlHidden_social_security','demoText_social_security','imgurl_social_security');

    //入职登记表
    $.singleupload(upload,'headImg_entry_table','imgurlHidden_entry_table','demoText_entry_table','imgurl_entry_table');

    //保密协议
    $.singleupload(upload,'headImg_nda','imgurlHidden_nda','demoText_nda','imgurl_nda');


    //保密协议多图片上传
    //$.multiupload(upload,'multiple_banner','div_slide_show','banner_images')

    let table_name=$('#div_slide_show').attr('data-table')
    let id=$('#div_slide_show').attr('data-id')

    upload.render({
        elem: '#multiple_banner',
        url: '/admin/Common/uploadfile'+'?table='+table_name+'&id='+id,
        data: {
            'mark': 'img',
            'field': 'thumb_img',
            'oldSrc': ''
        },
        field: 'thumb_img',
        accept: 'images',
        acceptMime: 'image/*',
        multiple: true,
        before: function(obj) {
            //预读本地文件示例，不支持ie8
            obj.preview(function(index, file, result) {
                // $('#div_slide_show').append( '<div class="div_img" >\n' +
                //     '                                            <img src="'+result+'" alt="'+result+'" title="点击查看大图" style="width: 100px;height: 100px" class="layui-upload-img" >\n' +
                //     '                                            <i class="zmdi zmdi-close-circle-o zmdi-hc-fw" ></i>\n' +
                //     '                                        </div>')

            });
        },
        done: function(res) {
            //如果上传成功
            if (res.status == true) {
                $('#div_slide_show').append( '<div class="div_img" >\n' +
                    '                                            <img src="'+res.src+'" alt="'+res.src+'" title="点击查看大图" style="width: 100px;height: 100px" class="layui-upload-img" >\n' +
                    '                                            <i class="zmdi zmdi-close-circle-o zmdi-hc-fw" ></i>\n' +
                    ' <div class="mask"><h6>点击查看大图</h6></div>'+
                    '                         </div>')

                //追加图片成功追加文件名至图片容器
                var banner_url= $('#banner_images').val()
                var banner_arr=[]
                if (banner_url.length < 1) {

                    banner_arr[0]=res.src;

                }else{
                    banner_arr=banner_url.split(',')
                    banner_arr.push(res.src);
                }
                console.log(banner_arr);

                $('#banner_images').val(banner_arr.join(','));
            }
        },
        allDone: function(obj) {
        }
    });

    //点击删除
    $("#div_slide_show").on('click','i',function (re) {

        let elem=$("#banenrinput").find('input#banner_images')
        // console.log($(this).parents('.div_img').prop("outerHTML",''));
        // return
        $.delMultipleImgs($(this).siblings('img'),'banner',elem)
        $(this).parents('.div_img').prop("outerHTML",'')
    })
    //点击查看大图
    $("#div_slide_show").on('click','img',function (re) {
        let src=$(this).attr('src')
        let json={
            "title": '保密协议', //相册标题
            "id": 123, //相册id
            "start": 0, //初始显示的图片序号，默认0
            "data": [   //相册包含的图片，数组格式
                {
                    "alt": '保密协议',
                    "pid": 666, //图片id
                    "src": src, //原图地址
                    "thumb": src //缩略图地址
                }
            ]
        }

        layer.photos({
            photos: json
            ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        });
    })

    form.on('submit(saveBtn)', function (data){
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Files/ArchivesSave',
            type: 'post',
            dataType: 'json',
            data: data.field,
            success: function (res) {
                if (res.code === 0) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                    function () {
                        parent.location.reload();
                    });
                } else {
                    layer.alert(res.msg);
                }
            },
            error: function (re) {
                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">' + re.responseJSON.errorCode + ':</span>' + re.responseJSON.msg,
                });
            }
        });
    });
});