layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form=layui.form,
        table = layui.table;

    let id=$.getQueryVariable('id')

    //https://gitee.com/lzb8249/layui-table-merge
    table.render({
        elem: '#myTable',
        url: '/admin/Files/ChangeNumList?action=ajax&id='+id,
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'employee_name_before', title: '异动前信息', templet: '#change_before',width:600},
            // {field: 'department_before_title', title: '异动前所属部门'},
            // {field: 'station_before_title', title: '异动前所属岗位',},
            // {field: 'base_salary_before', title: '异动前基本工资'},
            // {field: 'fullattendance_salary_before', title: '异动前满勤工资'},
            // {field: 'station_salary_before', title: '异动前岗位工资'},

            {field: 'work_time', title: '生效时间',width:200},

            {field: 'employee_name_after', title: '异动后信息', templet: '#change_after',width:600},
            // {field: 'department_after_title', title: '异动后所属部门'},
            // {field: 'station_after_title', title: '异动后所属岗位',},
            // {field: 'base_salary_after', title: '异动后基本工资'},
            // {field: 'fullattendance_salary_after', title: '异动后满勤工资'},
            // {field: 'station_salary_after', title: '异动后岗位工资'},
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });

    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加员工',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Employee/SaveData'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });

        }

    });

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑员工信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Employee/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }



        if (obj.event === 'employee_head')
        {
            let json={
                "title": data.employee_name+'头像', //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": data.employee_name+'头像',
                        "pid": 666, //图片id
                        "src": data.employee_head, //原图地址
                        "thumb": data.employee_head //缩略图地址
                    }
                ]
            }

            layer.photos({
                photos: json
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
    });


});

