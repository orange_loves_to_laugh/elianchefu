layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/Files/Index?action=ajax',
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'employee_name', title: '员工姓名'},
            {field: 'employee_create', title: '入职时间'},
            {field: 'regular_time', title: '转正时间'},
            {field: 'department_title', title: '所属部门'},
            {field: 'employee_station_title', title: '所属岗位'},
            {field: 'employee_status_title', title: '当前状态'},
            {field: 'change_num', title: '异动次数', templet: '#change_num'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        page: true,
        skin: 'line'
    });

    /*
   员工异动
    */
    $('.layui-btn-container').on('click', '[data-action="add"]', function (re) {
        let index = layer.prompt({
            formType: 0,
            value: '',
            maxlength: 140,
            title: '输入员工姓名、手机号搜索',
        }, (value) => {
            layer.close(index);
            $.ajax({
                url:'/admin/Employee/RelateEmployee',
                type: 'post',
                data:{param:{
                        keywords:value,
                        button:'执行调度'
                    }},
                async: true,
                dataType: 'json',
                success: function (res) {
                    var index = layer.open({
                        type: 1,
                        title:'请选择需要调度的员工',
                        area: ['800px', '600px'],
                        shade: 0.2,
                        skin: 'layui-layer-custom', //加上边框
                        maxmin: true,
                        shadeClose: true,
                        content: res.data
                    });



                    $(window).on("resize", function () {
                        layer.full(index);
                    });
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                        +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
            return
            // $.ajax({
            //     url: '/admin/Files/search?search=' + value,
            //     type: 'post',
            //     dataType: 'json',
            //     success: function (res) {
            //         if (res.code === 0) {
            //             layer.open({
            //                 title: '编辑员工信息',
            //                 type: 2,
            //                 shade: 0.2,
            //                 maxmin: true,
            //                 shadeClose: true,
            //                 area: ['100%', '100%'],
            //                 content: '/admin/Files/SaveData?id=' + res.data.id,
            //             });
            //         } else {
            //             layer.alert('没有数据');
            //         }
            //     },
            //     error: function (re) {
            //         layer.open({
            //             title: '错误信息',
            //             content: '<span style="color:red">' + re.responseJSON.errorCode + ':</span>' + re.responseJSON.msg
            //         })
            //     }
            // });

        });
        // $(window).on("resize", function () {
        //     layer.full(index);
        // });
    });


    $("select[name='employee_status']").on('change', function (re) {
        $.submitInfo(table);
    });
    $("select[name='employee_sex']").on('change', function (re) {
        $.submitInfo(table);
    });
    $("select[name='employee_department_id']").on('change', function (re) {
        $.submitInfo(table);
    });
    $("select[name='employee_station_id']").on('change', function (re) {
        $.submitInfo(table);
    });
    $("input[name='keywords']").on('blur', function (re) {
        $.submitInfo(table);
    });
    /*
    部门岗位联动
     */
    $('select[name="employee_department_id"]').on('change', function (re) {

        $.ajax({
            url: '/admin/Station/DataList?department_id=' + $(this).val(),
            type: 'post',
            dataType: 'json',

            success: function (res) {
                console.log(res);
                let option = '<option value="">请选择所属岗位</option>'

                for (let i = 0; i in res.data; i++) {

                    option += '<option value="' + res.data[i].id + '">' + res.data[i].title + '</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();
            },
            error: function (re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">' + re.responseJSON.errorCode + ':</span>' + re.responseJSON.msg
                })
            }
        });


    })


    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'checkemployee') {
            var index = layer.open({
                title: '查看员工',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Employee/SaveData?from=file&id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'checkfile') {
            var index12 = layer.open({
                title: '查看档案',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Files/ArchivesInfo?id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index12);
            });
            return false;

        }




        if (obj.event === 'checkchangenum') {

            var index = layer.open({
                title: '异动列表',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Files/ChangeNumList?id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
    });

    form.render();
});

/*
关联员工
 */
function connect_detail_callback(obj){

    layer.open({
            title: '员工异动信息',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Files/SaveData?id=' + $(obj).attr('data-id'),
        });


}
/**
 * @context 搜索关联员工
 */
function searchRelevanceStore()
{

    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        let data={param:{
                keywords:search_title,
                notin:'true',
            }}
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/Employee/RelateEmployee',
                type: 'post',
                data:data,
                dataType: 'json',
                success: function(res) {

                    if (res.status) {

                        res.datalist.forEach(function(e) {
                            //console.log($(".lump"));
                            var  oldelement = $(".lump" + e.id);


                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

