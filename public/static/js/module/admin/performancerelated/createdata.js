layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
        ,$ = layui.$;

    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/PerformanceRelated/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });

    });

    // 监听一级部门联动
    $("select[name='employee_department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='employee_department_secid']").html('')
                $("select[name='employee_department_secid']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option_station)

                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })
    //监听二级部门联动
    $("select[name='employee_department_secid']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                        department_secid:re.currentTarget.value,
                        department_id:$('select[name="employee_department_id"]') .val()
                    }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }

                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })


    //弹窗创建关联考等级
    let title={1:'销售额考核',2:'办卡额考核',3:'办卡数量考核',4:'到店率考核',5:'好评率考核',6:'平台抽成金额'};
    let type=''
    let index_check_relate_check=0;
    let index_relate_check=0;
    let yes_no={1:'是',2:'否'}
    let checkrelatecheck_str=$('input[name="checkrelatecheck_str"]').val()
    $('button[data-action="createcheck"]').on('click',function (re) {
        type=$(this).attr('data-type')

        //表单初始化
        initform()
        //弹窗
        index_relate_check=layer.open({
            type: 1,
            title:title[type],
            skin: 'layui-layer-custom', //加上边框
            area: ['1200px', '600px'], //宽高
            content: $( '#form2'),
            shade: 0
        });
        $('input[name="type"]').val(type)

        form.render()
    })

    //未完成是否处罚
   $("select[name='undo_punish']").on('change',function (re) {
       if ($(this).val() == 1) {
           $('input[name="undo_punish_price"]').parents('.card').css("display",'')
       }else{
           $('input[name="undo_punish_price"]').parents('.card').css("display",'none')
       }
   })

    //是否关联其他考核
    $("select[name='relate_check_ids']").on('change',function (re) {
        if ($(this).val() == 1) {
            $('button[data-action="checkrelatecheck"]').parents('.card').css("display",'')
        }else{
            $('button[data-action="checkrelatecheck"]').parents('.card').css("display",'none')
        }
    })

    //弹窗创建考核等级关联其他考核
    $('button[data-action="checkrelatecheck"]').on('click',function (re) {

        if ($(".checkrelatecheck table tr").length == 5)
        {
            layer.msg("关联其他考核已达上限！",{icon:2,time:2000});
            return;
        }
        let option = '<option value="">关联考核类型</option>'
        for(let i =1; i in title;i++)
        {
            if (i != type)
            {
                option += '<option value="'+i+'">'+title[i]+'</option>'
            }
        }
        $("select[name='related_type']").html('')
        $("select[name='related_type']").html(option)
         index_check_relate_check=layer.open({
            type: 1,
            title:'关联其他考核',
            skin: 'layui-layer-custom',
            area: ['1200px', '600px'],
            content: $( '#form3'),
            shade: 0
        });
    })

    //提交考核等级关联其他考核
    form.on('submit(saveBtnform3)', function (data) {

        if (data.field.related_type < 1)
        {
            layer.msg('请选择关联考核',{icon:2,time:2000})
            return
        }

        $(".checkrelatecheck").css('display','')
        let tr = $(".checkrelatecheck table  tr:last");
        let aa= '<tr>\n' +
        '                    <td scope="row">'+title[data.field.related_type]+'<input type="hidden" name="related_type[]" value="'+data.field.related_type+'"></td>\n' +
        '                    <td>'+yes_no[data.field.undo_reward]+'<input type="hidden" name="undo_reward[]" value="'+data.field.undo_reward+'"></td>\n' +
        '                    <td><a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete">删除</a></td>\n' +
        '                </tr>'
        if(tr.length==0){
            layer.alert("指定的table或对应的行数不存在！");
            return;
        }
        tr.after(aa);

        //删除考核关联其他考核
        $(".checkrelatecheck table tr td").on('click','.layui-btn-danger',function (re) {

            $(this).parent().parent().remove();
        })

        layer.close(index_check_relate_check)

    });

    //提交关联考核等级
    form.on('submit(saveBtnform2)', function (data) {

        let formdata=data.field
        console.log(formdata);

        let class_name=".relatecheck_"+type
        let checkrelatecheck_str_new=''
        //类型-等级-关联类型-未完成是否奖励
        for(let i=0;i<10;i++)
        {
            if (formdata['undo_reward[' + i + ']'] > 0) {
                checkrelatecheck_str_new+= formdata.type+'-'+
                                           formdata.level+'-'+
                                           formdata['related_type[' + i + ']']+'-1'+ ','

            }

        }

        checkrelatecheck_str=checkrelatecheck_str+','+checkrelatecheck_str_new

        $('input[name="checkrelatecheck_str"]').val(checkrelatecheck_str)
        $(class_name).css('display','')
        let tr = $(class_name+" table tr:last");

        //等级 完成额度 奖励金额  未完成处罚金额 删除
        // let aa= '<tr>\n' +
        //     '                    <td >'+formdata.level+'<input type="hidden" name="level'+type+'[]" value="'+formdata.level+'"></td>\n' +
        //     '                    <td class="dbclickedit">'+formdata.todo_num+'<input data-title="完成额度" type="hidden" name="todo_num'+type+'[]" value="'+formdata.todo_num+'"></td>\n' +
        //     '                    <td class="dbclickedit">'+formdata.reward_price+'<input data-title="奖励金额" type="hidden" name="reward_price'+type+'[]" value="'+formdata.reward_price+'"></td>\n' +
        //     '                    <td>'+formdata.undo_punish_price+'<input type="hidden" name="undo_punish'+type+'[]" value="'+formdata.undo_punish+'">'+'<input type="hidden" name="undo_punish_price'+type+'[]" value="'+formdata.undo_punish_price+'"></td>\n' +
        //     '                    <td><a class="layui-btn layui-btn-xs layui-btn-normal" onclick="checkrelateother('+type+','+'\''+formdata.level+'\')">点击查看</a></td>\n' +
        //     '                    <td><a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete">删除</a><input type="hidden" name="undo_percentage'+type+'[]" value="'+formdata.undo_percentage+'"></td>\n' +
        //     '                </tr>'
        let aa= '<tr>\n' +
            '                    <td >'+formdata.level+'<input type="hidden" name="level'+type+'[]" value="'+formdata.level+'"></td>\n' +
            '                    <td class="dbclickedit">'+formdata.todo_num+'<input data-title="完成额度" type="hidden" name="todo_num'+type+formdata.level+'" value="'+formdata.todo_num+'"></td>\n' +
            '                    <td class="dbclickedit">'+formdata.reward_price+'<input data-title="奖励金额" type="hidden" name="reward_price'+type+formdata.level+'" value="'+formdata.reward_price+'"></td>\n' +
            '                    <td>'+formdata.undo_punish_price+'<input type="hidden" name="undo_punish'+type+formdata.level+'" value="'+formdata.undo_punish+'">'+'<input type="hidden" name="undo_punish_price'+type+formdata.level+'" value="'+formdata.undo_punish_price+'"></td>\n' +
            '                    <td><a class="layui-btn layui-btn-xs layui-btn-normal" onclick="checkrelateother('+type+','+'\''+formdata.level+'\')">点击查看</a></td>\n' +
            '                    <td><a class="layui-btn layui-btn-xs layui-btn-danger data-count-delete">删除</a><input type="hidden" name="undo_percentage'+type+formdata.level+'" value="'+formdata.undo_percentage+'"></td>\n' +
            '                </tr>'
        //'+'+'
        if(tr.length==0){
            layer.alert("指定的table或对应的行数不存在！");
            return;
        }
        tr.after(aa);

        //删除考核关联其他考核
        $(class_name+" table tr td").on('click','.layui-btn-danger',function (re) {

            $(this).parent().parent().remove();
        })
        //双击修改完成额度+奖励金额
        $(class_name+" table tr").on('dblclick','.dbclickedit',function (re) {
            let title=$(this).find('input').attr("data-title")
            let value=$(this).find('input').val()
            let that=$(this)

            layer.prompt({
                formType: 0,
                value: value,
                title: '请输入'+title,

            }, function(value, index, elem){
                that.find('input').val(value)
                that.text(value)
                layer.close(index);
            });
        })
        layer.close(index_relate_check)
    });

    //表单初始化
    function initform(){
        //未完成是否处罚
        $("select[name='undo_punish']").html('')
        $("select[name='undo_punish']").html('<option  value="2"  >否</option><option  value="1"  >是</option>')
        //未完成是否给提成
        $("select[name='undo_percentage']").html('')
        $("select[name='undo_percentage']").html('<option  value="1"  >是</option><option  value="2"  >否</option>')
        //是否关联其他考核
        $("select[name='relate_check_ids']").html('')
        $("select[name='relate_check_ids']").html('<option  value="2"  >否</option><option  value="1"  >是</option>')
        //关联按钮
        $('button[data-action="checkrelatecheck"]').parents('.card').css('display','none')
        //关联其他考核表格
        $(".checkrelatecheck  table tr").eq(0).nextAll().remove()
        $(".checkrelatecheck").css('display','none')
        $('#form2').children().each(function (i,n) {
            if ($(n).find('input').length > 0)
            {
                $(n).find('input').val('')
            }
            //未完成处罚金额
            if ($(n).find('input').attr('name') == 'undo_punish_price') {
                $(n).find('input').val(0)
                $(n).css('display','none')
            }
        })
    }


});

/**
 * [relevanceStore 获取员工信息]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2016-12-10T14:31:19+0800
 * @param    {[int]}     type [考核类型]
 * @param    {[string]}    level [考核级别]
 */
function checkrelateother(type,level) {
    let title={1:'销售额考核',2:'办卡额考核',3:'办卡数量考核',4:'到店率考核',5:'好评率考核',6:'平台抽成金额'};
    let relatecheck_str=$('input[name="checkrelatecheck_str"]').val()
    let len=(type+'-'+level).length
    let relatecheck_arr=relatecheck_str.split(',')
    let relatecheck_arr_tmp=[]
    let html='<table class="table table-bordered mb-0" id="table1" >\n' +
        '    <tr>\n' +
        '        <th>考核名称</th>\n' +
        '    </tr>\n'

    $.each(relatecheck_arr,function(index,value)
    {
        if (value.substr(0, len) == type + '-' + level)
        {
            relatecheck_arr_tmp.push(value)

            html+=' <tr>\n' +
                '        <th>'+title[value.split('-')[2]]+'</th>\n' +
                '    </tr>'

        }

    });
    html+='</table>'


    layer.open({
        type: 1,
        title:'已关联考核',
        skin: 'layui-layer-custom', //加上边框
        area: ['300px', '300px'], //宽高
        content: html,
        shade: 0
    });

   // console.log(type, level);
}

/**
 * [relevanceStore 获取员工信息]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2016-12-10T14:31:19+0800
 * @param    {[string]}     $service [元素的class或id]
 * @param    {[boolean]}    $directly [是否返回json对象（默认否）]
 * @return   {[object]}        		[josn对象]
 */
function relevanceStore($service='',$directly=false)
{
    let checkStock=$("#relevance_store").val();
    let checkStockPrice=$("#relevance_store_price").val();
    let year=$("select[name='year']").val();
    let month=$("select[name='month']").val();

    let data={
        param:{
            notin_employee_check:true,
            check_time:{year:year,month:month},
            employee_department_id:$("select[name='employee_department_id']").val(),
            employee_department_secid:$("select[name='employee_department_secid']").val(),
            id_str:checkStock
        }
    }



    $.ajax({
        url:'/admin/PerformanceRelated/RelateEmployee',
        type:'post',
        dataType:'json',
        data:data,
        success:function(res)
        {
            if(res.status){
                a=layer.open({
                    type: 1,
                    title:'关联员工',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }
        },
        error: function(re)
        {

            layer.open({
                title: '提示信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
            })
        }
    })
}

/**
 * @context 单独关联获取id 字符串
 * @param id
 * @param pro  商品标识
 * @param search_title 搜索的名称
 * @param select_value 筛选分类id
 */
function aloneOption(id)
{

    $.ajax({
        url: '/admin/FileOperation/bizproSession',
        type: 'post',
        dataType: 'json',
        data: {
            'value':id,

        },
        success: function (res) {
            if (res.status == true) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        // layer.closeAll();
                        //打开已关联列表
                        // location.reload();
                    })

            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/**
 * @context 搜索关联员工
 */
function searchRelevanceStore()
{

    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        let checkStock=$("#relevance_store").val();
        let year=$("select[name='year']").val();
        let month=$("select[name='month']").val();

        let data={
            param:{
                keywords:search_title,
                notin_employee_check:true,
                check_time:{year:year,month:month},
                employee_department_id:$("select[name='employee_department_id']").val(),
                employee_department_secid:$("select[name='employee_department_secid']").val(),
                id_str:checkStock
            }
        }
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/PerformanceRelated/RelateEmployee',
                type: 'post',
                data:data,
                dataType: 'json',
                success: function(res) {

                    if (res.status) {

                        res.datalist.forEach(function(e) {
                            //console.log($(".lump"));
                            var  oldelement = $(".lump" + e.id);


                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

/**
 * @context 关联员工全选反选
 * @param obj
 * @param mark
 */
function checkAll(obj,mark)
{
    if($(obj)[0].tagName=="BUTTON"){
        if($(obj).hasClass("checkbutton")){
            // 取消选中
            $(obj).removeClass("checkbutton");
            $("."+$(obj).data("mark")).prop("checked",false);
            $("."+mark).prop("checked",false);
            $(obj).text("全选");
        }else{
            $(obj).addClass("checkbutton");
            // 全选框选中
            $("."+$(obj).data("mark")).prop("checked",true);
            // 每项选中
            $("."+mark).prop("checked",true);
            $(obj).text("取消");
        }
    }else{
        if($(obj).prop("checked")){
            $("."+mark).prop("checked",true);
            $("button[data-mark='checkall']").text("取消");
            $("button[data-mark='checkall']").addClass("checkbutton");
        }else{
            // 取消选中
            $("."+mark).prop("checked",false);
            $("button[data-mark='checkall']").text("全选");
            $("button[data-mark='checkall']").removeClass("checkbutton");
        }
    }
}

/**
 * @context 批量关联员工
 * @param mark
 */
function relevanceOption(mark)
{
    //全选的 id str
    let str=$("."+mark+':checked').map(function () {

        return $(this).val();
    }).get().join(",");
    if(str == ''){
        layer.msg('请选择员工', {icon: 5, time: 1000} )
        return
    }
    $("#relevance_store").val(str)
    layer.msg('关联成功', {icon: 1, time: 1000},function(){
        layer.closeAll()
    })

}

/**
 * [connect_detail_callback 关联一个员工]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2016-12-10T14:31:19+0800
 * @param    {[object]}     obj [button juquery对象]
 * @return   {[object]}        		[josn对象]
 */
function connect_detail_callback(obj) {
    let id=$(obj).attr('data-id')
    let id_str=$("#relevance_store").val()
    id_str.split(',').forEach(function (ee) {
        if (id == ee)
        {
            layer.msg('请勿重复选择', {icon: 5, time: 1000} )
            return
        }
    })

    id_str=id_str+','+id
    $("#relevance_store").val(id_str)
    layer.msg('关联成功', {icon: 1, time: 1000},function(){
        layer.closeAll()
    })


}



/**
 * [disconnect_detail_callback 关联一个员工]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2016-12-10T14:31:19+0800
 * @param    {[object]}     obj [button juquery对象]
 * @return   {[object]}        		[josn对象]
 */
function disconnect_detail_callback(obj) {
    let id=$(obj).attr('data-id')

    $('.lump' + id).remove();

}



