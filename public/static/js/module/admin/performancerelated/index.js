var lTable;
layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    lTable = table;
    let title={1:'销售额考核',2:'办卡额考核',3:'办卡数量考核',4:'到店率考核',5:'好评率考核',6:'平台抽成'};
    table.render({
        elem: '#myTable',
        url: '/admin/PerformanceRelated/Index?action=ajax',
        method: 'post',
        parseData:function(res){

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[

            {field: 'employee_name', title: '员工姓名'},
            {field: 'department_title', title: '所属部门',width:200},
            {field: 'employee_station_title', title: '所属岗位'},
            {field: 'check_time', title: '考核月度'},
            {field: 'id', title: '销售额考核',templet:'#button_1',},
            {field: 'id', title: '办卡额考核',templet:'#button_2',},
            {field: 'id', title: '办卡数量考核',templet:'#button_3',},
            {field: 'id', title: '到店率考核',templet:'#button_4'},
            {field: 'id', title: '好评率考核',templet:'#button_5',},
            {field: 'id', title: '平台抽成金额',templet:'#button_6',},
            // {field: 'balance_pay_title', title: '获得奖励'},
            {title: '操作', toolbar: '#currentTableBar'}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });
    /*
    添加事件
     */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){

        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/PerformanceRelated/CreateData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });


    })

    /*
    搜索事件
     */
    $("select[name='year']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='month']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    // $("select[name='type']").on('change',function (re) {
    //     $.submitInfo(table);
    // })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '编辑',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/PerformanceRelated/EditData?id='+data.ec_id,
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;
        }
        //
        for (let titleKey in title) {

            if ('check_type' + titleKey===obj.event)
            {
                var index_check_type = layer.open({
                    title: '查看'+title[titleKey],
                    type: 2,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    area: ['60%', '50%'],
                    content: '/admin/PerformanceRelated/EditDataCheckType?id='+data.ec_id+'&type='+titleKey,
                });
                $(window).on("resize", function () {
                    layer.full(index_check_type);
                });
                return false;
            }
        }
        if (obj.event === 'check_type') {


        }

    });




});

$(function () {
    // $('#form-tags-1').tagsInput({
    //     'delimiter': ['、']
    // });
    let flatpickr_register_time=$("#searchTime").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        dateFormat:'Y-m',
        shorthandCurrentMonth:true,
        onChange:function (){
            flatpickr_register_time.close();
            $.submitInfo(lTable);
        }
    })
});

