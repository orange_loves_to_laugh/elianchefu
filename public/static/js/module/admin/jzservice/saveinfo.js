layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload;

    // let service_picurl=$("#service_picurl").val()
    // service_picurl=service_picurl.split(',')

    // let service_context=$("#service_context").val()
    // service_context=service_context.split(',')
    //ldaskvgdangkdakjq
    //二级分类id
    var second_class_id = $("#service_class_pid").val();

    //服务分类id
    var service_class_id = $("#class_pid").val();

    if(second_class_id !=''){
        //查询二级分类
        service_class(service_class_id,second_class_id);
    }
    // 是否可重复下单
    form.on('radio(repeat_order)', function(data){
        //是否可重复下单  1可重复 2 不可重复
        if(data.value == 1){
            $(".showRepeat").css("display","none");

        }else{
            $(".showRepeat").css("display","flex");
            var value =  $('input[name="repeat_cycle"]').val();
            $('input[name="repeat_cycle"]').val('').focus().val(value);
        }
    });
    //保养记录
    form.on('radio(is_maintain)', function(data){


        //服务  1不显示 2 显示
        if(data.value == 1){
            serviceLogInfo(1)

            $('.maintain_log').css('display', 'block');

        }else{
            $('.maintain_log').css('display', 'none');

        }
    });
    //维修记录
    form.on('radio(is_repair)', function(data){

        if(data.value == 1){
            serviceLogInfo(2)

            $('.repair_log').css('display', 'block');

        }else{
            $('.repair_log').css('display', 'none');

        }
    });
    //质保记录
    form.on('radio(is_warranty)', function(data){


        //服务  1不显示 2 显示
        if(data.value == 1){
            serviceLogInfo(3)

            $('.warranty_log').css('display', 'block');

        }else{
            $('.warranty_log').css('display', 'none');

        }
    });
    form.on('radio(is_relation)', function(data){


        //服务  1不显示 2 显示
        if(data.value == 1){

            $('.is_service').css('display', 'block');

        }else{
            $('.is_service').css('display', 'none');

        }
    });
    form.on('radio(is_parts)', function(data){


        //服务  1不显示 2 显示
        if(data.value == 1){

            $('.button_parts').css('display', 'block');
            $('#parts').css('display', 'none');

        }else{
            $('#parts').css('display', 'block');
            $('.button_parts').css('display', 'none');

        }
    });

    //提交表单
    form.on('submit(saveBtn)', function (data) {

        if (data.field.service_title.indexOf('-') > 0) {
            layer.msg('服务名称含有特殊字符', {icon: 2, time: 1000});
            return
        }

        $.ajax({
            url: '/admin/JzService/Save?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })
                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });

    });
    //积分 余额 卡券 保存
    form.on('submit(saveBtnPrize)', function (data) {
        var id =data.field.id
        var prize_type =data.field.prize_type
        var prize_title =data.field.prize_title
        var giving_number =data.field.giving_number
        var work_time =data.field.work_time

        if( prize_title == ''){
            layer.msg('卡券名称为空', {icon: 5, time: 1000} )
            return
        }
        if( giving_number == ''){
            layer.msg('赠送数量为空', {icon: 5, time: 1000} )
            return
        }
        if( work_time == ''){
            layer.msg('有效期为空', {icon: 5, time: 1000} )
            return
        }



        //红包判断

        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/JzService/giveServiceSession',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {
                    layer.closeAll(); //关闭所有页面层
                    $('#giveTypeDisplay').css('display', 'block');
                    $('#giveInfo').html(res.data);



                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });





    });
    form.on('submit(saveBtnLog)', function (data) {
        var id =data.field.id
        var type =data.field.type
        var log_title =data.field.log_title
        var log_validity =data.field.log_validity
        var log_kilometers =data.field.log_kilometers
        var log_scope =data.field.log_scope

        if(type == 1){
            //保养记录
            if( log_title == ''){
                layer.msg('保养名称为空', {icon: 5, time: 1000} )
                return
            }
            if( log_validity == ''){
                layer.msg('有效期为空', {icon: 5, time: 1000} )
                return
            }
            if( log_kilometers == ''){
                layer.msg('有效公里数为空', {icon: 5, time: 1000} )
                return
            }
        }else if(type == 3){
            //质保记录

            if( log_validity == ''){
                layer.msg('质保时间为空', {icon: 5, time: 1000} )
                return
            }
            if( log_kilometers == ''){
                layer.msg('质保公里数为空', {icon: 5, time: 1000} )
                return
            }
            if( log_scope == ''){
                layer.msg('质保范围为空', {icon: 5, time: 1000} )
                return
            }
        }

        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/JzService/serviceLogSession',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {
                    layer.closeAll(); //关闭所有页面层
                    if(type == 1){
                        $('#maintainDisplay').css('display', 'block');
                        $('#maintainInfo').html(res.data);

                    }else if(type == 3){
                        $('#warrantyDisplay').css('display', 'block');
                        $('#warrantyInfo').html(res.data);

                    }




                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });





    });

    //查询二级分类
    $("select[name='service_class_id']").on('change',function (re) {
        var val = $("select[name='service_class_id']").val()

        service_class(val);


    })
    //单图上传

    $.singleupload(upload,'headImg','imgurlHidden','demoText','imgurl')
    $.singleupload(upload,'headImgservice_advert','imgurlHiddenservice_advert','demoTextservice_advert','imgurlservice_advert')

    //多图上传
    //service_picurl
    $.multiupload(upload,'multiple_banner','div_slide_show','service_picurl')

    //service_context
    $.multiupload(upload,'multiple_banner_context','div_slide_show_context','service_context')
    /*
    删除 service_picurl
     */
    // is_relation();

    $(".layui-upload-list").on('click','img',function (re){
        let elem=$(this).parents('.card-body').find(".hidden")

        $.delMultipleImgs($(this),'banner',elem)
    });

    /*
    添加门店
     */

    $("#add_biz").on('click',function (re){
        relevanceStore('service');

    });
    /*选择商品*/
    $("#add_biz_pro").on('click',function (re){
        let btn=["商品","配件","耗材"];
        layer.open({
            type:1,
            skin:"prizeconfirm",
            title: "选择商品类型",
            content:false,
            area:['350px',"130px"],
            maxWidth:'200px',
            btn: btn,
            btn1: function (index, layero) {
                relevanceBizpro('1');
                layer.close(index);
                return false

            },
            btn2: function (index, layero) {
                relevanceBizpro('2');
                layer.close(index);
                return false
            } ,
            btn3: function (index, layero) {
                relevanceBizpro('3');
                layer.close(index);
                return false
            } ,

            cancel: function () {
                //右上角关闭回调
                //return false 开启该代码可禁止点击该按钮关闭
                layer.close();
            }
        });
    });
    /*关联配件*/
    $("#add_biz_pro_paret").on('click',function (re){
        relevanceBizpro('2');
    });

    /*
    添加赠送服务弹窗
     */
    let index=''
    $("#add_service").on('click',function (re){
        let p_bizlist =new Promise(function (resolve, reject) {
            $.ajax({
                url:'/admin/JzService/ServiceListStr',
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
          p_bizlist.then(
            function (res) {

                let bg_num=$('body').attr('data-sa-theme')


                let index1 = layer.open({
                    title: '添加',
                    type: 1,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['500px', '600px'],
                    content: res.data
                });


                $(window).on("resize", function () {
                    layer.full(index1);
                });


                $(".layui-layer").css({
                    backgroundImage: 'url(http://192.168.1.128:8082/static/img/bg/'+bg_num+'.jpg)',
                });
            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )

    })
    is_maintain();//是否关联保养记录
    is_repair();//是否关联维修记录
    is_warranty();//是否关联质保记录
    is_relation();//是否关联服务
    is_parts(); //是否关联配件
    discount_type_list(); //折扣类型


});

function is_maintain(){
    var is_maintain = $('input[name=is_maintain]:checked').val();

    if(is_maintain == 1){

        $('.maintain_log').css('display', 'block');

    }else{
        $('.maintain_log').css('display', 'none');

    }


}
function is_repair(){
    var is_repair = $('input[name=is_repair]:checked').val();

    if(is_repair == 1){
        $('.repair_log').css('display', 'block');

    }else{
        $('.repair_log').css('display', 'none');

    }


}
function is_warranty(){
    var is_warranty = $('input[name=is_warranty]:checked').val();

    if(is_warranty == 1){
        $('.warranty_log').css('display', 'block');

    }else{
        $('.warranty_log').css('display', 'none');

    }


}
function is_relation(){
    var is_relation = $('input[name=is_relation]:checked').val();

    //服务  1不显示 2 显示
    if(is_relation == 1){

        $('.is_service').css('display', 'block');

    }else{
        $('.is_service').css('display', 'none');

    }


}
//是否关联配件
function is_parts(){
    var is_parts = $('input[name=is_parts]:checked').val();

    //服务  1不显示 2 显示
    if(is_parts == 1){

        $('.button_parts').css('display', 'block');

        $('#parts').css('display', 'none');

    }else{
        $('#parts').css('display', 'block');
        $('.button_parts').css('display', 'none');

    }


}
function discount_type_list(){
    var discount_type_list =$("select[name='discount_type']").val();
    if(discount_type_list !=''){
        $('.discount_type_list').css('display', 'block');
    }else{
        $('.discount_type_list').css('display', 'none');

    }
}
//选择折扣类型  显示 折扣列表
$("select[name='discount_type']").on('change',function (re) {
    // $('#formbutton').click();

    $('.discount_type_list').css('display', 'block');
    getServiceDiscount($("#service_id").val())
})
getServiceDiscount($("#service_id").val())

//折扣列表 点击查看
function ServiceDiscount(serviceId){
    //获取折扣类型 根据 类型 查询 对应的 会员折扣 存到缓存里
   let discount_type = $("select[name='discount_type']").val();

    $.ajax({
        url: '/admin/JzService/ServiceDiscount',
        type: 'post',
        data: {
            discount_type,
            serviceId

        },
        dataType: 'json',
        success: function(res) {


            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:'服务折扣',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });
            }
        }
    })
}
// 进入页面先执行一下  获取折扣类型的列表  用来更新缓存  和折扣类型
function getServiceDiscount(serviceId){
    //获取折扣类型 根据 类型 查询 对应的 会员折扣 存到缓存里
    let discount_type = $("select[name='discount_type']").val();

    $.ajax({
        url: '/admin/JzService/ServiceDiscount',
        type: 'post',
        data: {
            discount_type,
            serviceId

        },
        dataType: 'json',
        success: function(res) {


            if (res.status == true) {
           /*     a= layer.open({
                    type: 1,
                    title:'服务折扣',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });*/
            }
        }
    })
}
//修改折扣
function ServiceDiscountWrite(id,k,value,mark,discount){

    let title ="请输入折扣";

    var index = layer.prompt({
        formType: 0,
        value: value,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        if(/^\d+(\.\d{1,2})?$/.test(value)){
            if (isNaN(value)) {
                layer.msg('请输入数字！', {icon: 5, time: 1000} )
                return;
            }else if(value >1 ){
                layer.msg('请输入大于0小于等于1的数字', {icon: 5, time: 1000} )

                return;
            }else if(value <=0){
                layer.msg('请输入大于0小于等于1的数字', {icon: 5, time: 1000} )

                return;
            }
        }else{
            layer.msg('请输入精度为两位小数', {icon: 5, time: 1000} )
            return;

        }


        $.ajax({
            url: '/admin/JzService/ServiceDiscountWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'id':id,
                'k':k,
                'value':value,
                'mark':mark,
                'discount':discount,
            },
            success: function (data) {

                if (data.status == true) {
                    layer.closeAll();
                    ServiceDiscount();

                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}






function service_class(val,second_class_id=''){
    $.ajax({
        url: '/admin/JzService/serviceClass',
        type: 'post',
        dataType: 'json',
        data:{
            service_class_id:val,
            second_class_id:second_class_id,
        },
        success: function(res) {
            if (res.status == true) {
                $('#classdiv').css('display', 'block');
                if(res.data == ''){
                    $('#classdiv').css('display', 'none');

                }else{

                    $('#classPid').html(res.data);

                }
            }  else {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+res.code+':</span>'+res.msg
                })
                return false
            }
        },
        error: function(re) {

            layer.open({
                title: '错误信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    });

}
/*
删除赠送服务
 */
function deltr(obj) {

    if ($('.add_service').find('.layui-table tr').length < 3)
    {
        $('.add_service').html('<span class="no-html">暂无数据</span>')
        return
    }
    $(obj).parent().parent().remove();
}

/*
添加赠送服务
 */
function Chooseservice(obj) {
    //名称
    let title=$(obj).find('td').html()
    let id=$(obj).attr('data-id')


    //判断是否重复添加
    let tr_elem=$('.add_service').find('.layui-table tr')

    for (let trElemKey in tr_elem)
    {
        if (id == tr_elem.eq(trElemKey).find('input').val()) {
            layer.close(layer.index)
            return
        }

    }

    // $('.add_biz').html(new_html);
    if($('.add_service').find('.layui-table').length<1)
    {
        let new_html='';
        new_html+=
            '<table class="layui-table"  class="layui-table" style="width: 50%;">\n' +
            '        <colgroup>\n' +
            '            <col width="150">\n' +
            '            <col width="100">\n' +
            '        </colgroup>\n' +
            '        <thead>\n' +
            '        <tr>\n' +
            '            <th>名称</th>\n' +
            '            <th>操作</th>\n' +
            '        </tr>\n' +
            '        </thead>\n' +
            '        <tbody>\n' +
            '\n' +
            '        <tr>\n' +
            '            <td>'+title+'<input name="giving_service_id[]" type="hidden" value="'+id+'">'+'</td>\n' +
            '            <td><button type="button" data-id="'+id+'" onclick="deltr(this)"    class="layui-btn additem">删除</button></td>\n' +
            '        </tr>\n' +
            '        </tbody>\n' +
            '    </table>'
        $('.add_service').html('')
        $('.add_service').append(new_html);
    }else{
        let tr = $(".add_service .layui-table tr:last");
        let aa='<tr>\n' +
            '            <td>'+title+'<input name="giving_service_id[]" type="hidden" value="'+id+'">'+'</td>\n' +
            '            <td><button type="button" data-id="'+id+'"  onclick="deltr(this)" class="layui-btn">删除</button></td>\n' +
            '        </tr>'
        tr.after(aa);
    }
    //修改bizpro_id值

    //关闭最新弹出层
    layer.close(layer.index)


}

/**
 * @context 搜索赠送服务
 */
function searchRelevanceService()
{
    var e =window.event;

        let search_id=$("#selectPointOfInterest").val()
        if (search_id != '' && search_id != undefined) {}
            $.ajax({
                url: '/admin/JzService/SearchService',
                type: 'post',
                data: {
                    service_class_id:search_id
                },
                dataType: 'json',
                success: function(res) {

                    if (res.status) {
                        //$("#storebody").empty()

                        let new_element=''
                        if (res.data.length < 1) {
                            new_element+='<tr ><td>未获取到相关数据</td></tr>'
                        }else{
                            res.data.forEach(function(e) {

                                new_element+='<tr onclick=\'Chooseservice(this)\'  data-id="'+ e.id+'"><td>'+e.service_title+'</td></tr>'
                                // let oldelement = $(".lump" + e.id);
                                //new_element+=$(".lump" + e.id).html()
                                //$("#storebody").empty()
                                //$("#storebody").children(":first").before(oldelement);

                            })
                        }
                        $("#storebody").empty()

                        $("#storebody").append(new_element)
                    }
                }
            })


}

//关联服务：搜索服务
function activeTypeService(type,mark,mtype){

    p= layer.prompt({
        formType: 0,
        value: '',
        title: "请输入服务名称搜索",
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/ServicePro/active_search',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':mark,
                'mtype':mtype,
                'value':value,

            },
            success: function (res) {
                if (res.status == true) {
                    pop= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}

//关联服务：弹窗内搜索服务
function searchServiceName()
{
    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        var activeType = $("#activeType").val();
        var mark = $("#mark").val();
        var mtype = $("#mtype").val();
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/ServicePro/searchServiceName',
                type: 'post',
                data: {
                    search_title,
                    activeType,
                    mark,
                    mtype
                },
                dataType: 'json',
                success: function(res) {



                    if (res.status == true) {
                        // layer.close(pop)
                        // pop= layer.open({
                        //     type: 1,
                        //     title:res.title,
                        //     skin: 'layui-layer-custom', //加上边框
                        //     area: ['80%', '80%'], //宽高
                        //     content: res.data
                        // });
                        res.list.forEach(function(e) {
                            let oldelement = $(".service" + e.id);

                            $("#storebody").children(":first").before(oldelement);

                        })
                        return
                    }else{
                        layer.msg(res.msg, {icon: 5, time: 1000} )
                    }
                }
            })
        }
    }
}

//关联服务：执行关联
function aloneOption_new(id,type,mark,mtype)
{
    activeBizproName(id,type,mark,mtype)
}


//查看服务价格  根据车来
function checkServiceCarPrice(id){
    layer.open({
        type: 2,
        title:'服务价格 ',
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['80%', '80%'],
        content: '/admin/biz/servicePrice?service_id='+id
    });

}

/**
 * @context 查询 显示 商品二级分类  服务名称  会员卡直接显示
 * @param mark
 * @param mtype: active 活动管理  task  任务管理
 */
function activeCreateGive(id,type,mark,mtype){
    $.ajax({
        url: '/admin/JzService/activeCreateGive',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {

            if (res.status == true) {
                if(type == 'member'){

                    layer.closeAll('page'); //关闭所有页面层
                    if(mark == 'type'){
                        $('#activeTypeDisplay').css('display', 'block');
                        $('#activeInfo').html(res.data);
                    }else{
                        $('#activeGiveDisplay').css('display', 'block');
                        $('#activeGiveInfo').html(res.data);
                    }
                }else{
                    a= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });
                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 查询 显示 商品名称
 * @param mark
 */
function activeBizproName(id,type,mark,mtype){
    $.ajax({
        url: '/admin/JzService/activeBizproName',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {

            if (res.status == true) {

                if(type == 'pro'){
                    a= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });
                }else{
                    layer.closeAll('page'); //关闭所有页面层
                    var bstitle = res.bstitle
                    var detail_price = res.detail_price
                    var pro_service_id = res.pro_service_id

                    if(mark == 'giveService'){
                        activeSession(id,type,mark,bstitle,pro_service_id)

                    }else{
                        activeSession(id,type,mark,bstitle,pro_service_id)

                    }

                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
//获取缓存列表
function activeSession(id,type,mark,bstitle,pro_service_id){
    $.ajax({
        url: '/admin/JzService/serviceSession',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'bstitle':bstitle,
            'pro_service_id':pro_service_id,

        },
        success: function (res) {
            if (res.status == true) {

                layer.closeAll('page'); //关闭所有页面层
                if(mark == 'privilege'){
                    //关联服务

                    $('#activeTypeDisplay').css('display', 'block');
                    $('#activeInfo').html(res.data);
                }else{
                    //赠送服务 弹窗 选择数量 有效期
                    ab=layer.open({
                        type: 1,
                        title:'服务赠送',
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: '<form id="form1"  class="layui-form" >\n' +
                            '\n' +
                            '            <div class="card">\n' +
                            '\n' +
                            '                <div class="card-body">\n' +
                            '\n' +
                            '                    <input type="hidden" name="prize_type"  value="'+type+'">\n' +
                            // '                    <input type="hidden" name="detail_price"  value="'+detail_price+'">\n' +
                            '                    <input type="hidden" name="pro_service_id"  value="'+pro_service_id+'">\n' +
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券名称</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="prize_title"  type="text" name="prize_title" value="'+bstitle+'" class="form-control form-control-lg" placeholder="请填写卡券名称">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +
                            '                    <!--task_type-->\n' +
                            '                   <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="giving_number"  type="number" min=\'1\' step="1" name="giving_number" value="1" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>'+

                            '\n' +
                            '\n' +

                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期(天)</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="work_time"  type="number" min=\'1\' step="1" name="work_time" value="" class="form-control form-control-lg" placeholder="请填写有效期">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +

                            '\n' +
                            '\n' +
                            '\n' +
                            '                </div>\n' +
                            '\n' +
                            '            </div>' +
                            '            <div class="card">\n' +
                            '                <div class="card-body">\n' +
                            '                    <div class="btn-demo">\n' +
                            '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnPrize" >保存</button>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '            </div>'
                    });

                }
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
// 修改 删除 之后 显示的页面（活动类型  活动赠送）
function giveServiceTypeList(){
    $.ajax({
        url: '/admin/JzService/giveServiceTypeList',
        type: 'post',
        dataType: 'json',
        data: { },
        success: function (res) {
            if (res.status == true) {

                layer.closeAll('page'); //关闭所有页面层
                $('#giveTypeDisplay').css('display', 'block');
                $('#giveInfo').html(res.data);
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
//修改数量 有效期
function giveServiceWrite(id,k,val,mark,vmark){
    if(vmark == 2){
        var title = '请修改赠送数量'

    }else if(vmark == 3){
        var title = '请修改有效期'

    }

    layer.prompt({
        formType: 0,
        value: val,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){


        $.ajax({
            url: '/admin/JzService/giveServiceWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'id':id,
                'k':k,
                'val':val,
                'value':value,
                'mark':mark,
                'vmark':vmark,
            },
            success: function (data) {

                if (data.status == true) {
                    layer.closeAll();
                    giveServiceTypeList();

                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}
//删除
function delServiceInfo(id,k,mark,type=''){

    $.ajax({
        url: '/admin/JzService/delServiceInfo',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'k':k,
            'mark':mark,
            'type':type,

        },
        success: function (data) {
            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                },function(){
                    $('#serviceDetailTr' + id).remove();
                });

            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}
/**
 * @context 门店已关联列表
 * @param id
 */
function ServiceRelation(id){
    $.ajax({
        url: '/admin/FileOperation/ServiceRelation',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:'关联门店列表 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 门店已关联列表  服务关联门店
 * @param id
 */
function ServiceRelationPrice(){
    //获取所有关联的id   和 对应的结算价格
    //门店结算价格字符串
    let biz_id_price=$("input[name='biz_id_price']").val()
    //门店id
    let biz_id=$("input[name='biz_id']").val()

    $.ajax({
        url: '/admin/FileOperation/ServiceRelationPrice',
        type: 'post',
        dataType: 'json',
        data: {
            biz_id_price,
            biz_id

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:'关联门店列表 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 商品已关联列表
 * @param id  服务id
 * @param mark  1 商品 2 配件 3 耗材
 */
function ServiceRelationPro(id,mark){
    if(mark == 1){
        var title = '关联配件列表';
    }else if(mark == 2){
        var title = '关联配件列表';

    }else{
        var title = '关联耗材列表';

    }
    $.ajax({
        url: '/admin/FileOperation/ServiceRelationPro',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'mark':mark,

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:title,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 服务关联记录
 * @param type  1 保养记录 2 维修记录 3 质保记录
 */
function serviceLogInfo(type){
    if(type == 1){
        layer.open({
            type: 1,
            title:'保养记录设置 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['1200px', '600px'], //宽高
            content: ' <form id="form1"  class="layui-form" >\n' +
                '\n' +
                '            <div class="card">\n' +
                '\n' +
                '                <div class="card-body">\n' +
                '\n' +
                '                    <input type="hidden" name="type"  value="'+type+'">\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>保养名称</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="log_title"  type="text" name="log_title" value="" class="form-control form-control-lg" placeholder="请填写保养名称">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>\n' +
                '                    <!--task_type-->\n' +
                '                   <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="log_validity"  type="number" min=\'1\' step="1" name="log_validity" value="" class="form-control form-control-lg" placeholder="请填写有效期（单位：天）">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>'+

                '\n' +
                '\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效公里数</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="log_kilometers"  type="number" min=\'1\' step="1" name="log_kilometers" value="" class="form-control form-control-lg" placeholder="请填写有效公里数">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>\n' +

                '\n' +
                '\n' +
                '\n' +
                '                </div>\n' +
                '\n' +
                '            </div>' +
                '            <div class="card">\n' +
                '                <div class="card-body">\n' +
                '                    <div class="btn-demo">\n' +
                '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnLog" >保存</button>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>'
        });

    }else if(type == 2){
        let title ="请输入维修记录名称";

        var index = layer.prompt({
            formType: 0,
            value: '',
            title: title,
            area: ['800px', '350px'] //自定义文本域宽高
        }, function(value, index, elem){

            $.ajax({
                url: '/admin/JzService/serviceLogSession',
                type: 'post',
                dataType: 'json',
                data: {
                    type,
                    log_title:value
                },
                success: function (data) {

                    if (data.status == true) {
                        layer.closeAll();
                        $('#repairDisplay').css('display', 'block');
                        $('#repairInfo').html(data.data);

                    }
                },
                error: function (err) {
                    layer.msg(err);
                }
            });
        });

    }else if(type == 3){
        layer.open({
            type: 1,
            title:'质保记录设置 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['1200px', '600px'], //宽高
            content: ' <form id="form1"  class="layui-form" >\n' +
                '\n' +
                '            <div class="card">\n' +
                '\n' +
                '                <div class="card-body">\n' +
                '\n' +
                '                    <input type="hidden" name="type"  value="'+type+'">\n' +

                '                    <!--task_type-->\n' +
                '                   <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>质保时间</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="log_validity"  type="number" min=\'1\' step="1" name="log_validity" value="" class="form-control form-control-lg" placeholder="请填写质保时间(天)">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>'+

                '\n' +
                '\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>质保公里数</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="log_kilometers"  type="number" min=\'1\' step="1" name="log_kilometers" value="" class="form-control form-control-lg" placeholder="请填写质保公里数">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>质保范围</span>\n' +
                '\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <textarea data-verify="required" id="log_scope"  type="text" name="log_scope" value="" class="form-control form-control-lg" placeholder="质保范围"></textarea>\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                    </div>\n' +
                '                    <br>\n' +

                '\n' +
                '\n' +
                '\n' +
                '                </div>\n' +
                '\n' +
                '            </div>' +
                '            <div class="card">\n' +
                '                <div class="card-body">\n' +
                '                    <div class="btn-demo">\n' +
                '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnLog" >保存</button>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>'
        });
    }

}
// 服务关联记录删除
function delServiceLogInfo(id,mark,type=''){
    $.ajax({
        url: '/admin/JzService/delServiceLogInfo',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'mark':mark,
            'type':type,

        },
        success: function (data) {
            if (data.status == true) {

                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                },function(){
                    $('.serviceLogDetailTr'+type).remove();
                    $('#serviceLogDetailTr'+id+type).remove();
                });

            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}

//修改数量 有效期
function logServiceWrite(id,mark,type,val,vmark){
    if(type == 1){
        if(vmark == 1){
            var title = '请修改保养名称'

        }else if(vmark == 2){
            var title = '请修改有效期'

        }else if(vmark == 3){
            var title = '请修改有效公里数'

        }
    }else if(type == 2){
        if(vmark == 1){
            var title = '请修改维修名称'

        }
    }else if(type == 3){
        if(vmark == 1){
            var title = '请修改质保时间'

        }else if(vmark == 2){
            var title = '请修改质保公里数'

        }else if(vmark == 3){
            var title = '请修改质保范围'

        }
    }


    layer.prompt({
        formType: 0,
        value: val,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){

        if(type == 1){
            if(vmark == 2){
                if (isNaN(value)) {
                    layer.msg('请输入数字！', {icon: 5, time: 1000} )

                    return;
                }

            }else if(vmark == 3){
                if (isNaN(value)) {
                    layer.msg('请输入数字！', {icon: 5, time: 1000} )

                    return;
                }

            }
        }else if(type == 3){
           if(vmark == 2){
                if (isNaN(value)) {
                    layer.msg('请输入数字！', {icon: 5, time: 1000} )
                    return;
                }

            }
        }

        $.ajax({
            url: '/admin/JzService/logServiceWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'id':id,
                'value':value,
                'mark':mark,
                'vmark':vmark,
                'type':type,
            },
            success: function (data) {

                if (data.status == true) {
                    layer.closeAll();
                    logServiceTypeList(type);

                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}
// 修改 删除 之后 显示的页面 关联类型
function logServiceTypeList(type){
    $.ajax({
        url: '/admin/JzService/logServiceTypeList',
        type: 'post',
        dataType: 'json',
        data: {type },
        success: function (res) {
            if (res.status == true) {
                if(type == 1){
                    // $('#maintainDisplay').css('display', 'block');
                    $('#maintainInfo').html(res.data);

                }else if(type == 2){

                    // $('#repairDisplay').css('display', 'block');
                    $('#repairInfo').html(res.data);

                }else if(type == 3){
                    // $('#warrantyDisplay').css('display', 'block');
                    $('#warrantyInfo').html(res.data);

                }
                layer.closeAll('page'); //关闭所有页面层

            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}