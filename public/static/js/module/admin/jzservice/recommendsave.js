layui.use(['layer', 'form','laydate'], function(){
    var layer = layui.layer
        ,form = layui.form;



    $("#start_time").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#start_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
    $("#end_time").flatpickr({
        enableTime: true,

        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#end_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
        /*
    $('select[name="service_class_pid"]').on('change',function (re) {
        let param={class_pid:$(this).val()}
        let p=new Promise(function(resolve, reject){
            $.ajax({
                url: '/admin/JzService/Index?action=ajax',
                type: 'post',
                dataType: 'json',
                data: {param:param},
                success: function(res) {
                    resolve(res)
                },
                error: function(re) {

                    reject(re)
                }
            });
        })

        p.then(
            function (res) {

                let option = '<option value="">请选择关联服务</option>'

                for(let i =0; i in res.data;i++){

                    option += '<option value="'+res.data[i].id+'">'+res.data[i].service_title+'</option>'
                }

                $("select[name='service_id']").html('')
                $("select[name='service_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })
    $('select[name="service_id"]').on('change',function (re) {
        let param={class_pid:$(this).val()}
        $('input[name="service_title"]').val($(this).find('option:selected').text())

    })

    */
    $('select[name="position"]').on('change',function (re) {

        let service_class_pid=$('select[name="service_class_pid"]').val()
        // if (service_class_pid < 1)
        // {
        //     layer.msg('请选择服务分类',{time:2000,icon:2})
        //     return false
        // }
        let position=$(this).val()
        let p=new Promise(function(resolve, reject){
            $.ajax({
                url: '/admin/JzService/RecommendStartTime?action=ajax',
                type: 'post',
                dataType: 'json',
                data: {service_class_pid:service_class_pid,position:position},
                success: function(res) {
                    resolve(res)
                },
                error: function(re) {

                    reject(re)
                }
            });
        })

        p.then(
            function (res) {

                if (typeof res.data != 'object')
                {
                    $("#start_time").val(res.data)
                }

            },
            function (re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })


    /*
  关联操作
   */
    $('input[name="service_title"]').on("click",function ()
    {
        layer.prompt({
            formType: 0,
            value: '',
            title: '服务名称搜索',
        }, function(value, index, elem){
            let data={param:{
                    type:1,
                    keywords:value,
                    fromrecommend:true
                }}
            let p=new Promise(function (resolve, reject) {
                $.ajax({
                    url:'/admin/Applay/SearchData',
                    type: 'post',
                    data:data,
                    async: true,
                    dataType: 'json',
                    success: function (res) {
                        resolve(res)
                    },
                    error: function(re) {
                        reject(re)
                    }
                });
            })
            p.then(
                function(res){

                    if (res.status) {
                        var index = layer.open({
                            type: 1,
                            title:'请选择需要关联的选项',
                            area: ['800px', '600px'],
                            shade: 0.2,
                            skin: 'layui-layer-custom', //加上边框
                            maxmin: true,
                            shadeClose: true,
                            content: res.data
                        });



                        $(window).on("resize", function () {
                            layer.full(index);
                        });
                        return false;
                    }else{
                        layer.msg('未搜索到相关数据', {icon: 2,time:2000});
                    }
                },
                function (res){
                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                        +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            )
            // layer.close(index);
        });


    })

    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/JzService/RecommendDoSave?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                layer.msg('保存成功', {icon: 1, time: 1000},
                    function(){
                        parent.location.reload();
                    })
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });




    });


});
/*
关联服务回调
 */
function connect_detail_callback(obj){
    $('input[name="service_id"]').val($(obj).attr('data-id'))
    $('input[name="service_title"]').val($(obj).attr('data-title'))
    layer.closeAll();

}
