layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    //let class_pid=$('select[name="service_class_pid"]').val()

    let render={
        elem: '#myTable',
        url: '/admin/JzService/RecommendIndex?action=ajax',
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'service_title', title: '服务名称'},
            {field: 'class_pid_title', title: '服务分类'},
            {field: 'position_title', title: '显示位置'},
            {field: 'show_time', title: '显示时间',edit:"text",width:400},
            {field: 'status_title', title: '状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    };
    table.render(render);

    //监听单元格编辑
    table.on('edit(currentTableFilter)', function(obj){
        var value = obj.value //得到修改后的值
            ,data = obj.data //得到所在行所有键值
            ,field = obj.field; //得到字段

        let param={
            id:data.id,
            start_time:value.split('至')[0],
            end_time:value.split('至')[1],
            service_id:data.service_id,
            position:data.position,
            service_class_pid:data.service_class_pid
        }

        $.ajax({
            url: '/admin/JzService/RecommendDoSave?action=ajax',
            type: 'post',
            dataType: 'json',
            data:param,
            success: function(res) {
                layer.msg('保存成功', {icon: 1, time: 1000})
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
        //layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);
    });

    /*
     添加事件
      */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){

        let index = layer.open({
            title: '添加推荐',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzService/RecommendSave'
            // content: '/admin/JzService/UnRecommendList'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
        return false
    })

    /*
      搜索事件
     */
    // $("input[name='keywords']").on('blur',function (re) {
    //     $.submitInfo(table);
    // })
    $("select[name='service_class_pid']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='position']").on('change',function (re) {
        $.submitInfo(table);
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //取消推荐
        if (obj.event === 'delete')
        {
            layer.confirm('真的删除推荐么', function (index) {
                $.ajax({
                    url: '/admin/JzService/DelRecommend',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

        if (obj.event === 'edit')
        {
            let index = layer.open({
                title: '添加推荐',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/RecommendSave?id='+data.id
                // content: '/admin/JzService/UnRecommendList'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }
    });


});