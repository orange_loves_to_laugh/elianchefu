layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;
    let service_id=$.getQueryVariable('service_id')
    table.render({
        elem: '#myTable',
        url: '/admin/JzService/ServiceBizNum?action=ajax&service_id='+service_id,
        method: 'post',
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res)
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'biz_title', title: '门店名称'},
            {field: 'biz_address', title: '门店地址'},
            {field: 'biz_phone', title: '联系方式'},
            {field: 'biz_leader', title: '负责人'},
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });



});