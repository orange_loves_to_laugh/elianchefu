layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    /*
     快捷服务事件
     */
    $('.layui-btn-container').on('click','[data-action="quick"]',function(re) {
        let index = layer.open({
            title: '添加快捷服务',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzService/save_quick'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    /*
     添加事件
    */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
            let index = layer.open({
                title: '添加服务',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/SaveInfo'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });

    })
    table.render({
        elem: '#myTable',
        url: '/admin/JzService/Index?action=ajax&biz_id='+$('#biz_id').val(),
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'service_number', title: '服务编码',templet:function (d) {
                    return "<div style=\"width:100%;height: 100%;text-align: left\" ondblclick=\"changeNumber("+d.id+",this)\">"+d.service_number+"</div>";
                }},
            {field: 'service_title', title: '服务名称'},
            {field: 'class_pid_name', title: '服务分类'},
            {field: 'discount_type', title: '折扣类型',templet:'#discountType'},

            {field: 'service_title', title: '服务价格',sort:true,templet:'#ServiceCarPrice'},
            {field: 'service_biz_num', title: '关联门店数量',sort:true,templet:'#bizTpl'},
            {field: 'real_num', title: '实际服务次数',sort:true},
            {field: 'service_top_title', title: '是否首页推荐'},
            {field: 'service_status_title', title: '状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='class_pid']").on('change',function (re) {
        // $('#formbutton').click();
        $.submitInfo(table);
    })
    $("select[name='discount_type']").on('change',function (re) {
        // $('#formbutton').click();
        $.submitInfo(table);
    });
    $("select[name='service_status']").on('change',function (re) {
        // $('#formbutton').click();
        $.submitInfo(table);
    });

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //console.log(data);return
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑服务',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/SaveInfo?id='+data.id+'&biz_id='+$('#biz_id').val(),
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzService/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                        service_status: obj.data.service_status,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

        if(obj.event === 'biznum'){
            let index = layer.open({
                title: '关联门店',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/ServiceBizNum?service_id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }
        if(obj.event === 'checkServiceCarPrice'){
            layer.open({
                type: 2,
                title:'服务价格 ',
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/servicePrice?service_id='+data.id+'&biz_id='+$('#biz_id').val()
            });

            return false;
        }
    });


    /**
     * toolbar监听事件
     */
    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加服务',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/SaveInfo'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }

    });

});

function changeNumber(id,obj)
{
    var val = parseInt($(obj).html())
    layer.prompt({
        formType: 0,
        value:val,
        title: '请修改服务编号',
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/JzService/changeNumber',
            type: 'post',
            dataType: 'json',
            data: {
                'val':value,
                'id':id,
            },
            success: function (res) {


                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    $(obj).html(value)
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
