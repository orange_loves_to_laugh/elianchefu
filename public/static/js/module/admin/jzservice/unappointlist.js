layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzService/Index?action=ajax&appoint=0',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {type:'checkbox'},
            {field: 'service_title', title: '服务名称'},
            {field: 'service_class_name', title: '服务分类'},
            {field: 'service_time', title: '服务时长'},
            {field: 'service_status', title: '状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'service_info') {
            var index = layer.open({
                title: '服务信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzService/SeviceInfo?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'add_single') {
            let post_data={
                id_arr:data.id,
                action:{appoint:1},
            }
            $.servicesavestatus('确定开启预约么',post_data,obj);
        }

    });

    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add_batch') {  // 监听添加操作

            let post_data=$.datahandle(table,'请选择需要开启预约的服务','id')

            if (Object.keys(post_data).length > 0)
            {
                post_data.action={appoint:1}
                $.servicesavestatus('确定开启预约么',post_data,'');
            }

            // console.log(id_arr); return
        }
    });



});