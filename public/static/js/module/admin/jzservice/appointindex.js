layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

//， 服务提成、商品提成、推荐提成、结算金额、
    table.render({
        elem: '#myTable',
        url: '/admin/JzService/AppointIndex?action=ajax',
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'biz_type_title', title: '门店类型'},
            {field: 'biz_title', title: '门店名称'},
            {field: 'total_address', title: '所在地区'},
            {field: 'class_pid_title', title: '服务分类'},
            {field: 'service_title', title: '服务名称'},

            {field: 'service_deduct', title: '服务提成',edit: 'text'},
            {field: 'biz_deduct', title: '商品提成',edit: 'text'},
            {field: 'recommond_royalty', title: '推荐提成',edit: 'text'},
            {field: 'service_status', title: '结算金额',templet:'#settlement_price'},

        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    //监听单元格编辑
    //监听单元格编辑
    table.on('edit(currentTableFilter)', function(obj){
        var value = obj.value //得到修改后的值
            ,data = obj.data //得到所在行所有键值
            ,field = obj.field; //得到字段
        let param={
            id:data.sb_id,
            service_deduct:data.service_deduct,
            recommond_royalty:data.recommond_royalty,
            biz_id:data.biz_id,
            service_class_id:data.class_pid,
            station_title:'默认名称',
            biz_deduct:data.biz_deduct
        }
        param[field]=value

        $.ajax({
            url: '/admin/JzService/AppointSaveData?action=ajax',
            type: 'post',
            dataType: 'json',
            data:param,
            success: function(res) {
                layer.msg('保存成功', {icon: 1, time: 1000,function(){
                    location.reload()
                    }})
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
        //layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);
    });

    /*
     添加事件
      */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加预约服务',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzService/UnAppointList'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    $("select[name='biz_id']").on('change',function (re) {
        $.submitInfo(table);

    })
    $("select[name='service_id']").on('change',function (re) {
        $.submitInfo(table);

    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //取消预约
        if (obj.event === 'checkprice') {
            let settlement_amount_arr=data.settlement_amount

            if (settlement_amount_arr.length < 1)
            {
                layer.msg('当前服务未设置结算价格',{icon: 2,time:2000})
                return false
            }

          let html= '<table class="table table-bordered mb-0">\n' +
              '        <thead>\n' +
              '        <tr>\n' +
              '            <th>车型</th>\n' +
              '            <th>价格</th>\n' +
              '        </tr>\n' +
              '        </thead>\n' +
              '        <tbody>'

            for (let settlementAmountArrKey in settlement_amount_arr) {
                html+='<tr>\n' +
                    '            <th scope="row">'+settlement_amount_arr[settlementAmountArrKey].car_level_title+'</th>\n' +
                    '            <th >'+settlement_amount_arr[settlementAmountArrKey].settlement_amount+'</th>\n' +
                    '        </tr>'
            }
            html+='</tbody>\n' +
                '    </table>'
            let index= layer.open({
                title:'结算金额',
                type: 1,
                area: ['500px', '500px'],
                skin: 'layui-layer-custom', //加上边框
                content: html
            });

            let bg_num=$('body').attr('data-sa-theme')


            $(".layui-layer").css({
                backgroundImage: 'url(http://192.168.1.128:8082/static/img/bg/'+bg_num+'.jpg)',
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }


    });



    async function start() {

       let file= await  swal({
            title: 'Select image',
            input: 'file',
            inputAttributes: {
                accept: 'image/*',
                'aria-label': 'Upload your profile picture'
            }
        })
        if (file) {
            const reader = new FileReader()
            reader.onload = (e) => {
                swal({
                    title: 'Your uploaded picture',
                    imageUrl: e.target.result,
                    imageAlt: 'The uploaded picture'
                })
            }
            reader.readAsDataURL(file)
        }



    }


});