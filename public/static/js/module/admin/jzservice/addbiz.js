layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    let service_id=$.getQueryVariable('service_id')

    table.render({
        elem: '#myTable',
        url: '/admin/JzService/AddBiz?action=ajax',
        method: 'post',
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res)
            // return {
            //     "code": res.code, //解析接口状态
            //     "msg": res.msg, //解析提示文本
            //     "count": res.total, //解析数据长度
            //     "data": res.data, //解析数据列表
            // };
        },
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {type:'checkbox'},
            {field: 'biz_title', title: '门店名称'},
            {field: 'biz_phone', title: '门店电话'},
            {field: 'biz_typestore', title: '门店类型'},
            {field: 'biz_address', title: '门店地区'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    //关联门店
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'addsingle') {

            let post_data={
                biz_id:data.id,
                service_id:service_id
            }
            addbiz('确定关联门店么',post_data,obj)
        }


    });


    /**
     * 批量关联
     */
    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'addbatch') {  // 监听添加操作
            let post_data=$.datahandle(table,'请选择需要推荐的服务','id')

            if (Object.keys(post_data).length > 0)
            {
                post_data.action={service_top:1}
                $.servicesavestatus('确定推荐服务么',post_data,'');
            }
        }

    });


    /*
    关联方法
     */
    function addbiz(msg,post_data,obj){
        layer.confirm(msg, function (index) {
            $.ajax({
                url: "/admin/JzService/DoAddBiz",
                type: 'post',
                async: true,
                data: post_data,
                dataType: 'json',
                success: function (res) {
                    if(res.status){
                        layer.close(index)
                        parent.layer.msg("操作成功!", {icon:1,time: 1000}, function () {
                            //重新加载父页面
                            parent.location.reload();
                            if (typeof obj==='object') {
                                $(obj)[0].tr.remove()
                            }else{
                                location.reload();
                            }


                        });
                        return;
                    }else{
                        layer.msg(res.msg);
                    }
                }
            });

        });
    }
});