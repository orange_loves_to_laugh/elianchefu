layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzPurchase/QuotedPrice?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[

            {field: 'create_time', title: '日期'},
            {field: 'title', title: '名称'},
            {field: 'title_param', title: '型号'},
            {field: 'brand_title', title: '品牌'},
            {field: 'brandmodel_title', title: '车型'},
            {field: 'configuration_show', title: '配置'},
            {field: 'identification', title: '识别代码'},
            {field: 'purchase_price_show', title: '进价'},
            {field: 'selling_price_show', title: '建议售价'},

             {field: 'oldfactory_price_show', title: '原厂配件'},
             {field: 'offline_price_show', title: '下线配件'},
             {field: 'brand_price_show', title: '品牌配件'},
             {field: 'vicefactory_price_show', title: '副厂配件'},
             {field: 'dismancar_price_show', title: '拆车配件'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    let flatpickr_start=$("#create_time").flatpickr({
        enableTime: false,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (re) {
            $.submitInfo(table);
            flatpickr_start.close()
        }
    })

    $("input[name='keywords']").on('blur',function (re) {

        $.submitInfo(table);
    })

    $("select[name='brandmodel_id']").on('change',function (re) {

        $.submitInfo(table);
    })

    // 监听车牌车型联动
    $("select[name='brand_id']").on('change',function (re) {
        $.submitInfo(table);
        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/JzPurchase/BrandList?pid="+re.currentTarget.value,
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                let option = '<option value="">车辆型号</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].brand_title+'</option>'
                }

                $("select[name='brandmodel_id']").html('')
                $("select[name='brandmodel_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑报价',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzPurchase/SaveQuotedPricePage?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }


    });


});