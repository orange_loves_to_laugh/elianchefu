layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzPurchase/BizDispath?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[

            {field: 'biz_title', title: '门店名称',width:250},
            {field: 'biz_type_title', title: '门店类型',width:100},
            {field: 'biz_id', title: '门店编号',width:100},
            {field: 'biz_address', title: '门店地址',width:300},
            {field: 'biz_phone', title: '门店电话'},
            {field: 'create_time', title: '发货日期'},
            {field: 'dispath_num', title: '发货数量'},
            {field: 'dispath_price', title: '发货金额'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}

        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });



    $("input[name='keywords']").on('blur',function (re) {

        $.submitInfo(table);
    })

    $("select[name='biz_type']").on('change',function (re) {

        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
       // console.log(data);return
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Pro/dispath_detail?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }


    });

});