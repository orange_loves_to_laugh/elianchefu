layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload
        ,$ = layui.$;
    /*
       初始化时间
        */
    $("#register_time").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#register_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })



    //监听提交
    form.on('submit(saveBtn)', function (data) {
        var id =data.id

        var mark = $("input[name='mark']").val();
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Member/DoSaveData?action=ajax&id='+id+'&mark='+mark,
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                            //window.location.reload();
                        })

                }  else {

                    layer.msg(res.msg, {icon: 5, time: 1000})

                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });





    });





});
