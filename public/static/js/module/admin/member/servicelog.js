layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    //初始化member_id
    let member_id=$.getQueryVariable('memberId')
    $('input[name="member_id"]').val(member_id)
    //初始化时间
    let flatpickr_start=$("#start_time").flatpickr({
        enableTime: false,
        allowInput:true,

        onChange:function (re) {
            flatpickr_start.close()
           // $.submitInfo(table);
        }
    })
    // let flatpickr_end=$("#end_time").flatpickr({
    //     enableTime: false,
    //     allowInput:true,
    //     onChange:function (re) {
    //         flatpickr_end.close()
    //       //  $.submitInfo(table);
    //     }
    // })
    //车牌号 门店名称 车型 车系 结算时间（服务项目 数量  服务金额 评论-号中 差）
//支付方式 是否使用凯泉 卡券抵用金额 实际支付
    table.render({
        elem: '#myTable',
        url: '/admin/Member/serviceLog?action=ajax&member_id='+member_id,
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData:function(res){
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            {field: 'car_liences', title: '车牌号',width:100},
            {field: 'biz_title', title: '门店名称'},
            {field: 'logo_title', title: '车辆品牌',width:100},
            {field: 'sort_title', title: '车辆系别',width:100},
            {field: 'server_arr', title: '服务名称'},
            {field: 'pay_create', title: '结算时间'},
            // {field: 'service_arr', title: '数据', templet:"#servicearr",width:600},
            {field: 'pay_type_title', title: '支付方式'},
            // {field: 'car_liences', title: '是否使用卡券'},
            {field: 'voucher_price', title: '卡券抵用金额'},

            {field: 'pay_price', title: '合计'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}

        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '订单详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['50%', '60%'],
                content: '/admin/Member/ServiceDetail?order_number='+data.order_number+'&evaluation_status='+data.evaluation_status,

            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
    })

    $("input[name='pay_create']").on('blur',function (re) {
        // var e =window.event;
        // if (e.keyCode == 13) {}
        $.submitInfo(table);

    })
    // $("input[name='end_time']").on('blur',function (re) {
    //
    //     $.submitInfo(table);
    //
    // })

    $("input[name='service_title']").on('blur',function (re) {

        $.submitInfo(table);

    })

    $("select[name='comment_level']").on('change',function (re) {

        $.submitInfo(table);
    })
    $("select[name='biz_id']").on('change',function (re) {

        $.submitInfo(table);
    })



});
