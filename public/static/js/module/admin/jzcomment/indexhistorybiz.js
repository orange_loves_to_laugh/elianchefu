layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    let cate=$.getQueryVariable('cate')
    let biz_id=$.getQueryVariable('biz_id')
    let employ_id=$.getQueryVariable('employ_id')
    let id_arr=$.getQueryVariable('id_arr')


    let render={
        elem: '#myTable',
        url: '/admin/jzComment/IndexHistoryBiz?action=ajax&biz_id='+biz_id+'&id_arr='+id_arr,
        method: 'post',
        // toolbar: '#toolbarDemo',
        parseData: function (res) { //res 即为原始返回的数据

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[
            //门店名称 车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值
            {field: 'biz_title', title: '门店名称',width:300},
            {field: 'car_liences', title: '车牌号'},
            {field: 'car_model', title: '车辆品牌型号'},
            {field: 'car_level', title: '车型',width:130},
            {field: 'create_time', title: '评论时间'},
            {field: 'service_title', title: '服务名称',},
            {field: 'score_title', title: '评论状态',width:100},
            {field: 'score', title: '评论分值',width:100},
            {title: '操作', toolbar: '#currentTableBar_biz', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }

    table.render(render);

    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'comment_user') {
            if (data.member_id.length < 1)
            {
               layer.msg('<span style="color:#fff">未获取到用户信息</span>')
               return
            }
            var index = layer.open({
                title: '用户评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexUserComment?member_id='+data.member_id+'&create_time='+data.create_url,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }
    });

});