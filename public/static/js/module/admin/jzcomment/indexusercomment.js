layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    let member_id=$.getQueryVariable('member_id');

    let create_time=$.getQueryVariable('create_time')

    table.render({
        elem: '#myTable',
        url: '/admin/jzComment/IndexUserComment?action=ajax&member_id='+member_id+'&create_time='+create_time,
        method: 'post',
        // toolbar: '#toolbarDemo',
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[
            {field: 'biz_title', title: '门店名称'},
            {field: 'car_liences', title: '车牌号'},
            {field: 'create_time', title: '评论时间'},
            {field: 'service_title', title: '服务名称',width:500},
            {field: 'score', title: '评论分数'},
            {field: 'score_title', title: '评论等级'},
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    });


    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'store') {
            var index = layer.open({
                title: '门店评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexHistory?cate=1&biz_id='+data.biz_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'staff') {

            var index = layer.open({
                title: '员工评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexHistory?cate=2&biz_id='+data.biz_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }
    });

    /**
     * toolbar监听事件
     */
    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzInfor/SaveData'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }

    });

    /*
    监听搜时间搜索
     */
    // $("input[name='create_time']").on('blur',function (re) {
    //     console.log($(this).val());
    //     submitInfo();
    // })

    function submitInfo(data) {
        let param = {};
        let t = $('#formInfo [name]');

        $.each(t, function () {

            if (this.value.length < 1) {
                return false
            }
            param[this.name] = data;
        });

        //let result = JSON.stringify(d);

        //执行搜索重载
        table.reload('myTable', {
            page: {
                curr: 1
            }
            , where: {
                param
            }
        }, 'data');
        return false;
    };
});