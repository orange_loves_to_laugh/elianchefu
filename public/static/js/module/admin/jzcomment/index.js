layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;





    table.render({
        elem: '#myTable',
        url: '/admin/jzComment/Index?action=ajax',
        method: 'post',

        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res)
            $("#total_score").html("总得分:"+res.count.score+"分");
            $("#total_top").html("好评次数:"+res.count.count+"次");
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            {field: 'biz_title', title: '门店名称'},

            {field: 'biz_type_title', title: '门店类型',width:100},

            {field: 'total_score', title: '总得分',width:90,sort:true},

            {field: 'score_even', title: '平均得分',width:100},

            {field: 'score_even', title: '好评数量 / 评论率',templet:"#countrate_f"},

            {field: 'total_score', title: '中评数量 / 评论率',templet:"#countrate_s"},

            {field: 'score_even', title: '差评数量 / 评论率',templet:"#countrate_n"},

            {field: 'comment_total', title: '评论总数',width:100},

            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    });


    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'store') {

             let url='/admin/JzComment/IndexHistoryBiz?biz_id='+data.biz_id+'&id_arr='+data.id_arr

            var index = layer.open({
                title: '门店评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content:url,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'staff') {

            var index = layer.open({
                title: '员工评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexHistoryEmployee?biz_id='+data.biz_id+'&id_arr='+data.id_arr,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }
    });


    /*
        搜索事件
         */
    $("select[name='year']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='month']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='biz_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    /*
    监听搜时间搜索
     */
    // $("input[name='create_time']").on('blur',function (re) {
    //     console.log($(this).val());
    //     submitInfo();
    // })

    // function submitInfo(data) {
    //     let param = {};
    //     let t = $('#formInfo [name]');
    //
    //     $.each(t, function () {
    //
    //         if (this.value.length < 1) {
    //             return false
    //         }
    //         param[this.name] = data;
    //     });
    //
    //     //let result = JSON.stringify(d);
    //
    //     //执行搜索重载
    //     table.reload('myTable', {
    //         page: {
    //             curr: 1
    //         }
    //         , where: {
    //             param
    //         }
    //     }, 'data');
    //     return false;
    // };
});