layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;


    let biz_id=$.getQueryVariable('biz_id')

    let id_arr=$.getQueryVariable('id_arr')

    let render= {
        elem: '#myTable',
        url: '/admin/jzComment/IndexHistoryEmployee?action=ajax&biz_id='+biz_id+'&id_arr='+id_arr,
        method: 'post',
        // toolbar: '#toolbarDemo',
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },

        defaultToolbar: [],
        cols: [[
            {field: 'biz_title', title: '门店名称',width:300},

            {field: 'employee_name', title: '员工姓名',width:145},

            {field: 'total_score', title: '总得分',width:90},

            {field: 'score_even', title: '平均得分',width:100},

            {field: 'score_even', title: '好评数量 / 好评论率',templet:"#countrate_f"},

            {field: 'total_score', title: '中评数量 / 好评论率',templet:"#countrate_s"},

            {field: 'score_even', title: '差评数量 / 好评论率',templet:"#countrate_n"},

            {field: 'comment_total', title: '评论总数',width:100},
            {title: '操作', toolbar: '#currentTableBar_employ', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }

    table.render(render);

    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //员工评论
        if (obj.event === 'comment_employ') {

            if (data.employee_id < 1) {
                layer.msg('<span style="color:#fff">获取员工信息失败</span>')
                return
            }
            var index = layer.open({
                title: '个人评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexHistoryEmployeeDetail?employ_id='+data.employee_id+'&create_time='+data.create_url,

            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }


    });

});