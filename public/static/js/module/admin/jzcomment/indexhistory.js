layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    let cate=$.getQueryVariable('cate')
    let biz_id=$.getQueryVariable('biz_id')
    let employ_id=$.getQueryVariable('employ_id')
    let render={
        elem: '#myTable',
        url: '/admin/jzComment/IndexHistory?action=ajax&cate='+cate+'&biz_id='+biz_id+'&employ_id='+employ_id,
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            //门店名称 车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值
            {field: 'biz_title', title: '门店名称',width:300},
            {field: 'car_liences', title: '车牌号',width:100},
            {field: 'car_model', title: '车辆品牌型号'},
            {field: 'car_level', title: '车型',width:130},
            {field: 'create_time', title: '评论时间'},
            {field: 'service_title', title: '服务名称',},
            {field: 'score_title', title: '评论状态',width:100},
            {field: 'score', title: '评论分值',width:100},
            {title: '操作', toolbar: '#currentTableBar_biz', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }
    if (cate == 2)
    {
        //员工评论
      render= {
         elem: '#myTable',
         url: '/admin/jzComment/IndexHistory?action=ajax&cate='+cate+'&biz_id='+biz_id+'&employ_id='+employ_id,
         method: 'post',
         // toolbar: '#toolbarDemo',
         defaultToolbar: [],
         cols: [[
             {field: 'biz_title', title: '门店名称',width:300},
              {field: 'employee_name', title: '员工姓名',width:145},
              {field: 'employee_jobs_title', title: '员工岗位',width:150},
              {field: 'total_score', title: '总得分',width:90},
              {field: 'score_even', title: '平均得分',width:100},
              {field: 'score_even', title: '好评数量 / 好评论率',templet:"#countrate_f"},
              {field: 'total_score', title: '中评数量 / 好评论率',templet:"#countrate_s"},
              {field: 'score_even', title: '差评数量 / 好评论率',templet:"#countrate_n"},
              {field: 'comment_total', title: '评论总数',width:100},
              {title: '操作', toolbar: '#currentTableBar_employ', minWidth: 120}
          ]],
         limits: [10, 15, 20, 25, 50, 100],
         limit:10,
         page: true,
         skin: 'line'
     }
    }
    table.render(render);

    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //员工评论
        if (obj.event === 'comment_employ') {
            var index = layer.open({
                title: '个人评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzComment/IndexHistory?cate=3&employ_id='+data.employee_id+'&create_time='+data.create_time,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'comment_user') {
           
            var index = layer.open({
                title: '用户评论历史',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzComment/IndexUserComment?member_id='+data.member_id,
                content: '/admin/JzComment/IndexUserComment?member_id='+data.member_id+'&create_time='+data.create_time,

            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }
    });

});