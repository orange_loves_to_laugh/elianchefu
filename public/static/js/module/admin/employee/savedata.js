layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
        ,$ = layui.$;
    let id=$("#id").val()


    $('#distpicker3').distpicker();
    //初始化离职时间判断
    let employee_status=$('select[name="employee_status"]').val()


    if (employee_status.length < 1 || employee_status >0) {
        $('#quit_time').parents('.card').css('display','none')
    }
    //初始化外呼账号状态
    initcallinfo()

    //入职时间 转正时间 停战时间
    $("#employee_create").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#employee_create").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
    $("#regular_time").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#regular_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
    $("#quit_time").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#quit_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
    /*
    离职时间展示判断
     */
    $("select[name='employee_status']").on('change',function (re) {

        if ($(this).val() < 1) {
            $("#quit_time").parents('.card').css('display','')
        }
        if ($(this).val() >0) {
            $("#quit_time").parents('.card').css('display','none')
        }
    })
    //图片上传
    $.singleupload(upload,'headImg','imgurlHidden','demoText','imgurl')
    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Employee/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });

    });
    /*
    接入混交平台联动
     */
    //监听指定开关
    form.on('radio(switchTest)', function(data){

        if (data.value == 1)
        {
            $('.calling_radio').css('display','')
        }
        if (data.value == 2)
        {
            $('.calling_radio').css('display','none')
        }

    });
    /*
    接入混交平台联动
     */
    inquiry_switchTest()
    //监听指定开关
    form.on('radio(inquiry_switchTest)', function(data){

        if (data.value == 1)
        {
            $('.inquiry_radio').css('display','')
        }
        if (data.value == 2)
        {
            $('.inquiry_radio').css('display','none')
        }

    });
    function inquiry_switchTest(){
        var is_status = $('input[name=inquiry_status]:checked').val();
        if (is_status == 1)
        {
            $('.inquiry_radio').css('display','')
        }
        if (is_status == 2)
        {
            $('.inquiry_radio').css('display','none')
        }
    }

    // 监听一级部门联动
    $("select[name='employee_department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='employee_department_secid']").html('')
                $("select[name='employee_department_secid']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option_station)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    //监听二级部门联动
    $("select[name='employee_department_secid']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                    department_secid:re.currentTarget.value,
                    department_id:$('select[name="employee_department_id"]') .val()
                }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }



                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    /*
    初始化外呼平台信息是否显示
     */
    function initcallinfo(){
        let calling_status=$('input[name="calling_status"]:checked').val()

        //表单显示状态
        if ( calling_status == 2&&$("#id").val()>0)
        {
            $('.calling').css('display','none')
        }
        if ( calling_status == 2&&$("#id").val()<1)
        {
            $('.calling_radio').css('display','none')
        }
        //表格显示状态
        if (calling_status == 1&&id>0)
        {
            $('.calling').css('display','none')
            $('.calling_table').css('display','')
        }
        //来自档案管理
        if($.getQueryVariable('from')=='file')
        {
            $("#L_repass").parents('.card').css("display",'none')
            $('.calling_table').css('display','none')
        }
    }
});




