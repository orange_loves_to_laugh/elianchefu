layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form=layui.form,
        table = layui.table;


    table.render({
        elem: '#myTable',
        url: '/admin/Employee/Index?action=ajax',
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'employee_name', title: '员工姓名',width:100},
            {field: 'employee_sex_title', title: '性别',width:100},
            {field: 'employee_head', title: '员工头像',templet:'#thumb'},
            {field: 'employee_phone', title: '联系方式'},
            {field: 'employee_code', title: '员工工号'},
            {field: 'department_title', title: '所属部门'},
            {field: 'employee_station_title', title: '所属岗位'},
            {field: 'contract_code', title: '是否签订劳动合同',templet:'#contract_code'},
            {field: 'employee_create', title: '入职时间'},
            {field: 'employee_status_title', title: '员工状态'},
            {title: '操作', toolbar: '#currentTableBar', width: 200}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });



        /*
    添加事件
     */
     $('.layui-btn-container').on('click','[data-action="add"]',function(re) {
    let index = layer.open({
        title: '添加员工',
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: '/admin/Employee/SaveData'
    });
    $(window).on("resize", function () {
        layer.full(index);
    });

    });
     // 离职
    var lock=false;
    $('.layui-btn-container').on('click','[data-action="quit"]',function(re) {
        let title = "请输入员工姓名/手机号";

        c = layer.prompt({
            formType: 0,
            value: '',
            title: title,
            area: ['800px', '350px'] //自定义文本域宽高
        }, function (value, index, elem) {
            layer.close(index)
            if(!lock) {
                lock = true;
                let data={param:{
                        keywords:value,
                        employeeStatus:'onTheJob',
                        button:'选择'
                        // notin:'true',
                    }}
                $.ajax({
                    url:'/admin/Employee/RelateEmployee',
                    type: 'post',
                    data:data,
                    async: true,
                    dataType: 'json',
                    success: function (res) {
                        lock=false;
                        var index = layer.open({
                            type: 1,
                            title:'选择要离职的员工',
                            area: ['800px', '600px'],
                            shade: 0.2,
                            skin: 'layui-layer-custom', //加上边框
                            maxmin: true,
                            shadeClose: true,
                            content: res.data
                        });
                        $(window).on("resize", function () {
                            layer.full(index);
                        });

                        return false;
                    },
                    error: function(re) {
                        lock= false;
                        layer.open({
                            title: '操作失败',
                            content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                                +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                        })
                    }
                });
            }
        });

    });

    /*
    编辑事件
     */
        table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit')
        {
            var index = layer.open({
                title: '编辑员工信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Employee/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
            if (obj.event === 'qrcode')
            {
                layer.open({
                    title: data.employee_name,
                    type: 1,
                    skin: 'whiteColor', //加上边框
                    area: ['500px', '500px'], //宽高
                    shadeClose: true,
                    content: '<div class="img" style="margin-left: 10%; margin-top: 5%;" id="qrcode"></div>'
                });

                $("#qrcode").qrcode({
                    render : "canvas",
                    text : 'http://web.elianchefu.com/#/pages/recommend/index?a=scan&form=employeeSal&employeeId='+data.id,
                    //'/takemember/index?assignBizId=0&employeeId='+data.id,
                    width : "400",
                    height : "400",
                    background : "#ffffff",
                    foreground : "#000000",
                    src: '/static/img/logo_qrcode.png',
                    imgWidth:90,
                    imgHeight:90,
                });

                // let qrcode = new QRCode(document.getElementById("qrcode"), {
                //     width: 400,
                //     height: 400,
                //
                // });
                //
                //  qrcode.makeCode("http://web.ecarfu.com/#/pages/recommend/index?mark=platformStaff&employeeId=12&assignBizId=12");
            }
        if (obj.event === 'employee_head')
        {
            let json={
                "title": data.employee_name+'头像', //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": data.employee_name+'头像',
                        "pid": 666, //图片id
                        "src": data.employee_head, //原图地址
                        "thumb": data.employee_head //缩略图地址
                    }
                ]
            }

            layer.photos({
                photos: json
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }

        if (obj.event === 'contract_code_do'){
            if (data.contract_code.length > 0) {
                layer.msg('<span style="color:#fff">员工的劳动合同号为'+data.contract_code+'</span>',{time:3000})
            }else{
                //例子2

                layer.prompt({
                    formType: 0,
                    value: '',
                    title: '请输入劳动合同编号',

                }, function(value, index, elem){
                    $.ajax({
                        url: '/admin/Employee/Save',
                        data:{id:data.id,contract_code:value},
                        type: 'post',
                        dataType: 'json',

                        success: function(res) {

                            layer.msg('保存成功', {icon: 1, time: 1000},
                                function(){
                                    parent.location.reload();
                                })
                        },
                        error: function(re) {

                            layer.open({
                                title: '错误信息',
                                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                            })
                        }
                    });
                    layer.close(index);
                });
            }
        }
    });


    // 监听一级部门联动
    $("select[name='employee_department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='employee_department_secid']").html('')
                $("select[name='employee_department_secid']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option_station)

                // ******************************************

                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })


    //监听二级部门联动
    $("select[name='employee_department_secid']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                        department_secid:re.currentTarget.value,
                        department_id:$('select[name="department_id"]') .val()
                    }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }



                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    /*
    搜索事件
     */
    $("select[name='employee_department_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee_department_secid']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("select[name='employee_station_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee_search_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
});
function connect_detail_callback(obj){
    var eid = $(obj).attr('data-id');
    layer.closeAll();
    let index = layer.open({
        title: '离职',
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: '/admin/Employee/employeeQuit?eid='+eid,
        end: function () {//无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。layer.open关闭事件
            location.reload();　　//layer.open关闭刷新
        }
    });
}

