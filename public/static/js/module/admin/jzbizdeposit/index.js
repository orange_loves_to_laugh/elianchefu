layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    //表格数据
    let render_data={
        elem: '#myTable',
        url: '/admin/JzBizDeposit/Index?action=ajax',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [ 'exports', 'print', ],
        cols:[[

            {field: 'title_info', title: '商家名称'},
            {field: 'username', title: '提现人名称'},
            {field: 'phone', title: '提现人手机号'},
            {field: 'type_title', title: '提现来源'},
            {field: 'launch_cate_title', title: '提现类型'},

            {field: 'type_status', title: '提现人状态'},
            {field: 'total_area', title: '门店所属地区'},
            {field: 'price', title: '提现金额'},
            {field: 'total_account', title: '收款账号'},
            {field: 'create_time', title: '提现时间'},
            {field: 'deposit_status_title', title: '提现操作'},

            {title: '操作', toolbar: '#currentTableBar',width:400}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }

    table.render(render_data);

    //return

    $("select[name='read_status']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("input[name='phone']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'detail') {
            if(data.source_type == 3){
                var index = layer.open({
                    title: '门店详情',
                    type: 2,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '/admin/JzMerchants/SaveData?status=1&id='+data.biz_id,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            }else{
                var index = layer.open({
                    title: '门店详情',
                    type: 2,
                    shade: 0.2,
                    maxmin: true,
                    shadeClose: true,
                    area: ['100%', '100%'],
                    content: '/admin/biz/Bizwrite?biz_type='+data.launch_cate+'&id='+data.biz_id,
                });
                $(window).on("resize", function () {
                    layer.full(index);
                });
            }



            return false;

        }

        if (obj.event === 'deposit') {
            let button_now=$(this)
            let button_html= '<button type="button" class="btn btn-success" lay-event="action" style="margin-right: 5px">成功</button>'+
                '<button type="button" class="btn btn-danger" lay-event="action">失败</button>'

            layer.confirm('确定提现么', function (index) {
                window.location='/admin/JzBizDeposit/ActionExcel?id='+data.id
                button_now.after(button_html)
                button_now.css('display','none')
                layer.close(index)
            });
        }


        if (obj.event === 'action') {
            let button_now=$(this)
            let msg='确定操作成功么'
            let status=3

            if (button_now.attr('class').indexOf('btn-danger')>0)
            {
                msg='确定操作失败么'
                status=4
            }

            layer.confirm(msg, function (index) {

                $.ajax({
                    url: '/admin/JzBizDeposit/ActionDeposit',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                        deposit_status:status,
                        source_type:obj.data.source_type,
                        launch_cate:obj.data.launch_cate,

                    },
                    success: function(data)
                    {
                        if (data.status) {
                            layer.msg('操作成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()
                            // parent.location.reload()
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
        if (obj.event === 'delete') {

            layer.confirm('确定删除么', function (index) {

                $.ajax({
                    url: '/admin/JzBizDeposit/DelDeposit',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

    });


    table.on('toolbar(currentTableFilter)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        console.log(checkStatus);
        // var data = checkStatus.data;
        // layer.alert(JSON.stringify(data));
        //
    });
});
