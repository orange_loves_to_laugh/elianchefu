$(function() {
    //月度费用统计
    month_expence()
    $('#month_form').on('change','select[name="month"]',function () {
        month_expence()
    })
    $('#month_form').on('change','select[name="year"]',function () {
        month_expence()
    })

    //年度支出统计
    year_expence()
    $('#year_form').on('change','select[name="year"]',function () {

        year_expence()
    })
});

function month_expence(){
    var  month=$('#month_form select[name="month"]').val()
    var  year_1=$('#month_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ExpensesStatisticsMonth', {month:month,year:year_1}, function(data){
        $('#month').html(data);
    });
}

function year_expence(){

    var  year_2=$('#year_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ExpensesStatisticsYear', {year:year_2}, function(data){
        $('#year').html(data);
    });
}

