$(function() {
    //月度支出统计
    month_expence()
    $('#month_form').on('change','select[name="month"]',function () {
        month_expence()
    })
    $('#month_form').on('change','select[name="year"]',function () {
        month_expence()
    })

    //年度支出统计
    year_expence()
    $('#year_form').on('change','select[name="year"]',function () {
        console.log(123);
        year_expence()
    })
});

function month_expence(){
    var  month=$('#month_form select[name="month"]').val()
    var  year_1=$('#month_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ExpenditureStatisticsMonth', {month:month,year:year_1}, function(data){
        $('#month').html(data);
    });
}

function year_expence(){

    var  year_2=$('#year_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ExpenditureStatisticsYear', {year:year_2}, function(data){
        $('#year').html(data);
    });
}

