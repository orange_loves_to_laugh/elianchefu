$(function() {
    //月度利润统计
    month_profit()
    $('#month_form').on('change','select[name="month"]',function () {
        month_profit()
    })
    $('#month_form').on('change','select[name="year"]',function () {
        month_profit()
    })

    //年度支出统计
    year_profit()
    $('#year_form').on('change','select[name="year"]',function () {

        year_profit()
    })
});

function month_profit(){
    var  month=$('#month_form select[name="month"]').val()
    var  year_1=$('#month_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ProfitStatisticsMonth', {month:month,year:year_1}, function(data){
        $('#month').html(data);
    });
}

function year_profit(){

    var  year_2=$('#year_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/ProfitStatisticsYear', {year:year_2}, function(data){
        $('#year').html(data);
    });
}

