$(function() {
     //月收入方式统计
     month_type()
    $('#month_type_form').on('change','select[name="month"]',function () {
        month_type()
    })
    $('#month_type_form').on('change','select[name="year"]',function () {
        month_type()
    })
    //日收入方式统计
    day_type()
    let flatpickr=$("#day_type_form").flatpickr({
        defaultDate: [$("#day_type_form").val()],
        onChange:function (re) {
            day_type()
            flatpickr.close()
        }
    })

    //月收入来源统计
    month_source()
    $('#month_source_form').on('change','select[name="month"]',function () {
        month_source()
    })
    $('#month_source_form').on('change','select[name="year"]',function () {
        month_source()
    })
    //日收入来源统计
    day_source()
    let flatpickr_source=$("#day_source_form").flatpickr({
        defaultDate: [$("#day_source_form").val()],
        onChange:function (re) {
            day_source()
            flatpickr_source.close()
        }
    })
});

function month_type(){
    let month=$('#month_type_form select[name="month"]').val()
    let year=$('#month_type_form select[name="year"]').val()

    $.get('/admin/JzFinancialStatistics/IncomeMonthType', {month:month,year:year}, function(data){
        $('#month_type').html(data);
    });
}
function day_type(){
   var  time=$('#day_type_form').val()

    var year=time.split(' ')[0].split('-')[0]
    var month=time.split(' ')[0].split('-')[1]
    var day=time.split(' ')[0].split('-')[2]

    $.get('/admin/JzFinancialStatistics/IncomeDayType', {month:month,year:year,day:day}, function(data){
        $('#day_type').html(data);
    });
}
function month_source(){
    let month=$('#month_source_form select[name="month"]').val()
    let year=$('#month_source_form select[name="year"]').val()
    $.get('/admin/JzFinancialStatistics/IncomeSource', {month:month,year:year}, function(data){

        $('#month_source').html(data);
    });
}
function day_source(){
    var  time=$('#day_source_form').val()
    var year=time.split(' ')[0].split('-')[0]
    var month=time.split(' ')[0].split('-')[1]
    var day=time.split(' ')[0].split('-')[2]
    $.get('/admin/JzFinancialStatistics/IncomeDaySource',{month:month,year:year,day:day}, function(data){
        console.log(data);
        $('#day_source').html(data);
    });
}




