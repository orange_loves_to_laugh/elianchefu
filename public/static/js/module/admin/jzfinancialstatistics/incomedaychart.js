var switchStatic = 1;
var start_time = '';
var end_time = '';
$(function() {

    $("select[name='switchStatic']").on('change',function (re) {
        switchStatic = $(this).val();
        if($(this).val()!=5){
            dataInit();
            $(".timeSearch").css("display","none")
        }else{
            $(".timeSearch").css("display","flex")
        }
    })
    let flatpickr_register_time=$("#start_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (data){
            start_time = $("#start_time").val()
            flatpickr_register_time.close()
        }
    })
    let flatpickr_member_create=$("#end_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (){
           end_time = $("#end_time").val()
            dataInit();
            flatpickr_member_create.close()
        }
    })
    dataInit();
});
function dataInit(){
    incomeChart();
}

function incomeChart()
{
    let chartDom = document.getElementById('incomeChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/JzFinancialStatistics/IncomeChart?switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {

                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'           // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params[0].data.title} </br> 
                            数值：${params[0].data.value} </br> 
                            占比：${params[0].data.ratio}% </br> `;
                    },
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: '60%',
                        //data: [10, 52, 200, 334, 390, 330, 220],
                        data: data.data,
                    },
                ]
            }
            );
        myChart.getZr().off('click');
        myChart.getZr().on('click', function(param) {
            // 获取 点击的 触发点像素坐标
            const pointInPixel = [param.offsetX, param.offsetY]
            // 判断给定的点是否在指定的坐标系或者系列上
            if (myChart.containPixel('grid', pointInPixel)) {
                // 获取到点击的 x轴 下标  转换为逻辑坐标
                let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
                // 做一些其他事情
                clickFun(data.data[xIndex])
            }
        })
    })
    function clickFun(param) {
        console.log("dkljdklsjf")
        var index = layer.open({
            title: '查看详情',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzFinancialStatistics/IncomeDetail?income_type='+param.income_type+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
        return false;
    }
    option && myChart.setOption(option);
}



