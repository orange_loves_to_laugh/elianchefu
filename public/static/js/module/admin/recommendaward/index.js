layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RecommendAward/Index?action=ajax',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[

            {field: 'title', title: '标题'},
            {field: 'award_max', title: '每日最高收益'},
            {field: 'award_once', title: '单次推荐收益'},
            {field: 'start_time', title: '开始时间'},
            {field: 'end_time', title: '结束时间'},
            {field: 'open_status', title: '状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        page: false,
        skin: 'line'
    });
    form.on('switch(switchTest)', function (data) {

        let switch_value;
        let title;
        let _this=this;
        if(this.checked){
            switch_value=1;
            title="是否开启开关?";
        }else{
            switch_value=2;
            title="是否关闭开关?";
        }
        layer.confirm(title, function (index) {
            layer.close(index);
            changeSwitch(switch_value,"recommendaward");
        },function(){
            _this.checked=! _this.checked;
            form.render("checkbox");
        });
    });
    /**
     * @context 改变开关状态
     */
    function changeSwitch(switch_value,mark){
        $.ajax({
            url: "/admin/market/changeSwitch",
            type: 'post',
            async: true,
            data: {
                value:switch_value,
                mark:mark
            },
            dataType: 'json',
            success: function (res) {
                return true;
            }
        });
    }

    /*
          添加事件
          */
    $('.layui-table-tool-temp').on('click','[data-action="log"]',function(re){
        let index = layer.open({
            title: '参与记录',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RacGame/RacLog'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    $('.layui-table-tool-temp').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '奖励创建',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RecommendAward/saveData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    $('.layui-table-tool-temp').on('click','[data-action="protoctl"]',function(re){
        let index = layer.open({
            title: '奖励规则',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/applay/saveprotocol?mark=bizprotocol'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '奖品设置',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/RacGame/SaveData',
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }else if(obj.event ==='delete'){
            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/RecommendAward/DelAward',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });
            });
        }
    });



});