layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    //表格数据
    let render_data={
        elem: '#myTable',
        url: '/admin/JzBizApply/PartnerIndex?action=ajax',
        method: 'post',
        defaultToolbar: [],
        cols:[[
            {field: 'member_title', title: '申请人姓名'},
            {field: 'phone', title: '手机号'},
            {field: 'member_address', title: '申请地区'},
            {field: 'member_level', title: '会员等级'},
            {field: 'recommender_title', title: '推荐人'},
            {field: 'recommender_phone', title: '推荐人手机'},
            {field: 'partner_price', title: '缴纳费用'},
            {field: 'remarks', title: '备注'},
            // {field: 'read_status_title', title: '读取状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }

    table.render(render_data);

    //return

    $("select[name='read_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='time_range']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("input[name='phone']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzBizApply/FranchiserDetail?id='+data.id,
                content: '/admin/Member/detail?id='+data.member_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }




    });



});