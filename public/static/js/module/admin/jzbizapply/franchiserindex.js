var audit_merchants_id = 0;
var merchants_obj='';
layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;


    let biz_type=$.getQueryVariable('biz_type')
    //标题
    let card_title='商家申请'
    //表格数据
    let render_data={
        elem: '#myTable',
        url: '/admin/JzBizApply/FranchiserIndex?action=ajax&biz_type='+biz_type,
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'title', title: '门店名称'},
            {field: 'address_area', title: '所属地区'},
            {field: 'username', title: '门店联系人'},
            {field: 'phone', title: '联系电话'},
            {field: 'address', title: '详细地址'},
            {field: 'relation_agent', title: '关联代理'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 360}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    }
    if (biz_type=='1')
    {
        render_data.cols=[[
            {field: 'username', title: '申请人姓名'},
            {field: 'phone', title: '手机号'},
            {field: 'address_title', title: '申请地区'},
            {field: 'biz_title', title: '门店名称'},
            {field: 'industry_cate_title', title: '所属行业分类'},
            {field: 'remarks', title: '备注'},
            {field: 'read_status_title', title: '读取状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]]
    }
    if (biz_type=='2')
    {
        card_title='加盟店申请'

        render_data.cols[0][1]={field: 'phone', title: '手机号',templet:'#memberDetail'}
    }
    if (biz_type=='3')
    {
        card_title='合作店申请'
    }
    if (biz_type=='4')
    {
        card_title='代理商申请'
    }
    $('.card-title').html(card_title)
    $('input[name="biz_type"]').val(biz_type)
    table.render(render_data);

    //return

    $("select[name='read_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='time_range']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("input[name='phone']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        $(this).parents('td').prev().find('div').html('已读')
        // console.log($(this).parents('td').prev().find('div').html('已读'));
        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzBizApply/FranchiserDetail?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;

        }
        if (obj.event === 'audit') {
            window.location='/admin/JzBizApply/ActionBizExcel?id='+data.biz_id
            let button_html="<a class=\"layui-btn layui-btn-normal  data-count-edit audit_btn\"  lay-event=\"access\">通过</a>\n" +
                "      <a class=\"layui-btn btn-danger    audit_btn \"  lay-event=\"noaccess\">驳回</a>"
            $(this).after(button_html)
            $(this).css("display","none")
            return false;

        }
        if(obj.event=='access'){
            console.log(data.id)
            /*这里是审核表的id*/
            audit_merchants_id = data.id
            merchants_obj = $(this)
            let datas={param:{
                    keywords:'',
                    // notin_merchants:'true',
                }}
            $.ajax({
                url:'/admin/Employee/RelateEmployee',
                type: 'post',
                data:datas,
                async: true,
                dataType: 'json',
                success: function (res) {
                    var index = layer.open({
                        type: 1,
                        title:'请选择需要关联的选项',
                        area: ['800px', '600px'],
                        shade: 0.2,
                        skin: 'layui-layer-custom', //加上边框
                        maxmin: true,
                        shadeClose: true,
                        content: res.data
                    });



                    $(window).on("resize", function () {
                        layer.full(index);
                    });
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                            +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
        }
        if(obj.event=='noaccess'){
            let obj = $(this)
            $.ajax({
                url:'/admin/JzBizApply/auditFail',
                type: 'post',
                data: {
                    id:data.id,
                },
                async: true,
                dataType: 'json',
                success: function (res) {
                    obj.parents('tr').remove()
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                            +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
        }
        //member_detail
        if (obj.event === 'member_detail') {
            var index_member_detail = layer.open({
                title: '用户详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],

                content: '/admin/Member/detail?id='+data.member_id,
            });
            $(window).on("resize", function () {
                layer.full(index_member_detail);
            });
            return false;

        }

        if (obj.event === 'delete') {


            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzBizApply/FranchiserDel',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

        if (obj.event === 'editremark') {
            delete data.address_title
            delete data.plan_area_title
            delete data.read_status_title
            delete data.industry_cate_title
            Swal.fire({
                width:'40%',
                input: 'textarea',
                inputValue: data.remarks,
                showCancelButton: true,
                background: 'rgba(0, 0, 0, 0.96)',
                confirmButtonText:
                    '<i class="fa fa-thumbs-up"></i>确定',
                cancelButtonText:
                    '<i class="fa fa-thumbs-down"></i>取消',
                preConfirm: (login) => {
                    data.remarks=login
                    return fetch('/admin/JzBizApply/FranchiserSave?action=ajax',{
                        method:"POST",
                        body:JSON.stringify(data),
                        headers: {
                            'Content-Type': 'application/json'
                        },

                    })
                        .then(response => response.text())
                        .then(res=>{
                            let re=JSON.parse(res)

                            if (!re.status)
                            {
                                Swal.fire({
                                    type: 'error',
                                    title: '异常信息',
                                    background: 'rgba(0, 0, 0, 0.96)',
                                    text: re.errorCode+':'+re.msg,
                                })
                                return false
                            }
                            const Toast = Swal.mixin({
                                toast: true,
                                showConfirmButton: false,
                                timer:1500
                            })
                            Toast.fire({
                                type: 'success',
                                title: '保存成功',

                            }).then((result) => {
                                location.reload();
                            })

                            return false

                        })
                        .catch(error => {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        })

                },
            })

            return
            layer.prompt({
                formType: 2,
                value: data.remarks,
                title: '修改备注',
                area: ['350px', '350px'] //自定义文本域宽高
            }, function(value, index, elem){
                data.remarks=value

                delete data.address_title
                delete data.plan_area_title
                delete data.read_status_title

               let re= $.ajax({
                    url: '/admin/JzBizApply/FranchiserSave?action=ajax',
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function(res) {
                        if (res.status == true) {
                            layer.msg('保存成功', {icon: 1, time: 1000},
                                function(){
                                    parent.location.reload();
                                })

                        }  else {
                            layer.open({
                                title: '错误信息',
                                content: '<span style="color:red">'+res.code+':</span>'+res.msg
                            })
                            return false
                        }
                    },
                    error: function(re) {

                        layer.open({
                            title: '错误信息',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });
                layer.close(index);
            });

        }
    });

    $(".btn-primary").on('click',function (re) {
        console.log(biz_type);
        var index = layer.open({
            title: '页面编辑',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzBizApply/ApplyPageDetail?biz_type='+biz_type,
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
        return false;
    })

});

/*
关联员工 并提交审核通过
 */
function connect_detail_callback(obj){
    let id = $(obj).attr('data-id')
    $.ajax({
        url:'/admin/JzBizApply/auditSuccess',
        type: 'post',
        data:{
            employee_id :id,
            apply_id:audit_merchants_id,
        },
        async: true,
        dataType: 'json',
        success: function (res) {
            merchants_obj.parents('tr').remove()
            return false;
        },
        error: function(re) {

            layer.open({
                title: '操作失败',
                content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                    +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
            })
        }
    });

    layer.closeAll();
}