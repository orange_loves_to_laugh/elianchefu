layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form


    $('.content__title h1').html('商家申请详情')

    if ($('input[name="biz_type"]').val() == '2')
    {
        $('.content__title h1').html('加盟店申请详情')
    }

    if ($('input[name="biz_type"]').val() == '3')
    {
        $('.content__title h1').html('合作店申请详情')
    }
    if ($('input[name="biz_type"]').val() == '4')
    {
        $('.content__title h1').html('代理商申请详情')
    }


    //监听提交
    form.on('submit(saveBtn)', function (data) {
        const Toast = Swal.mixin({
            toast: true,
            showConfirmButton: false,
            timer: 3000
        })
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/JzBizApply/FranchiserSave?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {

                    Toast.fire({
                        type: 'success',
                        title: '保存成功',

                    }).then((result) => {
                        parent.location.reload();
                    })

                }  else {
                    Toast.fire({
                        type: 'error',
                        title: '<span style="color:red">'+res.code+':</span>'+res.msg,
                    }).then((result) => {
                        parent.location.reload();
                    })
                    return false
                }
            },
            error: function(re) {

                Toast.fire({
                    type: 'error',
                    title: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                }).then((result) => {
                    parent.location.reload();
                })
            }
        });

    });

});

