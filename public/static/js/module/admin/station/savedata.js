layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,$ = layui.$;


    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Station/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){

                            parent.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });





    });


    // 监听部门联动
    $("select[name='department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }

                console.log(res);

                let option = '<option value="">请选择二级部门</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='department_secid']").html('')
                $("select[name='department_secid']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })
});




