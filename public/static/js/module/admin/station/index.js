layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table,
        form=layui.form;

    table.render({
        elem: '#myTable',
        url: '/admin/Station/Index?action=ajax',
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'title', title: '岗位名称'},
            {field: 'department_title', title: '所属部门'},
            {field: 'share_profit_month', title: '月度均摊收益'},
            {field: 'share_profit_season', title: '季度均摊收益'},
            {field: 'share_profit_year', title: '年度均摊收益'},
            {field: 'is_check_title', title: '是否参与考核'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });



    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑岗位',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Station/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/Station/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });


    // 监听一级部门联动
    $("select[name='department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='department_secid']").html('')
                $("select[name='department_secid']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='id']").html('')
                $("select[name='id']").html(option_station)

                // ******************************************

                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })


    //监听二级部门联动
    $("select[name='department_secid']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                    department_secid:re.currentTarget.value,
                    department_id:$('select[name="department_id"]') .val()
                }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }



                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='id']").html('')
                $("select[name='id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    /*
   添加事件
    */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){


        let index = layer.open({
            title: '添加岗位',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Station/SaveData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });


    })
    /*
    搜索事件
     */
    $("select[name='department_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='department_secid']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='id']").on('change',function (re) {
        $.submitInfo(table);
    })
});