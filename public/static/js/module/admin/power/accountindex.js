layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form=layui.form,
        table = layui.table;


    table.render({
        elem: '#myTable',
        url: '/admin/Power/AccountIndex?action=ajax',
        method: 'post',
        parseData:function(res){

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'username', title: '账号'},
            {field: 'employee_name', title: '员工姓名'},
            {field: 'employee_station_title', title: '岗位信息'},
            {field: 'role_title', title: '角色'},
            {title: '操作', toolbar: '#currentTableBar'}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });



    /*
添加事件
 */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re) {
        let index = layer.open({
            title: '添加权限',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Power/SaveAccount'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });

    })

    /*
    编辑事件
     */
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit')
        {
            var index = layer.open({
                title: '编辑权限信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Power/SaveAccount?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }




    });



    /*
    搜索事件
     */
    $("select[name='level']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='type']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
});

