layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
        ,$ = layui.$;
    let id=$("#id").val()


    //初始化上级模块和权限级别
    //initcallinfo()

    /*
    上级模块和权限级别展示判断
*/
    $("select[name='type']").on('change',function (re) {

        if ($(this).val() == 1) {//菜单
            $("select[name='level']").parents('.card').css('display','')
            $("select[name='pid']").parents('.card').css('display','')
        }
        if ($(this).val() ==2) {
            $("select[name='level']").parents('.card').css('display','none')
            //$("select[name='pid']").parents('.card').css('display','none')
        }
    })

    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Power/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });

    });


    //获取上级模块
    $("select[name='level']").on('change',function (re) {

        let level=$(this).val()
        let pid_level=0;
        // if (level > 1)
        // {}
            pid_level=level-1

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Power/DataList",
                data:{param:{type:1,level:pid_level,url:''}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染上级模块
                let option_department = '<option value="">请选择上级模块</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='pid']").html('')
                $("select[name='pid']").html(option_department)


                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })


    /*
 初始化外呼平台信息是否显示
  */
    function initcallinfo(){
        let type=$('select[name="type"]').val()
        console.log(type);

        //表单显示状态
        if ( type == 1)
        {
            $("select[name='level']").parents('.card').css('display','')
            $("select[name='pid']").parents('.card').css('display','')
        }
        if ( type == 2)
        {
            $("select[name='level']").parents('.card').css('display','none')

            $("select[name='pid']").parents('.card').css('display','none')
        }

        if ( type.length < 1)
        {
            $("select[name='level']").parents('.card').css('display','none')
            $("select[name='pid']").parents('.card').css('display','none')
        }
    }
});




