layui.use(['layer', 'form','laydate','upload'], function(){
    let layer = layui.layer
        ,form = layui.form;

    //初始化权限列表是否显示
    initcheckbox()
    //权限开启按钮事件
    $(".power_status").on('click','input',function(){



        if ($(this).val()==1) {//开启
            $(this).parents('tr').find('td:eq(2)').find('.power_idarr').css('display','')
        }else{
            $(this).parents('tr').find('td:eq(2)').find('.power_idarr').css('display','none')
        }
    })


    //执行创建服务奖励
    form.on('submit(saveBtn)', function (data) {
        $.ajax({
            url: '/admin/Power/DoSaveAccount?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){

                            parent.location.reload();

                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
    })

    // 监听一级部门联动
    $("select[name='employee_department_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Department/DataList?higher_department_id=",
                data:{param:{higher_department_id:re.currentTarget.value}},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {


                //渲染二级部门
                let option_department = '<option value="">请选择二级部门</option>'
                let data_department=res.data
                for(let i =0; i in data_department;i++){
                    option_department += '<option value="'+data_department[i].id+'">'+data_department[i].title+'</option>'
                }

                $("select[name='employee_department_secid']").html('')
                $("select[name='employee_department_secid']").html(option_department)

                //渲染岗位
                let option_station = '<option value="">请选择岗位</option>'
                let data_station=res.data_station
                for(let i =0; i in data_station;i++){
                    option_station += '<option value="'+data_station[i].id+'">'+data_station[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option_station)


                // //渲染员工
                // let option_employee = '<option value="">请选择员工</option>'
                // let data_employee=res.data_station
                // for(let i =0; i in data_employee;i++){
                //     option_employee += '<option value="'+data_employee[i].id+'">'+data_employee[i].title+'</option>'
                // }
                //
                // $("select[name='employee_id']").html('')
                // $("select[name='employee_id']").html(option_station)

                form.render();
            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    //监听二级部门联动
    $("select[name='employee_department_secid']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Station/DataList",
                data:{param:{
                        department_secid:re.currentTarget.value,
                        department_id:$('select[name="employee_department_id"]') .val()
                    }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {
                //渲染岗位
                let option = '<option value="">请选择岗位</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    //监听岗位
    $("select[name='employee_station_id']").on('change',function (re) {

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/Employee/SearchEmployee",
                data:{param:{
                        employee_station_id:re.currentTarget.value,
                    }},
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                //渲染员工
                let option = '<option value="">请选择员工</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].employee_name+'</option>'
                }

                $("select[name='employee_id']").html('')
                $("select[name='employee_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })

    /**
     * [ArrayTurnJson js数组转json]
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  0.0.1
     * @datetime 2021年2月6日13:04:40

     */
    function initcheckbox(){
        if ($('input[name="id"]').val() > 0) {

            $.each($('.power_status'),function (key,val) {

                if ($(val).find('input:checked').val() == 1) {
                    $(val).parents('tr').find('.power_idarr').css('display','')
                }else{
                    $(val).parents('tr').find('.power_idarr').css('display','none')
                }

            })

        }else{
            $('.power_idarr').css('display','none')
        }



    }
});