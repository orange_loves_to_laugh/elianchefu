layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;


    let time=$.getQueryVariable('time')
    //标题
    let card_title='今日余额使用信息'
    //表格数据
    let render_data={
        elem: '#myTable',
        url: '/admin/JzIncomeInfor/UseBalanceIndex?action=ajax&time='+time,
        method: 'post',
        parseData: function (res) { //res 即为原始返回的数据
            console.log(res);
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[

            {field: 'member_name', title: '用户名称'},
            {field: 'member_phone', title: '联系电话'},
            // {field: 'biz_title', title: '门店名称',templet:'#biz_detail'},
            // {field: 'biz_address', title: '门店地址'},
            // {field: 'biz_type_title', title: '门店类型'},
            {field: 'account', title: '使用金额',sort:true},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}

        ]],
        //limits: [10, 15, 20, 25, 50, 100],
       // limit:10,
        page: true,
        skin: 'line'
    }

    if (time=='yesterday')
    {
        card_title='昨日余额使用信息'
    }
    $("input[name='time']").val(time)
    $('.card-title').html(card_title)

    table.render(render_data);

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit')
        {
            var index = layer.open({
                title: '用户详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/detail?id='+data.member_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
        if (obj.event === 'biz_detail')
        {
            let url='/admin/Biz/Bizwrite?id='+data.biz_id+'&biz_type='+data.biz_type
            if (data.consump_type.length > 1)
            {
               url='/admin/JzMerchants/SaveData?id='+data.biz_id
            }

            var biz_detail = layer.open({
                title: '门店详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: url,
            });
            $(window).on("resize", function () {
                layer.full(biz_detail);
            });
            return false;



        }




    });

    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })


});