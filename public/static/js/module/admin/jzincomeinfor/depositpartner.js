layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    let flatpickr=$("#create_time").flatpickr({
        enableTime: true,
        defaultDate: [$("#create_time").attr('data-value')],
        onChange:function (re) {
            console.log($("#create_time").val());
            $.submitInfo(table);
            flatpickr.close()
        }
    })
    let url='/admin/JzIncomeInfor/DepositPartner?action=ajax'

    if ($.getQueryVariable('create_time').length > 0)
    {
        url= '/admin/JzIncomeInfor/DepositPartner?action=ajax&create_time=today'
    }
    table.render({
        elem: '#myTable',
        url: url,
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        parseData:function(data){
            $('#total_price').html('筛选总金额:'+data.price);
        },
        cols: [[
            {field: 'username', title: '申请人'},
            {field: 'phone', title: '手机号'},
            {field: 'price', title: '提现金额'},
            {field: 'create_time', title: '提现时间'},
            {field: 'deposit_type_title', title: '收款类型'},
            {field: 'deposit_status_title', title: '提现状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    //提现状态 时间 关键字 金额
    $("select[name='deposit_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {

        $.submitInfo(table);
    })
    $("input[name='price']").on('blur',function (re) {

        $.submitInfo(table);
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '合伙人信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/detail?id='+data.member_id,

            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }


    });



});
