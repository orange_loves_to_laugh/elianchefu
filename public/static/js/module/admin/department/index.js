layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        form=layui.form,
        table = layui.table;


    table.render({
        elem: '#myTable',
        url: '/admin/Department/Index?action=ajax',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'title', title: '部门名称'},
            {field: 'manager_employee_title', title: '负责人'},
            {field: 'higher_department_title', title: '上级部门'},
            {field: 'share_profit_month', title: '月度均摊收益'},
            {field: 'share_profit_season', title: '季度均摊收益'},
            {field: 'share_profit_year', title: '年度均摊收益'},
            {field: 'remark', title: '部门描述'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });

    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加部门',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Department/SaveData'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });

        }

    });

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑部门',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Department/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/Department/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });


});

/*
修改负责人
 */
function changemanager(obj){

    let p=new Promise(function (resolve, reject) {
       $.ajax({
           url: '/admin/Department/changemanager',
           type: 'post',
           dataType: 'json',
           success: function(res) {
               resolve(res)
           },
           error: function(re) {
             reject(re)
           }
       });
   })

    p.then(
        function (res){
            var html=''
            let select=''
            for (let dataKey in res.data)
            {
                if (obj.manager_employee_id== res.data[dataKey].id)
                {
                    html+='<option selected="selected" value="'+res.data[dataKey].id+'">'+res.data[dataKey].employee_name+'</option>'
                }else{
                    html+='<option '+select+' value="'+res.data[dataKey].id+'">'+res.data[dataKey].employee_name+'</option>'
                }
            }
            html+'</select>'

            //
            layer.open({
                // type: 1,
                title: '在线调试',
                content: '<select id="selectPointOfInterest" name="manager_employee_id">'+html+'</select>',
                offset: ['10%', '40%'],
                yes: function(index, layero){
                    let manager_employee_id=$('select[name="manager_employee_id"]').val()
                    obj.manager_employee_id=manager_employee_id
                    $.ajax({
                        url: '/admin/Department/Save',
                        type: 'post',
                        dataType: 'json',
                        data:obj,
                        success: function(res) {

                            if (res.status == true) {
                                layer.msg('保存成功', {icon: 1, time: 1000},
                                    function(){
                                        parent.location.reload();
                                    })
                            }
                        },
                        error: function(re) {

                            layer.open({
                                title: '提示信息',
                                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                            })
                        }
                    });
                    layer.close(index);
                }
            });
            let bg_num=$('body').attr('data-sa-theme')
            $(".layui-layer").css({
                backgroundImage: 'url(http://192.168.1.128:8082/static/img/bg/'+bg_num+'.jpg)',
            });
        },
        function (re){

            layer.open({
                title: '提示信息',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
            })
        }
    )


}