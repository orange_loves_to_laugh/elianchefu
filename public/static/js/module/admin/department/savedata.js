layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,$ = layui.$;

    //初始化显示上级部门
    if ($("select[name='level']").val() == 1)
    {
        $('select[name="higher_department_id"]').parents('.card').css('display','none')
    }
    console.log($("select[name='level']").val());
    //更换级别事件
    $("select[name='level']").on('change',function(){
        let level=$(this).val();
        if (level == 1)
        {
            $('select[name="higher_department_id"]').parents('.card').css('display','none')
            $('select[name="higher_department_id"]').val(0)
        }else{
            $('select[name="higher_department_id"]').parents('.card').css('display','')
        }

    })
    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Department/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                          parent.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });





    });


});




