layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/jzInfor/Index?action=ajax',
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'news_title', title: '标题'},
            {field: 'news_author', title: '发送人'},
            {field: 'create_time', title: '发布时间'},
            {field: 'news_accept_title', title: '接收人'},
            {field: 'news_status_title', title: '状态'},
            {field: 'check_num', title: '查看总人数'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });
    /*
     添加事件
      */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzInfor/SaveData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    /*
    搜索事件
     */
    $("select[name='news_accept']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='news_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzInfor/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzInfor/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });


});