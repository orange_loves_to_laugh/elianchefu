layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RacGame/Index?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[

            {field: 'prize_title', title: '奖品名称'},
            {field: 'prize_val', title: '奖品价值'},
            {field: 'winner_percent', title: '领奖总分'},
            {field: 'game_percent', title: '游戏总分'},
            {field: 'share_percent', title: '平均助力分数'},
            {field: 'repeat_num', title: '重复次数'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        page: false,
        skin: 'line'
    });


    /*
          添加事件
          */
    $('.layui-btn-container').on('click','[data-action="log"]',function(re){
        let index = layer.open({
            title: '参与记录',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RacGame/RacLog'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    $('.layui-btn-container').on('click','[data-action="store"]',function(re){
        let index = layer.open({
            title: '门店列表',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RacGame/racShop'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '奖品设置',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/RacGame/SaveData',
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
    });



});