layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RacGame/racShop?action=ajax',
        method: 'post',
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[

            {field: 'biz_title', title: '门店名称'},
            {field: 'biz_phone', title: '联系方式'},
            {field: 'biz_address', title: '门店地址'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limit:10,
        page: true,
        skin: 'line'
    });


    /*
          添加事件
          */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '关联门店',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RacGame/relationStore',
            cancel:function(){
                location.reload();
            },
            end:function(){
                // parent.location.reload();
            },
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        obj.del();
        if (obj.event === 'delete') {
            $.ajax({
                url:'/admin/RacGame/deleteRelation',
                type:'post',
                data:{
                    biz_id:data.id
                },
                dataType:'json',
                success:function(res){

                }
            })
            return false;

        }
    });



});