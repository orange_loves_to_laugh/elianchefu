layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form


    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/RacGame/saveData?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });

    });

});

