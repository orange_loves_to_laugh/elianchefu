layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RacGame/relationStore?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[
            {field: 'biz_title', title: '门店名称'},
            {field: 'biz_phone', title: '联系方式'},
            {field: 'biz_address', title: '门店地址'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        page: false,
        skin: 'line'
    });
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        obj.del();
        if (obj.event === 'relation') {
           $.ajax({
               url:'/admin/RacGame/startRelation',
               type:'post',
               data:{
                   biz_id:data.id
               },
               dataType:'json',
               success:function(res){

               }
           })
            return false;

        }
    });
    form.on('submit(data-search-btn)', function (data) {
        submitInfo();
        return false;
    });
    function submitInfo() {
        var d = {};
        var t = $('#formInfo [name]').serializeArray();
        $.each(t, function () {
            d[this.name] = this.value;
        });
        var result = JSON.stringify(d);
        //执行搜索重载
        table.reload('myTable', {
            page: {
                curr: 1
            }
            , where: {
                searchParams: result
            }
        }, 'data');
        return false;
    };

});