
layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RacGame/logHelp?action=ajax&member_id='+member_id+"&member_repeat_num="+member_repeat_num,
        method: 'post',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[
            {field: 'friend_name', title: '用户名称'},
            {field: 'help_percent', title: '助力分数'},
            {field: 'create_time', title: '助力时间'},
        ]],
        limit : 10,
        page: true,
        skin: 'line'
    });

});