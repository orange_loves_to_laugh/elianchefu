layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/RacGame/RacLog?action=ajax',
        method: 'post',
        defaultToolbar: [],
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        cols: [[

            {field: 'nickname', title: '用户名称'},
            {field: 'member_phone', title: '手机号'},
            {field: 'percent_now', title: '当前总分'},
            {field: 'member_game_percent', title: '游戏得分'},
            {field: 'help_person', title: '需好友助力总数'},
            {field: 'plurs_person', title: '剩余助力数量'},
            {field: 'rac_status', title: '当前状态',templet:'#statustpl'},
            {field: 'member_repeat_num', title: '重复领取次数'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limit : 10,
        page: true,
        skin: 'line'
    });


    /*
          添加事件
          */
    $('.layui-btn-container').on('click','[data-action="log"]',function(re){
        let index = layer.open({
            title: '参与记录',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/RacGame/RacLog'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'check') {
            var index = layer.open({
                title: '查看助力',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/RacGame/logHelp?member_id='+data.member_id+"&member_repeat_num="+data.member_repeat_num,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
    });
    form.on('submit(data-search-btn)', function (data) {
        submitInfo();
        return false;
    });
    $("#selectInfo").on('change',function (re) {
        console.log(re.currentTarget.value)
       // $('#formbutton').click();
        submitInfo();
        return false;
    });
    function submitInfo() {
        var d = {};
        var t = $('#formInfo [name]').serializeArray();
        $.each(t, function () {
            d[this.name] = this.value;
        });
        var result = JSON.stringify(d);
        //执行搜索重载
        table.reload('myTable', {
            page: {
                curr: 1
            }
            , where: {
                searchParams: result
            }
        }, 'data');
        return false;
    };


});