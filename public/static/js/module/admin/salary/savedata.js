layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,$ = layui.$;



    let time=$("#form1 table tbody tr td").eq(1).find('input').val().split('-')

    $(".content__title h1").html( time[0]+'年'+time[1]+'月薪资表')
    //监听提交
    form.on('submit(saveBtn)', function (data) {

        $.ajax({
            url: '/admin/Salary/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload()
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });

    });

    function DateAdd(interval, number, date) {
        switch (interval) {
            case "y ": {
                date.setFullYear(date.getFullYear() + number);
                return date;
                break;
            }

            case "w ": {
                date.setDate(date.getDate() + number * 7);
                return date;
                break;
            }
            case "d ": {
                date.setDate(date.getDate() + number);
                return date;
                break;
            }
            case "h ": {
                date.setHours(date.getHours() + number);
                return date;
                break;
            }
            case "m ": {
                date.setMinutes(date.getMinutes() + number);
                return date;
                break;
            }
            case "s ": {
                date.setSeconds(date.getSeconds() + number);
                return date;
                break;
            }
            default: {
                date.setDate(d.getDate() + number);
                return date;
                break;
            }
        }
    }



    /**
     * [calcfullattendancesalary 计算满勤工资]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月6日23:18:55
     * @param    {[object]}    obj [jquery兑现]
     * @return   {[float]}        		fullattendancesalary[满勤工资]
     */
    function calcfullattendancesalary(obj,mark='') {
        //满勤工资
        let fullattendance_salary=parseFloat(obj.find('.fullattendance_salary').attr('data-oldvalue'))
if(mark != 'full') {
    //入职时间判断
    //统计工资所在的时间
    let salary_time = $(".content__title").attr('data-title')

    //入职时间
    let employee_create = obj.find('.employee_create').html()

    //实际出勤天数
    let attendance_days = parseFloat(obj.find('.attendance_days').html())

    //应出勤天数
    let fullattendance_days = parseFloat(obj.find('.fullattendance_days').html())

    let condition = obj.find('.employee_create').attr('data-in-salary-time') == 1
        && salary_time.substr(0, 7) == employee_create.substr(0, 7)


    //入职时间在上月
    if (condition) {

        //大于应出勤天数一半
        if (attendance_days >= fullattendance_days / 2 && attendance_days < fullattendance_days) {
            fullattendance_salary = Math.round(fullattendance_salary / 2)
        }
        if (attendance_days < fullattendance_days / 2) {
            fullattendance_salary = 0

        }
        if (attendance_days >= fullattendance_days) {
            fullattendance_salary = parseFloat(obj.find('.fullattendance_salary').attr('data-oldvalue'))
        }
        //大于应出勤天数：全给

    }
    else {

        employee_create = new Date(employee_create)
        salary_time = new Date(salary_time)
        //入职在本月

        if (employee_create > salary_time) {
            fullattendance_salary = 0
        } else {

            if (attendance_days != fullattendance_days) {
                fullattendance_salary = 0
            }
        }
    }

    obj.find('.fullattendance_salary').html(fullattendance_salary)
    obj.find('.fullattendance_salary').next('input[data-input="fullattendance_salary"]').val(fullattendance_salary)
}else{
     fullattendance_salary = parseFloat(obj.find('.fullattendance_salary').next('input[data-input="fullattendance_salary"]').val())
}
        return parseFloat(fullattendance_salary);
    }

    /**
     * [calcactionsalary 计算应发工资：基本工资+岗位工资/应出勤天数*实际出勤天数]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月6日23:18:55
     * @param    {[object]}    obj [jquery兑现]
     * @return   {[object]}        		obj[josn对象]
     */
    function calcactionsalary(obj){

        //应发工资
        let action_salary=0.00
        //基本薪资
        let base_salary=parseFloat(obj.find('.base_salary').html())

        //岗位薪资
        let station_salary=parseFloat(obj.find('.station_salary').html())

        //实际出勤天数
        let attendance_days=parseFloat(obj.find('.attendance_days').html())

        //应出勤天数
        let fullattendance_days=parseFloat(obj.find('.fullattendance_days').html())

        //应发工资:基本工资+岗位工资/应出勤天数*实际出勤天数


        action_salary= parseFloat((parseFloat(station_salary)+parseFloat(base_salary))/parseFloat(fullattendance_days)*parseFloat(attendance_days),2).toFixed(2)

        obj.find('.action_salary').html(action_salary)
        obj.find('.action_salary').next('input[data-input="action_salary"]').val(action_salary)

        return parseFloat(action_salary);

    }
    /**
     * 计算实际发放工资:应发工资+满勤工资+绩效工资+提成+平台分红-系统处罚-公司处罚
     * @desc     description
     * @param    {{}}                 obj [当前数据所在行]
     */
    function calctotalprice(obj,mark=''){
        //满勤工资
        let fullattendance_salary= calcfullattendancesalary(obj,mark)
;
        //应发工资
        let action_salary= calcactionsalary(obj)

        //绩效工资
        let performance_salary= parseFloat(obj.find('.performance_salary').html())
        //提成
        let percentage= parseFloat(obj.find('.percentage').html())
        //分红
        let bonus= parseFloat(obj.find('.bonus').html())
        //系统处罚
        let system_punish= parseFloat(obj.find('.system_punish').html())
        //公司处罚
        let company_punish= parseFloat(obj.find('.company_punish').html())
        //实际发放工资
        let actually_salary=action_salary+fullattendance_salary+performance_salary+percentage+bonus-system_punish-company_punish

        obj.find('.actually_salary').html(actually_salary.toFixed(2))
        obj.find('.actually_salary').next('input[data-input="actually_salary"]').val(actually_salary.toFixed(2))


    }
    /*
       修改实际出勤天数：应发工资 满勤工资计算
     */

    $('table tbody').on('dblclick','td[data-title="attendance_days"]',function (rew){

        let that=$(this)
        layer.prompt({
            formType: 0,
            value: that.find('span').html(),
            title: '请输入实际出勤天数',

        }, function(value, index, elem){

            that.find('span').html(value)
            that.find('input').val(value)
            //应发工资:基本工资+岗位工资/应出勤天数*实际出勤天数
             //满勤工资
            //let fullattendance_salary=calcfullattendancesalary(that.parents('tr'))

            //应发工资
           // let action_salary=calcactionsalary(that.parents('tr'))
            /*
            //应出勤天数
            let fullattendance_days=parseInt(that.parents('tr').find('.fullattendance_days').html())
            //实际出勤天数
            let attendance_days= parseInt(value)
            */

            /*
            if (attendance_days <= fullattendance_days / 2)
            {
                fullattendance_salary=0
            }

            if (attendance_days >= fullattendance_days/2&&attendance_days<fullattendance_days)
            {

                fullattendance_salary=Math.round(fullattendance_salary/2)
            }
            that.parents('tr').find('.fullattendance_salary').html(fullattendance_salary)
            that.parents('tr').find('.fullattendance_salary').next('input[data-input="fullattendance_salary"]').val(fullattendance_salary)

            let base_salary=parseFloat(that.parents('tr').find('.base_salary').html())
            let station_salary=parseFloat(that.parents('tr').find('.station_salary').html())
            let action_salary= parseFloat((station_salary+base_salary)/fullattendance_days*attendance_days,2).toFixed(2)
            */

            //计算实际发放工资
            calctotalprice(that.parents('tr'))
            layer.close(index);
        });
    })

    /*
    除实际出勤天数 之外的修改
     */
    $('table tbody').on('dblclick','td',function (rew){

        let that=$(this)
        let title=that.attr('data-title')
        //实际出勤天数不在这里修改
        if (title == 'attendance_days')
        {
            return
        }

        //文字部分不能修改
        //|| title == 'fullattendance_salary'
        if (typeof title == 'undefined'||title=='action_salary'||title=='actually_salary'||title=='employee_create' )
        {
            return
        }

        layer.prompt({
            formType: 0,
            value: that.find('span').html(),
            title: '请输入'+that.parents('tr').find('.'+title).attr('data-title'),

        }, function(value, index, elem){
            that.parents('tr').find('.'+title).html(value)
            that.parents('tr').find('.'+title).next('input[data-input="'+title+'"]').val(value)
            calctotalprice(that.parents('tr'),'full')
            layer.close(index);
        });
    })




    /*
    部门 岗位联动
     */
    $('select[name="employee_department_id"]').on('change',function (re) {

        $.ajax({
            url: '/admin/Station/DataList?department_id='+$(this).val(),
            type: 'post',
            dataType: 'json',

            success: function(res) {

                let option = '<option value="">请选择所属岗位</option>'

                for(let i =0; i in res.data;i++){

                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='employee_station_id']").html('')
                $("select[name='employee_station_id']").html(option)
                form.render();
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });


    })

    /*
     岗位 职工联动
     */
    $('select[name="employee_station_id"]').on('change',function (re) {
        let param={
            employee_station_id: $(this).val(),
            employee_department_id:  $('select[name="employee_department_id"]').val(),
        }

        $.ajax({
            url: '/admin/Employee/SearchEmployee',
            type: 'post',
            dataType: 'json',
            data:{param},
            success: function(res) {

                let option = '<option value="">请选择员工</option>'

                for(let i =0; i in res.data;i++){

                    option += '<option value="'+res.data[i].id+'">'+res.data[i].employee_name+'</option>'
                }
                $("select[name='employee_id']").html('')
                $("select[name='employee_id']").html(option)
                form.render();
            },
            error: function(re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });


    })

    /*
     员工 联动信息
     */
    $('select[name="employee_id"]').on('change',function (re) {
        let param={
            keywords: $(this).text().replace("请选择员工",""),
          }

        $.ajax({
            url: '/admin/Employee/SearchEmployee',
            type: 'post',
            dataType: 'json',
            data:{param},
            success: function(res) {

                let sex_arr=[]
                let option = '<option value="">请选择员工状态</option>'

                for(let i =0; i < 2;i++){

                    option += '<option value="'+res.data[i].id+'">'+res.data[i].employee_name+'</option>'
                }
                $("select[name='employee_status']").html('')
                $("select[name='employee_status']").html(option)
                form.render();
            },
            error: function(re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });


    })
});




