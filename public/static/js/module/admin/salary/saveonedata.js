layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
        ,$ = layui.$;

    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Salary/Save',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            window.location.reload();
                        })

                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });

    });


    $('select[name="department_id"]').on('change',function (re) {

        $.ajax({
            url: '/admin/Station/DataList?department_id='+$(this).val(),
            type: 'post',
            dataType: 'json',

            success: function(res) {
                console.log(res);
                let option = '<option value="">请选择所属岗位</option>'

                for(let i =0; i in res.data;i++){

                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='station_id']").html('')
                $("select[name='station_id']").html(option)
                form.render();
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });


    })
});




