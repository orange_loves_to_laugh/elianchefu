layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    //员工名称  员工状态 基本工资 工位工资 应出勤天数 实际出勤天数 应发工资

    //满勤工资 绩效工资 提成 平台分红 系统处罚 公司处罚
    table.render({
        elem: '#myTable',
        url: '/admin/Salary/Index?action=ajax',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [ 'exports', 'print'],
        cols: [[
            {field: 'employee_name', title: '员工名称'},
            // {field: 'department_title', title: '所属部门'},
            // {field: 'station_title', title: '所属岗位'},
            {field: 'employee_status_title', title: '员工状态'},
            {field: 'base_salary', title: '基本工资'},
            {field: 'station_salary', title: '岗位工资'},
            {field: 'fullattendance_days', title: '应出勤'},
            {field: 'attendance_days', title: '实际出勤'},
            {field: 'fullattendance_salary', title: '满勤工资'},
            {field: 'action_salary', title: '应发工资'},
            {field: 'percentage', title: '提成'},
            {field: 'performance_salary', title: '绩效工资'},

            {field: 'bonus', title: '平台分红'},
            {field: 'system_punish', title: '系统处罚'},
            {field: 'company_punish', title: '公司处罚'},
            {field: 'actually_salary', title: '实际发放'},

        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    /*
    添加事件
    */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){

        let pre_time=$("#formInfo").attr('data-pre-time')
        let now_time=$("#formInfo").attr('data-now-time')
        let search_year = parseInt($("select[name='year']").val());
        let search_month = parseInt($("select[name='month']").val());
        let now_year = parseInt(now_time.split('-')[0]);
        let now_month = parseInt(now_time.split('-')[1]);
        let pre_year = parseInt(pre_time.split('-')[0]);
        let pre_month = parseInt(pre_time.split('-')[1]);
        if (search_year < now_year) {
            pre_year = search_year;
            pre_month = search_month;
        }
        if (search_year == now_year) {
            if (search_month < now_month) {
                pre_month = search_month;
            }
        }
        if (now_time == pre_time) {
            layer.msg(+pre_time.split('-')[0]+'-'+pre_time.split('-')[1]+'月份的薪资数据还未生成',{icon: 2, time: 2000})
            return
        }

        layer.confirm('将要创建'+pre_year+'-'+pre_month+'月份的工资表？', function(index){

            let index_add = layer.open({
                title: '添加薪资',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Salary/SaveData?year='+pre_year+'&month='+pre_month
            });
            $(window).on("resize", function () {
                layer.full(index_add);
            });
            layer.close(index);
        });


    })
    /*
    搜索事件
     */
    $("select[name='year']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='month']").on('change',function (re) {
        $.submitInfo(table);
    })




    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '查看',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Salary/SaveOneData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;

        }


    });




});
