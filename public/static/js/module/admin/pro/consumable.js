layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload
        ,$ = layui.$;
    //声明全局变量form
    lform = form
    form.on('submit(saveBtnPrize)', function (data) {
        if(data.field.num<=0){
            layer.msg("请输入使用数量")
            return
        }
        $.ajax({
            url: '/admin/Pro/relationServiceSession',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {
                if (res.status == true) {
                    layer.closeAll(); //关闭所有页面层
                    relationView();
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });

});
/**
 * @context 门店已关联列表
 * @param mark
 */
function ServiceRelation(id){
    $.ajax({
        url: '/admin/FileOperation/ServiceRelation',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:'关联门店列表 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['80%', '80%'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * 显示商品分类
 * @param type
 */
function showClassType(type)
{
    $.ajax({
        url:'/admin/Pro/adminGetProClass',
        type:'post',
        dataType:'json',
        data:{
            type:type
        },
        success:function(res)
        {
            if(res.status){
                $("#class_type").append(res.data);
            }
        }
    })
}

// 搜索服务
function activeTypePrompt(type,mark,mtype){
    if(type == 'pro'){
        var title ="请输入商品名称搜索";

    }else if(type == 'service'){
        var title ="请输入服务名称搜索";

    }
    layer.prompt({
        formType: 0,
        value: '',
        title: title,
        area: ['100px','100px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/ServicePro/active_search',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':mark,
                'mtype':mtype,
                'value':value,

            },
            success: function (res) {
                if (res.status == true) {
                    pop= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
function aloneOption_new(id,type,mark,mtype){
    taskSession(id,type,mark,mtype)
}

function taskSession(id,type,mark,mtype){
    ab=layer.open({
        type: 1,
        title:'数量设置',
        skin: 'layui-layer-custom', //加上边框
        area: ['600px', '300px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <!--task_type-->\n' +
            '                   <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>单次服务使用数量</span>\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input  id="service_id"  type="hidden" min=\'0\' step="0.01" name="service_id" value="'+id+'" class="form-control form-control-lg">\n' +
            '                            <input data-verify="required" id="prize_num"  type="number" min=\'0\' step="0.01" name="num" value="" class="form-control form-control-lg" placeholder="使用数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnPrize" >保存</button>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
    });
}

function changeRelationNum(id,mark,obj)
{
    let num = parseFloat($(obj).html());
    layer.prompt({
        formType: 0,
        value: num,
        title: '输入数量',
        area: ['100px',"100px"] //自定义文本域宽高
    }, function(value, index, elem){
        if (isNaN(value)) {
            layer.msg("请输入数字！")
            return;
        }
        $.ajax({
            url: '/admin/Pro/changeRelationNum',
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                num:value,
                mark:mark,
            },
            success: function (res) {
                if (res.status == true) {
                    $(obj).html(value)
                    layer.closeAll();
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}

function delRelation(obj,id,mark)
{
    layer.confirm('确认删除？',function(){
        $(obj).parents("tr").remove();
        layer.closeAll()
        $.ajax({
            url: '/admin/Pro/delRelation',
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                mark:mark,
            },
            success: function (res) {
                if (res.status == true) {

                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });
    })
}

function relationView(){
    $.ajax({
        url: '/admin/Pro/relationView',
        type: 'post',
        dataType: 'json',
        data: {
            id:consumable_id,
        },
        success: function (res) {
            if (res.status == true) {
                $(".relationBlockContent").html(res.data)
                $(".relationBlock").css("display","block")
            }else{
                $(".relationBlockContent").html('')
                $(".relationBlock").css("display","none")
            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}