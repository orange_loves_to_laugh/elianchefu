layui.use(['form', 'table'], function () {
    var $ = layui.jquery,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzFinance/ExpensesTypeIndex?action=ajax',
        method: 'post',
        toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'type_title', title: '标题'},
            {field: 'type_cate', title: '状态',templet:"#showTypeTpl"},
            {field: 'type_discribe', title: '说明'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        limits: [10, 15, 20, 25, 50, 100],

        page: true,
        skin: 'line'
    });


    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzFinance/SaveType?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzFinance/DelType',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });

    /**
     * toolbar监听事件
     */
    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzFinance/SaveType'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }

    });

});