layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzFinance/ExpensesIndex?action=ajax&category='+category+'&type_id='+type_id+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
        method: 'post',

        defaultToolbar: [],
        cols: [[
            {field: 'title', title: '名称'},
            {field: 'category_title', title: '状态'},
            {field: 'type_title', title: '类型'},
            {field: 'explain', title: '渠道'},
            {field: 'create_time', title: '创建时间'},
            {field: 'price', title: '金额'},
            {field: 'username', title: '经手人'},
            {field: 'detail', title: '备注'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    });
    /*
       添加事件
       */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzFinance/SaveExpenses'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    /*
    类型管理事件
     */

    $('.layui-btn-container').on('click','[data-action="catemenage"]',function(re){
        let index = layer.open({
            title: '费用类型管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzFinance/ExpensesTypeIndex'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    $("select[name='category']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("select[name='type_id']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("input[name='title']").on('blur',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑费用',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzFinance/SaveExpenses?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzFinance/DelExpenses',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });




});