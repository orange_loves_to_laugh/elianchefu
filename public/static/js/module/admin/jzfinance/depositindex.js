layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzFinance/DepositIndex?action=ajax',
        method: 'post',
        defaultToolbar: [],
        parseData:function(data){

            $('#total_price').html('待提现总额:'+'￥'+data.price.total_price);
            $('#cooperate_price').html('合作端待提现金额:'+'￥'+data.price.cooperate_price);
            $('#partner_price').html('合伙人待提现金额:'+'￥'+data.price.partner_price);
            $('#merchants_price').html('商家端待提现金额:'+'￥'+data.price.merchants_price);
        },
        cols: [[
            {field: 'username', title: '提现人姓名'},
            {field: 'phone', title: '联系方式'},
            {field: 'create_time', title: '发起时间'},
            {field: 'account_time', title: '到账时间'},
            {field: 'launch_cate_title', title: '提现类型'},
            {field: 'price', title: '提现金额'},
            {field: 'deposit_type_title', title: '提现途径'},
            {field: 'deposit_status_title', title: '提现状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    });
    $('.layui-btn-container').on('click','[data-action="statics"]',function(re){


        // 监听添加操作
        let index = layer.open({
            title: '提现统计',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzFinance/deposit_static'
        });

        return false;


    })

    //return

    $("select[name='total_cate']").on('change',function (re) {
        $.submitInfo(table);
    })

    $("input[name='phone']").on('blur',function (re) {
        $.submitInfo(table);
    })

    $("select[name='deposit_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: data.url,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzNotice/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });


});
function toDetail(type){
    layer.open({
        title: '查看详情',
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: '/admin/JzFinance/DepositStaticDetail?type=1&search_type='+type,
    });
    $(window).on("resize", function () {
        layer.full(index);
    });
    return false;
}