layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload
        ,$ = layui.$;
    //百度编辑器
    let ue = UE.getEditor('news_context', {
        // initialFrameWidth: 800,
        initialFrameHeight: 600,
        autoFloatEnabled: true,
        autoHeightEnabled: true,
        elementPathEnabled: true,
        enableAutoSave: true,
    });
    /*
       初始化时间
        */
    $("#start_time").flatpickr({
        enableTime: true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#start_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })
    $("#end_time").flatpickr({
        enableTime: true,

        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        defaultDate: $("#end_time").attr('data-value'),
        time_24hr:true,
        defaultHour:'00',
    })


    //监听提交
    form.on('submit(saveBtn)', function (data) {
        var id =data.id

        var mark = $("input[name='mark']").val();
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/applay/savebanner?action=ajax&id='+id+'&mark='+mark,
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                            //window.location.reload();
                        })

                }  else {
                    console.log(res.msg)
                    layer.msg(res.msg, {icon: 5, time: 1000})

                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });





    });
    //轮播图多图片上传
    //$.multiupload(upload,'multiple_banner','div_slide_show','banner_images')

    //图片上传
    $.singleupload(upload,'headImg','banner_images','demoText','imgurl')

    //删除多图图片
    // $(".layui-upload-list").on('click','img',function (re){
    //     let elem=$("#banenrinput").find('input#banner_images')
    //
    //     $.delMultipleImgs($(this),'banner',elem)
    // })

    /*
   选择是否跳转
    */
    $(".input-group").on("click",".layui-form-radio",function ()
    {
        //支持跳转 1 是  2 否
        if ($('input[name="is_jump"]:checked').val() == 1)
        {
            $(".do_jump").attr('style','display:block')
            $("#jumpUri").css("display","none")
            $(".dos_jump").attr('style','display:none')
            $("#uecontext").css("display","none")
            if($("input[name='is_type']:checked").val()>0){
                if($("input[name='is_type']:checked").val() ==1){
                    $(".dos_jump").attr('style','display:none')
                    $("#jumpUri").css("display","none")
                    $("#uecontext").css("display","block")
                }else{
                    $(".dos_jump").attr('style','display:block')
                    $("#jumpUri").css("display","none")
                    if($("#jump_type").children('option:selected').val()==9){
                        $("#relation").css("display","none")
                        $("#jumpUri").css("display","block")
                    }
                }
            }
            //$('button[name="connect_button"]').parents('.do_jump').attr('style','display:block')
        }else{
            $(".do_jump").attr('style','display:none')
            $("#jumpUri").css("display","none")
            $(".dos_jump").attr('style','display:none')
            $("#uecontext").css("display","none")
            //$('button[name="connect_button"]').parents('.do_jump').attr('style','display:none')
        }



    })
    /*类型选择切换*/
    $("#jump_type").on("change",function(){
       let val =  $(this).children('option:selected').val();
       if(val!=9){
            $("#jumpUri").css("display","none")
           $("#relation").css("display","block")
       }else{
           $("#relation").css("display","none")
           $("#jumpUri").css("display","block")
       }
    })
    /*
    关联操作
     */
    $('.input-group').on("click","button[name='connect_button']",function ()
    {

        let type=$('#jump_type option:selected').val()
        //1=>'服务', 2=>'活动',3=>'会员',4=>'二手车',5=>'联盟商家',6=>'招募',
        if (type == 5 ||type == 4||type==11)//商户 二手车
        {
            let obj={
                5:{btn: ['关联商户列表', '关联商户详情', ], td:['列表关联','商户列表'],type:type},
                4:{ btn: ['关联二手车列表', '关联二手车详情', ], td:['列表关联','二手车列表'],type:type},
                11:{ btn: ['关联本地生活列表', '关联本地生活详情', ], td:['列表关联','本地生活列表'],type:type},
            }
            connect_confirm(obj[type])
            return
        }



        connect_detail_search(type)



    })
});

/*
关联确认
*/
function connect_confirm(obj)
{
    console.log(obj);
    layer.confirm('请选择需要关联的类型', {
        btn: obj.btn //可以无限个按钮
    }, function(index, layero){
        if(obj.type==5){
            merchantsCateList(obj,2)
        }else if(obj.type==11){
            merchantsCateList(obj,1)
        }else{
            $('input[name="jump_id"]').val(0)
            $("#connect_table tr").eq(1).find('td').eq(0).text(obj.td[0])
            $("#connect_table tr").eq(1).find('td').eq(1).text(obj.td[1])
            layer.closeAll();
        }

    }, function(index){
        connect_detail_search(obj.type)

    });
}
/*
关联详情 关键字搜索
 */
function connect_detail_search(type){
    layer.prompt({
        formType: 0,
        value: '',
        title: '名称搜索',
    }, function(value, index, elem){
        let data={param:{
                type:type,
                keywords:value
            }}
        let p=new Promise(function (resolve, reject) {
            console.log(data);
            $.ajax({
                url:'/admin/Applay/SearchData',
                type: 'post',
                data:data,
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function(res){

                if (res.status) {
                    var index = layer.open({
                        type: 1,
                        title:'请选择需要关联的选项',
                        area: ['800px', '600px'],
                        shade: 0.2,
                        maxmin: true,
                        shadeClose: true,
                        content: res.data
                    });

                    $('.layui-layer-content').attr('data-sa-theme',$('body').attr('data-sa-theme'))

                    $(window).on("resize", function () {
                        layer.full(index);
                    });
                    return false;
                }else{
                    layer.msg('未搜索到相关数据', {icon: 2,time:2000});
                }
            },
            function (res){
                layer.open({
                    title: '操作失败',
                    content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                        +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                })
            }
        )
        // layer.close(index);
    });

}
/*
关联详情
 */
function connect_detail_callback(obj){
    let type_title_obj={1:'服务详情',2:'活动详情',3:'会员体验卡详情',4:'二手车详情',5:'联盟商家详情',6:'招募详情',11:'本地生活详情'}

    let title=$(obj).parents('tr').find('td').eq(0).text()
    let type=$('#jump_type option:selected').val()
    let type_title=type_title_obj[type]

    if($(obj).attr('data-type')){
        $('input[name="jump_id"]').val(0)
        $('input[name="cate_id"]').val($(obj).attr('data-id'))
        if($(obj).attr('data-type')==1){
            type_title = "本地生活分类"
            if($(obj).attr('data-id')<=0){
                type_title = "列表关联"
                title = "本地生活列表"

            }
        }else{
            type_title = "联盟商家分类"
            if($(obj).attr('data-id')<=0){
                type_title = "列表关联"
                title = "联盟商家列表"
            }
        }
    }else{
        $('input[name="jump_id"]').val($(obj).attr('data-id'))
    }


    $("#connect_table tr").eq(1).find('td').eq(0).text(type_title)
    $("#connect_table tr").eq(1).find('td').eq(1).text(title)
    layer.closeAll();

}

function merchantsCateList(obj,is_type){
    $.ajax({
        url:'/admin/Applay/merchantsCateList',
        type: 'post',
        data: {
            is_type:is_type
        },
        async: true,
        dataType: 'json',
        success: function (res) {
            var index = layer.open({
                type: 1,
                title:'请选择分类',
                area: ['800px', '600px'],
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                content: res.data
            });

            $('.layui-layer-content').attr('data-sa-theme',$('body').attr('data-sa-theme'))

            $(window).on("resize", function () {
                layer.full(index);
            });
        },
        error: function(re) {

        }
    });

}