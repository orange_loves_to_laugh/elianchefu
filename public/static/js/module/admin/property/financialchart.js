var province = '';
var city = '';
var area = '';
var start_time='';
var end_time="";
$(function() {
    $('#target').distpicker({});
    let flatpickr_register_time=$("#start_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (data){
            start_time = $("#start_time").val()
            flatpickr_register_time.close()
        }
    })
    let flatpickr_member_create=$("#end_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (){
           end_time = $("#end_time").val()
            timeInit();
            flatpickr_member_create.close()
        }
    })
    $("select[name='province']").on('change',function (re) {
        province = $("select[name='province'] option:selected").val()
        dataInit()
    })
    $("select[name='city']").on('change',function (re) {
        city =  $("select[name='city'] option:selected").val()
        dataInit()
    })
    $("select[name='area']").on('change',function (re) {
        area = $("select[name='area'] option:selected").val()
        dataInit()
    })
    dataInit();
});
function dataInit(){
    incomeChart();
    subsidyChart();
    disburseChart();
    subsidyPropertyChart();
    profitChart();
}


/**
 * @context 选择时间
 */
function timeInit(){
    disburseChart();
    subsidyPropertyChart();
}


function incomeChart()
{
    let chartDom = document.getElementById('incomeChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/Property/BindChart?province='+province+'&city='+city+'&area='+area, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {

                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'           // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params[0].data.title} </br> 
                            数值：${params[0].data.value} </br> 
                             `;
                    },
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: '60%',
                        //data: [10, 52, 200, 334, 390, 330, 220],
                        data: data.data,
                    },
                ]
            }
            );
        // myChart.getZr().off('click');
        // myChart.getZr().on('click', function(param) {
        //     // 获取 点击的 触发点像素坐标
        //     const pointInPixel = [param.offsetX, param.offsetY]
        //     // 判断给定的点是否在指定的坐标系或者系列上
        //     if (myChart.containPixel('grid', pointInPixel)) {
        //         // 获取到点击的 x轴 下标  转换为逻辑坐标
        //         let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
        //         // 做一些其他事情
        //         clickFun(data.data[xIndex])
        //     }
        // })
    })

    // function clickFun(param) {
    //     var index = layer.open({
    //         title: '查看详情',
    //         type: 2,
    //         shade: 0.2,
    //         maxmin: true,
    //         shadeClose: true,
    //         area: ['100%', '100%'],
    //         content: '/admin/JzFinancialStatistics/IncomeDetail?income_type='+param.income_type+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
    //     });
    //     $(window).on("resize", function () {
    //         layer.full(index);
    //     });
    //     return false;
    // }
    option && myChart.setOption(option);
}


function subsidyChart()
{
    let chartDom = document.getElementById('subsidyChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/Property/payFeeChart?province='+province+'&city='+city+'&area='+area, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {

                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'           // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params[0].data.title} </br> 
                            数值：${params[0].data.value} </br> 
                             `;
                    },
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        },
                        axisLabel: {    // 坐标轴标签
                            show: true,  // 是否显示
                            inside: false, // 是否朝内
                            fontSize:12,
                            interval:0,
                            rotate:40
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: 60,  // 柱形的宽度
                        barCategoryGap: 80,  // 柱形的间距

                        //data: [10, 52, 200, 334, 390, 330, 220],
                        data: data.data,
                    },
                ]
            }
        );
        // myChart.getZr().off('click');
        // myChart.getZr().on('click', function(param) {
        //     // 获取 点击的 触发点像素坐标
        //     const pointInPixel = [param.offsetX, param.offsetY]
        //     // 判断给定的点是否在指定的坐标系或者系列上
        //     if (myChart.containPixel('grid', pointInPixel)) {
        //         // 获取到点击的 x轴 下标  转换为逻辑坐标
        //         let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
        //         // 做一些其他事情
        //         clickFun(data.data[xIndex])
        //     }
        // })
    })
    // function clickFun(param) {
    //     var index = layer.open({
    //         title: '查看详情',
    //         type: 2,
    //         shade: 0.2,
    //         maxmin: true,
    //         shadeClose: true,
    //         area: ['100%', '100%'],
    //         content: '/admin/JzFinancialStatistics/subsidyDetail?subsidy_type='+param.subsidy_type+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
    //     });
    //     $(window).on("resize", function () {
    //         layer.full(index);
    //     });
    //     return false;
    // }
    option && myChart.setOption(option);
}

function disburseChart()
{
    let chartDom = document.getElementById('disburseChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/Property/commissionChart?start_time='+start_time+'&end_time='+end_time+'&province='+province+'&city='+city+'&area='+area, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {
                tooltip: {
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params.data.title} </br> 
                            数值：${params.data.value} </br> 
                         `;
                    },
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        },
                        axisLabel: {    // 坐标轴标签
                            show: true,  // 是否显示
                            inside: false, // 是否朝内
                            fontSize:12,
                            interval:0,
                            rotate:40
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'line',
                        data: data.data,
                    },
                ]
            }
        );
        // myChart.getZr().off('click');
        // myChart.getZr().on('click', function(param) {
        //     // 获取 点击的 触发点像素坐标
        //     const pointInPixel = [param.offsetX, param.offsetY]
        //     // 判断给定的点是否在指定的坐标系或者系列上
        //     if (myChart.containPixel('grid', pointInPixel)) {
        //         // 获取到点击的 x轴 下标  转换为逻辑坐标
        //         let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
        //         // 做一些其他事情
        //         clickFun(data.data[xIndex])
        //     }
        // })
    })
    // function clickFun(param) {
    //     var index = layer.open({
    //         title: '查看详情',
    //         type: 2,
    //         shade: 0.2,
    //         maxmin: true,
    //         shadeClose: true,
    //         area: ['100%', '100%'],
    //         content: '/admin/JzFinance/ExpensesIndex?category=2&type_id='+param.id+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
    //     });
    //     $(window).on("resize", function () {
    //         layer.full(index);
    //     });
    //     return false;
    // }
    option && myChart.setOption(option);
}

function subsidyPropertyChart()
{
    let chartDom = document.getElementById('subsidyPropertyChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/Property/subsidyChart?start_time='+start_time+'&end_time='+end_time+'&province='+province+'&city='+city+'&area='+area, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {
                tooltip: {
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params.data.title} </br> 
                            数值：${params.data.value} </br> 
                         `;
                    },
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        },
                        axisLabel: {    // 坐标轴标签
                            show: true,  // 是否显示
                            inside: false, // 是否朝内
                            fontSize:12,
                            interval:0,
                            rotate:40
                        },
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'line',
                        data: data.data,
                    },
                ]
            }
        );
        // myChart.getZr().off('click');
        // myChart.getZr().on('click', function(param) {
        //     // 获取 点击的 触发点像素坐标
        //     const pointInPixel = [param.offsetX, param.offsetY]
        //     // 判断给定的点是否在指定的坐标系或者系列上
        //     if (myChart.containPixel('grid', pointInPixel)) {
        //         // 获取到点击的 x轴 下标  转换为逻辑坐标
        //         let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
        //         // 做一些其他事情
        //         clickFun(data.data[xIndex])
        //     }
        // })
    })
    // function clickFun(param) {
    //     var index = layer.open({
    //         title: '查看详情',
    //         type: 2,
    //         shade: 0.2,
    //         maxmin: true,
    //         shadeClose: true,
    //         area: ['100%', '100%'],
    //         content: '/admin/JzFinance/ExpensesIndex?category=2&type_id='+param.id+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
    //     });
    //     $(window).on("resize", function () {
    //         layer.full(index);
    //     });
    //     return false;
    // }
    option && myChart.setOption(option);
}

function profitChart()
{
    let chartDom = document.getElementById('profitChart');
    let myChart = echarts.init(chartDom);
    let option;
    myChart.showLoading();
    $.post('/admin/Property/recommendChart?province='+province+'&city='+city+'&area='+area, function (data) {
        myChart.hideLoading();
        myChart.setOption(
            option = {

                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'           // 默认为直线，可选为：'line' | 'shadow'
                    },
                    formatter(params) {
                        // 返回值就是html代码可以使用标签
                        return `
                            名称：${params[0].data.title} </br> 
                            数值：${params[0].data.value} </br> 
                             `;
                    },
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data.title,
                        axisTick: {
                            alignWithLabel: true
                        },
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLine:{
                            lineStyle:{
                                color:"#FFF"
                            }
                        }
                    }
                ],

                series: [
                    {
                        name: '直接访问',
                        type: 'bar',
                        barWidth: '60%',
                        //data: [10, 52, 200, 334, 390, 330, 220],
                        data: data.data,
                    },
                ]
            }
        );
        // myChart.getZr().off('click');
        // myChart.getZr().on('click', function(param) {
        //     // 获取 点击的 触发点像素坐标
        //     const pointInPixel = [param.offsetX, param.offsetY]
        //     // 判断给定的点是否在指定的坐标系或者系列上
        //     if (myChart.containPixel('grid', pointInPixel)) {
        //         // 获取到点击的 x轴 下标  转换为逻辑坐标
        //         let xIndex = myChart.convertFromPixel({ seriesIndex: 0 },pointInPixel)[0]
        //         // 做一些其他事情
        //         clickFun(data.data[xIndex])
        //     }
        // })
    })

    // function clickFun(param) {
    //     var index = layer.open({
    //         title: '查看详情',
    //         type: 2,
    //         shade: 0.2,
    //         maxmin: true,
    //         shadeClose: true,
    //         area: ['100%', '100%'],
    //         content: '/admin/JzFinancialStatistics/IncomeDetail?income_type='+param.income_type+'&switchStatic='+switchStatic+'&start_time='+start_time+'&end_time='+end_time,
    //     });
    //     $(window).on("resize", function () {
    //         layer.full(index);
    //     });
    //     return false;
    // }
    option && myChart.setOption(option);

}



