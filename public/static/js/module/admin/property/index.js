layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    table.render({
        elem: '#myTable',
        url: '/admin/Property/Index?action=ajax',
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {field: 'nh_title', title: '社区名称'},
            {field: 'area_detail', title: '所在地区'},
            {field: 'username', title: '联系人'},
            {field: 'telphone', title: '联系电话'},
            {field: 'property_title', title: '物业公司名称'},
            {field: 'house_num', title: '房屋数量'},
            {field: 'draw_prop', title: '抽成比例'},
            {field: 'recommend_num', title: '园区关联用户数量'},
            {title: '操作', toolbar: '#currentTableBar', width: 400}
        ]],
        limit:10,
        page: true,
        skin: 'line'
    });
    $('#target').distpicker({});
    /*
    添加事件
     */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Property/SaveData'
        });


    })
    $('.layui-btn-container').on('click','[data-action="static"]',function(re){
        let index = layer.open({
            title: '数据统计',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Property/financialChart'
        });


    })


    /*
    弹窗管理
    */
    $('.layui-btn-container').on('click','[data-action="editPopup"]',function(re){


        let index = layer.open({
            title: '商户弹窗管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/SavePopup'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //任务管理
    $('.layui-btn-container').on('click','[data-action="task"]',function(re){

        let index = layer.open({
            title: '商户任务管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/task_index'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //红包管理
    $('.layui-btn-container').on('click','[data-action="redpacket"]',function(re){

        let index = layer.open({
            title: '商户弹窗管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/redpacket_detail_index'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //邮寄物料
    $('.layui-btn-container').on('click','[data-action="materiel"]',function(re){

        let index = layer.open({
            title: '邮寄物料',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/merchants_materiel?status='+1
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    // 投放卡券
    $('.layui-btn-container').on('click','[data-action="launchVoucher"]',function(re){


        let index = layer.open({
            title: '投放卡券',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/market/launch_voucher'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })



    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='area']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee_sal']").on('change',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '编辑',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Property/SaveData?id='+data.id,
            });
            return false;

        }

        if (obj.event === 'cache') {

            var index_cache = layer.open({
                title: '提现记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Property/CacheLog?id='+data.merchants_id,
            });
            $(window).on("resize", function () {
                layer.full(index_cache);
            });
            return false;
        }

        if (obj.event === 'income') {

            var index_income = layer.open({
                title: '收入记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzMerchants/IncomeLog?id='+data.id,
                content: '/admin/Property/CollectionsLog?id='+data.merchants_id,
            });
            $(window).on("resize", function () {
                layer.full(index_income);
            });
            return false;
        }

        if (obj.event === 'member') {

            var index_member = layer.open({
                title: '推荐会员',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Property/CardLog?id='+data.merchants_id,
            });
            $(window).on("resize", function () {
                layer.full(index_member);
            });
            return false;
        }

        if (obj.event === 'showimg'){

            let aa={
                "title": '物业收款码', //相册标题
                "id": 1, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": '物业收款码',
                        "pid": 2, //图片id
                        "src": data.collections_bar, //原图地址
                        "thumb":data.collections_bar//缩略图地址
                    }
                ]
            }
            layer.photos({
                photos: aa
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
        if (obj.event === 'showMerchants'){

            let url='http://web.elianchefu.com/#/pages/recommend/index?a=scan&form=merchants&employeeId=0&assignBizId='+data.merchants_id
            let url2='http://web.elianchefu.com/#/pages/index/propertyInfo?id='+data.id+'&form=merchants&employeeId=0&assignBizId='+data.merchants_id

            layer.open({
                title: '物业分享码',
                type: 1,
                skin: 'whiteColor', //加上边框
                area: ['600px', '300px'], //宽高
                shadeClose: true,
                content: '<div style="display: flex;color:#000000">\n' +
                    '          <div style="margin-left: 10%; margin-top: 2%;">\n' +
                    '            <div style="width:100%;text-align: center;margin-bottom:10px;">物业推荐码</div>\n' +
                    '            <div class="img"  id="qrcode"></div>\n' +
                    '          </div>\n' +
                    '        <div style="margin-left: 10%; margin-top: 2%;">\n' +
                    '          <div style="width:100%;text-align: center;margin-bottom:10px;">物业收款码</div>\n' +
                    '          <div class="img"  id="qrcode2"></div>\n' +
                    '        </div>\n' +
                    '      </div>'
            });
            makeQrcode('qrcode',url)
            makeQrcode('qrcode2',url2)
        }


    });




});

function makeQrcode(id,url)
{
    $("#"+id).qrcode({
        render : "canvas",
        text : url,
        width : "200",
        height : "200",
        background : "#ffffff",
        foreground : "#000000",
        src: '/static/img/logo_qrcode.png',
        imgWidth:40,
        imgHeight:40,
    });
}
