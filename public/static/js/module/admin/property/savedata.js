layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
    //声明全局变量form
    lform = form;
    //关联员工
    $('button[data-type="relate_employee"]').on("click",function ()
    {
        let data={param:{
                keywords:'',
                // notin_merchants:'true',
            }}
        $.ajax({
            url:'/admin/Employee/RelateEmployee',
            type: 'post',
            data:data,
            async: true,
            dataType: 'json',
            success: function (res) {
                var index = layer.open({
                    type: 1,
                    title:'请选择需要关联的选项',
                    area: ['800px', '600px'],
                    shade: 0.2,
                    skin: 'layui-layer-custom', //加上边框
                    maxmin: true,
                    shadeClose: true,
                    content: res.data
                });



                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            },
            error: function(re) {

                layer.open({
                    title: '操作失败',
                    content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                    +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                })
            }
        });



    })


    //进来就执行 显示分类
    /*
     * @content  监听二级分类联动
     * */
    form.on('select(cate_pid_select)', function(data){
        console.log(data.elem); //得到select原始DOM对象
        console.log(data.value); //得到被选中的值
        console.log(data.othis); //得到美化后的DOM对象
        $.ajax({
            url: "/admin/JzMerchantsCate/DataList?pid="+data.value,
            type: 'post',
            async: true,
            dataType: 'json',
            success: function (res) {
                console.log(res.data,'00')

                $("select[name='cate_id']").html(res.data)
                // $('#classPid').html(res.data);
                lform.render();

                // resolve(res)
            },
            error: function(re) {
                // reject(re)
            }
        });

    });
    //图片上传
    $.singleupload(upload,'headImg','imgurlHidden','demoText','imgurl')
    //营业执照图
    $.singleupload(upload,'headImg_license_imgurl','imgurlHidden_license_imgurl','demoText_license_imgurl','imgurl_license_imgurl')

    //视频上传
    $.singleupload(upload,'headImg1','imgurlHidden1','demoText1','imgurl1','video')
    //轮播图多图片上传
    $.multiupload(upload,'multiple_banner','div_slide_show','banner_images')

    //轮播图删除 onclick="delMultipleImgs(this,'banner')"

    $("#div_slide_show").on('click','img',function (re) {

        let elem=$("#banenrinput").find('input#banner_images')

        $.delMultipleImgs($(this),'banner',elem)
    })

    //监听提交
    form.on('submit(saveBtn)', function (data) {



        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/Property/SaveData?savemark=1&action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    $("#L_repass").css('display','none');

                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });

    });
    //监听提交
    form.on('submit(saveBuildBtn)', function (data) {
        //发异步，把数据提交给php
        console.log(data.field)
        if(data.field.house_title==""){
            layer.msg('请输入楼号名称')
            return false;
        }
        if(data.field.build_cate==0){
            layer.msg('请选择房屋类型')
            return false;
        }
        let type_title = $("select[name='build_cate'] :selected").text();
        layer.close(layer.index);
        createUnit(data.field.house_title,data.field.build_cate,type_title);

    });
    /**
     * @context 创建楼房提交
     */
    form.on('submit(saveHouseBtn)', function (data) {
        if(data.field.property_price<=0){
            layer.msg('请填写物业费单价')
            return false;
        }
        $.ajax({
            url: '/admin/Property/SaveBuild',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    layer.closeAll('page');
                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
    });
    /**
     * @context 创建单元提交
     */
    form.on('submit(saveUnitHouseBtn)', function (data) {
        if(data.field.unit_title==""){
            layer.msg('请填写单元名称')
            return false;
        }
        if(data.field.floor_num<=0){
            layer.msg('请填写楼层数量')
            return false;
        }
        if(data.field.house_num<=0){
            layer.msg('请填写每层户数')
            return false;
        }
        roomInfo(data.field.unit_title,data.field.floor_num,data.field.house_num);
    });
    function roomInfo(title,floor_num,house_num){
        let str="";
        for(let i=1;i<=floor_num;i++){
            for(let j=1;j<=house_num;j++){
                str+='<div class=" input-group input-group-lg" style="margin-bottom:10px;">\n' +
                    '                            <div style="display: flex">\n' +
                    '                                <span class="input-group-addon" >单元名称：</span>\n' +
                    '                                <input  data-verify="required" style="border-width: 0 0 1px 1px;" name="unit_title[]" readonly  type="text"  value="'+title+'"  class="form-control form-control-lg" >\n' +
                    '                            </div>\n' +
                    '                            <div style="display: flex;margin-left:10px;">\n' +
                    '                                <span class="input-group-addon" >楼层：</span>\n' +
                    '                                <input  data-verify="required" style="border-width: 0 0 1px 1px;" name="floor_number[]"   type="text"  value="'+i+'层"  class="form-control form-control-lg" >\n' +
                    '                            </div>\n' +
                    '                            <div style="display: flex;margin-left:10px;">\n' +
                    '                                <span class="input-group-addon" >房间号：</span>\n' +
                    '                                <input  data-verify="required" style="border-width: 0 0 1px 1px;" name="room_number[]"   type="text"  value="'+pad(j,house_num.toString().length)+'"  class="form-control form-control-lg" >\n' +
                    '                            </div>\n' +
                    '                            <div style="display: flex;margin-left:10px;">\n' +
                    '                                <span class="input-group-addon" >房屋面积：</span>\n' +
                    '                                <input  data-verify="required" style="border-width: 0 0 1px 1px;" name="room_area[]"   type="text"  value=""  class="form-control form-control-lg" >\n' +
                    '                            </div>\n' +
                    '                            <div style="display: flex;margin-left:10px;">\n' +
                    '                                <span class="input-group-addon" >房主姓名：</span>\n' +
                    '                                <input  data-verify="required" style="border-width: 0 0 1px 1px;" name="member_title[]"   type="text"  value=""  class="form-control form-control-lg" >\n' +
                    '                            </div>\n' +
                    '                            <div style="display: flex;margin-left:10px;">\n' +
                    '                                   <button class="btn btn-info btn-danger" onclick="delRoom(this)" type="button" >删除</button>\n'+
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                       ';
            }
        }
        layer.close(layer.index);
        $("#room_content").append(str);


    }
    function pad(num, n) {
        console.log(num)
        console.log(num<10)
        if(num<10){
            console.log(123)
            return '0' + num;
        }
        if(String(num).length > n) return num;
        return (Array(n).join(0) + num).slice(-n);
    }
    function createUnit(title,type,type_title){
        layer.open({
            type: 1,
            title:'房屋信息 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['100%', '100%'], //宽高
            end:function(){

            },
            content: '<form id="form2"  class="layui-form" >\n' +
                '\n' +
                '            <div class="card">\n' +
                '\n' +
                '                <div class="card-body">\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>楼号名称</span>\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="house_title"  type="text" name="house_title" readonly value="'+title+'" class="form-control form-control-lg" placeholder="请填写楼号名称">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <br>\n' +
                '               <div class="input-group input-group-lg">\n' +
                '                    <span class="input-group-addon">房屋类型</span>\n' +
                '                    <div class="form-group input-group-lg">\n' +
                '                         <input data-verify="required" id="build_cate"  type="hidden" name="build_cate" value="'+type+'" class="form-control form-control-lg" placeholder="请填写楼号名称">\n' +
                '                         <input data-verify="required"   type="text" name="build_cate_title" value="'+type_title+'" class="form-control form-control-lg" readonly placeholder="请填写楼号名称">\n' +
                '                         <i class="form-group__bar"></i>\n' +
                '                    </div>\n' +
                '                </div>'+
                '                <br>'+
                '               <div class="input-group input-group-lg">\n' +
                '                    <span class="input-group-addon">物业费单价</span>\n' +
                '                    <div class="form-group input-group-lg">\n' +
                '                         <input data-verify="required" name="property_price"  type="number" step="0.01" min="0"   class="form-control form-control-lg"  placeholder="请填写物业费单价">\n' +
                '                         <i class="form-group__bar"></i>\n' +
                '                    </div>\n' +
                '                </div>'+
                '                <br>'+
                '               <div class="input-group input-group-lg">\n' +
                '                        <button class="btn btn-primary" type="button"  onclick="createUnitHouse()">创建单元</button>\n' +
                '                </div>'+
                '                <br>'+
                '               <div id="room_content" class="form-group input-group-lg">\n' +


                '                </div>'+
                '                </div>\n' +
                '            </div>' +
                '            <div class="card">\n' +
                '                <div class="card-body">\n' +
                '                    <div class="btn-demo">\n' +
                '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveHouseBtn">保存创建</button>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>' +
                '           </form> '
        });
        lform.render();
    }



    $("#create_house").on("click",function(){
        layer.open({
            type: 1,
            title:'房屋信息 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['60%', '60%'], //宽高
            end:function(){

            },
            content: '<form id="form2"  class="layui-form" >\n' +
                '\n' +
                '            <div class="card">\n' +
                '\n' +
                '                <div class="card-body">\n' +
                '                    <div class="input-group input-group-lg">\n' +
                '                        <span class="input-group-addon"><span class="" style="color:red">*</span>楼号名称</span>\n' +
                '                        <div class="form-group input-group-lg">\n' +
                '                            <input data-verify="required" id="house_title"  type="text" name="house_title" class="form-control form-control-lg" placeholder="请填写楼号名称">\n' +
                '                            <i class="form-group__bar"></i>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <br>\n' +
                '               <div class="input-group input-group-lg">\n' +
                '                    <span class="input-group-addon">房屋类型</span>\n' +
                '                    <div class="form-group input-group-lg" id="systSelect">\n' +
                '                        <select class="select2" name="build_cate"  >\n' +
                '                             <option  value="0"  >请选择房屋类型</option>\n' +
                '                             <option  value="1"  >高层</option>\n' +
                '                             <option  value="2"  >公寓</option>\n' +
                '                             <option  value="3"  >小高层</option>\n' +
                '                             <option  value="4"  >多层</option>\n' +
                '                             <option  value="5"  >洋房</option>\n' +
                '                             <option  value="6"  >别墅</option>\n' +
                '                             <option  value="7"  >门市</option>\n' +
                '                        </select>\n' +
                '                    </div>\n' +
                '                </div>'+
                '                <br>'+
                '                </div>\n' +
                '            </div>' +
                '            <div class="card">\n' +
                '                <div class="card-body">\n' +
                '                    <div class="btn-demo">\n' +
                '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBuildBtn">下一步</button>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>' +
                '           </form> '
        });
        lform.render();
    })
    $("#check_house").on("click",function(){
        let id=$("input[name='id']").val();
        layer.open({
            title: '房屋信息',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/Property/checkHouse?id='+id,
        });
        return false;
    })
});

/**
 * @context 搜索关联员工
 */
function searchRelevanceStore()
{

    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        let data={param:{
                keywords:search_title,
                // notin_merchants:'true',
            }}
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/Employee/RelateEmployee',
                type: 'post',
                data:data,
                dataType: 'json',
                success: function(res) {

                    if (res.status) {

                        res.datalist.forEach(function(e) {
                            //console.log($(".lump"));
                            var  oldelement = $(".lump" + e.id);


                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

/*
执行关联操作
 */
function connect_detail_callback(obj){

    $('#staffDisplay').css('display', 'block');

    $('input[name="assign_employee"]').val($(obj).attr('data-id'));

    $('#staffTable tbody tr:eq(0) td:eq(0)').html($(obj).attr('data-title'));




    layer.closeAll();

}

function createUnitHouse(){
    layer.open({
        type: 1,
        title:'房屋信息 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['100%', '100%'], //宽高
        end:function(){

        },
        content: '<form id="form2"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>单元名称</span>\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="unit_title"  type="text" name="unit_title" class="form-control form-control-lg" placeholder="请填写单元名称">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '               <div class="input-group input-group-lg">\n' +
            '                    <span class="input-group-addon">楼层数量</span>\n' +
            '                    <div class="form-group input-group-lg">\n' +
            '                         <input data-verify="required" name="floor_num"   type="number" step="1" min="0" class="form-control form-control-lg" placeholder="请填写楼层数量">\n' +
            '                         <i class="form-group__bar"></i>\n' +
            '                    </div>\n' +
            '                </div>'+
            '                <br>'+
            '               <div class="input-group input-group-lg">\n' +
            '                    <span class="input-group-addon">每层户数</span>\n' +
            '                    <div class="form-group input-group-lg">\n' +
            '                         <input data-verify="required" name="house_num"   type="number" step="1" min="0"   class="form-control form-control-lg"  placeholder="请填写每层户数">\n' +
            '                         <i class="form-group__bar"></i>\n' +
            '                    </div>\n' +
            '                </div>'+
            '                <br>'+
            '                </div>\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveUnitHouseBtn">保存</button>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>' +
            '           </form> '
    });
    lform.render();
}

function delRoom(obj)
{
    $(obj).parent().parent().remove();
    return false;
}

/**
 *
 * @context 修改各种数量
 * @param mark
 * @param obj
 */
function getMerchantsNum(mark,obj){
    var val = parseInt($(obj).html())
    if(mark == 'thumbup_num'){
        var title = '请修改点赞数量';
    }else if(mark == 'forward_num'){
        var title = '请修改转发数量';
    }else if(mark == 'look_num'){
        var title = '请修改查看数量';

    }
    var id = $('#id').val();
    layer.prompt({
        formType: 0,
        value:val,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        if(parseInt(value) < parseInt(val)){
            layer.msg('输入的数量不能小于当前数量', {icon: 5, time: 1000} )
            return;
        }
        $.ajax({
            url: '/admin/Property/changePropertyNum',
            type: 'post',
            dataType: 'json',
            data: {
                'mark':mark,
                'val':value,
                'id':id,

            },
            success: function (res) {
                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    $(obj).html(value);
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}

/*
物业操作
 */
function MerchantsLog(title){
    let merchants_id=$("#merchants_id").val(); // 商户的id
    let id=$("input[name='id']").val(); // 商户的id
    let title_arr={
        'member':{title:'缴费用户',url:'/admin/Member/Index?propertyMark=1&property_id='+id,},
        'cache':{title:'提现记录',url:'/admin/Property/CacheLog?id='+merchants_id,},
        'collections':{title:'收款记录',url:'/admin/Property/CollectionsLog?id='+merchants_id,},
        'voucher':{title:'物业优惠券',url:'/admin/JzMerchants/VoucherList?id='+merchants_id,},
        'card':{title:'推荐会员',url:'/admin/Property/CardLog?id='+merchants_id,},
        'stafflist':{title:'物业员工列表',url:'/admin/Property/StaffList?id='+merchants_id,},
        'commentlist':{title:'物业评论列表',url:'/admin/Property/CommentList?id='+merchants_id,},
    }
    var index_member = layer.open({
        title: title_arr[title].title,
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: title_arr[title].url,
    });
    $(window).on("resize", function () {
        layer.full(index_member);
    });
    return false;
}



