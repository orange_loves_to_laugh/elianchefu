layui.use(['table'], function () {
    var $ = layui.jquery,

        table = layui.table;


    table.render({
        elem: '#myTable',
        url: '/admin/Setmeal/RechargeIndex?action=ajax',
        parseData: function (res) { //res 即为原始返回的数据

            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.count, //解析数据长度
                "data": res.data, //解析数据列表
            };
        },
        method: 'post',
        defaultToolbar: [],
        cols: [[
            {field: 'level_title', title: '级别名称'},
            {field: 'recharge_arr', title: '充值&赠送数据', templet:"#servicearr",width:600},
            {title: '操作', toolbar: '#currentTableBar'}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],
        limit:10,
        page: true,
        skin: 'line'
    });


    $("select[name='level']").on('change',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {

        if (obj.event === 'add'){
            let index = layer.open({
                title: '会员充值金额创建',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/setmeal/recharge_save?level='+obj.data.level
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }

    });


});

/*
删除充值
 */
function delrecharge(obj){


    layer.confirm('真的删除行么', function (index) {

        $.ajax({
            url: '/admin/setmeal/rechargeDeleteInfo',
            type: 'post',
            dataType: 'json',
            data: {
                id: $(obj).attr('data-id'),
            },
            success: function(data) {
                if (data.status) {
                    layer.msg('删除成功!', {icon: 1, time: 1000});
                    $(obj).parents('.rechargearr').remove()

                }else{
                    layer.msg(data.msg, {
                        icon: 2,
                        time: 3000
                    });
                }
            },
            error: function(re) {
                console.log(re)
                layer.open({
                    title: '删除失败',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });


        // $(obj)[0].tr.remove()
    });
}

/*
修改价格
 */
function changePrices(id, price, type) {
    if(type == 'price'){
        var title = '修改充值金额';

    }else{
        var title = '修改赠送金额';

    }
    layer.prompt({
        formType: 0,
        value:price,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){

        $.ajax({
            url: '/admin/setmeal/change_price',
            type: 'post',
            dataType: 'json',
            data: {
                'price':value,
                'id':id,
                'type':type,
            },
            success: function (res) {
                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    location.reload();


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}