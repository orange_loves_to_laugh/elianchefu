let layer_voucher_detail='';
let layer_create_vouchertype='';
let layer_searchpro='';
let layer_prolist='';
layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload
        ,$ = layui.$;


    //声明全局变量form
    lform = form
    reflushRecommendPresent();
    // 监听全选
    form.on('checkbox(checkall)', function (data) {

        if (data.elem.checked) {
            $('.checkboxs').prop('checked', true);
        } else {
            $('.checkboxs').prop('checked', false);
        }
        form.render();
    });

    //监听提交
    form.on('submit(saveBtn)', function (data) {
        var id =data.field.id

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/setmeal/save?action=ajax&id='+id+'',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {

                    layer.msg(res.msg, {icon: 5, time: 1000})

                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });




    });
    form.on('submit(saveBtnRecommend)', function (data) {
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/setmeal/recommendPresent?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {

                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            layer.closeAll();
                            reflushRecommendPresent();
                        })

                }  else {

                    layer.msg(res.msg, {icon: 5, time: 1000})

                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '提示信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg,
                })
            }
        });




    });
    //积分 余额 卡券 保存
    form.on('submit(saveBtnPrize)', function (data) {
        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/setmeal/setmealWrite',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {
                    layer.closeAll(); //关闭所有页面层

                    ServiceRelation(res.setMeal_id)


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });





    });
    //红包卡券保存
    form.on('submit(saveBtnVoucher)', function (data) {

        var id =data.field.id
        var prize_type =data.field.prize_type
        //卡券名称
        var voucher_title =data.field.voucher_title
        if( voucher_title == ''){
            layer.msg('卡券名称为空', {icon: 5, time: 1000} )
            return
        }
        // 卡券价格
        var voucher_price =data.field.voucher_price
        if( voucher_price == ''){
            layer.msg('卡券低值金额为空', {icon: 5, time: 1000} )
            return
        }
        //满多少 的 消费标准
        var money_off =data.field.money_off
        if( money_off == ''){
            layer.msg('消费标准为空', {icon: 5, time: 1000} )
            return
        }
        //有效期
        var voucher_over =data.field.voucher_over
        if( voucher_over == ''){
            layer.msg('有效期为空', {icon: 5, time: 1000} )
            return
        }
        // 赠送数量
        var voucher_number =data.field.voucher_number
        if( voucher_number == ''){
            layer.msg('赠送数量为空', {icon: 5, time: 1000} )
            return
        }
        //上架数量
        var ground_num =data.field.ground_num
        if( ground_num == ''){
            layer.msg('上架数量数量为空', {icon: 5, time: 1000} )
            return
        }



        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/Setmeal/voucherSession',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {

                        layer.close(layer_voucher_detail);

                        layer.close(layer_create_vouchertype);

                        layer.close(layer_searchpro);

                        layer.close(layer_prolist);

                    $('#voucherDisplay').css('display', 'block');
                    $('#voucherInfo').html(res.data);



                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });





    });
    //红包保存
    form.on('submit(saveBtnRed)', function (data) {

        var id =data.field.id
        var price =data.field.price
        var packet_title =data.field.packet_title
        var prize_type =data.field.prize_type
        //套餐id  赠送数量 会员等级
        var ground_num =data.field.ground_num
        //其他物品
        if( ground_num == ''){
            layer.msg('红包数量为空', {icon: 5, time: 1000} )
            return
        }
        //红包判断
        if(prize_type == 'packet'){
            if( packet_title == ''){
                layer.msg('红包名称为空', {icon: 5, time: 1000} )
                return
            }
            if( price == ''){
                layer.msg('红包价值为空', {icon: 5, time: 1000} )
                return
            }
        }
        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/Setmeal/createpacketvoucher',
            type: 'post',
            dataType: 'json',
            data:  data.field,
            success: function (res) {

                if (res.status == true) {

                    layer.closeAll(); //关闭所有页面层
                    ServiceRelation(res.data.setmeal_id)


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }

            },
            error: function (err) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+err.responseJSON.errorCode+':</span>'+err.responseJSON.msg
                })
            }
        });





    });
    //单图片上传
    $.singleupload(upload,'headImg','imgurlHidden','demoText')

    //进入页面时触发
    is_pop();

    $(".input-group").on("click",".layui-form-radio",function () {
        is_pop();

    })

    /*
    免排队数量显示判断
     */
    $('.form-group').on('click','.layui-form-checkbox ',function(){

        if ($(this).prev().attr('data-title') == '免排队')
        {
            if ($(this).prev().is(":checked")) {
                $("#lineup_car_limit").attr('style','')
            }else{
                $("#lineup_car_limit").attr('style','display:none')
                $("#lineup_car_limit").find('input').val(0)

            }
        }

    })

});

function is_pop(){
    // alert(11)
    //是否跳转 1 否  2 是
    var level_limit = $('input[name=level_limit]:checked').val();

    if(level_limit == 1){

        $('.ispopurl').css('display', 'none');

    }else{
        $('.ispopurl').css('display', 'block');

    }

}

$(function () {
    getSetmealList();

})

function getSetmealTitle(){
    var id=$("input[name='id']").val();
    layer.prompt({
        formType: 0,
        value: '',
        title: '请填写套餐名称',
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){

        $.ajax({
            url: '/admin/setmeal/getSetmealTitle',
            type: 'post',
            dataType: 'json',
            data: {
                'title':value,
                'level_id':id,

            },
            success: function (res) {

                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    checkSetMeal(res.data)
                    getSetmealList();

                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
//套餐显示
function getSetmealList(){
    var level_id=$("input[name='id']").val();
    $.ajax({
        url: '/admin/setmeal/getSetmealList',
        type: 'post',
        dataType: 'json',
        data: {
            'level_id':level_id
        },
        success: function (res) {
            if (res.status == true) {
                $('#activeInfo').html(res.param);


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
/*
* 显示套餐详情
* */
function checkSetMeal(setmeal_id){

    $.ajax({
        url: '/admin/setmeal/checkSetMeal',
        type: 'post',
        data:{
            setmeal_id:setmeal_id,
        },
        async: true,
        dataType:'json',
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:res.title,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.param
                });
            }
        }
    });
}
/*
* 删除 套餐 并删除对应套餐详情
* */
function setmealDel(setMeal_id){
    $.ajax({
        url: '/admin/setmeal/SetmealDel',
        type: 'post',
        dataType: 'json',
        data: {
            'setMeal_id':setMeal_id,
        },
        success: function (data) {

            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                });
                layer.close(a);
                getSetmealList();
            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });

}



//删除
function delDetailInfo(id,k,mark,upgradeId){

    $.ajax({
        url: '/admin/setmeal/SetmealTypeDel',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'k':k,
            'mark':mark,

        },
        success: function (data) {

            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                });
                $('#setmealDetailTr'+id).remove()

            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}
function activeGiveService(type,id){
    if(type == 'integral'){
        var  title = '请输入积分';
        getinbalance(type,id,title)
    }else if(type == 'balance'){
        var  title = '请输入余额';
        getinbalance(type,id,title)

    }else if(type == 'pro'){
        confirmStore('pro','give');
    }else if(type == 'service'){
        confirmStore('service','give');
    }else if(type == 'red'){
        confirmStore('red',id);
    }


}
//查询会员升级信息
function ServiceRelation(id){
    $.ajax({
        url: '/admin/setmeal/checkSetMeal',
        type: 'post',
        dataType: 'json',
        data: {
            'setmeal_id':id,

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:res.title,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.param
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
//直接存到数据库中
function getinbalance(type,id,title){
    layer.prompt({
        formType: 0,
        value: '',
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){

        $.ajax({
            url: '/admin/setmeal/upgradeWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'val':value,
                'prize_type':type,
                'id':id,

            },
            success: function (res) {

                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    ServiceRelation(id);


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
//卡券创建
function confirmStore(type,mark){

    if (type == 'red') {
        let level=$('#id').val()
        layer.open({
            type: 1,
            title:'红包设置 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['100%', '100%'], //宽高
            content: '<form id="form1"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                    <input type="hidden" name="level_id" value="'+level+'">\n' +
            '                    <input type="hidden" name="setmeal_id" value="'+mark+'">\n' +
            '                    <input type="hidden" name="prize_type" value="packet">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包名称</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="packet_title"  type="text" name="packet_title" value="" class="form-control form-control-lg" placeholder="请填写红包名称">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +


            '                    <!--task_type-->\n' +
            '                   <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包价值</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="price"  type="number" min=\'1\' step="1" name="price" value="" class="form-control form-control-lg" placeholder="请填写红包价值">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>'+
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包数量</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="ground_num"  type="number" min=\'1\' step="1" name="ground_num" value="" class="form-control form-control-lg" placeholder="请填写红包数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +


            '                  <div class="input-group input-group-lg" >\n' +
            '                        <span class="input-group-addon">创建红包</span>\n' +
            '                        <button class="btn btn-primary" type="button"   onclick="redPacketService()">创建</button>\n' +
            '\n' +
            '\n' +
            '                    </div>'+
            '                    <br>'+
            '\n' +
            '\n' +
            '                   <div class="layui-form-item layui-form-text" id="voucherDisplay" >\n' +
            '                        <div class="layui-input-block">\n' +
            '                            <table class="layui-table" id="activeInfoTable" lay-filter="voucherInfoTable">\n' +
            '                                <thead>\n' +
            '                                <tr>\n' +
            '                                    <th>卡券名称</th>\n' +
            '                                    <th>红包类型</th>\n' +
            '                                    <th>低值金额</th>\n' +
            '                                    <th>消费金额</th>\n' +
            '                                    <th>有效期(单位：天)</th>\n' +
            '                                    <th>赠送数量</th>\n' +

            '                                    <th>操作</th>\n' +
            '                                </tr>\n' +
            '                                </thead>\n' +
            '                                <tbody id="voucherInfo">\n' +
            '\n' +
            '                                </tbody>\n' +
            '                            </table>\n' +
            '                        </div>\n' +
            '                    </div>'+
            '                </div>\n' +
            '\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnRed" >保存</button>\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
        });
    }else{
        $.ajax({
            url: '/admin/setmeal/activeTypeService',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':mark,
                'mtype':'upgrade'

            },
            success: function (res) {


                if (res.status == true) {

                    if (res.data==null) {
                        layer.msg('未获取到商品信息')
                        return
                    }
                    a= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{

                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });
    }


}

/**
 * @context 商品服务  服务新增  会员新增
 * @param mark
 * @param mtype : 管理类型 active :活动管理  task 任务管理
 */
function activeTypeService(type,mark,mtype){
    $.ajax({
        url: '/admin/setmeal/activeTypeService',
        type: 'post',
        dataType: 'json',
        data: {
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:res.title,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/**
 * @context 查询 显示 商品二级分类  服务名称  会员卡直接显示
 * @param mark
 * @param mtype: active 活动管理  task  任务管理
 */
function activeCreateGive(id,type,mark,mtype){
    $.ajax({
        url: '/admin/setmeal/activeCreateGive',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {
            if (res.status == true) {
                if(type == 'member'){

                    layer.closeAll('page'); //关闭所有页面层
                    if(mark == 'type'){
                        $('#activeTypeDisplay').css('display', 'block');
                        $('#activeInfo').html(res.data);
                    }else{
                        $('#activeGiveDisplay').css('display', 'block');
                        $('#activeGiveInfo').html(res.data);
                    }
                }
                else{
                    a= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });
                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 查询 显示 商品名称
 * @param mark
 */
function activeBizproName(id,type,mark,mtype,class_type=1){
    $.ajax({
        url: '/admin/setmeal/activeBizproName',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,
            'classtype':class_type,
        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id
            if (res.status == true) {
                if(type == 'pro'){
                    if(class_type==2){
                        getCheckVoucher(type,bstitle,detail_price,pro_service_id)
                    }else{
                        a= layer.open({
                            type: 1,
                            title:res.title,
                            skin: 'layui-layer-custom', //加上边框
                            area: ['1200px', '600px'], //宽高
                            content: res.data
                        });
                    }

                }else{
                    //选择服务赠送数量
                    getCheckVoucher(type,bstitle,detail_price,pro_service_id)
                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

function upgradeSession(id,type,mark){
    $.ajax({
        url: '/admin/setmeal/bizproService',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,

        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id
            if (res.status == true) {
                getCheckVoucher(type,bstitle,detail_price,pro_service_id)
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}



/*
* @param :type 类型 pro 商品卡券设置 service 服务卡券设置
* @param :bstitle  名称
* @param :detail_price 价值
* @param :pro_service_id  id 卡券  红包id
* @content 公共展示卡券创建页面
* */
function getCheckVoucher(type,bstitle,detail_price,pro_service_id){
    if(type == 'pro'){
        //商品
        var title='商品卡券设置';
    }else{
        //服务
        var title='服务卡券设置';

    }
    ab=layer.open({
        type: 1,
        title:title,
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '\n' +
            '                    <input type="hidden" name="prize_type"  value="'+type+'">\n' +
            '                    <input type="hidden" name="detail_price"  value="'+detail_price+'">\n' +
            '                    <input type="hidden" name="pro_service_id"  value="'+pro_service_id+'">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券名称</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="title"  type="text" name="title" value="'+bstitle+'" class="form-control form-control-lg" placeholder="请填写卡券名称">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '                    <!--task_type-->\n' +
            '                   <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="giving_number"  type="number" min=\'1\' step="1" name="giving_number" value="1" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>'+

            // '                   <div class="input-group input-group-lg">\n' +
            // '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期</span>\n' +
            // '\n' +
            // '                        <div class="form-group input-group-lg">\n' +
            // '                            <input data-verify="required"    type="number" min=\'1\' step="1" name="card_time" value="365" class="form-control form-control-lg" placeholder="请填写有效期（单位：天）">\n' +
            // '                            <i class="form-group__bar"></i>\n' +
            // '\n' +
            // '                        </div>\n' +
            // '\n' +
            // '                    </div>\n' +
            // '                    <br>'+

            '                </div>\n' +
            '\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnPrize" >保存</button>\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
    });
}

//修改数量 有效期
function upgradeTypeWrite(id,k,value,mark,vmark,setMeal_id){
    layer.prompt({
        formType: 0,
        value: value,
        title: '请输入修改数量',
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){


        $.ajax({
            url: '/admin/setmeal/setmealTypeWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'id':id,
                'k':k,
                'value':value,
                'mark':mark,
                'vmark':vmark,
            },
            success: function (data) {
                if (data.status == true) {
                    layer.closeAll();
                    ServiceRelation(setMeal_id);

                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}
// 修改 删除 之后 显示的页面（活动类型  活动赠送）
function getUpgradeList(id){
    $.ajax({
        url: '/admin/setmeal/getUpgradeList',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,

        },
        success: function (res) {
            if (res.status == true) {

                layer.closeAll('page'); //关闭所有页面层

                $('#taskTypeDisplay').css('display', 'block');
                $('#taskInfo').html(res.data);
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
//求会员升级后的等级
function changeLevel(){
    let now_level=$("#now_level option:selected").val();
    $.ajax({
        url:"/admin/setmeal/changeLevel",
        type:'post',
        data:{
            now_level:now_level,
        },
        async:true,
        success:function(res){
            $("#up_level").html(res);
        },
        error:function(res){

        }

    })

}

/*
服务关键词搜索
 */
function searchRelevanceStore() {

    var e =window.event;
    var pid=$("#search_title").attr('data-pid')
    if (e.keyCode === 13) {
        if (pid < 1)
        {
            return
        }


        $.ajax({
            url: '/admin/setmeal/searchservice',
            type: 'post',
            dataType: 'json',
            data: {
                param:{
                    'class_pid':pid,
                    'keywords':$("#search_title").val()
                }

            },
            success: function (res) {

                if (res.status) {

                    // $.each($("#ppp").children("div"),function(key,val){
                    //     $(val).attr('data-show','')
                    //
                    // })

                    $.each(res.data,function(key,val){

                        $(".lump" + val.id).attr('data-show',1)

                    })

                    $.each($("#ppp").children("div"),function(key,val){
                        if ($(val).attr('data-show') == 1||$(val).attr('class')=='form-group') {
                            $(val).css('display','')
                        }else{
                            $(val).css('display','none')
                        }
                        $(val).attr('data-show','')
                    })

                    // res.data.forEach(function(e) {
                    //     console.log($(".lump"));
                    //      var  oldelement = $(".lump" + e.id);
                    //     $("#ppp").children(":eq(1)").before(oldelement);
                    //
                    //
                    // })
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });
    }


}

//创建红包:红包类型
function redPacketService(){

      $.ajax({
        url: '/admin/market/redPacketService',
        type: 'post',
        dataType: 'json',
        data: { },
        success: function (res) {
            if (res.status == true) {
                layer_create_vouchertype =layer.open({
                    type: 1,
                    title:'红包设置 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });


            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
//删除红包中的卡券
function voucherDelInfo(k,mark,type){
    $.ajax({
        url: '/admin/Setmeal/voucherDelInfo',
        type: 'post',
        dataType: 'json',
        data: {
            'k':k,


        },
        success: function (data) {

            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                });
                $('#voucherDetailTr').remove()


            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}
//创建红包卡券：根据商品或服务
function aloneOption_new(id,type,mark,mtype){
    taskSession(id,type,mark,mtype)
}
//创建红包卡券：获取商品或服务详情
function taskSession(id,type,mark,mtype){

    $.ajax({
        url: '/admin/market/bizproService',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id

            if (res.status == true) {
                if(mtype == 'task'){
                    getCheckVoucher(type,bstitle,detail_price,pro_service_id)

                } else if(mtype == 'redPacket'){
                    if(type == 'pro'){
                        //商品
                        showVoucherList(bstitle,3,pro_service_id)

                    }else{
                        //服务
                        showVoucherList(bstitle,4,pro_service_id)


                    }
                }else if(mtype == 'recommend'){
                    presentSet(bstitle,pro_service_id);
                }
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });

}

//修改红包卡券：数量 有效期
function voucherWrite(k,val,vmark,mark,id){
    a=layer.prompt({
        formType: 0,
        value: val,
        title: '请修改信息',
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/setmeal/voucherWrite',
            type: 'post',
            dataType: 'json',
            data: {
                'k':k,
                'val':value,
                'vmark':vmark,
                'mark':mark,
                'id':id,

            },
            success: function (res) {

                if (res.status == true) {
                    layer.close(a);
                    $('#voucherInfo').html(res.data);
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });
    });
}
//根据红包抵用券显示页面
function getCreatePacket(value,voucher_type){

    if(voucher_type == 3 ){
        //商品
        activeTypePrompt('pro','give','redPacket');
        // confirmStore('pro','give','redPacket');

    }else  if(voucher_type == 4){
        //服务
        activeTypePrompt('service','give','redPacket');
        // confirmStore('service','give','redPacket');

    }else if(voucher_type == 7){
        showAddConsumpVoucher(5);
    } else{

        //展示抵用卡券创建页
        showVoucherList(value,voucher_type,0);


    }

}
function flushPacket(){
    $.ajax({
        url: '/admin/market/flushPacket',
        type: 'post',
        dataType: 'json',
        data: {},
        success: function (res) {
            layer.close(layer_create_vouchertype);
            $('#voucherDisplay').css('display', 'block');
            $('#voucherInfo').html(res.str);
        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

function showAddVoucherFirst(active_type,type='',days_id=''){
    let btn=["通用券","分类券"];

    layer.open({
        type:1,
        skin:"prizeconfirm",
        title: "请选择类型",
        content:false,
        area:['200px',"130px"],
        maxWidth:'200px',
        btn: btn,
        btn1: function (index, layero) {
            showAddVoucher(active_type,1,days_id);
            layer.close(index);
            return false

        },
        btn2: function (index, layero) {
            showAddVoucher(active_type,2,days_id);
            layer.close(index);
            return false
        } ,
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
            layer.close();
        }
    });
}



//红包卡券创建
function activeTypePrompt(type,mark,mtype){
    if(type == 'pro'){
        var title ="请输入商品名称搜索";

    }else if(type == 'service'){
        var title ="请输入服务名称搜索";

    }
    layer_searchpro= layer.prompt({
        formType: 0,
        value: '',
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/ServicePro/active_search',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':mark,
                'mtype':mtype,
                'value':value,

            },
            success: function (res) {
                if (res.status == true) {
                    layer_prolist= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
/*@content 展示抵用卡券创建页
* @param value：名称
* @param voucher_type：卡券类型
* @param voucher_id：卡券id (商品id  服务id)
* */
function showVoucherList(value,voucher_type,voucher_id){

      layer_voucher_detail = layer.open({
        type: 1,
        title:'抵用券创建 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
        '\n' +
        '            <div class="card">\n' +
        '\n' +
        '                <div class="card-body">\n' +
        '                    <input type="hidden" name="voucher_over" value="365">\n' +
        '                    <input type="hidden" name="voucher_type" value="'+voucher_type+'">\n' +
        '                    <input type="hidden" name="voucher_id" value="'+voucher_id+'">\n' +
        '                    <input type="hidden" name="session_tag" value="setmeal">\n' +
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券名称</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_title"  type="text" name="voucher_title" value="'+value+'" class="form-control form-control-lg" placeholder="请填写卡券名称">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券类型</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="packet_title"  type="text" name="" value="'+value+'" class="form-control form-control-lg" placeholder="请填写卡券类型" disabled>\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +


        '                    <!--task_type-->\n' +
        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>低值金额</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_price"  type="number" min=\'1\' step="1" name="voucher_price" value="" class="form-control form-control-lg" placeholder="请填写低值金额">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+
        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>消费标准</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="money_off"  type="money_off" min=\'1\' step="1" name="money_off" value="" class="form-control form-control-lg" placeholder="请填写消费标准">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+

        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_number"  type="number" min=\'1\' step="1" name="voucher_number" value="" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +

        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon">是否当天使用</span>\n' +
        '\n' +
        '\n' +
        '                        <div class="input-group-addon">\n' +
        '\n' +
        '                            <div class="input-group">\n' +
        '                                <input type="radio" name="voucher_start" value="1" title="是" checked>\n' +
        '                                <input type="radio" name="voucher_start" value="2" title="否" >\n' +
        '                            </div>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +

        '\n' +
        '                </div>\n' +
        '\n' +
        '            </div>' +
        '            <div class="card">\n' +
        '                <div class="card-body">\n' +
        '                    <div class="btn-demo">\n' +
        '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnVoucher" >保存</button>\n' +
        '\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>'
    });
    lform.render();
}

//查看服务价格  根据车来
function checkServiceCarPrice(id){
    layer.open({
        type: 2,
        title:'服务价格 ',
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['80%', '80%'],
        content: '/admin/biz/servicePrice?service_id='+id
    });

}
var recommend_channel = 0;
function recommendSetmeal(){
    let btn=["推广员","联盟商家","合作门店"];

    layer.open({
        type:1,
        skin:"prizeconfirm",
        title: "请选择办理渠道",
        content:false,
        area:['350px',"130px"],
        maxWidth:'200px',
        btn: btn,
        btn1: function (index, layero) {
            // 推广员
            presentType(1);
            layer.close(index);
            return false

        },
        btn2: function (index, layero) {
            // 联盟商家
            presentType(2);
            layer.close(index);
            return false
        } ,
        btn3: function (index, layero) {
            // 合作门店
            presentType(3);
            layer.close(index);
            return false
        } ,

        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
            layer.close();
        }
    });
}

function presentType(channel){
    recommend_channel = channel;
    let btn=["服务","积分"];
    layer.open({
        type:1,
        skin:"prizeconfirm",
        title: "请选择赠送类型",
        content:false,
        area:['350px',"130px"],
        maxWidth:'200px',
        btn: btn,
        btn1: function (index, layero) {
            // 服务
            activeTypePrompt("service","give","recommend")
            layer.close(index);
            return false

        },
        btn2: function (index, layero) {
            // 积分
            integralSet()
            layer.close(index);
            return false
        } ,

        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
            layer.close();
        }
    });
}

function presentSet(title,id){
    console.log(title,id,recommend_channel)
    layer_voucher_detail = layer.open({
        type: 1,
        title:'抵用券创建 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                    <input type="hidden" name="level_id" value="'+level_id+'">\n' +
            '                    <input type="hidden" name="service_id" value="'+id+'">\n' +
            '                    <input type="hidden" name="present_type" value="1">\n' +
            '                    <input type="hidden" name="recommend_channel" value="'+recommend_channel+'">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>服务名称</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="voucher_title"  type="text" name="voucher_title" value="'+title+'" class="form-control form-control-lg">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>'+

            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="num"  type="number" min=\'1\' step="1" name="num" value="" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="voucher_over"  type="number" min=\'1\' step="1" name="voucher_over" value="" class="form-control form-control-lg" placeholder="请填写卡券有效期">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon">是否当天使用</span>\n' +
            '\n' +
            '\n' +
            '                        <div class="input-group-addon">\n' +
            '\n' +
            '                            <div class="input-group">\n' +
            '                                <input type="radio" name="voucher_start" value="1" title="是" checked>\n' +
            '                                <input type="radio" name="voucher_start" value="2" title="否" >\n' +
            '                            </div>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '\n' +
                '<div class="input-group input-group-lg">\n' +
                    '<span class="input-group-addon"><span class="" style="color:red"></span>上架时间</span>\n' +
                    '<div class="form-group">\n' +
                    '<input type="text" data-time_24hr="true" id="start_time" class="form-control datetime-picker flatpickr-input" name="start_time" value="" placeholder="请填写上架时间" readOnly="readonly">\n' +
                    '<i class="form-group__bar"></i>\n' +
                    '</div>\n' +
                '</div>\n'+
                '\n' +
            '                    <br>\n' +
                '<br>\n' +
                '<div class="input-group input-group-lg">\n' +
                    '<span class="input-group-addon"><span class="" style="color:red"></span>下架时间</span>\n' +
                    '<div class="form-group">\n' +
                    '<input type="text" data-time_24hr="true" id="end_time" class="form-control datetime-picker flatpickr-input" name="end_time" value="" placeholder="请填写上架时间" readOnly="readonly">\n' +
                    '<i class="form-group__bar"></i>\n' +
                    '</div>\n' +
                '</div>\n'+
            '\n' +
            '\n' +
            '\n' +
            '                </div>\n' +
            '\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnRecommend" >保存</button>\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
    });
    $(".layui-layer-custom").css("z-index",4)
    $(".layui-layer-shade").css("z-index",3)
    $(".layui-layer-prompt").css("z-index",2)
    initDatePicker($("#start_time"))
    initDatePicker($("#end_time"))
    lform.render();
}
function integralSet(){
     layer.open({
        type: 1,
        title:'积分创建 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                    <input type="hidden" name="level_id" value="'+level_id+'">\n' +
            '                    <input type="hidden" name="present_type" value="2">\n' +
            '                    <input type="hidden" name="recommend_channel" value="'+recommend_channel+'">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="num"  type="number" min=\'1\' step="1" name="num" value="" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '\n' +
            '<div class="input-group input-group-lg">\n' +
                '<span class="input-group-addon"><span class="" style="color:red"></span>上架时间</span>\n' +
                '<div class="form-group">\n' +
                '<input type="text" data-time_24hr="true" id="start_time" class="form-control datetime-picker flatpickr-input" name="start_time" value="" placeholder="请填写上架时间" readOnly="readonly">\n' +
                '<i class="form-group__bar"></i>\n' +
                '</div>\n' +
            '</div>\n'+
            '\n' +
            '                    <br>\n' +
            '<br>\n' +
            '<div class="input-group input-group-lg">\n' +
                '<span class="input-group-addon"><span class="" style="color:red"></span>下架时间</span>\n' +
                '<div class="form-group">\n' +
                '<input type="text" data-time_24hr="true" id="end_time" class="form-control datetime-picker flatpickr-input" name="end_time" value="" placeholder="请填写上架时间" readOnly="readonly">\n' +
                '<i class="form-group__bar"></i>\n' +
                '</div>\n' +
            '</div>\n'+
            '                </div>\n' +
            '\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnRecommend" >保存</button>\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
    });
    $(".layui-layer-custom").css("z-index",4)
    $(".layui-layer-shade").css("z-index",3)
    $(".layui-layer-prompt").css("z-index",2)
    initDatePicker($("#start_time"))
    initDatePicker($("#end_time"))
    lform.render();
}
function reflushRecommendPresent()
{
    $.ajax({
        url: '/admin/Setmeal/reflushRecommendPresent',
        type: 'post',
        dataType: 'json',
        data: {
            level_id:level_id
        },
        success: function (res) {
            if (res.status == true) {
                $(".relationBlockContent").html(res.data)
                $(".relationBlock").css("display","block")
            }else{
                $(".relationBlockContent").html('')
                $(".relationBlock").css("display","none")
            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
function changeRecommendNum(id,mark,obj)
{
    let num = parseInt($(obj).html());
    layer.prompt({
        formType: 0,
        value: num,
        title: '输入数量',
        area: ['100px',"100px"] //自定义文本域宽高
    }, function(value, index, elem){
        if (isNaN(value)) {
            layer.msg("请输入数字！")
            return;
        }
        $.ajax({
            url: '/admin/Setmeal/changeRecommendNum',
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                num:value,
                mark:mark,
            },
            success: function (res) {
                if (res.status == true) {
                    $(obj).html(value)
                    layer.closeAll();
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}

function delRecommendPresent(obj,id,mark)
{
    layer.confirm('确认删除？',function(){
        $(obj).parents("tr").remove();
        layer.closeAll()
        $.ajax({
            url: '/admin/Setmeal/delRecommendPresent',
            type: 'post',
            dataType: 'json',
            data: {
                id:id,
                mark:mark,
            },
            success: function (res) {
                if (res.status == true) {
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });
    })
}

//手动初始化日期选择插件
function initDatePicker(ele){
    ele.flatpickr({
        altFormat:'Y-m-d H:i:s',
        altInput:true,
        local:'zh',
        enableTime:true,
        time_24hr:true,
    })
}