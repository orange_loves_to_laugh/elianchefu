var audit_merchants_id = 0;
var merchants_obj='';
layui.use(['form', 'table', 'form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    let status = $.getQueryVariable('status')

    table.render({
        elem: '#myTable',
        url: '/admin/JzMerchants/Index?action=ajax&status=' + status,
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            // {type:'checkbox',field:'id'},
            {field: 'title', title: '名称'},
            {field: 'cate_title', title: '分类'},
            {field: 'contacts_phone', title: '联系电话', sort: true},
            {field: 'total_area', title: '地区'},
            {field: 'level_title', title: '是否为优质商家'},
            // {field: 'level_title', title: '商家等级'},
            {field: 'is_type', title: '展示类型', templet: '#is_type'},
            {field: 'commission_bi', title: '抽成比例'},
            // {field: 'balance_pay_title', title: '余额支付'},
            // {field: 'collections_bar', title: '商家收款码',templet:'#collectionsTpl'},
            {field: 'status_title', title: '商家状态'},
            {field: 'setTimeUp', title: '展示状态', templet: '#setTimeUp'},
            {field: 'agent_name', title: '关联代理',},
            {title: '操作', toolbar: '#currentTableBar', width: 400}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit: 10,
        page: true,
        skin: 'line'
    });
    $('#target').distpicker({});
    /*
    添加事件
     */
    $('.layui-btn-container').on('click', '[data-action="add"]', function (re) {
        let btn = ["本地生活", "联盟商家"];

        layer.open({
            type: 1,
            skin: "prizeconfirm",
            title: "请选择商户类型",
            content: false,
            area: ['250px', "130px"],
            maxWidth: '200px',
            btn: btn,
            btn1: function (index, layero) {
                // 选择本地生活
                confirmAdd(1);
                layer.close(index);
                return false

            },
            btn2: function (index, layero) {
                // 选择联盟商家
                confirmAdd(2);
                layer.close(index);
                return false
            },


            cancel: function () {
                //右上角关闭回调
                //return false 开启该代码可禁止点击该按钮关闭
                layer.close();
            }
        });


    })

    function confirmAdd(is_type) {

        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/SaveData?savemark=1&is_type=' + is_type
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    }

    /*
    弹窗管理
    */
    $('.layui-btn-container').on('click', '[data-action="editPopup"]', function (re) {


        let index = layer.open({
            title: '商户弹窗管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/SavePopup'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //任务管理
    $('.layui-btn-container').on('click', '[data-action="task"]', function (re) {

        let index = layer.open({
            title: '商户任务管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/task_index'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //红包管理
    $('.layui-btn-container').on('click', '[data-action="redpacket"]', function (re) {

        let index = layer.open({
            title: '商户弹窗管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/redpacket_detail_index'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    //邮寄物料
    $('.layui-btn-container').on('click', '[data-action="materiel"]', function (re) {

        let index = layer.open({
            title: '邮寄物料',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/merchants_materiel?status=' + 1
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    // 投放卡券
    $('.layui-btn-container').on('click', '[data-action="launchVoucher"]', function (re) {


        let index = layer.open({
            title: '投放卡券',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/market/launch_voucher'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    $("select[name='cate_id']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='is_type']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='level']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur', function (re) {
        $.submitInfo(table);
    })
    $("select[name='province']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='city']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='area']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='setUpTime']").on('change', function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee_sal']").on('change', function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '编辑',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/SaveData?savemark=1&id=' + data.id,//&status='+data.status+'
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;

        }

        if (obj.event === 'cache') {

            var index_cache = layer.open({
                title: '提现记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/CacheLog?id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_cache);
            });
            return false;
        }

        if (obj.event === 'income') {

            var index_income = layer.open({
                title: '收入记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzMerchants/IncomeLog?id='+data.id,
                content: '/admin/JzMerchants/CollectionsLog?mark=income&id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_income);
            });
            return false;
        }

        if (obj.event === 'member') {

            var index_member = layer.open({
                title: '到店用户',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/MemberLog?id=' + data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_member);
            });
            return false;
        }

        if (obj.event === 'showimg') {

            let aa = {
                "title": '商户收款码', //相册标题
                "id": 1, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": '商户收款码',
                        "pid": 2, //图片id
                        "src": data.collections_bar, //原图地址
                        "thumb": data.collections_bar//缩略图地址
                    }
                ]
            }
            layer.photos({
                photos: aa
                , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
        if (obj.event === 'showMerchants') {

            let url = 'http://web.elianchefu.com/#/pages/recommend/index?a=scan&form=merchants&employeeId=0&assignBizId=' + data.id
            let url2 = 'http://web.elianchefu.com/#/pages/index/pay_shop?merchants_id=' + data.id + '&price=0';
            var urlDouyin = ''
            layer.open({
                title: '门店分享码',
                type: 1,
                skin: 'whiteColor', //加上边框
                area: ['900px', '300px'], //宽高
                shadeClose: true,
                content: '<div style="display: flex;color:#000000">\n' +
                    '          <div style="margin-left: 10%; margin-top: 2%;">\n' +
                    '            <div style="width:100%;text-align: center;margin-bottom:10px;">商家推荐码</div>\n' +
                    '            <div class="img"  id="qrcode"></div>\n' +
                    '          </div>\n' +
                    '        <div style="margin-left: 10%; margin-top: 2%;">\n' +
                    '          <div style="width:100%;text-align: center;margin-bottom:10px;">商家收款码</div>\n' +
                    '          <div class="img"  id="qrcode2"></div>\n' +
                    '        </div>\n' +
                    '        <div style="margin-left: 10%; margin-top: 2%;">\n' +
                    '          <div style="width:100%;text-align: center;margin-bottom:10px;">抖音小程序码</div>\n' +
                    '          <div class="img"  id="qrcode3">'+
                    '          </div>\n' +
                    '        </div>\n' +
                    '      </div>'
            });
            makeQrcode('qrcode', url)
            makeQrcode('qrcode2', url2);
            $.ajax({
                url: '/admin/JzMerchants/getQrcode',
                type: 'post',
                data: {id: data.id},
                async: true,
                dataType: 'json',
                success: function (res) {
                    urlDouyin = res.url;
                    $("#qrcode3").html('<img src="'+urlDouyin+'" width="200" height="200" />');
                },
                error: function (re) {
                    console.log(re);
                }
            });
        }
        if (obj.event === 'audit') {
            window.location='/admin/JzMerchants/ActionExcel?id='+data.id
            let button_html="<a class=\"layui-btn layui-btn-normal layui-btn-xs data-count-edit audit_btn\"  lay-event=\"access\">通过</a>\n" +
                "      <a class=\"layui-btn btn-danger  layui-btn-xs  audit_btn \"  lay-event=\"noaccess\">驳回</a>"
            $(this).after(button_html)
            $(this).css("display","none")
            // $(".audit_btn_all").css("display","none")
            // $(".audit_btn").css("display","inline-block")
            // layer.confirm('确定提现么', function (index) {
            //     window.location='/admin/JzMerchants/ActionExcel?id='+data.id
            //     $(".audit_btn_all").css("display","none")
            //     $(".audit_btn").css("display","inline-block")
            //     layer.close(index)
            // });

            return false;

        }
        if(obj.event=='access'){
            console.log(data.id)
            audit_merchants_id = data.id
            merchants_obj = $(this)
            let datas={param:{
                    keywords:'',
                    // notin_merchants:'true',
                }}
            $.ajax({
                url:'/admin/Employee/RelateEmployee',
                type: 'post',
                data:datas,
                async: true,
                dataType: 'json',
                success: function (res) {
                    var index = layer.open({
                        type: 1,
                        title:'请选择需要关联的选项',
                        area: ['800px', '600px'],
                        shade: 0.2,
                        skin: 'layui-layer-custom', //加上边框
                        maxmin: true,
                        shadeClose: true,
                        content: res.data
                    });



                    $(window).on("resize", function () {
                        layer.full(index);
                    });
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                            +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
        }
        if(obj.event=='noaccess'){
            let obj = $(this)
            $.ajax({
                url:'/admin/JzMerchants/auditFail',
                type: 'post',
                data: {
                    id:data.id,
                    agent_id:data.agent_id,
                    mark:'auditfail'
                },
                async: true,
                dataType: 'json',
                success: function (res) {
                    obj.parents('tr').remove()
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                            +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
        }
        if(obj.event=='audit_delete'){
            let obj = $(this)
            $.ajax({
                url:'/admin/JzMerchants/auditFail',
                type: 'post',
                data: {
                    id:data.id,
                    agent_id:data.agent_id,
                    mark:'delete'
                },
                async: true,
                dataType: 'json',
                success: function (res) {
                    obj.parents('tr').remove()
                    return false;
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                            +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                    })
                }
            });
        }


    });


});
/*
关联员工 并提交审核通过
 */
function connect_detail_callback(obj){
    let id = $(obj).attr('data-id')
    $.ajax({
        url:'/admin/JzMerchants/auditSuccess',
        type: 'post',
        data:{
            employee_id :id,
            merchants_id:audit_merchants_id,
        },
        async: true,
        dataType: 'json',
        success: function (res) {
            merchants_obj.parents('tr').remove()
            return false;
        },
        error: function(re) {

            layer.open({
                title: '操作失败',
                content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                    +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
            })
        }
    });

    layer.closeAll();
}

function makeQrcode(id, url) {
    $("#" + id).qrcode({
        render: "canvas",
        text: url,
        width: "200",
        height: "200",
        background: "#ffffff",
        foreground: "#000000",
        src: '/static/img/logo_qrcode.png',
        imgWidth: 40,
        imgHeight: 40,
    });
}
