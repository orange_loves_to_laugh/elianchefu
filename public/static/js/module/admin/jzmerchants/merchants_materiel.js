layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    var lform = form
    let status=$.getQueryVariable('status')
    table.render({
        elem: '#myTable',
        url: '/admin/JzMerchants/merchants_materiel?action=ajax&status='+status,
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {type:'checkbox',field:'check'},
            {field: 'name', title: '收货人姓名',width:300,align: "center"},
            {field: 'phone', title: '收货人手机号',width:300,align: "center"},
            {field: 'materiel_address', title: '收货地址',width:300,align: "center"},

            {field: 'status', title: '状态',width:300,align: "center",templet:'#is_status'},

            {title: '操作', field:'cz',toolbar: '#currentTableBar', width: 300,align: "center"}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });
    $('#target').distpicker({});
    /*
    添加事件
     */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let btn=["本地生活","联盟商家"];

        layer.open({
            type:1,
            skin:"prizeconfirm",
            title: "请选择商户类型",
            content:false,
            area:['250px',"130px"],
            maxWidth:'200px',
            btn: btn,
            btn1: function (index, layero) {
                // 选择本地生活
                confirmAdd(1);
                layer.close(index);
                return false

            },
            btn2: function (index, layero) {
                // 选择联盟商家
                confirmAdd(2);
                layer.close(index);
                return false
            } ,


            cancel: function () {
                //右上角关闭回调
                //return false 开启该代码可禁止点击该按钮关闭
                layer.close();
            }
        });


    })
    function confirmAdd(is_type){

        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/SaveData?is_type='+is_type
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    }

    $('.layui-btn-container').on('click','[data-action="editPopup"]',function(re){
        var checkStatus = layui.table.checkStatus('myTable'); //demo 即为基础参数 id 对应的值

        console.log(checkStatus.data) //获取选中行的数据即 多个行对象
        console.log(checkStatus.data.length) //获取选中行数量，可作为是否有选中行的条件
        console.log(checkStatus.isAll ) //表格是否全选
        if (checkStatus.data.length>0) {
            var idsArray = [];
            for (var i = 0; i < checkStatus.data.length; i++) {
                idsArray.push(checkStatus.data[i].id);
            }
            var ids = idsArray.toString();
            console.log(ids,'ids');
            window.location='/admin/JzMerchants/materielDataExcel?id='+ids

            // $(".layui-form-checked").not('.header').parents('tr').remove();
        }
        else
        {
            layui.layer.alert("请至少选择一行");
        }

    })
    //打印
    $('.layui-btn-container').on('click','[data-action="dispath_print"]',function(re){
        $("#layui-table-page1").css('display','none')
        $("[data-field= 'cz']").css('display','none')
        $("[data-field= 'check']").css('display','none')

        $("#print_content").jqprint({
            importCSS: true,
            noPrintSelector: ".no-print" ,

        });
        // $('#currentTableBar').css('display:none')
        $("[data-field= 'cz']").css('display','block')
        $("[data-field= 'check']").css('display','block')
        $("#layui-table-page1").css('display','block')

    })

    form.on('switch(switchTest)', function (data) {
        console.log(data.value,'邮寄id')

        let switch_value;
        let title;
        let _this=this;
        if(this.checked){
            switch_value=2;
            title="是否邮寄?";
        }else{
            switch_value=1;
            title="是否改成未邮寄?";
        }
        layer.confirm(title, function (index) {
            layer.close(index);
            changeSwitch(switch_value,data.value);
        },function(){
            _this.checked=! _this.checked;
            form.render("checkbox");
        });
    });
    /**
     * @context 改变开关状态
     */
    function changeSwitch(switch_value,id){
        $.ajax({
            url: "/admin/JzMerchants/materielSwitch",
            type: 'post',
            async: true,
            data: {
                value:switch_value,
                id:id
            },
            dataType: 'json',
            success: function (res) {
                return true;
            }
        });
    }
    $("select[name='cate_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='status']").on('change',function (re) {
        $.submitInfo(table);
    })
    //剩余天数筛选
    $("select[name='expire_day']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='area']").on('change',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '编辑',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;

        }
        if (obj.event === 'delete') {

            layer.confirm('真的删除么', function (index) {

                $.ajax({
                    url: '/admin/JzMerchants/materielDelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

        if (obj.event === 'cache') {

            var index_cache = layer.open({
                title: '提现记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/CacheLog?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_cache);
            });
            return false;
        }

        if (obj.event === 'income') {

            var index_income = layer.open({
                title: '收入记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzMerchants/IncomeLog?id='+data.id,
                content: '/admin/JzMerchants/CollectionsLog?mark=income&id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_income);
            });
            return false;
        }

        if (obj.event === 'member') {

            var index_member = layer.open({
                title: '到店用户',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/MemberLog?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_member);
            });
            return false;
        }

        if (obj.event === 'showimg'){

            let aa={
                "title": '商户收款码', //相册标题
                "id": 1, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": '商户收款码',
                        "pid": 2, //图片id
                        "src": data.collections_bar, //原图地址
                        "thumb":data.collections_bar//缩略图地址
                    }
                ]
            }
            layer.photos({
                photos: aa
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
    });




});