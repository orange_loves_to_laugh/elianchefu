layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload=layui.upload
    //声明全局变量form
    lform = form;
    //关联员工
    $('button[data-type="relate_employee"]').on("click",function ()
    {
        let data={param:{
                keywords:'',
                // notin_merchants:'true',
            }}
        $.ajax({
            url:'/admin/Employee/RelateEmployee',
            type: 'post',
            data:data,
            async: true,
            dataType: 'json',
            success: function (res) {
                var index = layer.open({
                    type: 1,
                    title:'请选择需要关联的选项',
                    area: ['800px', '600px'],
                    shade: 0.2,
                    skin: 'layui-layer-custom', //加上边框
                    maxmin: true,
                    shadeClose: true,
                    content: res.data
                });



                $(window).on("resize", function () {
                    layer.full(index);
                });
                return false;
            },
            error: function(re) {

                layer.open({
                    title: '操作失败',
                    content: '<span style="color:red">'+res.responseJSON.errorCode+':</span>'
                    +'<span style="color:#000">'+res.responseJSON.msg+':</span>'
                })
            }
        });



    })

    //初始化显示商家详情
    if ($('#id').val() < 1) {
        $('.baseinfo').css('display','none')
    }
    let val=  ($('input[name="commission"]').val()*100).toFixed(0)

    $('#commission').val(val+'%')
    //抽成比例事件
    $('input[data-name="commission"]').on('blur',function (re) {
        if ($(this).val() > 1) {
            layer.msg('<span style="color:#fff">请填写0到1之间任意数字</span>')

            return
        }

        let val=($(this).val()*100).toFixed(0)
         $('input[name="commission"]').val($(this).val())
         $(this).val(val+'%')
       // console.log(val);
    })
    //进来就执行 显示分类
    getcateDelSession();
    /*
     * @content  监听二级分类联动
     * */
    form.on('select(cate_pid_select)', function(data){
        console.log(data.elem); //得到select原始DOM对象
        console.log(data.value); //得到被选中的值
        console.log(data.othis); //得到美化后的DOM对象
        $.ajax({
            url: "/admin/JzMerchantsCate/DataList?pid="+data.value,
            type: 'post',
            async: true,
            dataType: 'json',
            success: function (res) {
                console.log(res.data,'00')

                $("select[name='cate_id']").html(res.data)
                // $('#classPid').html(res.data);
                lform.render();

                // resolve(res)
            },
            error: function(re) {
                // reject(re)
            }
        });

    });

    form.on('radio(open_toutiao)', function(data){

        if (data.value == 1)
        {

            if(parseInt($("input[name='id']").val())>0){
                return
            }
            changeExpiration('open_toutiao_expiration_start','open_toutiao_expiration','open_toutiao_expiration_date','openToutiao','open_toutiao')
        }else{
            hiddenExpiration('openToutiao')
        }
    });
    form.on('radio(take_video)', function(data){

        if (data.value == 1)
        {
            if(parseInt($("input[name='id']").val())>0){
                return
            }
            changeExpiration('take_video_expiration_start','take_video_expiration','take_video_expiration_date','takeVideo','take_video')
        }else{
            hiddenExpiration('takeVideo')
        }
    });
    form.on('radio(bully_screen)', function(data){

        if (data.value == 1)
        {
            if(parseInt($("input[name='id']").val())>0){
                return
            }
            changeExpiration('bully_screen_expiration_start','bully_screen_expiration','bully_screen_expiration_date','bullyScreen',"bully_screen")
        }else{
            hiddenExpiration('bullyScreen')
        }
    });



   /* $("select[name='cate_pid']").on('change',function (re) {
        console.log(re,'000000000000000000000000')

        let p =new Promise(function (resolve, reject) {
            $.ajax({
                url: "/admin/JzMerchantsCate/DataList?pid="+re.currentTarget.value,
                type: 'post',
                async: true,
                dataType: 'json',
                success: function (res) {
                    resolve(res)
                },
                error: function(re) {
                    reject(re)
                }
            });
        })
        p.then(
            function (res) {

                // if(res.data.length==0)
                // {
                //     layer.msg('未获取到相关子分类',{ icon: 2,time:2000})
                //     return false
                // }



                let option = '<option value="">请选择二级分类</option>'

                for(let i =0; i in res.data;i++){
                    option += '<option value="'+res.data[i].id+'">'+res.data[i].title+'</option>'
                }

                $("select[name='cate_id']").html('')
                $("select[name='cate_id']").html(option)
                form.render();

            },
            function (re) {
                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        )
    })*/
    //省市区三级联动
    // https://www.jq22.com/demo/jQueryDistpicker20160621/
    /*//初始化地图
    let map_center=''
    if ($('select[name="city"]').val().indexOf('市辖区') > 0)
    {
        map_center=$('select[name="province"]').val()

    }else{
        map_center=$('select[name="city"]').val()

    }

    let  map = new BMap.Map("l-map");
    //建立一个自动完成的对象
    var ac = new BMap.Autocomplete(
        {"input" : "suggestId"
            ,"location" : map_center
        });

    $("select[name='province']").on('change',function (re) {
        if ($('select[name="city"]').val().indexOf('市辖区') > 0)
        {
            map_center=$('select[name="province"]').val()
           // map.centerAndZoom(map_center,12);
        }else{
            map_center=$('select[name="city"]').val()
            //map.centerAndZoom(map_center,12);
        }

        //建立一个自动完成的对象
        ac = new BMap.Autocomplete(
            {
                "input" : "suggestId"
                ,"location" : map_center
            });
    })

      //map.centerAndZoom(map_center,12);

    let myValue=$("#suggestId").val();
    if (myValue.length > 0) {
        setPlace()
    }

    //获取坐标
    function G(id) {
        return document.getElementById(id);
    }
    function setPlace(){
        //map.clearOverlays();    //清除地图上所有覆盖物
        function myFun(){


            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
            console.log(pp.lng + "," + pp.lat);
            document.getElementById("positionId").value = pp.lng + "," + pp.lat;

           // map.centerAndZoom(pp, 18);
            //map.addOverlay(new BMap.Marker(pp));    //添加标注
        }
        var local = new BMap.LocalSearch(map, { //智能搜索
            onSearchComplete: myFun
        });

        local.search(myValue);
    }

   // 初始化地图,设置城市和地图级别。
    ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件

        var str = "";
        var _value = e.fromitem.value;
        var value = "";
        if (e.fromitem.index > -1)
        {
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }

        str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

        value = "";
        if (e.toitem.index > -1) {
            _value = e.toitem.value;
            value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
        }
        str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
        G("searchResultPanel").innerHTML = str;
    });

    ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
        var _value = e.item.value;

        myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;

        G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
        G("suggestId").innerHTML =myValue

        $("#suggestId").val(_value.street +  _value.business)

        //$("#suggestId1").val(456)
            ///myValue
        setPlace();
    });

    $("#suggestId").val($("#suggestId").attr('placeholder'))*/

    //图片上传
    $.singleupload(upload,'headImg','imgurlHidden','demoText','imgurl')
    //营业执照图
    $.singleupload(upload,'headImg_license_imgurl','imgurlHidden_license_imgurl','demoText_license_imgurl','imgurl_license_imgurl')

    //视频上传
    $.singleupload(upload,'headImg1','imgurlHidden1','demoText1','imgurl1','video')
    //轮播图多图片上传
    $.multiupload(upload,'multiple_banner','div_slide_show','banner_images')

    //轮播图删除 onclick="delMultipleImgs(this,'banner')"

    $("#div_slide_show").on('click','img',function (re) {

        let elem=$("#banenrinput").find('input#banner_images')

        $.delMultipleImgs($(this),'banner',elem)
    })

    //监听提交
    form.on('submit(saveBtn)', function (data) {



        //发异步，把数据提交给php

        $.ajax({
            url: '/admin/JzMerchants/Save?savemark=1&action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    $("#L_repass").css('display','none');

                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });

    });


});



/*
商家操作
 */
function MerchantsLog(title){
    let id=$("input[name='id']").val()
    let title_arr={

        'member':{title:'到店用户',url:'/admin/JzMerchants/MemberLog?id='+id,},
        'cache':{title:'提现记录',url:'/admin/JzMerchants/CacheLog?id='+id,},
        'collections':{title:'收款记录',url:'/admin/JzMerchants/CollectionsLog?id='+id,},
        'voucher':{title:'商家优惠券',url:'/admin/JzMerchants/VoucherList?id='+id,},
        'recommondproduct':{title:'商家推荐商品',url:'/admin/JzMerchants/RecommondProduct?id='+id,},
        // 'recommondservice':{title:'商家推荐服务',url:'/admin/JzMerchants/RecommondService?id='+id,},
        'card':{title:'推荐会员',url:'/admin/JzMerchants/CardLog?id='+id,},
        'stafflist':{title:'商户员工列表',url:'/admin/JzMerchants/StaffList?id='+id,},
        'commentlist':{title:'商户评论列表',url:'/admin/JzMerchants/CommentList?id='+id,},
    }
    var index_member = layer.open({
        title: title_arr[title].title,
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: title_arr[title].url,
    });
    $(window).on("resize", function () {
        layer.full(index_member);
    });
    return false;
}

/**
 * @context 搜索关联员工
 */
function searchRelevanceStore()
{

    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        let data={param:{
                keywords:search_title,
                // notin_merchants:'true',
            }}
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/Employee/RelateEmployee',
                type: 'post',
                data:data,
                dataType: 'json',
                success: function(res) {

                    if (res.status) {

                        res.datalist.forEach(function(e) {
                            //console.log($(".lump"));
                            var  oldelement = $(".lump" + e.id);


                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

/*
执行关联操作
 */
function connect_detail_callback(obj){

    $('#staffDisplay').css('display', 'block');

    $('input[name="relate_employee"]').val($(obj).attr('data-id'));

    $('#staffTable tbody tr:eq(0) td:eq(0)').html($(obj).attr('data-title'));




    layer.closeAll();

}

function changeExpiration(start_mark,mark,date_mark,class_name,radio_name){
    let radio_val = parseInt($("input[type='radio'][name='"+radio_name+"']:checked").val())
    console.log(radio_val)
    if(radio_val!=1){
        return false
    }
    layer.open({
        type: 1,
        title:'请输入有效期时长 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['60%', '60%'], //宽高
        zIndex:10,
        content:'            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>选择日期</span>\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" type="text" id="start_time"  name="start_time" value=""\n' +
            '                                   class="form-control form-control-lg" readonly >\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                   <br>'+
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>选择时长</span>\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" type="number" min="1" step="1" id="duration_time" value=""\n' +
            '                                   class="form-control form-control-lg"  >\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '                        </div>\n' +
            '                    </div>\n' +
            '                    <br>\n' +
            '                <br>'+
            '                </div>\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button" id="confirmButton">确定</button>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
    });
    $(".layui-layer-custom").css("z-index",4);
    $(".layui-layer-shade").css("z-index",3);
    $("#start_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (data){

        }
    })
    $("#confirmButton").off('click');
    $("#confirmButton").on('click',function(){
        confirmUpdateTime(start_mark,mark,date_mark,class_name);
    })
}
function confirmUpdateTime(start_mark,mark,date_mark,class_name)
{
    let start_time = $("#start_time").val();
    let value = parseInt($("#duration_time").val());
    $.ajax({
        url: '/admin/JzMerchants/changeExpiration',
        type: 'post',
        dataType: 'json',
        data: {
            start_time:start_time,
            'value':value,
        },
        success: function (data) {
            if (data.status) {
                layer.closeAll('page'); //关闭所有页面层
                showExpiration(start_mark,start_time,mark,value,date_mark,data.date,class_name)
            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/**
 *
 * @param mark 时长的class名
 * @param expriation 时长
 * @param date_mark 日期class名
 * @param date   日期
 * @param class_name 总控class名
 */
function showExpiration(start_mark,start_time,mark,expriation,date_mark,date,class_name){
    $("."+start_mark).val(start_time)
    $("."+mark).val(expriation)
    $("."+date_mark).val(date)
    $("."+class_name).css("display","flex")
}

function hiddenExpiration(class_name)
{
    $("."+class_name).css("display","none")
}