layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
    var lform = form
    let status=$.getQueryVariable('status')
    table.render({
        elem: '#myTable',
        url: '/admin/JzMerchants/merchants_expiration?action=ajax&status='+status,
        method: 'post',
        // toolbar: '#toolbarDemo',
        defaultToolbar: [],
        cols: [[
            {type:'checkbox',field:'check'},
            {field: 'title', title: '商户名称',width:150,align: "center"},
            {field: 'cate_title', title: '分类',width:250,align: "center"},
            {field: 'contacts_phone', title: '联系电话',width:150,align: "center"},
            {field: 'total_area', title: '地区',width:250,align: "center"},
            {field: 'relate_employee_title', title: '业务员',width:100,align: "center"},
            {field: 'expire_time', title: '到期时间',width:100,align: "center"},
            {field: 'expire_day', title: '剩余天数',width:100,align: "center",sort: true},
            {field: 'is_type_title', title: '商家类型',width:100,align: "center"},
            {field: 'level_title', title: '商家状态',width:100,align: "center"},

            {title: '操作', field:'cz',toolbar: '#currentTableBar', width: 150,align: "center"}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });
    $('#target').distpicker({});
    /*
    添加事件
     */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let btn=["本地生活","联盟商家"];

        layer.open({
            type:1,
            skin:"prizeconfirm",
            title: "请选择商户类型",
            content:false,
            area:['250px',"130px"],
            maxWidth:'200px',
            btn: btn,
            btn1: function (index, layero) {
                // 选择本地生活
                confirmAdd(1);
                layer.close(index);
                return false

            },
            btn2: function (index, layero) {
                // 选择联盟商家
                confirmAdd(2);
                layer.close(index);
                return false
            } ,


            cancel: function () {
                //右上角关闭回调
                //return false 开启该代码可禁止点击该按钮关闭
                layer.close();
            }
        });


    })
    function confirmAdd(is_type){

        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/SaveData?is_type='+is_type
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    }

    $('.layui-btn-container').on('click','[data-action="editPopup"]',function(re){
        var checkStatus = layui.table.checkStatus('myTable'); //demo 即为基础参数 id 对应的值

        console.log(checkStatus.data) //获取选中行的数据即 多个行对象
        console.log(checkStatus.data.length) //获取选中行数量，可作为是否有选中行的条件
        console.log(checkStatus.isAll ) //表格是否全选
        if (checkStatus.data.length>0) {
            var idsArray = [];
            for (var i = 0; i < checkStatus.data.length; i++) {
                idsArray.push(checkStatus.data[i].id);
            }
            var ids = idsArray.toString();
            console.log(ids,'ids');
            window.location='/admin/JzMerchants/DataExcel?id='+ids

            // $(".layui-form-checked").not('.header').parents('tr').remove();
        }
        else
        {
            layui.layer.alert("请至少选择一行");
        }

    })
    //任务管理
    $('.layui-btn-container').on('click','[data-action="dispath_print"]',function(re){
        $("#layui-table-page1").css('display','none')
        $("[data-field= 'cz']").css('display','none')
        $("[data-field= 'check']").css('display','none')
        $('table.layui-table layui-table-page layui-box layui-laypage layui-laypage-default').addClass('layui-hide');
        $("#print_content").jqprint({
            importCSS: true,
            noPrintSelector: ".no-print" ,

        });
        // $('#currentTableBar').css('display:none')
        $("[data-field= 'cz']").css('display','block')
        $("[data-field= 'check']").css('display','block')
        $("#layui-table-page1").css('display','block')

    })
    //红包管理
    $('.layui-btn-container').on('click','[data-action="redpacket"]',function(re){

        let index = layer.open({
            title: '商户弹窗管理',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzMerchants/redpacket_detail_index'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    $("select[name='cate_id']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='employee']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='is_type']").on('change',function (re) {
        $.submitInfo(table);
    })
    //剩余天数筛选
    $("select[name='expire_day']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='area']").on('change',function (re) {
        $.submitInfo(table);
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index_edit = layer.open({
                title: '编辑',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_edit);
            });
            return false;

        }
        if (obj.event === 'delete') {

            layer.confirm('真的删除么', function (index) {

                $.ajax({
                    url: '/admin/JzMerchants/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }

        if (obj.event === 'cache') {

            var index_cache = layer.open({
                title: '提现记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/CacheLog?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_cache);
            });
            return false;
        }

        if (obj.event === 'income') {

            var index_income = layer.open({
                title: '收入记录',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                // content: '/admin/JzMerchants/IncomeLog?id='+data.id,
                content: '/admin/JzMerchants/CollectionsLog?mark=income&id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_income);
            });
            return false;
        }

        if (obj.event === 'member') {

            var index_member = layer.open({
                title: '到店用户',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzMerchants/MemberLog?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index_member);
            });
            return false;
        }

        if (obj.event === 'showimg'){

            let aa={
                "title": '商户收款码', //相册标题
                "id": 1, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                        "alt": '商户收款码',
                        "pid": 2, //图片id
                        "src": data.collections_bar, //原图地址
                        "thumb":data.collections_bar//缩略图地址
                    }
                ]
            }
            layer.photos({
                photos: aa
                ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
            });
        }
    });




});