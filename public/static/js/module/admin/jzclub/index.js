layui.use(['form', 'table','jquery'], function () {
    var $ = layui.jquery,
        table = layui.table,
        form = layui.form;

    table.render({
        elem: '#myTable',
        url: '/admin/JzClub/Index?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[
            {field: 'club_title', title: '标题'},
            {field: 'club_author', title: '作者'},
            {field: 'create_time', title: '创建时间'},
            {field: 'club_check', title: '阅读量',style: 'cursor: pointer;',
                templet: function (d) {
                    return '<a href="#" ondblclick="changeNum(\'club_check\','+d.id+',this)" style="color: #1e9fff;text-decoration: underline;width:50%;display: block ">' + d.club_check + '</a>';
                }},
            {field: 'club_transmit', title: '转发量',style: 'cursor: pointer;',
                templet: function (d) {
                    return '<a href="#" ondblclick="changeNum(\'club_transmit\','+d.id+',this)" style="color: #1e9fff;text-decoration: underline;width:50%;display: block">' + d.club_transmit + '</a>';
                }},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    $("input[name='keywords']").on('blur',function (re) {

        $.submitInfo(table);
    })
    /*
    banner
      */
    $('.layui-btn-container').on('click','[data-action="clubbanner"]',function(re){
        let index = layer.open({
            title: '俱乐部banner',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/applay/banner?mark=clubbanner'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    /*
     添加事件
      */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzClub/SaveData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑文章',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzClub/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除行么', function (index) {

                $.ajax({
                    url: '/admin/JzClub/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });

    /**
     * toolbar监听事件
     */
    table.on('toolbar(currentTableFilter)', function (obj) {

        if (obj.event === 'add') {  // 监听添加操作
            let index = layer.open({
                title: '添加',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzClub/SaveData'
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
        }

    });


});

function changeNum(mark,id,obj){
    var val = parseInt($(obj).html())
    if(mark == 'club_check'){
        var title = '请修改阅读数量';
    }else if(mark == 'club_transmit'){
        var title = '请修改转发数量';
    }
    layer.prompt({
        formType: 0,
        value:val,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        if(parseInt(value) < parseInt(val)){
            layer.msg('输入的数量不能小于当前数量', {icon: 5, time: 1000} )
            return;
        }

        $.ajax({
            url: '/admin/JzClub/changeNum',
            type: 'post',
            dataType: 'json',
            data: {
                'mark':mark,
                'val':value,
                'id':id,

            },
            success: function (res) {


                if (res.status == true) {
                    layer.closeAll('page'); //关闭所有页面层
                    $(obj).html(value)
                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )
                }
            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}