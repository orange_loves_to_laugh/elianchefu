layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form
        ,upload = layui.upload;

    //百度编辑器
    let ue = UE.getEditor('club_content', {
        // initialFrameWidth: 800,
        initialFrameHeight: 600,
        autoFloatEnabled: true,
        autoHeightEnabled: true,
        elementPathEnabled: true,
        enableAutoSave: true,
    });
    //监听提交
    form.on('submit(saveBtn)', function (data) {

        //发异步，把数据提交给php
        $.ajax({
            url: '/admin/JzClub/Save?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data.field,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });




    });
    //图片上传
    $.singleupload(upload,'headImg','imgurlHidden','demoText','imgurl')
    // let uploadInst = upload.render({
    //     elem: '#headImg',
    //     url: '/admin/JzInfor/uploadfile'+'?table='+table_name+'&id='+id,
    //     size: 500,
    //     field: 'thumb_img',
    //     accept: 'images',
    //     acceptMime: 'image/*',
    //     before: function(obj) {
    //         //预读本地文件示例，不支持ie8
    //         obj.preview(function(index, file, result) {
    //             $('#imgurl').attr('src', result); //图片链接（base64）;
    //         });
    //         this.data = {
    //             'mark': 'img',
    //             'field': 'thumb_img',
    //             'oldSrc': $("#imgurlHidden").val()
    //         };
    //     },
    //     done: function(res) {
    //         //如果上传失败
    //         if (res.status == false) {
    //             return layer.msg('上传失败');
    //         }
    //         //上传成功
    //         //打印后台传回的地址: 把地址放入一个隐藏的input中, 和表单一起提交到后台, 此处略..
    //         // window.parent.uploadHeadImage(res.src);
    //         $("#imgurlHidden").val(res.src);
    //         let demoText = $('#demoText');
    //         demoText.html('<span style="color: #8f8f8f;">上传成功!!!</span>');
    //     },
    //     error: function() {
    //         //演示失败状态，并实现重传
    //         let demoText = $('#demoText');
    //         demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
    //         demoText.find('.demo-reload').on('click', function() {
    //             uploadInst.upload();
    //         });
    //     },
    //
    // });
    //多图上传
    /*
    Dropzone.autoDiscover = false;
    let option={
    url:'/admin/JzInfor/uploadfile',//上传地址
    paramName: "news_thumb",
    autoProcessQueue:true,
    maxFiles:1,//一次性上传的文件数量上限
    dictMaxFilesExceeded: "您一次最多只能上传{{maxFiles}}个文件",
    acceptedFiles: ".png,.gif,.jpg,.jpeg",
    dictInvalidFileType: "你不能上传该类型文件,文件类型只能是*.xls。",
    addRemoveLinks : true,//添加移除文件
    dictCancelUploadConfirmation:'你确定要取消上传吗？',
    dictResponseError: '文件上传失败!',
    dictCancelUpload: "取消上传",
    dictRemoveFile: "移除文件",
    dictDefaultMessage:"拖拉图片文件到这里或者点击",
    uploadMultiple:false,
        // thumbnailWidth: 400,
        // thumbnailHeight: 400,
    resize: function(file) {
        var resizeInfo = {
            // srcX: 0,
            // srcY: 0,
            // trgX: 0,
            // trgY: 0,
            srcWidth: file.width,
            srcHeight: file.height,
            trgWidth: this.options.thumbnailWidth,
            trgHeight: this.options.thumbnailHeight
        };

        return resizeInfo;
    },
    init: function() {
        myDropzone = this; // closure
        //回显图片
        let advert = $("#img_url").val();
        if(advert !=''||advert !=undefined){
            let dataStrArr=advert.split(",");  //分割成字符串数组
            $(dataStrArr).each(function(i,val){
                var img = new Image();
                img.src =val
                img.height = '500px';
                img.width = '500px';

                var mockFile = {
                    name: val, //需要显示给用户的图片名
                    size: 12345, //图片尺寸
                    height:200,
                    width:200,
                    type: '.jpg,.png,.gif'//图片文件类型
                };
                myDropzone.displayExistingFile(mockFile, val, null, null, true);

            });
        }
        //删除图片
        this.on('removedfile',function(re,d){
            let img_url=$("#imgurl").val()
            let arr=img_url.split(',')
            let local = $.inArray(re.src, arr); //根据元素值查找下标，不存在返回-1
            arr.splice(local,1);//根据下标删除一个元素   1表示删除一个元素
            img_url=arr.join(',')
            $("#imgurl").val(img_url)
        })
        //添加图片
        this.on('success',function (file,data) {
            file['src'] = data.src;

            let re=$("#imgurl").val()

            if(re.length<1){
                $("#imgurl").val(data.src)
            }else{
                $("#imgurl").val(re+','+data.src)
            }
            console.log($("#imgurl").val());

        })

    },


};
    let dropz = new Dropzone('div.dropzone',option)
*/
});

