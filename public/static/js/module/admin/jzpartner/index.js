layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    $('#target').distpicker({});
    let create_time=$.getQueryVariable('create_time')
    let url='/admin/JzPartner/Index?action=ajax'
    if (create_time.length > 0)
    {
      url='/admin/JzPartner/Index?action=ajax&create_time='+create_time
    }

    table.render({
        elem: '#myTable',
        url: url,
        method: 'post',
        parseData:function(res){
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[

            {field: 'member_name', title: '昵称'},
            {field: 'phone', title: '手机号'},

            {field: 'total_address', title: '地区'},
            {field: 'create_time', title: '注册日期'},
            {field: 'recommender_name', title: '推荐人',templet:'#recommender_detail'},
            {field: 'recommend_num', title: '推荐人数',templet:'#recommend_list',sort:true},
            {field: 'partner_balance_total', title: '推广收益',sort:true},
            {field: 'partner_balance', title: '收益余额',sort:true},
            {field: 'status_title', title: '账户状态'},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_area']").on('change',function (re) {
        $.submitInfo(table);
    })
    /*
    添加事件
    */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        layer.prompt({
            formType: 0,
            value: '',
            maxlength: 140,
            title:'请输入用户手机号'
        }, function(value, index, elem){
            layer.close(index)
            // if (value.length < 11)
            // {
            // layer.msg('<span style="color: #fff">请输入完整手机号</span>')
            // return false
            // }
            $.ajax({
                url: '/admin/JzPartner/memberlist?phone='+value,
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    //layer.close(index);
                    if (data.status) {
                        var index = layer.open({
                            title: '用户列表',
                            type: 1,
                            shade: 0.2,
                            maxmin: true,
                            shadeClose: true,
                            area: ['50%', '40%'],
                            content: data.data,
                        });
                        $('.layui-layer-content').attr('data-sa-theme',$('body').attr('data-sa-theme'))
                        $(window).on("resize", function () {
                            layer.full(index);
                        });
                        return false;
                    }else{
                        layer.msg(data.msg, {icon: 2, time: 2000});
                    }

                },
                error: function(re) {
                    layer.msg('未获取到相关信息!', {icon: 2, time: 1000});
                }
            });

        });
    })
    /*
    分销设置
     */
    $('.layui-btn-container').on('click','[data-action="partnerset"]',function(re){
        layer.open({
            title: '分销设置',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzPartner/PartnerSet',
        });
        return false;
    })
    /*
   高级合伙人
    */
    $('.layui-btn-container').on('click','[data-action="partnerManage"]',function(re){
        layer.open({
            title: '合伙人列表',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzPartner/partnerManage',
        });
        return false;
    })
    /*
   合伙人统计
    */
    $('.layui-btn-container').on('click','[data-action="partnerDataStatic"]',function(re){
        layer.open({
            title: '合伙人数据统计',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzPartner/partnerDataStatic',
        });
        return false;
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        //查看详情
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/detail?id='+data.member_id,
               // content: '/admin/JzDistribution/DistributionDetail?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'start')
        {
            changestatus(1,data.id)
        }
        if (obj.event === 'stop')
        {
            changestatus(2,data.id)
        }

        //推荐人信息
        if (obj.event === 'recommender_detail')
        {
            console.log(12321)
            var recommender_detail = layer.open({
                title: '推荐人信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/detail?id='+data.partner_recommender_id,
            });
            $(window).on("resize", function () {
                layer.full(recommender_detail);
            });
            return false;
        }



        //推荐人数列表
        if (obj.event === 'recommend_list')
        {
            if (data.recommend_num < 1)
            {
                layer.msg('推荐人数为0', {icon: 2, time: 1000});
                return
            }
            console.log(123)
            var recommend_list = layer.open({
                title: '推荐列表',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzPartner/RecommendList?recommender_id='+data.member_id,
            });

            $(window).on("resize", function () {
                layer.full(recommend_list);
            });
            return false;

        }


    });


    table.on('toolbar(currentTableFilter)', function (obj) {
        if (obj.event === 'activeimgurl')
        {
            layer.open({
                title: '首页活动图',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/homeimg/index?type=4',
            });
            return false;
        }
    });
     function changestatus(status,id){

         let msg='',
            msg_re='';
        if (status<2)
        {
             msg='确认启用么',
                 msg_re='启用';
        }else{
            msg='确认停用么',
                msg_re='停用';
        }
        layer.confirm(msg, function (index) {
            $.ajax({
                url: '/admin/JzPartner/DelInfo',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id,
                    status:status
                },
                success: function(data) {
                    if (data.status) {
                        layer.msg(msg_re+'成功', {icon: 1, time: 1000},function (re) {
                            parent.location.reload();
                        });


                    }else{
                        layer.msg(data.msg, {
                            icon: 2,
                            time: 3000
                        });
                    }
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                    })
                }
            });


            // $(obj)[0].tr.remove()
        });
    }
});

/*
新建分销页面
 */
function setDistributionPage(member_id)
{
    $.ajax({
        url: '/admin/JzPartner/setDistributionPage?member_id='+member_id,
        type: 'post',
        dataType: 'json',
        success: function(data) {
            var index = layer.open({
                title: '创建合伙人',
                type: 1,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['50%', '50%'],
                content: data.data.html,
            });
            $('.layui-layer-content').attr('data-sa-theme',$('body').attr('data-sa-theme'))
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        },
        error: function(re) {

            layer.msg('未获取到相关信息!', {icon: 2, time: 1000});
        }
    });

}

/*
新建分销商
 */
function setDistribution(member_id){
    $.ajax({
        url: '/admin/JzPartner/setDistribution?member_id='+member_id,
        type: 'post',
        dataType: 'json',

        success: function(data) {
            if (data.status) {
                layer.msg('创建成功!', {icon: 1, time: 1000},function (re) {
                    parent.location.reload();
                });
            }
        },
        error: function(re) {

            layer.open({
                title: '创建失败',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    });
}
/*
取消新建分销商
 */
function cancelDistribution(member_id){
    parent.location.reload();
}