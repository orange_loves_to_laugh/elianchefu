var tables;
layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
        tables  =table;
    $('#target').distpicker({});
    let create_time=$.getQueryVariable('create_time')
    let url='/admin/JzPartner/partnerManage?action=ajax'
    if (create_time.length > 0)
    {
      url='/admin/JzPartner/partnerManage?action=ajax&create_time='+create_time
    }
    manageStatic()
    table.render({
        elem: '#myTable',
        url: url,
        method: 'post',
        parseData:function(res){
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[

            {field: 'member_name', title: '昵称'},
            {field: 'phone', title: '手机号'},

            {field: 'total_address', title: '地区'},
            {field: 'create_time', title: '注册日期'},
            {field: 'recommender_name', title: '推荐人',templet:'#recommender_detail'},
            {field: 'recommend_num', title: '推荐人数',templet:'#recommend_list',sort:true},
            {field: 'low_level', title: '团队人数',templet:'#low_level_list',sort:true},
            {field: 'partner_balance_total', title: '推广收益',sort:true},
            {field: 'partner_balance', title: '收益余额',sort:true},
            {field: 'status_title', title: '账户状态'},

            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });

    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_area']").on('change',function (re) {
        $.submitInfo(table);
    })
    /*
    添加事件
    */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        layer.prompt({
            formType: 0,
            value: '',
            maxlength: 140,
            title:'请输入用户手机号'
        }, function(value, index, elem){
            layer.close(index)
            $.ajax({
                url: '/admin/JzPartner/memberlist?phone='+value+"&senior_partner=1",
                type: 'post',
                dataType: 'json',
                success: function(data) {
                    if (data.status) {
                        var index = layer.open({
                            title: '合伙人列表',
                            type: 1,
                            shade: 0.2,
                            maxmin: true,
                            shadeClose: true,
                            area: ['50%', '40%'],
                            content: data.data,
                        });
                        $('.layui-layer-content').attr('data-sa-theme',$('body').attr('data-sa-theme'))
                        $(window).on("resize", function () {
                            layer.full(index);
                        });
                        return false;
                    }else{
                        layer.msg(data.msg, {icon: 2, time: 2000});
                    }

                },
                error: function(re) {
                    layer.msg('未获取到相关信息!', {icon: 2, time: 1000});
                }
            });

        });
    })
    /*
    数据统计
     */
    $('.layui-btn-container').on('click','[data-action="partnerset"]',function(re){
        console.log(123)
        layer.open({
            title: '数据统计',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzPartner/static_list',
        });
        return false;
    })

    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;
        console.log(data)
        //查看详情
        if (obj.event === 'edit') {
            var index = layer.open({
                title: '查看详情',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzPartner/partnerRelation?member_id='+data.member_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'start')
        {
            changestatus(1,data.id)
        }
        if (obj.event === 'stop')
        {
            changestatus(2,data.id)
        }

        //推荐人信息
        if (obj.event === 'recommender_detail')
        {

            var index = layer.open({
                title: '推荐人信息',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/detail?id='+data.partner_recommender_id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;
        }



        //推荐人数列表
        if (obj.event === 'recommend_list')
        {
            if (data.recommend_num < 1)
            {
                layer.msg('推荐人数为0', {icon: 2, time: 1000});
                return
            }
            var index = layer.open({
                title: '推荐列表',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['60%', '50%'],
                content: '/admin/JzPartner/RecommendList?recommender_id='+data.member_id,
            });

            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }
        if (obj.event === 'low_level_list')
        {
            if (data.low_level <=0)
            {
                layer.msg('团队人数为0', {icon: 2, time: 1000});
                return
            }
            var index = layer.open({
                title: '团队列表',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/Member/index?team_id='+data.member_id+"&team_coll="+data.team_coll,
            });

            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }




    });


    table.on('toolbar(currentTableFilter)', function (obj) {
        if (obj.event === 'activeimgurl')
        {
            layer.open({
                title: '首页活动图',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/homeimg/index?type=4',
            });
            return false;
        }
    });
     function changestatus(status,id){

         let msg='',
            msg_re='';
        if (status<2)
        {
             msg='确认启用么',
                 msg_re='启用';
        }else{
            msg='确认停用么',
                msg_re='停用';
        }
        layer.confirm(msg, function (index) {
            $.ajax({
                url: '/admin/JzPartner/DelInfo',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id,
                    status:status
                },
                success: function(data) {
                    if (data.status) {
                        layer.msg(msg_re+'成功', {icon: 1, time: 1000},function (re) {
                            parent.location.reload();
                        });


                    }else{
                        layer.msg(data.msg, {
                            icon: 2,
                            time: 3000
                        });
                    }
                },
                error: function(re) {

                    layer.open({
                        title: '操作失败',
                        content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                    })
                }
            });


            // $(obj)[0].tr.remove()
        });
    }
});

/*
新建分销页面
 */
function setDistributionPage(member_id)
{
    layer.confirm('是否创建该用户为高级合伙人？',{
        btn:['创建',"取消"]
    },function(){
        setDistribution(member_id)
    })

}

/*
新建分销商
 */
function setDistribution(member_id){
    $.ajax({
        url: '/admin/JzPartner/setSeniorPertner?member_id='+member_id,
        type: 'post',
        dataType: 'json',

        success: function(data) {
            if (data.status) {
                layer.msg('创建成功!', {icon: 1, time: 1000},function (re) {
                    layer.closeAll()
                    tables.reload('myTable');
                    manageStatic()
                });
            }
        },
        error: function(re) {

            layer.open({
                title: '创建失败',
                content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
            })
        }
    });
}
/*
取消新建分销商
 */
function cancelDistribution(member_id){
    parent.location.reload();
}

function manageStatic(){
    $.ajax({
        url: '/admin/JzPartner/ManageStatic?',
        type: 'post',
        dataType: 'json',

        success: function(data) {
            if(data){
                let str='';
                data.forEach(function(item){
                    str+=" <div class=\"static-item\">"+item.member_city+item.member_area+"："+item.num+"</div>"
                })
                $(".static-contain").html(str)
            }
        },
        error: function(re) {
        }
    });
}