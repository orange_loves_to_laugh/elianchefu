var tables;
layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;
        tables  =table;
    $('#target').distpicker({});
    let url='/admin/JzPartner/partnerDataStatic?action=ajax'
    table.render({
        elem: '#myTable',
        url: url,
        method: 'post',
        parseData:function(res){
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.data, //解析数据列表
            };
        },
        defaultToolbar: [],
        cols: [[
            {field: 'area', title: '地区',width:200},
            {field: 'num', title: '合伙人数量',sort:true},
            {field: 'nowMonthAdd', title: '本月新增推广员',sort:true},
            {field: 'lastMonthAdd', title: '上月新增推广员',sort:true},
            {field: 'nowCumulative', title: '本月累计消费',sort:true},
            {field: 'lastCumulative', title: '上月累计消费',sort:true},
            {field: 'lowLevelEarn', title: '推广员总收益',sort:true},
            {field: 'nowMonthEarn', title: '本月总收益',sort:true},
            {field: 'lastMonthEarn', title: '上月总收益',sort:true},
            {field: 'relationMember', title: '关联会员数量',sort:true},
            {field: 'nowRelationMember', title: '本月会员新增',sort:true},
            {field: 'lastRelationMember', title: '上月会员新增',sort:true},

        ]],
        page: false,
        skin: 'line'
    });
    let flatpickr_register_time=$("#start_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (){
            flatpickr_register_time.close()
            $.submitInfo(table);
        }
    })
    let flatpickr_member_create=$("#end_time").flatpickr({
        enableTime: false,
        allowInput:true,
        nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
        prevArrow: '<i class="zmdi zmdi-long-arrow-left" />',
        onChange:function (){
            flatpickr_member_create.close()
            $.submitInfo(table);
        }
    })
    /*
    搜索事件
     */
    $("input[name='keywords']").on('blur',function (re) {
        $.submitInfo(table);
    })
    $("select[name='status']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_province']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_city']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='member_area']").on('change',function (re) {
        $.submitInfo(table);
    })

});