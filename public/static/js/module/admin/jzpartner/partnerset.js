let wform=null;
let layer_create_vouchertype=null
let layer_create_voucherdetail=null
let layer_create_vouchersearch=null
let layer_create_vouchersearchprompt=null

let layer_proservice_cate=null

let layer_proservice_list=null

let layer_create_pro=null

let layer_create_service=null

let red_type={
    1:'商品通用抵用券',
    2:'服务通用抵用券',
    3:'单独商品抵用券',
    4:'单独服务抵用券',
    5:'商品服务通用券',
    6:'现金低值通用券',
    7:'商家消费券',
    8:'有派生活消费券',
}
layui.use(['layer', 'form','laydate','upload'], function(){
    var layer = layui.layer
        ,form = layui.form;
        wform =layui.form



    //监听奖励类型
    $("#create_give").on('click',function (re) {

        let btn=["赠送服务","赠送商品","赠送红包"];



        layer.confirm('请选择赠送类型', {
            btn:btn //可以无限个按钮
            ,btn3: function(index, layero){
                //赠送红包
                redPacketService()
                //confirmStore('red');
            }
        }, function(index, layero){
            //赠送服务
            confirmStore('service')
        }, function(index){
            //赠送商品
            confirmStore('pro');
        });
    })

    $("#check_give").on('click',function (re) {
        var index = layer.open({
            title: '查看赠送',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['60%', '50%'],
            content: '/admin/JzPartner/PartnerPrise',
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })

    //执行创建服务奖励
    form.on('submit(saveBtnPrize)', function (data) {
        $.ajax({
            url: '/admin/JzPartner/DoPartnerSet?action=ajax',
            type: 'post',
            dataType: 'json',
            data:{
                type:'prise_info',
                data:data.field
            },
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){

                            layer.close(layer_proservice_cate);
                            layer.close(layer_proservice_list);
                            layer.close(layer_create_pro);
                            layer.close(layer_create_service);

                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
    })

    //创建红包:红包卡券
    form.on('submit(saveBtnVoucher)', function (data){
        //卡券类型
        var voucher_type =data.field.voucher_type
        //当天使用
        var voucher_start =data.field.voucher_start
        //商品或服务id
        var voucher_id =data.field.voucher_id
        //卡券名称
        var voucher_title =data.field.voucher_title
        if( voucher_title == ''){
            layer.msg('卡券名称为空', {icon: 5, time: 1000} )
            return
        }
        // 卡券价格
        var voucher_price =data.field.voucher_price
        if( voucher_price == ''){
            layer.msg('卡券低值金额为空', {icon: 5, time: 1000} )
            return
        }
        //消费标准
        var money_off =data.field.money_off
        if( money_off == ''){
            layer.msg('消费标准为空', {icon: 5, time: 1000} )
            return
        }
        //有效期
        var voucher_over =data.field.voucher_over
        if( voucher_over == ''){
            layer.msg('有效期为空', {icon: 5, time: 1000} )
            return
        }
        // 赠送数量
        var voucher_number =data.field.voucher_number
        if( voucher_number == ''){
            layer.msg('赠送数量为空', {icon: 5, time: 1000} )
            return
        }



        /*
        let tr = $("#activeInfoTable  tr:last");
        let aa= '<tr>\n' +
            '    <td ><input type="hidden" name="voucher_title[]" value="'+voucher_title+'">'+voucher_title+'</td>\n' +
            '    <td><input type="hidden" name="voucher_type[]" value="'+voucher_type+'">'+red_type[voucher_type]+'</td>\n' +
            '    <td><input type="hidden" name="voucher_price[]" value="'+voucher_price+'">'+voucher_price+'</td>\n' +
            '    <td><input type="hidden" name="money_off[]" value="'+money_off+'">'+money_off+'</td>\n' +
            '    <td><input type="hidden" name="voucher_over[]" value="'+voucher_over+'">'+voucher_over+'</td>\n' +
            '    <td><input type="hidden" name="voucher_number[]" value="'+voucher_number+'">'+voucher_number+'</td>\n' +
            '    <td><input  type="hidden" name="voucher_id[]" value="'+voucher_id+'"> <input type="hidden" name="voucher_start[]" value="'+voucher_start+'"><button   onclick="delredvoucher(this)" type="button" class="btn btn-danger">删除</button></td>\n' +
            '</tr>'
        if(tr.length==0){
            layer.alert("指定的table或对应的行数不存在！");
            return;
        }
        tr.after(aa);


         */
        let send_data= {type:'prise_info', data:data.field}
        send_data.data.prize_type='red'
        sendajaxdata(send_data)

    })



    /**
     * 发送数据:成为合伙人赠送
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     */
    function sendajaxdata(data) {
        $.ajax({
            url: '/admin/JzPartner/DoPartnerSet?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            layer.close(layer_create_vouchertype)
                            layer.close(layer_create_voucherdetail)
                            layer.close(layer_create_vouchersearch)
                            layer.close(layer_create_vouchersearchprompt)
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
    }

});
/**
 * 发送数据:除 成为合伙人赠送 之外的其他修改
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function sendajaxdata_other(obj) {

    let title=$(obj).parent('.form-group').siblings('span').html()
    layer.prompt({
        formType: 0,
        value: $(obj).val(),
        title: '请输入'+title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        let data={
            type:$(obj).attr('name'),
            data:value
        }
        $.ajax({
            url: '/admin/JzPartner/DoPartnerSet?action=ajax',
            type: 'post',
            dataType: 'json',
            data:data,
            success: function(res) {
                if (res.status == true) {
                    layer.msg('保存成功', {icon: 1, time: 1000},
                        function(){
                            parent.location.reload();
                        })

                }  else {
                    layer.open({
                        title: '错误信息',
                        content: '<span style="color:red">'+res.code+':</span>'+res.msg
                    })
                    return false
                }
            },
            error: function(re) {

                layer.open({
                    title: '错误信息',
                    content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                })
            }
        });
    });

}
/**
 * 删除红包中的卡券
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function delredvoucher(obj) {
    // let prize_detail=$("input[name='prize_detail']").val()
    //
    // prize_detail=prize_detail.split(',')
    // let data_del=$(obj).attr('data-del')
    //
    //
    // $.each(prize_detail,function(index,val){
    //     if (val == data_del) {
    //
    //     }
    // })
    $(obj).parents('tr').remove();
}
/**
 * 赠送服务：关键词搜索
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function searchRelevanceStore() {

    var e =window.event;
    var pid=$("#search_title").attr('data-pid')
    if (e.keyCode === 13) {

        if (pid < 1)
        {
            return
        }


        $.ajax({
            url: '/admin/setmeal/searchservice',
            type: 'post',
            dataType: 'json',
            data: {
                param:{
                    'class_pid':pid,
                    'keywords':$("#search_title").val()
                }

            },
            success: function (res) {

                if (res.status) {

                    // $.each($("#ppp").children("div"),function(key,val){
                    //     $(val).attr('data-show','')
                    //
                    // })

                    $.each(res.data,function(key,val){

                        $(".lump" + val.id).attr('data-show',1)

                    })

                    $.each($("#ppp").children("div"),function(key,val){
                        if ($(val).attr('data-show') == 1||$(val).attr('class')=='form-group') {
                            $(val).css('display','')
                        }else{
                            $(val).css('display','none')
                        }
                        $(val).attr('data-show','')
                    })

                    // res.data.forEach(function(e) {
                    //     console.log($(".lump"));
                    //      var  oldelement = $(".lump" + e.id);
                    //     $("#ppp").children(":eq(1)").before(oldelement);
                    //
                    //
                    // })
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });
    }


}

/**
 * 设置合伙人奖励
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 * @param    [string]          type [赠送类型]
 */
function confirmStore(type){

    if (type == 'red') {

        layer.open({
            type: 1,
            title:'红包设置 ',
            skin: 'layui-layer-custom', //加上边框
            area: ['100%', '100%'], //宽高
            content: '<form id="form1"  class="layui-form" >\n' +
            '\n' +
            '            <div class="card">\n' +
            '\n' +
            '                <div class="card-body">\n' +
            '                      <input type="hidden" name="prize_type" value="red">' +
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包名称</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="packet_title"  type="text" name="packet_title" value="" class="form-control form-control-lg" placeholder="请填写红包名称">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +


            '                    <!--task_type-->\n' +
            '                   <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包价值</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="price"  type="number" min=\'1\' step="1" name="price" value="" class="form-control form-control-lg" placeholder="请填写红包价值">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>'+
            '                    <div class="input-group input-group-lg">\n' +
            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>红包数量</span>\n' +
            '\n' +
            '                        <div class="form-group input-group-lg">\n' +
            '                            <input data-verify="required" id="ground_num"  type="number" min=\'1\' step="1" name="ground_num" value="" class="form-control form-control-lg" placeholder="请填写红包数量">\n' +
            '                            <i class="form-group__bar"></i>\n' +
            '\n' +
            '                        </div>\n' +
            '\n' +
            '                    </div>\n' +
            '                    <br>\n' +


            '                  <div class="input-group input-group-lg" >\n' +
            '                        <span class="input-group-addon">创建红包</span>\n' +
            '                        <button class="btn btn-primary" type="button"   onclick="redPacketService()">创建</button>\n' +
            '\n' +
            '\n' +
            '                    </div>'+
            '                    <br>'+
            '\n' +
            '\n' +
            '                   <div class="layui-form-item layui-form-text" id="voucherDisplay" >\n' +
            '                        <div class="layui-input-block">\n' +
            '                            <table class="layui-table" id="activeInfoTable" lay-filter="voucherInfoTable">\n' +
            '                                <thead>\n' +
            '                                <tr>\n' +
            '                                    <th>卡券名称</th>\n' +
            '                                    <th>红包类型</th>\n' +
            '                                    <th>低值金额</th>\n' +
            '                                    <th>消费金额</th>\n' +
            '                                    <th>有效期(单位：天)</th>\n' +
            '                                    <th>赠送数量</th>\n' +

            '                                    <th>操作</th>\n' +
            '                                </tr>\n' +
            '                                </thead>\n' +
            '                                <tbody id="voucherInfo">\n' +
            '\n' +
            '                                </tbody>\n' +
            '                            </table>\n' +
            '                        </div>\n' +
            '                    </div>'+
            '                </div>\n' +
            '\n' +
            '            </div>' +
            '            <div class="card">\n' +
            '                <div class="card-body">\n' +
            '                    <div class="btn-demo">\n' +
            '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnRed" >保存</button>\n' +
            '\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '            </div>'
        });
    }
    else{
        $.ajax({
            url: '/admin/setmeal/activeTypeService',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':'give',
                'mtype':'upgrade'

            },
            success: function (res) {


                if (res.status == true) {

                    if (res.data==null) {
                        layer.msg('未获取到商品信息')
                        return
                    }
                    layer_proservice_cate= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });
    }


}


/**
 * 点击服务分类
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 * @param    [string]          type [输入参数]
 */
function activeCreateGive(id,type,mark,mtype){
    $.ajax({
        url: '/admin/setmeal/activeCreateGive',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {
            if (res.status == true) {
                if(type == 'member'){

                    layer.closeAll('page'); //关闭所有页面层
                    if(mark == 'type'){
                        $('#activeTypeDisplay').css('display', 'block');
                        $('#activeInfo').html(res.data);
                    }else{
                        $('#activeGiveDisplay').css('display', 'block');
                        $('#activeGiveInfo').html(res.data);
                    }
                }
                else{
                    layer_proservice_list= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });
                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/**
 * @context 查询 显示 商品名称
 * @param mark
 */
function activeBizproName(id,type,mark,mtype){
    $.ajax({
        url: '/admin/setmeal/activeBizproName',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,
            'mtype':mtype,

        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id
            if (res.status == true) {
                if(type == 'pro'){
                    console.log(res.data);

                    layer_create_pro= layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });
                }else{
                    //选择服务赠送数量
                    getCheckVoucher(type,bstitle,detail_price,pro_service_id)
                }



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/*
* @param :type 类型 pro 商品卡券设置 service 服务卡券设置
* @param :bstitle  名称
* @param :detail_price 价值
* @param :pro_service_id  id 卡券  红包id
* @content 公共展示卡券创建页面
* */
function getCheckVoucher(type,bstitle,detail_price,pro_service_id){
    if(type == 'pro'){
        //商品
        var title='商品卡券设置';
    }else{
        //服务
        var title='服务卡券设置';

    }
    layer_create_service=layer.open({
        type: 1,
        title:title,
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
        '\n' +
        '            <div class="card">\n' +
        '\n' +
        '                <div class="card-body">\n' +
        '\n' +
        '                    <input type="hidden" name="prize_type"  value="'+type+'">\n' +
        '                    <input type="hidden" name="detail_price"  value="'+detail_price+'">\n' +
        '                    <input type="hidden" name="pro_service_id"  value="'+pro_service_id+'">\n' +
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券名称</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="title"  type="text" name="title" value="'+bstitle+'" class="form-control form-control-lg" placeholder="请填写卡券名称">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +
        '                    <!--task_type-->\n' +
        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="giving_number"  type="number" min=\'1\' step="1" name="giving_number" value="1" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+

        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required"   type="number" min=\'1\' step="1" name="card_time" value="1" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+

        '                </div>\n' +
        '\n' +
        '            </div>' +
        '            <div class="card">\n' +
        '                <div class="card-body">\n' +
        '                    <div class="btn-demo">\n' +
        '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnPrize" >保存</button>\n' +
        '\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>'
    });
}
/**
 * 选择赠送商品
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 * @param    [int]          id [商品id]
 * @param    [string]          type [赠送类型：服务 商品 红包]
 * @param    [string]          mark [输入参数]
 */
function upgradeSession(id,type,mark){
    $.ajax({
        url: '/admin/setmeal/bizproService',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
            'mark':mark,

        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id
            if (res.status == true) {
                getCheckVoucher(type,bstitle,detail_price,pro_service_id)
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}

/**
 * 创建红包
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function redPacketService(){

    $.ajax({
        url: '/admin/market/redPacketService',
        type: 'post',
        dataType: 'json',
        data: { },
        success: function (res) {
            if (res.status == true) {
                layer_create_vouchertype =layer.open({
                    type: 1,
                    title:'红包设置 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });


            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

/**
 * 展示红包卡券类型
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 * @param    [string]          value [红包类型名称]
 * @param    [string]          voucher_type [红包类型值]
 */
function getCreatePacket(value,voucher_type){

    if(voucher_type == 3 ){
        //商品
        activeTypePrompt('pro','give','redPacket');
        // confirmStore('pro','give','redPacket');

    }else  if(voucher_type == 4){
        //服务
        activeTypePrompt('service','give','redPacket');
        // confirmStore('service','give','redPacket');

    }else{

        //展示抵用卡券创建页
        showVoucherList(value,voucher_type,0);
    }
}
/**
 * 创建红包卡券：单独商品或单独服务卡券
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 * @param    [string]          value [红包类型名称]
 * @param    [string]          voucher_type [红包类型值]
 */
function activeTypePrompt(type,mark,mtype){
    if(type == 'pro'){
        var title ="请输入商品名称搜索";

    }else if(type == 'service'){
            var title ="请输入服务名称搜索";

    }
     ;

    layer_create_vouchersearchprompt=layer.prompt({
        formType: 0,
        value: '',
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
        $.ajax({
            url: '/admin/ServicePro/active_search',
            type: 'post',
            dataType: 'json',
            data: {
                'type':type,
                'mark':mark,
                'mtype':mtype,
                'value':value,

            },
            success: function (res) {
                if (res.status == true) {
 layer_create_vouchersearch=   layer.open({
                        type: 1,
                        title:res.title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: res.data
                    });


                }else{
                    layer.msg(res.msg, {icon: 5, time: 1000} )

                }


            },
            error: function (err) {
                layer.msg(err);
            }
        });

    });
}
/**
 * 创建红包卡券：除 单独商品单独服务卡券之外的卡券
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function showVoucherList(value,voucher_type,voucher_id){

    layer_create_voucherdetail=layer.open({
        type: 1,
        title:'抵用券创建 ',
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content: '<form id="form1"  class="layui-form" >\n' +
        '\n' +
        '            <div class="card">\n' +
        '\n' +
        '                <div class="card-body">\n' +
        '                    <input type="hidden" name="voucher_type" value="'+voucher_type+'">\n' +
        '                    <input type="hidden" name="voucher_id" value="'+voucher_id+'">\n' +
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券名称</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_title"  type="text" name="voucher_title" value="'+value+'" class="form-control form-control-lg" placeholder="请填写卡券名称">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>卡券类型</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="packet_title"  type="text" name="" value="'+value+'" class="form-control form-control-lg" placeholder="请填写卡券类型" disabled>\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +


        '                    <!--task_type-->\n' +
        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>低值金额</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_price"  type="number" min=\'1\' step="1" name="voucher_price" value="" class="form-control form-control-lg" placeholder="请填写低值金额">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+
        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>消费标准</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="money_off"  type="money_off" min=\'1\' step="1" name="money_off" value="" class="form-control form-control-lg" placeholder="请填写消费标准">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+

        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required" id="voucher_number"  type="number" min=\'1\' step="1" name="voucher_number" value="" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>\n' +

        '                   <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期</span>\n' +
        '\n' +
        '                        <div class="form-group input-group-lg">\n' +
        '                            <input data-verify="required"    type="number" min=\'1\' step="1" name="voucher_over" value="365" class="form-control form-control-lg" placeholder="请填写有效期（单位：天）">\n' +
        '                            <i class="form-group__bar"></i>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '                    <br>'+
        '                    <div class="input-group input-group-lg">\n' +
        '                        <span class="input-group-addon">是否当天使用</span>\n' +
        '\n' +
        '\n' +
        '                        <div class="input-group-addon">\n' +
        '\n' +
        '                            <div class="input-group">\n' +
        '                                <input type="radio" name="voucher_start" value="1" title="是" checked>\n' +
        '                                <input type="radio" name="voucher_start" value="2" title="否" >\n' +
        '                            </div>\n' +
        '\n' +
        '                        </div>\n' +
        '\n' +
        '                    </div>\n' +
        '\n' +
        '\n' +
        '\n' +
        '                </div>\n' +
        '\n' +
        '            </div>' +
        '            <div class="card">\n' +
        '                <div class="card-body">\n' +
        '                    <div class="btn-demo">\n' +
        '                        <button class="btn btn-primary" type="button"  lay-submit lay-filter="saveBtnVoucher" >保存</button>\n' +
        '\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>'
    });
    wform.render();
}

/**
 * 创建红包卡券：获取商品或服务详情
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function aloneOption_new(id,type,mark,mtype){
    taskSession(id,type,mark,mtype)
}

//创建红包卡券：获取商品或服务详情
function taskSession(id,type,mark,mtype){

    $.ajax({
        url: '/admin/market/bizproService',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,
        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id

            if (res.status == true) {
                if(mtype == 'task'){
                    getCheckVoucher(type,bstitle,detail_price,pro_service_id)

                } else if(mtype == 'redPacket'){
                    if(type == 'pro'){
                        //商品
                        showVoucherList(bstitle,3,pro_service_id)

                    }else{
                        //服务
                        showVoucherList(bstitle,4,pro_service_id)


                    }
                }
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });

}

/**
 * 创建红包卡券：商品搜索
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年11月27日10:37:18
 * @desc    description
 */
function searchServiceName() {
    var e =window.event;

    if (e.keyCode === 13) {

        $.ajax({
            url: '/admin/ServicePro/active_search',
            type: 'post',
            dataType: 'json',
            data: {
                'type':$("#search_title").attr('data-type'),
                'mark':'give',
                'mtype':'redPacket',
                'value':$("#search_title").val(),

            },
            success: function (res) {

                if (res.status) {

                    // $.each($("#ppp").children("div"),function(key,val){
                    //     $(val).attr('data-show','')
                    //
                    // })

                    $.each(res.list,function(key,val){

                        $(".service" + val.id).attr('data-show',1)

                    })

                    $.each($("#storebody").find("tr"),function(key,val){
                        if ($(val).attr('data-show') == 1) {
                            $(val).css('display','')
                        }else{
                            $(val).css('display','none')
                        }
                        $(val).attr('data-show','')
                    })

                    // res.data.forEach(function(e) {
                    //     console.log($(".lump"));
                    //      var  oldelement = $(".lump" + e.id);
                    //     $("#ppp").children(":eq(1)").before(oldelement);
                    //
                    //
                    // })
                }

            },
            error: function (err) {
                layer.msg(err);
            }
        });
    }

}


//查看服务价格  根据车来
function checkServiceCarPrice(id){
    layer.open({
        type: 2,
        title:'服务价格 ',
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['80%', '80%'],
        content: '/admin/biz/servicePrice?service_id='+id
    });

}


