layui.use(['form', 'table','form'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table;

    table.render({
        elem: '#myTable',
        url: '/admin/JzNotice/Index?action=ajax',
        method: 'post',

        defaultToolbar: [],
        cols: [[

            {field: 'title', title: '标题'},
            {field: 'accepter_title', title: '显示端口'},
            {field: 'start_time', title: '开始时间'},
            {field: 'end_time', title: '结束时间'},
            {field: 'show_status', title: '状态'},
            {field: 'content', title: '内容',width:500},
            {title: '操作', toolbar: '#currentTableBar', minWidth: 120}
        ]],
        //limits: [10, 15, 20, 25, 50, 100],

        limit:10,
        page: true,
        skin: 'line'
    });


    /*
          添加事件
          */
    $('.layui-btn-container').on('click','[data-action="add"]',function(re){
        let index = layer.open({
            title: '添加',
            type: 2,
            shade: 0.2,
            maxmin: true,
            shadeClose: true,
            area: ['100%', '100%'],
            content: '/admin/JzNotice/SaveData'
        });
        $(window).on("resize", function () {
            layer.full(index);
        });
    })
    /*
    搜索事件
     */
    $("select[name='accepter']").on('change',function (re) {
        $.submitInfo(table);
    })
    $("select[name='show_status']").on('change',function (re) {
        $.submitInfo(table);
    })
    table.on('tool(currentTableFilter)', function (obj) {
        var data = obj.data;

        if (obj.event === 'edit') {
            var index = layer.open({
                title: '编辑通知',
                type: 2,
                shade: 0.2,
                maxmin: true,
                shadeClose: true,
                area: ['100%', '100%'],
                content: '/admin/JzNotice/SaveData?id='+data.id,
            });
            $(window).on("resize", function () {
                layer.full(index);
            });
            return false;

        }

        if (obj.event === 'delete') {

            layer.confirm('真的删除么', function (index) {

                $.ajax({
                    url: '/admin/JzNotice/DelInfo',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: obj.data.id,
                    },
                    success: function(data) {
                        if (data.status) {
                            layer.msg('删除成功!', {icon: 1, time: 1000});
                            $(obj)[0].tr.remove()

                        }else{
                            layer.msg(data.msg, {
                                icon: 2,
                                time: 3000
                            });
                        }
                    },
                    error: function(re) {
                        console.log(re)
                        layer.open({
                            title: '删除失败',
                            content: '<span style="color:red">'+re.responseJSON.errorCode+':</span>'+re.responseJSON.msg
                        })
                    }
                });


                // $(obj)[0].tr.remove()
            });
        }
    });



});