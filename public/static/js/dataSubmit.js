/**
 * @context 表单提交方法
 * @param url
 * @param callback
 * @returns {boolean}
 */
function dataSubmit(url,callback){
    var arr=$("form").serialize();
    let vali_data=$("form").serializeArray();
    let a,b;

    vali_data.forEach((v,k)=>{
        a=$("[name='"+v.name+"']").data("verify");

        if(a!=''&&a!==undefined){
            b=dataVerify(v.value,a);

            if(b.status==false){

                swal({
                    title:'验证错误',
                    text: b.msg,//b.msg,
                    timer: 2000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                });

                return false;
            }
        }
    });



    $.ajax({
        url:url,
        type:'post',
        dataType:'json',
        data:arr,
        success:function(res){
           if (res.status){
               callback(res);
           }else{
               console.log(res)
               if(!res.code){
                   res.code='00000';
               }
               swal({
                   title: '保存失败',
                   text: res.msg+res.code,
                   type: 'error',
                   timer:3000,
                   buttonsStyling: false,
                   confirmButtonClass: 'btn btn-light',
                   background: 'rgba(0, 0, 0, 0.96)'
               });
           }
        }
    });
    return false;
}

var verify={
        required: [/[\S]+/, "必填项不能为空"],
        phone: [/^1\d{10}$/, "请输入正确的手机号"],
        email: [/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/, "邮箱格式不正确"],
        url: [/(^#)|(^http(s*):\/\/[^\s]+\.[^\s]+)/, "链接格式不正确"],
        number: function (e) {
            if (!e || isNaN(e)) return "只能填写数字"
        },
    //(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))
        numbera: [/(([0-9]+.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*.[0-9]+)|([0-9]*[1-9][0-9]*))/, "请填写正确的金额格式"],
        date: [/^(\d{4})[-\/](\d{1}|0\d{1}|1[0-2])([-\/](\d{1}|0\d{1}|[1-2][0-9]|3[0-1]))*$/, "日期格式不正确"],
        identity: [/(^\d{15}$)|(^\d{17}(x|X|\d)$)/, "请输入正确的身份证号"]
}

/**
 * @context 执行验证方法
 * @param data
 * @param mark
 * @returns {*}
 */
function dataVerify(data,mark)
{

    let vali=verify[mark];
    if(vali!=null&&vali!=undefined){

        if(vali[0].test(data)){
            return {status:true};
        }else{
            return {status:false,msg:vali[1]};
        }

    }
    return {status:false}
}

/**
 * @context 开关切换方法
 * @param obj
 * @param config
 * @param confirmCallBack
 */
function switchCheckboxChange(obj,config,confirmCallBack)
{
    let old_change=!config.status;
    let title ='';
    if(obj.checked==true){
        title=config.checktitle;
    }else{
        title=config.canceltitle;
    }
    swal({
        title: title,
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-danger',
        confirmButtonText: '确定',
        cancelButtonClass: 'btn btn-light',
        cancelButtonText:'取消',
        background: 'rgba(0, 0, 0, 0.96)'
    }).then(function(res){
        confirmCallBack()
    }).catch(()=>{
        obj.checked=old_change;
    });
}