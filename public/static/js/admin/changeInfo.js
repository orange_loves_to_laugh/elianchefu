function changeInfo(obj,callback)
{
    let str="<div class=\"col-sm-12\" style=\"margin-top:20px;\">\n";
    obj.module.forEach(function(item,index){
        str+=modleFormat(item)
    })
    str+="</div>\n";
    str+='<div class="col-sm-12" style="margin-top:20px;text-align:center">\n' +
        '<button class="layui-btn layui-btn-normal" id=\'confirmChange\'>确认保存</button>\n'+
        ' </div>' ;
    let confirmOpen = layer.open({
        title: '修改信息',
        type: 1 ,
        shade: 0.2,
        skin: 'layui-layer-custom', //加上边框
        maxmin: true,
        shadeClose: true,
        area:obj.area,
        content: str ,
    });

    $("#confirmChange").click(function(){
        callback();
        layer.close(confirmOpen);
    });
}

function modleFormat(data)
{
    switch(data.type){
        case 'text':
        return '<div class="form-group">\n' +
            '      <input type="text" name="'+data.name+'" id="'+data.id+'" value="'+data.value+'" class="form-control" data-verify="required" autocomplete="off" placeholder="'+data.placeholder+'">\n' +
            '      <i class="form-group__bar"></i>\n' +
            '</div>\n';
            break;
        case 'number':
            return '<div class="form-group">\n' +
                '      <input type="number" name="'+data.name+'" id="'+data.id+'" value="'+data.value+'" min="'+data.min+'" max="'+data.max+'" step="'+data.step+'" class="form-control" data-verify="required" autocomplete="off" placeholder="'+data.placeholder+'">\n' +
                '      <i class="form-group__bar"></i>\n' +
                '</div>\n';
            break;
    }
}