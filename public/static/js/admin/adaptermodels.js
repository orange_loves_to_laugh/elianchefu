var adapter={};
var a;
var b;
var logoWindowStart;
/**
 * @context 显示车辆品牌
 */
function adapterModels()
{
    $.ajax({
        url:'/admin/FileOperation/adapterModelsLogo',
        type:'post',
        dataType:'json',
        data:{},
        success:function(res){
            if(res.status){
                let str=adapterModelsIntegration(res.data);
                var logoWindowStart=layer.open({
                    id:"logoWindow",
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: str
                });
            }
        },
    })
}


/**
 * @context 搜索品牌
 */
function searchLogo(obj)
{
    let e= window.event;
    let search=$(obj).val();
    if(e.keyCode==13){
        $.ajax({
            url:'/admin/FileOperation/adapterModelsLogo',
            type:'post',
            dataType:'json',
            data:{
                search:search
            },
            success:function(res){
                if(res.status){
                    let str=adapterModelsIntegration(res.data,search);
                    $("#logoWindow").html(str);
                }
            },
        })
    }
}
/**
 * @context logo模板整理
 * */
function adapterModelsIntegration(module,search='')
{
    let mark="";
    let str=" <div class=\"car_content\">\n" +
        "                                <div class=\"logo_list\">\n" +
        "                                    <input type=\"text\" id=\"search_logo\" value='"+search+"' class=\"form-control\" autocomplete=\"off\" onkeyup=\"searchLogo(this)\" placeholder=\"请输入品牌名称搜索\">\n" +
        "                                </div>\n" +
        "                                <div class=\"logo_list\">\n" +
        "                                    <div class=\"initial\">选择全部</div>\n" +
        "                                    <div class=\"logo_content\">\n" +
        "                                        <div class=\"car-item ";
    if(adapter[0]!=undefined){
        str+=' active';
    }
    str+="\" onclick=\"allLogo(this)\">选择全部</div>\n" +
        "                                    </div>\n" +
        "                                </div>\n";
    for(let i=0;i<module.length;i++){
        if(mark!=module[i]['initial']){
            if(mark!=""){
                str+="                                    </div>\n" +
                    "                                </div>\n";
            }
            str+="                                <div class=\"logo_list\">\n" +
                "                                    <div class=\"initial\">"+module[i]['initial']+"</div>\n" +
                "                                    <div class=\"logo_content\">\n";
            mark=module[i]['initial'];
        }
        str+= "<div class=\"car-item logo-item car_logo_"+module[i]['id'];
        if((adapter[0]!=undefined)||(adapter[module[i]['id']]!=undefined&&adapter[module[i]['id']]!='')){
            str+=' active';
        }
        str+=" \" data-logo=\""+module[i]['id']+"\" onclick=\"showCarSort(this,"+module[i]['id']+")\">"+module[i]['logo_title']+"</div>\n"
    }
    str+="</div>" +
        "</div>" +
        "</div>";
    return str;
}
/**
 * @context 选择所有品牌
 */
function allLogo(obj)
{
    if($(obj).hasClass("active")){
        // 取消全选
        $(obj).removeClass("active");
        $(".logo-item").removeClass("active");
        adapter={};
    }else{
        // 全选
        $(obj).addClass("active");
        $(".logo-item").addClass("active");
        adapter[0]={0:[0]};
    }
}
/**
 * @context 选择品牌展示系列
 */
function showCarSort(obj,logo_id)
{

    $.ajax({
        url:'/admin/FileOperation/adapterModelsSort',
        type:'post',
        dataType:'json',
        data:{
            logo_id:logo_id
        },
        success:function(res){
            if(res.status){
                let str=adapterModelsSortIntegration(res.data,logo_id);
                a=layer.open({
                    id:"sortWindow",
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: str
                });
            }
        },
    })
}
/**
 * @context 系列模板整理
 *
 */
function adapterModelsSortIntegration(module,logo_id)
{
    let str="                                    <div class=\"logo_content\">\n" +
        "                                        <div class=\"car-item logo_sort_"+logo_id+"_0 ";
    if((adapter[0]!=undefined&&adapter[0]==0)||(adapter[logo_id]!=undefined&&adapter[logo_id][0]==0)||
        (adapter[logo_id]!=undefined&&adapter[logo_id][0]!=undefined)){
        str+=' active';
    }
    str+="\" onclick=\"allSort(this,"+logo_id+")\">选择全部</div>\n";
    for(let i=0;i<module.length;i++){

        str+="<div class=\"car-item car_sort_"+module[i]['id']+" logo_sort_"+logo_id;
        if((adapter[0]!=undefined&&adapter[0]==0)||(adapter[logo_id]!=undefined&&adapter[logo_id][0]==0)||
            (adapter[logo_id]!=undefined&&adapter[logo_id][module[i]['id']]!=undefined)){
            str+=' active';
        }
        str+="\" data-sort=\""+module[i]['id']+"\" onclick=\"showCarGroup(this,"+module[i]['id']+","+logo_id+")\">"+module[i]['sort_title']+"</div>";
    }
    str+="</div>";
    return str;
}

/**
 * @context 选择所有系列
 * @param obj
 * @param logo_id
 */
function allSort(obj,logo_id){
    if($(obj).hasClass("active")){
        // 取消全选
        $(obj).removeClass("active");
        $(".logo_sort_"+logo_id).removeClass("active");
        $(".car_logo_"+logo_id).removeClass("active");
        delete adapter[0];
        adapter[logo_id]={};

    }else{
        // 全选
        $(obj).addClass("active");
        $(".logo_sort_"+logo_id).addClass("active");
        $(".car_logo_"+logo_id).addClass("active");
        adapter[logo_id]={0:[0]};
    }
}
/**
 * @context 选择单个系列，展示该系列所有型号
 * @param obj
 * @param sort_id
 * @param logo_id
 */
function showCarGroup(obj,sort_id,logo_id)
{
    $.ajax({
        url:'/admin/FileOperation/adapterModelsGroup',
        type:'post',
        dataType:'json',
        data:{
            sort_id:sort_id
        },
        success:function(res){
            if(res.status){
                let str=adapterModelsGroupIntegration(res.data,sort_id,logo_id);
                b=layer.open({
                    id:"groupWindow",
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: str
                });
            }
        },
    })
}
/**
 * @context 型号整理
 */
function adapterModelsGroupIntegration(module,sort_id,logo_id)
{
    let str="<div class=\"logo_content\">\n" +
        "                                    <div class=\"car-item sort_"+sort_id+" logo_"+logo_id+" sort_"+sort_id+"_0";
    if((adapter[0]!=undefined&&adapter[0]==0)||(adapter[logo_id]!=undefined&&adapter[logo_id][0]==0)||
        (adapter[logo_id]!=undefined&&adapter[logo_id][sort_id]!=undefined&&adapter[logo_id][sort_id].includes(0))){
        str+=' active';
    }
    str+="\" data-group=\"0\" onclick=\"allGroup(this,"+sort_id+","+logo_id+")\">选择全部</div>\n";
    for(let i=0;i<module.length;i++){
        str+="<div class=\"car-item  sort_"+sort_id+" logo_"+logo_id;
        if((adapter[0]!=undefined&&adapter[0]==0)||(adapter[logo_id]!=undefined&&adapter[logo_id][0]==0)||
            (adapter[logo_id]!=undefined&&adapter[logo_id][sort_id]!=undefined&&(adapter[logo_id][sort_id].includes(module[i]['id'])||adapter[logo_id][sort_id].includes(0)))){
            str+=' active';
        }
        str+="\" data-group=\""+module[i]['id']+"\" onclick=\"chooseCarGroup(this,"+module[i]['id']+","+sort_id+","+logo_id+")\">"+module[i]['grouptitle']+"</div>";
    }
    str+="</div><div style='justify-content: center;height:10%;align-items: center;margin-top: 350px;margin-left: 550px;'><button class=\"layui-btn layui-btn-info \"   onclick='checkbutton()'>确认</button></div>";
    return str;
}
function checkbutton(){
    layer.close(a)
    layer.close(b)
}
/**
 * @context 选择该系列所有车型
 * @param obj
 * @param group_id
 * @param sort_id
 * @param logo_id
 */
function allGroup(obj,sort_id,logo_id)
{
    if($(obj).hasClass("active")){
        // 取消全选
        $(obj).removeClass("active");
        $(".sort_"+sort_id).removeClass("active");
        if($(".car_sort_"+sort_id).hasClass('active')){
            $(".car_sort_"+sort_id).removeClass('active');
        }
        let count1=$(".logo_sort_"+logo_id+".active").length;
        if(count1==0){
            $(".car_logo_"+logo_id).removeClass("active");
        }
        adapter[logo_id][sort_id]=[];
        delete adapter[0];
    }else{
        // 全选
        $(obj).addClass("active");
        $(".sort_"+sort_id).addClass("active");
        if(!$(".car_sort_"+sort_id).hasClass('active')){
            $(".car_sort_"+sort_id).addClass('active');
        }
        $(".car_logo_"+logo_id).addClass("active");
        if(adapter[logo_id]==undefined){
            adapter[logo_id]={};
        }
        if(adapter[logo_id][sort_id]==undefined){
            adapter[logo_id][sort_id]=new Array();
        }
        adapter[logo_id][sort_id]=[0]
    }
}

/**
 * @context 选择单个车型
 * @param obj
 * @param group_id
 * @param sort_id
 * @param logo_id
 */
function chooseCarGroup(obj,group_id,sort_id,logo_id)
{
    if($(obj).hasClass("active")){
        // 取消
        $(obj).removeClass("active");
        $(".sort_"+sort_id+"_0").removeClass("active");
        let count2=$(".sort_"+sort_id+".active").length;
        if(count2==0){
            if($(".car_sort_"+sort_id).hasClass('active')){
                $(".car_sort_"+sort_id).removeClass('active');
            }
        }
        let count1=$("logo_sort_"+logo_id+".active").length;
        if(count1==0){
            $(".car_logo_"+logo_id).removeClass("active");
        }
        delete adapter[0];
        if(adapter[logo_id][sort_id][0]==0){
            // 全部选中状态情况下
            adapter[logo_id][sort_id]=[];
            for(var i=0;i<$(".sort_"+sort_id+".active").length;i++){
                let group_value=parseInt($(".sort_"+sort_id+".active")[i].dataset.group);
                adapter[logo_id][sort_id].push(group_value);
            }
        }else{
            for(var j=0;j<adapter[logo_id][sort_id].length;j++){
                if(adapter[logo_id][sort_id][j]==$(obj).data("group")){
                    adapter[logo_id][sort_id].splice(j,1);
                    break;
                }
            }
        }
    }else{
        // 选中
        $(obj).addClass("active");
        // 上级系列选中
        if(!$(".car_sort_"+sort_id).hasClass('active')){
            $(".car_sort_"+sort_id).addClass('active');
        }
        if(!$(".car_logo_"+logo_id).hasClass("active")){
            $(".car_logo_"+logo_id).addClass("active");
        }
        let count1=$(".sort_"+sort_id).length;
        let count2=$(".sort_"+sort_id+".active").length;
        if(count1==count2+1){
            $(".sort_"+sort_id+"_0").addClass("active");
            adapter[logo_id][sort_id]=[0];
        }else{
            if(adapter[logo_id]==undefined){
                adapter[logo_id]={};
            }
            if(adapter[logo_id][sort_id]==undefined){
                adapter[logo_id][sort_id]=new Array();
            }
            adapter[logo_id][sort_id].push(parseInt($(obj).data("group")));
        }
    }
}

/**
 * @context 查看适配车型
 * @param oe_number
 */
function checkAdapterModels(oe_number)
{
    $.ajax({
        url:'/admin/FileOperation/checkAdapterModels',
        type:'post',
        dataType:'json',
        data:{
            oe_number
        },
        success:function(res){
            if(res.status){
                layer.open({
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }
        },
    })
}