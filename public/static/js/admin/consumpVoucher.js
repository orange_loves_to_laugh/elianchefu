/**
 *
 * @param active_type 1.活动创建 2 任务创建 3 签到 4 大转轮奖品 5红包 6积分兑换
 * @param type
 */
function showAddConsumpVoucher(active_type,type='',days_id='')
{
    var index = layer.open({
        title: '创建消费券',
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: '/admin/Market/consump_voucher?active_types='+active_type+'&type='+type+"&days_id="+days_id,
    });
}
function showAddVoucherFirst(active_type,type='',days_id=''){
    let btn=["通用券","分类券"];

    layer.open({
        type:1,
        skin:"prizeconfirm",
        title: "请选择类型",
        content:false,
        area:['200px',"130px"],
        maxWidth:'200px',
        btn: btn,
        btn1: function (index, layero) {
            showAddVoucher(active_type,1,days_id);
            layer.close(index);
            return false

        },
        btn2: function (index, layero) {
            showAddVoucher(active_type,2,days_id);
            layer.close(index);
            return false
        } ,
        cancel: function () {
            //右上角关闭回调
            //return false 开启该代码可禁止点击该按钮关闭
            layer.close();
        }
    });
}

function showAddVoucher(active_type,type='',days_id='')
{
    var title='汽车服务类';
    if(type==2){
        title += '--分类券';
    }else{
        title += '--通用券(该类型的券只能用于合作店创建的服务)';
    }
    var index = layer.open({
        title: title,
        type: 2,
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['100%', '100%'],
        content: '/admin/Setmeal/add_voucher?active_types='+active_type+'&type='+type+"&days_id="+days_id,
    });
}