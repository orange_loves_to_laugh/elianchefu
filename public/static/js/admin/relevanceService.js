/**
 * @context 获取服务
 * */
var a;
function relevanceService()
{
    let aaa=layer.open({
        type: 2,
        title:'赠送服务',
        skin: 'layui-layer-custom', //加上边框
        area: ['1200px', '600px'], //宽高
        content:'/admin/JzService/aaa'
    });
    return

    let checkStock=$("#giving_service").val();
    $.ajax({
        url:'/admin/FileOperation/getsRelevanceService',
        type:'post',
        dataType:'json',
        data:{
            checkStock
        },
        success:function(res)
        {
            if(res.status){
                a=layer.open({
                    type: 1,
                    title:'赠送服务',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content:res.data
                });
            }else{
                swal({
                    title: '系统提示',
                    text: res.msg,
                    timer:1000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                }).catch(()=>{

                })
            }
        }
    })

}

/**
 * @context 搜索关联门店
 */
function searchRelevanceStore()
{
    var e =window.event;

    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/FileOperation/searchRelevanceStore',
                type: 'post',
                data: {
                    search_title
                },
                dataType: 'json',
                success: function(res) {
                    console.log(res.param);
                    if (res.status) {
                        res.param.forEach(function(e) {
                            let oldelement = $(".lump" + e);
                            //console.log(oldelement);
                            //$("#storebody").empty()
                            // $("#storebody").append(oldelement.html())
                            //console.log($("#storebody").children(".lump" + e).html());
                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

/**
 * @context 全选反选
 * @param obj
 * @param mark
 */
function checkAll(obj,mark)
{
    if($(obj)[0].tagName=="BUTTON"){
        if($(obj).hasClass("checkbutton")){
            // 取消选中
            $(obj).removeClass("checkbutton");
            $("."+$(obj).data("mark")).prop("checked",false);
            $("."+mark).prop("checked",false);
            $(obj).text("全选");
        }else{
            $(obj).addClass("checkbutton");
            // 全选框选中
            $("."+$(obj).data("mark")).prop("checked",true);
            // 每项选中
            $("."+mark).prop("checked",true);
            $(obj).text("取消");
        }
    }else{
        if($(obj).prop("checked")){
            $("."+mark).prop("checked",true);
            $("button[data-mark='checkall']").text("取消");
            $("button[data-mark='checkall']").addClass("checkbutton");
        }else{
            // 取消选中
            $("."+mark).prop("checked",false);
            $("button[data-mark='checkall']").text("全选");
            $("button[data-mark='checkall']").removeClass("checkbutton");
        }
    }
}

/**
 * @context 将选择的门店赋值给隐藏input
 * @param mark
 */
function relevanceOption(mark)
{
    let str=$("."+mark+':checked').map(function () {
        return $(this).val();
    }).get().join(",");
    $("#relevance_store").val(str);
    layer.close(a);
}

/**
 * @context 商品查看关联的门店
 * @param id
 */
function checkRelevanceStore(id)
{
    $.ajax({
        url: '/admin/FileOperation/checkRelevanceStore',
        type: 'post',
        data: {
            id
        },
        dataType: 'json',
        success: function(res) {
            if (res.status) {
                layer.open({
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }
        }
    })
}