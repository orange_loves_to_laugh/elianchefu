define(['baidueditor','dropzone','layer','jquery'],function (UE,dropzone,layer,$) {
    let member={
        init:function(){
            // 百度编辑器
            UE.getEditor('myeditor');
            layer.config({
                path: '/static/vendors/assets/layer/dist/'
            });
            // //layer绑定
            $('#btnSubmit').click(function() {
                layer.confirm('789',function () {
                    alert(666)
                })
            });
            //图片上传
            let option={
                url:'/admin/member/toupload',//上传地址
                paramName: "file",
                autoProcessQueue:true,
                addRemoveLinks : true,//添加移除文件
                dictCancelUploadConfirmation:'你确定要取消上传吗？',
                dictResponseError: '文件上传失败!',
                dictCancelUpload: "取消上传",
                dictRemoveFile: "移除文件",
                uploadMultiple:false,
                init: function() {
                    myDropzone = this; // closure
                    this.on('removedfile',function(re,d){
                        let img_url=$("#imgurl").val()
                        let arr=img_url.split(',')
                        let local = $.inArray(re.src, arr); //根据元素值查找下标，不存在返回-1
                        arr.splice(local,1);//根据下标删除一个元素   1表示删除一个元素
                        img_url=arr.join(',')
                        $("#imgurl").val(img_url)


                    })

                    this.on('success',function (file,data) {
                        file['src'] = data.src;
                        let re=$("#imgurl").val()
                        if(re.length<1){
                            $("#imgurl").val(data.src)
                        }else{
                            $("#imgurl").val(re+','+data.src)
                        }


                    })

                },


            };
            $('#juziupload').dropzone(option);

        },
    };
    member.init()

    return member;




});