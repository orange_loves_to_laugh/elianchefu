/**
 * @context 查询 显示 商品二级分类  服务名称  会员卡直接显示
 * @param mark
 * @param mtype: active 活动管理  task  任务管理
 */
function aloneOption_new(id,type,mark,mtype){

    taskSession(id,type,mark,mtype)

}
/**
 * @context 显示创建服务  商品  页面
 * @param mark
 */
function taskSession(id,type,mark,mtype){

    if(type == 'pro'){
        //商品
        var title='商品设置';

    }else{
        //服务
        var title='服务设置';

    }
    $.ajax({
        url: '/admin/market/bizproService',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'type':type,


        },
        success: function (res) {
            var bstitle = res.bstitle
            var detail_price = res.detail_price
            var pro_service_id = res.pro_service_id
            console.log(res.bstitle);
            console.log(mtype);
            if (res.status == true) {
                    ab=layer.open({
                        type: 1,
                        title:title,
                        skin: 'layui-layer-custom', //加上边框
                        area: ['1200px', '600px'], //宽高
                        content: '<form id="form1"  class="layui-form" >\n' +
                            '\n' +
                            '            <div class="card">\n' +
                            '\n' +
                            '                <div class="card-body">\n' +
                            '\n' +
                            '                    <input type="hidden" name="prize_type"  value="'+type+'">\n' +
                            '                    <input type="hidden" name="id"  value="'+id+'">\n' +
                            '                    <input type="hidden" name="mark"  value="'+mark+'">\n' +
                            '                    <input type="hidden" name="mtype"  value="'+mtype+'">\n' +
                            '                    <input type="hidden" name="detail_price"  value="'+detail_price+'">\n' +
                            '                    <input type="hidden" name="pro_service_id"  value="'+pro_service_id+'">\n' +
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>活动名称</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="active_title"  type="text" name="active_title" value="'+bstitle+'" class="form-control form-control-lg" placeholder="请填写活动名称">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +
                            '                    <!--task_type-->\n' +
                            '                   <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>赠送数量</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="detail_number"  type="number" min=\'1\' step="1" name="detail_number" value="" class="form-control form-control-lg" placeholder="请填写赠送数量">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>'+

                            '\n' +
                            '\n' +
                          /*  '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>上架数量</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="ground_num"  type="number" min=\'1\' step="1" name="ground_num" value="" class="form-control form-control-lg" placeholder="请填写上架数量">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +*/
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>有效期(天)</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="expiration"  type="number" min=\'1\' step="1" name="expiration" value="" class="form-control form-control-lg" placeholder="请填写有效期">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>结算金额-小型车</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="settlement_price1"  type="number" min=\'1\' step="1" name="settlement_price[1]" value="" class="form-control form-control-lg" placeholder="小型车-结算金额">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>结算金额-中型车</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="settlement_price2"  type="number" min=\'1\' step="1" name="settlement_price[2]" value="" class="form-control form-control-lg" placeholder="中型车-结算金额">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +
                            '                    <div class="input-group input-group-lg">\n' +
                            '                        <span class="input-group-addon"><span class="" style="color:red">*</span>结算金额-大型车</span>\n' +
                            '\n' +
                            '                        <div class="form-group input-group-lg">\n' +
                            '                            <input data-verify="required" id="settlement_price3"  type="number" min=\'1\' step="1" name="settlement_price[3]" value="" class="form-control form-control-lg" placeholder="大型车-结算金额">\n' +
                            '                            <i class="form-group__bar"></i>\n' +
                            '\n' +
                            '                        </div>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                    <br>\n' +

                            '\n' +
                            '\n' +
                            '\n' +
                            '                </div>\n' +
                            '\n' +
                            '            </div>' +
                            '            <div class="card">\n' +
                            '                <div class="card-body">\n' +
                            '                    <div class="btn-demo">\n' +
                            '                        <button class="btn btn-primary" type="button"   lay-submit lay-filter="saveBtnPrize" >保存</button>\n' +
                            '\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '            </div>'
                    });

            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
//查看服务价格  根据车来
function checkServiceCarPrice(id){
    layer.open({
        type: 2,
        title:'服务价格 ',
        shade: 0.2,
        maxmin: true,
        shadeClose: true,
        area: ['80%', '80%'],
        content: '/admin/biz/servicePrice?service_id='+id
    });

}
/**
 * @context 搜索服务  商品 名称
 * @params search_title  服务 商品 名称
 * @params activeType  service:服务 type :商品
 * @params mark  type 是活动套餐(类型) 选择 give  活动赠送选择
 */
function searchServiceName()
{
    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        var activeType = $("#activeType").val();
        var mark = $("#mark").val();
        var mtype = $("#mtype").val();
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/ServicePro/searchServiceName',
                type: 'post',
                data: {
                    search_title,
                    activeType,
                    mark,
                    mtype
                },
                dataType: 'json',
                success: function(res) {
                    if (res.status == true) {
                        layer.closeAll()
                        a= layer.open({
                            type: 1,
                            title:res.title,
                            skin: 'layui-layer-custom', //加上边框
                            area: ['80%', '80%'], //宽高
                            content: res.data
                        });
                    }else{
                        layer.msg(res.msg, {icon: 5, time: 1000} )
                    }
                }
            })
        }
    }
}
