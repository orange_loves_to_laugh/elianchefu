
/**
 * @context 获取关联门店
 * */
var a;
function relevanceStore($service='',$directly=false)
{

    let checkStock=$("#relevance_store").val();
    let checkStockPrice=$("#relevance_store_price").val();
    let data={
        checkStock,
        checkStockPrice,
        service:$service,
        is_directly:$directly
    }

    $.ajax({
        url:'/admin/FileOperation/getsRelevanceStore',
        type:'post',
        dataType:'json',
        data:data,
        success:function(res)
        {
            if(res.status){
                a=layer.open({
                    type: 1,
                    title:'关联门店',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }else{
                swal({
                    title: '系统提示',
                    text: res.msg,
                    timer:1000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                }).catch(()=>{

                })
            }
        }
    })

}

/**
 * @context 搜索关联门店
 */
function searchRelevanceStore()
{

    var e =window.event;
    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/FileOperation/searchRelevanceStore',
                type: 'post',
                data: {
                    search_title
                },
                dataType: 'json',
                success: function(res) {

                    if (res.status) {
                        res.param.forEach(function(e) {
                            let oldelement = $(".lump" + e);
                            //console.log(oldelement);
                            //$("#storebody").empty()
                           // $("#storebody").append(oldelement.html())
                            //console.log($("#storebody").children(".lump" + e).html());
                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}

/**
 * @context 全选反选
 * @param obj
 * @param mark
 */
function checkAll(obj,mark)
{
    if($(obj)[0].tagName=="BUTTON"){
        if($(obj).hasClass("checkbutton")){
            // 取消选中
            $(obj).removeClass("checkbutton");
            $("."+$(obj).data("mark")).prop("checked",false);
            $("."+mark).prop("checked",false);
            $(obj).text("全选");
        }else{
            $(obj).addClass("checkbutton");
            // 全选框选中
            $("."+$(obj).data("mark")).prop("checked",true);
            // 每项选中
            $("."+mark).prop("checked",true);
            $(obj).text("取消");
        }
    }else{
        if($(obj).prop("checked")){
            $("."+mark).prop("checked",true);
            $("button[data-mark='checkall']").text("取消");
            $("button[data-mark='checkall']").addClass("checkbutton");
        }else{
            // 取消选中
            $("."+mark).prop("checked",false);
            $("button[data-mark='checkall']").text("全选");
            $("button[data-mark='checkall']").removeClass("checkbutton");
        }
    }
}

/**
 * @context 关联获取id 字符串
 * @param mark
 */
function relevanceOption(mark)
{
    //全选的 id str
    let str=$("."+mark+':checked').map(function () {

        return $(this).val();
    }).get().join(",");
    if(str == ''){
        layer.msg('请选择商品', {icon: 5, time: 1000} )
        return
    }
    $.ajax({
        url: '/admin/FileOperation/bizproSession',
        type: 'post',
        dataType: 'json',
        data: {
            'value':str,

        },
        success: function (res) {
            if (res.status == true) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        // layer.closeAll();
                        // searchName(pro,search_title,select_value)
                    })

            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
/**
 * @context 将选择的门店赋值给隐藏input
 * @param mark
 */
function relevanceOptionService(mark)
{

    var  str=$("."+mark+':checked').map(function () {
        return $(this).val();
    }).get().join(",");
    console.log(str,'141111111111111')

    let str_price=$("."+mark+':checked').map(function () {
        let dprice=$("input[name='settlement_dprice"+$(this).val()+"']").val()
        let zprice=$("input[name='settlement_zprice"+$(this).val()+"']").val()
        let xprice=$("input[name='settlement_xprice"+$(this).val()+"']").val()
        if(dprice == ''){
            dprice = 0;
        }
        if(zprice == ''){
            zprice = 0;
        }
        if(xprice == ''){
            xprice = 0;
        }
       /* if (Number(dprice)<1)
        {
            layer.msg('请输入大型车结算价格', {icon: 2});
            return false
        }
        if (Number(zprice)<1)
        {
            layer.msg('请输入中型车结算价格', {icon: 2});
            return false
        }
        if (Number(xprice)<1)
        {
            layer.msg('请输入小型车结算价格', {icon: 2});
            return false
        }*/

        return $(this).val()+'-'+'3d'+'-'+dprice+','+$(this).val()+'-'+'2z'+'-'+zprice+','+$(this).val()+'-'+'1x'+'-'+xprice
    }).get().join(",");


    $.ajax({
        url: '/admin/FileOperation/decideServiceBiz',
        type: 'post',
        data: {
            str,
            str_price

        },
        dataType: 'json',
        success: function(res) {
            if (res.status) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        $("#relevance_store").val(res.bizid_str);
                        $("#relevance_store_price").val(res.bizid_strprice);
                        //服务关联门店数量赋值
                        $("#relationServiceNum").val('已关联:'+res.count);
                        layer.close(a);

                        // layer.closeAll();
                        // searchName(pro,search_title,select_value)
                    })
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000})
            }
        }
    })

}


/**
 * @context 商品查看关联的门店
 * @param id
 */
function checkRelevanceStore(id)
{
    $.ajax({
        url: '/admin/FileOperation/checkRelevanceStore',
        type: 'post',
        data: {
            id
        },
        dataType: 'json',
        success: function(res) {
            if (res.status) {
                layer.open({
                    type: 1,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }
        }
    })
}
//关联门店删除
function bizGoodsDel(k,id,mark,pro_id)
{
    $.ajax({
        url: '/admin/FileOperation/bizGoodsDel',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'k':k,
            'mark':mark,
            'pro_id':pro_id,

        },
        success: function (data) {
            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                },function(){
                    $('.lump' + id).remove();
                });


            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}
//服务关联门店删除
function bizServiceDel(k,id,mark,service_id='',bizid='')
{
    let relevance_store_price = $("#relevance_store_price").val();
    if(mark == 'DB'){
        var biz_id = bizid;
    }else{
        var biz_id = $("#relevance_store").val();

    }
    var bid_count = $("#relevance_store").val();
    //获取input 中的门店id 数量

    $.ajax({
        url: '/admin/FileOperation/bizServiceDel',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'k':k,
            'mark':mark,
            'service_id':service_id,
            'biz_id_price':relevance_store_price,
            'biz_id':biz_id,
            'biz_id_count':bid_count,

        },
        success: function (data) {
            console.log(data.biz_id_price,'00')
            console.log(data,'001111')
            if (data.status == true) {
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                },function(){
                    $('.lump' + id).remove();
                    if(mark == 'S'){
                        $("#relevance_store_price").val(data.biz_id_price);
                        $("#relevance_store").val(data.biz_id);


                    }
                    //服务关联门店数量赋值
                    $("#relationServiceNum").val('已关联:'+data.count);

                });


            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}



//服务关联门店修改
function bizServiceWrite(id,price,car,mark,service_id='',k,bizid=''){

    var val = $('input[name=settlement_dprice'+k+''+mark+''+id+']').val();

    //input 隐藏的门店价格
    let relevance_store_price = $("#relevance_store_price").val();
    if(mark == 'DB'){
       var biz_id = bizid;
    }else{
        //input 隐藏的门店id
       var biz_id = $("#relevance_store").val();
    }


    $.ajax({
        url: '/admin/FileOperation/bizServiceWrite',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'price':price,
            'car':car,
            'mark':mark,
            'service_id':service_id,
            'biz_id_price':relevance_store_price,
            'biz_id':biz_id,
            'value':val,

        },
        success: function (data) {
            console.log(data.biz_id_price,'00')
            console.log(data,'001111')
            if (data.status == true) {
                layer.msg('修改成功!', {
                    icon: 1,
                    time: 1000
                },function(){
                    // $('.lump' + id).remove();
                    if(mark == 'S'){
                        $("#relevance_store_price").val(data.biz_id_price);
                        $("#relevance_store").val(data.biz_id);
                    }

                });

                // ServiceRelationPrice();
                // layer.closeAll();
            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });

}
/**
 * @context 门店已关联列表  服务关联门店
 * @param id
 */
function ServiceRelationPrice(serviceId=''){
    //获取所有关联的id   和 对应的结算价格
    //门店结算价格字符串
    let biz_id_price=$("input[name='biz_id_price']").val()
    //门店id
    let biz_id=$("input[name='biz_id']").val()
    console.log(biz_id_price,'biz_id_price');
    console.log(biz_id,'biz_id');
    $.ajax({
        url: '/admin/FileOperation/ServiceRelationPrice',
        type: 'post',
        dataType: 'json',
        data: {
            biz_id_price,
            biz_id,
            serviceId

        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:'关联门店列表 ',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });



            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
/**
 * @context 单独关联获取id 字符串
 * @param id
 * @param pro  商品标识
 * @param search_title 搜索的名称
 * @param select_value 筛选分类id
 */
function aloneOption(id)
{

    $.ajax({
        url: '/admin/FileOperation/bizproSession',
        type: 'post',
        dataType: 'json',
        data: {
            'value':id,

        },
        success: function (res) {
            if (res.status == true) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        // layer.closeAll();
                        //打开已关联列表
                        // location.reload();
                    })

            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });
}

//服务单独关联门店
function aloneOptionService(id){
    console.log(id,'id');
        let dprice=$("input[name='settlement_dprice"+id+"']").val()
        let zprice=$("input[name='settlement_zprice"+id+"']").val()
        let xprice=$("input[name='settlement_xprice"+id+"']").val()
        console.log(dprice,'price')
        if (Number(dprice)<1)
        {
            layer.msg('请输入大型车结算价格', {icon: 2});
            return
        }
        if (Number(zprice)<1)
        {
            layer.msg('请输入中型车结算价格', {icon: 2});
            return
        }
        if (Number(xprice)<1)
        {
            layer.msg('请输入小型车结算价格', {icon: 2});
            return
        }
    var str_price = id+'-'+'3d'+'-'+dprice+','+id+'-'+'2z'+'-'+zprice+','+id+'-'+'1x'+'-'+xprice
    console.log(str_price);
    $.ajax({
        url: '/admin/FileOperation/decideServiceBiz',
        type: 'post',
        data: {
            str:id,
            str_price
        },
        dataType: 'json',
        success: function(res) {
            if (res.status) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        $("#relevance_store").val(res.bizid_str);
                        $("#relevance_store_price").val(res.bizid_strprice);
                        //服务关联门店数量赋值
                        $("#relationServiceNum").val('已关联:'+res.count);
                        layer.close(a);

                        // layer.closeAll();
                        // searchName(pro,search_title,select_value)
                    })
            }else{
                layer.msg(res.msg, {icon: 5, time: 1000})
            }
        }
    })

}





/**
 * @context 获取关联商品
 * */
function relevanceBizpro(mark)
{
    let checkStock=$("#relevance_store").val();
    let checkStockPrice=$("#relevance_store_price").val();

    $.ajax({
        url:'/admin/FileOperation/getsRelevanceBizpro',
        type:'post',
        dataType:'json',
        data:{
            checkStock,
            checkStockPrice,
            mark
        },
        success:function(res)
        {
            if(res.status){
                a=layer.open({
                    type: 1,
                    title:'关联商品',
                    skin: 'layui-layer-custom', //加上边框
                    area: ['1200px', '600px'], //宽高
                    content: res.data
                });
            }else{
                swal({
                    title: '系统提示',
                    text: res.msg,
                    timer:1000,
                    showConfirmButton: false,
                    buttonsStyling: false,
                    background: 'rgba(0, 0, 0, 0.96)'
                }).catch(()=>{

                })
            }
        }
    })

}
/**
 * @context 单独关联获取id 字符串
 * @param id
 * @param pro  商品标识
 * @param search_title 搜索的名称
 * @param select_value 筛选分类id
 */
var a
function aloneOptionPro(id,mark)
{
        //商品
        a=layer.prompt({
            formType: 0,
            value: '',
            title: "请输入商品数量",
            area: ['800px', '350px'] //自定义文本域宽高
        }, function(value, index, elem){


            $.ajax({
                url: '/admin/FileOperation/bizProInfoSession',
                type: 'post',
                dataType: 'json',
                data: {
                    'value':id,
                    'pro_num':value,
                    'mark':mark,
                },
                success: function (res) {
                    if (res.status == true) {
                        layer.msg('关联成功！', {icon: 1, time: 1000},
                            function(){
                                layer.closeAll();

                                //relevanceBizpro(mark);
                                //打开已关联列表
                                // location.reload();
                            })

                    }else {
                        layer.msg(res.msg, {icon: 5, time: 1000})
                    }

                },
                error: function (err) {
                    layer.msg(err);
                }
            });

        });


}
/**
 * @context 关联获取商品id 字符串
 * @param mark
 * @param protype 1 商品 2 配件 3 耗材  商品类型
 */
function relevanceOptionPro(mark,protype)
{
    //全选的 id str
    let str=$("."+mark+':checked').map(function () {
        console.log($(this).val(),'5555')
        return $(this).val();
    }).get().join(",");
    if(str == ''){
        layer.msg('请选择商品', {icon: 5, time: 1000} )
        return
    }
    aloneOptionPro(str,protype)
   /* $.ajax({
        url: '/admin/FileOperation/bizProInfoSession',
        type: 'post',
        dataType: 'json',
        data: {
            'value':str,

        },
        success: function (res) {
            if (res.status == true) {
                layer.msg('关联成功！', {icon: 1, time: 1000},
                    function(){
                        // layer.closeAll();
                        // searchName(pro,search_title,select_value)
                    })

            }else {
                layer.msg(res.msg, {icon: 5, time: 1000})
            }

        },
        error: function (err) {
            layer.msg(err);
        }
    });*/

}
/**
 * @context 搜索关联门店
 * @params  protype   1 商品2 配件 3 耗材
 */
function searchRelevanceBizpro(protype)
{
    var e =window.event;

    if (e.keyCode === 13) {
        let search_title = $("#search_title").val();
        if (search_title != '' && search_title != undefined) {
            $.ajax({
                url: '/admin/FileOperation/searchRelevanceBizpro',
                type: 'post',
                data: {
                    search_title,
                    protype:protype
                },
                dataType: 'json',
                success: function(res) {
                    console.log(res.param);
                    if (res.status) {
                        res.param.forEach(function(e) {
                            let oldelement = $(".lump" + e);
                            //console.log(oldelement);
                            //$("#storebody").empty()
                            // $("#storebody").append(oldelement.html())
                            //console.log($("#storebody").children(".lump" + e).html());
                            $("#storebody").children(":first").before(oldelement);

                        })
                    }
                }
            })
        }
    }
}
//关联商品删除 protype 1 商品 2 配件 3 耗材
function logServiceProDel(k,id,mark,service_id,protype,obj)
{
    $.ajax({
        url: '/admin/FileOperation/logServiceProDel',
        type: 'post',
        dataType: 'json',
        data: {
            'id':id,
            'k':k,
            'mark':mark,
            'service_id':service_id,
            'protype':protype,

        },
        success: function (data) {
            if (data.status == true) {
                $(obj).parents("tr").remove();
                layer.msg('已删除!', {
                    icon: 1,
                    time: 1000
                },function(){

                });


            }
        },
        error: function (err) {
            layer.msg(err);
        }
    });


}
//修改商品数量  protype 1 商品 2 配件 3 耗材

function logServiceProWrite(id,k,value,mark,vmark,protype,obj){
    if(vmark == 6){
        var title="请输入商品数量";

    }
    value = parseFloat($(obj).html());
    var index = layer.prompt({
        formType: 0,
        value: value,
        title: title,
        area: ['800px', '350px'] //自定义文本域宽高
    }, function(value, index, elem){
                if(vmark == 6){
                    if (isNaN(value)) {
                        layer.msg('请输入数字！')
                        return;
                    }
                }
                $.ajax({
                    url: '/admin/FileOperation/logServiceProWrite',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        'id':id,
                        'k':k,
                        'value':value,
                        'mark':mark,
                        'vmark':vmark,
                        'protype':protype,
                    },
                    success: function (data) {
                        if (data.status == true) {
                            $(obj).html(value)
                            layer.close(index);
                        }
                    },
                    error: function (err) {
                        layer.msg(err);
                    }
                });
            });
}
/**
 * @context 商品已关联列表
 * @param id  服务id
 */
function ServiceRelationPro(id,mark){
    var title = '关联商品列表';
    $.ajax({
        url: '/admin/FileOperation/ServiceRelationPro',
        type: 'post',
        dataType: 'json',
        data: {
            id,
            mark
        },
        success: function (res) {
            if (res.status == true) {
                a= layer.open({
                    type: 1,
                    title:title,
                    skin: 'layui-layer-custom', //加上边框
                    area: ['100%', '100%'], //宽高
                    content: res.data
                });


            }else{
                layer.msg(res.msg, {icon: 5, time: 1000} )

            }


        },
        error: function (err) {
            layer.msg(err);
        }
    });
}
