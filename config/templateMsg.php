<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/25
 * Time: 9:18
 */
return array(
    /*会员办理成功所需的模板消息内容*/
    'level' => array(
        'first' => '恭喜您成为E联车服的会员!!',
        'level1' => '这里是初级体验的会员专享福利',
        'level2' => '这里是体验卡的会员专享福利',
        'level3' => '这里是银卡会员的会员专享福利',
        'level4' => '这里是金卡会员的会员专享福利',
        'level5' => '这里是白金卡会员的会员专享福利',
        'level6' => '这里是如意卡会员的会员专享福利',
        'level7' => '这里是贵宾卡会员的会员专享福利',
        'level8' => '这里是钻石卡会员的会员专享福利',
        'level9' => '这里是至尊卡会员的会员专享福利',
        'remark' => '我们在店里等着您的到来哦!!!'
    ),
    /*预约服务成功所需的模板消息内容*/
    'reservation' => array(
        'first' => "您已成功预约",
        'remark' => '请您合理安排您的时间,时间过期该订单将作废!!!'
    ),
    /*预约服务--过期--所需的模板消息内容*/
    'reservation_expired' => array(
        'first' => "您已成功预约",
        'remark' => '请您在预约时间到店，同时我们也会为您保留30分钟，谢谢！'
    ),
    /*服务购买成功所需的模板消息内容*/
    'service' => array(
        'first' => "服务购买成功啦!!",
        'remark' => "恭喜恭喜啊",
    ),
    /*好友注册成功给推荐人发送模板消息,提示积分到账*/
    'register' => array(
        'first' => "您的积分到账啦!!",
        'register' => "注册成功",
        'remark' => "恭喜恭喜啊!",
    ),
    /*会员办理成功,给推荐人发送模板消息,提示积分到账*/
    'ReferrerIntegral' => array(
        'first' => "您的积分到账啦!",
        'msg' => "办理会员啦",
        'remark' => '恭喜啊,厉害啊!来消费啊!',
    ),
    /*拼团成功给所有人发送模板消息*/
    'GroupActive' => array(
        'first' => "拼团成功啦!",
        'remark' => "您已经拼团成功,别忘了来店里啊!!!",
    ),
    'balancerefund' => array(
        'remark' => "您参加的活动已经结束，已退款至您的个人账户，请注意查收",
    ),
    'timeoutrefund' => array(
        'remark' => "您预约的服务已超时，系统取消了您的订单并根据用户协议将预约费用退至您的个人账户",
    ),
    "getsIntegralsuccess" => array(
        'first' => "积分到账提醒",
        'remark' => "积分已到账,可进入'E联车服'公众号查看",
    ),
    /*服务完成,给用户发送模板消息,提示评论*/
    'comment' => array(
        'first' => "尊敬的客户，您的本次服务已经完成",
        'remark' => '感谢您对我们的支持，您可以对本次服务进行评价，欢迎您下次光临！',
    ),
    /*余额变更提醒*/
    'balance' => array(
        'first' => "亲爱的用户,您的余额已经发生变更",
        'remark' => "可进入'E联车服'公众号查看"
    ),
    /*现金卡券到账提醒*/
    'redPacket' => array(
        'first' => "亲爱的用户,您的现金卡券到账成功",
        'remark' => "可进入'E联车服'公众号查看"
    ),
    /*会员充值*/
    "recharge" => array(
        'first' => "尊敬的会员，您好",
        'remark' => "感谢您的大力支持，期待您再次光临"
    ),
    /*活动上新*/
    'newActive' => array(
        'first' => '活动上新啦!',
        'remark' => '快去点击查看'
    ),
    /*俱乐部上新*/
    'newClub' => array(
        'first' => '俱乐部上新啦!',
        'remark' => '快去点击查看'
    ),
    /*消息上新*/
    'newNews' => array(
        'first' => '您有一条新消息',
        'remark' => '快去点击查看'
    ),
    /*积分兑换*/
    'integralExchange' => array(
        'first' => '积分兑换成功',
        'remark' => '兑换成功啦'
    ),
    /*会员到期*/
    'vipExpiration' => array(
        'first' => '您好，您的会员即将到期，请您注意。',
        'remark' => '请注意时间，防止过期失效'
    ),
    /*卡券即将过期提醒*/
    'coupon_expired' => array(
        'first' => '您有卡券即将过期',
        'remark' => '请赶快使用 , 逾期作废啦'
    ),
);
