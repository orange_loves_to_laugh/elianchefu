<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/23
 * Time: 14:47
 * 分享标题/文案/缩略图
 */
return array(
    # 分享服务
    'service' => array(
        'title' => 'nice!,这个服务很nice!!',
        'describe' => '线上预约E联车服的"{$title}"服务，享受更多优惠',
        'imgUrl' => imgUrl('static/img/bigLogo.png', true),
        'jumpUrl' => 'http://web.elianchefu.com/#/pages/index/service_info',
        'paySuccess'=>'http://preferent.youpilive.com/download.html'
    ),
    # 分享文章
    'shareClub' => array(
        'title' => 'E联车服-爱车智能服务真是爽！',
        'describe' => '超级优惠！超级优惠！E联车服-真的太好了！良心平台，优质服务体验！都来看看吧…',
        'imgUrl' => imgUrl('static/img/bigLogo.png', true),
        'jumpUrl' => 'http://web.elianchefu.com/#/pages/club/ArticleDetail',
        'paySuccess'=>'http://preferent.youpilive.com/download.html'
    ),
    # 客服电话
    "customerServerTel"=>"400-8045288",
    # 分享会员
    'shareVip'=>array(
        'title' => '洗车不花钱的APP “E联车服”',
        'describe' => '下载“E联车服” APP，洗车，吃饭都省钱！',
        'imgUrl' => imgUrl('static/img/bigLogo.png', true)
    ),
    # 推荐会员
    'recommendVip'=>array(
        'title'=>'洗车不花钱的APP “E联车服”',
        'describe'=>'下载“E联车服” APP，洗车，吃饭都省钱！',
//        'describe'=>'享专属优惠，终身享受平台特权，养车不花钱！',
        'imgUrl'=>imgUrl('static/img/bigLogo.png', true)
    ),
    # 活动支付成功-分享跳转地址
    'paySuccess'=>'http://preferent.youpilive.com/download.html',
    # 分享订单
    'shareOrder'=>array(
//        'title' => 'E联车服-爱车智能服务真是爽！',

//        'describe' => '超级优惠！超级优惠！E联车服-真的太好了！良心平台，优质服务体验！都来看看吧…',
        'title'=>'洗车不花钱的APP “E联车服”',
        'describe'=>'下载“E联车服” APP，洗车，吃饭都省钱！',
        'imgUrl' => imgUrl('static/img/bigLogo.png', true),
        'jumpUrl' => 'http://preferent.youpilive.com/download.html',
        'paySuccessUrl'=>'http://preferent.youpilive.com/download.html'
    ),
    # 分享红包
    'shareRedPacket'=>array(
//        'title' => 'E联车服-爱车智能服务真是爽！',
//        'describe' => '超级优惠！超级优惠！E联车服-真的太好了！良心平台，优质服务体验！都来看看吧…',
        'title'=>'洗车不花钱的APP “E联车服”',
        'describe'=>'下载“E联车服” APP，洗车，吃饭都省钱！',
        'imgUrl' => imgUrl('static/img/bigLogo.png', true),
        'jumpUrl' => 'http://preferent.youpilive.com/download.html',
        'paySuccessUrl'=>'http://preferent.youpilive.com/download.html'
    ),
    # 头条审核  (false=> 首页办会员入口 我的页面会员信息展示 我的广告 关于我们里面二维码)
    'toutiaoMember'=>false,
    # 头条审核  (false=> 首页办吃喝玩乐,商家列表)
    'toutiaoShow'=>false
);
