<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/7/3
 * Time: 9:09
 */


return [
    // 开发模式
    'is_develop'                            => true,

    // 默认编码
    'default_charset'                       => 'utf-8',

    // 公共系统配置信息key
    'cache_common_my_config_key'            => 'cache_common_my_config_data',


    // 应用数据缓存
    'cache_plugins_data_key'                => 'cache_plugins_data_key_data_',


    // 配置信息一条缓存 拼接唯一标记 [ only_tag ]
    'cache_config_row_key'                  => 'cache_config_row_data_',

    // 用户缓存信息
    'cache_merchant_info'                       => 'cache_merchant_info_',

    // 附件host, 数据库图片地址以/static/...开头
    'attachment_host'                       => defined('__MY_PUBLIC_URL__') ? substr(__MY_PUBLIC_URL__, 0, -1) : '',

    'attachment_localhost'                       => defined('__MY_ROOT_PUBLIC__') ?__MY_ROOT_PUBLIC__ : '',

    // 开启U带域名
    'url_domain_deploy'                     => true,

    //默认自动收货时间
    'auto_recept_days'                      =>'15days',



];