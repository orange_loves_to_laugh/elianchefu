<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/11
 * Time: 8:48
 * 修改车辆限制
 */
return array(
    # 删除车辆限制 , 几天内 几辆车 有过服务  不可操作
    'delCar' => array(
        1 => array('day' => 3, 'carNum' => 1),#体验卡会员
        2 => array('day' => 3, 'carNum' => 1),
        3 => array('day' => 3, 'carNum' => 2),
        4 => array('day' => 3, 'carNum' => 2),
        5 => array('day' => 3, 'carNum' => 3),
        6 => array('day' => 3, 'carNum' => 3),
        7 => array('day' => 3, 'carNum' => 4),
        8 => array('day' => 3, 'carNum' => 5),
        9 => array('day' => 3, 'carNum' => 1),# 普通卡会员
    ),
    # 免排队车辆修改限制 , 有几辆免排队的车 , 几天内服务过几次  不可操作
    'lineUpCar' => array(
        #      免排队的车辆数量 <= 2
        array('day' => 3, 'serviceNum' => 1),
        #  2 < 免排队的车辆数量 <= 4
        array('day' => 3, 'serviceNum' => 2),
        #      免排队的车辆数量  > 4
        array('day' => 3, 'serviceNum' => 3),
    )
);
