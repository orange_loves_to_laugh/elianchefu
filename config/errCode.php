<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/1
 * Time: 16:01
 */

return [
    //微信支付

    'SYSTEMERROR' => '系统超时',
    'PARAM_ERROR' => '参数错误',
    'ORDERPAID' => '订单已支付',
    'NOAUTH' => '商户无权限',
    'AUTHCODEEXPIRE' => '二维码已过期,请用户刷新后重新扫码',
    'NOTENOUGH' => '用户余额不足',
    'NOTSUPORTCARD' => '不支持卡类型',
    'ORDERCLOSED' => '订单已关闭',
    'ORDERREVERSED' => '订单已撤销',
    'BANKERROR' => '银行系统异常',
    'USERPAYING' => '用户支付中，需要输入密码',
    'AUTH_CODE_ERROR' => '每个二维码仅限使用一次，请刷新再试',
    'AUTH_CODE_INVALID' => '请扫描微信支付被扫条码/二维码',
    'XML_FORMAT_ERROR' => 'XML格式错误',
    'REQUIRE_POST_METHOD' => '请使用post方法',
    'SIGNERROR' => '签名错误',
    'LACK_PARAMS' => '缺少参数',
    'NOT_UTF8' => '编码格式错误',
    'BUYER_MISMATCH' => '支付帐号错误,暂不支持同一笔订单更换支付方',
    'APPID_NOT_EXIST' => 'APPID不存在',
    'MCHID_NOT_EXIST' => 'MCHID不存在',
    'OUT_TRADE_NO_USED' => '商户订单号重复',
    'APPID_MCHID_NOT_MATCH' => 'appid和mch_id不匹配',
    'INVALID_REQUEST' => '无效请求',
    'TRADE_ERROR' => '交易错误,请确认用户帐号是否存在异常',

    //支付宝支付

    'SYSTEM_ERROR' => '接口返回错误',//请立即调用查询订单API，查询当前订单的状态，并根据订单状态决定下一步的操作，如果多次调用依然报此错误码，请联系支付宝客服
    'INVALID_PARAMETER' => '参数无效',//检查请求参数，修改后重新发起请求
    'ACCESS_FORBIDDEN' => '无权限使用接口',//未签约条码支付或者合同已到期
    'EXIST_FORBIDDEN_WORD' => '订单信息中包含违禁词',//	修改订单信息后，重新发起请求
    'PARTNER_ERROR' => '应用APP_ID填写错误',//联系支付宝小二（联系支付宝文档右边的客服头像或到支持中心咨询），确认APP_ID的状态
    'TOTAL_FEE_EXCEED' => '订单总金额超过限额',//修改订单金额再发起请求
    'PAYMENT_AUTH_CODE_INVALID' => '支付授权码无效',//用户刷新条码后，重新扫码发起请求
    'CONTEXT_INCONSISTENT' => '交易信息被篡改',//更换商家订单号后，重新发起请求
    'TRADE_HAS_SUCCESS' => '交易已被支付',//确认该笔交易信息是否为当前买家的，如果是则认为交易付款成功，如果不是则更换商家订单号后，重新发起请求
    'TRADE_HAS_CLOSE' => '交易已经关闭',//更换商家订单号后，重新发起请求
    'BUYER_BALANCE_NOT_ENOUGH' => '买家余额不足',//买家绑定新的银行卡或者支付宝余额有钱后再发起支付
    'BUYER_BANKCARD_BALANCE_NOT_ENOUGH' => '用户银行卡余额不足',//建议买家更换支付宝进行支付或者更换其它付款方式
    'ERROR_BALANCE_PAYMENT_DISABLE' => '余额支付功能关闭',//用户打开余额支付开关后，再重新进行支付
    'BUYER_SELLER_EQUAL' => '买卖家不能相同',//	更换买家重新付款
    'TRADE_BUYER_NOT_MATCH' => '交易买家不匹配',//	更换商家订单号后，重新发起请求
    'BUYER_ENABLE_STATUS_FORBID' => '买家状态非法',//用户联系支付宝小二（联系支付宝文档右边的客服头像或到支持中心咨询），确认买家状态为什么非法
    'PULL_MOBILE_CASHIER_FAIL' => '唤起移动收银台失败	',//用户刷新条码后，重新扫码发起请求
    'MOBILE_PAYMENT_SWITCH_OFF' => '用户的无线支付开关关闭	',//用户在PC上打开无线支付开关后，再重新发起支付
    'PAYMENT_FAIL' => '支付失败',//用户刷新条码后，重新发起请求，如果重试一次后仍未成功，更换其它方式付款
    'BUYER_PAYMENT_AMOUNT_DAY_LIMIT_ERROR' => '买家付款日限额超限',//更换买家进行支付
    'BEYOND_PAY_RESTRICTION' => '商户收款额度超限',//联系支付宝小二提高限额（联系电话：95188）
    'BEYOND_PER_RECEIPT_RESTRICTION' => '商户收款金额超过月限额',//联系支付宝小二提高限额（联系电话：95188）
    'BUYER_PAYMENT_AMOUNT_MONTH_LIMIT_ERROR' => '买家付款月额度超限',//让买家更换账号后，重新付款或者更换其它付款方式
    'SELLER_BEEN_BLOCKED' => '商家账号被冻结',//联系支付宝小二，解冻账号（联系电话：95188）
    'ERROR_BUYER_CERTIFY_LEVEL_LIMIT' => '买家未通过人行认证',//让用户联系支付宝小二并更换其它付款方式（联系电话：95188）
    'PAYMENT_REQUEST_HAS_RISK' => '支付有风险',//更换其它付款方式
    'NO_PAYMENT_INSTRUMENTS_AVAILABLE' => '没用可用的支付工具',//更换其它付款方式
    'USER_FACE_PAYMENT_SWITCH_OFF' => '用户当面付付款开关关闭',//让用户在手机上打开当面付付款开关
    'INVALID_STORE_ID' => '商户门店编号无效',//检查传入的门店编号是否有效
    'SUB_MERCHANT_CREATE_FAIL' => '二级商户创建失败',//检查上送的二级商户信息是否有效
    'SUB_MERCHANT_TYPE_INVALID' => '二级商户类型非法',//检查上传的二级商户类型是否有效
    'AGREEMENT_NOT_EXIST' => '用户协议不存在',//确认代扣业务传入的协议号对应的协议是否已解约
    'AGREEMENT_INVALID' => '用户协议失效',//代扣业务传入的协议号对应的用户协议已经失效，需要用户重新签约
    'AGREEMENT_STATUS_NOT_NORMAL' => '用户协议状态非NORMAL',//代扣业务用户协议状态非正常状态，需要用户解约后重新签约
    'MERCHANT_AGREEMENT_NOT_EXIST' => '商户协议不存在',//确认商户与支付宝是否已签约
    'MERCHANT_AGREEMENT_INVALID' => '商户协议已失效',//商户与支付宝合同已失效，需要重新签约
    'MERCHANT_STATUS_NOT_NORMAL' => '商户协议状态非正常状态',//商户与支付宝的合同非正常状态，需要重新签商户合同
    'CARD_USER_NOT_MATCH' => '脱机记录用户信息不匹配',//请检查传入的进展出站记录是否正确
    'CARD_TYPE_ERROR' => '卡类型错误',//检查传入的卡类型
    'CERT_EXPIRED' => '凭证过期',//凭证已经过期
    'AMOUNT_OR_CURRENCY_ERROR' => '订单金额或币种信息错误',//检查订单传入的金额信息是否有误，或者是不是当前币种未签约
    'CURRENCY_NOT_SUPPORT' => '订单币种不支持',//	请检查是否签约对应的币种
    'MERCHANT_UNSUPPORT_ADVANCE' => '	先享后付2.0准入失败,商户不支持垫资支付产品',//	先享后付2.0准入失败,商户不支持垫资支付产品
    'BUYER_UNSUPPORT_ADVANCE' => '先享后付2.0准入失败,买家不满足垫资条件',//先享后付2.0准入失败,买家不满足垫资条件
    'ORDER_UNSUPPORT_ADVANCE' => '订单不支持先享后付垫资',//订单不支持先享后付垫资
    'CYCLE_PAY_DATE_NOT_MATCH' => '扣款日期不在签约时的允许范围之内',//对于周期扣款产品，签约时会约定扣款的周期。如果发起扣款的日期不符合约定的周期，则不允许扣款。请重新检查扣款日期，在符合约定的日期发起扣款。
    'CYCLE_PAY_SINGLE_FEE_EXCEED' => '周期扣款的单笔金额超过签约时限制',//对于周期扣款产品，签约时会约定单笔扣款的最大金额。如果发起扣款的金额大于约定上限，则不允许扣款。请在允许的金额范围内扣款。
    'CYCLE_PAY_TOTAL_FEE_EXCEED' => '周期扣款的累计金额超过签约时限制',//对于周期扣款产品，签约时可以约定多次扣款的累计金额限制。如果发起扣款的累计金额大于约定上限，则不允许扣款。请在允许的金额范围内扣款。
    'CYCLE_PAY_TOTAL_TIMES_EXCEED' => '周期扣款的总次数超过签约时限制',//对于周期扣款产品，签约时可以约定多次扣款的总次数限制。如果发起扣款的总次数大于约定上限，则不允许扣款。请在允许的次数范围内扣款
    'SECONDARY_MERCHANT_STATUS_ERROR' => '商户状态异常',//请联系对应的服务商咨询
    'AUTH_NO_ERROR' => '预授权号错误或状态不对',//1、确认预授权单号是否正确；2、确认预授权订单的参与方与支付单的参与方是否一致；3、确认预授权订单的状态是否为已授权状态；
    'BUYER_NOT_EXIST' => '买家不存在',//联系支付宝小二，确认买家是否已经注销账号
    'PRODUCT_AMOUNT_LIMIT_ERROR' => '产品额度超限',//联系支付宝小二提高限额（联系电话：95188）
    'SECONDARY_MERCHANT_ALIPAY_ACCOUNT_INVALID' => '二级商户账户异常',//确认传入的二级商户结算账户是否与进件时设置的结算账户一致，如果一致可联系支付宝小二确认是否商户的账号信息有变更
    'INVALID_RECEIVE_ACCOUNT' => '收款账户不支持',//确认seller_id信息是否传递正确，如正确请确认seller_id是否在签约中设置了收款权限
    'SELLER_NOT_EXIST' => '卖家不存在',//确认卖家信息是否传递正确
    'AUTH_AMOUNT_NOT_ENOUGH' => '授权金额不足',//订单金额大于授权剩余金额，请检查授权单剩余金额信息
    'AGREEMENT_ERROR' => '协议信息异常',//请检查传入的协议信息是否正确
    'BEYOND_PER_RECEIPT_SINGLE_RESTRICTION' => '订单金额超过单笔限额',//联系支付宝小二提高限额（联系电话：95188）
    'PAYER_UNMATCHED' => '付款人不匹配',//建议用户更换为指定的支付宝账号进行支付
    'PRE_AUTH_PROD_CODE_INCONSISTENT' => '预授权产品码不一致',//请检查预授权订单和转交易订单传入的产品码是否一致
    'SECONDARY_MERCHANT_ID_INVALID' => '二级商户不存在',//请检查传入的二级商户编号是否正确
    'NOW_TIME_AFTER_EXPIRE_TIME_ERROR' => '当前时间已超过允许支付的时间',//请检查传入的支付超时时间是否正确
    'SECONDARY_MERCHANT_NOT_MATCH' => '二级商户信息不匹配',//请检查传入的二级商户编号是否正确
    'REQUEST_AMOUNT_EXCEED' => '请求金额超限',//请检查传入的订单金额是否正确，预授权订单场景下请检查订单金额是否大于冻结金额
    'SUB_GOODS_SIZE_MAX_COUNT' => '子商品明细超长',//请检查子商品明细是否超过了150条
    'NOT_SUPPORT_PAYMENT_INST' => '不支持的钱包版本',//业务不支持使用该客户端支付，建议买家更换客户端进行支付或者更换其它付款方式
    'BUYER_NOT_MAINLAND_CERT' => '买家证件类型非大陆身份证',//该服务仅支持中国大陆身份证实名制用户,建议买家完善实名信息或者更换其它付款方式
    'SECONDARY_MERCHANT_ID_BLANK' => '二级商户编号错误',//请检查是否正确传入二级商户编号
    'TRADE_SETTLE_ERROR' => '交易结算异常',//请检查传入的结算项信息是否正确，如果正确请联系支付宝小二
    'AUTH_TOKEN_IS_NOT_EXIST' => '支付授权码为空',//请检查请求参数是否正确，支付授权码、协议信息或预授权号是否正确传入
    'SMILE_PAY_MERCHANT_NOT_MATCH' => '请求支付和刷脸服务的商户身份不一致',//请检查请求支付和刷脸服务使用的pid是否一致
    'NOT_CERTIFIED_USER' => '买家非实名认证用户',//建议买家完善实名信息后再重试或者更换其它付款方式
    'SECONDARY_MERCHANT_ISV_PUNISH_INDIRECT' => '商户状态异常',//请联系对应的服务商咨询
    'RESTRICTED_MERCHANT_INDUSTRY' => '行业信息交易受限',//订单金额超过所属行业支持的最大金额
    'PLATFORM_BUSINESS_ACQUIRE_MODE_MUST_MERCHANT_ID' => '二级商户编码为空',//请检查是否正确传入二级商户编号
    'BEYOND_PER_RECEIPT_DAY_RESTRICTION' => '订单金额超过当日累计限额',//联系支付宝小二提高限额（联系电话：95188）
    'TRADE_STATUS_ERROR' => '交易状态异常',//请检查订单状态是否已经支付成功
    'MERCHANT_PERM_RECEIPT_SUSPEND_LIMIT' => '商户暂停收款',//联系支付宝小二处理（联系电话：95188）
    'MERCHANT_PERM_RECEIPT_SINGLE_LIMIT' => '超过单笔收款限额',//联系支付宝小二处理（联系电话：95188）
    'MERCHANT_PERM_RECEIPT_DAY_LIMIT' => '超过单日累计收款额度',//联系支付宝小二处理（联系电话：95188）

    //微信退款
    'BIZERR_NEED_RETRY' => '系统超时',//请不要更换商户退款单号，请使用相同参数再次调用API。
    'TRADE_OVERDUE' => '订单已经超过退款期限',//订单已经超过可退款的最大期限(支付后一年内可退款)
    'ERROR' => '业务错误',//该错误都会返回具体的错误原因，请根据实际返回做相应处理。
    'USER_ACCOUNT_ABNORMAL' => '退款请求失败',//此状态代表退款申请失败，商户可自行处理退款。
    'INVALID_REQ_TOO_MUCH' => '无效请求过多',//	请检查业务是否正常，确认业务正常后请在1分钟后再来重试
    'INVALID_TRANSACTIONID' => '无效transaction_id',//请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败
    'ORDERNOTEXIST' => '订单号不存在',
    'FREQUENCY_LIMITED' => '频率限制',
    'CERT_ERROR' => '证书校验错误',
    'REFUND_FEE_MISMATCH' => '订单金额或退款金额与之前请求不一致，请核实后再试',
    'ORDER_NOT_READY' => '订单处理中，暂时无法退款，请稍后再试',

    //商户转账

    //支付宝退款
    'SELLER_BALANCE_NOT_ENOUGH' => '卖家余额不足',//商户支付宝账户充值后重新发起退款即可
    'REFUND_AMT_NOT_EQUAL_TOTAL' => '退款金额超限',//检查退款金额是否正确，重新修改请求后，重新发起退款
    'REASON_TRADE_BEEN_FREEZEN' => '请求退款的交易被冻结',//联系支付宝小二，确认该笔交易的具体情况
    'TRADE_NOT_EXIST' => '交易不存在',//检查请求中的交易号和商户订单号是否正确，确认后重新发起
    'TRADE_HAS_FINISHED' => '交易已完结',//该交易已完结，不允许进行退款（即使重试后也无法成功），确认请求的退款的交易信息是否正确
    'DISCORDANT_REPEAT_REQUEST' => '不一致的请求',//检查该退款号是否已退过款或更换退款号重新发起请求
];
