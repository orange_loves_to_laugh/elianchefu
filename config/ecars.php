<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/11
 * Time: 17:10
 */

// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 开发模式
    'is_develop'                            => true,

    //服务分类缓存
    'cache_service_category_key'            =>'cache_service_category_',

    // 全局token
    'cache_global_token'                       => 'cache_global_token_',
    // 用户缓存信息
    'cache_user_info'                       => 'cache_user_info_',
    //系统配置信息
    'cache_common_my_config_key'          => 'cache_common_my_config_data',
    // 附件host, 数据库图片地址以/static/...开头
    'attachment_host'                       => defined('__MY_PUBLIC_URL__') ? substr(__MY_PUBLIC_URL__, 0, -1) : '',
    //平台文章标签
    'doc_common_type'                       =>['agreement','getJngInfo','doc3'],
    // token前缀MD5(eservice_users_token_prefix)
    'token_prefix'                          => '5debd83faf3b1a8b2ea4b00352b7be3c',
    // 头条小程序id
    'toutiao_Appid'=>'tt4c163aabd32fa84501',
    // 头条小程序
    'toutiao_AppSecret'=>'5a29be0565c104fef54c995050121423304fd552',
];

