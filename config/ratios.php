<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/19
 * Time: 16:12
 */
return [
    # 用户评论----提成比率
    'comment_rate_commission' => array('good' => 1, 'medium' => 0.5, 'bad' => 0),
    # 合作店用户评论----结算比率
    'comment_rate_settlement' => array('good' => 1, 'medium' => 0.9, 'bad' => 0.8),
    # 差评扣款金额
    'comment_bad' => 30,
    # 服务提成比例(平台服务)=> 15%
    'service_rate' => 0.15,
    # 服务提成比例(自定义服务)=> 10%
    'custom_service_rate' => 0.1,
    # 商品提成比例(平台商品/自定义商品/推荐)=> 5%
    'pro_rate' => 0.05,
    # 合伙人收益体现设置
    'partner_withdrawal' => array(
        'min'=>20,
        'divide'=>20,
        'fee_ratio'=>0.05,
        'max'=>2000
    )
];
