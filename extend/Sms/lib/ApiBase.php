<?php
/*
* Copyright ©2013-2016 YiDont.All Rights Reserved.
* Version 1.0 
* 2017-06-24
*/
namespace Sms\Lib;

require_once 'ApiHttpClient.php';
require_once 'ApiHttpUtil.php';
/**
 * Api Base 基类
 */
class ApiBase {

    /**
     * 获取access token url
     * @var string
     */
//    protected $accessTokenUrl = 'http://oauth.cloudfoci.com/token.php';
    protected $accessTokenUrl = 'http://121.201.72.14:8883/token.php';
	protected $accessTokenPath= '/apache/SDK/sdk/user_token';

    /**
     * appId
     * @var string
     */
    protected $appId = '';

    /**
     * apiKey
     * @var string
     */
    protected $apiKey = '';

    

    /**
     * 权限
     * @var array
     */
    protected $scope = 'basic';//brain_all_scope//basic

    /**
     * @param string $appId
     * @param string $apiKey
     */
    public function __construct($appId, $apiKey){
        $this->appId = trim($appId);
        $this->apiKey = trim($apiKey);
        $this->isCloudUser = null;
        $this->client = new ApiHttpClient();
        $this->version = '1_0_0';//接口版本，可以忽略
    }

    /**
     * 连接超时
     * @param int $ms 毫秒
     */
    // public function setConnectionTimeoutInMillis($ms){
        // $this->client->setConnectionTimeoutInMillis($ms);
    // }

    /**
     * 响应超时
     * @param int $ms 毫秒
     */
    // public function setSocketTimeoutInMillis($ms){
        // $this->client->setSocketTimeoutInMillis($ms);
    // }

    /**
     * Api 请求
     * @param  string $url
     * @param  mixed $data
     * @return mixed
     */
    protected function request($url, $data, $headers=array()){
        try{
            $params = array();
            $authObj = $this->auth();
//			$params=json_decode($data,true);
            $headers = $this->getAuthHeaders('POST', $url, $params, $headers);//第二种方式
//			print_r($authObj);print_r($headers);
            if($this->isCloudUser === false){//token 请求,params为空则是sign请求
                $params['access_token'] = $authObj['access_token'];
            }
			$data['access_token'] = $params['access_token'];
           $response = $this->client->post($url, $data, $params, $headers);//print_r($response);
           $obj = $this->proccessResult($response['content']);
			
            if(!$this->isCloudUser && isset($obj['code']) && $obj['code'] == 110){
				//110	Access Token失效  Access token invalid or no longer valid	
				//失效，采用refresh 刷新token
				//echo '失效，采用refresh 刷新token';
                $authObj = $this->auth(true);
                $params['access_token'] = $authObj['access_token'];
                $response = $this->client->post($url, $data, $params, $headers);
                $obj = $this->proccessResult($response['content']);
            }

           // if(empty($obj) || !isset($obj['code'])){
			   if($obj['code']=='100'){//写入缓存
                $this->writeAuthObj($authObj);
            }
        }catch(Exception $e){
            return array("code" => 101,"msg" => "请求失败","result" =>"");
        }
		if(empty($obj)){$obj=array("code" => 101,"msg" => "请求失败","result" =>"");}
        return $obj;
    }




    /**
     * 格式化结果
     * @param $content string
     * @return mixed
     */
    protected function proccessResult($content){
        return json_decode($content, true);
    }

    /**
     * 返回 access token 路径
     * @return string
     */
    private function getAuthFilePath(){
		// $this->accessTokenPath. DIRECTORY_SEPARATOR . md5($this->appId);
        //return dirname(__FILE__) . DIRECTORY_SEPARATOR . md5($this->appId);
		return $this->accessTokenPath. DIRECTORY_SEPARATOR . md5($this->appId);
    }

    /**
     * 写入本地文件
     * @param  array $obj
     * @return void
     */
    private function writeAuthObj($obj){
        if($obj === null || (isset($obj['is_read']) && $obj['is_read'] === true)){
            return;
        }

        $obj['time'] = time();
        $obj['is_cloud_user'] = $this->isCloudUser;
        @file_put_contents($this->getAuthFilePath(), json_encode($obj));
    }

    /**
     * 读取本地缓存
     * @return array
     */
    private function readAuthObj(){
        $content = @file_get_contents($this->getAuthFilePath());
        if($content !== false){
            $obj = json_decode($content, true);
            $this->isCloudUser = $obj['is_cloud_user'];
            $obj['is_read'] = true;
            if($this->isCloudUser || $obj['time'] + $obj['expires_in'] - 30 > time()){
                return $obj;
            }
        }

        return null;
    }

    /**
     * 认证
     * @param bool $refresh 是否刷新
     * @return array
     */
    private function auth($refresh=false){

        //非过期刷新
        if(!$refresh){//存在
            $obj = $this->readAuthObj();
            if(!empty($obj)){
                return $obj;
            }
        }

        $response = $this->client->post($this->accessTokenUrl, array(
            'grant_type' => 'client_credentials',
            'client_id' => $this->appId,
            'client_secret' => $this->apiKey,
        ));

        $obj = json_decode($response['content'], true);

        $this->isCloudUser = !$this->isPermission($obj);
        return $obj;
    }

    /**
     * 判断认证是否有权限
     * @param  array   $authObj 
     * @return boolean          
     */
    private function isPermission($authObj)
    {
        if(empty($authObj) || !isset($authObj['scope'])){
            return false;
        }

        $scopes = explode(' ', $authObj['scope']);
        return in_array($this->scope, $scopes);//如果存在，这是token,否则则是sign方式
    }

    /**
     * @param  string $method HTTP method
     * @param  string $url
     * @param  array $param 参数
     * @return array
     */
    private function getAuthHeaders($method, $url, $params=array(), $headers=array()){
        
        //不是云的老用户则不用在header中签名 认证
        if($this->isCloudUser === false){
            return $headers;
        }

        $obj = parse_url($url);
        if(!empty($obj['query'])){
            foreach(explode('&', $obj['query']) as $kv){
                if(!empty($kv)){
                    list($k, $v) = explode('=', $kv, 2);
                    $params[$k] = $v;
                }
            }
        }

        //UTC 时间戳
        $timestamp = gmdate('Y-m-d\TH:i:s\Z');
        $headers['Host'] = isset($obj['port']) ? sprintf('%s:%s', $obj['host'], $obj['port']) : $obj['host'];
        $headers['x-bce-date'] = $timestamp;

        //签名
        $headers['authorization'] = ApiSampleSigner::sign(array(
            'appId' => $this->appId,
            'apiKey' => $this->apiKey,
        ), $method, $obj['path'], $headers, $params, array(
            'timestamp' => $timestamp,
            'headersToSign' => array_keys($headers),
        ));

        return $headers;
    }

}
