<?php
namespace Sms;

//require_once 'lib/ApiBase.class.php';
use Sms\Lib\ApiBase;


/**
 * 亿动云通讯SDK 焦云
 */
class Sms extends ApiBase {

    /**
     * 短信API
     * @var string
     */
//	private  $smsUrl = 'http://api.cloudfoci.com/sms.php';
	private  $smsUrl = 'http://121.201.72.14:8881/sms.php';

    /**
     * 语音API
     * @var string
     */
    private $voiceUrl = 'http://api.cloudfoci.com/voice.php';

    /**
     * 流量API
     * @var string
     */
    private $flowUrl = 'http://api.cloudfoci.com/flow.php';


    /**
     * 发送短信
     * @phone	string	必须	接收短信的号码
	 * @tpl_id	string	必须	短信模块ID,可到短信模板页面获取
	 * @tpl_value	string	可选	短信模板的变量名和对应变量值对,请参照上面的变量示例
     * @return array
     */
    public function sendSms($phone,$tpl_id,$tpl_value){

        $data = array();
		$data['action']='verification';
        $data['phone'] = urlencode($phone);
		$data['tpl_id'] = urlencode($tpl_id);
		$data['tpl_value'] = urlencode($tpl_value);
        return $this->request($this->smsUrl, $data);
    }

	
	 /**
	*获取短信状态
	* @token	string  必填	调用token请求接口返回的token值。
	* @pageSize	int	   可选	    返回记录数，默认20条，最大值为50条
	*/
    public function getSmsStatus($pageSize){

        $data = array();
		$data['action']='getStatus';
        $data['pageSize'] = $pageSize;
        return $this->request($this->smsUrl, $data);
    }

	
	/**
	*获取上行短信状态
	* @token	string  必填	调用token请求接口返回的token值。
	* @pageSize	int	   可选	    返回记录数，默认20条，最大值为50条
	*/
    public function getUpSmsStatus($pageSize){

        $data = array();
		$data['action']='getUpSmsStatus';
        $data['pageSize'] = $pageSize;
        return $this->request($this->smsUrl, $data);
    }
	
	
	/**
	*获取短信数量
	* @token	string  必填	调用token请求接口返回的token值。
	*/
    public function queryBlance($type){

        $data = array();
		$data['action']='queryBlance';
		$data['type'] = $type;
        return $this->request($this->smsUrl, $data);
    }
	
	
	
	/**
	*发送语音
	* @	phone		string  必填	要发送验证码的手机号码。
	* @	tpl_id	    string  必填	语音模板的id,可到模板列表中查看。
	* @	tpl_code	string  选填	不填写此项参数，系统会自动生成6位数字的验证码。
	* @	voiceId		string  选填	不同的声调语音库,不填写此项参数，系统则选择默认的声音库。
	* @ return 		array
	*/ 
	public function sendVoice($phone,$tpl_id,$tpl_code,$voiceId,$is_message){
        $data = array();
		$data['action']='verification';
        $data['phone'] = urlencode(mb_convert_encoding($phone, 'GBK', 'UTF8'));
		$data['tpl_id'] = urlencode(mb_convert_encoding($tpl_id, 'GBK', 'UTF8'));
		$data['tpl_code'] = urlencode(mb_convert_encoding($tpl_code, 'GBK', 'UTF8'));
		$data['voiceId'] = urlencode(mb_convert_encoding($voiceId, 'GBK', 'UTF8'));
		$data['is_message'] = urlencode(mb_convert_encoding($is_message, 'GBK', 'UTF8'));

        return $this->request($this->voiceUrl,$data);
    }


	/**
	*发送语音通知
	* @	phone		string  必填	要发送验证码的手机号码。
	* @	tpl_id	    string  必填	语音模板的id,可到模板列表中查看。
	* @	tpl_code	string  选填	不填写此项参数，系统会自动生成6位数字的验证码。
	* @	voiceId		string  选填	不同的声调语音库,不填写此项参数，系统则选择默认的声音库。
	* @ return 		array
	*/ 
	public function sendVoiceNotice($phone,$tpl_id,$tpl_code,$voiceId){
        $data = array();
		$data['action']='voiceNotice';
        $data['phone'] = urlencode(mb_convert_encoding($phone, 'GBK', 'UTF8'));
		$data['tpl_id'] = urlencode(mb_convert_encoding($tpl_id, 'GBK', 'UTF8'));
		$data['tpl_code'] = urlencode(mb_convert_encoding($tpl_code, 'GBK', 'UTF8'));
		$data['voiceId'] = urlencode(mb_convert_encoding($voiceId, 'GBK', 'UTF8'));
		$data['is_message'] = urlencode(mb_convert_encoding($is_message, 'GBK', 'UTF8'));

        return $this->request($this->voiceUrl,$data);
    }
	
	/**
	*获取流量包
	* @	token	string  必填	调用token请求接口返回的token值。
	* @ return array
	*/
	public function flowGetPackage($type,$range){
        $data = array();
		$data['action'] = 'flowGetPackage';
		$data['type'] = urlencode(mb_convert_encoding($type, 'GBK', 'UTF8'));
		$data['range'] = urlencode(mb_convert_encoding($range, 'GBK', 'UTF8'));
        return $this->request($this->flowUrl, $data);
    }


	/**
	*流量充值
	*@	token	string  必填	调用token请求接口返回的token值。
	*@	orderId	string	必须	商户订单ID
	*@ 	phone	string	必须	充值号码
	*@	value	string	必须	套餐流量值
	*@  range    string  可选    流量类型，0全国漫游流量，1本地流量。默认为全国漫游流量
	*@ 	sign	string	必须	签名,参考上面的签名算法获取
	*/ 
	public function flowRecharge($orderId,$phone,$value,$range){
        $data = array();
		$data['action'] = 'flowRecharge';
        $data['orderId'] = urlencode(mb_convert_encoding($orderId, 'GBK', 'UTF8'));
		$data['phone'] = urlencode(mb_convert_encoding($phone, 'GBK', 'UTF8'));
		$data['value'] = urlencode(mb_convert_encoding($value, 'GBK', 'UTF8'));
		$data['range'] = urlencode(mb_convert_encoding($range, 'GBK', 'UTF8'));
        return $this->request($this->flowUrl, $data);
    }


	/**
	*获取充值状态
	*@	token	string  必填	调用token请求接口返回的token值。
	*@	orderId	string	必须	商户订单ID
	*@  dataTime	string	必须	商户订单提交日期 yyyy-MM-dd
	*@ 	sign	string	必须	签名,参考上面的签名算法获取
	*/
	public function flowOrderQuery($orderId,$dataTime){
        $data = array();
		$data['action'] = 'flowOrderQuery';
        $data['orderId'] = urlencode(mb_convert_encoding($orderId, 'GBK', 'UTF8'));
		$data['dataTime'] = urlencode(mb_convert_encoding($dataTime, 'GBK', 'UTF8'));
        return $this->request($this->flowUrl, $data);
    }

}
