<?php
namespace base;
// +----------------------------------------------------------------------
// | ShopXO 国内领先企业级B2C免费开源电商系统
// +----------------------------------------------------------------------
// | Copyright (c) 2011~2019 http://shopxo.net All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: juzi
// +----------------------------------------------------------------------


use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;

/**
 * Excel驱动.
 * @author   juzi
 * @blog    https://i.csdn.net/#/uc/profile
 * @version  0.0.1
 * @datetime 2020年5月17日16:03:09T21:51:08+0800
 */
class Excel
{
    private $filename;
    private $file_type;
    private $suffix;
    private $data;
    private $title;
    private $jump_url;
    private $msg;
    private $horizontal_center;
    private $vertical_center;
    private $warap_text;

    /**
     * [__construct 构造方法].
     * @author   juzi
     * @blog    https://i.csdn.net/#/uc/profile
     * @version  0.0.1
     * @datetime 2020年5月17日16:03:09T15:09:17+0800
     *
     * @param [string] $params['filename']          [文件名称（追加当前时间）]
     * @param [string] $params['suffix']            [文件后缀名（默认xls）]
     * @param [string] $params['jump_url']          [出错跳转url地址（默认上一个页面）]
     * @param [string] $params['msg']               [错误提示信息]
     * @param [string] $params['file_type']         [导出文件类型（默认excel）]
     * @param [array]  $params['title']             [标题（二维数组）]
     * @param [array]  $params['data']              [数据（二维数组）]
     * @param [int]    $params['horizontal_center'] [是否水平居中 1是]
     * @param [int]    $params['vertical_center']   [是否垂直居中 1是]
     * @param [int]    $params['warap_text']        [是否内容自动换行 1是]
     */
    public function __construct($params = [])
    {
        // 文件名称
        $date = date('YmdHis');
        $this->filename = isset($params['filename']) ? $params['filename'].'-'.$date : $date;

        // 文件类型, 默认excel
        $type_all = ['excel' => 'vnd.ms-excel', 'pdf' => 'pdf'];
        $this->file_type = (isset($params['file_type']) && isset($type_all[$params['file_type']])) ? $type_all[$params['file_type']] : $type_all['excel'];

        // 文件后缀名称
        $this->suffix = empty($params['suffix']) ? 'xls' : $params['suffix'];

        // 标题
        $this->title = isset($params['title']) ? $params['title'] : [];

        // 数据
        $this->data = isset($params['data']) ? $params['data'] : [];

        // 出错跳转地址
        $this->jump_url = empty($params['jump_url']) ? (empty($_SERVER['HTTP_REFERER']) ? __MY_URL__ : $_SERVER['HTTP_REFERER']) : $params['jump_url'];

        // 错误提示信息
        $this->msg = empty($params['msg']) ? 'title or data cannot be empty!' : $params['msg'];

        // 水平,垂直居中
        $this->horizontal_center = isset($params['horizontal_center']) ? intval($params['horizontal_center']) : 1;
        $this->vertical_center = isset($params['vertical_center']) ? intval($params['vertical_center']) : 1;

        // 内容自动换行
        $this->warap_text = isset($params['warap_text']) ? intval($params['warap_text']) : 1;

        // 引入PHPExcel类库
        //require ROOT_PATH.'../extend'.DS.'phpexcel'.DS.'PHPExcel.php';
    }



    /**
     * [Export excel文件导出].
     * @author   juzi
     * @blog    https://i.csdn.net/#/uc/profile
     * @version  0.0.1
     * @datetime 2020年5月17日16:03:09T15:12:01+0800
     */
    public function Export(){

        $letter = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'];

        $field =[];
        $title_arr = [];
        foreach ($this->title as $k=>$v) {
            $field[$k]= $v['field'];
            $title_arr[$k]= $v['name'];
        }



        $title_sheet=explode('-',$this->filename)[0];
        $title_file=$this->filename;


        $cell=[];
        $data=[];

        foreach ($this->data as $info_key=>$info) {
            $info_new_key=0;
            foreach ($info as $k=>$v)
            {

                $re=array_search($k, $field);

                if ($re||$re===0) {

                    $info_new[$re]=$v;
                    $info_new_key=$info_new_key+1;
                }

//                if (in_array($k,$field)) {
//                    $info_new[$info_new_key]=$v;

//                }
            }
            ksort($info_new) ;

            foreach ($info_new as $k=>$v)
            {
                $cell['title_'.$letter[$k]]=$v;
            }

            $data[$info_key]=$cell;
        }



        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()->setTitle($title_sheet);
        // 使用 setCellValueByColumnAndRow

        //设置单元格内容
        //设置表头
        foreach ($title_arr as $key => $value)
        {
            $sheet->setCellValueByColumnAndRow($key + 1, 1, $value);
        }

        $row = 2; // 从第二行开始
        foreach ($data as $item) {
            $column = 1;
            foreach ($item as $value) {
                // 单元格内容写入
                $sheet->setCellValueByColumnAndRow($column, $row, $value);
                $column++;
            }
            $row++;
        }
        //设定样式
        //所有sheet的表头样式 加粗
        $font = [
            'font' => [
                'bold' => true,
            ],
        ];

        //所有sheet的内容样式 加黑色边框
        $borders = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => 'black'],
                ],
            ],
        ];



        $sheet->getStyle('A1:'.$letter[$info_new_key-1].'1')->applyFromArray($font);

        $i=0;
        while ($letter[$i]!=$letter[$info_new_key])
        {
            $sheet->getColumnDimension($letter[$i])->setWidth(30);
            $i=$i+1;

        }

//        dump($letter[$i]);
//        exit;


        $sheet->getStyle('A1:'.$letter[$info_new_key-1]. ($row - 1))->applyFromArray($borders);
        ob_end_clean();
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title_file.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }




    /**
     * [Import excel文件导入].
     * @author   juzi
     * @blog    https://i.csdn.net/#/uc/profile
     * @version  0.0.1
     * @datetime 2017-04-06T18:18:55+0800
     * @param [string] $file [文件位置,空则读取全局excel的临时文件]
     * @return [array] [数据列表]
     */
    public function Import($file = '')
    {
        // 文件为空则取全局文变量excel的临时文件
        if (empty($file) && !empty($_FILES['excel']['tmp_name'])) {
            $file = $_FILES['excel']['tmp_name'];
        }

        // 文件地址是否有误,title数据是否有数据
        if (empty($file) || empty($this->title)) {
            echo '<script>alert("'.$this->msg.'");</script>';
            echo '<script>window.location.href="'.$this->jump_url.'"</script>';
            die;
        }

        // 取得文件基础数据
        $reader = \PHPExcel_IOFactory::createReader('Excel5');
        $excel = $reader->load($file);

        // 取得总行数
        $worksheet = $excel->getActiveSheet();

        // 取得总列数
        $highest_row = $worksheet->getHighestRow();

        // 取得最高的列
        $highest_column = $worksheet->getHighestColumn();

        // 总列数
        $highest_column_index = \PHPExcel_Cell::columnIndexFromString($highest_column);

        // 定义变量
        $result = [];
        $field = [];

        // 读取数据
        for ($row = 1; $row <= $highest_row; ++$row) {
            // 临时数据
            $info = [];

            // 注意 highest_column_index 的列数索引从0开始
            for ($col = 0; $col < $highest_column_index; ++$col) {
                $value = $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                if (1 == $row) {
                    foreach ($this->title as $tk => $tv) {
                        if ($value == $tv['name']) {
                            $tv['field'] = $tk;
                            $field[$col] = $tv;
                        }
                    }
                } else {
                    if (!empty($field)) {
                        $info[$field[$col]['field']] = ('int' == $field[$col]['type']) ? trim(ScienceNumToString($value)) : trim($value);
                    }
                }
            }
            if ($row > 1) {
                $result[] = $info;
            }
        }

        return $result;
    }
}

