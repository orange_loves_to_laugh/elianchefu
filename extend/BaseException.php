<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/4/21
 * Time: 10:28
 */


use think\Exception;


/**
 * PHPMailer exception handler.
 *
 * @author  Marcus Bointon <phpmailer@synchromedia.co.uk>
 */
class BaseException extends Exception
{
    /**
     * Prettify error message output.
     *
     * @return string
     * //     */
//    public function errorMessage()
//    {
//        return '<strong>' . htmlspecialchars($this->getMessage()) . "</strong><br />\n";
//    }

    // http状态码 正常200
    public $code = 400;
    // 错误具体信息
    public $message = '参数错误';
    // 自定义的错误码
    public $errorCode = 10000;
    // 自定义的请求状态
    public $status;
    // 自定义返回的数据
    public $data=null;

    public $debug=true;

    public function __construct($params = [])
    {

        if (!is_array($params)) {
            // return;

            throw new \Exception('参数必须是数组');
        }

        $this->data = [];
        if (array_key_exists('debug', $params)) {
            $this->debug = $params['debug'];
        }
        if (array_key_exists('code', $params)) {
            $this->code = $params['code'];
        }
        if (array_key_exists('msg', $params)) {
            $this->message = $params['msg'];
        }
        if (array_key_exists('errorCode', $params)) {
            $this->errorCode = $params['errorCode'];
        }
        if (array_key_exists('status', $params)) {
            $this->status = $params['status'];
        }

        if (array_key_exists('data', $params)) {
            $this->data = $params['data'];
        }
    }

}






