
方法名     |         说明     |                               地址
--------  | ----       | -----
change_to_quotes     | 给字符串加引号     |                  public.func.php
foo                  |             导入表格使用方法 替换关联数组为索引数组     |                    public.func.php
cutSubstr            |       显示指定长度的字符串，超出长度以省略号(...)填补尾部显示     |    public.func.php
searchKey            |       根据值查询数组对应的键(当存在多个相同的值时,只会返回第一个查询到的数据)(多维数组时返回的是第一层数组的键)        |            public.func.php             
iserializer          |     序列化     |                          public.func.php
iunserializer        |   反序列化     |                        public.func.php
is_serialized     |   判断是否序列化过     |                public.func.php
getip     |           获取操作端ip     |                    public.func.php
DataReturn     |      返回数据     |                        public.func.php
CurrentScriptName                 |              获取当前脚本名称     |                public.func.php
PramsChecked     |   参数校验方法     |                    public.func.php
CheckMobile     |     手机号码格式校验     |                public.func.php
getLonLat     |       根据地址获取经纬度     |              public.func.php
getAddressResolution                  |          逆地址解析     |                     public.func.php
priceFormat     |     价格格式化 整数保留1位，2小数末位是0，显示保留1位 或保留两位     |      public.func.php
randCodes     |       随机生成6位短信验证码     |           public.func.php
mb_str_split1     |   字符串无分隔符拆分成数组     |        public.func.php
get_images_from_html               |             从HTML文本中提取所有图片          |   public.func.php
PathToParams     |   路径解析指定参数     |               public.func.php
GetDocumentRoot     |获取当前系统所在根路径     |          public.func.php
fileFormat     |      根据获取后缀名判断文件类型         | ResourceService.php
getRandomString     | 获取对应长度的随机数字符串          | ResourceService.php
getMsectime     |     返回当前的毫秒时间戳     |            ResourceService.php
delFile     |         按路径删除文件方法     |              ResourceService.php
uploadBase     |      上传base64格式图片     |              ResourceService.php
imgUrl     |          图片路径整理(返回 http/https)     |                              ResourceService.php
delCacheItem     |    将文件地址在上传文件缓存中删除，以免删除文件     |               ResourceService.php
delUploadCache     |  删除缓存中无用文件方法     |          ResourceService.php
uploadFile     |      上传图片方法     |                    ResourceService.php
getsProClass     |    获取商品分类     |                   ProclassService.php
updateProClass     |  更新商品分类     |                    ProclassService.php
getSex             |          性别转换     |                       czt.func.php
getMemberType     |   用户状态转换(普通/合伙人)     |       czt.func.php
switchCheckboxChange          |                   状态切换js方法          |             dataSubmit.js                                       dataSubmit.js

