<?php
/**
 * @return string
 * @content 图片路径前最
 */
function getUrl()
{
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        return $http_type . $_SERVER['HTTP_HOST'] . '/' .'upload/';
}
/*
 * @access public
 * @param $price 价格
 * @return bool
 * @context 价格格式化
 * */
function getsPriceFormat($price,$num=2)
{
    return sprintf("%.".$num."f", floatval($price));
}
/*
 * @access public
 * @param $price 应用管理类型
 * @return bool
 * */
function getApplayTitle($mark){
    if(!empty($mark)){
        switch ($mark){
            case 'banner':
                $re='首页banner';
                break;
            case 'protocol':
                $re='协议管理';
                break;
            case 'cardbanner':
                $re='会员页banner';
                break;
            case 'carbanner':
                $re='二手车';
                break;
            case 'clubbanner':
                $re='俱乐部banner';
                break;
            case 'rcardimg':
                $re='推荐会员底图';
                break;
            case 'fstoreimg':
                $re='加盟店底图';
                break;
            case 'cstoreimg':
                $re='合作店底图';
                break;
            case 'businessimg':
                $re='商家底图';
                break;
            case 'partnerimg':
                $re='合伙人底图';
                break;
            case 'loading':
                $re='加载页管理';
                break;
            case 'proadvert':
                $re='商品详情页广告';
                break;
            case 'indexpopup':
                $re='首页弹窗';
                break;
            case 'indexadvert':
                $re='首页广告';
                break;
            case 'mineadvert':
                $re='我的页广告';
                break;
            case 'aboutmine':
                $re='关于我们';
                break;
            case 'statement':
                $re='声明管理';
                break;



        }
        return $re;
    }


}
/*
 * @access public
 * @param $price 应用管理类型
 * @return bool
 * */
function getApplayClass($mark){
    if(!empty($mark)){
        switch ($mark){
            case 'banner':
                $re='image';
                break;
            case 'protocol':
                $re='saveprotocol';
                break;
            case 'cardbanner':
                $re='image';
                break;
            case 'carbanner':
                $re='image';
                break;
            case 'clubbanner':
                $re='image';
                break;
            case 'rcardimg':
                $re='base';
                break;
            case 'fstoreimg':
                $re='base';
                break;
            case 'cstoreimg':
                $re='base';
                break;
            case 'businessimg':
                $re='base';
                break;
            case 'partnerimg':
                $re='base';
                break;
            case 'loading':
                $re='advert';
                break;
            case 'proadvert':
                $re='advert';
                break;
            case 'indexpopup':
                $re='advert';
                break;
            case 'indexadvert':
                $re='advert';
                break;
            case 'mineadvert':
                $re='advert';
                break;
            case 'mineadvert_bottom':
                $re='advert';
                break;
            case 'mineadvert_center':
                $re='advert';
                break;
            case 'aboutmine':
                $re='saveprotocol';
                break;
            case 'statement':
                $re='saveprotocol';
                break;



        }
        return $re;
    }


}
/*
 * @access public
 * @param $price 积分管理类型
 * @return bool
 * */
function getIntegralTitle($mark){
    if(!empty($mark)){
        switch ($mark){
            case 'register_integra':
                $re='注册奖励积分';
                break;
            case 'share_register_integra':
                $re='分享奖励积分';
                break;
            case 'evaluation_integral':
                $re='评论奖励积分';
                break;
            case 'look_article_integral':
                $re='查看文章';
                break;
            case 'club_integral':
                $re='分享文章';
                break;
            case 'recom_register_integral':
                $re='推荐好友注册';
                break;
            case 'share_busines_integral':
                $re='分享商家';
                break;
            case 'share_car_integral':
                $re='分享二手车';
                break;

        }
        return $re;
    }


}
/*
 * @access public
 * @param $price 任务管理类型 获取
 * @return bool
 * */
function getTaskType(){
    $res = array(
        '1'=> '分享好友',
        '2'=> '推荐好友注册',
        '3'=> '卡券核销',
        '4'=> '分享二手车',
        '5'=> '分享商家',
        '6'=> '分享文章',
        '7'=> '查看文章',
        '8'=> '参加活动',
        '9'=> '服务评论',
        '10'=> '积分兑换',
        '11'=> '预约服务',
        '12'=> '会员办理',
        '13'=> '用户充值',
        '14'=> '用户升级',
        '15'=> '推荐用户',
        '16'=> '参与摇奖',
        '17'=> '连续签到',
    );
    return $res;
}
/**
 * @access public
 * @param $level_id 会员id
 * @return title 会员名称
 * @context 获取会员名称
 */
function getsMemberLevelTitle($id)
{
    $member_level = \think\Db::table("member_level")->field(array('level_title'))->where(array('id' => $id))->select();
    if (!empty($member_level)) {
        return $member_level[0]['level_title'];
    }
}
/**
 * @access public
 * @return null
 * @context 商品或服务名称
 */
function getActiveTitle($type, $id)
{
    if ($type == 1) {
        $title = \think\Db::table('biz_pro')->field('biz_pro_title')->where(array('id' => $id))->select();
        if (!empty($title[0])) {
            return array('title' => $title[0]['biz_pro_title'], 'type' => '商品');
        }
    } elseif ($type == 2) {
        $title =\think\Db::table('service')->field('service_title')->where(array('id' => $id))->select();
        if (!empty($title[0])) {
            return array('title' => $title[0]['service_title'], 'type' => '服务');
        }
    } elseif ($type == 3) {
        return array('title' => '赠送积分', 'type' => '积分');
    } elseif ($type == 4) {
        return array("title" => '赠送余额', 'type' => '余额');
    }

}
/**
 * @access public
 * @return null
 * @context 搜索 去空格
 */
function strim($param){
    $re=str_replace(" ",'',$param);
    return $re;
}
/**
 * @access public
 * @return null
 * @context 查询服务分类名称
 */
function getServiceClassTitle($service_class_id){
    $title = Db::table('service_class')->where("id = $service_class_id")->value('service_class_title');
    return $title;

}
/**
 * @access function
 * @param string $address 地址
 * @param string $city 城市
 * @return array
 * @context 地理编码 经纬度
 */
/*function getLonLat($address, $city = null)
{
    $_receipt = null;
    $url = "https://restapi.amap.com/v3/geocode/geo?key=3f5396b83b7b09cfb811027943f8d7c7&address=" . trim($address) . "&city=" . trim($city);
    $_curl = new \base\Curl();
    $_receipt = $_curl->get($url);
    return json_decode($_receipt, true);
}*/
/*
 * @access public
 * @content 折扣类型 名称
 * @return bool
 * */
function getDiscountTypeTitle($mark){
    if(!empty($mark)){
        switch ($mark){
            case 1:
                $re='商品折扣';
                break;
            case 2:
                $re='服务折扣';
                break;
            case 3:
                $re='轮胎折扣';
                break;
            case 4:
                $re='洗车折扣';
                break;
            case 5:
                $re='喷漆折扣';
                break;
            case 6:
                $re='电瓶折扣';
                break;
            case 7:
                $re='玻璃折扣';
                break;

        }
        return $re;
    }


}
/*
 * @content 获取前端颜色键值
 * */
function getColorCase($color){
    $re=1;
    if(!empty($color)){
        switch ($color){
            case '#FFFFFF':
                $re=0;
                break;
            case "#999999":
                $re=1;
                break;
            case "#333333":
                $re=2;
                break;
            case '#E7373B':
                $re=3;
                break;
            case '#F6BC58':
                $re=4;
                break;
            case '#F3F015':
                $re=5;
                break;
            case '#8CC247':
                $re=6;
                break;
            case '#129295':
                $re=7;
                break;
            case '#3091F2':
                $re=8;
                break;
            case '#3DDEFB':
                $re=9;
                break;
            case '#EE85BF':
                $re=10;
                break;
            case '#9155D3':
                $re=11;
                break;
            case '#0A127C':
                $re=12;
                break;
            case '#6D1313':
                $re=13;
                break;

        }
        return $re;
    }
}
/**
 * @param NUll
 * @return string
 * @throws \think\Exception
 * @context 生成订单号
 */
function getOrderNumber()
{
    $rand =substr(mt_rand(12345678945,100000000000000000),4,4) ;
    $str = substr(date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8), 4, 16). $rand;
    return $str;
}
/**
 * @param type  service   在服务创建时  biz  在门店创建时
 * @param bizServiceId  门店id  或服务id
 * @return string
 * @throws \think\Exception
 * @context 创建 直营门店 关联 所有服务   创建服务 关联所有直营门店
 */
function  getRelationServiceBiz($type,$bizServiceId){
    $data = '';
    if($type == 'biz'){
        #在门店创建时
        #直营门店
        #查询所有服务
        $serviceId = \think\Db::table('service')->where("is_del = 2")->field('id')->select();
        $_array_service = array();
        if(!empty($serviceId)){
            foreach ($serviceId as $k=>$v){
                $_array_service[$k]['service_id'] = $v['id'];
                $_array_service[$k]['biz_id'] = $bizServiceId;

            }
            $data = \think\Db::table('service_biz')->insertAll($_array_service);
        }
    }else{
        #在服务创建时
        #查询所有直营店 biz_type = 1 biz_status 商户状态\n0:停用\n1:启用\n2:待审核
        $bizId = \think\Db::table('biz')->where("biz_type = 1 and biz_status = 1")->field('id')->select();
        $_array_service = array();
        if(!empty($bizId)){
            foreach ($bizId as $k=>$v){
                $_array_service[$k]['biz_id'] = $v['id'];
                $_array_service[$k]['service_id'] = $bizServiceId;

            }
            $data = \think\Db::table('service_biz')->insertAll($_array_service);
        }
    }
    return $data;

}
/*
 * @删除文件
 * */
function delFile($src){
    if (file_exists($src)) {
//                    $url = $_SERVER['CONTEXT_DOCUMENT_ROOT'] . "/uploads/" . $v;
//                    $url = $_SERVER['HTTP_ORIGIN'] . "/uploads/" . $v;
        unlink($src);
    }
}

/*
 * @content 获取表中value
 * */
function getTableValue($params = []){
    $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
    $data = \think\Db::name($params['table'])->where($where)->value($params['value']);
    if(!empty($data)){
        return $data;
    }
}

