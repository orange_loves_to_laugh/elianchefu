<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/17
 * Time: 13:16
 */

/**
 * @param $data
 * @content 添加门店收入表数据
 * @return array
 *   1=>'服务收入',2=>'办理会员',3=>'商品销售', 4=>'活动收入' ,5=>'生活超市' ,6=>'升级会员',7=>'会员冲值' ,8=>'推荐会员提成',
 * 9=>'推荐合伙人提成',10=>'会员消费提成',11=>'合伙人消费提成'  12,合作店抽成  13 商家抽成  14 商家推广费  15配件收入  16耗材收入  17 平台收入
 */
function addBizIncome($data)
{
    if (empty($data)) {
        return array('status' => false, 'msg' => '系统错误,参数信息为空');
    }
    $fieldArr = array(
        'biz_id'=>'门店id',
        'biz_income'=>'收入金额',
        'pay_type'=>'支付方式',
        'income_type'=>'收入类型'
    );
    $dataKey = array_keys($data);
    foreach ($fieldArr as $k=>$v){
        if(!in_array($k,$dataKey)){
            return array('status'=>false,'msg'=>$v.'不存在');
        }
    }
    \think\Db::table('biz_income')->insert(array(
        'biz_id' => $data['biz_id'],
        'biz_income' => $data['biz_income'],
        'pay_type' => $data['pay_type'],
        'pay_create' => date('Y-m-d H:i:s'),
        'member_id'=>$data['member_id']??0,
        'income_type'=>$data['income_type'],
        'pid'=>$data['pid']??0,
        'pay_year'=>date('Y',time()),
        'pay_month'=>date('m',time()),
        'pay_day'=>date('d',time()),
        'custommark'=>$data['custommark']??1,
        'pay_mode'=>$data['pay_model']??1,
        'order_number'=>$data['order_number']
    ));
    return array('status'=>true,'msg'=>'添加成功');
}

/**
 * 获取毫秒级别的时间戳
 */
function getMillisecond()
{
    //获取毫秒的时间戳
    $time = explode(" ", microtime());
    $time = $time[1] . ($time[0] * 1000);
    $time2 = explode(".", $time);
    $time = $time2[0];
    return $time;
}

/**
 * @return bool
 * @context 激活卡券在小年到除夕不可使用
 */
function voucherDeadLine()
{
    if (date("Y-m-d H:i") >= '2022-01-21 21:00' and date("Y-m-d H:i") <= '2022-02-06 06:00') {
        return false;
    }
    return true;
}

/**
 * 本周的开始日期
 * @param bool $His 是否展示时分秒 默认true
 * @return false|string
 */
function beginWeek($His = true)
{
    $timestamp = mktime(0, 0, 0, date('m'), date('d') - date('w'), date('Y'));
    return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
}

/**
 * 本周的结束日期
 * @param bool $His 是否展示时分秒 默认true
 * @return false|string
 */
function endWeek($His = true)
{
    $timestamp = mktime(23, 59, 59, date('m'), date('d') - date('w') + 7, date('Y'));
    return $His ? date('Y-m-d H:i:s', $timestamp) : date('Y-m-d', $timestamp);
}

/**
 * @param int $begin_time 开始时间戳
 * @param int $end_time 结束时间戳
 * @param boolean $mark 时间格式  true=>时间戳  false=>日期格式
 * @return array
 * @content  计算两个时间戳之间相差的日时分秒
 */
function timediff($begin_time, $end_time, $mark = true)
{
    if ($mark == false) {
        # 日期格式,将时间转为时间戳格式
        $begin_time = strtotime($begin_time);
        $end_time = strtotime($end_time);
    }
    # 判断时间大小
    if ($begin_time < $end_time) {
        $starttime = $begin_time;
        $endtime = $end_time;
    } else {
        $starttime = $end_time;
        $endtime = $begin_time;
    }

    //计算天数
    $timediff = $endtime - $starttime;
    $days = intval($timediff / 86400);
    //计算小时数
    $remain = $timediff % 86400;
    $hours = intval($remain / 3600);
    //计算分钟数
    $remain = $remain % 3600;
    $mins = intval($remain / 60);
    //计算秒数
    $secs = $remain % 60;
    $res = array("day" => $days, "hour" => $hours, "min" => $mins, "sec" => $secs, 'timediff' => $timediff);
    return $res;
}

/**
 * @param $date1
 * @param $date2
 * @param string $tags
 * @return float|int
 * @context 计算两个日期之间相差几个月
 */
function getMonthNum( $date1, $date2, $tags='-' ){
    $date1 = explode($tags,$date1);
    $date2 = explode($tags,$date2);
    return abs($date1[0] - $date2[0]) * 12 + abs($date1[1] - $date2[1]);
}

/**
 * @param $str
 * @param int $index
 * @param string $setStr
 * @return string
 * @content 字符串指定位置加指定字符
 */
function addAStr($str, $index = 2, $setStr = '·')
{
    return mb_substr($str, 0, $index) . $setStr . mb_substr($str, $index);
}

/**
 * @param $imgUrl
 * @param $mark
 * @return string
 * @content 图片路径整理(返回 http/https)
 */
function imgUrl($imgUrl, $mark = false)
{
    if (!empty($imgUrl) and !is_null($imgUrl)) {
        if (preg_match('/(http:\/\/)|(https:\/\/)/i', $imgUrl)) {
            return $imgUrl;
        }
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        if (!$mark) {
            if (file_exists(ROOT_PATH . "upload/" . $imgUrl)) {
                $imgUrl = str_replace('\\', '/', $imgUrl);
                return $http_type . $_SERVER['HTTP_HOST'] . "/upload/" . $imgUrl;
            } else {
                return "";
            }
        } else {
            if (file_exists(ROOT_PATH . $imgUrl)) {
                $imgUrl = str_replace('\\', '/', $imgUrl);
                return $http_type . $_SERVER['HTTP_HOST'] . '/' . $imgUrl;
            } else {
                return "";
            }
        }
    } else {
        return "";
    }
}

/**
 * @param $idCard
 * @return array
 * @content 通过身份证号获取年龄
 */
function getAge($idCard)
{

# 1.从身份证中获取出生日期
    $birth_Date = strtotime(substr($idCard, 6, 8));//截取日期并转为时间戳

# 2.格式化[出生日期]
    $Year = date('Y', $birth_Date);//yyyy
    $Month = date('m', $birth_Date);//mm
    $Day = date('d', $birth_Date);//dd

# 3.格式化[当前日期]
    $current_Y = date('Y');//yyyy
    $current_M = date('m');//mm
    $current_D = date('d');//dd

# 4.计算年龄()
    $age = $current_Y - $Year;//今年减去生日年
    if ($Month > $current_M || $Month == $current_M && $Day > $current_D) {//深层判断(日)
        $age--;//如果出生月大于当前月或出生月等于当前月但出生日大于当前日则减一岁
    }
# 返回
    return array('age' => $age, 'year' => $Year, 'month' => $Month, 'day' => $Day, 'born' => date('Y-m-d', $birth_Date));

}

/**
 * @param $sex
 * @return string
 * @content 性别转换
 */
function getSexInfo($sex)
{
    switch ($sex) {
        case '男':
            return 1;
            break;
        case '女':
            return 0;
            break;
    }
}

/**
 * @access public
 * @param int $_sex_number
 * @return  string
 * @context 性别查询展示
 */
function getSex($_sex_number)
{
    switch (intval($_sex_number)) {
        case 2:
            return "未设置";
            break;
        case 1:
            return "男";
            break;
        case 0:
        default:
            return "女";
            break;
    }
}

/**
 * @param $_type
 * @return string
 * @content 用户状态(普通/合伙人)
 */
function getMemberType($_type, $is_status = false)
{
    $id = \think\Db::name('member_partner')->where('status=1 and member_id=' . $_type)->value('id');

    if (!empty($id)) {

        return $is_status ? '是' : "推广员";
    }

    return $is_status ? '否' : "普通用户";
//    switch (intval($_type)) {
//        case 1:
//        default:
//            return "普通";
//            break;
//        case 2:
//            return "合伙人";
//            break;
//    }
}

/**
 * @param  int $pid 用户id
 * @return bool|mixed|string
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 * @context 根据用户id 查找用户姓名(用户详情推荐人展示)
 */
function getMemberName($pid)
{
    $_receipt = null;
    if ($pid) {
        $_receipt = \think\Db::table('member')->field(array('member_name', 'nickname'))->where(array('id' => intval($pid)))->find();
        if (!empty($_receipt)) {
            if (!empty($_receipt['member_name'])) {
                $_res = $_receipt['member_name'];
            } else {
                $_res = base64_decode($_receipt['nickname']);
            }
        } else {
            $_res = '无';
        }
    } else {
        $_res = '无';
    }
    return $_res;
}

/**
 * @param int $id 员工id
 * @return mixed|string
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 * @content 根据员工id 查询员工姓名
 */
function getEmployeeName($id)
{
    $_return = '无';
    if (!empty($id)) {
        $_return = \think\Db::table('employee')->field('employee_name name')->where(array('id' => intval($id)))->find()['name'];
    }
    return $_return;
}

/**
 * @param int $id 门店id
 * @return mixed|string
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 * @content 根据门店id查询门店名称
 */
function getBizTitle($id)
{
    $_return = '';
    if (!empty($_return)) {
        $_return = \think\Db::table('biz')->field('biz_title title')->where(array('id' => intval($id)))->find()['title'];
    }
    return $_return;
}

/**
 * @param int $type
 * @return mixed
 * @content 获取保养类型名称、公里数、价格
 */
function getMaintainType($type = -1)
{
    if ($type == -1) {
        return include("MaintainType.cfg.php");
    } else {
        $typeInfo = include("MaintainType.cfg.php");
        return $typeInfo[$type];
    }
}

/**
 * @access public
 * @param int $type 支付类型
 * @return string
 * @context 支付类型
 */
function getpay_type($type)
{
    switch (intval($type)) {
        case 0:
            return "";
            break;
        case 1:
        default:
            return "微信支付";
            break;
        case 2:
            return "支付宝支付";
            break;
        case 3:
            return "银联支付";
            break;
        case 4:
            return "现金支付";
            break;
        case 5:
            return "余额支付";
            break;
        case 6:
            return "卡券核销";
            break;
        case 7:
            return "积分兑换";
            break;
    }
}

/**
 * @access public
 * @param $_type
 * @return string
 * @context 积分来源
 */
function getIntegralType($_type)
{
    switch (intval($_type)) {
        case -12:
            return "线上购买商品额外赠送";
            break;
        case -11:
            return "线下充值";
            break;
        case -10:
            return "线下升级";
            break;
        case -6:
            return "线下储值";
            break;
        case -3:
            return "门店购买服务";
            break;
        case -2:
            return "评论分享";
            break;
        case -1:
            return "实名认证";
            break;
        case 0:
        default:
            return "额外赠送";
            break;
        case 1:
            return "首次登陆";
            break;
        case 2:
            return "分享注册";
            break;
        case 3:
            return "线上购买服务";
            break;
        case 4:
            return "评论订单";
            break;
        case 5:
            return "积分兑换";
            break;
        case 6:
            return "线上储值";
            break;
        case 7:
            return "关注公众号";
            break;
        case 8:
            return "分享文章";
            break;
        case 9:
            return "注册赠送";
            break;
        case 10:
            return "线上升级";
            break;
        case 11:
            return "线上充值";
            break;
        case 12:
            return "门店购买商品额外赠送";
            break;
        case 13:
            return "活动赠送";
            break;
        case 14:
            return "查看文章";
            break;
        case 15:
            return "分享服务";
            break;
        case 16:
            return "分享商家";
            break;
        case 17:
            return "分享二手车";
            break;
        case 18:
            return "分享用户";
            break;
        case 19:
            return "推荐会员办理";
            break;
        case 20:
            return "推荐用户充值";
            break;
        case 21:
            return "推荐用户升级";
            break;
        case 22:
            return "下载APP";
            break;
        case 23:
            return "成为推广员";
            break;
        case 24:
            return "签到奖励";
            break;
        case 25:
            return "商家推广";
            break;
        case 26:
            return "分享联盟商家";
            break;
        case 27:
            return "商家支付";
            break;
        case 28:
            return "任务奖励";
            break;
        case 29:
            return "服务消费";
            break;
        case 30:
            return "物业缴费";
            break;
        case 31:
            return "拍视频";
            break;
        case 32:
            return "转发视频";
            break;
        case 33:
            return "会员赠送";
            break;
        case 34:
            return "看视频奖励";
            break;
    }
}

/**
 * @access public
 * @param $_type
 * @return string
 * @context 套餐赠送类型
 */
function givingTypeTitle($_type)
{
    switch (intval($_type)) {
        case 0:
        default:
            return "额外赠送";
            break;
        case 1:
            return "商品";
            break;
        case 2:
            return "服务";
            break;
        case 3:
            return "红包";
            break;
        case 7:
            return "联盟商家";
            break;
        case 8:
            return "本地生活";
            break;
        case 9:
            return "物业消费";
            break;
        case 10:
            return "有派精品购";
            break;
        case 11:
            return "服务通用券";
            break;
        case 12:
            return "商品通用券";
            break;
        case 13:
            return "全部通用券";
            break;
        case 14:
            return "服务分类券";
            break;
        case 15:
            return "商品分类券";
            break;
        case 16:
            return "全部分类券";
            break;
    }
}

/**
 * @param $mark
 * @return int
 * @content 添加积分记录时使用 的  积分来源
 */
function integralSource($mark)
{
    switch ($mark) {
        # 分享服务
        case 'shareService':
            return 15;
            break;
        # 查看文章
        case 'look_article_integral':
            return 14;
            break;
        # 分享文章
        case 'is_share_club':
            return 8;
            break;
        # 分享商家
        case 'share_busines_integral':
            return 16;
            break;
        # 分享二手车
        case 'share_car_integral':
            return 17;
            break;
        # 分享会员
        case 'shareVip':
            return 18;
            break;
        # 分享评论
        case 'is_forward':
            return -2;
            break;
        default:
            return 0;
            break;
    }
}

