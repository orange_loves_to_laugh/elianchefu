<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/7/2
 * Time: 9:35
 */

namespace app\reuse\controller;


/**
 * 返回json格式数据
 * Trait ResponseJson
 * @package app\reuse\controller
 */
trait ResponseJson
{

    /**
     * @param array $data
     * @param boolean $type true=>返回json,false=>返回数组
     * @return string
     * 成功时返回数据
     */
    public function jsonSuccessData($data = [], $type = false)
    {
        return $this->jsonResponse(0, 'success', $data, $type);
    }

    /**
     * @param $code
     * @param $msg
     * @param array $data
     * @param boolean $type true=>返回json,false=>返回数组
     * @return string
     * 失败时返回数据
     */
    public function jsonData($code, $msg, $data = [], $type = false)
    {
        return $this->jsonResponse($code, $msg, $data, $type);
    }

    /**
     * @param int $code 返回码
     * @param string $msg 提示信息
     * @param array $data 返回数据
     * @param boolean $type true=>返回json,false=>返回数组
     * @return string
     * 返回json
     */
    private function jsonResponse($code, $msg, $data, $type)
    {
        $content = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        );
        if ($type) {
            return json_encode($content);
        } else {
            return $content;
        }
    }


}