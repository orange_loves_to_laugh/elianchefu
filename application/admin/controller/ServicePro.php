<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/14
 * Time: 16:11
 */

namespace app\admin\controller;
use app\service\ActiveService;
use app\service\AdminService;
use think\Db;
use think\facade\Session;
/*
 * @content :查询服务卡券  商品 卡券 信息  弹窗样式
 * */
class ServicePro extends Common
{
    /**
     * @content :[ 服务  商品 搜索名称]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function active_search(){
        $params = input("post.");
        if(isset($params['days_id'])){
            #签到天数的id
            session::set('daysId',$params['days_id']);
        }
        $params['value'] = strim($params['value']);
        #获取服务 或商品 搜索的名称
        #查询 服务 和商品 的名字  返回html 赋值到页面
        $data = self::activeTypeSearch($params);

        if($data['html'] == false){
            return array('status'=>false,'msg'=>'暂无数据');
        }else{
            return array('status'=>true,'data'=>$data['html'],'list'=>$data['list']);

        }

    }
    #获取服务 商品 详情 信息
    static function activeTypeSearch($params){
        if($params['type'] == 'pro'){
            #商品
            $_where ='biz_pro_paret=1 and biz_pro_status != 3';
            #搜索商品名称查询
            if(!empty($params['value'])){
                $_where .= " and b.biz_pro_title like '%".$params['value']."%'";

            }
            $list = Db::table('biz_pro b')
                ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
                ->join('pro_class pc','pc.id = b.biz_pro_class_id')
                ->where($_where)->select();

        }else if($params['type'] == 'service'){
            #服务
            $_where='';
            #搜索商品名称查询
            if(!empty($params['value'])){
                $_where .= " and s.service_title like '%".$params['value']."%'";

            }
            // 条件
            $list = Db::table('service s')
                ->field('s.id,s.service_title,s.service_time,s.appoint,s.settlement_min,sc.service_class_title')
                ->join('service_class sc','sc.id = s.service_class_id')
                ->where("s.is_del = 2 and s.service_status = 1".$_where)
                ->select();
        }
        if(empty($list)){
            return false;
        }

        $res = self::getServiceProInfo($list,$params['type'],'','',$params['mark'],$params['mtype']);
        return ['html'=>$res,'list'=>$list];



    }
    /*
     * @content:商品 和服务  的html
     * @params:$arr :数组
     * @params:$type : Pro  商品  service ：服务
     * @params:$search_title :显示名称
     * @params:$select_value :搜索名称
     * @params:$mark : type 是活动套餐(类型) 选择 give  活动赠送选择
     * */
    static function getServiceProInfo($arr,$type,$search_title='',$select_value='',$mark,$mtype=''){

        if(!empty($arr) and is_array($arr)){
            if($type == 'pro'){
                #商品

                    $str=" 
              <input type='hidden' name='activeType' id='activeType' value='pro'>
              <input type='hidden' name='mark' id='mark' value='$mark'>
              <input type='hidden' name='mtype' id='mtype' value='$mtype'>
              <div class=\"row\" style=\"width: 100 %;float: left\">
                    <div class=\"col-sm-6\">
                        <div class=\"form-group\">
                            <input type='text' id='search_title' data-type='pro' value='{$search_title}'  class=\"form-control\" onkeyup=\"searchServiceName()\" placeholder='请输入商品名称,回车键搜索' autocomplete=\"off\">
                            <i class=\"form - group__bar\"></i>
                        </div>
                    </div>
                </div>
                                                                            <br>
              
                                                                    
             <div class='card-body'  >
                                        <div class='col-sm-12'>
                                      
                               
                                    <table class='table table-hover mb-0'  >
                                        <thead>
                                            <tr>
                                             <!--   <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>-->
                                                <th>商品名称</th>
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                    foreach($arr as $k=>$v){
                        $str.="     <tr class='service".$v['id']."'>
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$v['class_title']}</td>
                        <td>" . getsPriceformat($v['biz_pro_purch']) . "</td>
                      
                        <td> <button class=\"btn btn-info\" onclick=\"aloneOption_new('{$v['id']}','pro','".$mark."','".$mtype."')\" style=\"\">确定</button></td>
                    </tr>";
                    }
                    $str.="
                </tbody>
            </table>
        </div>
            </div>";



            }
            else{
                #服务
                $str=" 
              <div class=\"row\" style=\"width: 50 %;float: left\">
                            <input type='hidden' name='activeType' id='activeType' value='service'>
                            <input type='hidden' name='mark' id='mark' value='$mark'>
                            <input type='hidden' name='mtype' id='mtype' value='$mtype'>

                            <div class='col-sm-12'>
                                <div class='form-group'>
                                    <input type='text' data-type='service' id='search_title' value='{$search_title}'  class=\"form-control\" onkeyup='searchServiceName()' placeholder='请输入服务名称，回车键搜索' autocomplete=\"off\">
                                    <i class=\"form-group__bar\"></i>
                                </div>
                            </div>
                        </div>
               
                <br>
 <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                      
                                 <!--   <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            <button class=\"btn btn-info\" onclick=\"serviceOption('check-item')\" style=\"\">批量关联</button>
                                      
                                        </div>
                                    </div>-->
                                    <table class='table table-hover mb-0'  >
                                        <thead>
                                            <tr>
                                             <!--   <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>-->
                                                <th>服务名称</th>
                                                <th>服务分类</th>
                                                <th>服务价格</th>
                                                <th>服务时长</th>
                                                <th>是否预约</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                foreach($arr as $k=>$v){
                    if($v['appoint'] == 1){
                        $appoint='是';

                    }else{
                        $appoint='是';

                    }
                    /*    <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ></td>";*/



                    $str.=" <tr class='service".$v['id']."'>
                        <td>{$v['service_title']}</td>
                        <td>{$v['service_class_title']}</td>
                        <td><button class=\"btn btn-info\" onclick=\"checkServiceCarPrice('{$v['id']}')\" >查看</button></td>
                        <td>{$v['service_time']}</td>
                        <td>$appoint</td>
                        <td> <button class=\"btn btn-info\" onclick=\"aloneOption_new('{$v['id']}','service','".$mark."','".$mtype."')\" style=\"\">确定</button></td>
                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            </div>";
            }
            return $str;

        }

    }
    /*
     * @content 商品  服务 存 session
     *
     * */
    function serviceProSession(){
        $activeId = Session('activeId');
        $params = input("post.");
        if($params['prize_type'] == 'pro'){
            $title = '商品二级分类';
            $active_mold = 1;
        }else if($params['prize_type'] == 'service'){
            $title = '服务名称';
            $active_mold = 2;

        }else{
            $title ='';
            $active_mold = 3;

        }
        $data  = ActiveService::activeSession($params,$activeId);

        #算下 服务价格总共多少
        return array('status' => true, 'data' => $data['str'], 'title' => $title,'totalPrice'=>$data['totalPrice'],'active_mold'=>$active_mold,'active_explain'=>$data['active_explain']);
    }
    /**
     * [searchServiceName 搜索服务名称   和商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function searchServiceName(){
        #服务 商品  名称
        $search_title = strim(input("post.search_title"));
        if(empty($search_title))
            $search_title =strim(input("post.searchTitle"));
        #service :服务 pro :商品
        $activeType = input("post.activeType");
        if(empty($activeType))
            $activeType =input("post.searchType");
        #筛选的分类
        $select_biztype = input("post.selectValue");
        if(empty($select_biztype))
            $select_biztype =input("post.selectValue");
        #获取type 是活动套餐(类型) 选择 give  活动赠送选择
        $mark= input("post.mark");
        #mtype redPacket:红包标识
        $mtype= input("post.mtype");
        $_where='';
        if($activeType =='service'){
            if(!empty($search_title))
                $_where .= " and s.service_title like '%".$search_title."%'";
            if(!empty($select_biztype))
                $_where .= " and s.service_class_id =$select_biztype";
            # 查询服务名称
            $service = Db::table('service s')
                ->field('s.id,s.service_title,s.service_time,s.appoint,s.settlement_min,sc.service_class_title')
                ->join('service_class sc','sc.id = s.service_class_id')
                ->where("s.is_del = 2 and s.service_status = 1".$_where)->select();
      /*      $countService = Db::table('service_biz sb')
                ->where(array('sb.biz_id'=>$biz_id))->count();*/
            if(!empty($service)) {
               /* #求缓存的 数组个数
                $_session = session('serviceInfoSession');
                if(!empty($_session))
                    $count_session = count($_session);
                $countService = intval($countService)+intval($count_session);*/
                #关联服务权限模板
                $str = ServicePro::getServiceProInfo($service, $activeType,$search_title,$select_biztype,$mark,$mtype);
                return array('status' => true, 'data' => $str,'list'=>$service);

            }else{
                return array("status"=>false,'msg'=>'暂无服务数据');
            }


        }else{
            if(!empty($search_title))
                $_where .= " and b.biz_pro_title like '%".$search_title."%'";
            if(!empty($select_biztype))
                $_where .= " and b.biz_pro_class_id =$select_biztype";
            # 查询服务名称
            # 查询商品名称
            $pro = Db::table('biz_pro b')
                ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
                ->join('pro_class pc','pc.id = b.biz_pro_class_id')
                ->where("biz_pro_status != 3".$_where)->select();

           /* $countBizpro = Db::table('biz_goods bg')
                ->where(array('bg.biz_id' => $biz_id))->count();*/
            if(!empty($pro)) {
                #求缓存的 数组个数
             /*   $_session = session('bizproInfoSession');
                if(!empty($_session))
                    $count_session = count($_session);
                $countBizpro = intval($countBizpro)+intval($count_session);*/
                #关联服务权限模板
                $str = ServicePro::getServiceProInfo($pro,$activeType,$search_title,$select_biztype,$mark,$mtype);
                return array('status' => true, 'data' => $str);

            }else{
                return array("status"=>false,'msg'=>'暂无数据');
            }
        }


    }
}