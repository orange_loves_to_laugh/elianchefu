<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/17
 * Time: 9:09
 */

namespace app\admin\controller;


use app\reuse\controller\ResponseJson;
use app\service\ProclassService;
use BaseException;
use think\App;
use think\Controller;
use think\Db;
use think\Exception;
use think\facade\Session;

class Pro extends Common
{
    use ResponseJson;
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日13:45:48
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }
    function class_index()
    {
        $order="id desc";
        $where=" pc.id !=0 ";
        $title=input("get.title");
        $this->assign("title",$title);
        if(!empty($title)){
            $where.=" and pc.class_title like '%".$title."%'";
            $order="class_id asc";
        }
        $class_type=input("get.class_type");
        $this->assign("class_type",$class_type);
        if(!empty($class_type)){
            $where.=" and pc.class_type={$class_type}";
            $order="class_id asc";
        }
        $list=Db::table("pro_class pc")
            ->field("pc.class_title,pc.id,pc.class_type,pc.class_id,if(pc.class_id=0,'最高级',ppc.class_title) p_title,pc.status")
            ->join("pro_class ppc","pc.class_id=ppc.id",'left')
            ->order("status,".$order)
            ->where($where)
            ->paginate(9,false,['query'=>request()->param()]);

        $page=$list->render();
        $this->assign("page",$page);
        $this->assign("list",$list->toArray()['data']);
        $this->assign("request",http_build_query($this->request->param(),"&"));
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @throws BaseException
     * @throws \think\Exception
     * @context 商品分类添加
     */
    function class_add()
    {
        if(input("get.active")=='create'){
            $param=input("post.");
            try{
                Db::table("pro_class")->insert($param);
                # 更新类型
                ProclassService::updateProClass($param['class_type']);
                return array("status"=>true);
            }catch(Exception $e){
                throw new BaseException(array("code"=>403,"msg"=>"商品分类添加错误","errorcode"=>10002,"status"=>false));
            }
        }else{

            return $this->fetch();
        }
    }

    /**
     * @return array|mixed
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 分类修改
     */
    function class_write()
    {
        if(input("get.active")=="write"){
            $param=input("post.");
            $id=$param['id'];
            unset($param['id']);
            $res=Db::table("pro_class")->where(array("id"=>$id))->update($param);
            # 更新类型
            ProclassService::updateProClass($param['class_type']);
            if($res){
                return array("status"=>true);
            }else{
                return array("status"=>true,"msg"=>"修改失败");
            }
        }else{
            $request=request()->param();
            $info=Db::table("pro_class")->where(array("id"=>$request['id']))->find();
            $this->assign("info",$info);
            $this->assign("request", http_build_query($request,"&"));
            return $this->fetch();
        }
    }

    /**
     * @return array
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 分类删除
     */
    function class_delete()
    {
        $id=input("post.id");
        $res=Db::table("pro_class")->where(array("id"=>$id))->delete();
        if($res){
            return array("status"=>true);
        }else{
            return array("status"=>false);
        }
    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 后台获取商品分类
     */
    function adminGetProClass()
    {
        $type=input("post.type");
        $class_id=input("post.class_id");
        $now_id=input("post.now_id");
        $class=ProclassService::getsProClass($type);
        if(!empty($class)){
            $str=null;
            foreach($class as $k=>$v){
                if(!empty($now_id) and $v['value']==$now_id){
                    continue;
                }
                $str.=" <option value=\"{$v['value']}\"";
                if(!empty($class_id) and $class_id==$v['value']){
                    $str.=" selected";
                }
                $str.=" >".$v['label']."</option>";
            }
            return array("status"=>true,"data"=>$str);
        }else{
            return array("status"=>false);
        }
    }

    /**
     * @return array
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 分类状态切换
     */
    function changeStatus()
    {
        $status=input("post.status");
        $id=input("post.id");
        Db::table("pro_class")->where(array("id"=>$id))->update(array("status"=>$status));
        return array("status"=>true);
    }

    /**
     * @return array
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 商品状态切换
     */
    function changeProStatus()
    {
        $status=input("post.status");
        $id=input("post.id");
        Db::table("biz_pro")->where(array("id"=>$id))->update(array("biz_pro_status"=>$status));
        return array("status"=>true);
    }


    function pro_index()
    {
        $where="bp.id >0";
        $search=input("get.search");
        $this->assign('search',$search);
        if(!empty($search)){
            $where.=" and (bp.biz_pro_title like '%".$search."%' or bp.biz_pro_number='".$search."')";
        }
        $class_id=input("get.class_id");
        $this->assign("class_id",$class_id);
        if(!empty($class_id)){
            $where.=" and bp.biz_pro_class_id={$class_id}";
        }
        # 判断是否存在进货价格排序
        $_bizProPurch = input('get.bizProPurch');
        $this->assign('bizProPurch',$_bizProPurch);
        $bizId = input('get.biz_id')??0;
        if(!empty($bizId)) {
            $where .= " and bp.biz_pid={$bizId}";
        }
        $list=Db::table("biz_pro bp")
            ->field("bp.*,pc.class_title,(select count(id) from biz_goods where pro_id=bp.id) relevancestore")
            ->join("pro_class pc","bp.biz_pro_class_id=pc.id and pc.class_type=2","left")
            ->where("bp.biz_pro_status<3 and bp.biz_pro_paret=1")
            ->where($where)
            ->order("bp.id desc,bp.biz_pro_purch $_bizProPurch,bp.biz_pro_status")
            ->paginate(9,false,['query'=>request()->param()]);
        $page=$list->render();
        $this->assign('biz_id',$bizId);
        $this->assign('page',$page);
        $this->assign('list',$list->toArray()['data']);
        $this->assign("request",http_build_query($this->request->param(),"&"));
        $class=ProclassService::getsProClass(2);
        $this->assign("class",$class);
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商品添加
     */
    function pro_add()
    {
        if(input("get.active")=="create"){
            $param=input("post.");
            $param['biz_pro_create']=date("Y-m-d H:i:s");
            $param['biz_pro_barcode']=$param['biz_pro_number'];
            $param['biz_delivery']=1;
//            $level=$param['level'];
            unset($param['level']);
            $discount=$param['discount'];
            unset($param['discount']);
//            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $res=Db::table("biz_pro")->insertGetId($param);
            if($res){
                #创建卡券  添加一个商品 之后 同时创建一个卡券，默认有效期30天
                Db::table('card_voucher')->insert(array(
                    'card_title'=>$param['biz_pro_title'].'券',
                    'card_detile'=>$param['biz_pro_title'].'商品卡券',
                    'card_time'=>90,
                    'card_price'=>$param['biz_pro_online'],
                    'card_create'=>date('Y-m-d H:i:s'),
                    'server_id'=>$res,
                    'card_type_id'=>3

                ));
                # 添加折扣
              /*  $_discount_array=array();
                foreach($level as $k=>$v){
                    array_push($_discount_array,array("biz_pro_id"=>$res,"member_level"=>$v,"discount"=>$discount[$k]));
                }
                if(!empty($_discount_array)){
                    Db::table("biz_pro_mediscount")->insertAll($_discount_array);
                }*/
                # 添加关联门店
                    $relevance_store_array=session('bizStoreInfoSession');
                    if(!empty($relevance_store_array)){
                        $_store_array=array();
                        foreach($relevance_store_array as $k=>$v){
                            array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                                "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                        }
                    }

                    if(!empty($_store_array)){
                        Db::table("biz_goods")->insertAll($_store_array);
                    }
                    Session::delete('bizStoreInfoSession');
                    Session::delete('proId');
                return array("status"=>true);
            }else{
                return array("status"=>false);
            }
        }else{
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');

            # 查询会员等级
            $level=Db::table("member_level")->field("level_title title,id")->select();
            $this->assign('level',$level);
            #查询 关联门店的数量
          /*  $bizNum = self::getbizNum();
            $this->assign('bizNum',$bizNum);*/


            return $this->fetch();
        }
    }
    /*
     * @content : 获取关联门店的数量
     * @params : id  :商品id
     * */
    /*static function getbizNum($id=''){
        if(!empty($id)){
            $num = Db::table('biz_goods')->where(array('pro_id'=>$id))->count();
        }else{
            $num =0;
        }
        $_session = session('bizStoreInfoSession');
        if(!empty($_session)){
            $_snum = count($_session);
        }else{
            $_snum = 0;
        }
        $_count = intval($num)+intval($_snum);
        return $_count;

    }*/
    /**
     * @return array
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 商品删除
     */
    function pro_delete()
    {
        $id=input("post.id");
        Db::table("biz_pro")->where(array("id"=>$id))->update(array("biz_pro_status"=>3));
        return array("status"=>true);
    }

    /**
     * @return array|mixed
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 商品修改
     */
    function pro_write()
    {
        if(input("get.active")=='write'){
            $param=input("post.");
            $id=$param['id'];
            unset($param['id']);
//            $level=$param['level'];
            unset($param['level']);
            $discount=$param['discount'];
            unset($param['discount']);
            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $res=Db::table("biz_pro")->where(array("id"=>$id))->update($param);
            #修改卡券  修改一个商品 之后 同时修改卡券，
            Db::table('card_voucher')->where(array('server_id'=>$id,'card_type_id'=>3))->update(array(
                'card_title'=>$param['biz_pro_title'].'券',
                'card_detile'=>$param['biz_pro_title'].'商品卡券',
                'card_price'=>$param['biz_pro_online'],
//                'card_time'=>90,

            ));
        /*    # 修改折扣
            foreach($level as $k=>$v){
                Db::table("biz_pro_mediscount")->where(array("biz_pro_id"=>$id,"member_level"=>$v))->update(array("discount"=>$discount[$k]));
            }*/
            # 修改关联门店
            /*if(!empty($relevance_store)){
                $relevance_store_array=explode(',',$relevance_store);
                $_store_array=array();
                # 查询原来的关联门店
                $old=Db::table("biz_goods")->field("biz_id")->where(array("pro_id"=>$id))->select();
                $old_coll=array_column($old,"biz_id");
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    if(in_array($v,$old_coll)){
                        # 进行修改
                        Db::table("biz_goods")->where(array("pro_id"=>$id,"biz_id"=>$v))->update(array("goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch']));
                    }else{
                        # 进行添加
                        Db::table("biz_goods")->insert(array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    }
                }
                # 删除不在修改的关联里的门店
                Db::table("biz_goods")->where("pro_id = {$id} and biz_id not in ($relevance_store)")->delete();
            }else{
                # 删除关联的门店
                Db::table("biz_goods")->where(array("pro_id"=>$id))->delete();
            }*/
            $relevance_store_array=session('bizStoreInfoSession');
            if(!empty($relevance_store_array)){
                $_store_array=array();
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                }
            }
            if(!empty($_store_array)){
                Db::table("biz_goods")->insertAll($_store_array);
            }
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');
            return array("status"=>true);
        }else{
            Session::delete('bizStoreInfoSession');
            $id=input("get.id");
            session::set('proId',$id);
            $info=Db::table("biz_pro")->where(array("id"=>$id))->find();
            $this->assign('info',$info);
            # 查询会员折扣
            $discount=Db::table("biz_pro_mediscount bpm")
                ->field("bpm.discount,ml.level_title title,ml.id")
                ->join("member_level ml",'ml.id=bpm.member_level')
                ->where(array("bpm.biz_pro_id"=>$id))
                ->select();
            $this->assign('level',$discount);
            # 查询关联门店并用，连接成字符串
            $relevance_store=Db::table("biz_goods bg")->field("group_concat(biz_id) coll")->where(array("pro_id"=>$id))->group("bg.pro_id")->find();
            $this->assign('relevance_store',$relevance_store['coll']);
            $request=request()->param();
            $this->assign("request", http_build_query($request,"&"));

            #查询 商品是否关联服务
            $serviceproCount = Db::table('log_servicepro')->where("pro_id = $id")->count('service_id');
            $this->assign('serviceproCount',$serviceproCount);
            return $this->fetch();
        }
    }

    function paret_index()
    {
        $where="bp.id >0";
        $search=input("get.search");
        $this->assign('search',$search);
        if(!empty($search)){
            $where.=" and (bp.biz_pro_title like '%".$search."%' or bp.biz_pro_number='".$search."')";
        }
        $search_type = input("get.search_type");
        $search_oe = input("get.search_oe");
        if(!empty($search_type)){
            $where.=" and biz_pro_paret_type = $search_type";
            $this->assign('search_type',$search_type);
        }
        if(!empty($search_oe)){
            $where.=" and biz_pro_number = '$search_oe'";
            $this->assign('search_oe',$search_oe);

        }
        $class_id=input("get.class_id");
        $this->assign("class_id",$class_id);
        if(!empty($class_id)){
            $where.=" and bp.biz_pro_class_id={$class_id}";
        }
        $list=Db::table("biz_pro bp")
            ->field("bp.*,(select count(id) from biz_goods where pro_id=bp.id) relevancestore,(select count(id) from oe_adapt oea where oea.oe_number=bp.biz_pro_number) adaptermodels")
            ->where("bp.biz_pro_status<3 and bp.biz_pro_paret=2")
            ->where($where)
            ->order("bp.biz_pro_status,bp.id desc")
            ->paginate(9,false,['query'=>request()->param()]);
        $page=$list->render();

        $this->assign('page',$page);
        $this->assign('list',$list->toArray()['data']);
        $this->assign("request",http_build_query($this->request->param(),"&"));
        #查询 配件适配车型
        $oe_adapt = Db::table('oe_adapt')->group('oe_number')->select();
        $this->assign('oe_adapt',$oe_adapt);
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 配件添加
     */
    function paret_add()
    {
        if(input("get.active")=='create'){
            $param=input("post.");
            $param['biz_pro_create']=date("Y-m-d H:i:s");
            $param['biz_pro_barcode']=$param['biz_pro_number'];
            $param['biz_pro_oe']=$param['biz_pro_number'];
            $param['biz_delivery']=1;
            $param['biz_pro_paret']=2;
            $level=$param['level'];
            unset($param['level']);
            $discount=$param['discount'];
            unset($param['discount']);
            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $adaptermodels=json_decode($param["adaptermodels"],true);
            unset($param["adaptermodels"]);
            $res=Db::table("biz_pro")->insertGetId($param);
            if($res){
                # 添加折扣
               /* $_discount_array=array();
                foreach($level as $k=>$v){
                    array_push($_discount_array,array("biz_pro_id"=>$res,"member_level"=>$v,"discount"=>$discount[$k]));
                }
                if(!empty($_discount_array)){
                    Db::table("biz_pro_mediscount")->insertAll($_discount_array);
                }*/
            /*    # 添加关联门店
                if(!empty($relevance_store)){
                    $relevance_store_array=explode(',',$relevance_store);
                    $_store_array=array();
                    foreach($relevance_store_array as $k=>$v){
                        array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    }
                    if(!empty($_store_array)){
                        Db::table("biz_goods")->insertAll($_store_array);
                    }
                }*/
                # 添加关联门店
                $relevance_store_array=session('bizStoreInfoSession');
                if(!empty($relevance_store_array)){
                    $_store_array=array();
                    foreach($relevance_store_array as $k=>$v){
                        array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                    }
                }

                if(!empty($_store_array)){
                    Db::table("biz_goods")->insertAll($_store_array);
                }
                Session::delete('bizStoreInfoSession');
                Session::delete('proId');
                # 添加配件关联车型
                if(!empty($adaptermodels)){
                    ignore_user_abort(true);     // 忽略客户端断开
                    set_time_limit(0);           // 设置执行不超时
                    $_return_array=array();
                    foreach($adaptermodels as $k=>$v){
                        foreach($v as $mk=>$mv){
                            foreach($mv as $bk=>$bv){
                                array_push($_return_array,array("oe_number"=>$param['biz_pro_number'],"car_logo"=>$k,"car_sort"=>$mk,"car_group"=>$bv));
                            }
                        }
                    }
                    Db::table("oe_adapt")->insertAll($_return_array);
                }
                return array("status"=>true);
            }else{
                return array("status"=>false);
            }
        }else{
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');
            $level=Db::table("member_level")->field("level_title title,id")->select();
            $this->assign('level',$level);
            return $this->fetch();
        }
    }

    /**
     * @return mixed
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 配件修改
     */
    function paret_write()
    {
        if(input("get.active")=='write'){
            $param=input("post.");
            $id=$param['id'];
            unset($param['id']);
            $param['biz_pro_create']=date("Y-m-d H:i:s");
            $param['biz_pro_barcode']=$param['biz_pro_number'];
            $param['biz_pro_oe']=$param['biz_pro_number'];
            $param['biz_delivery']=1;
            $param['biz_pro_paret']=2;
            $level=$param['level'];
            unset($param['level']);
            $discount=$param['discount'];
            unset($param['discount']);
//            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $adaptermodels=json_decode($param["adaptermodels"],true);
            unset($param["adaptermodels"]);
            $res=Db::table("biz_pro")->where(array("id"=>$id))->update($param);
            # 修改折扣
          /*  foreach($level as $k=>$v){
                Db::table("biz_pro_mediscount")->where(array("biz_pro_id"=>$id,"member_level"=>$v))->update(array("discount"=>$discount[$k]));
            }*/
            # 修改关联门店
           /* if(!empty($relevance_store)){
                $relevance_store_array=explode(',',$relevance_store);
                $_store_array=array();
                # 查询原来的关联门店
                $old=Db::table("biz_goods")->field("biz_id")->where(array("pro_id"=>$id))->select();
                $old_coll=array_column($old,"biz_id");
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    if(in_array($v,$old_coll)){
                        # 进行修改
                        Db::table("biz_goods")->where(array("pro_id"=>$id,"biz_id"=>$v))->update(array("goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch']));
                    }else{
                        # 进行添加
                        Db::table("biz_goods")->insert(array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    }
                }
                # 删除不在修改的关联里的门店
                Db::table("biz_goods")->where("pro_id = {$id} and biz_id not in ($relevance_store)")->delete();
            }else{
                # 删除关联的门店
                Db::table("biz_goods")->where(array("pro_id"=>$id))->delete();
            }*/
            $relevance_store_array=session('bizStoreInfoSession');
            if(!empty($relevance_store_array)){
                $_store_array=array();
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                }
            }
            if(!empty($_store_array)){
                Db::table("biz_goods")->insertAll($_store_array);
            }
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');
            if(!empty($adaptermodels)){
                # 删除原来的适配车型
                Db::table("oe_adapt")->where(array("oe_number"=>$param['biz_pro_number']))->delete();
                # 重新添加适配车型
                ignore_user_abort(true);     // 忽略客户端断开
                set_time_limit(0);           // 设置执行不超时
                $_return_array=array();
                foreach($adaptermodels as $k=>$v){
                    foreach($v as $mk=>$mv){
                        foreach($mv as $bk=>$bv){
                            array_push($_return_array,array("oe_number"=>$param['biz_pro_number'],"car_logo"=>$k,"car_sort"=>$mk,"car_group"=>$bv));
                        }
                    }
                }
                Db::table("oe_adapt")->insertAll($_return_array);
            }
            return array("status"=>true);

        }else{
            Session::delete('bizStoreInfoSession');
            $id=input("get.id");
            session::set('proId',$id);
            $info=Db::table("biz_pro")->where(array("id"=>$id))->find();
            $this->assign('info',$info);
            # 查询会员折扣
            $discount=Db::table("biz_pro_mediscount bpm")
                ->field("bpm.discount,ml.level_title title,ml.id")
                ->join("member_level ml",'ml.id=bpm.member_level')
                ->where(array("bpm.biz_pro_id"=>$id))
                ->select();
            $this->assign('level',$discount);
            # 查询关联门店并用，连接成字符串
            $relevance_store=Db::table("biz_goods bg")->field("group_concat(biz_id) coll")->where(array("pro_id"=>$id))->group("bg.pro_id")->find();
            $this->assign('relevance_store',$relevance_store['coll']);
            # 查询适配车型
            $_adapter=array();
            $oe_adapt=Db::table("oe_adapt")->where(array("oe_number"=>$info['biz_pro_number']))->select();
            if(!empty($oe_adapt)){
                foreach($oe_adapt as $k=>$v){
                    if(!array_key_exists($v['car_logo'],$_adapter)){
                        # 品牌不存在
                        $_adapter[$v['car_logo']]=array();
                    }
                    if(!array_key_exists($v['car_sort'],$_adapter[$v['car_logo']])){
                        $_adapter[$v['car_logo']][$v['car_sort']]=array();
                    }
                    if(!in_array($v['car_group'],$_adapter[$v['car_logo']][$v['car_sort']])){
                        array_push($_adapter[$v['car_logo']][$v['car_sort']],$v['car_group']);
                    }
                }
            }
            $this->assign("oe_adapt",json_encode($_adapter));
            $request=request()->param();
            $this->assign("request", http_build_query($request,"&"));
            #查询 商品是否关联服务
            $serviceproCount = Db::table('log_servicepro')->where("pro_id = $id")->count('service_id');
            $this->assign('serviceproCount',$serviceproCount);
            return $this->fetch();
        }
    }

    function dispath_index()
    {
        if(input("get.action")=='ajax'){
            $where="dp.id > 0 ";
            $searchParams=input("post.searchParams");
            if(!empty($searchParams)){
                $searchParams=json_decode($searchParams,true);
                if(!empty($searchParams['search'])){
                    $where.=" and b.biz_title like '%".trim($searchParams['search'])."%'";
                }
                if(!empty($searchParams['start_time'])){
                    $where.=" and date(dp.create_time) = '".$searchParams['start_time']."'";
                }
                if(!empty($searchParams['type'])){
                    $where.=" and dp.type = {$searchParams['type']}";
                }
            }

            $list=Db::table("dispath dp")
                ->field("dp.*,convert(dp.dispath_price,decimal(10,2)) dispath_price,b.biz_title,b.biz_phone,
                if(dp.type=1,'发货单','退货单') type_title")
                ->join('biz b',"dp.biz_id=b.id")
                ->where($where)
                ->order("dp.create_time desc")
                ->paginate(10, false, ['query' => request()->param()]);
            return $this->jsonSuccessData($list);
        }else{
            return $this->fetch();
        }
    }

    function dispath_add()
    {
        if(input("get.action")=='create'){
            $proInfo=json_decode(input("post.proInfo"),true);
            $biz_id=input("post.biz_id");
            $dispath_type=input("post.dispath_type");
            Db::startTrans();
            $dispath_id=Db::table("dispath")->insertGetId(array("biz_id"=>$biz_id,"type"=>$dispath_type,"create_time"=>date("Y-m-d H:i:s")));
            $_detail_array=array();
            $_dispath_price=0;
            $_dispath_num=0;
            foreach($proInfo as $k=>$v){
                $_dispath_price+=$v['num']*$v['pro_purch'];
                $_dispath_num+=$v['num'];
                array_push($_detail_array,array("dispath_id"=>$dispath_id,"pro_id"=>$v['id'],"pro_num"=>$v['num'],"pro_purch"=>$v['pro_purch']));
                if($dispath_type==1){
                    # 增加门店库存
                    $biz_invent=Db::table("biz_invent")->where(array("biz_id"=>$biz_id,"biz_pro_id"=>$v['id']))->find();
                    if(!empty($biz_invent)){
                        Db::table("biz_invent")->where(array("biz_id"=>$biz_id,"biz_pro_id"=>$v['id']))->setInc("number",$v['num']);
                        Db::table("biz_invent")->where(array("biz_id"=>$biz_id,"biz_pro_id"=>$v['id']))->update(array("pro_purch"=>$v['pro_purch']));
                    }else{
                        Db::table("biz_invent")->insert(array("biz_id"=>$biz_id,"biz_pro_id"=>$v['id'],"number"=>$v['num'],"pro_purch"=>$v['pro_purch']));
                    }
                }else{
                    # 减少门店库存
                    Db::table("biz_invent")->where(array("biz_id"=>$biz_id,"biz_pro_id"=>$v['id']))->setDec("number",$v['num']);
                }
            }
            $res=Db::table("dispath_detail")->insertAll($_detail_array);
            Db::table("dispath")->where(array("id"=>$dispath_id))->update(array("dispath_price"=>$_dispath_price,"dispath_num"=>$_dispath_num));
            if($res){
                Db::commit();
                return array("status"=>true);
            }else{
                Db::rollback();
                return array("status"=>false);
            }
        }else{
            return $this->fetch();
        }
    }

    function dispath_detail()
    {
        $id=input("get.id");
        $info=Db::table("dispath dp")
            ->field("dp.*,convert(dp.dispath_price,decimal(10,2)) dispath_price,b.biz_title,b.biz_phone,b.biz_address")
            ->join('biz b',"dp.biz_id=b.id")
            ->where(array("dp.id"=>$id))
            ->find();
        if ($info['pro_from']==1)
        {
          //普通商品
            /*
               * <th>商品名称</th>

                          <th>车辆品牌</th>
                          <th>车辆型号</th>
                          <th>配件型号</th>
                          <th>发货数量</th>

                          <th>商品类型</th>

                          <th>商品单价</th>
                          <th>合计金额（发货价格）</th>
                          <th>创建时间</th>
               */
            $list=Db::table("dispath_detail dd")
                ->field("dd.pro_num,convert(dd.pro_purch,decimal(10,2)) pro_purch,bp.biz_pro_title,pc.class_title,
            (case bp.biz_pro_paret when 1 then '商品' when 2 then '配件' when 3 then '耗材' end ) type_title,
            convert((dd.pro_num*dd.pro_purch),decimal(10,2)) dispath_price,bp.biz_pro_brand brand_title,bp.biz_pro_param title_param")
                ->join("biz_pro bp","dd.pro_id=bp.id")
                ->join("pro_class pc","pc.id = bp.biz_pro_class_id","left")
                ->where(array("dispath_id"=>$id))
                ->select();
            foreach ($list as &$v)
            {
                $v['title_param']=explode(';',$v['title_param']);
                $v['title_param']=join('<br>',$v['title_param']);
            }
            //dump($list);exit;
        }else{
            //询价商品
            $list=Db::table("dispath_detail")
                ->where(array("dispath_id"=>$id))
                ->select();
            foreach ($list as &$v)
            {
                $pro_info=Db::name('quotedprice')->where('id='.$v['pro_id'])->find();
                $v['biz_pro_title']=$pro_info['title'];
                $v['brand_title']=Db::name('brand')->where('id='.$v['brand_id'])->value('brand_title');
                $v['brandmodel_title']=Db::name('brand')->where('id='.$v['brandmodel_id'])->value('brand_title');
                $v['title_param']=$pro_info['title_param'];
                $v['type_title']='询价商品';
                $v['pro_purch']=$pro_info['selling_price'];
                $v['dispath_price']= $v['pro_num']*$v['pro_purch'];
            }
        }


        /*
         * ->field("dd.pro_num,convert(dd.pro_purch,decimal(10,2)) pro_purch,bp.biz_pro_title,pc.class_title,
            (case bp.biz_pro_paret when 1 then '商品' when 2 then '配件' when 3 then '耗材' end ) type_title,
            convert((dd.pro_num*dd.pro_purch),decimal(10,2)) dispath_price
            ")
            ->join("biz_pro bp","dd.pro_id=bp.id")
            ->join("pro_class pc","pc.id = bp.biz_pro_class_id","left")
         */
        //dump($list);exit;
        $this->assign("info",$info);
        $this->assign("list",$list);
        return $this->fetch();
    }

    /**
     * @return mixed|string
     * @throws \think\exception\DbException
     * @context 库存-门店列表
     */
    function invent_bizlist()
    {
        if(input("get.action")=='ajax'){
            $where='';
            $searchParams=input("post.searchParams");
            if(!empty($searchParams)){
                $searchParams=json_decode($searchParams,true);
                if(!empty($searchParams['search'])){
                    $where="biz_title like '%".trim($searchParams['search'])."%'";
                }

                if(!empty($searchParams['biz_type'])){
                    $where="biz_type ='".$searchParams['biz_type']."' ";
                }
            }
            $list=Db::table("biz")
                ->field("id,biz_title,(case biz_type when 1 then '自营' when 2 then '加盟' when 3 then '合作' end) biz_type_title,
                biz_phone,biz_leader,(select convert(coalesce(sum(number*pro_purch),0),decimal(10,2)) from biz_invent where biz_id = biz.id) invent_price")
                ->where(array("biz_status"=>1))
                ->where($where)
                ->paginate(10, false, ['query' => request()->param()]);
        return $this->jsonSuccessData($list);
        }else{
            return $this->fetch();
        }
    }

    function biz_invent()
    {
        $biz_id=input("get.biz_id");
        if(input("get.action")=='ajax'){

            $where='';
            $searchParams=input("post.searchParams");
            if(!empty($searchParams)){
                $searchParams=json_decode($searchParams,true);
                if(!empty($searchParams['search'])){
                    $where .=" and bp.biz_pro_title like '%".trim($searchParams['search'])."%'";
                }

                if(!empty($searchParams['type'])){
                    $where .=" and bp.biz_pro_paret ='".$searchParams['type']."' ";
                }
            }

            $list=Db::table("biz_invent bin")
                ->field("bin.*,bp.biz_pro_title,(case bp.biz_pro_paret when 1 then '商品' when 2 then '配件' when 3 then '耗材' end ) pro_type_title,
                convert((bin.number*bin.pro_purch),decimal(10,2)) invent_price,convert((bin.pro_purch),decimal(10,2)) pro_purch")
                ->join('biz_pro bp',"bp.id=bin.biz_pro_id","left")
                ->where("bin.biz_id = $biz_id".$where)
                ->order("bin.number desc")
                ->paginate(10, false, ['query' => request()->param()]);
            return $this->jsonSuccessData($list);
        }else{
            $this->assign("biz_id",$biz_id);
            return $this->fetch();
        }
    }

    function confirmChangePurch()
    {
        $id=input("post.id");
        $pro_purch=input("post.pro_purch");
        Db::table("biz_invent")->where(array("id"=>$id))->update(array("pro_purch"=>$pro_purch));
        return array("status"=>true);
    }

    function consumable_index()
    {
        $where="bp.id >0";
        $search=input("get.search");
        $this->assign('search',$search);
        if(!empty($search)){
            $where.=" and (bp.biz_pro_title like '%".$search."%' or bp.biz_pro_number='".$search."')";
        }
        $list=Db::table("biz_pro bp")
            ->field("bp.*,pc.class_title,(select count(id) from biz_goods where pro_id=bp.id) relevancestore")
            ->join("pro_class pc","bp.biz_pro_class_id=pc.id and pc.class_type=3","left")
            ->where("bp.biz_pro_status<3 and bp.biz_pro_paret=3")
            ->where($where)
            ->order("bp.biz_pro_status,bp.id desc")
            ->paginate(9,false,['query'=>request()->param()]);
        $page=$list->render();
        $this->assign('page',$page);
        $this->assign('list',$list->toArray()['data']);
        $this->assign("request",http_build_query($this->request->param(),"&"));
        $class=ProclassService::getsProClass(2);
        $this->assign("class",$class);
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商品添加
     */
    function consumable_add()
    {
        if(input("get.active")=="create"){
            $param=input("post.");
            $param['biz_pro_create']=date("Y-m-d H:i:s");
            $param['biz_pro_barcode']=$param['biz_pro_number'];
            $param['biz_delivery']=1;
            $param['biz_pro_paret']=3;
//            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $res=Db::table("biz_pro")->insertGetId($param);
            if($res){
                # 添加关联门店
             /*   if(!empty($relevance_store)){
                    $relevance_store_array=explode(',',$relevance_store);
                    $_store_array=array();
                    foreach($relevance_store_array as $k=>$v){
                        array_push($_store_array,array("pro_id"=>$res,"goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    }
                    if(!empty($_store_array)){
                        Db::table("biz_goods")->insertAll($_store_array);
                    }
                }*/
                # 添加关联门店
                $relevance_store_array=session('bizStoreInfoSession');
                if(!empty($relevance_store_array)){
                    $_store_array=array();
                    foreach($relevance_store_array as $k=>$v){
                        array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                    }
                }

                if(!empty($_store_array)){
                    Db::table("biz_goods")->insertAll($_store_array);
                }
                # 添加关联服务
                $proRelationService=Session("proRelationService");
                if(!empty($proRelationService)){
                    foreach($proRelationService as &$v){
                        $v['pro_id'] = $res;
                        $v['create_time'] = date("Y-m-d H:i:s");
                    }
                    Db::table("log_servicepro")->insertAll($proRelationService);
                }
                Session::delete('bizStoreInfoSession');
                Session::delete('proId');
                Session::delete("proRelationService");
                return array("status"=>true);
            }else{
                return array("status"=>false);
            }
        }else{
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');
            Session::delete("proRelationService");
            return $this->fetch();
        }
    }


    /**
     * @return array|mixed
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 商品修改
     */
    function consumable_write()
    {
        if(input("get.active")=='write'){
            $param=input("post.");
            $id=$param['id'];
            unset($param['id']);
            $relevance_store=$param['relevance_store'];
            unset($param['relevance_store']);
            $res=Db::table("biz_pro")->where(array("id"=>$id))->update($param);
            # 修改关联门店
            /*if(!empty($relevance_store)){
                $relevance_store_array=explode(',',$relevance_store);
                $_store_array=array();
                # 查询原来的关联门店
                $old=Db::table("biz_goods")->field("biz_id")->where(array("pro_id"=>$id))->select();
                $old_coll=array_column($old,"biz_id");
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$res,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    if(in_array($v,$old_coll)){
                        # 进行修改
                        Db::table("biz_goods")->where(array("pro_id"=>$id,"biz_id"=>$v))->update(array("goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch']));
                    }else{
                        # 进行添加
                        Db::table("biz_goods")->insert(array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                            "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v));
                    }
                }
                # 删除不在修改的关联里的门店
                Db::table("biz_goods")->where("pro_id = {$id} and biz_id not in ($relevance_store)")->delete();
            }else{
                # 删除关联的门店
                Db::table("biz_goods")->where(array("pro_id"=>$id))->delete();
            }*/

            $relevance_store_array=session('bizStoreInfoSession');
            if(!empty($relevance_store_array)){
                $_store_array=array();
                foreach($relevance_store_array as $k=>$v){
                    array_push($_store_array,array("pro_id"=>$id,"goods_price"=>$param['biz_pro_price'],"goods_online"=>$param['biz_pro_online'],
                        "goods_purch"=>$param['biz_pro_purch'],"biz_id"=>$v['id']));
                }
            }
            if(!empty($_store_array)){
                Db::table("biz_goods")->insertAll($_store_array);
            }
            # 添加关联服务
            $proRelationService=Session("proRelationService");
            if(!empty($proRelationService)){
                foreach($proRelationService as &$v){
                    $v['pro_id'] = $id;
                    $v['create_time'] = date("Y-m-d H:i:s");
                }
                Db::table("log_servicepro")->insertAll($proRelationService);
            }
            Session::delete("proRelationService");
            Session::delete('bizStoreInfoSession');
            Session::delete('proId');
            return array("status"=>true);
        }else{
            Session::delete("proRelationService");
            Session::delete('bizStoreInfoSession');
            $id=input("get.id");
            session::set('proId',$id);
            $info=Db::table("biz_pro")->where(array("id"=>$id))->find();
            $this->assign('info',$info);
            # 查询关联门店并用，连接成字符串
            $relevance_store=Db::table("biz_goods bg")->field("group_concat(biz_id) coll")->where(array("pro_id"=>$id))->group("bg.pro_id")->find();
            $this->assign('relevance_store',$relevance_store['coll']);
            $request=request()->param();
            $this->assign("request", http_build_query($request,"&"));
            return $this->fetch();
        }
    }
    /**
     * @return array|mixed
     * @context 查看关联门店
     */
    function checkNumberBiz(){
        $id = input("get.id");
        if(input("get.action") == 'ajax'){
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            #查询关联门店  biz_goods  关联门店详情表
            $data = Db::table('biz_goods bg')
                ->field('bg.id,
                (select b.biz_title from biz b where b.id = bg.biz_id and b.biz_status = 1) biz_title, 
                (select b.biz_address from biz b where b.id = bg.biz_id and b.biz_status = 1) biz_address, 
                (select b.biz_phone from biz b where b.id = bg.biz_id and b.biz_status = 1) biz_phone, 
                (select bi.number from biz_invent bi where bi.biz_pro_id = bg.id and bi.biz_id =b.id) biz_invent, 
                (select bs.biz_pro_sales from biz_pro_sales bs where bs.biz_pro_id = bg.id and bs.biz_id =  b.id) biz_sales
                ')
                ->join('biz b','b.id = bg.biz_id','left')
                ->where("pro_id = $id")
                ->paginate($listRows, false, ['query' => request()->param()]);
            return DataReturn('获取成功','0',$data);
        }else{
            $this->assign('id',$id);
            return $this->fetch();
        }


    }
    /*
     * @content : 关联门店删除
     * */
    function bizDelInfo(){
        $id = input("post.id");
        if(!empty($id)){
            Db::table('biz_goods')->where(array('id'=>$id))->delete();
            return array('status'=>true);
        }
    }
    /*
     * @content ：查看商品的发货记录
     * @params id 商品id
     * */
    function dispath_log(){
        $id = input("get.id");
        if(input("get.action") == 'ajax'){
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            #查寻 dispath_detail
            $data = Db::table("dispath_detail dd")
                ->field('dd.id,dd.pro_num,(dd.pro_num*dd.pro_purch) total_price,dd.pro_purch,
                (select d.create_time from dispath d where d.id = dd.dispath_id) create_time,
                (select b.biz_title from biz b where b.id = (select d.biz_id from dispath d where d.id = dd.dispath_id)) biz_title')
                ->where("dd.pro_id = $id")
                ->paginate($listRows, false, ['query' => request()->param()]);
            return DataReturn('获取成功','0',$data);
        }else{
            $this->assign('id',$id);
            return $this->fetch();
        }

    }

    /*
     * @content ：查看商品的关联服务
     * @params id 商品id
     * */
    function servicepro_log(){
        $id = input("get.id");
        if(input("get.action") == 'ajax'){
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            #查寻 dispath_detail
            $data = Db::table("log_servicepro ls")
                ->field('ls.*,s.service_title')
                ->join('service s','s.id = ls.service_id','left')
                ->where(array('ls.pro_id'=>$id))
                ->paginate($listRows, false, ['query' => request()->param()]);
            return DataReturn('获取成功','0',$data);
        }else{
            $this->assign('id',$id);
            return $this->fetch();
        }

    }

    /**
     * @return bool[]
     * @context 耗材关联服务
     */
    function relationServiceSession(){
        $param = input("post.");
        if(!empty($param['id'])){
            # 修改有id的情况 查询是否已经关联这个服务
            $is_relation = Db::table("log_servicepro")->where(array("pro_id"=>$param['id'],"service_id"=>$param['service_id']))->find();
            if(!empty($is_relation)){
                return array("status"=>false,"msg"=>"当前商品已关联该服务");
            }
        }
        $relationSession = Session("proRelationService");
        if(empty($relationSession)){
            $relationSession =array();
        }
        array_push($relationSession,array("service_id"=>$param['service_id'],"pro_num"=>$param['num'],"biz_pro_paret"=>3));
        Session("proRelationService",$relationSession);
        return array("status"=>true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 关联服务显示
     */
    function relationView()
    {
        $pro_id = input("post.id");
        $_array=array();
        if(!empty($pro_id)){
            $list = Db::table("log_servicepro ls")
                ->field("ls.id,ls.service_id,ls.pro_num,s.service_title")
                ->leftJoin("service s","s.id = ls.service_id")
                ->where(array("ls.pro_id"=>$pro_id))->select();
            if(!empty($list)){
                $_array=$list;
            }
        }
        $relationSession = Session("proRelationService");
        if(!empty($relationSession)){
            foreach($relationSession as &$v){
                $v['service_title'] = Db::table("service")->field("service_title")->where(array("id"=>$v['service_id']))->find()['service_title'];
                array_push($_array,$v);
            }
        }
        $str = $this->relationViewModel(array_values($_array));
        return array("status"=>true,"data"=>$str);
    }

    /**
     * @param $data
     * @return string|null
     * @context 关联服务模板
     */
    function relationViewModel($data)
    {
        $str = null;
        if(!empty($data)){
            foreach($data as $k=>$v){
                $mark = "DB";
                $id = $v['id'];
                if(empty($v['id'])){
                    $mark = "S";
                    $id = $k;
                }
                $str.="<tr>
                            <td>{$v['service_title']}</td>
                            <td ondblclick=\"changeRelationNum('".$id."','".$mark."',this)\">{$v['pro_num']}</td>
                            <td>
                                <button type=\"button\" class=\"btn btn-danger\" onclick=\"delRelation(this,'".$id."','".$mark."')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $str;
    }

    /**
     * @return bool[]
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 修改关联服务的使用商品数量
     */
    function changeRelationNum(){
        $param = input();
        if($param['mark']=="DB"){
            Db::table("log_servicepro")->where(array("id"=>$param['id']))->update(array("pro_num"=>$param['num']));
        }else{
            $relationSession = Session("proRelationService");
            $relationSession[$param['id']]['pro_num'] = $param['num'];
            Session("proRelationService",$relationSession);
        }
        return array("status"=>true);
    }

    /**
     * @return bool[]
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 删除关联服务
     *
     */
    function delRelation(){
        $param = input();
        if($param['mark']=="DB"){
            Db::table("log_servicepro")->where(array("id"=>$param['id']))->delete();
        }else{
            $relationSession = Session("proRelationService");
            unset( $relationSession[$param['id']]);
            Session("proRelationService",$relationSession);
        }
        return array("status"=>true);
    }

}
