<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/17 0017
 * Time: 20:18
 */

namespace app\admin\controller;

use app\service\BaseService;
use app\service\EmployeeService;
use app\service\ResourceService;
use think\Db;
use think\db\Where;
use think\Exception;
use function Couchbase\defaultDecoder;

/**
 * 档案管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月17日19:59:36
 */
class Files extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();
        // 登录校验
        $this->IsLogin();
    }
    
    /**
     * [Index 档案列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {
        
        if (input('get.action') == 'ajax') {
            
            $where = EmployeeService::EmployeeListWhere(input());

            $data_params = [
                'page'   => true,
                'number' => 10,
                'where'  => $where,
                'table'  => 'employee_sal',
                'order'  => 'employee_create desc',
            
            ];
            $data = EmployeeService::EmployeeDataList($data_params);
           // dump($data);exit;
            $total = BaseService::DataTotal('employee_sal', [['is_del', '=', 2]]);
            
            
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('department_arr', Db::name('employee_department')->where('is_del = 2')->field('id,title')->select());
            $this->assign('member_sex', lang('member_sex'));
            return $this->fetch();
        }
        
    }
    
    /**
     * [SaveInfo 档案编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日13:48:10
     */
    public function SaveData()
    {
        
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm'     => 0,
                'n'     => 1,
                'page'  => false,
                'table' => 'employee_sal',
                'order' => 'employee_create desc',
            ];
            
            $ret = EmployeeService::EmployeeDataList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        //多余图片处理
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'employee' . $params['id'];
        ResourceService::delUploadCache();
        
        $employee_department = Db::name('employee_department')->where('is_del = 2 and higher_department_id=0')->field('id,title')->select();
        //$employee_station = Db::name('employee_station')->where('is_del = 2')->field('id,title')->select();
        //$this->assign('employee_station', $employee_station);
//        dump($employee_department);exit;
        $this->assign('data', $data);
        $this->assign('department_arr', $employee_department);

        
        return $this->fetch();
    }

    /**
     * [Save 档案图片显示]
     * @author   ows
     * @version 1.0.0
     * @date    2020年12月18日22:18:59
     */
    public function ArchivesInfo()
    {
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm'     => 0,
                'n'     => 1,
                'page'  => false,
                'table' => 'employee_sal',
                'order' => 'employee_create desc',
            ];

            $ret = EmployeeService::EmployeeDataList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        //多余图片处理
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'employee' . $params['id'];
        ResourceService::delUploadCache();

        $employee_department = Db::name('employee_department')->where('is_del = 2')->field('id,title')->select();

        $bannerArr=BaseService::HandleImg($data['nda_thumb']);

        $this->assign('bannerArr', $bannerArr);
        $this->assign('data', $data);
        $this->assign('employee_department', $employee_department);
//        $employee_station = Db::name('employee_station')->where('is_del = 2')->field('id,title')->select();
//        $this->assign('employee_station', $employee_station);

        return $this->fetch();
    }

    /**
     * [Save 档案图片保存]
     * @author   ows
     * @version 1.0.0
     * @date    2020年12月18日22:18:59
     */
    public function ArchivesSave()
    {

        $id = input('id');
        $idcard_thumb = input('idcard_thumb');
        $education_thumb = input('education_thumb');
        $contract_thumb = input('contract_thumb');
        $social_security_thumb = input('social_security_thumb');
        $entry_table_thumb = input('entry_table_thumb');
        $nda_thumb = input('nda_thumb');
        //dump($this->data_post);exit;
        $rlt = [
            'code' => 1,
            'msg'  => '',
            'data' => '',
        ];
        ResourceService::$session_suffix='source_upload'.'employee'.$id;
        ResourceService::delCacheItem($idcard_thumb);
        ResourceService::delCacheItem($education_thumb);
        ResourceService::delCacheItem($contract_thumb);
        ResourceService::delCacheItem($social_security_thumb);
        ResourceService::delCacheItem($entry_table_thumb);
       // $nda_thumb=BaseService::HandleImg($nda_thumb);
        ResourceService::delCacheItem(BaseService::HandleImg($nda_thumb));

        $info = Db::table('employee_sal')->where('id',$id)->find();
        if ($info) {
            if ($idcard_thumb != '') {
                $info['idcard_thumb'] = $idcard_thumb;
            }
            if ($education_thumb != '') {
                $info['education_thumb'] = $education_thumb;
            }
            if ($contract_thumb != '') {
                $info['contract_thumb'] = $contract_thumb;
            }
            if ($social_security_thumb != '') {
                $info['social_security_thumb'] = $social_security_thumb;
            }
            if ($entry_table_thumb != '') {
                $info['entry_table_thumb'] = $entry_table_thumb;
            }
            if ($nda_thumb != '') {
                $info['nda_thumb'] = $nda_thumb;
            }
            //dump($info);exit;
            $res = Db::table('employee_sal')->update($info);
            if ($res) {
                $rlt['code'] = 0;
            } else {
                $rlt['msg'] = '保存失败';
            }
        } else {
            $rlt['msg'] = '参数错误';
        }
        return $rlt;
    }
    
    /**
     * [Save 档案执行编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function Save()
    {
        
        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }
        
        // 开始操作
        $params = input('post.');
        
        return EmployeeService::EmployeeDataSave($params);
        
        
    }

    /**
     * [Save 档案异动保存]
     * @author   ows
     * @version 1.0.0
     * @date    2020年12月18日22:18:59
     */
    public function SaveDispatch()
    {
        Db::startTrans();
        $rlt = [
            'code' => 1,
            'msg'  => '',
            'data' => '',
        ];

        try {
            $id = input('id');
            $base_salary = input('base_salary_after');
            $work_time = input('work_time');
            $fullattendance_salary = input('fullattendance_salary_after');
            $station_salary = input('station_salary_after');
            $employee_department_id = input('department_id_after');
            $employee_station_id = input('station_id_after');
            //dump(input());exit;
            $info = Db::table('employee_sal')->where('id', $id)->find();
            if (!$info) {
                throw new Exception('参数错误');
            }
            switch ($info['employee_status']) {
                case 1:
                    $dispatch_data = [
                        'employee_id'                  => $id,
                        'employee_name_before'         => $info['employee_name'],
                        'department_id_before'         => $info['employee_department_id'],
                        'station_id_before'            => $info['employee_station_id'],
                        'base_salary_before'           => $info['base_salary'],
                        'station_salary_before'        => $info['station_salary'],
                        'fullattendance_salary_before' => $info['fullattendance_salary'],
                        'employee_name_after'          => $info['employee_name'],
                        'work_time'                    => $work_time,

                        'department_id_after'          => $employee_department_id,
                        'station_id_after'             => $employee_station_id,
                        'base_salary_after'            => $base_salary,
                        'station_salary_after'         => $station_salary,
                        'fullattendance_salary_after'  => $fullattendance_salary,
                    ];
                    $dispatch_res = Db::table('employee_dispatch')->insert($dispatch_data);
                    if (!$dispatch_res) {
                        throw new Exception('调度信息添加失败');
                    }

                    if ($base_salary != '') {
                        $info['base_salary'] = $base_salary;
                    }
                    if ($fullattendance_salary != '') {
                        $info['fullattendance_salary'] = $fullattendance_salary;
                    }
                    if ($station_salary != '') {
                        $info['station_salary'] = $station_salary;
                    }
                    if ($employee_department_id != '') {
                        $info['employee_department_id'] = $employee_department_id;
                    }
                    if ($employee_station_id != '') {
                        $info['employee_station_id'] = $employee_station_id;
                    }
                    $res = Db::table('employee_sal')->update($info);
                    if (!$res) {
                        throw new Exception('保存员工数据失败');
                    }
                    break;
                case 2:
                    $dispatch_data = [
                        'employee_id'                  => $id,
                        'employee_name_before'         => $info['employee_name'],
                        'department_id_before'         => $info['employee_department_id'],
                        'station_id_before'            => $info['employee_station_id'],
                        'base_salary_before'           => $info['regular_base_salary'],
                        'station_salary_before'        => $info['regular_station_salary'],
                        'fullattendance_salary_before' => $info['regular_fullattendance_salary'],
                        'employee_name_after'          => $info['employee_name'],
                        'work_time'                    => $work_time,
                        'department_id_after'          => $employee_department_id,
                        'station_id_after'             => $employee_station_id,
                        'base_salary_after'            => $base_salary,
                        'station_salary_after'         => $station_salary,
                        'fullattendance_salary_after'  => $fullattendance_salary,
                    ];
                    $dispatch_res = Db::table('employee_dispatch')->insert($dispatch_data);
                    if (!$dispatch_res) {
                        throw new Exception('调度信息添加失败');
                    }

                    if ($base_salary != '') {
                        $info['regular_base_salary'] = $base_salary;
                    }
                    if ($fullattendance_salary != '') {
                        $info['regular_fullattendance_salary'] = $fullattendance_salary;
                    }
                    if ($station_salary != '') {
                        $info['regular_station_salary'] = $station_salary;
                    }
                    if ($employee_department_id != '') {
                        $info['employee_department_id'] = $employee_department_id;
                    }
                    if ($employee_station_id != '') {
                        $info['employee_station_id'] = $employee_station_id;
                    }
                    $res = Db::table('employee_sal')->update($info);
                    if (!$res) {
                        throw new Exception('保存员工数据失败');
                    }
                    break;
                default:
                    throw new Exception('离职不支持修改');
            }

            $rlt['code'] = 0;
            Db::commit();
        } catch (Exception $e) {
            $rlt['msg'] = $e->getMessage();
            Db::rollback();
        }
        return $rlt;
    }
    
    /**
     * [ChangeNumList 异动列表页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function ChangeNumList()
    {
        if (input('get.action') == 'ajax') {
            
            $data_params = [
                'page'   => true,
                'number' => 10,
                'where'  => [['employee_id', '=', input('get.id')]],
                'table'  => 'employee_dispatch',
            
            
            ];
            $data = EmployeeService::EmployeeDataList($data_params);

            $total = BaseService::DataTotal('employee_dispatch', [['id', '>', 0]]);
            
            
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            
            return $this->fetch();
        }
    }
    
    /**
     * [search 搜索]
     * @author   osw
     * @version 1.0.0
     * @date    2020年12月17日23:12:59
     */
    public function search()
    {
        $search = input('search');
        
        $where = new Where();
        // 条件搜索
        $where['employee_name|employee_phone'] = $search;
        
        $res = Db::table('employee_sal')->where($where)->find();
        if ($res) {
            return ['code' => 0, 'msg' => '', 'data' => $res];
        } else {
            return ['code' => 1, 'msg' => '没有数据', 'data' => []];
        }
    }
    
}