<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\MerchantService;
use app\service\SalaryService;
use think\Db;
use think\helper\Str;

/**
 * 薪资管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年12月22日11:41:20
 */
class Salary extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 薪资列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function Index()
    {
        if (input('get.action') == 'ajax') {
            //Db::name('employee_salary')->where('id > 0')->delete();

            $where = SalaryService::SalaryListWhere(input());

            $data_params = array(
                'page' => false,
                'number' => 10,
                'table' => 'employee_salary_sal',
                'where' => $where,
                'order' => 'salary_time desc'

            );
            $data = SalaryService::SalaryDataList($data_params);

            $total = BaseService::DataTotal('employee_salary_sal', [['id', '>', 0]]);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $time = SalaryService::GetRecentTime();

            $default_time = explode('-', $time['time_default']);
            // dump($default_time);exit;
            $this->assign('time_now', $time['time_now']);
            $this->assign('time_pre', $time['time_pre']);

            $this->assign('year', $default_time[0]);
            $this->assign('month', $default_time[1]);
            return $this->fetch();
        }
    }

    /**
     * [SaveData 新建工资表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function SaveData()
    {

        // 参数
        $year = input('get.year');
        $month = input('get.month');
        $time = date('Y-m', strtotime($year . '-' . $month));

        $data = SalaryService::CalcSalaryData($time);

        $this->assign('data', $data);

        $this->assign('salary_time', $data[0]['salary_time']);
        return $this->fetch();
    }

    /**
     * [SaveData 新建/编辑工资信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function Save()
    {
        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return SalaryService::DataSave($params);
    }

    /**
     * [SaveOneData 编辑单条工资信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function SaveOneData()
    {
        $data_params = array(
            'page' => true,
            'number' => 10,
            'table' => 'employee_salary_sal',
            'where' => ['id' => input('get.id')],
            'order' => 'salary_time desc'

        );
        $data = SalaryService::SalaryDataList($data_params)[0];
        //dump($data);
        $this->assign('department_arr', Db::name('employee_department')->where('is_del = 2')->field('id,title')->select());
        $this->assign('data', $data);
        return $this->fetch();
    }
}
