<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\BizApplyService;
use app\service\SmsCode;
use base\Excel;
use think\Db;
use think\facade\Request;


/**
 * 门店申请管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月28日16:57:54
 */
class JzBizApply extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [FranchiserIndex 加盟门店申请列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function FranchiserIndex()
    {

        if (input('get.action') == 'ajax') {
            $params = input();

            $params['param']['biz_type'] = $params['biz_type'];
            // 条件
            $where = BizApplyService::ApplyListWhere($params['param']);

            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'biz_apply',
                'field' => '*',
                'order' => 'read_status asc'
            );

            $data = BaseService::DataList($data_params);
            //dump($data);exit;
            $data = BizApplyService::DataDealWith($data);

            $total = BaseService::DataTotal('biz_apply', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {

            $this->assign('read_status', lang('read_status'));
            return $this->fetch();
        }
    }

    /**
     * [FranchiserDetail 加盟门店申请详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function FranchiserDetail()
    {
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            Db::name('biz_apply')->where('id=' . $params['id'])->update(['read_status' => 2]);
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm' => 0,
                'n' => 1,
                'page' => false,
                'table' => 'biz_apply',
                'field' => '*',
            ];
            $ret = BaseService::DataList($data_params);
            $ret = BizApplyService::DataDealWith($ret);

            $data = empty($ret[0]) ? [] : $ret[0];
        }

        $industry_cate = Db::name('industry_cate')->select();
        $this->assign('industry_cate', $industry_cate);
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [FranchiserSave 加盟门店申请数据保存]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function FranchiserSave()
    {

        // 是否ajax
//        if(!IS_AJAX)
//        {
//            return $this->error('非法访问');
//        }
        // 开始操作
        $params = input('post.');
        //throw new \BaseException(['code'=>403 ,'errorCode'=>4001,'msg'=>'保存申请失败','status'=>false,'debug'=>false]);

        return json(BizApplyService::ApplyDataSave($params));

    }

    /**
     * [FranchiserDel 删除申请]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function FranchiserDel()
    {
        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }
        $params = $this->data_post;
        $params['table'] = 'biz_apply';
        $params['soft'] = false;
        $params['errorcode'] = 900;
        $params['msg'] = '删除失败';
        BaseService::DelInfo($params);
        return DataReturn('删除成功', 0);
    }

    /**
     * [ApplyPageDetail 申请编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function ApplyPageDetail()
    {
        $params = input();
        $data = BizApplyService::ApplyPage($params);

        $this->assign('data', $data['data']);
        return $this->fetch();
    }

    /**
     * [ApplyPageSave 申请页面编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function ApplyPageSave()
    {
        $params = input('post.');
        // dump($data);exit;
        return BizApplyService::PageSave($params);
    }

    /**
     * [PartnerIndex 合伙人申请列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function PartnerIndex()
    {

        if (input('get.action') == 'ajax') {
            $params = input();
            $params['param']['biz_type'] = $params['biz_type'];
            // 条件
            $where = BizApplyService::ApplyListWhere($params['param']);

            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'member_partner',
                'field' => '*',
                'order' => 'create_time asc'
            );

            $data = BaseService::DataList($data_params);

            $data = BizApplyService::PartnerDataDealWith($data);

            $total = BaseService::DataTotal('member_partner', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [PartnerDetail 合伙人申请详情页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function PartnerDetail()
    {
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm' => 0,
                'n' => 1,
                'page' => false,
                'table' => 'member_partner',
                'field' => '*',

            ];
            $ret = BaseService::DataList($data_params);

            $ret = BizApplyService::PartnerDataDealWith($ret);

            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [PartnerDetailSave 合伙人申请数据保存]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function PartnerDetailSave()
    {
        $params = input('post.');
        // dump($data);exit;
        return BizApplyService::PartnerSave($params);
    }

    /**
     *
     * @context 门店审核导出
     */
    public function ActionBizExcel()
    {
        $params = input();
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => ['id' => $params['id']],
            'table' => 'biz',
            'field' => '*',

        );
        $info = BaseService::DataList($data_params);
        if (!empty($info)) {
            $excel_params = [
                'filename' => $info['title'] . '门店审核',
                'title' => lang('excel_action_biz_list'),
                'data' => $info
            ];
            $excel = new Excel($excel_params);
            return $excel->Export();
        }

    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 审核通过
     */
    function auditSuccess()
    {
        $employee_id = input("post.employee_id");
        $apply_id = input("post.apply_id");
        # 查询门店
        $applyInfo = Db::table("biz_apply")->field("biz_id")->where(array("id" => $apply_id))->find();
        if (!empty($applyInfo)) {
            $bizInfo = Db::table("biz")->where(array("id" => $applyInfo['biz_id']))->find();
            # 可以加代理任务、提成
            Db::table("biz")->where(array("id" => $applyInfo['biz_id']))->update(array("biz_status" => 1));
            Db::name('assign_staff')->insertGetId(['type' => 1, 'biz_id' => $applyInfo['biz_id'], 'employee_id' => $employee_id]);
            Db::table("biz_apply")->where(array("id" => $apply_id))->delete();
            # 发短信--11、合作门店入驻审核通过通知(·后台合作店入驻审核操作通过·推送合作店联系方式)
            if (!empty($bizInfo['biz_phone'])) {
                $SmsCode = new SmsCode();
                $smsInfo = '恭喜您：您的入住申请审核已通过，请在“E联车服联盟”APP登录，设置相关信息。';
                $SmsCode->Ysms($bizInfo['biz_phone'], $smsInfo, false);
            }
        }
        return array("status" => true);
    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 驳回审核
     */
    function auditFail()
    {
        $apply_id = input("post.id");
//        $applyInfo = Db::table("biz_apply")->field("biz_id")->where(array("id"=>$apply_id))->find();
//        if(!empty($applyInfo)){
//            Db::table("biz")->where(array("id"=>$applyInfo['biz_id']))->update(array("biz_status"=>0));
//        }
        Db::table("biz_apply")->where(array("id" => $apply_id))->delete();
        return array("status" => true);
    }

}