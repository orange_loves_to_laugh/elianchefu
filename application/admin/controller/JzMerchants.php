<?php


namespace app\admin\controller;


use app\businessapi\controller\Task;
use app\service\BaseService;
use app\service\BizDepositService;
use app\service\EmployeeService;
use app\service\FinanceService;
use app\service\MerchantService;
use app\service\ResourceService;
use app\service\SmsCode;
use app\service\TaskService;
use base\Excel;
use Protocol\Curl;
use Redis\Redis;
use think\Db;
use think\facade\Session;

/**
 * 商户管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月11日18:01:30
 */
class JzMerchants extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日18:01:30
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();
    }

    function storeSettlement()
    {
        # 门店下架结算
        $m_id = input('post.id');
        # 查询门店状态
        $mStatus = Db::table('merchants')->where(array('id' => $m_id))->value('status');
        if ($mStatus == 1) {
            # 还没下架
            $redis = new Redis();
            /* $status = $redis->get('settlementTask'.$bizId);
             if (!empty($status)) {
                 return false;
             }
             $redis->set('settlementTask'.$bizId, 'YES');*/
            $status = $redis->lock('storeSettlementMerchant' . $m_id);
            if ($status) {
                # 先结算
                # 查询门店信息
                $bizInfo = Db::table('merchants')
                    ->field('balance,bond_price,contacts,contacts_phone,title,province,city,area,address')
                    ->where(array('id' => $m_id))
                    ->find();
                # 添加提现信息
                $amount = $bizInfo['balance'] + $bizInfo['bond_price'];
                $logId = Db::table('log_deposit')->insertGetId(array(
                    'biz_id' => $m_id,
                    'create_time' => date('Y-m-d H:i:s'),
                    'price' => $amount,
                    'username' => $bizInfo['title'],
                    'phone' => $bizInfo['contacts_phone'],
                    'account_name' => $bizInfo['contacts'],
                    'account_code' => $bizInfo['contacts_phone'],
                    'launch_cate' => 8,
                    'deposit_type' => 2,
                    'province' => $bizInfo['province'],
                    'city' => $bizInfo['city'],
                    'area' => $bizInfo['area'],
                    'address' => $bizInfo['address'],
                    'source_type' => 3
                ));
                if ($logId) {
                    # 修改门店状态
                    Db::table('merchants')
                        ->where(array('id' => $m_id))
                        ->update(array('status' => 2));
                }
                $redis->unlock('storeSettlementMerchant' . $m_id);
            }
            return array('status' => true, 'msg' => '已经提交审核');
        } else {
            # 已经下架
            return array('status' => false, 'msg' => '已经下架');
        }
    }

    /**
     * [Index 列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function Index()
    {
        $status = input("get.status");
        if (input('get.action') == 'ajax') {
            $params = input();
            $params['status'] = $status;
            $where = MerchantService::MerchantsListWhere(input());
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants',
                'where' => $where,
                'order' => 'status asc,id desc',

            );
            $data = BaseService::DataList($data_params);
            #查询下 商户所关联的分类

            $data = MerchantService::DataDealWith($data);
            $total = BaseService::DataTotal('merchants', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('cate_id', Db::name('merchants_cate')->where('status =1')->field('title,id')->select());
            $this->assign('level', lang('merchants_level'));
            #查询邮寄物料
            $merchants_materiel = Db::table('merchants_materiel')->where("status = 1")->count();
            $this->assign('merchants_materiel', $merchants_materiel);
            $this->assign('status', $status);
            # 查询关联员工信息
            $employeeSal = Db::table('employee_sal')->field('id,employee_name name')
                ->where(array('relation_status' => 1))
                ->where('employee_status != 0')
                ->select();
            $this->assign('employeeSal', $employeeSal);

            return $this->fetch();
        }
    }

    function getQrcode()
    {
        $merchandId = input('post.id');
        if (!empty($merchandId)) {
            # 图片名称
            $filename = 'qrcodes' . $merchandId . '.png';
            # 绝对路径
            $file = ROOT_PATH . '/static/img/MerchantsQrCode/' . $filename;
            # 判断是否已经存在,存在直接放回 , 不存在接口获取

//                    "path" => urlencode("pages/index/shop_info?id=" . $merchandId),
            //"path" => urlencode("pages/index/video_share?m_id=" . $merchandId."&source=2"),
            if (!file_exists($file)) {
                $postUrl = 'https://developer.toutiao.com/api/apps/qrcode';
                $_arr = array(
                    "access_token" => $this->getAccessToken(),
                    "appname" => "douyin",
                    "path" => urlencode("pages/index/video_share?m_id=" . $merchandId . "&source=2"),
                    "width" => 430,
                    "line_color" => ["r" => 0, "g" => 0, "b" => 0],
                    "background" => ["r" => 255, "g" => 255, "b" => 255],
                    "set_icon" => true
                );
                $curl = new Curl();
                $res = $curl->post($postUrl, $_arr, 'json');
                $a = file_put_contents($file, $res);
                # 图片访问地址
                $retFile = imgUrl('static/img/MerchantsQrCode/' . $filename, true);
                return array('url' => $retFile);
            } else {
                # 图片访问地址
                $retFile = imgUrl('static/img/MerchantsQrCode/' . $filename, true);
                return array('url' => $retFile);
            }
        }
    }

    function getAccessToken()
    {
        $postUrl = 'https://developer.toutiao.com/api/apps/v2/token';
        $_arr = array(
            "appid" => config('ecars.toutiao_Appid'),
            "secret" => config('ecars.toutiao_AppSecret'),
            "grant_type" => "client_credential"
        );
        $curl = new Curl();
        $res = $curl->post($postUrl, $_arr, 'json');
        $resData = json_decode($res, true);
        $accessToken = '';
        if (!empty($resData) && $resData['err_no'] == 0) {
            $accessToken = $resData['data']['access_token'];
        }
        return $accessToken;
    }

    /**
     * [SaveData 添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function SaveData()
    {
        $is_type = input("get.is_type");
        $status = input("get.status");
        if (empty($status)) {
            $status = 1;
        }
        //savemark == 1 保存按钮不隐藏
        $savemark = input("get.savemark");
        // 参数
        $params = input();
        // 数据
        $data = ['backgroud_color' => '#ffffff', 'ad_color' => '#ff4800'];
        $table = 'merchants';

        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm' => 0,
                'n' => 1,
                'page' => false,
                'table' => $table
            ];
            $ret = BaseService::DataList($data_params);
            $ret = MerchantService::DataDealWith($ret);
            $bannerArr = explode('*', $ret[0]['remarks']);
            $this->assign('bannerArr', $bannerArr);
            $data = empty($ret[0]) ? [] : $ret[0];
            if (!empty($data['is_type'])) {
                $is_type = $data['is_type'];
            }
            #本地生活 联盟商家 的 分类
            if ($is_type == 1) {
                #本地生活
                $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id' => $params['id']))->field('cate_pid,id')->select();
                $this->assign('cate_relation_1', $cate_relation[0]['cate_pid']);
                $this->assign('cate_relation_2', $cate_relation[1]['cate_pid']);
                $this->assign('cate_relation_3', $cate_relation[2]['cate_pid']);
            }

        }
        //dump($data);exit;
        //多余图片处理
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'merchants' . $params['id'];
        ResourceService::delUploadCache();


        $cates = Db::name('merchants_cate')->where("pid=0 and type=$is_type and status = 1")->select();
//        dump($cates);die;
        $open_log_info = Db::table("log_merchants_expiration")->where(array("merchants_id" => $params['id'], "type" => 1))->order("id desc")->find();
        if (!empty($open_log_info)) {
            $data['open_toutiao_expiration_start'] = $open_log_info['expiration_start'];
            $data['open_toutiao_expiration'] = $open_log_info['duration_time'];
        }
        $take_log_info = Db::table("log_merchants_expiration")->where(array("merchants_id" => $params['id'], "type" => 2))->order("id desc")->find();
        if (!empty($take_log_info)) {
            $data['take_video_expiration_start'] = $take_log_info['expiration_start'];
            $data['take_video_expiration'] = $take_log_info['duration_time'];
        }
        $full_log_info = Db::table("log_merchants_expiration")->where(array("merchants_id" => $params['id'], "type" => 3))->order("id desc")->find();
        if (!empty($full_log_info)) {
            $data['bully_screen_expiration_start'] = $full_log_info['expiration_start'];
            $data['bully_screen_expiration'] = $full_log_info['duration_time'];
        }
        $this->assign('cates', $cates);
        # is_type  1 本地生活 2 联盟商家
        $this->assign('is_type', $is_type);
        $this->assign('status', $status);
        $this->assign('savemark', $savemark);
        $this->assign('data', $data);
        //删除分类缓存
        Session::delete('getCateSession');
        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function Save()
    {
        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return MerchantService::SaveMerchants($params);

    }

    /**
     * [CacheLog 提现记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CacheLog()
    {
        if (input('get.action') == 'ajax') {

            $where[] = ['source_type', '=', 3];
            $where[] = ['biz_id', '=', input('merchants_id')];
//            dump($where);exit;
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'log_deposit'
            );
            $data = FinanceService::DepositList($data_params);
            //dump($data);exit;
            $total_num = BaseService::DataTotal('log_deposit', $where);

            $total_price = FinanceService::DepositTotalPrice($where);

            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data, 'price' => $total_price];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [IncomeLog 收入记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function IncomeLog()
    {
        if (input('get.action') == 'ajax') {
            $merchants_id = input('merchants_id');

            $where = "merchants_id = $merchants_id and pay_type < 8 ";

            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_income',
                'where' => $where,

            );
            $data = MerchantService::MerchantsIncomeList($data_params);


            $total_num = BaseService::DataTotal('merchants_income', $where);

            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [MemberLog 到店记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function MemberLog()
    {
        if (input('get.action') == 'ajax') {

            $merchants_id = input('merchants_id');

            $where = "merchants_id = $merchants_id and pay_type < 8 ";
//            $where=MerchantService::MerchantsIncomeListWhere(input());
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_income',
                'where' => $where,
                'group' => 'member_id',
                'field' => '*,count(*) member_count'
            );
            $data = MerchantService::MerchantsIncomeList($data_params);

//dump($data);exit;
//            $total_num = BaseService::DataTotal('merchants_income',$where);
            $total_num = 0;
            if (!empty($data)) {
                $total_num = count($data);

            }


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [MemberLog 用户到店记录表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function MemberLogList()
    {
        if (input('get.action') == 'ajax') {

//            $where[]=['merchants_id','=',input('merchants_id')];
//            $where[]=['member_id','=',input('member_id')];
//            $where=MerchantService::MerchantsIncomeListWhere(input());
            $merchants_id = input('merchants_id');
            $member_id = input('member_id');


            $where = "pay_type < 8 ";
            if (!empty($merchants_id)) {
                $where .= " and member_id = $member_id ";
            }
            if (!empty($merchants_id)) {
                $where .= " and merchants_id = $merchants_id ";
            }
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_income',
                'where' => $where,


            );
            $data = MerchantService::MerchantsIncomeList($data_params);


            $total_num = BaseService::DataTotal('merchants_income', $where);

            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [CollectionsLog 收款记录表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CollectionsLog()
    {
        $mark = input("get.mark");//income  是收入  查询订单状态为 已到店核销成功的订单

        if (input('get.action') == 'ajax') {
            $merchants_id = input('merchants_id');
            $_where = "merchants_id = $merchants_id ";
            $searchParams = input("post.searchParams");

            if (!empty($searchParams['show_type'])) {
                #展示类型 all  全部展示  part 部分展示（用户id为0 的不展示）
                if ($searchParams['show_type'] == 'part') {
                    $_where .= " and member_id>0";

                }

            }

            //$where=MerchantService::MerchantsIncomeListWhere(input());
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_income',
                'where' => $_where,
                'field' => '*'
            );
            $data = MerchantService::MerchantsCollectionsList($data_params);


            $total_num = BaseService::DataTotal('merchants_income', $_where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            $this->assign('mark', $mark);
            return $this->fetch();
        }
    }

    /**
     * [VoucherList 商户卡卷]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function VoucherList()
    {
        if (input('get.action') == 'ajax') {

            //$where=MerchantService::MerchantsIncomeListWhere(input());
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_voucher',
                'where' => [['merchants_id', '=', input('merchants_id')]],
                'field' => '*'
            );
            $data = MerchantService::MerchantsVoucherList($data_params);


            $total_num = BaseService::DataTotal('merchants_voucher', [['merchants_id', '=', input('merchants_id')]]);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [CardLog 推荐会员信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CardLog()
    {
        if (input('get.action') == 'ajax') {

            $merchants_id = input('merchants_id');
            $where = "log_resource = 3 and biz_id=$merchants_id";
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'log_handlecard',
                'where' => $where,
                'field' => '*'
            );
            $data = MerchantService::MerchantsCardlogList($data_params);


            $total_num = BaseService::DataTotal('log_handlecard', $where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [RecommondProduct 推荐商品]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function RecommondProduct()
    {
        if (input('get.action') == 'ajax') {

            $where = [
                ['merchants_id', '=', input('merchants_id')],
//                ['is_recommend','=',1]
            ];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_pro',
                'where' => $where,
                'field' => '*'
            );
            $data = MerchantService::MerchantsProductList($data_params);


            $total_num = BaseService::DataTotal('merchants_product', $where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [RecommondService 推荐服务]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function RecommondService()
    {
        if (input('get.action') == 'ajax') {

            $where = [
                ['merchants_id', '=', input('merchants_id')],
                ['is_recommend', '=', 1]
            ];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_service',
                'where' => $where,
                'field' => '*'
            );
            $data = MerchantService::MerchantsServiceList($data_params);


            $total_num = BaseService::DataTotal('merchants_service  ', $where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [StaffList 员工列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function StaffList()
    {

        if (input('get.action') == 'ajax') {

            $where = [
                ['merchants_id', '=', input('merchants_id')],

            ];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'table' => 'merchants_staff',
                'where' => $where,
                'field' => '*'
            );
            $data = MerchantService::MerchantsStaffList($data_params);


            $total_num = BaseService::DataTotal('merchants_staff  ', $where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [CommentList 评论列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CommentList()
    {
        if (input('get.action') == 'ajax') {
            // 消费金额  付款方式
            //
            $where = MerchantService::MerchantsCommentListWhere(input());

            $data_params = array(
                'page' => true,
                'number' => 10,

                'where' => $where,
                'field' => 'me.create_time,me.evalua_level,me.evalua_comment,mo.actual_price,mo.cash_type'
            );
            $data = MerchantService::MerchantsCommentDataList($data_params);


            $total_num = MerchantService::MerchantsCommentDataTotal($where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        } else {
            $this->assign('cash_type', lang('cash_type'));
            return $this->fetch();
        }
    }

    /**
     * [SavePopup 商户弹窗管理页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function SavePopup()
    {
        $data = Db::name('sysset')->where(['tagname' => 'merchants_popup'])->value('desc');
        $data = json_decode($data, true);
        // dump($data);exit;

        $this->assign('data', $data);
        $this->assign('open_close_status', lang('open_close_status'));
        return $this->fetch();
    }

    /**
     * [DoSavePopup 商户弹窗修改]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function DoSavePopup()
    {
        $param = $this->data_post;
        return MerchantService::SaveMerchantsPopup($param);
    }

    /*
     * @content 任务列表
     * */
    function task_index()
    {
        if (input("get.action") == 'ajax') {
            #查询任务
            $_where = "id > 0";
            $searchParams = input("post.param");

            if (isset($searchParams['task_title'])) {
                $_where .= " and task_title like '%" . $searchParams['task_title'] . "%'";
            }
            # 状态筛选
            $time = date("Y-m-d H:i:s");
            if (!empty($searchParams['status'])) {
                if ($searchParams['status'] == 1) {
                    $_where .= " and start_time > '" . $time . "'";
                }
                if ($searchParams['status'] == 2) {
                    $_where .= " and start_time <= '" . $time . "' and end_time > '" . $time . "'";
                }
                if ($searchParams['status'] == 3) {
                    $_where .= " and end_time > '" . $time . "'";
                }
            }
            # 类型筛选
            if (!empty($searchParams['task_type'])) {
                $_where .= " and task_type = " . $searchParams['task_type'];
            }
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_task',
                'field' => '*,(case when start_time>now() and end_time>now() then 2 
                                when start_time<=now() and end_time>=now() then 1
                                when start_time<now() and end_time<now() then 3 end) task_sort',
                'order' => 'task_sort asc,id asc'
            );
            $dataList = BaseService::DataList($data_params);
            $data = MerchantService::DataDealWithTask($dataList);
//            $total = Db::table('merchants_task')->where($_where)->count();
            $total = BaseService::DataTotal('merchants_task', $_where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }

    }

    /*
     * @contetn 任务编辑 创建
     * */
    function SaveDataTask()
    {
        $id = input("get.id");
        if (input("get.action") == 'ajax') {
            #添加  或编辑
            #查询任务
            $params = input('post.');
            $data = MerchantService::SaveDataTask($params);


            return $data;
        } else {
            if (!empty($id)) {
                #查询任务
                $_where = " id=$id";
                $data_params = array(
                    'page' => false,
                    'number' => 10,
                    'where' => $_where,
                    'table' => 'merchants_task',
                    'field' => '*',
                    'order' => 'id asc'
                );
                $task = BaseService::DataList($data_params);
                $data = MerchantService::DataDealWithTask($task);
                $this->assign('task', $data[0]);
            }

            $this->assign('id', $id);
            return $this->fetch();
        }
    }

    /*
     * @content  任务删除
     * */
    function taskDel()
    {
        $taskId = input('post.id');
        if (!empty($taskId)) {
            # 查询是否存在已经领取任务没完成的，存在不能删除
            $taskCount = Db::table('merchants_task_log')->where(array('merchants_task_id' => $taskId))->where('status =1')->count();
            if ($taskCount > 0) {
                # 不能删除
                return array('status' => false, 'msg' => '任务正在进行中，不可删除');
            }
            # 判断任务的开始时间和结束时间
            $taskTime = Db::table('merchants_task')->where(array('id' => $taskId))
                ->where("start_time <= '" . date('Y-m-d H:i:s') . "' and end_time >= '" . date('Y-m-d H:i:s') . "'")
                ->find();
            if (!empty($taskTime)) {
                # 不能删除
                return array('status' => false, 'msg' => '任务正在进行中，无法删除');
            }
            # 先删除门店任务列表
            Db::table('merchants_task_log')->where(array('merchants_task_id' => $taskId))->delete();
            # 删除任务列表
            Db::table('merchants_task')->where(array('id' => $taskId))->delete();
            return array('status' => true);
        } else {
            return array('status' => false, 'msg' => '系统错误');
        }
    }

    /*
     * @content 红包管理最外层
     * */
    function redpacket_detail_index()
    {
        if (input("get.action") == 'ajax') {
            $time = date("Y-m-d H:i:s");
            #查询红包领取数量
            $_where = "id != 0";
            /*  #可按名称 和时间 筛选
              $param = input("post.param");

              if(!empty($param['title'])){
                  $_where .=" and title like '%".$param['title']."%'";
              }
              if(!empty($param['status'])){
                  if($param['status'] == 1){
                      #待发放
                      $_where .=" and '$time' < start_time and '$time' < end_time";

                  }else if($param['status'] == 2){
                      #发放中
                      $_where .=" and '$time' >= start_time and '$time' <= end_time";
                  }else if($param['status'] == 3){
                      #已结束
                      $_where .=" and '$time' > start_time and '$time' > end_time";
                  }
              }*/
            #按时间排序
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_redpacket_log',
                'field' => '*, sum(num) already_count_num',
                'order' => 'create_time desc',
                'group' => 'DATE_FORMAT(create_time,\'%Y-%m-%d\')'
            );
            $dataList = BaseService::DataList($data_params);
            $data = MerchantService::DataDealWithReadpacketLog($dataList);
            $total = (Db::table('merchants_redpacket_log')->where($_where)->group('DATE_FORMAT(create_time,\'%Y-%m-%d\')')->count());
//            $total = BaseService::DataTotal('merchants_redpacket_log',$_where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {

            #查询 正在显示的的红包  符合当前时间的
            $time = date("Y-m-d H:i;s");
            $_where = "'$time' >= start_time and '$time' <= end_time";
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_redpacket',
                'field' => '*',
                'order' => 'id asc',

            );
            $dataList = BaseService::DataList($data_params);
            $data = MerchantService::DataDealWithReadpacket($dataList);
            if (empty($data)) {
                $data = array();

            }

            $this->assign('info', $data[0]);
            $this->assign('infoCount', COUNT($data));
            return $this->fetch();
        }

    }

    /*
     * @content 红包管理列表
     * */
    function redpacket_index()
    {
        if (input("get.action") == 'ajax') {
            $time = date("Y-m-d H:i:s");
            #查询任务
            $_where = "id != 0";
            #可按名称 和时间 筛选
            $param = input("post.param");

            if (!empty($param['title'])) {
                $_where .= " and title like '%" . $param['title'] . "%'";
            }
            if (!empty($param['status'])) {
                if ($param['status'] == 1) {
                    #待发放
                    $_where .= " and '$time' < start_time and '$time' < end_time";

                } else if ($param['status'] == 2) {
                    #发放中
                    $_where .= " and '$time' >= start_time and '$time' <= end_time";
                } else if ($param['status'] == 3) {
                    #已结束
                    $_where .= " and '$time' > start_time and '$time' > end_time";
                }
            }
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_redpacket',
                'field' => '*',
                'order' => 'id desc'
            );
            $dataList = BaseService::DataList($data_params);
            $data = MerchantService::DataDealWithReadpacket($dataList);
            $total = BaseService::DataTotal('merchants_redpacket', $_where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }

    }

    /*
     * @contetn 红包管理编辑
     * */
    function SaveDataRedpacket()
    {
        $id = input("get.id");
        if (input("get.action") == 'ajax') {
            #查询任务
            $params = input('post.');
            if (!empty($params)) {
                return MerchantService::SaveMerchantsRedpacket($params);
            }
        } else {
            #查询任务
            if (!empty($id)) {
                $_where = " id=$id";
                $data_params = array(
                    'page' => false,
                    'number' => 10,
                    'where' => $_where,
                    'table' => 'merchants_redpacket',
                    'field' => '*',
                    'order' => 'id asc'
                );
                $task = BaseService::DataList($data_params);
                $data = MerchantService::DataDealWithReadpacket($task);
                $this->assign('redpacket', $data[0]);
            }

            $this->assign('id', $id);
            return $this->fetch();
        }
    }

    /*
    * @contetn 红包删除
    * */
    function redpacketDel()
    {
        $id = input("post.id");
        if (!empty($id)) {
            #删除红包
            Db::table('merchants_redpacket')->where("id = $id")->delete();
            return array('status' => true);
        }

    }


    /*
     * @content 获取一级分类cate_pid  和 二级分类 cate_id  存到缓存
     * @array
     * */
    function cateSession()
    {
        $info = input("post.");
        $merchants_id = input("get.id");//商户id
        $is_type = input("get.is_type");// 1 本地生活 2 联盟商家  默认联盟商家 才走这个  本地生活 不走
        $info['merchants_id'] = $merchants_id;
        $info['is_type'] = $is_type;
        $count_S = 0;
        $count_DB = 0;
        #求缓存中和数据库中的 一级分类id   作为条件 查询 不是这些的一级分类
        $_str_id = MerchantService::getCatePidStr($merchants_id);
        $_str_id_arr = explode(',', $_str_id);
        #判断新添加的一级分类id 是否在这个数组里  在 提示 不要重复添加分类名称
        if (in_array($info['cate_id'], $_str_id_arr)) {
            return array('status' => false, 'msg' => '不要重复添加分类名称!');
        }

        $_session_array = Session('getCateSession');
        if (!empty($_session_array)) {
            $count_S = count($_session_array);
        }
        if (!empty($merchants_id)) {
            $count_DB = Db::table('merchants_cate_relation')->where(array('merchants_id' => $merchants_id))->count();

        }
        #统计关联分类一共多少
        $count = intval($count_S) + intval($count_DB);

        if ($count == 3) {
            return array('status' => false, 'msg' => '关联分类不能超过3个!');
        } else {
            #处理 一级分类 id  和 二级分类id 存缓存 最后 返回拼接的 html  展示在 页面
            $data = MerchantService::getCateSession($info);
            return array('status' => true, 'data' => $data);

        }


    }

    /*
     * @content  删除 完事的方法
     * */
    function cateDelSession()
    {
        $info = input("post.");
        $merchants_id = input("get.id");//商户id
        $is_type = input("get.is_type");// 1 本地生活 2 联盟商家  默认联盟商家 才走这个  本地生活 不走
        $info['merchants_id'] = $merchants_id;
        $info['is_type'] = $is_type;
        #处理 一级分类 id  和 二级分类id 存缓存 最后 返回拼接的 html  展示在 页面
        $data = MerchantService::getcateDelSession($info);
        return array('status' => true, 'data' => $data);


    }

    /*
      * @content 删除分类
      * @param k 键值
      * @param mark S 缓存  DB 数据库
      * @param cate_id 二级分类id
      * @param cate_pid 一级分类id
      * @param id merchants_cate_relation 表的id
      * */
    function cateDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            #数据库
            Db::table('merchants_cate_relation')->where(array('id' => $info['id']))->delete();
            return array('status' => true);
        } else {
            # 操作session
            $_session = Session('getCateSession');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    Session::set('getCateSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }

    }

    /*
     * @content 修改商户数量
     * */
    function getMerchantsNum()
    {
        $info = input("post.");
        if ($info['mark'] == 'thumbup_num') {
            $num = 'thumbup_num';
        } else if ($info['mark'] == 'forward_num') {
            $num = 'forward_num';
        } else if ($info['mark'] == 'look_num') {
            $num = 'look_num';
        } else if ($info['mark'] == 'start') {
            $num = 'start';
        }
        #计算下当前商户的点赞数 和 查看数量 算出增加多少  然后判断 当前是否有点赞  查看任务 有->数量增加
        $merchants = Db::table('merchants')->where(array('id' => $info['id']))->field('id,thumbup_num,look_num')->find();
        $data = Db::table('merchants')->where(array('id' => $info['id']))->update(array($num => $info['val']));
        if ($data) {

            $params['merchants_id'] = $info['id'];
            if ($info['mark'] == 'thumbup_num') {
                #增加点赞数
                $params['num'] = intval($info['val'] - $merchants['thumbup_num']);
                \app\businessapi\ApiService\TaskService::getTaskPaySucess($params, 8);
            } else if ($info['mark'] == 'look_num') {
                #增加查看数
                $params['num'] = intval($info['val'] - $merchants['look_num']);
                \app\businessapi\ApiService\TaskService::getTaskPaySucess($params, 7);;
            }


        }
        return array('status' => true, 'val' => $info['val']);


    }

    /*
     * @content 删除 员工
     * */
    function delStaffInfo()
    {
        return array('status' => true);
    }

    /*
    * @content 修改开关状态
    * @param  mark  类型  sing:签到

    * */
    function changeSwitch()
    {
        $id = input("post.id");
        $value = input("post.value");

        if (!empty($id)) {
            Db::table('merchants_redpacket')->where(array('id' => $id))->update(array('status' => $value));
            return array('status' => true);
        }
    }

    /*
     * @content  查询商户 信息
     * */
    function SaveRedpacketLog()
    {
        $create_time = input("get.create");
        $create = date('Y-m-d', strtotime("-1day", strtotime($create_time)));;

        if (input("get.action") == 'ajax') {
            if (!empty($create)) {
                $_where = "DATE_FORMAT(create_time,'%Y-%m-%d') = '$create'";
            }
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_redpacket_log',
                'field' => '*',
                'order' => 'id asc',
            );
            $dataList = BaseService::DataList($data_params);
            $data = MerchantService::DataDealWithSaveReadpacket($dataList, $create);
            $total = BaseService::DataTotal('merchants_redpacket_log', $_where);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('create', $create_time);

            return $this->fetch();

        }

    }

    /*
     * @content 删除到期时间的商户
     * */
    function DelInfo()
    {
        #修改状态为 renew_status = 1
        Db::table('merchants')->where(array('id' => input('post.id')))->update(array('renew_status' => 1));
        return array('status' => true);
    }

    /*
     * @content 到期 商户 联盟商家类型
     * */
    function merchants_expiration()
    {
        if (input("get.action") == 'ajax') {

            $time = date("Y-m-d");
            $datetime = date('Y-m-d', strtotime(" + 15 day"));
            $_where = " renew_status = 0 and status = 1 and level<3";
            $params = input("post.");
            #员工筛选 地区筛选  分类筛选
            //分类
            if (!empty($params['param']['cate_id'])) {
                #查询出对应的商户id
                $merchants_id = Db::table('merchants_cate_relation')->where("cate_pid = '" . $params['param']['cate_id'] . "'")->column('merchants_id');
                $merchants_id_str = implode(',', $merchants_id);
                $_where .= " and id in ($merchants_id_str)";

//                $_where .=" and relate_employee = '".$params["param"]['employee']."'";
            }
            //员工
            if (!empty($params['param']['employee'])) {
                $_where .= " and relate_employee = '" . $params["param"]['employee'] . "'";
            }
            //省
            if (!empty($params['param']['province'])) {
                $_where .= " and province = '" . $params["param"]['province'] . "'";
            }
            //市
            if (!empty($params['param']['city'])) {
                $_where .= " and city = '" . $params["param"]['city'] . "'";

            }
            //区
            if (!empty($params['param']['area'])) {
                $_where .= " and area = '" . $params["param"]['area'] . "'";

            }
            //商家类型 is_type
            if (!empty($params['param']['is_type'])) {
                $_where .= " and is_type = '" . $params["param"]['is_type'] . "'";

            }
            //剩余天数 expire_day
            if (!empty($params['param']['expire_day'])) {
                if ($params['param']['expire_day'] == 1) {

                    #当前时间 +1天
                    $datetime = date('Y-m-d', strtotime(" + 1 day"));

                } else if ($params['param']['expire_day'] == 5) {
                    #当前时间 +5 天
                    $datetime = date('Y-m-d', strtotime(" + 5 day"));

                } else if ($params['param']['expire_day'] == 10) {
                    #当前时间 +10 天
                    $datetime = date('Y-m-d', strtotime(" + 10 day"));

                } else if ($params['param']['expire_day'] == 15) {
                    #当前时间 +15天
                    $datetime = date('Y-m-d', strtotime(" + 15 day"));

                }

            }
//            $_where .="  and '$time'<= DATE_FORMAT(expire_time,'%Y-%m-%d') and '$datetime' >= DATE_FORMAT(expire_time,'%Y-%m-%d') ";
            $_where .= "  and '$time'<= DATE_FORMAT(expire_time,'%Y-%m-%d') and '$datetime' >= DATE_FORMAT(expire_time,'%Y-%m-%d') ";


            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants',
                'field' => '*,DATE_FORMAT(expire_time,\'%Y-%m-%d\') expire_time',
                'order' => 'id asc',
            );
            $dataList = BaseService::DataList($data_params);

            #查询到期商户标识 merchangts_expiration
            $data = MerchantService::DataDealWith($dataList);
            $total = BaseService::DataTotal('merchants', $_where);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $where = "id > 0";

            $data_params = array(
                'page' => false,
                'where' => $where,
                'table' => 'employee_sal',
                'order' => 'employee_create desc',
                'field' => 'id,employee_name'

            );
            $data = EmployeeService::EmployeeDataList($data_params);
            $this->assign('employee', $data);
            #查询分类一级 联盟商家类型
            $cates = Db::name('merchants_cate')->where("pid=0 and type=2")->select();
            $this->assign('cates', $cates);
            return $this->fetch();

        }
    }

    /*
    * @content 删除邮寄物料
    * */
    function materielDelInfo()
    {
        #修改状态为 renew_status = 1
        Db::table('merchants_materiel')->where(array('id' => input('post.id')))->update(array('status' => 3));
        return array('status' => true);
    }

    /*
     * @content 邮寄物料
     * */

    function merchants_materiel()
    {
        $status = input('get.status');
        if (input("get.action") == 'ajax') {
            $_where = "id > 0";
            #默认查询未邮寄的

            $params = input("post.");
            //类型 status

            if (!empty($params['param']['status'])) {
                if ($params['param']['status'] == 'all') {
                    $_where .= " and status = 1 or status = 2";
                } else {
                    $status = $params['param']['status'];
                    $_where .= " and status = '" . $params["param"]['status'] . "'";
                }

            } else {
                if (!empty($status)) {
                    $_where .= " and status = $status";

                }
            }
            //省
            if (!empty($params['param']['province'])) {
                $_where .= " and province = '" . $params["param"]['province'] . "'";
            }
            //市
            if (!empty($params['param']['city'])) {
                $_where .= " and city = '" . $params["param"]['city'] . "'";

            }
            //区
            if (!empty($params['param']['area'])) {
                $_where .= " and area = '" . $params["param"]['area'] . "'";

            }


            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_materiel',
                'field' => '*',
                'order' => 'id asc',
            );
            $dataList = BaseService::DataList($data_params);

            #查询到期商户标识 merchangts_expiration
            if (!empty($dataList)) {
                foreach ($dataList as $k => $v) {
                    if (isset($v['status'])) {
                        if ($v['status'] == 1) {
                            $dataList[$k]['is_status_title'] = '未邮寄';

                        } else if ($v['status'] == 1) {
                            $dataList[$k]['is_status_title'] = '已邮寄';

                        } else if ($v['status'] == 1) {
                            $dataList[$k]['is_status_title'] = '拒收    ';

                        }
                    }
                    $dataList[$k]['materiel_address'] = $v['province'] . $v['city'] . $v['area'] . $v['address'];
                }
            }


            $total = BaseService::DataTotal('merchants_materiel', $_where);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $dataList];
        } else {
            $this->assign('status', $status);

            return $this->fetch();

        }
    }

    /*
  * @content 修改邮寄状态

  * */
    function materielSwitch()
    {
        $id = input("post.id");
        $value = input("post.value");

        if (!empty($id)) {
            Db::table('merchants_materiel')->where(array('id' => $id))->update(array('status' => $value));
            return array('status' => true);
        }
    }

    /*
     * @content  导出
     * */
    function DataExcel()
    {
        $params = input();
        $where = " id in('" . $params['id'] . "')";
        $data_params = array(
            'page' => true,
            'number' => 10,
            'table' => 'merchants',
            'where' => $where,
            'field' => 'id,title,contacts_phone,province,city,area,address,relate_employee,expire_time,is_type,level',

        );
        $data = BaseService::DataList($data_params);
        #查询下 商户所关联的分类

        $data = MerchantService::DataDealWith($data);


        $excel_params = [
            'filename' => '即將到期商户',
            'title' => lang('excel_action_merchants_expire_list'),
            'data' => $data
        ];
        $excel = new Excel($excel_params);
        return $excel->Export();
    }/*
     * @content  导出
     * */
    function materielDataExcel()
    {
        $params = input();
        $where = " id in('" . $params['id'] . "')";
        $data_params = array(
            'page' => true,
            'number' => 10,
            'table' => 'merchants_materiel',
            'where' => $where,
            'field' => '*',

        );
        $dataList = BaseService::DataList($data_params);
        #查询下 商户所关联的分类
        if (!empty($dataList)) {
            foreach ($dataList as $k => $v) {
                if (isset($v['status'])) {
                    if ($v['status'] == 1) {
                        $dataList[$k]['is_status_title'] = '未邮寄';

                    } else if ($v['status'] == 1) {
                        $dataList[$k]['is_status_title'] = '已邮寄';

                    } else if ($v['status'] == 1) {
                        $dataList[$k]['is_status_title'] = '拒收    ';

                    }
                }
                $dataList[$k]['materiel_address'] = $v['province'] . $v['city'] . $v['area'] . $v['address'];
            }
        }


        $excel_params = [
            'filename' => '邮寄物料',
            'title' => lang('excel_action_merchants_materiel_list'),
            'data' => $dataList
        ];
        $excel = new Excel($excel_params);
        return $excel->Export();
    }

    function changeExpiration()
    {
        $val = input("post.value");
        $start_time = input("post.start_time");
        return array("status" => true, "date" => date("Y-m-d", strtotime("+" . $val . " month", strtotime($start_time))));
    }

    function proSave()
    {
        if (input("get.action") == "ajax") {
            $params = input("post.");
            $ret = $this->valiData($params);
            if ($ret['status'] !== true) {
                throw new \BaseException(['code' => 403, 'errorCode' => $ret["errcode"], 'msg' => $ret["msg"], 'status' => false, 'debug' => false]);
            }
            if (!empty($params['describe_title'])) {
                $_array = array();
                foreach ($params['describe_title'] as $k => $v) {
                    array_push($_array, array("title" => $v, "discribe" => $params['describe_cont'][$k]));
                }
                $params['describe'] = json_encode($_array);
                unset($params['describe_title'], $params['describe_cont']);
            }
            if (!empty($params['notice_title'])) {
                $_array = array();
                foreach ($params['notice_title'] as $k => $v) {
                    array_push($_array, array("title" => $v, "discribe" => $params['notice_cont'][$k]));
                }
                $params['notice'] = json_encode($_array);
                unset($params['notice_title_title'], $params['notice_title_cont']);
            }
            if (!empty($params['id'])) {
                # 修改
                $id = $params['id'];
                if (empty($params['imgurl'])) {
                    $params['imgurl'] = $params['old_imgurl'];
                }
                unset($params['id'], $params['thumb_img'], $params['old_imgurl']);
                Db::table("merchants_pro")->where(array("id" => $id))->update($params);

            } else {
                # 添加
                unset($params['id'], $params['thumb_img']);
                Db::table("merchants_pro")->insertGetId($params);
            }
            return DataReturn('保存成功', 0);
        } else {
            $id = input("get.id");
            if (!empty($id)) {
                $data = Db::table("merchants_pro")->where(array("id" => $id))->find();
                if (!empty($data['banner'])) {
                    $this->assign('bannerArr', explode(',', $data['banner']));
                }
                if (!empty($data['detailimage'])) {
                    $this->assign('detailArr', explode(',', $data['detailimage']));
                }

            } else {
                $merchants_id = input("get.merchants_id");
                $data = array("merchants_id" => $merchants_id);
            }
            $this->assign("data", $data);
            return view();
        }

    }

    function valiData($params)
    {
        if (empty($params['title'])) {
            return array("status" => false, "errcode" => 101, "msg" => "请输入名称");
        }
        if (empty($params['price'])) {
            return array("status" => false, "errcode" => 102, "msg" => "请输入价格");
        }
        if (empty($params['original_price'])) {
            return array("status" => false, "errcode" => 103, "msg" => "请输入原价");
        }
        if (empty($params['num'])) {
            return array("status" => false, "errcode" => 104, "msg" => "请输入上架数量");
        }
        if (empty($params['start_time'])) {
            return array("status" => false, "errcode" => 105, "msg" => "请输入上架时间");
        }
        if (empty($params['end_time'])) {
            return array("status" => false, "errcode" => 106, "msg" => "请输入下架时间");
        }
        if (empty($params['tips'])) {
            return array("status" => false, "errcode" => 107, "msg" => "请输入标签");
        }
        if (empty($params['id'])) {
            if (empty($params['imgurl'])) {
                return array("status" => false, "errcode" => 108, "msg" => "请输入缩略图");
            }
            if (empty($params['banner'])) {
                return array("status" => false, "errcode" => 109, "msg" => "请输入缩略图");
            }
            if (empty($params['detailimage'])) {
                return array("status" => false, "errcode" => 110, "msg" => "请输入缩略图");
            }
        }
        return array("status" => true);
    }

    /**
     * [ActionExcel 开始提现导出数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function ActionExcel()
    {
        $params = input();
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => ['id' => $params['id']],
            'table' => 'merchants',
            'field' => '*',

        );

        $info = BaseService::DataList($data_params);
        $info = MerchantService::DataDealWith($info);
        if (!empty($info)) {
            $excel_params = [
                'filename' => $info['title'] . '商家审核',
                'title' => lang('excel_action_merchants_list'),
                'data' => $info
            ];
            $excel = new Excel($excel_params);
            return $excel->Export();
        }

    }

    function auditSuccess()
    {
        $employee_id = input("post.employee_id");
        $merchants_id = input("post.merchants_id");
        Db::table("merchants")->where(array("id" => $merchants_id))->update(array("relate_employee" => $employee_id, "status" => 1));
        # 发短信----10、商家入驻审核通过通知(·后台商家入驻审核操作通过·推送商家联系方式)
        $merchantsInfo = Db::table('merchants')->field('tel')->where(array('id' => $merchants_id))->find();
        if (!empty($merchantsInfo['tel'])) {
            $SmsCode = new SmsCode();
            $smsInfo = '恭喜您：您的入住申请审核已通过，请在“E联商家版”APP登录，设置相关信息。';
            $SmsCode->Ysms($merchantsInfo['tel'], $smsInfo, false);
        }
        return array("status" => true);
    }

    function auditFail()
    {
        $id = input("post.id");
        $agent_id = input("post.agent_id");
        $mark = input("post.mark");
        Db::table("merchants")->where(array("id" => $id))->update(array("status" => 2));
        if ($mark != 'delete') {

        }
        return array("status" => true);
    }
}
