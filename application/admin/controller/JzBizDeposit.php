<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\BizApplyService;
use app\service\BizDepositService;
use base\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use think\Db;

/**
 * 门店提现申请管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月3日15:04:32
 */
class JzBizDeposit extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月3日15:04:32
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [Index 门店提现申请列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {

        if (input('get.action') == 'ajax') {
            $params = input();
            if (array_key_exists('params', $params)) {
                $params = $params['param'];
            }
            // 条件
            $where = BizDepositService::ListWhere($params);
            $where[] = ['source_type', 'in', [1, 3]];
            $where[] = ['deposit_status', 'in', [1, 2]];

            // dump($where);exit;deposit_status asc,
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'log_deposit',
                'field' => '*,(case source_type
                    when 1 then (select b.biz_title from biz b where b.id=biz_id) 
                    when 2 then (select m.member_name from member m where m.id=member_id)
                    when 3 then (select m.title from  merchants m where m.id=biz_id)
                    end) title_info',
                'order' => 'id desc'
            );

            $data = BaseService::DataList($data_params);

            $data = BizDepositService::DataDealWith($data);

            $total = BaseService::DataTotal('log_deposit', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [ActionExcel 开始提现导出数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function ActionExcel()
    {
        $params = input();
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => ['id' => $params['id']],
            'table' => 'log_deposit',
            'field' => '*',

        );

        $info = BaseService::DataList($data_params);
        $info = BizDepositService::DataDealWith($info);

        $params['deposit_status'] = 2;
        $re = BizDepositService::ApplySave($params);

        if ($re['status']) {
            $excel_params = [
                'filename' => '提现审核申请',
                'title' => lang('excel_action_deposit_list'),
                'data' => $info
            ];
            $excel = new Excel($excel_params);
            return $excel->Export();
        }

    }

    /**
     * [ActionDeposit 提交提现操作结果]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function ActionDeposit()
    {
        $params = input();
        return BizDepositService::DepositResultSave($params);
    }

    /**
     * [DelDeposit 提现删除]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function DelDeposit()
    {
        $params = input();
        return BizDepositService::DepositDel($params);
    }
}