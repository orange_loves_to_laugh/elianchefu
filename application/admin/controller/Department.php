<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\EmployeeService;
use think\Db;

/**
 * 部门管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月16日11:33:06
 */
class Department extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月16日11:33:06
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();
        $this->IsLogin();
    }

    /**
     * [Index 部门列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {

        if (input('get.action')=='ajax') {


            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => [['is_del','=',2]],
                'table'=>'employee_department',

            );
            $data = EmployeeService::DepartmentDataList($data_params);

            $total = BaseService::DataTotal('employee_department',[['is_del','=',2]]);



            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{

            return $this->fetch();
        }

    }
    /**
     * [SaveInfo 部门添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日13:48:10
     */
    public function SaveData(){

        // 参数
        $params = input();
        // 数据
        $data = [];
        $where=[
            //'is_del = 2 and higher_department_id=0'
            ['is_del','=',2,],
            ['higher_department_id','=',0],
        ];
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'employee_department',
            ];

            $ret = EmployeeService::DepartmentDataList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
            $where=[
                //'is_del = 2 and higher_department_id=0'
                ['is_del','=',2,],
                ['higher_department_id','=',0],
                ['id','<>',$params['id']],
            ];
        }

        $this->assign('department_arr',Db::name('employee_department')->where($where)->field('id,title')->select());
        $this->assign('employee_arr',Db::name('employee_sal')->where('is_del = 2')->field('id,employee_name')->select());

        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function Save()
    {

        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return EmployeeService::DepartmentDataSave($params);


    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日18:21:37
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='employee_department';
        $params['soft']=true;
        $params['soft_arr']=['is_del'=>1];
        $params['errorcode']=19013;
        $params['msg']='删除失败';

        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);

    }

    /**
     * [获取员工列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日18:21:37
     */
    public function changemanager(){

        return DataReturn('ok',0,Db::name('employee')->where('is_del = 2')->field('id,employee_name')->select());
    }

    /**
     * [DataList 搜索部门列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function DataList()
    {

        $where=EmployeeService::DepartmentListWhere(input());

        $data_params = array(
            'page'         => false,
            'where'     => $where,
            'table'=>'employee_department',

        );
        $data_department = EmployeeService::DepartmentDataList($data_params);

        $where=EmployeeService::StationListWhere(input());
        $data_params = array(
            'page'         => false,
            'where'     => $where,
            'table'=>'employee_station',

        );
        $data_station = EmployeeService::StationDataList($data_params);



        return ['code' => 0, 'msg' => '', 'data' => $data_department,'data_station'=>$data_station];

    }
}