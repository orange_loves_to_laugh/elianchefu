<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/28 0028
 * Time: 14:51
 */

namespace app\admin\controller;

use app\service\BaseService;
use app\service\PowerService;
use think\Db;

/**
 * 权限管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2021年1月28日14:53:26
 */
class Power extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }


    /**
     * [Index 员工列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {
//        return $this->error('权限不足');

        if (input('get.action')=='ajax') {

            $where=PowerService::ListWhere(input());
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'       => $where,
                'order'       =>'level asc',
            );


            $data=PowerService::PowerDataList($data_params);

            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{

            return $this->fetch();
        }

    }

    /**
     * [SaveInfo 权限添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日13:48:10
     */
    public function SaveData(){

        // 参数
        $params = input();
        // 数据
        $data = [
            'calling_status'=>2,
            'pid_title'=>'请选择上级模块'
//            'employee_status'=>1,
        ];
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'order'       =>'id desc',
            ];

            $ret=PowerService::PowerDataList($data_params);

            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function Save()
    {

        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return PowerService::PowerDataSave($params);


    }
    /**
     * [DataList 获取权限信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function DataList(){
        $where=PowerService::ListWhere(input());
        //dump($where);exit;
        $data_params = array(
            'page'         => false,
            'where'       => $where,
            'order'       =>'id desc',
        );


        $data=PowerService::PowerDataList($data_params);

        return DataReturn('ok',0,$data);


    }
    /**
     * [AccountIndex 账号列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2021年2月6日15:39:40
     */
    public function AccountIndex()
    {


        if (input('get.action')=='ajax') {

            $where=PowerService::AccountListWhere(input());

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'       => $where,
                'order'       =>'add_time asc',
            );


            $data=PowerService::AccountDataList($data_params);

            return ['code' => 0, 'msg' => '',  'data' => $data["data"]];
        }else{

            return $this->fetch();
        }

    }

    /**
     * [SaveAccount 添加账号页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年2月6日14:38:55
     */
    public function SaveAccount(){
        $params = input();
        $data = [
            'login_salt'=>GetNumberCode(),

            'employee_department_sectitle'=>'请选择二级部门',

            'employee_station_title'=>'请选择岗位',

            'employee_name'=>'请选择员工',

            'power_arr'=>[],

            'power_status_datacenter'=>2,

            'power_status_member'=>2
        ];
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'order'       =>'add_time asc',
            ];

            $ret=PowerService::AccountDataList($data_params);

            //
            $data = empty($ret["data"][0]) ? [] : $ret["data"][0];
        }

        $employee_department = Db::name('employee_department')->where('is_del = 2 and higher_department_id=0')->field('id,title')->select();

        //用户管理相关权限
        $power_arr_member=Db::name('power')->where(['pid'=>15])->select();

        //数据中心权限
        $power_arr_datacenter=Db::name('power')->where(['pid'=>14])->select();

        //dump($power_arr_datacenter);exit;
        $this->assign('power_arr_datacenter', $power_arr_datacenter);

        $this->assign('power_arr_member', $power_arr_member);

        $this->assign('department_arr', $employee_department);

        $this->assign('data', $data);

        return $this->fetch();
    }

    public function DoSaveAccount(){
        $param=input();

        return PowerService::SaveAccount($param);

    }
}