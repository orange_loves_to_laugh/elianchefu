<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 9:39
 */

namespace app\admin\controller;

use app\service\BaseService;
use app\service\ClubService;
use app\service\NoiceService;
use app\service\ResourceService;

/**
 * 俱乐部管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月23日09:27:10
 */
class JzClub extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 俱乐部列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {
        if (input('get.action')=='ajax') {
            $params = input();
            // 条件
            $where = ClubService::ClubListWhere($params);
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'club'
            );
            $data=ClubService::ClubList($data_params);

            $total = BaseService::DataTotal('club',$where);
            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [SaveData 俱乐部添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月9日09:39:00
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'club'
            ];

            $data=ClubService::ClubList($data_params);
            $data = empty($data[0]) ? [] : $data[0];
        }
        //多余图片处理
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'club'.$params['id'];
        ResourceService::delUploadCache();
        // dump($data);exit;
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function Save()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return ClubService::ClubSave($params);
    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='club';
        $params['soft']=false;
        $params['errorcode']=900;
        $params['msg']='删除失败';
        BaseService::DelInfo($params);
        return DataReturn('删除成功', 0);

    }

    function changeNum()
    {
        $params = input();
        return ClubService::changeNumService($params);
    }
}