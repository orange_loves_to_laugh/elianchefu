<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/29
 * Time: 9:47
 */

namespace app\admin\controller;


use app\api\ApiService\AdvanceService;
use app\reuse\controller\ResponseJson;
use app\service\ApplayService;
use app\service\BaseService;
use app\service\MerchantService;
use app\service\ResourceService;
use think\Db;

class Applay extends Common
{
    use ResponseJson;

    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [banner 首页banner]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function banner()
    {
        #banner 标识 首页banner  会员页 cardbanner 二手车 carbanner
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            $_where = array("application_mark" => $mark);
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => 10, //分页个数
                'where' => $_where,//查找条件
            );

            $data = ApplayService::ApplayIndex($data_params);
            $data = $data->toArray();
            if(!empty($data['data'])){
                foreach($data['data'] as $k=>$v){
                    if($v['is_jump']==1 and $v['is_type']==2){
                        if($v['jump_type']==9){
                            $jump = AdvanceService::getsJumpUrl($v['jump_type'], $v['jump_id'],$v['pop_url']);
                            $data['data'][$k]['application_uri'] = $jump['url'];
                        }else{
                            $data['data'][$k]['application_uri'] = lang('image_jump_type')[$v['jump_type']];
                        }

                    }
                }
            }
            return $this->jsonSuccessData($data);


        } else {
            $this->assign('mark', $mark);
            $this->assign('mark_title', lang('application_banner_type')[$mark]);
            return $this->fetch();
        }

    }

    /**
     * [banner 首页banner添加 修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function savebanner()
    {
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空

                $params['mark']=$mark;

                $data = ApplayService::AppInsertWrite($params, $mark,'savebanner');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            //$id = Db::table("application")->where(array('application_mark' => $mark))->find();
            $id = input("get.id");
            $info['is_jump']=2;
            if (!empty($id)) {
                $info = Db::table("application")->where(array('id' => $id))->find();

                #执行修改
//                $_bannerurl = explode(",", $info["application_image"]);
//                //dump($info["application_image"]);exit;
//                $this->assign("bannerArr", $_bannerurl);
//
//                #用来删除图片的 是个数组
//                $info["bannerurl"] = implode(',', array_map("change_to_quotes", $_bannerurl));
//
//                $this->assign('bannerurl', $info["bannerurl"]);
                $this->assign('id', $id);
                $info['jump_type_title']='';
                $info['jump_type_detail_title']='';
                if ($info['is_jump']==1)
                {
                    $info['jump_type_title']=ApplayService::JumpTypeTitle($info)['type'];
                    $info['jump_type_detail_title']=ApplayService::JumpTypeTitle($info)['name'];
                }
            }

            $this->assign('info', $info);
            #banner 添加页
            $this->assign('mark', $mark);
            $this->assign('application_banner_type', lang('application_banner_type'));
            $this->assign('image_jump_type', lang('image_jump_type'));

            return $this->fetch();
        }
    }

    /**
     * [Delapplay 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function Delapplay()
    {
        $id = input("post.id");
        if (!empty($id))
        {
            Db::table("application")->where(array('id' => $id))->delete();
            return json(DataReturn('删除成功', 0));
        }
    }

    /**
     * [saveprotocol 协议 关于我们   声明管理  添加  修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function saveprotocol()
    {
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空
                    $data = ApplayService::AppInsertWrite($params, $mark,'saveprotocol');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $info = Db::table("application")->where(array('application_mark' => $mark))->find();
            if (!empty($info)) {
                $this->assign('info', $info);
                $this->assign('id', $info['id']);
            }

            #banner 添加页
            $this->assign('mark', $mark);
            return $this->fetch();
        }

    }
    /**
     * [baseadvert  招募底图管理   广告管理  添加  修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function baseadvert()
    {

        $mark = input("get.mark");

        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));

            if (!empty($params)) {

                #=接收参数不为空
                $params['mark']=$mark;

                $data = ApplayService::AppInsertWrite($params, $mark,'baseadvert');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = Db::table("application")->where(array('application_mark' => $mark))->value('id');
            $info=[];
            if (!empty($id))
            {
                $info = Db::table("application")->where(array('id' => $id))->find();
                $info['jump_type_title']='';
                $info['jump_type_detail_title']='';
                if ($info['is_jump']==1)
                {
                    $info['jump_type_title']=ApplayService::JumpTypeTitle($info)['type'];
                    $info['jump_type_detail_title']=ApplayService::JumpTypeTitle($info)['name'];
                }

            }
            //多余图片处理
            $params['id']=empty($id['id'])?'':$id['id'];
            ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
            ResourceService::delUploadCache();

            $this->assign('info', $info);
            $image_type=BaseService::StatusHtml($mark,lang('application_banner_type'),false);
            $this->assign('image_jump_type', lang('image_jump_type'));
            $this->assign('image_type', $image_type);
            return $this->fetch();
        }

    }


    /**
     * [pop_index  首页弹窗管理  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function pop_index(){
        if (input("get.action") == 'ajax') {
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => 10, //分页个数
                'where' => "is_del = 2",//查找条件
            );

            $data = ApplayService::popIndex($data_params);
            return $this->jsonSuccessData($data);


        } else {
            return $this->fetch();
        }

    }
    /**
     * [pop_save  首页弹窗管理  添加 修改  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function pop_save(){
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空
                $data = ApplayService::popInsertWrite($params,$params['id']);
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = input("get.id");
            if (!empty($id)) {
                $info = Db::table("apop")->where(array('id' => $id))->find();


                $this->assign('id', $id);
                $info['jump_type_title']='';
                $info['jump_type_detail_title']='';
                if ($info['is_skip']==1 and $info['is_type']==2 and $info['jump_type']!=9)
                {
                    $info['jump_type_title']=ApplayService::JumpTypeTitle($info)['type'];
                    $info['jump_type_detail_title']=ApplayService::JumpTypeTitle($info)['name'];
                }
            }
            $this->assign('info', $info);
            $this->assign('application_banner_type', lang('application_banner_type'));
            $this->assign('image_jump_type', lang('image_jump_type'));
//            $bannerArr = explode('*',$info['pop_detail']);
//            $this->assign('bannerArr',$bannerArr);
            return $this->fetch();
        }
    }
    /**
     * [pop_save  首页弹窗管理  删除  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function popdel(){
        $id = input("post.id");
        if(!empty($id)){
            Db::table('apop')->where(array('id'=>$id))->update(array('is_del'=>1));
            return array('status' => true);
        }

    }
    /********************积分设置**********************/
    /**
     * [pop_save  积分设置   ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function integral_set(){
        if (input("get.action") == 'ajax') {
            #查询积分
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 20;
            }

            // 获取列表
            $data_params = array(
                'page'         => false, //是否分页
                'number'         => $listRows, //分页个数
//                'where'     => $_where,//查找条件
            );
            $data = ApplayService::IntegralIndex($data_params);

            $total=BaseService::DataTotal('integral_set',[['id','>',0]]);

            return ['code' => 0, 'msg' => '','data' => $data,'total'=>$total];

        }else{

            return $this->fetch();

        }

    }
    /**
     * [IntegralWrite  积分修改   ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function IntegralWrite(){
        $id = input("get.id");

        if (input("get.action") == 'ajax') {
            $params = input("post.");
           // dump($params);exit;
            if (isset($params['thumb_img']))
            {
                unset($params['thumb_img']);
                // dump($params);exit;
                ResourceService::$session_suffix='source_upload'.'integral_set'.$id;
                ResourceService::delCacheItem($params['share_thumb']);
            }


            Db::table('integral_set')->where(array('id'=>$id))->update($params);
            return array('status'=>true);

        }else{
            $info = Db::table('integral_set')->where(array('id'=>$id))->find();
            if(!empty($info['integral_type']))
            {
                $info['integral_type_title'] = BaseService::StatusHtml($info['integral_type'],lang('integral_type'),false);
            }
            //多余图片处理
            ResourceService::$session_suffix='source_upload'.'integral_set'.$id;
            ResourceService::delUploadCache();
            $this->assign('app_client',lang('app_client'));
            $this->assign('info',$info);
            $this->assign('id',$id);
            return $this->fetch();
        }

    }

    /**
     * [SearchData 根据关键字筛选相应数据]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function SearchData()
    {

        return ApplayService::SearchByCateAndKeywords(input());
    }
    /********************************商户端协议 ****************************************/
    /**
     * [merprotocol 协议 关于我们   添加  修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function merprotocol()
    {
        $mark = input("get.mark");
        $id = input("get.id");
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空
                $data = ApplayService::MerInsertWrite($params, $mark,'merprotocol');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $info =[];

            if($mark == 'aboutmine'){
                #联系电话
                $_infomark_phone = '9';
                #公司简介
                $_infomark_brief = '2';
                #公司优势
                $_infomark_advantage = '8';
                $phone_id = Db::table("merchants_application")->where(array('mark' => $_infomark_phone))->value('id');
                $brief_id = Db::table("merchants_application")->where(array('mark' => $_infomark_brief))->value('id');
                $advantage_id = Db::table("merchants_application")->where(array('mark' => $_infomark_advantage))->value('id');
                if (!empty($phone_id)) {
                    $info['phone'] = Db::table("merchants_application")->where(array('id' => $phone_id))->find();


                    $this->assign('phone_id', $phone_id);
                }
                if (!empty($brief_id)) {
                    $info['brief'] = Db::table("merchants_application")->where(array('id' => $brief_id))->find();

                    $this->assign('brief_id', $brief_id);
                }
                if (!empty($advantage_id)) {
                    $info['advantage'] = Db::table("merchants_application")->where(array('id' => $advantage_id))->find();

                    $this->assign('advantage_id', $advantage_id);
                }

            }else{
                $id = Db::table("merchants_application")->where(array('mark' => $mark))->value('id');
                if (!empty($id)) {
                    $info= Db::table("merchants_application")->where(array('id' => $id))->find();

                    $this->assign('id', $id);
                }
            }

            $this->assign('phone_mark', $_infomark_phone);
            $this->assign('brief_mark', $_infomark_brief);
            $this->assign('advantage_mark', $_infomark_advantage);
            $this->assign('info', $info);

            #banner 添加页
            $this->assign('mark', $mark);
            return $this->fetch();
        }

    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商家分类关联列表
     */
    function merchantsCateList(){
        $is_type=input("post.is_type");
        $MerchantService = new \app\api\ApiService\MerchantService();
        $list = $MerchantService->merchantCate("more", $is_type);
        array_unshift($list, array("id" => 0, "name" => "全部", "child" => array(array("id" => 0, "name" => "全部"))));
        return ApplayService::merchantsCateFormat($list,$is_type);

    }

}