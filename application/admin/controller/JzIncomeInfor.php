<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\FinanceService;
use app\service\NoiceService;
use app\service\StatisticalService;
use think\Db;

/**
 * 今日收入数据显示
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月3日15:04:32
 */
class JzIncomeInfor extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月3日15:04:32
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [Index 门店提现申请列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index(){
        //方式统计
        $time=explode('-',date('Y-m-d',time()));
        $incometype=StatisticalService::IncomeTypeDayInfo(['year'=>$time[0],'month'=>$time[1],'day'=>$time[2]]);

        $this->assign('incometype_today',$incometype['data']);

        //来源
        $incomesource=StatisticalService::IncomeSourceInfo(['year'=>$time[0],'month'=>$time[1],'day'=>$time[2]]);

        $this->assign('incomesource_today',$incomesource['data']);

        return $this->fetch();
    }
    /**
     * [UseBalance 余额使用情况列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function UseBalanceIndex(){

        if (input('get.action')=='ajax') {
            $params = input();

            // 数据
            $data = FinanceService::BizTrafficData($params);

           // $total = FinanceService::BizTrafficData($params)['data']['total'];

            return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
        }else{
            return $this->fetch();
        }
    }
    /**
     * [DepositPartner 合伙人提现信息]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function DepositPartner(){
        if (input('get.action')=='ajax') {
            $params = input();
            // 条件
            $where =  FinanceService::DepositListWhere($params);
            $where[]=['source_type','=',2];
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'log_deposit'
            );
            $data=FinanceService::DepositList($data_params);

            $total_num = BaseService::DataTotal('log_deposit',$where);

            $total_price = FinanceService::DepositTotalPrice($where);

            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data,'price'=>$total_price];
        }else{
            return $this->fetch();
        }
    }
}