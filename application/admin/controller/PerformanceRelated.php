<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/17 0017
 * Time: 19:58
 */

namespace app\admin\controller;


use app\service\BaseService;
use app\service\MecrhantService;
use app\service\PerformanceService;
use think\Db;

/**
 * 绩效管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月17日19:59:36
 */
class PerformanceRelated extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月22日11:41:20
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function Index()
    {

        if (input('get.action') == 'ajax') {

            $field = 'ec.id ec_id,ec.title,ec.check_time,ec.employee_station_id,ec.employee_id,
                    ec.create_time,ec.employee_department_id,ec.employee_department_secid';
            $where = PerformanceService::ListWhere(input());
            // 时间筛选
            $params = input();
            if (!empty($params['param']['searchTime'])) {
                $_where = "date_format(ec.check_time,'%Y-%m') = '" . $params['param']['searchTime'] . "'";
            } else {
                $_where = "date_format(ec.check_time,'%Y-%m') = '" . date('Y-m') . "'";
            }
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'field' => $field
            );
            $data = PerformanceService::DataList($data_params,$_where);

            $data = PerformanceService::DataDealWith($data);

            return ['code' => 0, 'msg' => '', 'data' => $data];
        } else {
            $this->assign('check_type_arr', lang('check_type'));

            return $this->fetch();
        }
    }

    /**
     * [SaveData 新建考核页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function CreateData()
    {

        $re = Db::name('employee_department')->where('is_del = 2 and higher_department_id=0')->field('id,title')->select();
        //dump($re);exit;
        $this->assign('department_arr', $re);
        $this->assign('year', date("Y", time()));
        $this->assign('month', date("m", time()));
        $this->assign('check_type_arr', lang('check_type'));
        $this->assign('station_arr', Db::name('employee_station')->where('is_del =2 and is_check=1')->field('title,id')->select());
        return $this->fetch();
    }

    /**
     * [Save 保存数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function Save()
    {

        return PerformanceService::DataSave($this->data_post);
    }

    /**
     * [EditData 查看数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function EditData()
    {
        // 参数
        $params = input();
        // 数据
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => ['ec.id' => $params['id']],
            'field' => 'ec.*',
            ////            ecr.id ,ecr.type,ecr.level,ecr.todo_num,ecr.reward_price,ecr.undo_punish_price,ecr.undo_punish,ecr.relate_check_ids,ecr.relate_check_str,ecr.undo_percentage'
        );
        $data = PerformanceService::DataList($data_params)['data'][0];
        $this->assign('type_arr', $data['type']);

        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * [EditDataCheckType 查看考核类型数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function EditDataCheckType()
    {
        // 参数
        $params = input();
        // 数据
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => ['ec.id' => $params['id']],
            'field' => 'ec.*',
        );
        $data = PerformanceService::DataList($data_params)['data'][0];

        $this->assign('type', $params['type']);
        $this->assign('data', $data['type'][$params['type']]);
        $applyMember = array_values($data['type'][$params['type']])[0]['applyMember'];
        $this->assign('applyMember', $applyMember);
        return $this->fetch();
    }

    /**
     * [RelateEmployee 获取关联员工]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function RelateEmployee()
    {
        return PerformanceService::EmployeeDataHtml(input());
    }


}
