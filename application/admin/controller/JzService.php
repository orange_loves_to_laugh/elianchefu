<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/22
 * Time: 13:37
 */

namespace app\admin\controller;

use app\service\ActiveService;
use app\service\AdminService;
use app\service\BaseService;
use app\service\BizService;
use app\service\InforService;
use app\service\ResourceService;
use app\service\ServiceCategory;
use Redis\Redis;
use think\Db;
use think\facade\Session;

/**
 * 服务管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月22日13:45:48
 */
class JzService extends Common
{


    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日13:45:48
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

         // 登录校验
        $this->IsLogin();


    }

    /**
     * [Index 列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日13:45:48
     */
    public function Index()
    {


        if (input('get.action')=='ajax') {
            $params = input();
            // 条件
            $where = AdminService::GetAdminIndexWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,

                'field'     =>'*',
            );
            $data=AdminService::ServicesList($data_params);

            $total = BaseService::DataTotal('service',$where);
           // dump($data[0]['service_biz_num']);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            #删除服务
            Session::delete('serviceId');
            #删除 门店id
            Session::delete('bizId');
            #关联赠送服务
            Session::delete('giveJzServiceInfo');
            #关联商品
            Session::delete('bizProInfoSession1');
            #关联配件
            Session::delete('bizProInfoSession2');
            #查询服务分类
            $service_class = Db::table('service_class')->field('service_class_title,id')
                ->where("is_del=2 and service_class_pid = 0")->select();

            $this->assign('service_class',$service_class);
            $bizId = input('get.biz_id')??0;
            $this->assign('biz_id',$bizId);
            return $this->fetch();
        }
    }
    /**
     * [ServiceBizNum 合作门店列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日13:45:48
     */
    public function ServiceBizNum(){
        if (input('get.action')=='ajax') {
            $params = input();

            $data=AdminService::ServiceBizList($params)['data'];
            //dump($data);exit;
            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [RecommendIndex 推荐管理（已推荐服务列表）]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function RecommendIndex()
    {
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where = AdminService::RecommendServiceWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>'*',
            );
            $data=AdminService::RecommendServiceList($data_params)['data'];

            $total = BaseService::DataTotal('service_recommend',$where);
            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $where=[
                'where'=>"is_del = 2 and service_class_pid = 0",
            ];
            $category=ServiceCategory::ServiceCategory($where)["data"];
            $this->assign('position_arr',lang('recommond_serviced_position'));
            $this->assign('service_class_arr',$category);
            return $this->fetch();
        }


    }

    /**
     * [DelRecommend 删除推荐的服务]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function DelRecommend()
    {
        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='service_recommend';
        $params['soft']=false;

        $params['errorcode']=20005;
        $params['msg']='删除推荐服务失败';
        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);
    }

    /**
     * [RecommendSave 添加/编辑推荐服务]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function RecommendSave()
    {
        // 参数
        $params = input();

        // 数据
        $data = [];

        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'field'     =>'*',
            ];

            $ret=AdminService::RecommendServiceList($data_params)['data'];
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }

        $where=[
            'where'=>"is_del = 2 and service_class_pid = 0",
        ];
        $category=ServiceCategory::ServiceCategory($where)["data"];
        $this->assign('service_class_arr',$category);
        $this->assign('position',lang('recommond_serviced_position'));
        $this->assign('data', $data);
        //dump($data);exit;
        return $this->fetch();
    }

    /**
     * [RecommendSave 添加/编辑推荐服务]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日16:55:57
     */
    public function RecommendDoSave(){

        return AdminService::RecommendServiceSaveData($this->data_post);
    }

    /**
     * [RecommendSave 添加/编辑推荐服务]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日16:55:57
     */
    public function RecommendStartTime(){
        return AdminService::RecommendServiceCalcStartTime($this->data_post);
    }


    /**
     * [AppointIndex 预约管理（已预约服务列表）]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日16:45:38
     */
    public function AppointIndex()
    {
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where = AdminService::AppointserviceListWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>'sb.id sb_id,b.biz_title,b.province,b.city,b.area,b.biz_type,s.service_title,s.class_pid,
                sb.settlement_min,sb.service_id,sb.biz_id',
            );
            /*st.biz_deduct,st.service_deduct,st.recommond_royalty, ,st.id sb_id*/
            $data=  AdminService::AppointserviceList($data_params)['data'];
            //dump($data);exit;
            $total = AdminService::AppointserviceDataTotal($where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $biz_arr=Db::name('biz')->where('biz_type > 1 and biz_status > 0')->field('id,biz_title')->select();
            $service_arr=Db::name('service')->where('service_status > 0')->field('id,service_title')->select();
            $this->assign('biz_arr',$biz_arr);
            $this->assign('service_arr',$service_arr);
            return $this->fetch();
        }


    }
    /**
     * [AppointSaveData 修改]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日16:45:38
     */
    public function AppointSaveData()
    {
        return AdminService::AppointserviceSaveData($this->data_post);
    }

    /**
     * [UnAppointList 待预约服务列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日16:45:38
     */
    public function UnAppointList(){
        return $this->fetch();
    }

    /**
     * [SaveStatus 执行/取消推荐]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日16:45:38
     */
    public function SaveStatus()
    {
        $params=input();

        return AdminService::ServiceStatusSave($params);
    }



    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='service';
        $params['soft']=true;
        $params['soft_arr']=['is_del'=>1];
        $params['errorcode']=900;
        $params['msg']='删除失败';
        if($params['service_status'] == 1){
            $params['msg']='上架状态不可删除服务';
            throw new \BaseException(['code'=>403 ,'errorCode'=>$params['errorcode'],'msg'=>$params['msg'],'status'=>false,'debug'=>false]);
            return;

        }
        BaseService::DelInfo($params);
        return DataReturn('删除成功', 0);

    }

    /**
     * [SaveInfo 服务添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月9日09:39:00
     */
    public function SaveInfo()
    {

        Session::delete('PrivilegePrizeInfo');
        #关联商品
        Session::delete('bizProInfoSession1');
        #关联配件
        Session::delete('bizProInfoSession2');
        Session::delete("bizProInfoSession");
        #服务折扣
        Session::delete('ServiceDiscountType');
        #服务折扣类型
        Session::delete('DiscountType');
        #关联保养类型
        Session::delete('getJzServiceLogInfo1');
        #关联维修类型
        Session::delete('getJzServiceLogInfo2');
        #关联质保类型
        Session::delete('getJzServiceLogInfo3');
        #关联门店
        Session::delete('getServiceBizId');


        // 参数
        $params = input();
        // 数据
        $data = AdminService::InitServiceInfo()['data'];
        //删除缓存无用图片

        if (!empty($params['id'])) {
            #把服务id  存到缓存里
            Session::set('serviceId',$params['id']);
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'service'
            ];

            $data=AdminService::ServicesList($data_params);


            #查询服务 的关联服务
            $getService = Db::table('service')
                ->field('service_title,id')
                ->where("id ='".$data[0]['service_pid']."'")->find();
           // dump($getService);exit;
            $this->assign('getService',$getService);
            #查询 赠送 服务
            $giveService = AdminService::giveServiceContent($data[0]['id']);
            $this->assign('giveService',$giveService);
            #查询关联类型  1 保养 2 维修 3 质保记录
            $logmaintain = AdminService::logServiceContent($data[0]['id'],1);
            $logrepair = AdminService::logServiceContent($data[0]['id'],2);
            $logwarranty = AdminService::logServiceContent($data[0]['id'],3);

            if(!empty($logmaintain))
                $this->assign('logmaintain',$logmaintain[0]);
            if(!empty($logrepair))
                $this->assign('logrepair',$logrepair[0]);
            if(!empty($logwarranty))
                $this->assign('logwarranty',$logwarranty[0]);

            $biz_id = input('get.biz_id')??0;
            $this->assign('biz_id',$biz_id);
            #查询服务自己价格  大型车 线上  线下
            $car_dprice_online= AdminService::ServicePrice($params['id'],3,1,$biz_id);

            $car_dprice_offline= AdminService::ServicePrice($params['id'],3,2,$biz_id);
           // dump($car_dprice_online);exit;
            if(!empty($car_dprice_online))
                $this->assign('car_dprice_online',$car_dprice_online[0]['discount']);
            if(!empty($car_dprice_offline))
                $this->assign('car_dprice_offline',$car_dprice_offline[0]['discount']);

            #中型车 线上  线下
            $car_zprice_online = AdminService::ServicePrice($params['id'],2,1,$biz_id);
            $car_zprice_offline = AdminService::ServicePrice($params['id'],2,2,$biz_id);

            if(!empty($car_zprice_online))
                $this->assign('car_zprice_online',$car_zprice_online[0]['discount']);
            if(!empty($car_zprice_offline))
                $this->assign('car_zprice_offline',$car_zprice_offline[0]['discount']);

            #小行车 线上  线下
            $car_xprice_online = AdminService::ServicePrice($params['id'],1,1,$biz_id);
            $car_xprice_offline = AdminService::ServicePrice($params['id'],1,2,$biz_id);
            if(!empty($car_xprice_online))
                $this->assign('car_xprice_online',$car_xprice_online[0]['discount']);
            if(!empty($car_xprice_offline))
                $this->assign('car_xprice_offline',$car_xprice_offline[0]['discount']);

            #统计 服务关联门店分数
            $relationServiceNum = AdminService::relationServiceNum($params['id']);
            $this->assign('relationServiceNum',$relationServiceNum);

            //服务分类id 最高级分类id
            $this->assign('service_class_id',$data[0]['class_pid']);
            //服务二级分类id             service_class_id 是服务分类 也是 二级分类id
            $this->assign('second_class_id',$data[0]['service_class_id']);
            $data = empty($data[0]) ? [] : $data[0];

        }
        $params['id']=empty($params['id'])?'':$params['id'];
        //多余图片处理
        ResourceService::$session_suffix='source_upload'.'service'.$params['id'];
        //dump(Session('source_upload'.'service'.$params['id']));exit;
        ResourceService::delUploadCache();

        $where=[
            'where'=>"is_del = 2 and service_class_pid = 0",
        ];

        $data['category']=ServiceCategory::ServiceCategory($where)["data"];




        $this->assign('data', $data);

        return $this->fetch();
    }
    /**
     * 保存服务
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     */
    public function Save(){
        $params=input();

        return  AdminService::SaveInfo($params);

    }
    /**
     * 获取赠送服务
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     */
    function ServiceListStr()
    {

        $data=AdminService::RelevanceServiceModel();
        return DataReturn('ok','0',$data['data']);
    }
    /**
     * [servicePrice 判断服务价格显示页]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function servicePrice(){
        $id = input("get.service_id");
        $biz_id = input("get.biz_id");
        if(!empty($id)){
            #线上价格
            $oline_price = BizService::getServicePrice($id,'1','',$biz_id);
            #门店价格
            $biz_price = BizService::getServicePrice($id,'2','',$biz_id);
            $this->assign('biz_online',$oline_price);
            $this->assign('biz_price',$biz_price);
            return $this->fetch('/biz/servicePrice');


        }

    }

    /**
     * [serviceClass 查询服务二级分类]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function serviceClass(){

        #查询服务分类
        $service_class_id = input("post.service_class_id");
        #获取服务二级分类
        $second_class_id = input("post.second_class_id");
        $str = AdminService::getServicePrice($service_class_id,$second_class_id);
        return (array('status'=>true,'data'=>$str));
    }

    /*
     * 搜索服务
     */
    public function SearchService(){
        $params=input();
        // 条件
        $where = AdminService::GetAdminIndexWhere($params);

        $data_params = array(
            'page'         =>false,
            'where'     => $where,
            'table'     =>'service',
            'field'     =>'service_title,id',
            'n'=>0
        );

        $data=AdminService::ServicesList($data_params);
        //dump($data);exit;
        return DataReturn('获取成功','0',$data);
    }



/************************ 关联服务 start *******************************************/
    /**
     * [activeTypeService   商品 服务 会员]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeTypeService(){
        $info = input("post.");

        if(!empty($info)) {
            if($info['type'] == 'pro'){
                $title = '商品分类';
            }else if($info['type'] == 'service'){
                $title = '服务分类';
            }else if($info['type'] == 'member'){
                $title = '会员名称';
            }
            $mtype = $info['mtype'];
            unset($info['mtype']);
            $data = ActiveService::activeTypeService($info,$mtype);
            return array('status' => true, 'data' => $data, 'title' => $title);

        }

    }
    /**
     * [activeBizproName 获取商品名称  根据二级分类id]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeBizproName(){
        #获取缓存活动id 只有修改时候有
        $privilegeId = Session('privilegeId');
        $params = input("post.");
        $mtype  = input('post.mtype');// relevance_service : 关联服务
        unset($params['mtype']);
        if(!empty($params)){
            if($params['type'] == 'pro'){
                $title = '商品二级分类';
            }

            if($params['type'] == 'pro')
            {
                $data = ActiveService::activeBizproName($params,$mtype);

                if(!empty($data)){
                    return array('status' => true, 'data' => $data, 'title' => $title);

                }else{
                    return array('status' => false, 'msg' => '暂无商品名称');

                }
            }else{

                if($mtype == 'active'){
                    #活动管理
                    #存服务session  显示
                    $data  = ActiveService::activeSession($params['id'],$params['type'],$params['mark'],$privilegeId);
                    return array('status' => true, 'data' => $data);
                }else  if($mtype == 'redPacket'){
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'],$params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true,'bstitle'=>$title,'detail_price'=>$detail_price,'pro_service_id'=>$pro_service_id);
                }else{
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'],$params['type'],$mtype);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true,'bstitle'=>$title,'detail_price'=>$detail_price,'pro_service_id'=>$pro_service_id);
                }





            }
        }

    }
    /**
     * [activeCreateGive 查询 显示 商品二级分类  服务名称  会员卡直接显示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeCreateGive(){
        #获取缓存活动id 只有修改时候有
        $params = input("post.");
        if(!empty($params)){
            if($params['type'] == 'pro'){
                $title = '商品二级分类';
            }else if($params['type'] == 'service'){
                $title = '服务名称';
            }else{
                $title ='';
            }
            $mtype = input("post.mtype");
            unset($params['mtype']);
            $data = ActiveService::activeTypeService($params,$mtype);
            if($params['type'] == 'pro'){
                if(!empty($data)){
                    return array('status' => true, 'data' => $data, 'title' => $title);

                }else{
                    return array('status' => false, 'msg' => '暂无二级分类');

                }
            }else  if($params['type'] == 'service'){
                if(!empty($data)){
                    return array('status' => true, 'data' => $data, 'title' => $title);

                }else{
                    return array('status' => false, 'msg' => '暂无服务名称');

                }
            }
        }

    }
    #整理缓存数据和数据库数据显示在页面
    function serviceSession(){
        $params = input("post.");
        if($params['mark'] =='privilege'){
            #关联服务 不需要获取id
            $data = AdminService::serviceSession($params);

        }


        return array('status' => true, 'data' => $data);


    }
    #赠送服务 存缓存 显示页面
    function giveServiceSession(){
        $params = input("post.");
        #获取服务id 赠送服务 修改时存
        $serviceId = Session('serviceId');

        $data = AdminService::giveServiceSession($params,$serviceId);
        return array('status' => true, 'data' => $data);
    }
    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function giveServiceTypeList(){
        $data = AdminService::giveServiceTypeList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [giveServiceWrite 赠送服务修改信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     */
    function giveServiceWrite(){
        $info = input("post.");
        if(!empty($info)){
            if ($info['mark'] == 'S') {
                # 操作session
                #活动赠送
                $_session = Session('giveJzServiceInfo');
                if (!empty($_session)) {

                    if($info['vmark'] == 2){
                        #充值金额
                        $_session[$info['k']]['giving_number'] = $info['value'];

                    }else if($info['vmark'] == 3){
                        #赠送金额
                        $_session[$info['k']]['work_time'] = $info['value'];

                    }

                    #存session
                    Session::set('giveJzServiceInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }

            }else{
                if(!empty($info['id'])){
                    # 直接操作数据库 service_giving
                    if($info['vmark'] == 2){
                        #充值金额
                        DB::table('service_giving')->where(array('id'=>$info['id']))->update(array('giving_number'=>$info['value']));


                    }else if($info['vmark'] == 3){
                        #赠送金额
                        DB::table('service_giving')->where(array('id'=>$info['id']))->update(array('work_time'=>$info['value']));


                    }


                    return array('status' => true);
                }

            }
        }
    }
    /**
     * [delServiceInfo 关联服务 赠送服务删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function delServiceInfo(){
        $info = input("post.");
        if($info['type'] == 'giveService'){
            #赠送服务
            if($info['mark'] == 'DB'){
                if(!empty($info['id'])){
                    # 直接操作数据库 service_giving
                    DB::table('service_giving')->where(array('id'=>$info['id']))->delete();

                    return array('status' => true);
                }
            }else{
                # 操作session
                Session::delete('giveJzServiceInfo');
                return array('status' => true);
            }
        }else{
            #关联服务
            if($info['mark'] == 'DB'){
                if(!empty($info['id'])){

                    # 直接操作数据库
                    DB::table('service')->where(array('id'=>$info['id']))->update(array('service_pid'=>0));

                    return array('status' => true);
                }
            }else{
                # 操作session
                Session::delete('getJzServiceInfo');
                return array('status' => true);
            }
        }



    }
/************************ 关联服务 end *******************************************/
    /**
     * [ServiceDiscount 服务折扣列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function ServiceDiscount(){
        $serviceId = input("post.serviceId");
        #折扣类型
        $discount_type = input("post.discount_type");

        #先看是否有 服务id  有的话查询 数据库 反之 查询缓存
        if(!empty($serviceId)){
            $_discount_session = Db::table('service_mediscount')->field('member_level level_id,service_id,id,discount,discount_type')->where(array('service_id'=>$serviceId,'discount_type'=>$discount_type))->select();
            if(!empty($_discount_session)){
                session::set('ServiceDiscountType',$_discount_session);
                #获取折扣类型
                $_discount_type = Session('DiscountType');
                if($_discount_type == $discount_type ){
                    #页面传过来的折扣类型 和 缓存中的相等  证明没换类型
                    $_discount_session = Session('ServiceDiscountType');

                }else{
                    #存下折扣类型  用于 下次选择类型 时作比较、
                    session::set('DiscountType',$discount_type);
                    #页面传过来的折扣类型 和 缓存中的不相等  证明换类型了
                    #缓存为空 根据类型查询会员折扣
                    $level_discount = Db::table('level_discount')->field('level_id,type,discount')->where("type = $discount_type")->select();
                    session::set('ServiceDiscountType',$level_discount);
                    $_discount_session = Session('ServiceDiscountType');


                }
                #重新获取折扣缓存
                #获取到的折扣数组 赋值在页面上
                $str = AdminService::ServiceDiscountList($_discount_session,'S',$discount_type,$serviceId);
            }else{
                #获取折扣类型
                $_discount_type = Session('DiscountType');
                if($_discount_type == $discount_type ){
                    #页面传过来的折扣类型 和 缓存中的相等  证明没换类型
                    $_discount_session = Session('ServiceDiscountType');

                }else{
                    #存下折扣类型  用于 下次选择类型 时作比较、
                    session::set('DiscountType',$discount_type);
                    #页面传过来的折扣类型 和 缓存中的不相等  证明换类型了
                    #缓存为空 根据类型查询会员折扣
                    $level_discount = Db::table('level_discount')->field('level_id,type,discount')->where("type = $discount_type")->select();
                    session::set('ServiceDiscountType',$level_discount);
                    #重新获取折扣缓存
                    $_discount_session = Session('ServiceDiscountType');
                }

                #获取到的折扣数组 赋值在页面上
                $str =AdminService::ServiceDiscountList($_discount_session,'S',$discount_type);
            }


        }else{
            #获取折扣类型
            $_discount_type = Session('DiscountType');
            if($_discount_type == $discount_type ){
                #页面传过来的折扣类型 和 缓存中的相等  证明没换类型
                $_discount_session = Session('ServiceDiscountType');

            }else{
                #存下折扣类型  用于 下次选择类型 时作比较、
                session::set('DiscountType',$discount_type);
                #页面传过来的折扣类型 和 缓存中的不相等  证明换类型了
                #缓存为空 根据类型查询会员折扣
                $level_discount = Db::table('level_discount')->field('level_id,type,discount')->where("type = $discount_type")->select();
                session::set('ServiceDiscountType',$level_discount);
                #重新获取折扣缓存
                $_discount_session = Session('ServiceDiscountType');
            }

            #获取到的折扣数组 赋值在页面上
            $str =AdminService::ServiceDiscountList($_discount_session,'S',$discount_type);


        }
        return array('status'=>true,'data'=>$str);



    }
    //服务点击修改
    function ServiceDiscountWrite(){
        /*id  k  value  mark, vamrk*/
        $info = input("post.");

        if(!empty($info)){
            if ($info['mark'] == 'DB') {
                # 直接操作数据库
                $a = Db::table("service_mediscount")->where(array('id'=>$info['id']))->update(array('discount'=>$info['value']));

                return array('status' => true);

            } else {
                # 操作session
                $_session = Session('ServiceDiscountType');
                if (!empty($_session)) {
                    #服务折扣  discount
                    $_session[$info['k']]['discount'] = $info['value'];

                    Session::set('ServiceDiscountType', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }
/************************* 快捷服务操作 start ***********************/
    /*
     * @content : 快捷服务创建
     * */
    function save_quick(){
        if(input("get.action") == 'ajax'){

        }else{
            $data = AdminService::serviceSort();
            $this->assign('service',$data);
            return $this->fetch();

        }

    }
    /*
     * @content : 搜索快捷服务名称
     * */
    function quickService(){
        $serviceName = input("post.value");
        $data = AdminService::quickService($serviceName);
        return array('status'=>true,'data'=>$data);

    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 搜索服务名称
     */
    function searchRelevanceService()
    {
        $_where = '';
        $search_title=input("post.search_title");
        if(!empty($search_title)){
            $_where .= " and service_title like '%".$search_title."%'";
        }
        $list = AdminService::searchServiceName($_where);
        if(!empty($list)){
            $idcon=array_column($list,'id');
            rsort($idcon);
            return array('status'=>true,"param"=>$idcon);
        }else{
            return array("status"=>false);
        }
    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 存 快捷服务
     */
    function getServiceSort(){
        $info = input("post.");
        #位置排序不能重复
        $sort = Db::table('service_quick')->where(array('sort'=>$info['sort']))->find();
        if(empty($sort)){
            $title = Db::table('service_quick')->where(array('sort'=>$info['service_id']))->find();
            if(empty($title)){
                if(!empty($info)){
                    Db::table('service_quick')->insert($info);
                }
                return array('status'=>true);
            }else{
                #不为空 提示
                return array('status'=>false,'msg'=>'服务名称不能重复');
            }
        }else{
            #不为空 提示
            return array('status'=>false,'msg'=>'位置排序不能重复');

        }



    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取快捷服务名称
     */
    function serviceSort(){
        $quick = AdminService::serviceSort();
        $data = AdminService::serviceList($quick);

        if(!empty($data)){
            return array('status'=>true,'data'=>$data);
        }

    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 修改快捷服务排序
     */
    function getServiceQuickWrite(){
        $info = input("post.");
        #位置排序不能重复
        $sort = Db::table('service_quick')->where(array('sort'=>$info['sort']))->find();
        if(empty($sort)){
            Db::table('service_quick')->where(array('id'=>$info['id']))->update($info);
            $quick = AdminService::serviceSort();
            $data = AdminService::serviceList($quick);
            return array('status'=>true,'data'=>$data);
        }else{
            #不为空 提示
            return array('status'=>false,'msg'=>'位置排序不能重复');

        }



    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 删除快捷服务名称
     */
    function delQuickServiceInfo(){
        $id = input("post.id");
        Db::table('service_quick')->where(array('id'=>$id))->delete();
        return array('status'=>true);

    }


/************************* 快捷服务操作 end ***********************/
/************************** 服务 关联 保养记录 维修记录 关联 质保记录 start *****************************************/
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 关联保养记录  关联维修记录  关联质保记录 存到缓存
     */
    #整理缓存数据和数据库数据显示在页面
    function serviceLogSession(){
        $params = input("post.");
        #关联服务 不需要获取id
        $data = AdminService::serviceLogSession($params);



        return array('status' => true, 'data' => $data);


    }
    /**
     * [delServiceLogInfo  关联记录删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function delServiceLogInfo(){
        $info = input("post.");

        if($info['mark'] == 'DB'){
            if(!empty($info['id'])){
                # 直接操作数据库 service_giving
                DB::table('service_log')->where(array('service_id'=>$info['id'],'log_type'=>$info['type']))->delete();

                return array('status' => true);
            }
        }else{
            # 操作session
            Session::delete('getJzServiceLogInfo'.$info['type']);
            return array('status' => true);
        }

    }
    /*
     * @content :关联修改
     * */
    function logServiceWrite(){
        $info = input("post.");
        if(!empty($info)){
            if ($info['mark'] == 'S') {
                # 操作session
                $_session = Session('getJzServiceLogInfo'.$info['type']);

                if (!empty($_session)) {
                    if($info['type'] == 1){
                        #保养记录
                        if($info['vmark'] == 1){
                            #保养名称
                            $_session['log_title'] = $info['value'];

                        }else if($info['vmark'] == 2){
                            #有效期

                            $_session['log_validity'] = $info['value'];

                        }else if($info['vmark'] == 3){
                            #有效期公里数
                            $_session['log_kilometers'] = $info['value'];

                        }
                    }else if($info['type'] == 2){
                        #维修记录
                        if($info['vmark'] == 1){
                            #维修名称
                            $_session['log_title'] = $info['value'];

                        }
                    }else if($info['type'] == 3){
                        #质保记录
                        if($info['vmark'] == 1){
                            #质保时间
                            $_session['log_validity'] = $info['value'];

                        }else if($info['vmark'] == 2){
                            #有效公里数

                            $_session['log_kilometers'] = $info['value'];

                        }else if($info['vmark'] == 3){
                            #质保范围
                            $_session['log_scope'] = $info['value'];

                        }
                    }

                    #存session
                    Session::set('getJzServiceLogInfo'.$info['type'], $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }

            }else{
                if(!empty($info['id'])){
                    # 直接操作数据库 service_log
                    if($info['type'] == 1){
                        #保养记录
                        if($info['vmark'] == 1){
                            #保养名称
                            $a = DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_title'=>$info['value']));

                        }else if($info['vmark'] == 2){
                            #有效期
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_validity'=>$info['value']));


                        }else if($info['vmark'] == 3){
                            #有效期公里数
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_kilometers'=>$info['value']));

                        }
                    }else if($info['type'] == 2){
                        #维修记录
                        if($info['vmark'] == 1){
                            #维修名称
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_title'=>$info['value']));

                        }
                    }else if($info['type'] == 3){
                        #质保记录
                        if($info['vmark'] == 1){
                            #质保时间
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_validity'=>$info['value']));

                        }else if($info['vmark'] == 2){
                            #有效公里数
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_kilometers'=>$info['value']));


                        }else if($info['vmark'] == 3){
                            #质保范围
                            DB::table('service_log')->where(array('id'=>$info['id'],'log_type'=>$info['type']))->update(array('log_scope'=>$info['value']));

                        }
                    }


                    return array('status' => true);
                }

            }
        }
    }


    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function logServiceTypeList(){
        $type = input("post.type");
        $data = AdminService::logServiceTypeList($type);
        return array('status' => true, 'data' => $data);
    }


/************************** 服务 关联 保养记录 维修记录 关联 质保记录 end *****************************************/

    function changeNumber()
    {
        $params = input();
        Db::table("service")->where(array("id"=>$params['id']))->update(array("service_number"=>$params['val']));
        return array("status"=>true);
    }

}
