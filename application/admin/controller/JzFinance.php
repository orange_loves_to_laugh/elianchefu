<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 9:35
 */

namespace app\admin\controller;

use app\api\ApiService\SubsidyService;
use app\service\BaseService;
use app\service\DisburseSrervice;
use app\service\DisburseTypeService;
use app\service\FinanceService;
use think\facade\Request;

/**
 * 财务管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月21日10:37:36
 */
class JzFinance extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }
    /**
     * [DepositIndex 提现记录列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function DepositIndex()
    {
        if (input('get.action')=='ajax') {
            $params = input();
//            dump(Request::post('param'));exit;
            // 条件
            $where =  FinanceService::DepositListWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'log_deposit'
            );
            $data=FinanceService::DepositList($data_params);

            $total = BaseService::DataTotal('log_deposit',$where);

            $total_price = FinanceService::TypeUnDepositTotalPrice();

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data,'price'=>$total_price];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [DepositDetail 提现记录详情]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function DepositDetail(){
         // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'log_deposit'
            ];

            $ret = FinanceService::DepositList($data_params);

            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }


    /**
     * [ExpensesIndex 费用列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpensesIndex(){
        $params = input();
        if (input('get.action')=='ajax') {

            // 条件
            $where =  FinanceService::ExpensesListWhere($params["param"]);
            if(isset($params['category'])&&intval($params['category'])>0)
            {
                $where[] = ['category', '=', $params['category']];
            }
            //类型
            if(isset($params['type_id'])&&intval($params['type_id'])>0)
            {
                $where[] = ['type_id', '=', $params['type_id']];
            }
            if(isset($params['switchStatic'])){
                $whereTime = "id > 0";
                if($params['switchStatic']<5){
                    switch ($params['switchStatic']){
                        case 1 :
                            $whereTime .= " and date(create_time)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $whereTime .= " and date(create_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $whereTime .= " and date_format(create_time,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $whereTime .= " and date_format(create_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($params['start_time'])){
                        $whereTime.=" and date(create_time)>='".$params['start_time']."'";
                    }
                    if(!empty($params['end_time'])){
                        $whereTime.=" and date(create_time)<='".$params['end_time']."'";
                    }
                }
            }
            //dump($where);exit;
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                "wheres" => $whereTime,
                'table'=>'expenses'
            );

            $data=FinanceService::ExpensesList($data_params);

            $total = BaseService::DataTotal('expenses',$where);


            // return DataReturn('获取成功','0',$data);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $data_params = array(
                'page'         => false,
                'where'     => [['id','>',0]],
                'table'=>'costtype'
            );
            $typelist= DisburseTypeService::TypeList($data_params);
            $this->assign('typelist',$typelist);
            $this->assign("params",$params);
            return $this->fetch();
        }
    }
    /**
     * [SaveExpenses 添加/编辑费用页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function SaveExpenses(){
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'expenses'
            ];

            $ret = FinanceService::ExpensesList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }

        $data_params = array(
            'page'         => false,
            'where'     => [['id','>',0],['system_status','=',1]],
            'table'=>'costtype'
        );
        $typelist= DisburseTypeService::TypeList($data_params);
        $this->assign('typelist',$typelist);
        $this->assign('category',lang('fee_type'));
        $this->assign('cash_type',lang('cash_type'));
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [SaveExpenses 添加/编辑费用页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DoSaveExpenses(){
// 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');
        return FinanceService::ExpensesSave($params);
    }

    /**
     * [删除费用]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelExpenses(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='expenses';
        $params['soft']=false;
        $params['errorcode']=900;
        $params['msg']='删除失败';
        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);

    }

    /**
     * [ExpensesTypeIndex 费用类型列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月20日16:36:37
     * @desc    description
     */
    public function ExpensesTypeIndex(){

        if (input('get.action')=='ajax') {
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => [['id','>',0],['system_status','=',1]],
                'table'=>'costtype'
            );
            $data = DisburseTypeService::TypeList($data_params);

            $total = BaseService::DataTotal('costtype',[['id','>',0]]);

            // return DataReturn('获取成功','0',$data);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [SaveType 费用类型添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月20日16:36:37
     */
    public function SaveType(){
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'costtype'
            ];

            $ret = DisburseTypeService::TypeList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }
    /**
     * [DoSaveType 费用类型添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月20日16:36:37
     */
    public function DoSaveType()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return DisburseTypeService::TypeSave($params);
    }


    /**
     * [DelType 删除费用类型]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelType(){
        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        DisburseTypeService::DelType($params);


        return DataReturn('删除成功', 0);
    }
    /**
     * [DelType 待提现金额]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月29日18:53:07
     */
    public function UnDeposit()
    {
        if (input('get.action')=='ajax') {


            $data = FinanceService::TypeUnDepositList();

            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    function deposit_static()
    {
        if(input("get.action")=='ajax'){
            $params = input("post.");
            $list =FinanceService::depositStatic($params);
            $data = array(
                array("username"=>"提现成功总金额","price"=>getsPriceFormat($list['all']),"type"=>0),
                array("username"=>"推广员提现成功总金额","price"=>getsPriceFormat($list['partner']),"type"=>1),
                array("username"=>"合作店提现成功总金额","price"=>getsPriceFormat($list['biz']),"type"=>2),
                array("username"=>"商家提现成功总金额","price"=>getsPriceFormat($list['merchant']),"type"=>3),
            );
            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{
            return view();
        }
    }

    function DepositStaticDetail()
    {
        $param = input("get.");
        if(input("get.action")=='ajax'){
            $search = input("post.");
            if(!empty($search['param']['search'])){
                $param['search'] = $search['param']['search'];
            }
            $data = FinanceService::depositStaticDetail($param);
            return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
        }else{
            $this->assign("type",$param['type']); // 等待提现 或 2 提现成功
            $this->assign("search_type",$param['search_type']);// 身份类型
            $this->assign("switchStatic",$param['switchStatic']);// 时间类型
            $this->assign("start_time",$param['start_time']);   // 搜索开始时间
            $this->assign("end_time",$param['end_time']);       //  结束时间
            return view();

        }
    }

}