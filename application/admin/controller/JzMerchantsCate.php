<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\MerchantService;
use app\service\ResourceService;
use think\Db;

/**
 * 商户分类管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月12日09:49:38
 */
class JzMerchantsCate extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 分类列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function Index()
    {
        if (input('get.action')=='ajax') {
            $_where = 'status=1 and type = 2';

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'table'     =>'merchants_cate',
                'where'     =>$_where,
                'order'     =>'id asc'

            );
            $data = BaseService::DataList($data_params);


            $data=MerchantService::CateDataDealWith($data);

            $total = BaseService::DataTotal('merchants_cate',$_where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [DataList 分类列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月12日09:49:38
     */
    public function DataList(){
        $data_params = array(
            'where'        =>[['pid','=',input('get.pid')]],
            'page'         => false,
            'table'     =>'merchants_cate',
            'order'     =>'sort_num desc'

        );

        $data = BaseService::DataList($data_params);
        $data = MerchantService::getMerchantsCateClass($data);


        return DataReturn('ok',0,$data);
    }

    /**
     * [SaveData 添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [];
        $table='merchants_cate';
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table' =>$table
            ];
            $ret = BaseService::DataList($data_params);
            $ret=MerchantService::CateDataDealWith($ret);
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        //多余图片处理
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.$table.$params['id'];
        ResourceService::delUploadCache();
        $cates=Db::name($table)->where('pid=0 and type=2 and status= 1')->select();

        $this->assign('cates', $cates);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function Save()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        // 开始操作
        $params = input('post.');

        return MerchantService::CateSave($params);

    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;

        return MerchantService::DelCate($params);

    }
}