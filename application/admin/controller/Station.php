<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\EmployeeService;
use think\Db;


/**
 * 岗位管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月16日11:33:06
 */
class Station extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月16日11:33:06
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();
    }

    /**
     * [Index 岗位列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {

        if (input('get.action')=='ajax') {

            $where=EmployeeService::StationListWhere(input());
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'=>'employee_station',

            );
            $data = EmployeeService::StationDataList($data_params);

            $total = BaseService::DataTotal('employee_station',[['is_del','=',2]]);


            // return DataReturn('获取成功','0',$data);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{

            $this->assign('department_arr',Db::name('employee_department')->where('is_del = 2 and higher_department_id=0')->field('id,title')->select());
            return $this->fetch();
        }

    }
    /**
     * [SaveInfo 岗位添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日13:48:10
     */
    public function SaveData(){

        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'employee_station',
            ];

            $ret = EmployeeService::StationDataList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        $this->assign('department_arr',Db::name('employee_department')->where('is_del = 2 and higher_department_id=0')->field('id,title')->select());
        $this->assign('yes_no_status',lang('yes_no_status'));
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function Save()
    {

        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return EmployeeService::StationDataSave($params);


    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='employee_station';
        $params['soft']=true;
        $params['soft_arr']=['is_del'=>1];
        $params['errorcode']=19001;
        $params['msg']='删除失败';
        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);

    }
    /**
     * [获取岗位列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DataList(){
        $where=EmployeeService::StationListWhere(input());
        $data_params = array(
            'page'         => false,
            'where'     => $where,
            'table'=>'employee_station',

        );
        $data = EmployeeService::StationDataList($data_params);
        return DataReturn('ok',0,$data);
    }
}