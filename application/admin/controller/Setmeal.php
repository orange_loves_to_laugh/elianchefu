<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 18:56
 */

namespace app\admin\controller;

//套餐管理
use app\reuse\controller\ResponseJson;
use app\service\ActiveService;
use app\service\AdminService;
use app\service\AdService;
use app\service\ApplayService;
use app\service\BaseService;
use app\service\InforService;
use app\service\PacketService;
use app\service\ResourceService;
use app\service\ServiceCategory;
use app\service\SetmealService;
use app\service\TaskService;
use Redis\Redis;
use think\Db;
use think\facade\Session;

class Setmeal extends Common
{
    use ResponseJson;

    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();
    }

    /**
     * [index 套餐管理 会员卡列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function index()
    {
        if (input("get.action") == 'ajax') {
            $_where = 'ml.id > 1 and ml.id <= 5';
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );

            $data = SetmealService::SetmealIndex($data_params);
            $total = SetmealService::SetmealIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];


        } else {
            return $this->fetch();
        }

    }

    /**
     * [save 套餐管理详情  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function save()
    {

        $levelId = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = input("post.");

            $data = SetmealService::SetmealSave($params);
            return $data;

        } else {
            if (!empty($levelId)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('ml.id' => $levelId),//查找条件
                );
                $data = SetmealService::SetmealIndex($data_params);
                $level_privilege = explode(',', $data[0]['level_privilege']);
                # 查询会员所有特权
                $privilege = Db::table("privilege")->field(array("title", "id"))->where(array('status' => 1))->select();
                #查询会员卡对应折扣
                $member_discount = SetmealService::SetmealDiscount($levelId);

                #缓存存活动id
                Session::set('levelId', $levelId);
                $this->assign('member_discount', $member_discount);
                $this->assign('level', $data[0]);
                $this->assign("level_privilege", $level_privilege);
                $this->assign("privilege", $privilege);
                Session::delete("recommendPresent");

            }


            return $this->fetch();
        }

    }

    /**
     * [getSetmealList 修改页套餐展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function getSetmealList()
    {
        $level_id = input("post.level_id");
        $data = SetmealService::getSetmealList($level_id);
        return $data;
    }

    /**
     * [getSetmealTitle 修改页套餐名称创建]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function getSetmealTitle()
    {
        $info = input("post.");
        if (!empty($info)) {
            $id = Db::table('setmeal')->insertGetId($info);
            return DataReturn('ok', '0', $id);
        }

    }


    function add_voucher()
    {
        if (input("get.action") == 'ajax') {
            $param = input("post.");
            if ($param['active_types'] == 7) {
                # 办理会员套餐-赠送汽车服务类卡券(通用券/分类券)
                $param['intbal'] = $param['price'];
                $param['giving_number'] = $param['num'];
                $param['card_time'] = $param['validity_day'];
                $param['prize_type'] = 'addVoucher';
                $data = SetmealService::setmealSession($param, $param['days_id']);
            } elseif ($param['active_types'] == 8) {
                # 会员升级赠送-汽车服务类卡券(通用券/分类券)
                $param['intbal'] = $param['price'];
                $param['giving_number'] = $param['num'];
                $param['card_time'] = $param['validity_day'];
                $param['prize_type'] = 'addVoucher';
                $data = SetmealService::upgradeSession($param, $param['days_id']);
            } elseif ($param['active_types'] == 6) {

                $param['exchange_title'] = $param['title'];
                $param['exchange_num'] = $param['num'];
                $param['exchange_indate'] = $param['validity_day'];
                $param['exchange_type'] = $param['is_type'];
                Session::set('integralExchangeInfo', array($param));

            }
            return array("status" => true, "data" => $data);
        } else {
            # 营销活动的类型
            $active_type = input("get.active_types");
            $this->assign('active_types', $active_type);
            if ($active_type == 3 or $active_type == 7 or $active_type == 8) {
                $this->assign('days_id', input("get.days_id"));
            }
            # 消费券的类型 可以为NULL
            $type = input("get.type");
            $this->assign("type", $type);
            # 服务分类
            $where = [
                'where' => "is_del = 2 and service_class_pid = 0",
            ];
            $category = ServiceCategory::ServiceCategory($where)["data"];
            $this->assign("category", $category);
            return $this->fetch();
        }
    }

    /**
     * [checkSetMeal 显示套餐详情]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function checkSetMeal()
    {
        $setmeal_id = input("post.setmeal_id");
        Session::set('setMealId', $setmeal_id);
        $data = SetmealService::checkSetMeal($setmeal_id);
        return $data;
    }

    /**
     * [upgradeWrite   会员赠送直接添加到数据库]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function setmealWrite()
    {
        $params = input('post.');

        $setMeal_id = $params['id'];
        if (empty($setMeal_id))
            $setMeal_id = session('setMealId');
        unset($params['id']);
        $data = SetmealService::setmealSession($params, $setMeal_id);
        #删除 会员升级id
        Session::delete('setMealId');
        return array('status' => true, 'data' => $data, 'setMeal_id' => $setMeal_id);

    }

    /**
     * [setmealTypeWrite 修改数量]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   1pro:商品  52service ： 服务    3 integral: 积分   4 balance余额
     * @params val,   修改之前 的值
     */
    function setmealTypeWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {

                    # 直接操作数据库 有效期
                    Db::table("level_giving")->where(array('id' => $info['id']))->update(array('giving_number' => $info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('UpgradePrizeInfo');
                if (!empty($_session)) {
                    if ($info['vmark'] == 4) {
                        #有效期
                        $_session[$info['k']]['giving_number'] = $info['value'];

                    }
                    #存session
                    Session::set('UpgradePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /*
     * @content : 删除 套餐中的赠送服务
     * */
    function SetmealTypeDel()
    {
        $id = input("post.id");
        $giving = Db::table('level_giving')->where(array('id' => $id))->find();
        Db::startTrans();
        if ($giving['giving_type'] == 3) {
            $re = Db::table('redpacket')->where(array('id' => $giving['giving_id']))->delete();
            if (!$re) {
                Db::rollback();
                throw new \BaseException(['code' => 403, 'errorCode' => 21004, 'msg' => '删除套餐红包失败', 'status' => false, 'data' => null]);

            }
            $re = Db::table('redpacket_detail')->where(array('redpacket_id' => $giving['giving_id']))->delete();
            if (!$re) {
                Db::rollback();
                throw new \BaseException(['code' => 403, 'errorCode' => 21005, 'msg' => '删除套餐红包卡券失败', 'status' => false, 'data' => null]);

            }
        }
        $re = Db::table('level_giving')->where(array('id' => $id))->delete();
        if (!$re) {
            Db::rollback();
            throw new \BaseException(['code' => 403, 'errorCode' => 21006, 'msg' => '删除套餐赠送失败', 'status' => false, 'data' => null]);

        }
        Db::commit();
        return array('status' => true);

    }

    /*
     * @content 删除 套餐 和详情
     * */
    function SetmealDel()
    {
        $setMeal_id = input("post.setMeal_id");
        Db::table('setmeal')->where(array('id' => $setMeal_id))->delete();
        $giving_arr = Db::table('level_giving')->where(array('setmeal_id' => $setMeal_id))->select();
        Db::startTrans();
        foreach ($giving_arr as $v) {

            if ($v['giving_type'] == 3) {

                $re = Db::table('redpacket')->where(array('id' => $v['giving_id']))->delete();
                if (!$re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 21004, 'msg' => '删除套餐红包失败', 'status' => false, 'data' => null]);

                }
                $re = Db::table('redpacket_detail')->where(array('redpacket_id' => $v['giving_id']))->delete();
                if (!$re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 21005, 'msg' => '删除套餐红包卡券失败', 'status' => false, 'data' => null]);

                }
            }
        }
        $re = Db::table('level_giving')->where(array('setmeal_id' => $setMeal_id))->delete();
        if (!$re) {
            Db::rollback();
            throw new \BaseException(['code' => 403, 'errorCode' => 21006, 'msg' => '删除套餐赠送失败', 'status' => false, 'data' => null]);

        }

        Db::commit();
        return array('status' => true);

    }

    /**
     * [voucherSession 红包卡券信息存入缓存]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    function voucherSession()
    {
        $params = input("post.");

        $_str = TaskService::voucherSession($params);
        return array('status' => true, 'data' => $_str);

    }

    /**
     * [voucherDelInfo 删除红包卡券]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    function voucherDelInfo()
    {
        $info = input("post.");

        if ($info['mark'] == 'DB') {

            #修改红包详情的状态
            Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('status' => '2'));
            return array('status' => true);

        } else {

            # 操作session
            $_session = \session('setmeal');
            // Session::set('setmeal', null);
            //

            //

            //exit;
            if (isset($info['k'])) {

                if (!empty($_session)) {
                    if (count($_session) > 1) {

                        foreach ($_session as $k => $v) {

                            if ($info['k'] == $k) {
                                unset($_session[$k]);
                            }
                        }
                        Session::set('setmeal', $_session);
                    } else {
                        Session::set('setmeal', null);
                    }


                    // dump( \session('setmeal')); exit;
                    #赠送


                    return array('status' => true, 'data' => Session::get('setmeal'));
                } else {
                    return array('status' => false);
                }
            }
        }


    }

    /**
     * [createpacketvoucher 创建红包]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    function createpacketvoucher()
    {


        $param = input();

        Db::startTrans();

        $data = [
            'price' => $param['price'],
            'create_time' => TIMESTAMP,
            'packet_title' => $param['packet_title'],
            'type' => 4,
            'ground_num' => $param['ground_num'],
        ];
        // dump($data);exit;
        $redpacket_id = Db::name('redpacket')->insertGetId($data);
        if (!$redpacket_id) {
            Db::rollback();
            throw new \BaseException(['code' => 403, 'errorCode' => 21001, 'msg' => '创建套餐红包失败', 'status' => false, 'data' => null]);
        }

        $red_dedtail = session('setmeal');
        foreach ($red_dedtail as &$v) {
            if (isset($v['is_type'])) {
                $giving_id = Db::table("merchants_voucher")->insertGetId(array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                    "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']));

                unset($v['title'], $v['price'], $v['min_consume'], $v['num'], $v['validity_day'], $v['is_type'], $v['cate_pid'], $v['active_types']);
                $v['voucher_id'] = $giving_id;
                ksort($v);
            }
            $v['redpacket_id'] = $redpacket_id;
            $v['voucher_over'] = 365;
        }

        $redpacket_detail = Db::name('redpacket_detail')->insertAll($red_dedtail);

        if (!$redpacket_detail) {
            Db::rollback();
            throw new \BaseException(['code' => 403, 'errorCode' => 21002, 'msg' => '创建套餐红包卡券失败', 'status' => false, 'data' => null]);
        }
        //dump(123);exit;
        $data = [
            'giving_type' => 3,
            'giving_id' => $redpacket_id,
            'level_id' => $param['level_id'],
            'setmeal_id' => $param['setmeal_id'],

            'intbal' => $param['price'],
            'giving_number' => $param['ground_num'],

        ];
        $giving_id = Db::name('level_giving')->insertGetId($data);
        if (!$giving_id) {
            Db::rollback();
            throw new \BaseException(['code' => 403, 'errorCode' => 21003, 'msg' => '创建赠送套餐失败', 'status' => false, 'data' => null]);
        }
        Db::commit();
        session('setmeal', null);


        return DataReturn('ok', 0, ['setmeal_id' => $param['setmeal_id']]);
    }

    /**
     * [voucherWrite 修改红包]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    function voucherWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            $_session = Session('setmeal');

            # 操作session
            $_str = '';

            if ($info['mark'] == 'DB') {
                #修改数据库
                if ($info['vmark'] == 5) {
                    Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('voucher_over' => $info['val']));

                } else if ($info['vmark'] == 6) {
                    Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('voucher_number' => $info['val']));

                }

            } else {
                if (!empty($_session)) {
                    if ($info['vmark'] == 5) {
                        $_session[$info['k']]['voucher_over'] = $info['val'];

                    } else if ($info['vmark'] == 6) {
                        $_session[$info['k']]['voucher_number'] = $info['val'];

                    }


                }
            }
            $packetId = Session('packetId');
            if (!empty($packetId)) {
                $arr = PacketService::getPacketContent($packetId);

            }
            $_str .= TaskService::getVoucherArrInfo($arr, 'DB');

            Session::set('setmeal', $_session);

            $_str .= TaskService::getVoucherArrInfo($_session, 'S');

            return array('status' => true, 'data' => $_str);
        }
    }

    /****************************************************************************会员特权 start ****************************************************************************/

    /**
     * [privilege_index 套餐管理 套餐特权列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function privilege_index()
    {
        if (input("get.action") == 'ajax') {
            $_where = 'status= 1';   # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );

            $data = SetmealService::PrivilegeIndex($data_params);
            return $this->jsonSuccessData($data);
        } else {
            return $this->fetch();
        }

    }

    /**
     * [privilege_save 套餐特权 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function privilege_save()
    {
        $privilegeId = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = array_filter(input("post."));
            $data = SetmealService::PrivilegeSave($params);
            return $data;

        } else {
            if (!empty($privilegeId)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('status' => 1, 'id' => $privilegeId),//查找条件
                );
                $data = SetmealService::PrivilegeIndex($data_params);
                $data[0]['biztypemark'] = explode(',', $data[0]['biztypemark']);
                $data[0]['biz1'] = $data[0]['biztypemark']['0'];
                $data[0]['biz2'] = $data[0]['biztypemark']['1'];
                $data[0]['biz3'] = $data[0]['biztypemark']['2'];


                #缓存存活动id
                Session::set('privilegeId', $privilegeId);
                $this->assign('level', $data[0]);
                #获取赠送的服务
                $privilegeType = SetmealService::getTaskContent($privilegeId);
                $this->assign('privilegeType', $privilegeType);


            }


            return $this->fetch();
        }

    }

    /**
     * [Delinfo 特权 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     */
    function DelInfo()
    {
        $id = input("post.id");
        $mark = input("post.mark");
        if ($mark == 'privilege') {
            #会员特权
            if (!empty($id)) {
                #删除 假删
                Db::table('privilege')->where(array('id' => $id))->update(array('status' => 2));
                return array('status' => true);
            }

        } else {
            #活动
            if (!empty($id)) {
                #删除 假删
                Db::table('active')->where(array('id' => $id))->update(array('active_putstatus' => 4));
                return array('status' => true);
            }
        }

    }

    /**
     * [activeTypeService   商品 服务 会员]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * Content:$mtype  task  任务管理 ()
     */
    function activeTypeService()
    {
        $info = input("post.");
        if (isset($info['days_id'])) {
            #签到天数的id
            session::set('daysId', $info['days_id']);
        }
        if (!empty($info)) {
            if ($info['type'] == 'pro') {
                $title = '商品分类';
            } else if ($info['type'] == 'service') {
                $title = '服务分类';
            } else if ($info['type'] == 'member') {
                $title = '会员名称';
            }
            $mtype = $info['mtype'];
            unset($info['mtype']);
            $data = ActiveService::activeTypeService($info, $mtype);
            return array('status' => true, 'data' => $data, 'title' => $title);

        }

    }

    /**
     * [activeBizproName 获取商品名称  根据二级分类id]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeBizproName()
    {
        #获取缓存活动id 只有修改时候有
        $privilegeId = Session('privilegeId');
        $params = input("post.");
        $mtype = input('post.mtype');// task 任务  active 活动 红包 ：redpacket ，sign  签到
        unset($params['mtype']);

        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $classtype = input("post.classtype");
                if (!empty($classtype) and $classtype == 2) {

                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                } else {
                    $title = '商品二级分类';
                    $data = ActiveService::activeBizproName($params, $mtype);
                }


                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无商品名称');

                }
            } else {

                if ($mtype == 'active') {
                    #活动管理
                    #存服务session  显示
                    $data = ActiveService::activeSession($params['id'], $params['type'], $params['mark'], $privilegeId);
                    return array('status' => true, 'data' => $data);
                } else if ($mtype == 'redPacket') {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                } else {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);

                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                }


            }
        }

    }

    /**
     * [activeCreateGive 查询 显示 商品二级分类  服务名称  会员卡直接显示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeCreateGive()
    {
        #获取缓存活动id 只有修改时候有
        $params = input("post.");
        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $title = '商品名称';
            } else if ($params['type'] == 'service') {
                $title = '服务名称';
            } else {
                $title = '';
            }
            $mtype = input("post.mtype");
            unset($params['mtype']);

            $data = ActiveService::activeTypeService($params, $mtype);
            if ($params['type'] == 'pro') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无二级分类');

                }
            } else if ($params['type'] == 'service') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无服务名称');

                }
            }
        }

    }

    /**
     * [searchservice 搜索服务列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月10日17:23:51
     */
    public function searchservice()
    {
        $where = AdminService::GetAdminIndexWhere(input());

        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $where,
            'table' => 'service',
            'field' => 'id,service_title,class_pid',
            'order' => 'service_create desc'

        );
        $data = BaseService::DataList($data_params);

        return DataReturn('ok', 0, $data);
    }

    #整理缓存数据和数据库数据显示在页面
    function privilegeSession()
    {
        #获取缓存活动id 只有修改时候有
        $privilegeId = Session('privilegeId');
        $params = input("post.");

        $data = SetmealService::privilegeSession($params, $privilegeId);
        return array('status' => true, 'data' => $data);


    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getPrivilegeTypeList()
    {
        $data = SetmealService::getPrivilegeTypeList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [delPrivilegeInfo 服务删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function delPrivilegeInfo()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                # 直接操作数据库
                DB::table('relevance_service')->where(array('id' => $info['id']))->delete();

                return array('status' => true);
            }
        } else {
            # 操作session
            #赠送
            $_session = Session('PrivilegePrizeInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('PrivilegePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }
    /****************************************************************************会员特权 end ****************************************************************************/
    /****************************************************************************会员升级 start ****************************************************************************/

    /**
     * [upgrade_index 套餐管理 会员升级列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function upgrade_index()
    {
        if (input("get.action") == 'ajax') {
            $params = input();
            $_where = [['id', '>', 0]];

            //升级后
            if (!empty($params['param']['up_level'])) {
                $_where[] = ['up_level', '=', $params["param"]['up_level']];
            }
            //当前等级
            if (!empty($params['param']['now_level'])) {
                $_where[] = ['now_level', '=', $params["param"]['now_level']];
            }
            $_where[] = ['now_level', '=', 1];
            $_where[] = ['up_level', '=', 2];
            // dump($_where);exit;
            // 获取列表
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );

            $data = SetmealService::UpgradeIndex($data_params);
            $total = SetmealService::UpgradeIndexCount($_where);
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            $this->assign('member_level', Db::name('member_level')->field('id,level_title')->select());
            return $this->fetch();
        }

    }

    /**
     * [privilege_save 套餐特权 会员升级 添加修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function upgrade_save()
    {
        $privilegeId = input("get.id");
        if (input('get.action') == 'ajax') {
            $params = array_filter(input("post."));
            $data = SetmealService::UpgradeSave($params);
            return $data;

        } else {
            Session::delete('UpgradePrizeInfo');
            #查询会员等级
            $member_level = Db::table('member_level')->field('id,level_title')->select();
            $this->assign('level', $member_level);

            return $this->fetch();
        }

    }

    /**
     * @access public
     * @content 选择需要升级到的会员等级
     */
    function changeLevel()
    {
        $now_level = Input("post.now_level");
        if (!empty($now_level) and $now_level != 0) {
            # 查询升级表当前等级是否存在
            $upgrade = Db::table("upgrade")->field(array("up_level"))->where(array("now_level" => $now_level))->select();
            $_array = array();


            if (!empty($upgrade)) {
//                if($now_level == 1 ){
//                    #初级验卡
//                    $up_level = 2;
//                    array_push($_array,$up_level);
//
//                }
                #判断当是初级体验卡  把体验卡删除
                foreach ($upgrade as $k => $v) {
                    array_push($_array, $v['up_level']);
                }
                $upgrade_coll = implode(',', $_array);
            } else {
                $upgrade_coll = 0;
//                if($now_level == 1 ){
//                    #初级验卡
//                    $up_level = 2;
//                    array_push($_array,$up_level);
//                    $upgrade_coll = implode(',', $_array);
//                }
            }
            # 通过会员等级查询升级的等级
            $up_level = DB::query("select id,level_title from member_level 
                        where id > " . $now_level . " and id not in (" . $upgrade_coll . ")");
            $str = null;
            if (!empty($up_level)) {
                foreach ($up_level as $v) {
                    $str .= " <option value=\"{$v['id']}\">{$v['level_title']}</option>";
                }
            } else {
                $str .= " <option value=\"0\">当前等级已设置成功</option>";
            }
            echo($str);
        } else {
            echo(" <option value=\"\">请选择会员等级</option>");
        }
    }

    /*
     * @content 修改升级价格
     * */
    function upgrade_price()
    {
        $price = input("post.price");
        $id = input("post.id");
        if (!empty($id)) {
            Db::table('upgrade')->where(array('id' => $id))->update(array('price' => $price));
            return array('status' => true);
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function upgradeSession()
    {
        #获取缓存活动id 只有修改时候有
        $taskId = Session('taskId');
        $params = input("post.");

        $data = SetmealService::upgradeSession($params, $taskId);
        return array('status' => true, 'data' => $data);


    }

    /**
     * [bizproService 查询商品  服务 名称]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id, 商品id 或服务id
     * @params type,pro 商品 或 service服务
     */
    function bizproService()
    {
        #mark  为give 说明这个奖项是可以 赠送积分商品的
        $params = input("post.");
        $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
        $title = $bstitle['title'];
        $detail_price = $bstitle['detail_price'];
        $pro_service_id = $bstitle['detail_id'];

        return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
    }

    #修改 删除 之后 显示的页面（赠送）
    function getUpgradeList()
    {
        $data = SetmealService::getUpgradeList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [upgradeTypeWrite 修改数量]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   1pro:商品  52service ： 服务    3 integral: 积分   4 balance余额
     * @params val,   修改之前 的值
     */
    function upgradeTypeWrite()
    {
        $info = input("post.");
        $upgrade_id = session('upgradeId');
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {

                    # 直接操作数据库 有效期
                    Db::table("upgrade_giving")->where(array('id' => $info['id']))->update(array('giving_number' => $info['value']));
                }

                return array('status' => true, 'upgrade_id' => $upgrade_id);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('UpgradePrizeInfo');
                if (!empty($_session)) {
                    if ($info['vmark'] == 4) {
                        #有效期
                        $_session[$info['k']]['giving_number'] = $info['value'];

                    }
                    #存session
                    Session::set('UpgradePrizeInfo', $_session);

                    return array('status' => true, 'upgrade_id' => $upgrade_id);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [UpgradeTypeDel 活动  创建商品  赠送 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function UpgradeTypeDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {

                #赠送
                # 直接操作数据库
                DB::table('upgrade_giving')->where(array('id' => $info['id']))->delete();

                return array('status' => true);
            }
        } else {
            # 操作session
            #赠送
            $_session = Session('UpgradePrizeInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('UpgradePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }


    /**
     * [ServiceRelation   查看会员赠送]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function ServiceRelation()
    {
        #先删除 在存新的
        Session::delete('upgradeId');
        $_upgradeId = input("post.upgradeId");
        session::set('upgradeId', $_upgradeId);

        $_str = '';
        if (!empty($_upgradeId)) {
            $pro = SetmealService::getUpgradeContent($_upgradeId);
            $_str = SetmealService::getUpgradeInfo($pro, 'DB', $_upgradeId);
        }
        return array('status' => true, 'data' => $_str);

    }

    /**
     * [ServiceRelationList   修改后展示的页面]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function ServiceRelationList()
    {
        #先删除 在存新的
        Session::delete('upgradeId');
        $_upgradeId = input("post.upgradeId");
        session::set('upgradeId', $_upgradeId);

        $_str = '';
        if (!empty($_upgradeId)) {
            $pro = SetmealService::getUpgradeContent($_upgradeId);

            $_str = SetmealService::getUpgradeInfoWrite($pro, 'DB', $_upgradeId);
        }
        return array('status' => true, 'data' => $_str);

    }

    /**
     * [upgradeWrite   会员赠送直接添加到数据库]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function upgradeWrite()
    {
        $params = input('post.');
        $upgradeId = $params['id'];
        if (empty($upgradeId))
            $upgradeId = session('upgradeId');
        unset($params['id']);
        $data = SetmealService::upgradeSession($params, $upgradeId);
        #删除 会员升级id
        Session::delete('upgradeId');
        return array('status' => true, 'data' => $data, 'upgradeId' => $upgradeId);

    }

    /**
     * [LevelList 会员等级列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
    public function LevelList()
    {
        $data = Db::name('member_level')->where('id > ' . input('id'))->select();
        return DataReturn('ok', 0, $data);

    }
    /****************************************************************************会员升级 end ****************************************************************************/

    /****************************************************************************会员充值 start ****************************************************************************/
    /**
     * @access public
     * @content 会员充值列表
     */
    function recharge_index1()
    {
        # 清空Session中的充值信息
        Session::delete('RechargePrizeInfo');
        # 查询会员充值列表中的会员等级
        $_level = Db::query("select ml.id level_id,r.level,ml.level_title from recharge r 
                                 left join member_level ml on ml.id=r.level group by r.level order by r.level asc");

        $level = Input('get.search');
        if (!empty($level)) {
            $_where = " where r.level = " . $level;
        } else {
            $_where = '';
        }
        # 查询会员充值列表
        $_info = Db::query("select r.id,r.price,r.giving,r.level from recharge r " . $_where . " order by r.level,r.price asc");
        $this->assign('search', $level);
        //dump($_info);exit;
        $_str = '';
        if (!empty($_level)) {
            foreach ($_level as $k => $v) {
                if (!empty($level)) {
                    if ($level == $v['level'])
                        $_str .= " <div class=\"layui-card-body \">
                         <h2 style=\"padding: 5px;margin - left: 0;margin - bottom: 10px;font - weight: bold;text-align: center\">{$v['level_title']}</h2>
                        <table class=\"layui-table layui-form\">
                            <thead>
                            <tr>
                                <th style='text-align: center'>充值金额(元)</th>
                                <th style='text-align: center'>赠送金额(元)</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>";

                } else {
                    $_str .= " <div class=\"layui-card-body \">
                         <h2 style=\"padding: 5px;margin - left: 0;margin - bottom: 10px;font - weight: bold;text-align: center\">{$v['level_title']}</h2>
                        <table class=\"layui-table layui-form\">
                            <thead>
                            <tr>
                                <th style='text-align: center'>充值金额(元)</th>
                                <th style='text-align: center'>赠送金额(元)</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>";
                }
                foreach ($_info as $kk => $vv) {
                    if ($v['level'] == $vv['level']) {
                        $_str .= "<tr  id=\"info{$vv['id']}\">
                                <th style=\"cursor: pointer;text-align: center\" id=\"price{$vv['id']}\" ondblclick='changePrices(\"{$vv['id']}\",\"{$vv['price']}\",\"price\")'>" . getsPriceFormat($vv['price']) . "</th>
                                <th style=\"cursor: pointer;text-align: center\" id=\"giving{$vv['id']}\" ondblclick='changePrices(\"{$vv['id']}\",\"{$vv['giving']}\",\"giving\")'>" . getsPriceFormat($vv['giving']) . "</th>
                                <th><a href=\"JavaScript:void(0)\" class='layui-btn layui-btn-danger layui-btn-xs' onclick='deleteInfo(" . $vv['id'] . ")'>删除</a></th>
                            </tr>";
                    }
                }
                $_str .= "</tbody>
                        </table>
                </div>";
            }
        }
        $this->assign('level', $_level);
        $this->assign('info', $_str);
        return $this->fetch();
    }

    /**
     * [RechargeIndex 会员级别列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月14日16:04:56
     */
    public function RechargeIndex()
    {
        if (input('get.action') == 'ajax') {
            Session::delete('RechargePrizeInfo');
            $params = input();
            $where = [['id', '>', 0]];

            //会员级别
            if (!empty($params['param']['level'])) {
                $where[] = ['level', '=', $params["param"]['level']];
            }

            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'recharge',
                'group' => 'level',
                'order' => 'level desc'

            );
            $data = SetmealService::RechargeList($data_params)['data'];


            $total = SetmealService::RechargeTotal($where);


            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $level = Db::name('member_level')->field('id,level_title')->select();

            $this->assign('level', $level);

            return $this->fetch();
        }
    }

    /**
     * @access public
     * @content 修改价格
     */
    function change_price()
    {
        $params = input('post.');
        if (!empty($params)) {
            $data = SetmealService::UpgradeChangePrice($params);
            return array('status' => true);

        }

    }

    //会员升级删除
    function rechargeDeleteInfo()
    {
        $id = input("post.id");
        Db::table('recharge')->where(array('id' => $id))->delete();
        return array('status' => true);
    }

    /**
     * [recharge_save 套餐特权 会员充值金额 添加修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function recharge_save()
    {
        $rechargeId = input("get.id");
        if (input('get.action') == 'ajax') {
            $params = array_filter(input("post."));
            $data = SetmealService::RechareSave($params);
            return $data;

        } else {

            #查询会员等级
            $member_level = Db::table('member_level')->field('id,level_title')->select();
            $this->assign('level', $member_level);
            $this->assign('level_id', input('level'));
            return $this->fetch();
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function rechargeSession()
    {
        $params = input("post.");

        $data = SetmealService::rechargeSession($params);
        return array('status' => true, 'data' => $data);


    }

    /**
     * [rechareTypeWrite 修改金额]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     */
    function rechareTypeWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'S') {
                # 操作session
                #活动赠送
                $_session = Session('RechargePrizeInfo');
                if (!empty($_session)) {

                    if ($info['vmark'] == 1) {
                        #充值金额
                        $_session[$info['k']]['price'] = $info['value'];

                    } else if ($info['vmark'] == 2) {
                        #赠送金额
                        $_session[$info['k']]['giving'] = $info['value'];

                    }

                    #存session
                    Session::set('RechargePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }

            }
        }
    }

    /**
     * [rechareTypeDel 删除缓存  充值金额 ]
     * @author   lc
     * @version  1.0.0
     */
    function rechareTypeDel()
    {
        $k = input('post.k');
        #活动赠送
        $_session = Session('RechargePrizeInfo');
        if (!empty($_session)) {
            unset($_session[$k]);
            Session::set('RechargePrizeInfo', $_session);
        }
        return array('status' => true);

    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getRechareTypeList()
    {
        $data = SetmealService::getRechareTypeList();
        return array('status' => true, 'data' => $data);
    }

    /****************************************************************************会员充值 end ****************************************************************************/

    function recommendPresent()
    {
        $params = input();
        $recommendSession = Session("recommendPresent");
        $recommendSession = empty($recommendSession) ? array() : $recommendSession;
        $return = array("level_id" => $params['level_id'], "num" => $params['num'], "present_type" => $params['present_type'], "recommend_channel" => $params['recommend_channel'],
            "start_time" => $params['start_time'], "end_time" => $params['end_time'],
            "service_id" => 0, "voucher_title" => "", "voucher_id" => 0, "price" => 0, "voucher_over" => 0, "voucher_start" => 0, "voucher_type" => 0);
        if ($params['present_type'] == 1) {
            # 赠送卡券
            # 查询卡券id名称
            $voucherInfo = Db::table("card_voucher")->field("id,card_title,convert(card_price,decimal(10,2)) card_price")->where(array("server_id" => $params['service_id'], "card_type_id" => 1))->find();
            $return['service_id'] = $params['service_id'];
            $return['voucher_title'] = $voucherInfo['card_title'];
            $return['voucher_id'] = $voucherInfo['id'];
            $return['price'] = $voucherInfo['card_price'];
            $return['voucher_over'] = $params['voucher_over'];
            $return['voucher_start'] = $params['voucher_start'];
            $return['voucher_type'] = 4;

        }
        array_push($recommendSession, $return);
        Session("recommendPresent", array_values($recommendSession));
        return array("status" => true);
    }

    function reflushRecommendPresent()
    {
        $_return = array();
        $level_id = input("post.level_id");
        if (!empty($level_id)) {
            $info = Db::table("level_recommend")->where(array("level_id" => $level_id))->select();
            if (!empty($info)) {
                $_return = $info;
            }
        }
        $recommendSession = Session("recommendPresent");
        if (!empty($recommendSession)) {
            $_return = array_merge_recursive($_return, $recommendSession);
        }
        if (!empty($_return)) {
            $str = $this->recommendModel($_return);
            return array("status" => true, "data" => $str);
        } else {
            return array("status" => false);
        }
    }

    function recommendModel($data)
    {
        $str = null;
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $mark = "DB";
                $id = $v['id'];
                if (empty($v['id'])) {
                    $mark = "S";
                    $id = $k;
                }
                if ($v['present_type'] == 2) {
                    $title = "积分";
                    $voucher_over = $v['voucher_over'];
                    $voucher_over = "--";
                    $voucher_start = "--";
                } else {
                    $title = $v['voucher_title'];
                    $voucher_over = $v['voucher_over'];
                    $voucher_start = $v['voucher_start'] == 1 ? "是" : "否";
                }
                if ($v['recommend_channel'] == 1) {
                    $channel = "推广员";
                } else if ($v['recommend_channel'] == 2) {
                    $channel = "联盟商家";
                } else {
                    $channel = "合作门店";
                }
                $str .= "<tr>
                            <td>{$channel}</td>
                            <td>{$title}</td>
                            <td ondblclick=\"changeRecommendNum('" . $id . "','" . $mark . "',this)\">{$v['num']}</td>
                            <td >{$v['start_time']}</td>
                            <td >{$v['end_time']}</td>
                            <td >{$voucher_over}</td>
                            <td >{$voucher_start}</td>
                            <td>
                                <button type=\"button\" class=\"btn btn-danger\" onclick=\"delRecommendPresent(this,'" . $id . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $str;
    }

    /**
     * @return bool[]
     * @throws Exception
     * @throws \think\exception\PDOException
     * @context 修改关联服务的使用商品数量
     */
    function changeRecommendNum()
    {
        $param = input();
        if ($param['mark'] == "DB") {
            Db::table("level_recommend")->where(array("id" => $param['id']))->update(array("num" => $param['num']));
        } else {
            $relationSession = Session("recommendPresent");
            $relationSession[$param['id']]['num'] = $param['num'];
            Session("recommendPresent", $relationSession);
        }
        return array("status" => true);
    }

    function delRecommendPresent()
    {
        $param = input();
        if ($param['mark'] == "DB") {
            Db::table("level_recommend")->where(array("id" => $param['id']))->delete();
        } else {
            $relationSession = Session("recommendPresent");
            unset($relationSession[$param['id']]);
            Session("recommendPresent", $relationSession);
        }
        return array("status" => true);
    }
}
