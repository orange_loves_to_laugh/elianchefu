<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/16
 * Time: 16:28
 */

namespace app\admin\controller;


use app\service\AdapterModelsService;
use app\service\AdminService;
use app\service\BizService;
use app\service\ResourceService;
use think\Controller;
use think\Db;
use think\facade\Session;

class FileOperation extends Common
{

    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }
    //是否需要显示结算价格输入框
    private static $price_input_html=false;
    function uploadFile()
    {
        $info = input('post.file');
        ResourceService::uploadFile("img",'','file');

    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取关联门店
     */
    function getsRelevanceStore()
    {

        $checkStock=input("post.checkStock");
        $checkStockPrice=input("post.checkStockPrice");
        $service=input("post.service");
        $is_directly=input("post.is_directly");
        $where=[['biz_type','>',0]];
        if ($is_directly=='true')
        {
            $where=[['biz_type','>',1]];
        }

        self::$price_input_html=isset($checkStockPrice);
        $result=isset($checkStockPrice)?$checkStockPrice:$checkStock;
        $serviceId  = session("serviceId");
        $_where='biz_status = 1 and biz_type !=1 ';
        $biz_service_id_arr=array();
        if(!empty($serviceId)){
            #查询数据库中
            $biz_service_id_arr = Db::table('service_biz')->where(array('service_id'=>$serviceId))->column('biz_id');

        }
        #查询缓存中的 id   和数据库中  拼接
        $_session = session('getServiceBizId');
        if(!empty($_session)){
            foreach ($_session as $k=>$v){
                array_push($biz_service_id_arr,$v);
            }
        }
        #全新的数组 id  (数据库+缓存)
        if(!empty($biz_service_id_arr)){
            #门店id
            $biz_service_id = implode(',',$biz_service_id_arr);
            if(!empty($biz_service_id)){
                $_where .= " and id not in ($biz_service_id)";

            }
        }
       $list= Db::table("biz")
           ->where($where)
            ->field("id,biz_title,biz_phone,(case biz_type when 1 then '自营门店' when 2 then '加盟门店' when 3 then '合作门店' end) type_title,province,city,area,
            biz_address")
            ->where($_where)->order("biz_type")->select();
        $model= self::RelevanceStoreModel($list,$result,$service);
        return array("status"=>true,"data"=>$model);
//       if(!empty($list)){
//           $model= self::RelevanceStoreModel($list,$result,$service);
//           return array("status"=>true,"data"=>$model);
//       }else{
//           return array("status"=>false,"msg"=>"暂无门店信息");
//       }
    }

    /**
     * @param $list
     * @return string
     * @context 关联门店模板
     */
   private static function RelevanceStoreModel($list,$default,$service ='')
    {
        $_default_array=array();
        if(!is_null($default)){
            $_default_array=explode(',',$default);

            $id_price_arr=[];
            if (self::$price_input_html) {
                foreach ($_default_array as $v)
                {
                    $id_price_arr[explode('-',$v)[0]]=explode('-',$v)[1];
                }
            }

            //dump(isset($id_price_arr[55]));exit;
        }
        $price_input_html='';
        if (self::$price_input_html)
        {

            $price_input_html="<th>结算价格</th>";
        }
        if($service=='service'){
            $button_html = "<button class=\"btn btn-info\" onclick=\"relevanceOptionService('check-item')\" style=\"\">批量关联</button>
";

        }else{
            $button_html = "<button class=\"btn btn-info\" onclick=\"relevanceOption('check-item')\" style=\"\">批量关联</button>";
        }

        $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                        <div class=\"row\" style=\"width: 100%;float: left\">
                                            <div class=\"col-sm-6\">
                                                <div class=\"form-group\">
                                                    <input type=\"text\" id=\"search_title\"  class=\"form-control\" onkeyup=\"searchRelevanceStore()\" placeholder=\"请输入门店名称，回车键搜索\" autocomplete=\"off\">
                                                    <i class=\"form-group__bar\"></i>
                                                </div>
                                            </div>
                                        </div>
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            $button_html
                                        </div>
                                    </div>
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>        
                                                 ".$price_input_html."
                                                 <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

        foreach($list as $k=>$v){
            $str.="
                    <tr class='lump".$v['id']."'>
                        <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ";





            $price_input_html='';
            if($service == 'service'){
                $alone_button = "<td><button class=\"btn btn-info\" onclick=\"aloneOptionService('{$v['id']}','pro')\" style=\"\">关联</button></td>";

            }else{
                $alone_button = "<td><button class=\"btn btn-info\" onclick=\"aloneOption('{$v['id']}','pro')\" style=\"\">关联</button></td>";

            }
            if (self::$price_input_html)
            {

                if(!empty($_default_array) and isset($id_price_arr[$v['id']]))
                {
                    $str.='checked';

                }
                $price_input_html="<td>
<input name='settlement_dprice".$v['id']."' value='".$id_price_arr[$v['id']]."' placeholder='大型车结算价格' type='number' class='form-control form-control-lg'  >
<input name='settlement_zprice".$v['id']."' value='".$id_price_arr[$v['id']]."' placeholder='中型车结算价格' type='number' class='form-control form-control-lg'  >
<input name='settlement_xprice".$v['id']."' value='".$id_price_arr[$v['id']]."' placeholder='小型车结算价格' type='number' class='form-control form-control-lg'  >

</td>";
            }
            else{
                if(!empty($_default_array) and  in_array($v['id'],$_default_array)){

                    $str.='checked';
                }
            }
            $str.="></td>
                        <td>{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['province']}{$v['city']}{$v['area']}</td>
                         ".$price_input_html."
                         $alone_button
                    </tr>";
        }
        $str.="
                </tbody>
            </table>
        </div>
            </div>
             ";
        return $str;
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 搜索关联门店
     */
    function searchRelevanceStore()
    {
        $serviceId = session("serviceId");
        $_where = '';
        $biz_service_id_arr=array();
        if(!empty($serviceId)){
            #查询数据库中
            $biz_service_id_arr = Db::table('service_biz')->where(array('service_id'=>$serviceId))->column('biz_id');

        }
        #查询缓存中的 id   和数据库中  拼接
        /*   $_session = session('serviceInfoSession');
           if(!empty($_session)){
               foreach ($_session as $k=>$v){
                   array_push($biz_service_id_arr,$v['id']);
               }
           }*/
        #全新的数组 id  (数据库+缓存)
        if(!empty($biz_service_id_arr)){
            #,们店id
            $biz_service_id = implode(',',$biz_service_id_arr);
            if(!empty($biz_service_id)){
                $_where .= "id not in ($biz_service_id)";

            }
        }
        $search_title=input("post.search_title");
        $list= Db::table("biz")
        ->field("id")->where(array("biz_status"=>1))->where("biz_title like '%".$search_title."%'")->where($_where)->order("biz_type")->select();
        if(!empty($list)){
            $idcon=array_column($list,'id');
            rsort($idcon);
            return array('status'=>true,"param"=>$idcon);
        }else{
            return array("status"=>false);
        }
    }

    function checkRelevanceStore()
    {
        $pro_id=input("post.id");
        $list= Db::table("biz_goods bg")
            ->field("b.id,b.biz_title,b.biz_phone,(case b.biz_type when 1 then '自营门店' when 2 then '加盟门店' when 3 then '合作门店' end) type_title,
            b.biz_address")
            ->join("biz b","b.id=bg.biz_id")
            ->where("bg.pro_id={$pro_id}")
            ->select();
        if(!empty($list)){
            $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
            foreach($list as $k=>$v){
                $str.=" <td>{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['biz_address']}</td>
                    </tr>";
            }
            $str.="
                </tbody>
            </table>
        </div>
            </div>";
            return array("status"=>true,"data"=>$str);
        }else{
            return array("status"=>false);
        }

    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context logo 列表
     */
    function adapterModelsLogo()
    {
        $search=input("post.search");
        $list=AdapterModelsService::getsVinLogo($search);
        return array("status"=>true,"data"=>$list['data']);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取sort列表
     */
    function adapterModelsSort()
    {
        $logo_id=input("post.logo_id");
        $list=AdapterModelsService::getsVinSort($logo_id);
        return array("status"=>true,"data"=>$list['data']);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取车辆款式
     */
    function adapterModelsGroup()
    {
        $sort_id=input("post.sort_id");
        $list=AdapterModelsService::getsVinGroup($sort_id);
        return array("status"=>true,"data"=>$list['data']);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查看适配车型
     */
    function checkAdapterModels()
    {
        $oe_number=input("post.oe_number");
        $list=AdapterModelsService::checkAdapterModels($oe_number);
        if(!empty($list['data'])){
            $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>车型品牌</th>
                                                <th>车辆系列</th>
                                                <th>车辆型号</th>
                                            </tr>
                                        </thead>
                                        <tbody >";
            foreach($list['data'] as $k=>$v){
                $str.=" <td>{$v['logo_title']}</td>
                        <td>{$v['sort_title']}</td>
                        <td>{$v['group_title']}</td>
                    </tr>";
            }
            $str.="
                </tbody>
            </table>
        </div>
            </div>";
            return array("status"=>true,"data"=>$str);
        }else{
            return array("status"=>false);
        }
    }

    /**
     * [bizproSession 批量关联门店  存session]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizproSession(){
        #商品id 字符串
        $str = input("post.value");

        if(!empty($str)){
            $arr = explode(',',$str);
            $bizpro_arr = array();
            #获取商品 是否有重复的
            $bizproInfoSession = Session('bizStoreInfoSession');
            #缓存是否存在
            if(empty($bizproInfoSession)){
                foreach ($arr as $k=>$v){
                    $bizpro = Db::table("biz b")
                        ->field('b.id,b.biz_title,b.biz_phone,b.biz_address,(case b.biz_type when 1 then \'自营门店\' when 2 then \'加盟门店\' when 3 then \'合作门店\' end) type_title')
                        ->where(array('b.id'=>$v))->find();
                    array_push($bizpro_arr,$bizpro);

                }
            }

//            dump($bizpro_arr);die;
//            dump($str);
            #是否是第一次存的数组
            if(!empty($bizpro_arr)){
                foreach ($bizpro_arr as $k =>$v){
                    $bizpro_arr[$v['id']]=$v;
                    unset($bizpro_arr[$k]);
                }
                #存session
                Session::set('bizStoreInfoSession',$bizpro_arr);
            }else{
                #不是第一次 证明有缓存
                #判断缓存中的商品id  是否跟数据库中的相同  相同则删除



                #$arr  id 数组
                foreach ($arr as $ak=>$av){
                    if (!isset($bizproInfoSession[$av])) {
                        $bizpro = Db::table("biz b")
                            ->field('b.id,b.biz_title,b.biz_phone,b.biz_address,(case b.biz_type when 1 then \'自营门店\' when 2 then \'加盟门店\' when 3 then \'合作门店\' end) type_title')
                            ->where(array('b.id'=>$av))->find();
                        array_push($bizproInfoSession,$bizpro);
                    }

                }
                Session::set('bizStoreInfoSession',$bizproInfoSession);

            }
            $bizproInfoSession = Session('bizStoreInfoSession');
            #获取数组之前遍历 把id 赋值在二维数组键名上
            foreach ($bizproInfoSession as $sk =>$sv){
                $bizproInfoSession[$sv['id']]=$sv;
                unset($bizproInfoSession[$sk]);
            }

            #获取门店id
            $proId = session('proId');
            if(!empty($proId)){
                #查询数据库中的 商品id biz_goods
                $bizgoods =  self::BizGoodsInfo($proId);

                if(!empty($bizgoods)){
                    #获取最新缓存
                    $_Session = Session('bizStoreInfoSession');
                    foreach($bizgoods as $k=>$v){
                        if(isset($_Session[$v['biz_id']])){
                            #证明数据库中有值  删除缓存为这个键值的id
                            unset($_Session[$v['biz_id']]);
                        }
                    }
                    Session::set('bizStoreInfoSession',$_Session);
                }
            }
            #获取最新缓存
            $_session = Session('bizStoreInfoSession');
            if(!empty($_session)){
                return array('status'=>true);

            }else{

                return array('status'=>false,'msg'=>'已有关联门店！');

            }

        }

    }
    /**
     * [BizGoodsInfo 商品权限 门店里的商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function BizGoodsInfo($proId){
        $info = Db::table("biz_goods")->field('biz_id')->where(array('pro_id'=>$proId))->select();
        return $info;

    }
    /**
     * [relateOption 已关联门店列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function ServiceRelation(){
        #先看这个数组是否有
        $serviceInfoSession = Session('bizStoreInfoSession');
        if (empty($serviceInfoSession))
            $serviceInfoSession = array();

        $_str='';
        #商品id
        $_proId = input('post.id');

        if(!empty($_proId)){
            # 查询商品中的门店
            $_detail = Db::table('biz_goods bg')
                ->field('bg.*,b.biz_title,b.biz_address,b.biz_phone,(case b.biz_type when 1 then \'自营门店\' when 2 then \'加盟门店\' when 3 then \'合作门店\' end) type_title')
                ->join('biz b', 'b.id= bg.biz_id', 'left')
                ->where(array('bg.pro_id' => $_proId))->select();
            if (!empty($_detail)) {
                #已关联列表页面模板
                $_str .= self::getBizproArrInfo($_detail, 'DB',$_proId);
            }

        }

        #已关联列表页面模板
        $_str .= self::getBizproArrInfo($serviceInfoSession, 'S','');
        if(!empty($_str)){
            return array('status'=>true,'data'=>$_str);

        }else{
            return array('status'=>false,'msg'=>'暂无关联门店，请先关联！');

        }


    }

    /**
     * [getBizproArrInfo 商品权限  已关联列表页面整理]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getBizproArrInfo($arr,$mark,$_proId){
        $str = '';

        if(!empty($_proId)){
            if (!empty($arr) and is_array($arr)) {

                $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\" ><h3  style='justify-content: center;height:10%;align-items: center'>已关联门店</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                       <tr>
                                              
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach($arr as $k=>$v){

                    $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td>{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['biz_address']}</td>
                        <td>
                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizGoodsDel('$k','{$v['id']}','" .$mark . "','" .$v['pro_id'] . "')\" >删除</button>
                        </td>

                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            </div>";
            }

        }else{
            if (!empty($arr) and is_array($arr)) {

                $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\" style='justify-content: center;height:10%;align-items: center'><h3>暂未关联门店(保存之后变成已关联)</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach($arr as $k=>$v){
                    /*           商品编号                <td>{$v['biz_pro_number']}</td>
                     销售价格 <td onclick=\"bizproWrite('" . $k . "','" . $v['id'] . "','" . $v['biz_pro_online'] . "','" .$mark . "','4','" . $v['biz_id'] . "')\">" . getsPriceformat($v['biz_pro_online']) . "</td>*/
                    $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td>{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['biz_address']}</td>
                        <td>
                         <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizGoodsDel('$k','{$v['id']}','" .$mark . "','" .$v['pro_id'] . "')\" >删除</button>
                         </td>

                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            </div>";
            }

        }

        return $str;

    }
    /**
     * [bizGoodsDel 商品关联门店删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizGoodsDel(){
        /*id k mark*/
        $info = input("post.");
        if($info['mark'] == 'DB'){
            if(!empty($info['id'])){
                DB::table('biz_goods')->where(array('id'=>$info['id']))->delete();
                return array('status' => true);

            }
        }else{
            #删除缓存
            $_session = Session('bizStoreInfoSession');
            if(isset($info['k'])){

                if (!empty($_session)) {
                    unset($_session[$info['k']]);


                    Session::set('bizStoreInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }
    /***********************************获取关联商品模板 start **************************************/
    /**
     * @return array
     * @throws \think\exception\DbException
     * @context 获取关联商品
     */
    function getsRelevanceBizpro()
    {
/*(case biz_pro_status when 1 then '上架' when 2 then '下架' when 3 then '删除' end) type_title*/
        $mark = input("post.mark");
//        $_where =array("biz_pro_status"=>1,'biz_pro_paret'=>$mark);
        $_where = "biz_pro_status = 1 and biz_pro_paret = $mark";
        $bizpro =  self::bizproCheck($_where,'',$mark);

        $model= self::RelevanceBizproModel($bizpro,$mark);
        return array("status"=>true,"data"=>$model);

//        if(!empty($bizpro)){
//            $model= self::RelevanceBizproModel($bizpro,$mark);
//            return array("status"=>true,"data"=>$model);
//        }else{
//            return array("status"=>false,"msg"=>"暂无商品信息");
//        }
    }
    /**
     * @param $list
     * @return string
     * @context 关联商品模板
     */
    private static function RelevanceBizproModel($list,$mark)
    {


        $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                        <div class=\"row\" style=\"width: 100%;float: left\">
                                            <div class=\"col-sm-6\">
                                                <div class=\"form-group\">
                                                    <input type=\"text\" id=\"search_title\"  class=\"form-control\" onkeyup=\"searchRelevanceBizpro('{$mark}')\" placeholder=\"请输入门店名称，回车键搜索\" autocomplete=\"off\">
                                                    <i class=\"form-group__bar\"></i>
                                                </div>
                                            </div>
                                        </div>
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            <button class=\"btn btn-info\" onclick=\"relevanceOptionPro('check-item','{$mark}')\" style=\"\">批量关联</button>
                                        </div>
                                    </div>
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                                                <th>商品名称</th>
                                                <th>商品货号</th>
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                                <th>进货价格</th>        
                                                 <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
        foreach($list as $k=>$v){
            $str.="
                    <tr class='lump".$v['id']."'>
                        <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ";

                if(!empty($_default_array) and  in_array($v['id'],$_default_array)){

                    $str.='checked';
                }
            $str.="></td>
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$v['biz_pro_number']}</td>
                        <td>{$v['class_title']}</td>
                        <td>{$v['biz_pro_purch']}</td>
                        <td>{$v['supply_price']}</td>
                         <td> <button class=\"btn btn-info\" onclick=\"aloneOptionPro('{$v['id']}','{$mark}')\" style=\"\">关联</button></td>
                    </tr>";
        }
        $str.="
                </tbody>
            </table>
        </div>
            </div>
             ";
        return $str;
    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 搜索关联商品
     */
    function searchRelevanceBizpro()
    {
        $search_title=input("post.search_title");
        //商品类型 1 商品2 配件 3 耗材
        $protype=input("post.protype");
        $list= Db::table("biz_pro")
            ->field("id")->where(array("biz_pro_status"=>1,'biz_pro_paret'=>$protype))->where("biz_pro_title like '%".$search_title."%'")->order("biz_pro_create desc")->select();
        if(!empty($list)){
            $idcon=array_column($list,'id');
            rsort($idcon);
            return array('status'=>true,"param"=>$idcon);
        }else{
            return array("status"=>false);
        }
    }



    #查询商品信息
    static function bizproCheck($_where,$pro_num='',$mark){
        $where='';
        $serviceId  = session("serviceId");
        $biz_pro_id_arr=array();
        if(!empty($serviceId)){
            #查询数据库中
            $biz_pro_id_arr = Db::table('log_servicepro')->where(array('service_id'=>$serviceId))->column('pro_id');

        }
        #查询缓存中的 id   和数据库中  拼接
        $_session = Session('bizProInfoSession');
           if(!empty($_session)){
               foreach ($_session as $k=>$v){
                   array_push($biz_pro_id_arr,$v['id']);
               }
           }
        #全新的数组 id  (数据库+缓存)
        if(!empty($biz_pro_id_arr)){
            #,商品id
            $biz_pro_id_ = implode(',',$biz_pro_id_arr);
            if(!empty($biz_pro_id_)){
                $where = " bp.id not in ($biz_pro_id_)";

            }
        }
        $list= Db::table("biz_pro bp")
            ->field("bp.id,bp.biz_pro_title,pc.class_title,bp.biz_pro_class_id,bp.biz_pro_price,bp.biz_pro_number,bp.biz_pro_count,bp.biz_pro_purch,bp.supply_price,bp.biz_pro_paret")
            ->join('pro_class pc','pc.id = bp.biz_pro_class_id','left')
            ->where($_where)
            ->where($where)
            ->order("biz_pro_create desc")->select();
        if(!empty($pro_num)){
            foreach ($list as &$v){
                $v['pro_num'] = $pro_num;
            }
        }

        return $list;
    }

    /**
     * @return bool[]
     * @context 批量关联商品
     */
    function bizProInfoSession(){
        $str = input("post.value");
        $pro_num = input("post.pro_num");
        # mark 商品类型 1 商品 2 配件 3 耗材
        $mark = input("post.mark");
        if(!empty($str)){
            $arr = explode(',',$str);
            $bizproInfoSession = Session('bizProInfoSession');
            if(empty($bizproInfoSession)) $bizproInfoSession = array();
            foreach($arr as $k=>$v){
                $_where =array('bp.id'=>$v,'biz_pro_paret'=>$mark);
                $bizpro =  self::bizproCheck($_where,$pro_num,$mark);
                array_push($bizproInfoSession,$bizpro[0]);
            }
            Session("bizProInfoSession",$bizproInfoSession);
        }
        return array('status'=>true);
    }
    /**
     * [bizProInfoSession 批量关联商品  存session]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
//    function bizProInfoSession(){
//        #商品id 字符串
//        $str = input("post.value");
//        $pro_num = input("post.pro_num");
//        # mark 商品类型 1 商品 2 配件 3 耗材
//        $mark = input("post.mark");
//        if(!empty($str)){
//            $arr = explode(',',$str);
//            $bizpro_arr = array();
//            #获取商品 是否有重复的
//            $bizproInfoSession = Session('bizProInfoSession');
//            #缓存是否存在
//            if(empty($bizproInfoSession)){
//                foreach ($arr as $k=>$v){
//                    #查询商品信息
//                    $_where =array('bp.id'=>$v,'biz_pro_paret'=>$mark);
//                    $bizpro =  self::bizproCheck($_where,$pro_num,$mark);
//
//                    array_push($bizpro_arr,$bizpro[0]);
//
//                }
//            }
////            dump($bizpro_arr);die;
////            dump($str);
//            #是否是第一次存的数组
//            if(!empty($bizpro_arr)){
//                foreach ($bizpro_arr as $k =>$v){
//                    $bizpro_arr[$v['id']]=$v;
//                    unset($bizpro_arr[$k]);
//                }
//                #存session
//                Session::set('bizProInfoSession',$bizpro_arr);
//            }else{
//                #不是第一次 证明有缓存
//                #判断缓存中的商品id  是否跟数据库中的相同  相同则删除
//                #$arr  id 数组
//                foreach ($arr as $ak=>$av){
//                    if (!isset($bizproInfoSession[$av])) {
//                        $_where =array('bp.id'=>$av,'biz_pro_paret'=>$mark);
//                        #查询商品信息
//                        $bizpro =  self::bizproCheck($_where,$pro_num,$mark);
//                        array_push($bizproInfoSession,$bizpro[0]);
//                    }
//
//                }
//                Session::set('bizProInfoSession',$bizproInfoSession);
//
//            }
//            $bizproInfoSession = Session('bizProInfoSession');
//            #获取数组之前遍历 把id 赋值在二维数组键名上
//            foreach ($bizproInfoSession as $sk =>$sv){
//                $bizproInfoSession[$sv['id']]=$sv;
//                unset($bizproInfoSession[$sk]);
//            }
//
//            #获取服务id
//            $serviceId = session('serviceId');
//            if(!empty($proId)){
//                #查询数据库中的 商品id log_servicepro   LogServiceproInfo
//                $bizgoods =  self::LogServiceproInfo($serviceId);
//
//                if(!empty($bizgoods)){
//                    #获取最新缓存
//                    $_Session = Session('bizProInfoSession');
//                    foreach($bizgoods as $k=>$v){
//                        if(isset($_Session[$v['pro_id']])){
//                            #证明数据库中有值  删除缓存为这个键值的id
//                            unset($_Session[$v['pro_id']]);
//                        }
//                    }
//                    Session::set('bizProInfoSession',$_Session);
//                }
//            }
//            #获取最新缓存
//            $_session = Session('bizProInfoSession');
//            if(!empty($_session)){
//                return array('status'=>true);
//
//            }else{
//                if($mark == 1){
//                    return array('status'=>false,'msg'=>'已有关联商品！');
//
//                }else if($mark == 2){
//                    return array('status'=>false,'msg'=>'已有关联配件！');
//
//                }else{
//                    return array('status'=>false,'msg'=>'已有关联耗材！');
//
//                }
//
//            }
//
//        }
//
//    }
    /**
     * [BizGoodsInfo 商品权限 门店里的商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function LogServiceproInfo($serviceId){
        $info = Db::table("log_servicepro")->field('pro_id')->where(array('service_id'=>$serviceId))->select();
        return $info;

    }
    /**
     * [ServiceRelationPro 已关联商品列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params  mark  1 商品2 配件 3 耗材
     */

    function ServiceRelationPro(){
        $_mark = input('post.mark');

        #先看这个数组是否有
        $serviceInfoSession = Session('bizProInfoSession');
        if (empty($serviceInfoSession))
            $serviceInfoSession = array();

        $_str='';
        #服务id
        $_serviceId = input('post.id');
        if(empty($_serviceId)){
            $_serviceId = session('serviceId');

        }

        if(!empty($_serviceId)){
            # 查询服务中的商品
            $_detail = Db::table('log_servicepro ls')
                ->field("ls.id,bp.biz_pro_title,pc.class_title,bp.biz_pro_class_id,bp.biz_pro_price,bp.biz_pro_number,bp.biz_pro_count,bp.biz_pro_purch,bp.supply_price,ls.pro_num,bp.biz_pro_paret ")
                ->join('biz_pro bp', 'bp.id= ls.pro_id', 'left')
                ->join('pro_class pc','pc.id = bp.biz_pro_class_id','left')
                ->where(array('ls.service_id' => $_serviceId))->select();

            if (!empty($_detail)) {
                #已关联列表页面模板
                $_str .= self::getBizproServiceInfo($_detail, 'DB',$_serviceId,$_mark);
            }

        }

        #已关联列表页面模板
        $_str .= self::getBizproServiceInfo(array_values($serviceInfoSession), 'S','',$_mark);
        $_str = self::getBizproServiceInfoModel($_str);
        if(!empty($_str)){
            return array('status'=>true,'data'=>$_str);

        }else{

            if($_mark == 1){
                return array('status'=>false,'msg'=>'暂无关联商品，请先关联！');


            }else if($_mark == 2){
                return array('status'=>false,'msg'=>'暂无关联配件，请先关联！');


            }else{
                return array('status'=>false,'msg'=>'暂无关联耗材，请先关联！');


            }

        }


    }
    static function getBizproServiceInfoModel($_str){
        $str=" <div class=\"card-body\">
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                           <tr>
                                            <th>商品名称</th>
                                            <th>商品类型</th>
                                            <th>商品货号</th>
                                            <th>商品分类</th>
                                            <th>供货价格</th>
                                            <th>进货价格</th> 
                                            <th>使用数量</th>  
                                            <th>操作</th>
                                           </tr>
                                        </thead>
                                        <tbody id='storebody'>";
        $str.=$_str;
        $str.="
                </tbody>
            </table>
            </div>";
        return $str;
    }

    static function getBizproServiceInfo($arr,$mark,$_proId,$protype){
        $str = '';
        if (!empty($arr) and is_array($arr)) {
            $pro_num='';
            foreach($arr as $k=>$v){
                if($v['biz_pro_paret']==1){
                    $paret_title = "商品";
                }else if($v['biz_pro_paret']==2){
                    $paret_title = "配件";
                }else{
                    $paret_title = "耗材";
                }
                $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$paret_title}</td>
                        <td>{$v['biz_pro_number']}</td>
                        <td>{$v['class_title']}</td>
                        <td>{$v['biz_pro_purch']}</td>
                        <td>{$v['supply_price']}</td>
                        <td onclick=\"logServiceProWrite('" . $v['id'] . "','" . $k . "','" . $v['pro_num'] . "','" .$mark . "','6','".$protype."',this)\">{$v['pro_num']}</td>
                      
                        <td>
                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"logServiceProDel('$k','{$v['id']}','" .$mark . "','" .$v['pro_id'] . "','".$protype."',this)\" >删除</button>
                        </td>

                    </tr>";
            }
        }
        return $str;
    }
    /**
     * [getBizproServiceInfo 服务关联商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params $protype 1 商品 2 配件 3 耗材
     */
//    static function getBizproServiceInfo($arr,$mark,$_proId,$protype){
//
//        $str = '';
//        if(!empty($_proId)){
//            if (!empty($arr) and is_array($arr)) {
//                $str=" <div class=\"card-body\">
//                                    <table class=\"table table-hover mb-0\">
//                                        <thead>
//                                           <tr>
//                                                <th>商品名称</th>
//                                                <th>商品类型</th>
//                                                <th>商品货号</th>
//                                                <th>商品分类</th>
//                                                <th>供货价格</th>
//                                                <th>进货价格</th>
//                                                <th>使用数量</th>
//                                                <th>操作</th>
//                                            </tr>
//                                        </thead>
//                                        <tbody id='storebody'>";
//                $pro_num='';
//                foreach($arr as $k=>$v){
//                    if($protype == 1){
//                        $pro_num="  <td onclick=\"logServiceProWrite('" . $v['id'] . "','" . $k . "','" . $v['pro_num'] . "','" .$mark . "','6','".$protype."')\">{$v['pro_num']}</td>";
//                    }
//                    if($v['biz_pro_paret']==1){
//                        $paret_title = "商品";
//                    }else if($v['biz_pro_paret']==2){
//                        $paret_title = "配件";
//                    }else{
//                        $paret_title = "耗材";
//                    }
//                    $str.="
//                    <tr class='lump".$v['id']."'>
//
//                        <td>{$v['biz_pro_title']}</td>
//                        <td>{$paret_title}</td>
//                        <td>{$v['biz_pro_number']}</td>
//                        <td>{$v['class_title']}</td>
//                        <td>{$v['biz_pro_purch']}</td>
//                        <td>{$v['supply_price']}</td>
//                          ".$pro_num."
//
//                        <td>
//                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"logServiceProDel('$k','{$v['id']}','" .$mark . "','" .$v['pro_id'] . "','".$protype."')\" >删除</button>
//                        </td>
//
//                    </tr>";
//                }
//                $str.="
//                </tbody>
//            </table>
//        </div>
//            </div>";
//            }
//
//        }else{
//            if($protype == 1){
//                $pro_num_html="<th>商品数量</th>";
//                $title_html=" <div class=\"col-sm-12\" style='justify-content: center;height:10%;align-items: center'><h3>暂未关联商品(保存之后变成已关联)</h3>";
//            }else if($protype == 2){
//                $title_html=" <div class=\"col-sm-12\" style='justify-content: center;height:10%;align-items: center'><h3>暂未关联配件(保存之后变成已关联)</h3>";
//            }
//            if (!empty($arr) and is_array($arr)) {
//
//                $str=" <div class=\"card-body\">
//                                       $title_html
//
//
//                                    <table class=\"table table-hover mb-0\">
//                                        <thead>
//                                            <tr>
//
//                                                <th>商品名称</th>
//                                                 <th>商品类型</th>
//                                                <th>商品货号</th>
//                                                <th>商品分类</th>
//                                                <th>供货价格</th>
//                                                <th>进货价格</th>
//                                                 ".$pro_num_html."
//                                                <th>操作</th>
//                                            </tr>
//                                        </thead>
//                                        <tbody id='storebody'>";
//
//                foreach($arr as $k=>$v){
//                    if($protype == 1){
//                        $pro_num="  <td onclick=\"logServiceProWrite('" . $v['id'] . "','" . $k . "','" . $v['pro_num'] . "','" .$mark . "','6','".$protype."')\">{$v['pro_num']}</td>";
//                    }
//                    if($v['biz_pro_paret']==1){
//                        $paret_title = "商品";
//                    }else if($v['biz_pro_paret']==2){
//                        $paret_title = "配件";
//                    }else{
//                        $paret_title = "耗材";
//                    }
//                    /*           商品编号                <td>{$v['biz_pro_number']}</td>
//                     销售价格 <td onclick=\"bizproWrite('" . $k . "','" . $v['id'] . "','" . $v['biz_pro_online'] . "','" .$mark . "','4','" . $v['biz_id'] . "')\">" . getsPriceformat($v['biz_pro_online']) . "</td>*/
//                    $str.="
//                    <tr class='lump".$v['id']."'>
//
//                        <td>{$v['biz_pro_title']}</td>
//                         <td>{$paret_title}</td>
//                        <td>{$v['biz_pro_number']}</td>
//                        <td>{$v['class_title']}</td>
//                        <td>{$v['biz_pro_purch']}</td>
//                        <td>{$v['supply_price']}</td>
//                            ".$pro_num."
//                        <td>
//                         <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"logServiceProDel('$k','{$v['id']}','" .$mark . "','" .$v['pro_id'] . "','".$protype."')\" >删除</button>
//                         </td>
//
//                    </tr>";
//                }
//                $str.="
//                </tbody>
//            </table>
//        </div>
//            </div>";
//            }
//
//        }
//
//        return $str;
//
//    }
    /**
     * [bizGoodsDel 商品关联门店删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function logServiceProDel(){
        /*id k mark*/
        $info = input("post.");
        if($info['mark'] == 'DB'){
            if(!empty($info['id'])){
                DB::table('log_servicepro')->where(array('id'=>$info['id']))->delete();
                return array('status' => true);

            }
        }else{
            #删除缓存
            $_session = Session('bizProInfoSession');
            if(isset($info['k'])){

                if (!empty($_session)) {
                    unset($_session[$info['k']]);
                    Session::set('bizProInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }
    //商品数量点击修改
    function logServiceProWrite(){
        /*id  k  value  mark, vamrk*/
        $info = input("post.");
        if(!empty($info)){
            if ($info['mark'] == 'DB') {
                if($info['vmark'] == 6){
                    # 直接操作数据库
                    $a = Db::table("log_servicepro")->where(array('id'=>$info['id']))->update(array('pro_num'=>$info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                $_session = Session('bizProInfoSession');
                if (!empty($_session)) {
                    if($info['vmark'] == 6){
                        #商品数量 pro_num
                        $_session[$info['k']]['pro_num'] = $info['value'];

                    }
                    Session::set('bizProInfoSession', $_session);
                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }
    /***********************************获取关联商品模板 end **************************************/
    /******************************** 服务关联模板 start ****************************/
    /**
     * [ServiceRelationPrice 服务已关联门店列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function ServiceRelationPrice(){
        $_str='';

        #获取门店的id  和门店价格
        $biz_id = input("post.biz_id");
        $biz_id_price = input("post.biz_id_price");
        $biz_id_price_arr = explode(',',$biz_id_price);

        if(!empty($biz_id)){
            #跟据门店id  查询门店信息
            $_biz = Db::table('biz b')
                ->field('b.id,b.biz_title,b.province,b.city,b.area,b.biz_address,b.biz_phone,(case b.biz_type when 1 then \'自营门店\' when 2 then \'加盟门店\' when 3 then \'合作门店\' end) type_title')

                ->where("b.id in ($biz_id)")->select();
            foreach($_biz as $k=>$v){
                foreach($biz_id_price_arr as $bk=>$bv){
                    $_biz_id = $v['id'];
                    #strlen  求字符串长度  并加1
                    $length = strlen($_biz_id)+1;
                    #判断这个价格数组里 是否 有 id- 的 (根据id  和 数组的首个id拼接 -   作对比)
                    if(substr($bv,'0',$length) == $v['id'].'-'){
                        $price = explode('-',$bv);
                        $_biz[$k]['biz_id_price'][$bk]['price'] = $price[2];
                        $_biz[$k]['biz_id_price'][$bk]['car'] = $price[1];
                        $_biz[$k]['biz_id_price'][0]['biz_id_price_title'] = '大型车结算价格';
                        $_biz[$k]['biz_id_price'][1]['biz_id_price_title'] = '中型车结算价格';
                        $_biz[$k]['biz_id_price'][2]['biz_id_price_title'] = '小型车结算价格';

                    }

                }
            }
            #已关联列表页面模板
            $_str .= self::getBizServiceArrInfo($_biz, 'S');
        }


        #商品id
        $_serviceId = input('post.serviceId');

        if(!empty($_serviceId)){
            # 查询关联门店
            $_detail = self::getServiceBiz($_serviceId);
            if (!empty($_detail)) {
                #已关联列表页面模板
                $_str .= self::getBizServiceArrInfo($_detail, 'DB',$_serviceId);
            }

        }


        if(!empty($_str)){
            return array('status'=>true,'data'=>$_str);

        }else{
            return array('status'=>false,'msg'=>'暂无关联门店，请先关联！');

        }


    }
    /*
     * @content  查询已关联的门店
     * */
    static function getServiceBiz($_serviceId){
        #查询出 已关联门店
        $_detail = Db::table('service_biz sb')
            ->field('sb.*,b.biz_title,b.province,b.city,b.area,b.biz_address,b.biz_phone,(case b.biz_type when 1 then \'自营门店\' when 2 then \'加盟门店\' when 3 then \'合作门店\' end) type_title')
            ->join('biz b', 'b.id= sb.biz_id', 'left')
            ->where("sb.service_id = $_serviceId and b.biz_type>1")->select();
        #查询已关联门店的服务价格   线上  线下 结算价格相同  只求线上就行
        if(!empty($_detail)){
            foreach ($_detail as $k=>$v){
                #线上的结算价格  大型车
                $_detail[$k]['biz_id_price']  = Db::table('service_car_mediscount')->where(array('service_id'=>$_serviceId,'biz_id'=>$v['biz_id'],'service_type'=>1))->select();
                $_detail[$k]['biz_id_price'][0]['biz_id_price_title'] = '大型车结算价格';
                $_detail[$k]['biz_id_price'][1]['biz_id_price_title'] = '中型车结算价格';
                $_detail[$k]['biz_id_price'][2]['biz_id_price_title'] = '小型车结算价格';

            }

        }
        return $_detail;

    }
    /**
     * [getBizproArrInfo 商品权限  已关联列表页面整理]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getBizServiceArrInfo($arr,$mark,$_serviceId=''){
        $str = '';

        if(!empty($_serviceId)){
            if (!empty($arr) and is_array($arr)) {

                $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\" ><h3  style='justify-content: center;height:10%;align-items: center'>已关联门店</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                       <tr>
                                              
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>
                                                <th>结算价格</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach($arr as $k=>$v){
                    $price_input_html ='';
                    if(!empty($v['biz_id_price'])){
                        foreach($v['biz_id_price'] as $pk=>$pv){
                            $price_input_html .="<input onblur=\"bizServiceWrite('{$v['id']}','{$pv['settlement_amount']}','" .$pv['car_level_id'] . "','$mark','{$pv['service_id']}','$pk','{$v['biz_id']}')\"  name='settlement_dprice$pk$mark{$v['id']}'  value='".$pv['settlement_amount']."' placeholder='{$pv['biz_id_price_title']}' type='number' class='form-control form-control-lg pricehtml'>";
                        }
                    }
                    $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td >{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['province']}{$v['city']}{$v['area']}{$v['biz_address']}</td>
                        <td class='bizwrite' >$price_input_html</td>
                        <td>
                         <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizServiceDel('$k','{$v['id']}','" .$mark . "','".$_serviceId."','{$v['biz_id']}')\" >删除</button>
                        </td>

                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            </div>";
            }

        }else{
            if (!empty($arr) and is_array($arr)) {

                $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\" style='justify-content: center;height:10%;align-items: center'><h3>暂未关联门店(保存之后变成已关联)</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>门店名称</th>
                                                <th>门店电话</th>
                                                <th>门店类型</th>
                                                <th>门店地区</th>
                                                <th>结算价格</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach($arr as $k=>$v){
                    $price_input_html ='';
                    if(!empty($v['biz_id_price'])){
                        foreach($v['biz_id_price'] as $pk=>$pv){
                            $price_input_html .="<input onblur=\"bizServiceWrite('{$v['id']}','{$pv['price']}','" .$pv['car'] . "','$mark','','$pk')\"  name='settlement_dprice$pk$mark{$v['id']}'  value='".$pv['price']."' placeholder='{$pv['biz_id_price_title']}' type='number' class='form-control form-control-lg pricehtml'>";
                        }
                    }

                    $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td >{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$v['type_title']}</td>
                        <td>{$v['province']}{$v['city']}{$v['area']}{$v['biz_address']}</td>
                        <td class='bizwrite' >$price_input_html</td>
                        <td>
                         <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizServiceDel('$k','{$v['id']}','" .$mark . "')\" >删除</button>
                         </td>

                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            </div>";
            }

        }

        return $str;

    }
    /**
     * [bizServiceDel 服务关联门店删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizServiceDel(){


        /*id k mark*/
        $info = input("post.");
        //获取门店id 数组 为了统计关联门店数量
        if(!empty($info['biz_id_count'][0])){
            $biz_id_arr = explode(',',$info['biz_id_count']);
            $count_arr = count($biz_id_arr);
        }else{
            $count_arr = 0;
        }


        if($info['mark'] == 'DB'){
            #删除门店  和对应的门店结算价格
            if(!empty($info['id'])){
                #删除 service_biz  和 service_car_misdiscoumt
                $res = DB::table('service_biz')->where(array('id'=>$info['id']))->delete();
                Db::table('service_car_mediscount')->where(array('biz_id'=>$info['biz_id'],'service_id'=>$info['service_id'],'service_type'=>1))->delete();
                Db::table('service_car_mediscount')->where(array('biz_id'=>$info['biz_id'],'service_id'=>$info['service_id'],'service_type'=>2))->delete();
                if($res){
                    #获取服务关联门店数量
                    $bizCount = AdminService::relationServiceNum($info['service_id']);
                    $count = $bizCount+$count_arr;
                    return array('status' => true,'count'=>$count);

                }


            }
        }else{
            #input 隐藏中的门店结算价格
            $biz_id_price = input("post.biz_id_price");
            #input 隐藏中的门店id
            $biz_id = input("post.biz_id");
            #服务id
            $service_id = session('serviceId');



            if(!empty($biz_id_price)){
                #修改 input 中 门店结算价格
                #跟据 门店 id  查询 input 中这个 值  并删除
                $biz_id_price_arr = explode(',',$biz_id_price);
                $biz_id = explode(',',$biz_id);
                if(!empty($biz_id_price_arr)){
                    foreach ($biz_id_price_arr as $k=>$v){
                        #strlen  求字符串长度  并加1(这样数组首个的id 就能一一对应了)
                        $length = strlen($info['id'])+1;
                        #判断这个价格数组里 是否 有 id- 的 (根据id  和 数组的首个id拼接 -   作对比)
                        if(substr($v,'0',$length) == $info['id'].'-'){
                            #有  删除对应id 的 键值
                            unset($biz_id_price_arr[$k]);

                        }
                    }
                }
                if(!empty($biz_id)){
                    foreach ($biz_id as $bk=>$bv){
                        #删除input 隐藏中的input id
                        if($bv == $info['id']){
                            #有  删除对应id 的 键值
                            unset($biz_id[$bk]);


                        }
                    }
                }
                #重组价格 和 门店id
                $biz_id_price = implode(',',$biz_id_price_arr);
                $_biz_id = implode(',',$biz_id);
                #获取服务关联门店数量
                $bizCount = AdminService::relationServiceNum($service_id);
                $count = intval($bizCount)+$count_arr-1;
                return array('status' => true ,'biz_id_price'=>$biz_id_price,'biz_id'=>$_biz_id,'count'=>$count);

            }else{
                return array('status' => false);

            }
        }


    }
    /**
     * [bizServiceWrite 服务关联门店修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizServiceWrite(){
        /*id k mark*/
        $info = input("post.");
        if($info['mark'] == 'DB'){
            #修改 service_car_mediscount  中对应服务id  门店id 的结算价格
            if(!empty($info['id'])){
                #DB 时 这个id 为门店biz_id
                #线上
                $res = DB::table('service_car_mediscount')
                    ->where(array('biz_id'=>$info['biz_id'],'service_id'=>$info['service_id'],'car_level_id'=>$info['car'],'service_type'=>1))
                    ->update(array('settlement_amount'=>$info['value']));
                #线下（门店）
                DB::table('service_car_mediscount')
                    ->where(array('biz_id'=>$info['biz_id'],'service_id'=>$info['service_id'],'car_level_id'=>$info['car'],'service_type'=>2))
                    ->update(array('settlement_amount'=>$info['value']));
                if($res){
                    return array('status' => true);

                }

            }
        }else{
            #input 隐藏中的门店结算价格
            $biz_id_price = input("post.biz_id_price");

            #input 隐藏中的门店id
            $biz_id = input("post.biz_id");
            if(!empty($biz_id_price)){
                #修改 input 中 门店结算价格
                #跟据 门店 id  查询 input 中这个 值  并修改
                $biz_id_price_arr = explode(',',$biz_id_price);
                $biz_id = explode(',',$biz_id);
                if(!empty($biz_id_price_arr)){
                    foreach ($biz_id_price_arr as $k=>$v){
                        #strlen  求字符串长度  并加1(这样数组首个的id 就能一一对应了)
                        $length = strlen($info['id'])+3;
                        #判断这个价格数组里 是否 有 id- 的 (根据id  和 数组的首个id拼接 -   作对比)
                        if(substr($v,'0',$length) == $info['id'].'-'.$info['car']){
                            #有 修改大型车价格  中型车  小型车    $info['car'] =  3d  大型车 2z 中型车 1x 小型车
                            $price = explode('-',$v);
                            #修改价格
                            $price[2] =$info['value'];

                            $biz_id_price_arr[$k] = implode('-',$price);

                        }

                    }
                }

                #重组价格 和 门店id
                $biz_id_price = implode(',',$biz_id_price_arr);
                $_biz_id = implode(',',$biz_id);
                return array('status' => true ,'biz_id_price'=>$biz_id_price,'biz_id'=>$_biz_id);

            }else{
                return array('status' => false);

            }
        }


    }
    /**
     * [decideServiceBiz 判断服务关联门店 是否重复]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function decideServiceBiz(){
        $serviceId  = session('serviceId');
        #判断服务关联门店是否重复
        $biz_id = input("post.str");
        $biz_id_arr = explode(',',$biz_id);
        session::set('getServiceBizId',$biz_id_arr);

        $str_price = input("post.str_price");
        $str_price_arr = explode(',',$str_price);
        if(empty($biz_id)){
            return array('status'=>false,'msg'=>'请选择关联门店！');
        }
        if(!empty($serviceId)){
            $data = Db::table("service_biz")->where("biz_id in ($biz_id) and service_id =$serviceId")->column('biz_id');
            if(!empty($data)){
                #证明 数据库中有 biz_id
                #把查出来的数据  和 input  获取的门店id  做对比
                foreach ($biz_id_arr as $k=>$v){
                    if(in_array($v,$data)){
                        unset($biz_id_arr[$k]);
                        #strlen  求字符串长度  并加1
                        $length = strlen($v)+1;
                        foreach ($str_price_arr as $stk=>$stv){
                            #判断这个价格数组里 是否 有 id- 的 (根据id  和 数组的首个id拼接 -   作对比)
                            if(substr($stv,'0',$length) == $v.'-'){
                                unset($str_price_arr[$stk]);


                            }
                        }
                    }



                }
            }
        }

            if(!empty($biz_id_arr)){

                $bizid_str = implode(',',$biz_id_arr);
                $bizid_strprice = implode(',',$str_price_arr);

                #获取服务关联门店数量
                $bizCount= 0;
                if(!empty($serviceId)){
                    $bizCount = AdminService::relationServiceNum($serviceId);

                }
                $count = $bizCount+count($biz_id_arr);



                return array('status'=>true,'bizid_str'=>$bizid_str,'bizid_strprice'=>$bizid_strprice,'count'=>$count);
            }else{
                return array('status'=>false,'msg'=>'已经关联这个门店！请不要重复关联');

            }



    }
    /******************************** 服务关联模板 end ****************************/
}