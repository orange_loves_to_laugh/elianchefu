<?php
/**
 * Created by PhpStorm.
 * User: xiaoz
 * Date: 2021/1/8
 * Time: 9:59
 */

namespace app\admin\controller;


use think\Db;

class CooperativeTask extends Common
{
    function listInfo()
    {
        if (input('get.action') == 'view') {
            # 页面加载
            return $this->fetch();
        } else {
            # 数据加载
            $where = 'id > 0 ';
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            $time = date("Y-m-d H:i:s");
            # 搜索条件
            $searchParams = input('post.searchParams');
            # 状态筛选
            if (!empty($searchParams['status'])) {
                if ($searchParams['status'] == 1) {
                    $where .= " and start_time > '" . $time . "'";
                }
                if ($searchParams['status'] == 2) {
                    $where .= " and start_time <= '" . $time . "' and end_time > '" . $time . "'";
                }
                if ($searchParams['status'] == 3) {
                    $where .= " and end_time > '" . $time . "'";
                }
            }
            # 类型筛选
            if (!empty($searchParams['task_type'])) {
                $where .= " and type = " . $searchParams['task_type'];
            }
            $info = Db::table('cooperative_task')
                ->where($where)
                ->order(array('id' => 'desc'))
                ->paginate($listRows, false, ['query' => request()->param()])
                ->toArray()['data'];
            if (!empty($info)) {
                foreach ($info as $k => $v) {
                    if ($time < $v['start_time'] and $time < $v['end_time']) {
                        #当前时间 小于 开始时间 和结束时间
                        $info[$k]['status'] = '待上架';

                    } else if ($time >= $v['start_time'] and $time <= $v['end_time']) {
                        #当前时间 大于 等于开始 下雨等于 结束
                        $info[$k]['status'] = '进行中';

                    } else if ($time > $v['start_time'] and $time > $v['end_time']) {
                        #当前时间 大于 开始时间 和结束时间
                        $info[$k]['status'] = '已结束';
                    }
                    if ($v['type'] == 1) {
                        $info[$k]['num'] .= '个';
                    } elseif ($v['type'] == 2) {
                        $info[$k]['num'] = getsPriceFormat($v['num']) . '元';
                    } elseif ($v['type'] == 3) {
                        $info[$k]['num'] .= '次';
                    } elseif ($v['type'] == 4) {
                        $info[$k]['num'] .= '次';
                    } elseif ($v['type'] == 5) {
                        $info[$k]['num'] = getsPriceFormat($v['num']) . '元';
                    }
                    # 余额
                    $info[$k]['balance'] = getsPriceFormat($v['balance']) . '元';
                }
            }
            $total = Db::table('cooperative_task')->where($where)->count();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $info, 'total' => $total]];
        }
    }

    function taskAdd()
    {
        if (input('get.action') == 'view') {
            # 加载页面
            return $this->fetch('/cooperative_task/task_info');
        } else {
            # 添加数据
            $params = input("post.");
            unset($params['id']);
            Db::table('cooperative_task')->insert($params);
            return array('status' => true);
        }
    }

    function taskModify()
    {
        if (input('get.action') == 'view') {
            # 加载页面
            $taskId = input('get.id');
            $taskInfo = Db::table('cooperative_task')->where(array('id' => $taskId))->find();
            $this->assign('info', $taskInfo);
            return $this->fetch('/cooperative_task/task_info');
        } else {
            # 修改数据
            $params = input("post.");
            $taskId = $params['id'];
            unset($params['id']);
            Db::table('cooperative_task')->where(array('id' => $taskId))->update($params);
            return array('status' => true);
        }
    }

    function taskDel(){
        $taskId = input('post.id');
        if(!empty($taskId)){
            # 查询是否存在已经领取任务没完成的，存在不能删除
            $taskCount = Db::table('cooperative_task_list')->where(array('task_id'=>$taskId))->where('status =1 or status = 2')->count();
            if($taskCount>0){
                # 不能删除
                return array('status'=>false,'msg'=>'任务正在进行中，不可删除');
            }
            # 判断任务的开始时间和结束时间
            $taskTime = Db::table('cooperative_task')->where(array('id'=>$taskId))
                ->where("start_time <= '".date('Y-m-d H:i:s')."' and end_time >= '".date('Y-m-d H:i:s')."'")
                ->find();
            if(!empty($taskTime)){
                # 不能删除
                return array('status'=>false,'msg'=>'任务正在进行中，无法删除');
            }
            # 先删除门店任务列表
            Db::table('cooperative_task_list')->where(array('task_id'=>$taskId))->delete();
            # 删除任务列表
            Db::table('cooperative_task')->where(array('id'=>$taskId))->delete();
            return array('status'=>true);
        }else{
            return array('status'=>false,'msg'=>'系统错误');
        }
    }
}