<?php


namespace app\admin\controller;

use app\service\UeditorService;

/**
 * 百度编辑器控制器入口
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年9月30日13:16:21
 */
class Ueditor extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年9月30日13:16:21
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
       // $this->IsLogin();

    }


    /**
     * 运行入口
     * @author  juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年9月30日13:18:15
     * @desc    description
     */
    public function Index()
    {
        $ret = UeditorService::Run(input());
        if($ret['code'] == 0)
        {
            return json($ret['data']);
        }
        return $ret['msg'];
    }
}