<?php


namespace app\admin\controller;

use app\service\BaseService;

use app\service\RecruitmentService;
use app\service\ResourceService;

/**
 * 招募信息管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月23日09:27:10
 */
class Recruitment extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }
    /**
     * [Index 俱乐部列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {
        if (input('get.action')=='ajax') {
            $params = input();
            // 条件

            $where = RecruitmentService::RecruitmentListWhere($params);
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'recruitment'
            );
            $data=RecruitmentService::RecruitmentList($data_params);

            $total = BaseService::DataTotal('recruitment',$where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [SaveData 俱乐部添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月9日09:39:00
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'recruitment'
            ];

            $data=RecruitmentService::RecruitmentList($data_params);
            $data = empty($data[0]) ? [] : $data[0];
        }

        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function Save()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return RecruitmentService::RecruitmentDataSave($params);
    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='recruitment';
        $params['soft']=false;
        $params['errorcode']=18002;
        $params['msg']='删除失败';

        BaseService::DelInfo($params);
        return DataReturn('删除成功', 0);

    }
}