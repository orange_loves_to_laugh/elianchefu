<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/9 0009
 * Time: 20:11
 */

namespace app\admin\controller;

use think\Controller;
use think\Db;

/**
 * 任务管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2021年1月9日20:13:41
 */
class Crontab extends  Controller
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2021年1月9日20:13:41
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        //$this->IsLogin();
    }
    /**
     * 更改员工状态
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年7月2日10:47:45
     */
    public function ChangeEmployeeStatus(){
        $where=[
            ['employee_status','=',1]
        ];
        $data=Db::name('employee_sal')->where($where)->select();


        foreach ($data as $v)
        {

            if (strtotime($v['regular_time'])<=time())
            {

                Db::name('employee_sal')->where('id='.$v['id'])->update(['employee_status'=>2]);
            }
        }
    }
}