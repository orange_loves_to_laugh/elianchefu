<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 9:37
 */

namespace app\admin\controller;
use app\service\BaseService;
use app\service\InforService;
use app\service\ResourceService;

/**
 * 消息管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月23日09:27:10
 */
class JzInfor extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 消息列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {

        if (input('get.action')=='ajax') {
            $where=InforService::InforListWhere($this->data_post);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'=>'news',

            );
            $data = InforService::InfoList($data_params);

            $total = InforService::InforTotal([['is_del','=',2]]);

           // return DataReturn('获取成功','0',$data);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $this->assign('send_status_arr',lang('send_status'));
            $this->assign('accepter_arr',lang('accepter_type_news'));
            return $this->fetch();
        }

    }


    /**
     * [SaveInfo 消息添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日10:41:31
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id']))
        {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'=>'news'
            ];

            $ret = InforService::InfoList($data_params);
            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        //dump($data);exit;
        //多余图片处理
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'news'.$params['id'];
        ResourceService::delUploadCache();

        $this->assign('send_status_arr',lang('send_status'));
        $this->assign('accepter_arr',lang('accepter_type_news'));
        $this->assign('news_accept',lang('accepter_type_news'));
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function Save()
    {

        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return InforService::InforSave($params);


    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='news';
        $params['soft']=true;
        $params['soft_arr']=['is_del'=>1];
        $params['errorcode']=900;
        $params['msg']='删除失败';
        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);

    }


}