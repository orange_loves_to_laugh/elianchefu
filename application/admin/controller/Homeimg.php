<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 9:39
 */

namespace app\admin\controller;


use app\reuse\controller\ResponseJson;
use app\service\ApplayService;
use app\service\HomeimgService;
use app\service\ResourceService;
use think\Db;

class Homeimg extends Common
{
    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [banner 首页展示图列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function index(){
        $type = input('get.type');
        if(input("get.action")=='ajax'){
            $_where="type >0 and type = $type";
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page'         => true, //是否分页
                'number'         => $listRows, //分页个数
                'where'     => $_where,//查找条件
            );
            $data = HomeimgService::imgIndex($data_params);
            $total= HomeimgService::imgIndexCount();
            return ['code' => 0, 'msg' => '',  'data' => ['data'=>$data,'total'=>$total]];
        }else{
            $this->assign('type',$type);
            return $this->fetch();


        }
    }
    /**
     * [save 图片添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function save(){
        $img_id = input("get.id");
        $type = input("get.type");

        if(input('get.action') == 'ajax'){
            $params = input("post.");
            $params['type'] = $type;
            $data = HomeimgService::Save($params);
            return $data;

        }else{
            if(!empty($img_id)){
                // 获取列表
                $data_params = array(
                    'page'         => false, //是否分页
                    'where'     => array('id'=>$img_id),//查找条件
                );
                $data = HomeimgService::imgIndex($data_params);
                $this->assign('active',$data[0]);
            }
            $this->assign('type',$type);
            return $this->fetch();
        }

    }
    #contetn :删除
    function DelInfo(){
        $id= input("post.id");
        Db::table('home_imgurl')->where(array('id'=>$id))->update(array('type'=>0));
        return array('status'=>true);

    }
}