<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\NoiceService;

/**
 * 通知管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月10日11:14:22
 */
class JzNotice extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();
    }

    /**
     * [Index 通知列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public function Index()
    {
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where = NoiceService::NoticeListWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'notice'
            );
            $data = BaseService::DataList($data_params);
            $data=NoiceService::DataDealWith($data);
            $total = BaseService::DataTotal('notice',$where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{

            $this->assign('show_status',lang('sell_status'));
            $this->assign('accepter',lang('accepter_type_notice'));
            return $this->fetch();
        }
    }

    /**
     * [SaveInfo 添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月10日11:14:22
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table' =>'notice'
            ];

            $ret = BaseService::DataList($data_params);
            $ret=NoiceService::DataDealWith($ret);

            $data = empty($ret[0]) ? [] : $ret[0];
        }

        $this->assign('accepter',lang('accepter_type_notice'));
        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月10日11:14:22
     */
    public function Save()
    {

        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        // 开始操作
        $params = input('post.');

        return NoiceService::NoticeSave($params);

    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo(){

        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        $params=$this->data_post;
        $params['table']='notice';
        $params['soft']=true;
        $params['soft_arr']=['is_del'=>1];
        $params['errorcode']=900;
        $params['msg']='删除失败';
        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);

    }
}