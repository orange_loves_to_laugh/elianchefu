<?php


namespace app\admin\controller;


use app\reuse\controller\ResponseJson;
use app\service\BaseService;
use app\service\RecommendAwardService;
use think\Db;

class RecommendAward extends Common
{
    use ResponseJson;
    function __construct()
    {
        parent::__construct();
        // 登录校验
        $this->IsLogin();
    }

    function index()
    {
        if(input("get.action")=="ajax"){
            $where=[['status',"<>",3]];
            $data_params = array(
                'page'      => true,
                'number'    => 10,
                'where'     => $where,
                'table'     =>'recommend_award',
                'order'     =>'status asc,id desc',
            );
            $data=BaseService::DataList($data_params);
            $total_num = BaseService::DataTotal('recommend_award',$where);
            $data = RecommendAwardService::DataDealWith($data);
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        }else{
            #查询一下 开关状态
            $switch_status = Db::table('switch')->where(array('switch_type' => 'recommendaward'))->value('switch_status');

            $this->assign('switch_status', $switch_status);
            return $this->fetch();
        }
    }

    function saveData()
    {

        if(input("get.action")=="ajax"){
            $param = input("post.");
            $id = $param["id"];
            if(!empty( $id)){
                unset($param['id']);
                $res=Db::table("recommend_award")->where(array("id"=>$id))->update($param);
            }else{
                $res=Db::table("recommend_award")->where(array("id"=>$id))->insert($param);
            }
            if($res){
                return DataReturn('保存成功', 0);
            }else{
                throw new \BaseException(['code'=>403 ,'errorCode'=>4001,'msg'=>'保存失败','status'=>false,'debug'=>false]);
            }
        }else{
            $id = input("get.id");
            if(!empty($id)){
               $data =  Db::table("recommend_award")->where(array("id"=>$id))->find();
               $this->assign("id",$id);
               $this->assign("data",$data);
            }
            return view();
        }

    }

    function DelAward()
    {
        $id = input("post.id");
        Db::table("recommend_award")->where(array("id"=>$id))->update(array("status"=>3));
        return DataReturn('删除成功', 0);
    }
}