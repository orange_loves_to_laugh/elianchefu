<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\DispathService;
use app\service\NoiceService;
use app\service\QuotedPriceService;
use think\Db;


/**
 * 采购管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月6日16:14:52
 */
class JzPurchase  extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月6日16:14:52
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [QuotedPrice 报价列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月9日16:25:57
     */
    public function QuotedPrice()
    {

        if (input('get.action')=='ajax') {
            $params = input();

            // 条件

            $where = QuotedPriceService::QuotedPriceListWhere($params);
//            dump($where);exit;
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'quotedprice'
            );
            $data = BaseService::DataList($data_params);
            $data=QuotedPriceService::DataDealWith($data);

            $total = BaseService::DataTotal('quotedprice',$where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $brands=Db::name('brand')->where('pid=0')->select();
//            dump($brands);exit;
            $this->assign('brands',$brands);
            return $this->fetch();
        }
    }
    /**
     * [SaveQuotedPricePage 修改报价页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月9日16:25:57
     */
    public function SaveQuotedPricePage()
    {

        // 参数
        $params = input();

        // 获取列表
        $data_params = [
            'where'				=> ['id'=>$params['id']],
            'm'					=> 0,
            'n'					=> 1,
            'page'			  => false,
            'table' =>'quotedprice'
        ];

        $ret = BaseService::DataList($data_params);
        $ret=QuotedPriceService::DataDealWith($ret);

        $data = empty($ret[0]) ? [] : $ret[0];

        $this->assign('biz_arr', Db::name('biz')->where('biz_status > 0')->field('biz_title,id')->select());

        $this->assign('brands',Db::name('brand')->where('pid=0')->select());
        $this->assign('data', $data);

        return $this->fetch();
    }
    /**
     * [SaveQuotedPriceData 修改报价]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月9日16:25:57
     */
    public function SaveQuotedPriceData()
    {
        return QuotedPriceService::SaveQuotedPrice($this->data_post);
    }

    /**
     * [BizDispath 门店发货记录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月9日16:25:57
     */
    public function BizDispath(){
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where =  DispathService::ListWhere($params);
            $where[]=['d.type', '=', 1];
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>'d.id,d.biz_id,biz_title,biz_phone,biz_address,biz_type,d.create_time,dispath_num,dispath_price'
            );
            $data = DispathService::DataList($data_params);

            $data=DispathService::DataDealWith($data);

            $total = DispathService::DataTotal($where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $this->assign('biz_type',lang('biz_type'));
            return $this->fetch();
        }
    }
    /**
     * [BrandList 获取车辆型号数据列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月9日16:25:57
     */
    public function BrandList()
    {
        $params=$this->data_request;

        $brands=Db::name('brand')->where('pid='.$params['pid'])->select();

        return DataReturn('ok',0,$brands);

    }


}