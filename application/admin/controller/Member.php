<?php
/**
 * Created by PhpStorm.
 * User: 曹志涛
 * Date: 2020/6/16
 * Time: 9:51
 * Content: 用户管理
 */

namespace app\admin\controller;


use app\api\ApiService\SubsidyService;
use app\service\ActiveService;
use app\service\BaseService;
use app\service\BizDepositService;
use app\service\ConsumpService;

use app\service\DistributionService;
use app\service\MemberService;
use app\service\MemberVoucherService;

use app\service\StatisticMemeberService;
use base\Excel;
use  \BaseException;

use Redis\Redis;
use think\Db;
use think\Exception;

use think\facade\Request;
use think\facade\Session;


class Member extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * @return mixed
     * @throws BaseException
     * @throws Exception
     * @content 用户列表
     */


    function index()
    {

        $where = '';
        $redis = new Redis();
//      dump(Request::param());exit;
        # 姓名手机号车牌号搜索
        $keywords = input('get.keywords');
        if (!empty($keywords)) {

            $where .= " and (m.nickname like '%" . $keywords . "%' or m.member_name  LIKE '%" . $keywords . "%' OR m.member_phone LIKE '%" . $keywords
                . "%' OR mm.member_id in (select member_id from member_car where car_licens like '%" . $keywords . "%'))";
        }
        # 团队人数判断
        $team_id = input("get.team_id");
        if(!empty($team_id)){
            $team_coll = input("get.team_coll");
            $where .=" and mm.member_id in ({$team_coll})";
        }

        #合伙人判断
        $member_type = input('get.member_type');
        if (intval($member_type) > 0) {
            $partner_id_arr = Db::name('member_partner')->where('status=1')->column('member_id');
            $partner_id_arr = join(',', $partner_id_arr);
            $partner_id_arr = strlen($partner_id_arr) < 1 ? 0 : $partner_id_arr;
            // dump($partner_id_arr);exit;
            if ($member_type == 1) {
                $where .= " and mm.member_id in ($partner_id_arr)";
            } else {
                $where .= " and mm.member_id not in ($partner_id_arr)";
            }

        }

        # 会员级别判断
        $member_level = input('get.member_level');
        if (strlen($member_level) > 0) {
            $where .= " and mm.member_level_id =" . $member_level;

        }
        # 会员筛选判断
        $switchStatic = input("get.switchStatic");
        if(!empty($switchStatic)){
            $switchStaticwhere = NULL;
            if($switchStatic<5){
                switch ($switchStatic){
                    case 1 :
                        $switchStaticwhere .= "date(member_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $switchStaticwhere .= "date(member_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $switchStaticwhere .= "date_format(member_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $switchStaticwhere .= "date_format(member_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                $start_time = input("get.start_time");
                if(!empty($start_time)){
                    $switchStaticwhere.="date(member_create)>='".$start_time."'";
                }
                $end_time = input("get.end_time");
                if(!empty($end_time)){
                    $switchStaticwhere.="date(member_create)<='".$end_time."'";
                }
            }
            $loghandle = Db::table('log_handlecard')->where($switchStaticwhere)->column('member_id');
            if (count($loghandle) > 0) {
                $loghandlemember_idarr = join(',', $loghandle);
                $where .= ' and mm.member_id in (' . $loghandlemember_idarr . ')';
            }
        }
        #所在地区
        $member_province = input('get.member_province');
        if (isset($member_province)) {
            $where .= " and m.member_province ='" . $member_province . "'";
        }
        $member_city = input('get.member_city');
        if (isset($member_city)) {
            $where .= " and m.member_city = '" . $member_city . "'";
        }
        $member_area = input('get.member_area');
        if (isset($member_area)) {
            $where .= " and m.member_area = '" . $member_area . "'";
        }

        # 用户性别判断
        $member_sex = input('get.member_sex');
        if (strlen($member_sex) > 0) {
            $where .= " and m.member_sex =" . $member_sex;

        }
        # 办理会员总金额
        $memberPriceArray=array();
        $member_today_total=0;
        # 注册时间(开始时间)
        $registerTime_min = input('get.registerTime_min');
        if (!empty($registerTime_min)) {
            $where .= " and (select count(id) from log_handlecard lh where lh.member_id = mm.member_id and mm.member_level_id>0 and date(lh.member_create) >= '" . $registerTime_min . "') > 0";
            $memberPriceArray['start']=$registerTime_min;
        }
        # 注册时间(结束时间)
        $registerTime_max = input('get.registerTime_max');
        if (!empty($registerTime_max)) {
            $where .= " and (select count(id) from log_handlecard lh where lh.member_id = mm.member_id and mm.member_level_id>0 and date(lh.member_create) <= '" . $registerTime_max . "') > 0 ";
            $memberPriceArray['end']=$registerTime_max;
        }
        if(!empty($registerTime_min) or !empty($registerTime_max)){
            $member_today_total = StatisticMemeberService::MemberHandleTotal(2,$memberPriceArray);
        }

        # 余额最小值
        $balance_min = input('get.balance_min');
        if (!empty($balance_min)) {
            $where .= " and mm.member_balance >= " . $balance_min;
        }
        # 余额最大值
        $balance_max = input('get.balance_max');
        if (!empty($balance_max)) {
            $where .= " and mm.member_balance <= " . $balance_max;
        }

        #member_arr

        $member_arr = input('member_arr');

        if (strlen($member_arr) > 0) {
            $where .= ' and mm.member_id in (' . $member_arr . ')';
        }


        # 当前日期
        $currentDay = date('Y-m-d');
        # mark判断
        $mark = input('get.mark');

        /*
        if (!empty($mark)) {
            if ($mark == 'all') {
                # 所有用户
                $where = '';
            }
            if ($mark == 'vip') {
                # 会员用户
                //$where = "  and  (mm.member_level_id >1 or ((mm.member_level_id =1 or mm.member_level_id =2) and mm.member_expiration >='" . $currentDay . "'))";
                $where_vip=[
                    ['mm.member_level_id','>',0],
                ];
                $data_vip=Db::name('member_mapping')->alias('mm')
                    ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
                    ->where($where_vip)
                    ->select();
                $member_idarr=[];
                foreach ($data_vip as $k=>$v)
                {
                    if ($v['member_level_id']==1||$v['member_level_id']==2)
                    {
                        if (strtotime($v['member_expiration'])<time()) {
                            unset($data_vip[$k]);
                        }else{
                            $member_idarr[$k] =$v['member_id']  ;
                        }
                    }else{
                        $member_idarr[$k] =$v['member_id']  ;
                    }
                }

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }
                unset($where_vip);
                unset($data_vip);
                unset($member_idarr);
            }
            if ($mark == 'partner') {
                $partner_id_arr=Db::name('member_partner')->where('status=1')->column('member_id');
                $partner_id_arr=join(',',$partner_id_arr);
                $partner_id_arr=strlen($partner_id_arr)<1?0:$partner_id_arr;
                # 合伙人
                $where = " and mm.member_id in ($partner_id_arr)";
            }
            if ($mark == 'unregistered') {
                $where = " and (mm.member_code is null or mm.member_code = '')";
            }
            if ($mark == 'app'){
                $where_app = [
                    ['integral_source', '=', 22],
                    ['m.id', '>', 0],
                ];
//                $member_idarr=Db::name('log_integral')->where($where_app)
//                    ->group('member_id')
//                    ->column('member_id');
                $member_idarr=Db::name('log_integral')->alias('li')
                    ->leftJoin(['member'=>'m'],'m.id=li.member_id')
                    ->where($where_app)
                    ->column('m.id');

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }

                // unset($member_idarr);
            }
            if ($mark == 'authentication'){
                $where_auth = [
                    ['mm.member_status', '=',2],
                ];
                $member_idarr=Db::name('member_mapping')->alias('mm')->where($where_auth)->column('member_id');

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }

                unset($member_idarr);
            }
        }
        */
        # markdata  ：1新增用户 2总用户数 3今日浏览量 4总会员量 5新增会员量 6实名认证数量

        $markdata = input('get.markdata');

        //register_time 排序
        $register_time_order = Request::param('register_time');
        $order = ['mm.id' => 'desc'];

        if (isset($register_time_order)) {
            $order = ['m.register_time' => $register_time_order];
        }

        if (!empty($markdata) and empty($registerTime_min) and empty($registerTime_max)) {
//            $where='';
            if ($markdata == 1) {
                #今日新增用户

                $member_idarr = Db::table('member')
                    ->whereTime('register_time', 'today')
                    ->column('id');

                $where_login = [
                    ['mm.register_time', '<', '2021-01-15'],
                    ['ml.create_time', '>', date('Y-m-d 00:00:00')],
                    ['ml.create_time', '<', TIMESTAMP],
                ];
                $login_arr = Db::table('member_login_log')->alias('ml')
                    ->leftJoin(['member' => 'mm'], 'mm.id=ml.member_id')
                    ->where($where_login)
                    ->column('ml.member_id');


                foreach ($login_arr as $k => $v) {

                    $re = Db::table('member_login_log')->where('member_id=' . $v)->count();
                    if ($re > 1) {
                        unset($login_arr[$k]);
                    }
                }

                $member_idarr = array_merge($member_idarr, $login_arr);

                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', $member_idarr);
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }

                unset($member_idarr);
//                $memberCreate = 'member_create';
//                $today = 'today';
            } else if ($markdata == 2) {
                #总用户数量

                $where .= " and mm.member_status != 0";

            } else if ($markdata == 3) {
                #查询今日浏览量中的 用户id
                $memberId = StatisticMemeberService::memberString();
                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 4) {
                #总会员数量


                $data = Db::name('member_mapping')->where([['member_level_id', '>', 0]])->field('member_level_id,member_expiration,member_id')->select();

                $member_idarr = [];

                foreach ($data as $k => $v) {
                    if ($v['member_level_id'] == 1 || $v['member_level_id'] == 2) {
                        //if (strtotime($v['member_expiration']) < time()) {
                        if ($v['member_expiration'] < date("Y-m-d")) {
                            unset($data[$k]);
                        } else {
                            $member_idarr[$k] = $v['member_id'];
                        }
                    } else {
                        $member_idarr[$k] = $v['member_id'];
                    }
                }

                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', array_filter($member_idarr));
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }

                unset($member_idarr);

            } else if ($markdata == 5) {
                #今日新增会员
                $where_newvip = [
                    ['member_create', '>', date('Y-m-d 00:00:00')],
                    ['member_create', '<', TIMESTAMP],
                ];
                $member_idarr = Db::table('log_handlecard')->where($where_newvip)->column('member_id');
                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', array_filter($member_idarr));
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }
//                $where .= " and mm.member_level_id > 0";
//                $memberCreate = 'member_create';
//                $today = 'today';
//                $_whereTime = whereTime('member_create', 'today');

                $member_today_total = StatisticMemeberService::MemberHandleTotal(1);
            } else if ($markdata == 6) {
                #实名认证会员
                $where .= ' and mm.member_status=2';
            } else if ($markdata == 7) {
                #到店会员办理的用户
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");
                $memberId = StatisticMemeberService::memberCardString($biz_id, $cardmark);
                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 8) {
                #到店用户
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");

                $memberId = StatisticMemeberService::memberCardString($biz_id, $cardmark);

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 9) {
                #到店会员
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");
                if ($cardmark == 3 or $cardmark == 5) {
                    #今日到店顾客数   #今日到店会员
                    $day = 'today';
                } else {
                    #昨日
                    $day = 'yesterday';

                }
                $memberId_arr = Db::table('biz_traffic')->alias('bt')
                    ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=bt.member_id')
                    ->where([['bt.biz_id', '=', $biz_id], ['mm.member_level_id', '>', 0], ['member_expiration', '>', TIMESTAMP]])
                    ->whereTime('bt.create_time', $day)
                    ->field('bt.member_id,mm.member_level_id')
                    ->group('concat(mm.member_id,bt.biz_id)')
                    ->column('mm.member_id');

                if (!empty($memberId_arr)) {
                    $memberId = join(',', $memberId_arr);
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
                unset($memberId_arr);
                unset($cardmark);
            } else if ($markdata == 10) {
                #办理会员
                $cardmark = input("get.mark");
                if ($cardmark == 3 or $cardmark == 5) {
                    #今日到店顾客数   #今日到店会员
                    $day = 'today';
                } else {
                    #昨日
                    $day = 'yesterday';

                }
                $biz_id = input("get.biz_id");
                $memberId_arr = Db::name('log_handlecard')
                    ->whereTime('member_create', $day)
                    ->where(array('biz_id' => $biz_id))
                    ->column('member_id');
                if (!empty($memberId_arr)) {
                    $memberId = join(',', $memberId_arr);
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
                unset($memberId_arr);
                unset($cardmark);
            }else if($markdata == 11){
                # 今日下载app人数
                $where .= " and mm.member_id in (select member_id from log_integral where integral_source=22 and date(integral_time)='".date("Y-m-d")."')";
            } else if ($markdata == 'bizMember') {
                #对应门店的会员 数量  从合作门店点击门店关联会员 查询的
                $biz_id = input("get.bizId");
                #到店会员办理的用户
                $memberId = StatisticMemeberService::BizMembertring($biz_id);

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }

            } else {
                #超时未到店会员
                $data = Db::table('orders')
                    ->where(['order_status' => 3, 'order_type' => 3])
                    ->whereTime('order_create', 'today')
                    ->group('member_id')
                    ->column('member_id');
                $data = StatisticMemeberService::BizOrdersInfoDetail($data);
                #返回的是数组  获取当中需要的字符串
                $memberId = $data['cardMenber'];

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where = " and mm.member_id < 0";

                }
            }
        }
        if (!empty($mark)) {
            $member_arr = $redis->hGet('memberarr', $mark);


            if ($member_arr) {
                $where .= ' and m.id in (' . $member_arr . ')';

            }

        }
        $propertyMark = input("get.propertyMark");
        if(!empty($propertyMark)){
            $property_id = input("get.property_id");
            $where.= " and m.id in (select distinct ph.member_id from property_memberhouse ph where ph.nh_id = {$property_id} and ph.del_status=2 and ph.member_id>0)";
        }

        /*
        if($markdata == 1 or $markdata == 5)
        {
            try {
                # 用户列表
                $field='m.id,m.member_name,m.nickname,m.member_sex sex,m.member_age age,
                        mm.member_code code,m.member_phone phone,
                        (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
                        mm.member_balance balance,mm.member_level_id levelId,
                        if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
                        mm.register_time,mm.member_type';
                $info = Db::table('member_mapping mm')
                    ->field($field)
                    ->join('member m', 'm.id=mm.member_id', 'left')
                    ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
                    ->where('m.id >0' . $where)
//                    ->whereTime($memberCreate,$today)
                    ->order($order)

                    ->paginate(10, false, ['query' => request()->param()]);


            } catch (Exception $e) {
                throw new \BaseException(array('code' => 403, 'msg' => '用户列表查询错误', 'errorcode' => 'ms001', 'status' => false,'debug'=>false));
            }
            # 列表统计
            try {

                $statistics = Db::table('member_mapping mm')
                    ->field('count(mm.id) memberCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2 ' . $where . ' ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id =1 and member_expiration >= "' . $currentDay . '" ' . $where . ') vip1Count,

            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
            ')
                    ->join('member m', 'm.id=mm.member_id', 'left')
                    ->where('mm.id > 0' . $where)
//                    ->whereTime($memberCreate,$today)
                    ->find();

                //$statistics['authenticationCount']= Db::name('member_mapping')->where('member_status = 2')->count();

            } catch (Exception $e) {
                throw new \BaseException(array('code' => 403, 'msg' => '用户列表统计查询错误', 'errorCode' => 'ms002', 'status' => false,'debug'=>false));
            }
        }
        else{
            # 用户列表
            $field='m.nickname,m.id,m.member_name,
            m.member_sex sex,m.member_age age,mm.member_code code,m.member_phone phone,
            (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
            mm.member_balance balance,mm.member_level_id levelId,if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
            mm.register_time';
            $info = Db::table('member_mapping mm')
                ->field($field)
                ->join('member m', 'm.id=mm.member_id', 'left')
                ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
                ->where('m.id >0' . $where)
                ->order($order)
                //->select();
                ->paginate(10, false, ['query' => request()->param()]);

            # 列表统计
            $statistics = Db::table('member_mapping mm')
                ->field('count(mm.id) memberCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2 ' . $where . ' ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where (mm.member_level_id =1 or mm.member_level_id =2) and member_expiration >= "' . $currentDay . '" ' . $where . ') vip1Count,

            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
            ')
                ->join('member m', 'm.id=mm.member_id', 'left')
                ->where('mm.id > 0' . $where)
                ->find();
//
            $statistics['partnerCount']=Db::name('member_partner')->where('status = 1')->count();
            $statistics['useAppCount']=Db::name('member')->where('use_app = 1')->count();
            $statistics['authenticationCount']=Db::name('member')->where('length(member_idcard) > 0')->count();
        }
        */

        # 用户列表
        $field_info = 'm.nickname,m.id,m.member_name,m.toutiao_name,
            m.member_sex sex,m.member_age age,mm.member_code code,m.member_phone phone,
            (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
            mm.member_balance balance,mm.member_level_id levelId,if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
            m.register_time';

        $info = Db::table('member_mapping mm')
            ->field($field_info)
            ->join('member m', 'm.id=mm.member_id', 'left')
            ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
            //->join("log_handlecard lh","mm.member_id = lh.member_id",'left')
            ->where('m.id > 0 ' . $where)
            ->order($order)
            //->select();
            //
            ->paginate(10, false, ['query' => request()->param()]);



        # 列表统计
        $field_statistics = 'count(mm.id) memberCount';
        /*
         * (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2  ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where (mm.member_level_id =1 or mm.member_level_id =2) and member_expiration >= "' . $currentDay . '" ) vip1Count

         */
        //用户总量

        $statistics = Db::table('member_mapping mm')
            ->field($field_statistics)
            ->join('member m', 'm.id=mm.member_id', 'left')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where('mm.id > 0' . $where)
            ->find();


        $memberCount_arr = Db::table('member_mapping mm')
            ->join('member m', 'm.id=mm.member_id', 'left')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where('mm.id > 0' . $where)
            ->column('mm.member_id');

        $redis->hSet('memberarr', 'all', join(',', array_filter($memberCount_arr)));


        //会员总量
        $where_vip = [
            ['mm.member_level_id', '>', 0],
        ];
        $data_vip = Db::name('member_mapping')->alias('mm')
            ->leftJoin(['member' => 'm'], 'mm.member_id=m.id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_vip)
            ->where('mm.id > 0' . $where)
            ->select();

        $vipCount_arr = [];
        foreach ($data_vip as $k => $v) {
            if ($v['member_level_id'] == 1 || $v['member_level_id'] == 2) {
                //if (strtotime($v['member_expiration']) < time()) {
                if ($v['member_expiration'] < date("Y-m-d")) {
                    unset($data_vip[$k]);
                } else {
                    $vipCount_arr[$k] = $v['member_id'];
                }
            } else {
                $vipCount_arr[$k] = $v['member_id'];
            }
        }
        $statistics['vipCount'] = count($data_vip);
        $redis->hSet('memberarr', 'vip', join(',', array_filter($vipCount_arr)));


        //(select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
        //dump($statistics);exit;

        //未注册
        $where_reg = 'length(mm.member_code)<1';

//        $statistics['unregisteredCount']=Db::name('member_mapping')->alias('mm')
//            ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
//            ->where($where_reg)
//            ->where('mm.id > 0'.$where)
//
//            ->select();

        $unregisteredCount_arr = Db::name('member_mapping')->alias('mm')
            ->leftJoin(['member' => 'm'], 'mm.member_id=m.id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_reg)
            ->where('mm.id > 0' . $where)->column('m.id');


        $statistics['unregisteredCount'] = count($unregisteredCount_arr);

        $redis->hSet('memberarr', 'unregistered', join(',', array_filter($unregisteredCount_arr)));

        //合伙人数量
        $where_partner = [
            ['mp.status', '=', 1],
        ];

        $statistics['partnerCount'] = Db::name('member_partner')->alias('mp')
            ->join(['member_mapping' => 'mm'], 'mm.member_id=mp.member_id')
            ->join(['member' => 'm'], 'm.id=mp.member_id')
           // ->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_partner)
            ->where('mp.id > 0 ' . $where)
            ->count('mp.id');

        $partnerCount_arr = Db::name('member_partner')->alias('mp')
            ->join(['member_mapping' => 'mm'], 'mm.member_id=mp.member_id')
            ->join(['member' => 'm'], 'm.id=mp.member_id')
            ->where($where_partner)->column('m.id');

        $redis->hSet('memberarr', 'partner', join(',', array_filter($partnerCount_arr)));


        //app下载数量
        $where_app = [
            ['integral_source', '=', 22],
            ['m.id', '>', 0],
        ];

        $statistics['useAppCount'] = Db::name('log_integral')->alias('li')
            ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=li.member_id')
            ->leftJoin(['member' => 'm'], 'm.id=li.member_id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_app)
            ->where('li.id > 0' . $where)
            ->count('li.id');
        //dump($statistics['useAppCount']);exit;
        //->count('li.id');

        $useAppCount = Db::name('log_integral')->alias('li')
            ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=li.member_id')
            ->leftJoin(['member' => 'm'], 'm.id=li.member_id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_app)
            ->where('li.id > 0' . $where)->column('m.id');

        $redis->hSet('memberarr', 'app', join(',', array_filter($useAppCount)));


        //实名认证
        $where_auth = [
            ['mm.member_status', '=', 2],
        ];

        $statistics['authenticationCount'] = Db::name('member_mapping')->alias('mm')
            ->join(['member' => 'm'], 'm.id=mm.member_id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_auth)
            ->where('mm.id > 0' . $where)
            ->count();

        $authenticationCount = Db::name('member_mapping')->alias('mm')
            ->join(['member' => 'm'], 'm.id=mm.member_id')
            //->join("log_handlecard lh","lh.member_id = mm.member_id","left")
            ->where($where_auth)
            ->where('mm.id > 0' . $where)->column('m.id');

        $redis->hSet('memberarr', 'authentication', join(',', array_filter($authenticationCount)));


        $page = $info->render();

        $_info = $info->toArray();

        foreach ($_info['data'] as &$v) {
            if (empty($v['name'])) {
                $v['name'] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $v['name'] = CheckBase64($v['name']);
                if(empty($v['name']) and !empty($v['toutiao_name'])){
                    $v['name'] = $v['toutiao_name'];
                }
                $v['name'] = empty($v['name']) ? '未获取到用户名' : $v['name'];
                $v['balance'] = round($v['balance'], 3);
//                $v['channel']=Db::name('channel')->where('member_id='.$v['id'])->order('id desc')->find();
            }
        }

        session('excel_member_list', $_info['data']);

        $this->assign('member_sex_id', $member_sex);
        $this->assign('member_type_id', $member_type);
        $this->assign('member_level_id', $member_level);

        $this->assign('member_type', lang('member_type'));
        $this->assign('member_sex', lang('member_sex'));
        $this->assign('member_level', Db::name('member_level')->field('id,level_title')->select());
        $this->assign('register_time', $register_time_order);

        $this->assign('page', $page);
        $this->assign('info', $_info['data']);
        $this->assign('statistics', $statistics);
        $this->assign('keywords', $keywords);
        $this->assign('registerTime_min', $registerTime_min);
        $this->assign('registerTime_max', $registerTime_max);
        $this->assign('member_today_total', $member_today_total);
        $this->assign('balance_min', $balance_min);
        $this->assign('balance_max', $balance_max);

        $this->assign('markdata', $markdata);
        $this->assign('mark', $mark);
        $this->assign('cardmark', $cardmark);
        $this->assign('biz_id', $biz_id);
        return $this->fetch('index');
    }

    function index_old()
    {

        $where = '';
        $redis = new Redis();
//      dump(Request::param());exit;
        # 姓名手机号车牌号搜索
        $keywords = input('get.keywords');
        if (!empty($keywords)) {

            $where .= " and (from_base64(m.nickname) like '%" . $keywords . "%' or m.member_name  LIKE '%" . $keywords . "%' OR m.member_phone LIKE '%" . $keywords
                . "%' OR mm.member_id in (select member_id from member_car where car_licens like '%" . $keywords . "%'))";
        }

        #合伙人判断
        $member_type = input('get.member_type');
        if (intval($member_type) > 0) {
            $partner_id_arr = Db::name('member_partner')->where('status=1')->column('member_id');
            $partner_id_arr = join(',', $partner_id_arr);
            $partner_id_arr = strlen($partner_id_arr) < 1 ? 0 : $partner_id_arr;
            // dump($partner_id_arr);exit;
            if ($member_type == 1) {
                $where .= " and mm.member_id in ($partner_id_arr)";
            } else {
                $where .= " and mm.member_id not in ($partner_id_arr)";
            }

        }

        # 会员级别判断
        $member_level = input('get.member_level');
        if (strlen($member_level) > 0) {
            $where .= " and mm.member_level_id =" . $member_level;

        }
        #所在地区
        $member_province = input('get.member_province');
        if (isset($member_province)) {
            $where .= " and m.member_province ='" . $member_province . "'";
        }
        $member_city = input('get.member_city');
        if (isset($member_city)) {
            $where .= " and m.member_city = '" . $member_city . "'";
        }
        $member_area = input('get.member_area');
        if (isset($member_area)) {
            $where .= " and m.member_area = '" . $member_area . "'";
        }

        # 用户性别判断
        $member_sex = input('get.member_sex');
        if (strlen($member_sex) > 0) {
            $where .= " and m.member_sex =" . $member_sex;

        }

        # 注册时间(开始时间)
        $registerTime_min = input('get.registerTime_min');
        if (!empty($registerTime_min)) {
            $where .= " and m.register_time >= '" . $registerTime_min . "'";
        }
        # 注册时间(结束时间)
        $registerTime_max = input('get.registerTime_max');
        if (!empty($registerTime_max)) {
            $where .= " and m.register_time <= '" . $registerTime_max . "'";
        }
        # 余额最小值
        $balance_min = input('get.balance_min');
        if (!empty($balance_min)) {
            $where .= " and mm.member_balance >= " . $balance_min;
        }
        # 余额最大值
        $balance_max = input('get.balance_max');
        if (!empty($balance_max)) {
            $where .= " and mm.member_balance <= " . $balance_max;
        }

        #member_arr

        $member_arr = input('member_arr');

        if (strlen($member_arr) > 0) {
            $where .= ' and mm.member_id in (' . $member_arr . ')';
        }


        # 当前日期
        $currentDay = date('Y-m-d');
        # mark判断
        $mark = input('get.mark');

        /*
        if (!empty($mark)) {
            if ($mark == 'all') {
                # 所有用户
                $where = '';
            }
            if ($mark == 'vip') {
                # 会员用户
                //$where = "  and  (mm.member_level_id >1 or ((mm.member_level_id =1 or mm.member_level_id =2) and mm.member_expiration >='" . $currentDay . "'))";
                $where_vip=[
                    ['mm.member_level_id','>',0],
                ];
                $data_vip=Db::name('member_mapping')->alias('mm')
                    ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
                    ->where($where_vip)
                    ->select();
                $member_idarr=[];
                foreach ($data_vip as $k=>$v)
                {
                    if ($v['member_level_id']==1||$v['member_level_id']==2)
                    {
                        if (strtotime($v['member_expiration'])<time()) {
                            unset($data_vip[$k]);
                        }else{
                            $member_idarr[$k] =$v['member_id']  ;
                        }
                    }else{
                        $member_idarr[$k] =$v['member_id']  ;
                    }
                }

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }
                unset($where_vip);
                unset($data_vip);
                unset($member_idarr);
            }
            if ($mark == 'partner') {
                $partner_id_arr=Db::name('member_partner')->where('status=1')->column('member_id');
                $partner_id_arr=join(',',$partner_id_arr);
                $partner_id_arr=strlen($partner_id_arr)<1?0:$partner_id_arr;
                # 合伙人
                $where = " and mm.member_id in ($partner_id_arr)";
            }
            if ($mark == 'unregistered') {
                $where = " and (mm.member_code is null or mm.member_code = '')";
            }
            if ($mark == 'app'){
                $where_app = [
                    ['integral_source', '=', 22],
                    ['m.id', '>', 0],
                ];
//                $member_idarr=Db::name('log_integral')->where($where_app)
//                    ->group('member_id')
//                    ->column('member_id');
                $member_idarr=Db::name('log_integral')->alias('li')
                    ->leftJoin(['member'=>'m'],'m.id=li.member_id')
                    ->where($where_app)
                    ->column('m.id');

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }

                // unset($member_idarr);
            }
            if ($mark == 'authentication'){
                $where_auth = [
                    ['mm.member_status', '=',2],
                ];
                $member_idarr=Db::name('member_mapping')->alias('mm')->where($where_auth)->column('member_id');

                if (count($member_idarr)>0)
                {
                    $member_idarr=join(',',$member_idarr);
                    $where = ' and mm.member_id in ('.$member_idarr.')';
                }else{
                    $where = ' and mm.member_id in (0)';
                }

                unset($member_idarr);
            }
        }
        */
        # markdata  ：1新增用户 2总用户数 3今日浏览量 4总会员量 5新增会员量 6实名认证数量

        $markdata = input('get.markdata');

        //register_time 排序
        $register_time_order = Request::param('register_time');
        $order = ['mm.id' => 'desc'];
        if (isset($register_time_order)) {
            $order = ['m.register_time' => $register_time_order];
        }


        if (!empty($markdata)) {
//            $where='';
            if ($markdata == 1) {
                #今日新增用户

                $member_idarr = Db::table('member')
                    ->whereTime('register_time', 'today')
                    ->column('id');

                $where_login = [
                    ['mm.register_time', '<', '2021-01-15'],
                    ['ml.create_time', '>', date('Y-m-d 00:00:00')],
                    ['ml.create_time', '<', TIMESTAMP],
                ];
                $login_arr = Db::table('member_login_log')->alias('ml')
                    ->leftJoin(['member' => 'mm'], 'mm.id=ml.member_id')
                    ->where($where_login)
                    ->column('ml.member_id');


                foreach ($login_arr as $k => $v) {

                    $re = Db::table('member_login_log')->where('member_id=' . $v)->count();
                    if ($re > 1) {
                        unset($login_arr[$k]);
                    }
                }

                $member_idarr = array_merge($member_idarr, $login_arr);

                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', $member_idarr);
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }

                unset($member_idarr);
//                $memberCreate = 'member_create';
//                $today = 'today';
            } else if ($markdata == 2) {
                #总用户数量

                $where .= " and mm.member_status != 0";

            } else if ($markdata == 3) {
                #查询今日浏览量中的 用户id
                $memberId = StatisticMemeberService::memberString();
                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 4) {
                #总会员数量


                $data = Db::name('member_mapping')->where([['member_level_id', '>', 0]])->field('member_level_id,member_expiration,member_id')->select();

                $member_idarr = [];

                foreach ($data as $k => $v) {
                    if ($v['member_level_id'] == 1 || $v['member_level_id'] == 2) {
                        if (strtotime($v['member_expiration']) < time()) {
                            unset($data[$k]);
                        } else {
                            $member_idarr[$k] = $v['member_id'];
                        }
                    } else {
                        $member_idarr[$k] = $v['member_id'];
                    }
                }

                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', $member_idarr);
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }

                unset($member_idarr);

            } else if ($markdata == 5) {
                #今日新增会员
                $where_newvip = [
                    ['member_create', '>', date('Y-m-d 00:00:00')],
                    ['member_create', '<', TIMESTAMP],
                ];
                $member_idarr = Db::table('log_handlecard')->where($where_newvip)->column('member_id');
                if (count($member_idarr) > 0) {
                    $member_idarr = join(',', $member_idarr);
                    $where .= ' and mm.member_id in (' . $member_idarr . ')';
                } else {
                    $where .= ' and mm.member_id in (0)';
                }
//                $where .= " and mm.member_level_id > 0";
//                $memberCreate = 'member_create';
//                $today = 'today';
//                $_whereTime = whereTime('member_create', 'today');


            } else if ($markdata == 6) {
                #实名认证会员
                $where .= ' and mm.member_status=2';
            } else if ($markdata == 7) {
                #到店会员办理的用户
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");
                $memberId = StatisticMemeberService::memberCardString($biz_id, $cardmark);
                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 8) {
                #到店用户
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");

                $memberId = StatisticMemeberService::memberCardString($biz_id, $cardmark);

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
            } else if ($markdata == 9) {
                #到店会员
                $cardmark = input("get.mark");
                $biz_id = input("get.biz_id");
                if ($cardmark == 3 or $cardmark == 5) {
                    #今日到店顾客数   #今日到店会员
                    $day = 'today';
                } else {
                    #昨日
                    $day = 'yesterday';

                }
                $memberId_arr = Db::table('biz_traffic')->alias('bt')
                    ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=bt.member_id')
                    ->where([['bt.biz_id', '=', $biz_id], ['mm.member_level_id', '>', 0], ['member_expiration', '>', TIMESTAMP]])
                    ->whereTime('bt.create_time', $day)
                    ->field('bt.member_id,mm.member_level_id')
                    ->group('concat(mm.member_id,bt.biz_id)')
                    ->column('mm.member_id');

                if (!empty($memberId_arr)) {
                    $memberId = join(',', $memberId_arr);
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
                unset($memberId_arr);
                unset($cardmark);
            } else if ($markdata == 10) {
                #办理会员
                $cardmark = input("get.mark");
                if ($cardmark == 3 or $cardmark == 5) {
                    #今日到店顾客数   #今日到店会员
                    $day = 'today';
                } else {
                    #昨日
                    $day = 'yesterday';

                }
                $biz_id = input("get.biz_id");
                $memberId_arr = Db::name('log_handlecard')
                    ->whereTime('member_create', $day)
                    ->where(array('biz_id' => $biz_id))
                    ->column('member_id');
                if (!empty($memberId_arr)) {
                    $memberId = join(',', $memberId_arr);
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }
                unset($memberId_arr);
                unset($cardmark);
            } else if ($markdata == 'bizMember') {
                #对应门店的会员 数量  从合作门店点击门店关联会员 查询的
                $biz_id = input("get.bizId");
                #到店会员办理的用户
                $memberId = StatisticMemeberService::BizMembertring($biz_id);

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where .= " and mm.member_id < 0";

                }

            } else {
                #超时未到店会员
                $data = Db::table('orders')
                    ->where(['order_status' => 3, 'order_type' => 3])
                    ->whereTime('order_create', 'today')
                    ->group('member_id')
                    ->column('member_id');
                $data = StatisticMemeberService::BizOrdersInfoDetail($data);
                #返回的是数组  获取当中需要的字符串
                $memberId = $data['cardMenber'];

                if (!empty($memberId)) {
                    $where .= " and mm.member_id in ($memberId)";

                } else {
                    $where = " and mm.member_id < 0";

                }
            }
        }

        if (!empty($mark)) {
            $member_arr = $redis->hGet('memberarr', $mark);

            if ($member_arr) {
                $where .= ' and m.id in (' . $member_arr . ')';

            }

        }


        /*
        if($markdata == 1 or $markdata == 5)
        {
            try {
                # 用户列表
                $field='m.id,m.member_name,m.nickname,m.member_sex sex,m.member_age age,
                        mm.member_code code,m.member_phone phone,
                        (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
                        mm.member_balance balance,mm.member_level_id levelId,
                        if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
                        mm.register_time,mm.member_type';
                $info = Db::table('member_mapping mm')
                    ->field($field)
                    ->join('member m', 'm.id=mm.member_id', 'left')
                    ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
                    ->where('m.id >0' . $where)
//                    ->whereTime($memberCreate,$today)
                    ->order($order)

                    ->paginate(10, false, ['query' => request()->param()]);


            } catch (Exception $e) {
                throw new \BaseException(array('code' => 403, 'msg' => '用户列表查询错误', 'errorcode' => 'ms001', 'status' => false,'debug'=>false));
            }
            # 列表统计
            try {

                $statistics = Db::table('member_mapping mm')
                    ->field('count(mm.id) memberCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2 ' . $where . ' ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id =1 and member_expiration >= "' . $currentDay . '" ' . $where . ') vip1Count,

            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
            ')
                    ->join('member m', 'm.id=mm.member_id', 'left')
                    ->where('mm.id > 0' . $where)
//                    ->whereTime($memberCreate,$today)
                    ->find();

                //$statistics['authenticationCount']= Db::name('member_mapping')->where('member_status = 2')->count();

            } catch (Exception $e) {
                throw new \BaseException(array('code' => 403, 'msg' => '用户列表统计查询错误', 'errorCode' => 'ms002', 'status' => false,'debug'=>false));
            }
        }
        else{
            # 用户列表
            $field='m.nickname,m.id,m.member_name,
            m.member_sex sex,m.member_age age,mm.member_code code,m.member_phone phone,
            (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
            mm.member_balance balance,mm.member_level_id levelId,if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
            mm.register_time';
            $info = Db::table('member_mapping mm')
                ->field($field)
                ->join('member m', 'm.id=mm.member_id', 'left')
                ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
                ->where('m.id >0' . $where)
                ->order($order)
                //->select();
                ->paginate(10, false, ['query' => request()->param()]);

            # 列表统计
            $statistics = Db::table('member_mapping mm')
                ->field('count(mm.id) memberCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2 ' . $where . ' ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where (mm.member_level_id =1 or mm.member_level_id =2) and member_expiration >= "' . $currentDay . '" ' . $where . ') vip1Count,

            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
            ')
                ->join('member m', 'm.id=mm.member_id', 'left')
                ->where('mm.id > 0' . $where)
                ->find();
//
            $statistics['partnerCount']=Db::name('member_partner')->where('status = 1')->count();
            $statistics['useAppCount']=Db::name('member')->where('use_app = 1')->count();
            $statistics['authenticationCount']=Db::name('member')->where('length(member_idcard) > 0')->count();
        }
        */

        # 用户列表
        $field_info = 'm.nickname,m.id,m.member_name,
            m.member_sex sex,m.member_age age,mm.member_code code,m.member_phone phone,
            (select car_licens from member_car mc where mc.member_id=m.id limit 1) license,
            mm.member_balance balance,mm.member_level_id levelId,if(mm.member_level_id >0,level_title,"普通用户") levelTitle,
            m.register_time';

        $info = Db::table('member_mapping mm')
            ->field($field_info)
            ->join('member m', 'm.id=mm.member_id', 'left')
            ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
            ->where('m.id > 0 ' . $where)
            ->order($order)
            //->select();
            //
            ->paginate(10, false, ['query' => request()->param()]);


        # 列表统计
        $field_statistics = 'count(mm.id) memberCount';
        /*
         * (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2  ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where (mm.member_level_id =1 or mm.member_level_id =2) and member_expiration >= "' . $currentDay . '" ) vip1Count

         */
        //用户总量

        $statistics = Db::table('member_mapping mm')
            ->field($field_statistics)
            ->join('member m', 'm.id=mm.member_id', 'left')
            ->where('mm.id > 0' . $where)
            ->find();


        $memberCount_arr = Db::table('member_mapping mm')
            ->join('member m', 'm.id=mm.member_id', 'left')
            ->where('mm.id > 0' . $where)
            ->column('m.id');

        $redis->hSet('memberarr', 'all', join(',', $memberCount_arr));


        //会员总量
        $where_vip = [
            ['mm.member_level_id', '>', 0],
        ];
        $data_vip = Db::name('member_mapping')->alias('mm')
            ->leftJoin(['member' => 'm'], 'mm.member_id=m.id')
            ->where($where_vip)
            ->where('mm.id > 0' . $where)
            ->select();

        $vipCount_arr = [];
        foreach ($data_vip as $k => $v) {
            if ($v['member_level_id'] == 1 || $v['member_level_id'] == 2) {
                if (strtotime($v['member_expiration']) < time()) {
                    unset($data_vip[$k]);
                } else {
                    $vipCount_arr[$k] = $v['member_id'];
                }
            } else {
                $vipCount_arr[$k] = $v['member_id'];
            }
        }
        $statistics['vipCount'] = count($data_vip);

        $redis->hSet('memberarr', 'vip', join(',', $vipCount_arr));


        //(select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_code is null or mm.member_code = "" ' . $where . ') unregisteredCount
        //dump($statistics);exit;

        //未注册
        $where_reg = 'length(mm.member_code)<1';

//        $statistics['unregisteredCount']=Db::name('member_mapping')->alias('mm')
//            ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
//            ->where($where_reg)
//            ->where('mm.id > 0'.$where)
//
//            ->select();

        $unregisteredCount_arr = Db::name('member_mapping')->alias('mm')
            ->leftJoin(['member' => 'm'], 'mm.member_id=m.id')
            ->where($where_reg)
            ->where('mm.id > 0' . $where)->column('m.id');


        $statistics['unregisteredCount'] = count($unregisteredCount_arr);

        $redis->hSet('memberarr', 'unregistered', join(',', $unregisteredCount_arr));

        //合伙人数量
        $where_partner = [
            ['mp.status', '=', 1],
        ];

        $statistics['partnerCount'] = Db::name('member_partner')->alias('mp')
            ->join(['member_mapping' => 'mm'], 'mm.member_id=mp.member_id')
            ->join(['member' => 'm'], 'm.id=mp.member_id')
            ->where($where_partner)
            ->where('mp.id > 0 ' . $where)
            ->count('mp.id');

        $partnerCount_arr = Db::name('member_partner')->alias('mp')
            ->join(['member_mapping' => 'mm'], 'mm.member_id=mp.member_id')
            ->join(['member' => 'm'], 'm.id=mp.member_id')
            ->where($where_partner)->column('m.id');

        $redis->hSet('memberarr', 'partner', join(',', $partnerCount_arr));


        //app下载数量
        $where_app = [
            ['integral_source', '=', 22],
            ['m.id', '>', 0],
        ];

        $statistics['useAppCount'] = Db::name('log_integral')->alias('li')
            ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=li.member_id')
            ->leftJoin(['member' => 'm'], 'm.id=li.member_id')
            ->where($where_app)
            ->where('li.id > 0' . $where)
            ->count('li.id');
        //dump($statistics['useAppCount']);exit;
        //->count('li.id');

        $useAppCount = Db::name('log_integral')->alias('li')
            ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=li.member_id')
            ->leftJoin(['member' => 'm'], 'm.id=li.member_id')
            ->where($where_app)
            ->where('li.id > 0' . $where)->column('m.id');

        $redis->hSet('memberarr', 'app', join(',', $useAppCount));


        //实名认证
        $where_auth = [
            ['mm.member_status', '=', 2],
        ];

        $statistics['authenticationCount'] = Db::name('member_mapping')->alias('mm')
            ->join(['member' => 'm'], 'm.id=mm.member_id')
            ->where($where_auth)
            ->where('mm.id > 0' . $where)
            ->count();

        $authenticationCount = Db::name('member_mapping')->alias('mm')
            ->join(['member' => 'm'], 'm.id=mm.member_id')
            ->where($where_auth)
            ->where('mm.id > 0' . $where)->column('m.id');

        $redis->hSet('memberarr', 'authentication', join(',', $authenticationCount));


        $page = $info->render();

        $_info = $info->toArray();

        foreach ($_info['data'] as &$v) {
            if (empty($v['name'])) {
                $v['name'] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $v['name'] = CheckBase64($v['name']);
                $v['name'] = empty($v['name']) ? '未获取到用户名' : $v['name'];
                $v['balance'] = round($v['balance'], 3);
//                $v['channel']=Db::name('channel')->where('member_id='.$v['id'])->order('id desc')->find();
            }
        }

        session('excel_member_list', $_info['data']);

        $this->assign('member_sex_id', $member_sex);
        $this->assign('member_type_id', $member_type);
        $this->assign('member_level_id', $member_level);

        $this->assign('member_type', lang('member_type'));
        $this->assign('member_sex', lang('member_sex'));
        $this->assign('member_level', Db::name('member_level')->field('id,level_title')->select());
        $this->assign('register_time', $register_time_order);

        $this->assign('page', $page);
        $this->assign('info', $_info['data']);
        $this->assign('statistics', $statistics);
        $this->assign('keywords', $keywords);
        $this->assign('registerTime_min', $registerTime_min);
        $this->assign('registerTime_max', $registerTime_max);
        $this->assign('balance_min', $balance_min);
        $this->assign('balance_max', $balance_max);

        $this->assign('markdata', $markdata);
        $this->assign('mark', $mark);
        $this->assign('cardmark', $cardmark);
        $this->assign('biz_id', $biz_id);
        return $this->fetch('index');
    }


    /**
     * [DataExport 导出当前页的用户数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function DataExport()
    {
        $excel_params = [
            'filename' => '用户数据',
            'title' => lang('excel_member_list'),
            'data' => \session('excel_member_list')
        ];
        $excel = new Excel($excel_params);
        return $excel->Export();
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户详情
     */


    function detail()
    {
        //删除用户id 缓存
        Session::delete('meberIdVoucher');
        $id = input('get.id');

        if (!empty($id)) {
            $savemark = input("get.savemark");
            $this->assign("savemark",$savemark);
            # 查询用户信息、账户信息
            $field = 'm.id,m.member_name name,m.member_age age,m.member_sex sex,mm.register_time,mm.member_code ID,m.member_phone phone,
                    m.member_idcard idcard,m.member_province,m.member_city,m.member_area,m.member_address address,m.member_openid openid,
                    mm.member_level_id,ml.level_title,ml.car_limit,mm.member_create handling_time,mm.member_expiration expiration_time,
                    mm.member_balance balance,mm.member_integral integral,m.account_status,m.headimage,m.nickname';
            $memberInfo = Db::table('member m')
                ->join('member_mapping mm', 'm.id=mm.member_id', 'left')
//                ->join('channel c', 'c.member_id=m.id', 'left')
                ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
                ->field($field)
                ->where(array('m.id' => $id))
                ->find();

            if (empty($memberInfo)) {
                return $this->error('获取用户信息失败');
            }
            # 用户信息、账户信息
            $channel = Db::name('channel')->where('member_id=' . $memberInfo['id'].' and action_type=2')
                ->order('id desc')->find();
            // dump($channel);exit;
            $memberInfo['channel'] = $channel['channel'];

            $memberInfo['pid'] = $channel['pid'];

            $memberInfo['app_status'] = Db::name('log_integral')->where(['member_id' => $id, 'integral_source' => 22])->value('id');

            $memberInfo['app_status'] = $memberInfo['app_status'] > 0 ? '是' : '否';

            $memberInfo['integral'] = intval($memberInfo['integral']) == 0 ? '0' : $memberInfo['integral'];

            $memberInfo['address'] = $memberInfo['member_province'] . $memberInfo['member_city'] . $memberInfo['member_area'];

            $memberInfo['balance'] = priceFormat($memberInfo['balance']);

            $memberInfo['name'] = empty($memberInfo['name']) ? $memberInfo['nickname'] : $memberInfo['name'];

            $memberInfo['name'] = CheckBase64($memberInfo['name']);

            $memberInfo['channel_title'] = BaseService::StatusHtml($memberInfo['channel'], lang('member_source_type'), false);

            $memberInfo['channel_title'] = empty($memberInfo['channel_title']) ? '无' : $memberInfo['channel_title'];

            $memberInfo['channel_pid'] = '无';
            if ($memberInfo['channel'] == 1)//门店
            {
                $memberInfo['channel_pid'] = Db::name('biz')->where('id=' . $memberInfo['pid'])->value('biz_title');
            }

            if ($memberInfo['channel'] == 4)//员工
            {
                $memberInfo['channel_pid'] = Db::name('employee_sal')->where('id=' . $memberInfo['pid'])->value('employee_name');
                //dump($memberInfo['channel_pid']);exit;
            }

            if ($memberInfo['channel'] == 6)//商户
            {
                $memberInfo['channel_pid'] = Db::name('merchants')->where('id=' . $memberInfo['pid'])->value('title');
            }
            if ($memberInfo['channel'] == 7)//自主办理
            {
                $memberInfo['channel_pid'] = $memberInfo['name'];
            }

            if (in_array($memberInfo['channel'], [2, 3, 5]))//用户
            {
                $name = Db::name('member')->where('id=' . $memberInfo['pid'])->field('member_name,nickname')->find();

                $memberInfo['channel_pid'] = empty($name['member_name']) ? $name['nickname'] : $name['member_name'];

                $memberInfo['channel_pid'] = CheckBase64($memberInfo['channel_pid']);
            }


            $memberInfo['level_title'] = empty($memberInfo['level_title']) ? '普通用户' : $memberInfo['level_title'];
            $memberInfo['handling_time'] = $memberInfo['member_level_id'] > 0 ? $memberInfo['handling_time'] : '';
            if (!empty($memberInfo['handling_time'])) {
                $memberInfo['expiration_time'] = $memberInfo['member_level_id'] > 1 ? '永久有效' : $memberInfo['expiration_time'];
            }
            #获取最新的办理会员时间
            $handling_time = Db::table('log_consump')->where(array('member_id'=>$memberInfo['id'],'income_type'=>2))->field('consump_time,id,member_id')->order('id desc')->find();
            $memberInfo['handling_time'] = $handling_time['consump_time'];

            $this->assign('info', $memberInfo);
            # 查询用户车辆信息
            $field = 'mc.queue_status,mc.id,mc.car_licens,cl.title logoTitle,cs.sort_title sortTitle,
                 clv.level_title levelTitle,car_mileage mileage,register_time,car_recogin_code code,
                 car_engine_number number,(select order_create from orders o where o.car_liences=mc.car_licens 
                  order by o.id desc limit 1) latest_arrival_time
                ';
            $carInfo = Db::table('member_car mc')
                ->field($field)
                ->join('car_sort cs', 'mc.car_type_id=cs.id', 'left')
                ->join('car_logo cl', 'cs.logo_id=cl.id', 'left')
                ->join('car_level clv', 'clv.id=cs.level', 'left')
                ->where(array('member_id' => $id))
                ->where('status !=3')
                ->select();

            #查询当前会员  可绑定车辆数量
            $bindable = $memberInfo['car_limit'];


            $bound = Db::table('member_car mc')->where([['member_id', '=', $id], ['status', '<', 3]])->count();

            if (empty($bindable)) {
                $bindable = 0;
            } else {
                $bindable = $bindable - $bound;
            }

            $this->assign('bindable', $bindable);
            $this->assign('bound', $bound);
            $this->assign('carInfo', $carInfo);

            //合伙人信息:被推荐人列表         收益余额
            //推荐人名称 合伙收益金额
            $partner_info = Db::name('member_partner')->alias('mp')
                ->leftJoin(['member' => 'm'], 'mp.member_id=m.id')
                ->where('mp.member_id=' . $memberInfo['id'])
                ->field('m.nickname,m.member_name,mp.partner_recommender_id ,mp.partner_balance,partner_balance_total')->find();
            $partner_info = empty($partner_info) ? [] : $partner_info;

            if (!empty($partner_info)) {

                $where = [
                    ['pid', '=', $memberInfo['id']],
                    ['channel', 'in', [2, 3, 5]]
                ];
                $id_arr = Db::name('channel')->where($where)->group('member_id')->column('member_id');
                $partner_info['recommended_num'] = Db::name('member_partner')->where([['member_id', 'in', array_filter($id_arr)]])->group('member_id')->count();

                //推荐合伙人数量
//                $where=[
//                    ['partner_recommender_id','=',$memberInfo['id']]
//                ];
//                $partner_info['recommended_num'] =Db::name('member_partner')->where($where)->count();
                //推荐人姓名
                $recommender = Db::name('member')->where('id=' . $partner_info['partner_recommender_id'])->field('member_name,nickname')->find();
                $partner_info['recommender_name'] = '无';
                if (!empty($recommender)) {

                    $partner_info['recommender_name'] = empty($recommender['member_name']) ? $recommender['nickname'] : $recommender['member_name'];

                }


                //推荐人列表
                $id_arr = Db::name('member_partner')->where('partner_recommender_id =' . $memberInfo['id'])->column('member_id');
                $where = [
                    ['id', 'in', array_filter($id_arr)]
                ];
                $partner_info['recommended_list'] = Db::name('member')->where($where)->field('IFNULL(member_name,"无") member_name,member_phone,id member_id')->select();
                foreach ($partner_info['recommended_list'] as &$piv) {
                    $piv['recommend_cate'] = '合伙人';
                    $piv['member_level_id'] = Db::name('member_mapping')->where('member_id=' . $piv['member_id'])->value('member_level_id');
                    $piv['recommend_cate'] = empty($piv['member_level_id']) || $piv['member_level_id'] < 1 ? $piv['recommend_cate'] : '会员';
                }

                //推荐会员数量
                $where = [
                    ['pid', '=', $memberInfo['id']],
                    ['channel', 'in', [2, 3, 5]]
                ];
                $id_arr = Db::name('channel')->where($where)->group('member_id')->column('member_id');
                $partner_info['recommended_vip_num'] = Db::name('member_mapping')
                    ->where([['member_id', 'in', array_filter($id_arr)], ['member_level_id', '>', 0]])->count();


            }

            $this->assign('partner_info', $partner_info);
            $this->assign('member_level', lang('member_level'));
            return $this->fetch('detail');
        } else {
            echo("<script>history.back();</script>");
        }
    }

    /**
     * [SaveBaseDetail 用户基本信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function SaveBaseDetail()
    {
        $data = Db::table('member m')
            ->join('member_mapping mm', 'm.id=mm.member_id', 'left')
            ->field('m.id,m.member_name ,m.member_age ,m.member_sex ,mm.register_time,mm.member_code ,m.member_phone ,
                m.member_idcard ,m.member_address ,m.member_openid openid,m.account_status,m.member_province,m.member_address,member_city,member_area ')
            ->where(array('m.id' => input('id')))
            ->find();

        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * [changeExpiration 修改会员过期时间]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function changeExpiration()
    {
        $this->assign('member_id', input('get.member_id'));
        return $this->fetch();
    }

    /**
     * [DoSaveData 修改用户基本信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function DoSaveData()
    {
        return MemberService::SaveData(input());
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 保养记录
     */
    function maintainInfo()
    {
        # 车牌号
        $car = input('get.car');
        $this->assign('car', $car);
        # where条件
        $where = "car_licens = '" . $car . "'";
        # 保养名称搜索
        $searchTitle = input('get.searchTitle');
        $this->assign('searchTitle', $searchTitle);
        # 保养时间最小值
        $searchTime_min = input('get.searchTime_min');
        if (!empty($searchTime_min)) {
            $where .= " and maintain_time >= '" . $searchTime_min . "'";
        }
        $this->assign('searchTime_min', $searchTime_min);
        # 保养时间最大值
        $searchTime_max = input('get.searchTime_max');
        if (!empty($searchTime_max)) {
            $where .= " and maintain_time <= '" . $searchTime_max . "'";
        }
        $this->assign('searchTime_max', $searchTime_max);
        $info = Db::table("log_maintain")
            ->where($where)
            ->order(array("maintain_time" => 'desc'))
            ->select();
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                # 获取保养项目名称
                $type = getMaintainType($v['maintain_type']);
                $info[$k]['title'] = $type['title'];
                if (!empty($searchTitle)) {
                    # 名称搜索匹配
                    if (strpos($type['title'], $searchTitle) === false) {
                        unset($info[$k]);
                    }
                }
            }
        }
        $this->assign('info', $info);
        return $this->fetch('/member/maintainInfo');
    }

    /**
     * [TripLog 用户行程]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:39:20
     */
    public function TripLog()
    {
        $where[] = ['member_id', '=', input('id')];
        // dump($where);exit;
        $data_params = array(
            'page' => false,

            'where' => $where,
            'table' => 'member_trip',
            'n'=>4,


        );
        $data = BaseService::DataList($data_params);
        $this->assign('data', $data);
        return $this->fetch();
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 服务记录
     */
    function serviceLog1()
    {
        # 用户id
        $memberId = input('get.memberId');
        $where = 'member_id = ' . $memberId;
        $this->assign('memberId', $memberId);
        # 服务名称
        $title = input('get.searchTitle');
        if (!empty($title)) {
            $where .= " and title like '%" . $title . "%'";
        }
        $this->assign('title', $title);
        # 时间最小值
        $searchTime_min = input('get.searchTime_min');
        if (!empty($searchTime_min)) {
            $where .= " and time >= '" . $searchTime_min . "'";
        }
        $this->assign('searchTime_min', $searchTime_min);
        # 时间最大值
        $searchTime_max = input('get.searchTime_max');
        if (!empty($searchTime_max)) {
            $where .= " and time <= '" . $searchTime_max . "'";
        }
        $this->assign('searchTime_max', $searchTime_max);
        # 查询服务记录
        $info = Db::table('log_service ls')
            ->field('log_service_source title,log_service_price price,pay_type,log_service_create time,biz_title')
            ->join('biz b', 'b.id=ls.biz_id', 'left')
            ->where($where)
            ->order(array('ls.id' => 'desc'))
            ->select();
        #查询这个用户的 所有车牌号  在根据车牌号 查出 对应的服务记录
        $this->assign('info', $info);
        return $this->fetch('/member/serviceLog');
    }

    /**
     * [serviceLog 服务记录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年2月4日09:38:32
     */
    public function serviceLog()
    {

        if (input('get.action') == 'ajax') {

            $where = MemberService::ServiceListWhere(input());
            $where[] = ['o.order_status','=','5'];
            $where[] = ['o.order_type','neq','3'];
            $info = MemberService::ServiceDataList($where);

//            $total=MemberService::DataTotal($where);


            return ['code' => 0, 'msg' => '', 'data' => $info];
        } else {
            $biz_arr = Db::name('biz')->where('biz_status=1')->field('biz_title,id')->select();
            $this->assign('biz_arr', $biz_arr);
            return $this->fetch();
        }
    }

    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 预约记录
     *
     */
    public function appointLog()
    {

        if (input('get.action') == 'ajax') {
            $where = MemberService::ServiceListWhere(input());
            $where[] = ['o.order_type','=','3'];
            $info = MemberService::ServiceDataList($where);
            return ['code' => 0, 'msg' => '', 'data' => $info];
        } else {
            $biz_arr = Db::name('biz')->where('biz_status=1')->field('biz_title,id')->select();
            $this->assign('biz_arr', $biz_arr);
            return $this->fetch();
        }
    }

    /**
     * [ServiceDetail 服务详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年2月4日09:38:32
     */
    public function ServiceDetail()
    {
        $where[] = [
            ['os.order_number', '=', input('order_number')],
        ];
        $evaluation_status = input('get.evaluation_status');
        $service_arr = Db::name('order_server')->alias('os')
            ->leftJoin(['evaluation_score' => 'es'], 'os.id=es.orderserver_id')
            ->leftJoin(['service' => 's'], 's.id=os.server_id')
            ->where($where)
            ->group('os.id')
            ->field('s.service_title,os.price,es.score,server_id,os.id,coupon_price,original_price')
            ->select();


        foreach ($service_arr as &$vv) {
            if($evaluation_status==1){
                $vv['score_title']='未评论';
            }else {
                //拼评论等级
                if ($vv['score'] > 3) {
                    $vv['score_title'] = '好评';
                } elseif ($vv['score'] == 3) {
                    $vv['score_title'] = '中评';
                } else {
                    $vv['score_title'] = '差评';
                }
            }

            //服务数量
            $vv['num'] = 1;
        }
        //dump($service_arr);exit;
        $this->assign('data', $service_arr);
        return $this->fetch();
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 积分记录
     */
    function integralLog()
    {
        # 用户id
        $memberId = input('memberId');
        $where = 'member_id = ' . $memberId;
        $this->assign('memberId', $memberId);

        # 时间最小值
        $searchTime_min = input('searchTime_min');

        if (!empty($searchTime_min)) {
            $where .= " and integral_time >= '" . $searchTime_min . "'";
        }
        $this->assign('searchTime_min', $searchTime_min);

        # 时间最大值
        $searchTime_max = input('searchTime_max');

        if (!empty($searchTime_max)) {
            $where .= " and integral_time <= '" . $searchTime_max . "'";
        }
        $this->assign('searchTime_max', $searchTime_max);

        #筛选类型
        $type = input('type');

        if (isset($type) && strlen($type) > 0) {
            $where .= " and integral_type = $type";
        }

        $this->assign('type', $type);
        # 查询积分记录
        $info = Db::table('log_integral li')
            ->field('integral_time time,integral_type type,integral_source source,integral_number num')
            ->where($where)
            ->order(array('li.id' => 'desc'))
            ->select();
        if (!empty($info)) {
            foreach ($info as &$v) {
                if ($v['type'] == 1) {
                    #获得
                    $v['num'] = '+' . $v['num'];

                } else {
                    #使用
                    $v['num'] = '-' . $v['num'];

                }
            }
        }
        $this->assign('info', $info);
        return $this->fetch('/member/integralLog');
    }

    function add()
    {
        return $this->fetch();
    }

    /**
     * [RecommendLog 推荐记录记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function RecommendLog()
    {
        if (input('get.action') == 'ajax') {
            $params = input();

            // 条件

            //dump($where);exit;
            $data = MemberService::MemberPartnerDataList($params);

            // dump($data);exit;
//            $total = BaseService::DataTotal('member_partner',$where);

            return ['code' => 0, 'msg' => '', 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [incomeLog 收益记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function IncomeLog()
    {
        Session::delete('meberIdVoucher');

        if (input('get.action') == 'ajax') {
            $params = input();


            $where = ConsumpService::ConsumpListWhere($params);
            $where[] = ['income_type','in',[8,9,10,11]];
            $where[] = ['log_consump_type','=',2];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'log_consump',
                'order' => 'consump_time desc'
            );
            $data = BaseService::DataList($data_params);

            $data = ConsumpService::ConsumpDataDealWith($data);
            $total = BaseService::DataTotal('log_consump', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('member_fee_aim', lang('member_fee_aim'));
            return $this->fetch();
        }
    }

    /**
     * [CashLog 提现记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function CashLog()
    {
        Session::delete('meberIdVoucher');

        if (input('get.action') == 'ajax') {
            $params = input();

            // 条件
            $where = BizDepositService::ListWhere($params);
            $where[] = ['source_type', '=', 2];

            // dump($where);exit;
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'log_deposit',
                'field' => '*',
                'order' => 'deposit_status asc'
            );

            $data = BaseService::DataList($data_params);

            $data = BizDepositService::DataDealWith($data);

            $total = BaseService::DataTotal('log_deposit', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [rechargeLog 充值记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function rechargeLog()
    {
        Session::delete('meberIdVoucher');
        if (input('get.action') == 'ajax') {
            $params = input();
            // 条件
            $where = ConsumpService::ConsumpListWhere($params);
            $where[] = ['income_type', 'not in', "8,9,10,11"];
            $where[] = ['log_consump_type', '=', 2];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'log_consump',
                'order' => 'consump_time desc'
            );
            $data = BaseService::DataList($data_params);
            $data = ConsumpService::ConsumpDataDealWith($data);
            $total = BaseService::DataTotal('log_consump', $where);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [redEnvelopeLog 红包记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function RedEnvelopeLog()
    {
        if (input('get.action') == 'ajax') {
            $params = input();

            // 条件
//            $where =  ConsumpService::ConsumpListWhere($params);
//            $where[]=['income_type','in',[2,6,7]];
            // dump($where);exit;
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => [['member_id', '=', $params['member_id']]],
                'table' => 'redpacket_log',
                'order' => 'create_time desc'
            );
            $data = BaseService::DataList($data_params);

            $data = MemberService::RedEnvelopeDataDealWith($data);
            $total = BaseService::DataTotal('redpacket_log', [['member_id', '=', $params['member_id']]]);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [memberVoucher 卡券记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function memberVoucher()
    {
        $memberId = input("get.memberId");
        if (isset($memberId)) {
            #用户id  点击卡券记录进来的
            Session::set('meberIdVoucher', $memberId);
        }
        if (input('get.action') == 'ajax') {
            $params = input();

            // 条件
            $where = MemberVoucherService::MemberVoucherListWhere($params);
            $wherestr = "date(mv.create)>='".date("Y-m-d")."'";
            $where[] = ['mv.status', '=', 1];
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'field' => 'id,start_time,card_title,voucher_cost,card_time,create,voucher_source,card_time',
                'wherestr'=>$wherestr
            );

            $data = MemberVoucherService::DataList($data_params);
            $total = MemberVoucherService::DataTotal($where,$wherestr);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('memberId', $memberId);
            return $this->fetch();
        }
    }

    /**
     * [consumptionLog 商家消费记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月31日11:36:46
     */
    public function consumptionLog()
    {

        if (input('get.action') == 'ajax') {
            $params = input();

            // 条件

            $where[] = ['member_id', '=', input('member_id')];
            // dump($where);exit;
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'merchants_order',
                'order' => 'create_time desc'
            );
            $data = MemberService::ConsumptionDataList($data_params);

            $total = BaseService::DataTotal('merchants_order', $where);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            return $this->fetch();
        }
    }

    /**
     * [SaveMemberVoucher 赠送卡卷页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function SaveMemberVoucher()
    {
        return $this->fetch();
    }

    /**
     * [SaveMemberVoucher 赠送卡卷页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function DoSaveMemberVoucher()
    {
        $params = $this->data_post;
        return MemberVoucherService::SaveMemberVoucherData($params);
    }

    /**
     * [VoucherTypeStr 获取卡卷下属分类]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function VoucherTypeStr()
    {
        $card_type_id = input('card_type_id');
        return MemberVoucherService::VoucherTypeData($card_type_id);
    }

    /**
     * [VoucherService 获取卡卷面值]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function VoucherService()
    {
        $cate_id = input('cate_id');
        $card_type_id = input('card_type_id');
        return MemberVoucherService::VoucherServiceData($card_type_id, $cate_id);
    }

    /**
     * [DelMemberVoucher 删除卡卷]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function DelMemberVoucher()
    {
        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }
        $params = $this->data_post;
        $params['table'] = 'member_voucher';
        $params['soft'] = false;
        $params['errorcode'] = 13006;
        $params['msg'] = '删除失败';
        BaseService::DelInfo($params);
        return DataReturn('删除成功', 0);
    }

    /**
     * [SaveMemberData 修改用户信息页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function SaveMemberData()
    {
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm' => 0,
                'n' => 1,
                'page' => false,
                'table' => 'member'
            ];

//            $ret = BaseService::DataList($data_params);
//            $ret=NoiceService::DataDealWith($ret);

            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [DoEditPrice 执行修改用户积分或余额]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function DoEditIntegralBalance()
    {
        $params = $this->data_post;
        $action = $params['action'];
        $member_id = $params['member_id'];
        unset($params['action']);
        $filed = isset($params['member_balance']) ? ['member_balance', $params['member_balance']] : ['member_integral', $params['member_integral']];

        $re = Db::name('member_mapping')->where(['member_id' => $params['member_id']])->$action($filed[0], $filed[1]);

        if (!$re) {
            throw new \BaseException(array('code' => 403, 'msg' => '修改用户余额或积分失败', 'errorcode' => '14001', 'status' => false, 'debug' => false));
        }
        if($action=='setInc'){
            # 增加平台赠送记录
            if(isset($params['member_balance'])){
                # 增加充值记录
                Db::table("log_consump")->insert(array("member_id"=>$member_id,"consump_price"=>$params['member_balance'],"consump_time"=>date("Y-m-d H:i:s"),
                    "log_consump_type"=>2,"income_type"=>17,"biz_id"=>0));
                # 增加补贴记录
                SubsidyService::addSubsidy(array("subsidy_type"=>1,"subsidy_number"=>$params['member_balance'],"member_id"=>$member_id));
            }else{
                # 增加积分记录
                Db::table("log_integral")->insert(array("integral_number"=>$params['member_integral'],"integral_source"=>0,"integral_type"=>1,
                    "integral_time"=>date("Y-m-d H:i:s"),"member_id"=>$member_id));
            }
        }
        $MemberService=new \app\api\ApiService\MemberService();
        $MemberService->changeMemberInfo($member_id);
        return DataReturn('ok', 0);
    }

    /**
     * [DoSaveMemberData 执行修改用户信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function DoSaveMemberData()
    {
        $params = $this->data_post;


        if (isset($params['member_balance']) || isset($params['member_integral'])) {
            $action = $params['action'];
            $member_id = $params['member_id'];
            unset($params['action']);
            $filed = isset($params['member_balance']) ? ['member_balance', $params['member_balance']] : ['member_integral', $params['member_integral']];

            $re = Db::name('member_mapping')->where(['member_id' => $params['member_id']])->$action($filed[0], $filed[1]);

            if (!$re) {
                throw new \BaseException(array('code' => 403, 'msg' => '修改用户余额或积分失败', 'errorcode' => '14001', 'status' => false, 'debug' => false));
            }
            return DataReturn('ok', 0);
        }

        if (Db::name('member')->where(['id' => intval($params['id'])])->update($params)) {
            return DataReturn('ok', 0);
        }
    }

    /*
     * @content:删除 车辆信息
     * @retrun array
     * */
    function checkMemberCarDel()
    {
        $id = input("carId");
        if (!empty($id)) {
            $a = Db::table('member_car')->where(array('id' => $id))->update(array('status' => 3));
            return array('status' => true);
        }
    }

    /**
     * [activeTypeService   服务]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * Content:$mtype  task  任务管理 ()
     */
    function activeTypeService()
    {
        $info = input("post.");

        if (!empty($info)) {
            if ($info['type'] == 'pro') {
                $title = '商品分类';
            } else if ($info['type'] == 'service') {
                $title = '服务分类';
            } else if ($info['type'] == 'member') {
                $title = '会员名称';
            }
            $mtype = $info['mtype'];
            unset($info['mtype']);
            $data = ActiveService::activeTypeService($info, $mtype);
            return array('status' => true, 'data' => $data, 'title' => $title);

        }

    }

    /**
     * [activeBizproName 获取商品名称  根据二级分类id]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeBizproName()
    {
        $params = input("post.");
        $mtype = input('post.mtype');// task 任务  active 活动 红包 ：redpacket ，sign  签到
        unset($params['mtype']);
        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $title = '商品二级分类';
            }
            if ($params['type'] == 'pro') {
                $data = ActiveService::activeBizproName($params, $mtype);

                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无商品名称');

                }
            } else {

                if ($mtype == 'active') {
                    #活动管理
                    #存服务session  显示
                } else if ($mtype == 'redPacket') {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                } else {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                }


            }
        }

    }

    /**
     * [activeCreateGive 查询 显示 商品二级分类  服务名称  会员卡直接显示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeCreateGive()
    {
        #获取缓存活动id 只有修改时候有
        $params = input("post.");
        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $title = '商品二级分类';
            } else if ($params['type'] == 'service') {
                $title = '服务名称';
            } else {
                $title = '';
            }
            $mtype = input("post.mtype");
            unset($params['mtype']);
            $data = ActiveService::activeTypeService($params, $mtype);
            if ($params['type'] == 'pro') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无二级分类');

                }
            } else if ($params['type'] == 'service') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无服务名称');

                }
            }
        }

    }

    #整理提交的数据 存到数据库中
    function memberVoucherSession()
    {
        #获取缓存活动id 只有修改时候有
        $params = input("post.");
        $data = MemberVoucherService::memberVoucherSession($params);
        return array('status' => true, 'data' => $data);


    }

    #修改卡券有效期
    function mVoucherWrite()
    {
        $params = input('post.');
        $create = date('Y-m-d H:i:s', strtotime($params['start_time'] . '+' . $params['card_time'] . ' day'));

        Db::table('member_voucher')->where(array('id' => $params['id']))->update(array('create' => $create, 'card_time' => $params['card_time']));
        return array('status' => true);

    }


}
