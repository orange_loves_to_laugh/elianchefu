<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/10/16
 * Time: 15:50
 */

namespace app\admin\controller;


use app\api\ApiService\MerchantService;
use app\reuse\controller\ResponseJson;
use app\service\ActiveService;
use app\service\ActivetipsService;
use app\service\ApplayService;
use app\service\BaseService;
use app\service\ExchangeService;
use app\service\PacketService;
use app\service\ResourceService;
use app\service\RunnerService;
use app\service\SetmealService;
use app\service\SignService;
use app\service\TaskService;
use Redis\Redis;
use think\Db;
use think\facade\Session;

class Market extends Common
{
    use ResponseJson;

    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [BizIndex 活动列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function active_index()
    {

        if (input("get.action") == 'ajax') {
            $searchParams = input("post.searchParams");
            $_where = 'active_putstatus != 4';

            $time = date("Y-m-d H:i:s");
            if (!empty($searchParams['status'])) {
                if ($searchParams['status'] == 1) {
                    #待上架
                    $_where .= " and '$time' < active_start and '$time' < active_end";

                } else if ($searchParams['status'] == 2) {
                    #已上架
                    $_where .= " and '$time' >= active_start and '$time' <= active_end";
                } else if ($searchParams['status'] == 3) {
                    #已下架
                    $_where .= " and '$time' > active_start and '$time' > active_end";
                }


            }
            if (!empty($searchParams['activeType'])) {
                $_where .= " and a.active_mold = '" . $searchParams['activeType'] . "'";
            }
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = ActiveService::ActiveIndex($data_params);
            $total = ActiveService::activetipsIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    /**
     * [active_save 活动添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function active_save()
    {
        $active_id = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = input("post.");
            /*  if($params['use_person'] == 1){
                  #针对人群是会员
                  $session = session('ActiveGiveInfo');
                  $DbSession = Db::table('active_giving')->where(array('active_id'=>$active_id))->value('id');
                  if(!empty($session) or !empty($DbSession)){

                      $data = ActiveService::ActiveSave($params);

                  }else{
                      return array('status'=>false,'msg'=>'会员状态活动赠送不能为空');


                  }
              }else{

              }*/
            $data = ActiveService::ActiveSave($params);

            return $data;


        } else {
            // dump(Session::get('admin'));exit;
            if (!empty($active_id)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('id' => $active_id),//查找条件
                );
                $data = ActiveService::ActiveIndex($data_params);
                $bannerArr = BaseService::HandleImg($data[0]['active_picurl']);
                $content_detail_arr = BaseService::HandleImg($data[0]['content_detail']);
                //dump($content_detail_arr);exit;
                $activeType = ActiveService::getActiveContent($active_id, 'type');
                $activeGive = ActiveService::getActiveContent($active_id, 'give');
                if (!empty($activeType) and $activeType[0]['active_type'] == 3) {
                    $this->assign("showdetail", "2");
                } else {
                    $this->assign("showdetail", "1");
                }
                #缓存存活动id
                Session::set('activeId', $active_id);

                $this->assign('activeType', $activeType);
                $this->assign('activeGive', $activeGive);
                $this->assign('content_detail_arr', $content_detail_arr);
                $this->assign('bannerArr', $bannerArr);
                $this->assign('active', $data[0]);
            }
            $params['id'] = empty($params['id']) ? '' : $params['id'];
            //多余图片处理
            ResourceService::$session_suffix = 'source_upload' . 'active' . $params['id'];
            ResourceService::delUploadCache();
            if (empty($active_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('activeId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('ActiveTypeInfo' . 'pro');
            Session::delete('ActiveTypeInfo' . 'service');
            Session::delete('ActiveTypeInfo' . 'member');
            Session::delete('ActiveGiveInfo');
            Session::delete('VoucherInfo');


            return $this->fetch();
        }

    }

    /**
     * [activeTypeService 创建活动类型  商品 服务 会员]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * Content:$mtype  task  任务管理   active:活动管理
     */
    function activeTypeService()
    {
        $info = input("post.");
        if (isset($info['days_id'])) {
            #签到天数的id
            session::set('daysId', $info['days_id']);
        }
        if (!empty($info)) {
            if ($info['type'] == 'pro') {
                $title = '商品分类';
            } else if ($info['type'] == 'service') {
                $title = '服务分类';
            } else if ($info['type'] == 'member') {
                $title = '会员名称';
            }
            $mtype = $info['mtype'];
            unset($info['mtype']);
            $data = ActiveService::activeTypeService($info, $mtype);

            return array('status' => true, 'data' => $data, 'title' => $title);

        }

    }

    /**
     * [activeCreateGive 查询 显示 商品二级分类  服务名称  会员卡直接显示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeCreateGive()
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('activeId');
        $params = input("post.");
        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $title = '商品二级分类';
                $active_mold = 1;
            } else if ($params['type'] == 'service') {
                $title = '服务名称';
                $active_mold = 2;
            } else {
                $title = '';
                $active_mold = 3;
            }
            $mtype = input("post.mtype");
            unset($params['mtype']);
            $data = ActiveService::activeTypeService($params, $mtype);
            if ($params['type'] == 'pro') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无二级分类');

                }
            } else if ($params['type'] == 'service') {
                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无服务名称');

                }
            } else {
                if (!empty($params['id'])) {
                    #会员名称id  查询会员信息展示  存session
                    $params['prize_type'] = $params['type'];
                    $data = ActiveService::activeSession($params, $activeId);
                }
                #算下 服务价格总共多少
                return array('status' => true, 'data' => $data['str'], 'title' => $title, 'totalPrice' => $data['totalPrice'], 'active_mold' => $active_mold, 'active_explain' => $data['active_explain']);
//                return array('status' => true, 'data' => $data, 'title' => $title);
            }
        }

    }

    /**
     * [activeBizproName 获取商品名称  根据二级分类id]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activeBizproName()
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('activeId');
        $params = input("post.");
        $mtype = input('post.mtype');// task 任务  active 活动 红包 ：redpacket ，sign  签到
        unset($params['mtype']);
        if (!empty($params)) {
            if ($params['type'] == 'pro') {
                $title = '商品二级分类';
            }
            if ($params['type'] == 'pro') {
                $data = ActiveService::activeBizproName($params, $mtype);

                if (!empty($data)) {
                    return array('status' => true, 'data' => $data, 'title' => $title);

                } else {
                    return array('status' => false, 'msg' => '暂无商品名称');

                }
            } else {

                if ($mtype == 'active') {
                    #活动管理
                    #存服务session  显示
                    $data = ActiveService::activeSession($params['id'], $params['type'], $params['mark'], $activeId);
                    return array('status' => true, 'data' => $data);
                } else if ($mtype == 'task') {
                    #任务管理
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);

//                    $data  =TaskService::taskSession($params,$taskId);

                } else if ($mtype == 'redPacket') {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                } else {
                    #查询服务信息
                    $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
                    $title = $bstitle['title'];
                    $detail_price = $bstitle['detail_price'];
                    $pro_service_id = $bstitle['detail_id'];

                    return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
                }


            }
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function activeSession()
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('activeId');
        $params = input("post.");
        $params['prize_type'] = $params['type'];
        $data = ActiveService::activeSession($params, $activeId);
        return array('status' => true, 'data' => $data['str'], 'totalPrice' => $data['totalPrice']);

    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getActiveTypeList()
    {
        $params = input("post.");
        $data = ActiveService::getActiveTypeList($params['id'], $params['type'], $params['mark']);
        return array('status' => true, 'data' => $data['str'], 'totalPrice' => $data['totalPrice']);
    }

    /**
     * [activeTypeWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,    pro:商品  service ： 服务 member ：会员  integral: 积分  balance余额
     * @params activeMark  type:创建的  give 赠送的
     */
    function activeTypeWrite()
    {
        $info = input("post.");

        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 3) {
                    if ($info['activeMark'] == 'type') {
                        #创建商品 服务
                        # 直接操作数据库
                        Db::table("active_detail")->where(array('id' => $info['id'], 'active_type' => $info['type']))->update(array('detail_number' => $info['value']));
                    } else {

                        #赠送 # 直接操作数据库
                        $a = Db::table("active_giving")->where(array('id' => $info['id'], 'giving_type' => $info['type']))->update(array('giving_number' => $info['value']));
                    }
                } else if ($info['vmark'] == 5) {
                    if ($info['activeMark'] == 'type') {
                        #创建商品 服务
                        # 直接操作数据库
                        Db::table("active_detail")->where(array('id' => $info['id'], 'active_type' => $info['type']))->update(array('expiration' => $info['value']));
                    } else {
                        #赠送 # 直接操作数据库
                        Db::table("active_giving")->where(array('id' => $info['id'], 'giving_type' => $info['type']))->update(array('expiration' => $info['value']));
                    }
                } else if ($info['vmark'] == 6) {
                    $settlement = json_decode($info['settlement_price'], true);
                    $settlement[$info['size']] = $info['value'];
                    $settlement_price = json_encode($settlement);
                    if ($info['activeMark'] == 'type') {
                        #创建商品 服务
                        # 直接操作数据库
                        Db::table("active_detail")->where(array('id' => $info['id'], 'active_type' => $info['type']))->update(array('settlement_price' => $settlement_price));
                    } else {
                        #赠送 # 直接操作数据库
                        Db::table("active_giving")->where(array('id' => $info['id'], 'giving_type' => $info['type']))->update(array('settlement_price' => $settlement_price));
                    }
                }

                return array('status' => true);

            } else {
                # 操作session
                if ($info['activeMark'] == 'type') {
                    if ($info['activeType'] == 1) {
                        $activeType = 'pro';

                    } else if ($info['activeType'] == 2) {
                        $activeType = 'service';

                    } else if ($info['activeType'] == 3) {
                        $activeType = 'member';

                    }
                    #创建 活动类型
                    $_session = Session('ActiveTypeInfo' . $activeType);

                } else {
                    #活动赠送
                    $_session = Session('ActiveGiveInfo');

                }

                if (!empty($_session)) {

                    if ($info['vmark'] == 3) {
                        $_session[$info['k']]['detail_number'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        $_session[$info['k']]['expiration'] = $info['value'];

                    } else if ($info['vmark'] == 6) {
                        $_session[$info['k']]['settlement_price'][$info['size']] = $info['value'];

                    }

                    if ($info['activeMark'] == 'type') {
                        #创建
                        Session::set('ActiveTypeInfo' . $activeType, $_session);
                    } else {
                        #赠送
                        Session::set('ActiveGiveInfo', $_session);
                    }

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [activeTypeDel 活动  创建商品  赠送 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     * @params activeMark  type:创建的  give 赠送的
     */
    function activeTypeDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                if ($info['activeMark'] == 'type') {
                    #创建商品 服务
                    # 直接操作数据库
                    DB::table('active_detail')->where(array('id' => $info['id']))->delete();

                } else {
                    #赠送
                    # 直接操作数据库
                    DB::table('active_giving')->where(array('id' => $info['id']))->delete();

                }
                return array('status' => true);
            }
        } else {
            # 操作session
            if ($info['activeMark'] == 'type') {
                if ($info['activeType'] == 1) {
                    $activeType = 'pro';

                } else if ($info['activeType'] == 2) {
                    $activeType = 'service';

                } else if ($info['activeType'] == 3) {
                    $activeType = 'member';

                }
                #创建 活动类型
                $_session = Session('ActiveTypeInfo' . $activeType);


            } else {
                #赠送
                $_session = Session('ActiveGiveInfo');

            }
            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    if ($info['activeMark'] == 'type') {
                        #创建
                        Session::set('ActiveTypeInfo' . $activeType, $_session);
                    } else {
                        #赠送
                        Session::set('ActiveGiveInfo', $_session);
                    }

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    /**
     * [Delinfo 活动 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     */
    function DelInfo()
    {
        $id = input("post.id");
        $mark = input("post.mark");
        if ($mark == 'task') {
            #任务
            if (!empty($id)) {
                #删除 假删
                Db::table('task')->where(array('id' => $id))->update(array('task_status' => 3));
                return array('status' => true);
            }

        } else if ($mark == 'sign') {
            #签到奖品
            if (!empty($id)) {
                #删除
                Db::table('signs')->where(array('id' => $id))->delete();
                return array('status' => true);
            }

        } else if ($mark == 'integral') {
            #积分兑换
            if (!empty($id)) {
                #删除 假删
                Db::table('integral_exchange')->where(array('id' => $id))->update(array('status' => 2));
                return array('status' => true);
            }
        } else if ($mark == 'activetips') {
            #积分兑换
            if (!empty($id)) {
                #删除 假删
                Db::table('active_tips')->where(array('id' => $id))->update(array('status' => 2));
                #前端删除缓存
                $_redis = new Redis();
                $_redis->hDel("pop", "active");
                return array('status' => true);
            }
        } else if ($mark == 'redpacket') {
            #大转轮
            if (!empty($id)) {
                #删除 假删
                Db::table('redpacket')->where(array('id' => $id))->update(array('status' => 0));
                return array('status' => true);
            }
        } else {

            #活动
            if (!empty($id)) {
                $time = date("Y-m-d H:i:s");
                #进行中 不可删除
                $active = Db::table('active')->field('id,active_start,active_end')->where("id = $id")->find();
                if ($time >= $active['active_start'] and $time <= $active['active_end']) {
                    return array('status' => false, 'msg' => '活动正在进行中不可删除！');
                } else {
                    #删除 假删
                    Db::table('active')->where(array('id' => $id))->update(array('active_putstatus' => 4));
                    return array('status' => true);

                }

            } else {
                return array('status' => false, 'msg' => '活动id 为空！');
            }
        }

    }

    /**
     * [activeGiveRecord 查看  活动类型  活动赠送]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     */
    function activeGiveRecord()
    {
        $activeId = input("get.active_id");

        if (input('get.action') == 'ajax') {
            $_where = "";

            $searchParams = input("post.searchParams");
            /*    $keywords = strim($searchParams['keywords']);
                if(!empty($searchParams['keywords']))
                {
                    $_where .= " and s.service_title like '%".$keywords."%'";

                }*/
            if (!empty($searchParams['active_type'])) {
                $_where = " and ag.giving_type = '" . $searchParams['active_type'] . "'";

            }
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            #服务
            $active_give = Db::table('active_giving ag')
                ->field('ag.giving_type active_type,ag.giving_number detail_number,ag.giving_price detail_price,ag.expiration,giving_id')
                ->where("ag.active_id = $activeId" . $_where);
            $info = $active_give->paginate($listRows, false, ['query' => request()->param()]);
            $data = $info->toArray()['data'];
            if (!empty($data)) {
                foreach ($data as $k => $v) {
                    if ($v['active_type'] == 4) {
                        $data[$k]['title'] = '积分赠送';

                    } else if ($v['active_type'] == 5) {
                        $data[$k]['title'] = '余额赠送';
                    } else if ($v['active_type'] == 7 or $v['active_type'] == 8) {
                        $data[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'merchantVoucher');
                    } else if ($v['active_type'] == 1) {
                        #giving_id 是卡券的id  查询卡券的服务名称
                        $data[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'pro');


                    } else if ($v['active_type'] == 2) {
                        $data[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'service');


                    }
                    $data[$k]['detail_price'] = getsPriceFormat($v['detail_price']);


                }

            }
            $total = Db::name('active_giving')->where(array('active_id' => $activeId))->count();
            return ['code' => 0, 'msg' => '', 'data' => array('data' => $data, 'total' => $total)];

        } else {
            $this->assign('activeId', $activeId);
            return $this->fetch('/market/activeGiveRecord');
        }
    }
    /**********************************任务管理 start ***********************************************/
    /**
     * [task_index 任务列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function task_index()
    {
        if (input("get.action") == 'ajax') {
            $search = input("post.searchParams");
            $_where = 'task_status != 3';
            if (!empty($search['task_type'])) {
                $_where .= ' and t.task_type = "' . $search['task_type'] . '"';

            }
            $time = date("Y-m-d H:i:s");
            if (!empty($search['status'])) {
//                $_where .= ' and t.task_status = "' . $search['status'] . '"';
                if ($search['status'] == 1) {
                    #待上架
                    $_where .= " and '$time' < start_time and '$time' < end_time";

                } else if ($search['status'] == 2) {
                    #已上架
                    $_where .= " and '$time' >= start_time and '$time' <= end_time";
                } else if ($search['status'] == 3) {
                    #已下架
                    $_where .= " and '$time' > start_time and '$time' > end_time";
                }else if ($search['status'] == 3) {
                    #已下架
                    $_where .= ' and t.task_status = 2';
                }

            }
            $mainTask = input('get.main_task');
            if (!empty($mainTask)) {
                $_where .= ' and t.main_task = ' . $mainTask;
            }
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = TaskService::TaskIndex($data_params);
//            return $this->jsonSuccessData($data);
            $total = TaskService::activetipsIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            $task_type = getTaskType();
            $this->assign('taskType', $task_type);
            #删除 门店id
            Session::delete('bizId');
            $mainTask = input('get.main_task');
            if ($mainTask == 1) {
                return $this->fetch('/market/task_main');
            } else {
                return $this->fetch();
            }


        }

    }

    /**
     * [task_save 任务添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function task_save()
    {
        $task_id = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = input("post.");
            $data = TaskService::TaskSave($params);
            return $data;

        } else {
            if (!empty($task_id)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('id' => $task_id),//查找条件
                );
                $data = TaskService::TaskIndex($data_params);
                #查询任务奖励 信息
                $taskType = TaskService::getTaskContent($task_id);

                #缓存存活动id
                Session::set('taskId', $task_id);

                $this->assign('taskType', $taskType);
                $this->assign('task', $data[0]);
                $this->assign('main_task', $data[0]['main_task']);
            }else{
                $this->assign('main_task', input('get.main_task'));
            }
            if (empty($task_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('taskId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('TaskPrizeInfo');

            $task_type = getTaskType();
            $this->assign('task_type', $task_type);



            return $this->fetch();
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function taskSession()
    {
        #获取缓存活动id 只有修改时候有
        $taskId = Session('taskId');
        $params = input("post.");

        $data = TaskService::taskSession($params, $taskId);
        return array('status' => true, 'data' => $data);


    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getTaskTypeList()
    {
        $params = input("post.");
        $data = TaskService::getTaskTypeList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [taskTypeWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   3 pro:商品  5 service ： 服务  4 ：红包   1 integral: 积分   2 balance余额
     * @params val,   修改之前 的值
     */
    function taskTypeWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 3) {
                    #奖励数量
                    Db::table("task_prize")->where(array('id' => $info['id'], 'prize_type' => $info['type']))->update(array('prize_num' => $info['value']));

                } else if ($info['vmark'] == 4) {
                    $arr = array();
                    #剩余数量 = 原来的剩余数量 +修改的数量- 原来的上架数量
                    $arr['surplus_num'] = $info['surplus_num'] + $info['value'] - $info['val'];
                    $arr['ground_num'] = $info['value'];

                    # 直接操作数据库
                    Db::table("task_prize")->where(array('id' => $info['id'], 'prize_type' => $info['type']))->update($arr);
                } else if ($info['vmark'] == 5) {

                    # 直接操作数据库 有效期
                    Db::table("task_prize")->where(array('id' => $info['id'], 'prize_type' => $info['type']))->update(array('prize_indate' => $info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('TaskPrizeInfo');
                if (!empty($_session)) {

                    if ($info['vmark'] == 3) {
                        #奖励数量
                        $_session[$info['k']]['prize_num'] = $info['value'];

                    } else if ($info['vmark'] == 4) {
                        $surplus_num = $info['surplus_num'] + $info['value'] - $info['val'];
                        #修改上架数量 也同时修改 剩余数量
                        $_session[$info['k']]['surplus_num'] = $surplus_num;
                        $_session[$info['k']]['ground_num'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        #有效期
                        $_session[$info['k']]['prize_indate'] = $info['value'];

                    }
//                    dump($_session);die;

                    #存session
                    Session::set('TaskPrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [taskTypeDel 活动  创建商品  赠送 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function taskTypeDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {

                #赠送
                # 直接操作数据库
                DB::table('task_prize')->where(array('id' => $info['id']))->delete();

                return array('status' => true);
            }
        } else {
            # 操作session
            #赠送
            $_session = Session('TaskPrizeInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('TaskPrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    /**
     * [bizproService 查询商品  服务 名称]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id, 商品id 或服务id
     * @params type,pro 商品 或 service服务
     */
    function bizproService()
    {
        #mark  为give 说明这个奖项是可以 赠送积分商品的
        $params = input("post.");
        $bstitle = ActiveService::BizServiceTitle($params['id'], $params['type']);
        $title = $bstitle['title'];
        $detail_price = $bstitle['detail_price'];
        $pro_service_id = $bstitle['detail_id'];

        return array('status' => true, 'bstitle' => $title, 'detail_price' => $detail_price, 'pro_service_id' => $pro_service_id);
    }

    /**
     * [redPacketService 活动  创建红包]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function redPacketService()
    {

        $arr = array(1 => '商品通用抵用券', 2 => '服务通用抵用券', 3 => '单独商品抵用券', 4 => '单独服务抵用券', 6 => '现金低值通用券',17=>'会员抵值券', 7 => '消费券创建');
        $_str = '';
        foreach ($arr as $k => $v) {
            $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"getCreatePacket('" . $v . "','" . $k . "')\">" . $v . "</button></div>";

        }
        return array('status' => true, 'data' => $_str);

    }

    #整理卡券缓存数据和数据库数据显示在页面
    function voucherSession()
    {
        $params = input("post.");

        $data = TaskService::voucherSession($params);
        return array('status' => true, 'data' => $data);

    }

    #抵用卡券展示
    function getVoucherSessionList()
    {
        $params = input("post.");

        $data = TaskService::getVoucherSessionList($params);
        return array('status' => true, 'data' => $data);
    }

    /**
     * [voucherDelInfo 卡券 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params k,   缓存要修改的对应key值
     */
    function voucherDelInfo()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {

            #修改红包详情的状态
            Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('status' => '2'));
            return array('status' => true);

        } else {
            # 操作session
            $_session = Session('VoucherInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('VoucherInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }


    }

    /**
     * [voucherWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     */
    function voucherWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            $_session = Session('VoucherInfo');

            # 操作session
            $_str = '';

            if ($info['mark'] == 'DB') {
                #修改数据库
                if ($info['vmark'] == 5) {
                    Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('voucher_over' => $info['val']));

                } else if ($info['vmark'] == 6) {
                    Db::table('redpacket_detail')->where(array('id' => $info['id']))->update(array('voucher_number' => $info['val']));


                }


            } else {
                if (!empty($_session)) {
                    if ($info['vmark'] == 5) {
                        $_session[$info['k']]['voucher_over'] = $info['val'];

                    } else if ($info['vmark'] == 6) {
                        $_session[$info['k']]['voucher_number'] = $info['val'];

                    }


                }
            }
            $packetId = Session('packetId');
            if (!empty($packetId)) {
                $arr = PacketService::getPacketContent($packetId);

            }
            $_str .= TaskService::getVoucherArrInfo($arr, 'DB');

            Session::set('VoucherInfo', $_session);

            $_str .= TaskService::getVoucherArrInfo($_session, 'S');

            return array('status' => true, 'data' => $_str);
        }
    }


    /**********************************任务管理 end ***********************************************/
    /**********************************签到管理 start ***********************************************/

    /**
     * [sign_index 签到列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function sign_index()
    {
        if (input("get.action") == 'ajax') {
//            $_where='task_status != 3';
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => false, //是否分页
                'number' => $listRows, //分页个数
//                'where'     => $_where,//查找条件
            );
            $data = SignService::SignIndex($data_params);
            return $this->jsonSuccessData($data);
        } else {
            #查询一下 开关状态
            $switch_status = Db::table('switch')->where(array('switch_type' => 'sign'))->value('switch_status');

            $this->assign('switch_status', $switch_status);
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    #整理缓存数据和数据库数据显示在页面
    function signSession()
    {
        $params = input("post.");
        #直接存库
        SignService::signSession($params);
        return json(DataReturn('保存成功', 0));


    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function SignPrize()
    {
        $days_id = input("post.days_id");
        $data = SignService::getSignContent($days_id);
        if (!empty($data)) {
            return array('status' => true, 'data' => $data);

        } else {
            return array('status' => false, 'msg' => '暂无赠送');
        }

    }

    /**
     * [signTypeWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params days_id,   第几天的id
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   3 pro:商品  5 service ： 服务  4 ：红包   1 integral: 积分   2 balance余额
     * @params val,   修改之前 的值
     */
    function signTypeWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {
                    #奖励数量
                    Db::table("signs")->where(array('id' => $info['id']))->update(array('signs_number' => $info['value']));

                } else if ($info['vmark'] == 6) {

                    # 直接操作数据库 有效期
                    Db::table("signs")->where(array('id' => $info['id']))->update(array('prize_indate' => $info['value']));
                }

                $data = SignService::getSignContent($info['daysId']);

                return array('status' => true, 'data' => $data);

            }
        }
    }

    /*
     * @content 修改开关状态
     * @param  mark  类型  sing:签到
     * */
    function changeSwitch()
    {
        $mark = input("post.mark");
        $value = input("post.value");

        if (!empty($mark)) {
            Db::table('switch')->where(array('switch_type' => $mark))->update(array('switch_status' => $value));
            return array('status' => true);
        }
    }

    /*
     * @content 修改是否奖励 1 是 2 否
     * @param  sign_id 签到天数id
     * */
    function changeSignStatus()
    {
        $sign_id = input("post.sign_id");
        $value = input("post.value");

        if ($sign_id) {
            Db::table('sign')->where(array('id' => $sign_id))->update(array('sign_status' => $value));
            return array('status' => true);
        }

    }

    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 签到统计
     */
    function sign_static()
    {
        if (input("get.action") != 'ajax') {
            return $this->fetch();
        } else {
            $where = null;
            $start_time = input("post.start_time");
            $end_time = input("post.end_time");
            if (empty($start_time)) {
                $start_time = date("Y-m");
            }
            if (empty($end_time)) {
                $end_time = date("Y-m");
            }
            $day_list = Db::table("log_signs ls")
                ->field("date(ls.log_time) sign_date,count(distinct ls.member_id) sign_member")
                ->where("date_format(ls.log_time,'%Y-%m')>='" . $start_time . "' and date_format(ls.log_time,'%Y-%m')<='" . $end_time . "'")
                ->group("date_format(ls.log_time,'%Y-%m-%d')")
                ->order("sign_date")
                ->select();

            $month_list = Db::table("log_signs ls")
                ->field("date_format(ls.log_time,'%Y-%m') sign_date,count(ls.member_id) sign_member")
                ->where("date_format(ls.log_time,'%Y')>='" . date("Y", strtotime($start_time)) . "' and date_format(ls.log_time,'%Y')<='" . date("Y", strtotime($end_time)) . "'")
                ->group("date_format(ls.log_time,'%Y-%m')")
                ->order("sign_date")
                ->select();

            $_return_day = array("date_array" => array_column($day_list, 'sign_date'), "data_array" => array(array("name" => "日签到人数", "type" => 'line', "data" => array_column($day_list, "sign_member"))));
            $_return_month = array("date_array" => array_column($month_list, "sign_date"), "data_array" => array(array("name" => "月签到人数", "type" => "line", "data" => array_column($month_list, "sign_member"))));
            return array("status" => true, "day" => $_return_day, "month" => $_return_month);

        }
    }

    /**********************************签到管理 end ***********************************************/
    /**********************************红包管理 start ***********************************************/
    /**
     * [packet_index 红包列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function packet_index()
    {
        if (input("get.action") == 'ajax') {
            $searchParams = input("post.searchParams");
            $_where = 'type = 1 and status = 1';

            $time = date("Y-m-d H:i:s");
            if (!empty($searchParams['status'])) {
                if ($searchParams['status'] == 1) {
                    #待上架
                    $_where .= " and '$time' < create_time and '$time' < end_time";

                } else if ($searchParams['status'] == 2) {
                    #已上架
                    $_where .= " and '$time' >= create_time and '$time' <= end_time";
                } else if ($searchParams['status'] == 3) {
                    #已下架
                    $_where .= " and '$time' > create_time and '$time' > end_time";
                }


            }
            if (!empty($searchParams['person'])) {
                $_where .= " and person = '" . $searchParams['person'] . "'";
            }
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = PacketService::packetIndex($data_params);

            $total = PacketService::PacketIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    /**
     * [packet_save 红包添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function packet_save()
    {
        $packet_id = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = input("post.");
            $data = PacketService::packetSave($params);
            return $data;

        } else {
            if (!empty($packet_id)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('id' => $packet_id),//查找条件
                );
                $data = PacketService::packetIndex($data_params);
                #查询红包类型信息
                $packetType = PacketService::getTaskContent($packet_id);
                $person_level = explode(',', $data[0]['person_level']);
                $this->assign('person_level', $person_level);
                # 查询会员等级
                $level = Db::table("member_level")->field(array("level_title", "id"))->select();
                $this->assign('level', $level);
                #缓存存活动id
                Session::set('repacketId', $packet_id);
                #赋值 上架数量

                $this->assign('packetType', $packetType);
                $this->assign('packet', $data[0]);
            }
            if (empty($packet_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('repacketId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('VoucherInfo');
            Session::delete('redpacketTitlePrizeInfo');
            Session::delete('packetId');


            return $this->fetch();
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function packetSession()
    {
        #获取缓存活动id 只有修改时候有
        $packetId = Session('repacketId');
        $params = input("post.");

        $data = PacketService::packetSession($params, $packetId);
        return array('status' => true, 'data' => $data);


    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getRedpacketList()
    {
        $params = input("post.");
        $data = PacketService::getRedpacketList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [redpacketWrite 修改红包名称 价值]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     */
    function redpacketWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 1) {
                    #奖励数量
                    Db::table("redpacket_title")->where(array('id' => $info['id']))->update(array('redpacket_title' => $info['value']));

                } else if ($info['vmark'] == 2) {

                    # 直接操作数据库 有效期
                    Db::table("redpacket_title")->where(array('id' => $info['id']))->update(array('price' => $info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('redpacketTitlePrizeInfo');
                if (!empty($_session)) {

                    if ($info['vmark'] == 1) {
                        #奖励数量
                        $_session[$info['k']]['redpacket_title'] = $info['value'];

                    } else if ($info['vmark'] == 2) {
                        #有效期
                        $_session[$info['k']]['price'] = $info['value'];

                    }

                    #存session
                    Session::set('redpacketTitlePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [redpacketDelInfo   红包名称 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function redpacketDelInfo()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                # 直接操作数据库 删除红包名称  和 红包名称 对应的详情
                DB::table('redpacket_title')->where(array('id' => $info['id']))->update(array('status' => '2'));
                DB::table('redpacket_detail')->where(array('redpacket_id' => $info['id']))->update(array('status' => '2'));

                return array('status' => true);
            }
        } else {
            # 操作session
            $_session = Session('redpacketTitlePrizeInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('redpacketTitlePrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    /**********************************红包管理 end ***********************************************/
    /**********************************积分兑换管理 start ***********************************************/
    /**
     * [sign_index 签到列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function integral_index()
    {
        if (input("get.action") == 'ajax') {
            $search = input("post.searchParams");
            $_where = 'ie.status= 1';
            if (!empty($search['keywords'])) {
//                $_where .= ['exchange_title', 'like', '%'.$search['keywords'].'%'];
                $_where .= ' and ie.exchange_title like  "%' . $search['keywords'] . '%"   ';

            }
            if (!empty($search['exchange_type'])) {
                $_where .= ' and iep.exchange_type = "' . $search['exchange_type'] . '"';

            }
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = ExchangeService::integralIndex($data_params);

            $total = ExchangeService::integralIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    /**
     * [integral_save 积分兑换添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function integral_save()
    {
        $exchange_id = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = array_filter(input("post."));
            $data = ExchangeService::integralSave($params);
            return $data;

        } else {

            if (!empty($exchange_id)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('ie.id' => $exchange_id),//查找条件
                );
                $data = ExchangeService::integralIndex($data_params);
                $bannerArr = explode(',', $data[0]['banner']);

                #查询红包类型信息
                $integralType = ExchangeService::getIntegralContent($exchange_id);

                #缓存存活动id
                Session::set('exchangeId', $exchange_id);
                #赋值 上架数量

                $this->assign('integralType', $integralType);
                $this->assign('bannerArr', $bannerArr);

                $this->assign('exchange', $data[0]);
                $this->assign('sort_number', $data[0]['sort_number']);
            }else{
                # 查询最大的排序
                $sort_number = Db::table('integral_exchange')->field('sort_number')->max('sort_number');
                $this->assign('sort_number', $sort_number+1);
            }

            $params['id'] = empty($params['id']) ? '' : $params['id'];
            //多余图片处理
            ResourceService::$session_suffix = 'source_upload' . 'integral_exchange' . $params['id'];
            ResourceService::delUploadCache();

            if (empty($exchange_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('exchangeId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('VoucherInfo');
            #删除添加 商品 服务 会员 赠送 余额  缓存
            Session::delete('integralExchangeInfo');


            return $this->fetch();
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function integralSession()
    {
        #获取缓存活动id 只有修改时候有
        $taskId = Session('exchangeId');
        $params = input("post.");

        $data = ExchangeService::integralSession($params, $taskId);
        return array('status' => true, 'data' => $data);


    }

    /**
     * [taskTypeWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   3 pro:商品  5 service ： 服务  4 ：红包   1 integral: 积分   2 balance余额
     * @params val,   修改之前 的值
     */
    function integralTypeWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 3) {
                    #奖励数量
                    Db::table("integral_exchange_prize")->where(array('id' => $info['id'], 'exchange_type' => $info['type']))->update(array('exchange_num' => $info['value']));

                } else if ($info['vmark'] == 5) {

                    # 直接操作数据库 有效期
                    Db::table("integral_exchange_prize")->where(array('id' => $info['id'], 'exchange_type' => $info['type']))->update(array('exchange_indate' => $info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('integralExchangeInfo');


                if (!empty($_session)) {

                    if ($info['vmark'] == 3) {
                        #奖励数量
                        $_session[$info['k']]['exchange_num'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        #有效期
                        $_session[$info['k']]['exchange_indate'] = $info['value'];

                    }
//                    dump($_session);die;

                    #存session
                    Session::set('integralExchangeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getIntegralTypeList()
    {
        $params = input("post.");
        $data = ExchangeService::getIntegralTypeList();
        return array('status' => true, 'data' => $data);
    }

    /**
     * [integralTypeDel  删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function integralTypeDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                #赠送
                # 直接操作数据库
                DB::table('integral_exchange_prize')->where(array('id' => $info['id']))->delete();
                return array('status' => true);
            }
        } else {
            # 操作session
            #赠送
            $_session = Session('integralExchangeInfo');
            if (isset($info['k'])) {
                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('integralExchangeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }


    /**********************************积分兑换管理 end ***********************************************/
    /**********************************大转轮管理 start ***********************************************/
    /**
     * [runner_index 大转轮列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function runner_index()
    {
        if (input("get.action") == 'ajax') {
            $_where = [
                ['wheel_status', '=', 1],
            ];

            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = RunnerService::runnerIndex($data_params);
            $total = RunnerService::runnerIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    /**
     * [runner_save 大转轮添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function runner_save()
    {
        $runner_id = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = array_filter(input("post."));
            $data = RunnerService::runnerSave($params);
            return $data;

        } else {

            if (!empty($runner_id)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('id' => $runner_id),//查找条件
                );
                $data = RunnerService::runnerIndex($data_params);

                #查询红包类型信息
                $integralType = RunnerService::getRunnerContent($runner_id);

                #缓存存活动id
                Session::set('runnerId', $runner_id);
                #赋值 上架数量
                $this->assign('runnerType', $integralType['redpacket_detail']);
                $this->assign('percent', $integralType['percent']);

                $this->assign('runner', $data[0]);
            }

            if (empty($runner_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('runnerId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('VoucherInfo');
            #删除添加 商品 服务 会员 赠送 余额  缓存
            Session::delete('RunnerPrizeInfo');
            #删除 红包id
            Session::delete('packetId');


            return $this->fetch();
        }

    }

    #整理缓存数据和数据库数据显示在页面
    function RunnerSession()
    {
        #获取缓存活动id 只有修改时候有
        $taskId = Session('runnerId');
        $params = input("post.");

        $data = RunnerService::runnerSession($params, $taskId);
        #存session
        return array('status' => true, 'data' => $data['str'], 'count' => $data['count']);

    }

    #修改 删除 之后 显示的页面（活动类型  活动赠送）
    function getRunnerTypeList()
    {
        $params = input("post.");
        $data = RunnerService::getRunnerTypeList();
        return array('status' => true, 'data' => $data['str'], 'count' => $data['count']);
    }

    /**
     * [runnerWrite 修改数量 有效期]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params value, 要修改的值
     * @params mark,    Db::数据库 S ：缓存
     * @params vmark,   修改的第几项
     * @params type,   3 pro:商品  5 service ： 服务  4 ：红包   1 integral: 积分   2 balance余额
     * @params val,   修改之前 的值
     */
    function runnerWrite()
    {
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {
                    #奖励数量
                    Db::table("wheel_prize")->where(array('id' => $info['id']))->update(array('give_num' => $info['value']));

                } else if ($info['vmark'] == 5) {
                    $arr = array();
                    #剩余数量 = 原来的剩余数量 +修改的数量- 原来的上架数量
                    $arr['surplus_num'] = $info['surplus_num'] + $info['value'] - $info['val'];
                    $arr['ground_num'] = $info['value'];

                    # 直接操作数据库
                    Db::table("wheel_prize")->where(array('id' => $info['id']))->update($arr);
                } else if ($info['vmark'] == 6) {

                    # 直接操作数据库 中奖概率
                    Db::table("wheel_prize")->where(array('id' => $info['id']))->update(array('prize_probability' => $info['value']));
                } else if ($info['vmark'] == 7) {

                    # 直接操作数据库 有效期
                    Db::table("wheel_prize")->where(array('id' => $info['id']))->update(array('prize_indate' => $info['value']));
                }

                return array('status' => true);

            } else {
                # 操作session
                #活动赠送
                $_session = Session('RunnerPrizeInfo');


                if (!empty($_session)) {

                    if ($info['vmark'] == 4) {
                        #奖励数量
                        $_session[$info['k']]['give_num'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        $surplus_num = $info['surplus_num'] + $info['value'] - $info['val'];
                        #修改上架数量 也同时修改 剩余数量
                        $_session[$info['k']]['surplus_num'] = $surplus_num;
                        $_session[$info['k']]['ground_num'] = $info['value'];

                    } else if ($info['vmark'] == 6) {
                        #有效期
                        $_session[$info['k']]['prize_probability'] = $info['value'];

                    } else if ($info['vmark'] == 7) {
                        #有效期
                        $_session[$info['k']]['prize_indate'] = $info['value'];

                    }
//                    dump($_session);die;

                    #存session
                    Session::set('RunnerPrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [runnerTypeDel 大转轮  创建商品  赠送 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params id,  数据库中要修改的对应id
     * @params k,   缓存要修改的对应key值
     * @params mark,    Db::数据库 S ：缓存
     */
    function runnerTypeDel()
    {
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {

                #赠送
                # 直接操作数据库
                DB::table('wheel_prize')->where(array('id' => $info['id']))->delete();

                return array('status' => true);
            }
        } else {
            # 操作session
            #赠送
            $_session = Session('RunnerPrizeInfo');

            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);

                    #赠送
                    Session::set('RunnerPrizeInfo', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    function getRedpacket()
    {
        $prize_id = input("post.id");
        $_str = '';
        if (!empty($prize_id)) {

            #查询红包类型的奖品详情
            $repacketId = Db::table('wheel_prize')->where(array('id' => $prize_id, 'prize_type' => 4))->value('spoil_id');
            #存一下红包id
            Session::set('packetId', $repacketId);
            if (!empty($repacketId)) {
                $arr = PacketService::getPacketContent($repacketId);

            }
            $_str .= TaskService::getVoucherArrInfo($arr, 'DB');

        }
        #查询是否有红包详情缓存
        $_session = session('VoucherInfo');
        if (!empty($_session)) {
            $_str .= TaskService::getVoucherArrInfo($_session, 'S');
        }
        return array('status' => true, 'data' => $_str);


    }

    /**
     * [runner_check 查看详情 参与人数统计 奖品领取统计]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function runner_check()
    {
        #大转轮id
        $date = input("post.search_time");
        if (empty($date)) {
            $date = date("Y-m");
        }
        $wheelId = input("get.wheelId");
        if (input("get.action") == 'ajax') {
            $draw = DB::query("select count(distinct ld.member_id) as num,date_format(ld.draw_time,'%Y-%m-%d') as days
                                          from log_draw as ld,wheel_prize as wp where date_format(ld.draw_time,'%Y-%m')='" . $date . "'
                                           and ld.bear_status =1 and wp.wheel_id ={$wheelId} and ld.prize_id = wp.id 
                                           group by date_format(ld.draw_time,'%Y-%m-%d') order by ld.draw_time");
            if (!empty($draw)) {
                $_date = array();
                $_value = array();
                $_prop = array();
                foreach ($draw as $v) {
                    // or mm.member_level_id=2
                    $member = DB::query("select count(distinct ld.member_id) as allcount,
                                          (select count(distinct ld.member_id) from log_draw as ld,wheel_prize as wp,member_mapping as mm where 
                                           date_format(ld.draw_time,'%Y-%m-%d')='" . $v['days'] . "' and  ld.bear_status =1 and wp.wheel_id ={$wheelId} 
                                           and ld.prize_id = wp.id and ld.member_id =mm.member_id and mm.member_level_id=0 and mm.member_code is not null) as nonmember,
                                           
                                           (select count(distinct ld.member_id) from log_draw as ld,wheel_prize as wp,member_mapping as mm where 
                                           date_format(ld.draw_time,'%Y-%m-%d')='" . $v['days'] . "' and  ld.bear_status =1 and wp.wheel_id ={$wheelId} 
                                           and ld.prize_id = wp.id and ld.member_id =mm.member_id and mm.member_level_id=0 and mm.member_code is null) as noregister,
                                           
                                           (select count(distinct ld.member_id) from log_draw as ld,wheel_prize as wp,member_mapping as mm where 
                                           date_format(ld.draw_time,'%Y-%m-%d')='" . $v['days'] . "' and  ld.bear_status =1 and wp.wheel_id ={$wheelId} 
                                           and ld.prize_id = wp.id and ld.member_id =mm.member_id and (mm.member_level_id=1) 
                                           and mm.member_expiration <'" . date("Y-m-d") . "') as pastmember,
                                           
                                           (select count(ld.member_id)  from log_draw as ld,wheel_prize as wp where date_format(ld.draw_time,'%Y-%m-%d')='" . $v['days'] . "'
                                           and ld.bear_status =1 and wp.wheel_id ={$wheelId} and ld.prize_id = wp.id) as drawcount
                                           
                                          from log_draw as ld,wheel_prize as wp,member_mapping as mm 
                                          where date_format(ld.draw_time,'%Y-%m-%d')='" . $v['days'] . "' 
                                         and ld.bear_status =1 and wp.wheel_id ={$wheelId} and ld.prize_id = wp.id
                                         and ld.member_id =mm.member_id");
                    $member[0]['nowmember'] = $member[0]['allcount'] - $member[0]['nonmember'] - $member[0]['noregister'] - $member[0]['pastmember'];
                    $member[0]['notmember'] = $member[0]['nonmember'] + $member[0]['pastmember'];
                    $member[0]['averg'] = $member[0]['drawcount'] / $v['num'];

                    array_push($_prop, array("nowmember" => $member[0]['nowmember'], "notmember" => $member[0]['notmember'], "noregister" => $member[0]['noregister'], "averg" => $member[0]['averg']));
                    array_push($_date, $v['days']);
                    array_push($_value, $v['num']);
                }
                echo(json_encode(array("status" => true, "date" => $_date, "value" => $_value, "prop" => $_prop)));

//                return array("status"=>true,"date"=>$_date,"value"=>$_value,"prop"=>$_prop);

            } else {
                return array('status' => false);

            }


        } else {
            $this->assign('wheelId', $wheelId);
            return $this->fetch();
        }
    }

    /*
     * @content 奖品统计
     * */
    function prize_static()
    {
        $wheelId = input("get.wheelId");
        $prize = DB::query("select wp.prize_title,wp.ground_num,wp.id,surplus_num,
                                  (select count(member_id) from log_draw as ld where ld.prize_id=wp.id and ld.bear_status =1 and ld.get_status=2) as lq 
                                  from wheel_prize as wp where wheel_id={$wheelId} and prize_status=1");
        if (!empty($prize)) {
            $_title = array();
            foreach ($prize as $k => $v) {

                array_push($_title, $v['prize_title']);
            }
            echo(json_encode(array("status" => true, "title" => $_title, "prize" => $prize)));

//            return array("status"=>true,"title"=>$_title,"prize"=>$prize);

        } else {
            return array('status' => false);
        }
    }
    /**********************************大转轮管理 end ***********************************************/
    /**********************************活动提示管理 start***********************************************/
    /**
     * [runner_index 活动提示管理列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月22日13:23:09
     */
    function activetips_index()
    {
        if (input("get.action") == 'ajax') {
            $searchParams = input("post.searchParams");
            $_where = "status = 1";
            $time = date("Y-m-d H:i:s");
            if (!empty($searchParams['status'])) {
                if ($searchParams['status'] == 1) {
                    #待上架
                    $_where .= " and '$time' < active_start and '$time' < active_end";

                } else if ($searchParams['status'] == 2) {
                    #已上架
                    $_where .= " and '$time' >= active_start and '$time' <= active_end";
                } else if ($searchParams['status'] == 3) {
                    #已下架
                    $_where .= " and '$time' > active_start and '$time' > active_end";
                }


            }


            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = ActivetipsService::activetipsIndex($data_params);
            $total = ActivetipsService::activetipsIndexCount();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $data, 'total' => $total]];
        } else {
            #删除 门店id
            Session::delete('bizId');
            return $this->fetch();


        }

    }

    /**
     * [activetips_save 活动提示管理添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activetips_save()
    {
        $activetipsId = input("get.id");

        if (input('get.action') == 'ajax') {
            $params = input("post.");
            $data = ActivetipsService::activetipsSave($params);
            return $data;

        } else {

            if (!empty($activetipsId)) {
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => array('id' => $activetipsId),//查找条件
                );
                $data = ActivetipsService::activetipsIndex($data_params);

                #查询活动类型信息
//                $activeType = ActivetipsService::getActiveTipsContent($activetipsId);
                $activeType['jump_type_title'] = '';
                $activeType['jump_type_detail_title'] = '';
                if ($data[0]['is_relation'] == 1 and $data[0]['is_type'] == 2) {
                    $activeType['jump_type_title'] = ApplayService::JumpTypeTitle($data[0])['type'];
                    $activeType['jump_type_detail_title'] = ApplayService::JumpTypeTitle($data[0])['name'];
                }
                #缓存存活动id
                Session::set('activetipsId', $activetipsId);
                $this->assign('activeType', $activeType);
                $this->assign('active', $data[0]);
            }else{
                $data[0]['is_relation']=2;
                $this->assign('active', $data[0]);
            }

            if (empty($runner_id))
                #添加操作的时候删除活动缓存id  还有修改保存时
                Session::delete('activetipsId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('ActivetipsTitle');
            $this->assign('image_jump_type', lang('image_jump_type'));
            return $this->fetch();
        }

    }

    /**
     * [activetips_save 活动提示管理类型]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params  $type 1"服务详情",2"关联活动",3"办理会员活动","4二手车","5联盟商家"
     */
    function activetipsType()
    {
        $type = input("post.type");
        $mark = input("post.mark");
        $keywords = input("post.keywords");
        if (empty($type)) {
            $type = input("get.type");
        }
        if ($type == 1) {
            $title = '服务详情';
        } else if ($type == 2) {
            $title = '关联活动';

        } else if ($type == 3) {
            $title = '办理会员活动';

        } else if ($type == 4) {
            $title = '二手车';

        } else {
            $title = '联盟商家';

        }
        if ($mark == 'detail') {

            $data = ActivetipsService::activetipsType($type, $keywords);

        } else if ($mark == 'nosearch') {
            $data = ActivetipsService::activetipsType($type, $keywords);
        } else {
            #查询列表
            if ($type == 4) {
                $param['title'] = '二手车';
            } else if ($type == 5) {
                $param['title'] = '联盟商家';

            }
            $data = ActivetipsService::getActiveOptionInfo($param, '列表关联');

        }
        if (!empty($data)) {
            return array('status' => true, 'data' => $data, 'title' => $title);

        } else {
            return array('status' => false, 'msg' => '未搜索到相关数据');
        }

    }

    /**
     * [searchActivceNmae 搜索活动名称]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function searchActiveName()
    {
        $search_title = input("post.search_title");
        $activeType = input("post.activeType");
        $params['param']['type'] = $activeType;
        $params['param']['keywords'] = strim($search_title);
        $data = ApplayService::SearchByCateAndKeywordsGetData($params);


        #分页
        if (!empty($data)) {
            $re = ActivetipsService::getActiveArrInfo($data, $activeType, '', $search_title);
            return array('status' => true, "data" => $re);
        } else {
            return array("status" => false, 'msg' => '暂无数据');
        }

    }

    /**
     * [getActiveOption 关联活动  推荐商户 推荐二手车]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function getActiveOption()
    {
        $params = input("post.");
        $type = $params['type'];
        if (!empty($params)) {
            #存缓存
            Session::set('ActivetipsTitle', $params);
            if ($type == 1) {
                $title = '服务详情';
            } else if ($type == 2) {
                $title = '关联活动';

            } else if ($type == 3) {
                $title = '办理会员活动';

            } else if ($type == 4) {
                $title = '二手车';

            } else {
                $title = '联盟商家';

            }

            $re = ActivetipsService::getActiveOptionInfo($params, $title);

            return array('status' => true, "data" => $re);


        }

    }

    /**********************************活动提示管理 end ***********************************************/

    function consump_voucher()
    {
        if (input("get.action") == 'ajax') {
            $param = input("post.");
            if ($param['active_types'] == 1) {
                # 活动创建
                $param = $this->activeParamFormat($param);
                $data = ActiveService::activeSession($param, $param['active_id']);
            } elseif ($param['active_types'] == 2) {
                $param = $this->taskParamFormat($param);
                $data = TaskService::taskSession($param, $param['task_id']);
            } elseif ($param['active_types'] == 3) {
                $param = $this->signParamFormat($param);
                SignService::signSession($param);
            } elseif ($param['active_types'] == 4) {
                $param = $this->runnerParamFormat($param);
                $data = RunnerService::runnerSession($param, '');
            } elseif ($param['active_types'] == 5) {
                $param = $this->packetParamFormat($param);
                $data = $data = TaskService::voucherSession($param);
            } elseif ($param['active_types'] == 6) {
                $param = $this->integralParamFormat($param);
                $data = ExchangeService::integralSession($param, '');
            } elseif ($param['active_types'] == 7) {
                # 办理会员套餐
                $param = $this->setMealParamFormat($param);
                $data = SetmealService::setmealSession($param, $param['days_id']);
            } elseif ($param['active_types'] == 8) {
                # 会员升级赠送
                $param = $this->setMealParamFormat($param);
                $data = SetmealService::upgradeSession($param, $param['days_id']);
            } elseif ($param['active_types'] == 9) {
                # 会员升级赠送
                $param = $this->setMealParamFormat($param);
                $data = SetmealService::upgradeSession($param, $param['days_id']);
            }
            return array("status" => true, "data" => $data);
        } else {
            # 营销活动的类型
            $active_type = input("get.active_types");
            $this->assign('active_types', $active_type);
            if ($active_type == 3 or $active_type == 7 or $active_type == 8) {
                $this->assign('days_id', input("get.days_id"));
            }
            # 消费券的类型 可以为NULL
            $type = input("get.type");
            $this->assign("type", $type);
            return $this->fetch();
        }
    }

    /**
     * @return array
     * @context 获取商家分类
     */
    function getsMerchantCate()
    {
        # 分类
        $is_type = input("post.is_type");
        # 一级分类id , 用于查询二级分类id
        $pid = input("post.pid");
        $MerchantService = new MerchantService();
        $list = $MerchantService->cateLevel($is_type,$pid);
        return array("list" => $list);
    }

    /**
     * @param $param
     * @return mixed
     * @context 活动创建字段格式化
     */
    function activeParamFormat($param)
    {
        if($param['is_type']==1){
            $param['prize_type'] = "lifeVoucher";
            $param['active_type'] = 8;
        }elseif($param['is_type']==2){
            $param['prize_type'] = "merchantVoucher";
            $param['active_type'] = 7;
        }elseif($param['is_type']==3){
            $param['prize_type'] = "propertyVoucher";
            $param['active_type'] = 9;
        }
        $param['mark'] = 'give';
        $param['detail_number'] = $param['num'];
        $param['detail_price'] = $param['price'];
        $param['money_off'] = $param['min_consume'];
        $param['expiration'] = $param['validity_day'];
        $param['active_title'] = $param['title'];
        return $param;
    }

    /**
     * @return string[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 刷新活动赠送
     */
    function flushActive()
    {
        $str = null;
        $activeId = session("activeId");
        if (!empty($activeId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            #赠送表
            $pro = Db::table('active_giving')->field('id,giving_type active_type,giving_id detail_id,giving_number detail_number,giving_price detail_price,expiration')->where(array('active_id' => $activeId))->select();
            foreach ($pro as $k => $v) {
                if ($v['active_type'] == 1) {
                    #商品 $v['detail_id'] 为商品卡券id
                    $pro[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['detail_id'], 'pro');
                } else if ($v['active_type'] == 2) {
                    #服务 $v['detail_id'] 为服务卡券id
                    $pro[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['detail_id'], 'service');
                } else if ($v['active_type'] == 4) {
                    #积分
                    $pro[$k]['title'] = '积分';
                } else if ($v['active_type'] == 5) {
                    #余额
                    $pro[$k]['title'] = '余额';
                } else if ($v['active_type'] == 7 or $v['active_type'] == 8) {
                    $pro[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['detail_id'], 'merchantVoucher');
                }

            }
            $str = ActiveService::getActiveArrInfo($pro, 'DB', '', 'give');
        }
        $_sessionActiveTypeInfo = Session('ActiveGiveInfo');
        $str .= ActiveService::getActiveArrInfo($_sessionActiveTypeInfo, 'S', '', 'give');
        return array("str" => $str);
    }

    function taskParamFormat($param)
    {
        if($param['is_type']==1){
            $param['prize_type'] = "lifeVoucher";
        }elseif($param['is_type']==2){
            $param['prize_type'] = "merchantVoucher";
        }elseif($param['is_type']==3){
            $param['prize_type'] = "propertyVoucher";
        }
        $param['intbal'] = $param['price'];
        $param['prize_num'] = $param['num'];
        $param['prize_indate'] = $param['validity_day'];
        $param['prize_title'] = $param['title'];
        $param['surplus_num'] = $param['ground_num'];
        return $param;
    }

    function flushTask()
    {
        $taskId = session("taskId");
        $_sessionActiveTypeInfo = Session('TaskPrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在任务id
        if (!empty($taskId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = TaskService::getTaskContent($taskId);
            $_str = TaskService::getTaskPrizeInfo($pro, 'DB');
        }
        #type  :  表示创建  give  表示赠送
        $_str .= TaskService::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return array("str" => $_str);
    }

    function signParamFormat($param)
    {
        if($param['is_type']==1){
            $param['signs_type'] = "lifeVoucher";
        }elseif($param['is_type']==2){
            $param['signs_type'] = "merchantVoucher";
        }elseif($param['is_type']==3){
            $param['signs_type'] = "propertyVoucher";
        }
        $param['intebal_num'] = $param['price'];
        $param['signs_title'] = $param['title'];
        $param['signs_number'] = $param['num'];
        $param['prize_indate'] = $param['validity_day'];
        $param['surplus_num'] = $param['ground_num'];
        return $param;
    }

    function runnerParamFormat($param)
    {
        if($param['is_type']==1){
            $param['prize_type'] = "lifeVoucher";
        }elseif($param['is_type']==2){
            $param['prize_type'] = "merchantVoucher";
        }elseif($param['is_type']==3){
            $param['prize_type'] = "propertyVoucher";
        }
        $param['intebal_num'] = $param['price'];
        $param['give_num'] = $param['num'];
        $param['prize_indate'] = $param['validity_day'];
        $param['surplus_num'] = $param['ground_num'];
        $param['prize_title'] = $param['title'];
        return $param;
    }

    function flushRunner()
    {
        $taskId = Session('runnerId');
        $_sessionActiveTypeInfo = Session('RunnerPrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在$taskId  大转轮id
        if (!empty($taskId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = RunnerService::getRunnerContent($taskId);
            $_str = RunnerService::getTaskPrizeInfo($pro['redpacket_detail'], 'DB');
            $_DbCount = RunnerService::getTaskPrizeCount($pro['redpacket_detail']);
        }
        $_str .= RunnerService::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        $_Scount = RunnerService::getTaskPrizeCount($_sessionActiveTypeInfo);
        $_count = intval($_DbCount) + intval($_Scount);
        return array("str" => $_str, "count" => $_count);
    }

    function packetParamFormat($param)
    {
        $param['voucher_type'] = $param['is_type'] == 2 ? 7 : ($param['is_type'] == 3 ? 9 : ($param['is_type'] == 4 ? 10 : 8));;
        $param['voucher_price'] = $param['price'];
        $param['money_off'] = $param['min_consume'];
        $param['voucher_over'] = $param['validity_day'];
        $param['voucher_title'] = $param['title'];
        $param['voucher_number'] = $param['num'];
        return $param;
    }

    function flushPacket()
    {
        $_str = '';
        $_sessionActiveTypeInfo = Session("VoucherInfo");
        $_str = '';
        #packetId 关联红包详情的id
        $packetId = Session('packetId');
        if (!empty($packetId)) {
            $packet = PacketService::getPacketContent($packetId);
            $_str .= TaskService::getVoucherArrInfo($packet, 'DB');
        }

        $_str .= TaskService::getVoucherArrInfo($_sessionActiveTypeInfo, 'S');
        return array("str" => $_str);
    }

    function integralParamFormat($param)
    {
        $param['exchange_type'] = $param['is_type'] == 2 ? 'merchantVoucher' : $param['is_type'] == 3 ? 'propertyVoucher' :$param['is_type'] == 4 ? 'youpaiVoucher' :'lifeVoucher';
        $param['exchange_title'] = $param['title'];
        $param['exchange_num'] = $param['num'];
        $param['exchange_indate'] = $param['validity_day'];
        return $param;
    }

    function flushIntegral()
    {
        $_str = '';
        $_sessionActiveTypeInfo = Session('integralExchangeInfo');
        $_str .= ExchangeService::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S',0);
        return array("str" => $_str);
    }

    function setMealParamFormat($param)
    {
        $param['prize_type'] = $param['is_type'] == 2 ? 'merchantVoucher' : $param['is_type'] == 3 ? 'propertyVoucher' :$param['is_type'] == 4 ? 'youpaiVoucher' :'lifeVoucher';
        $param['intbal'] = $param['price'];
        $param['giving_number'] = $param['num'];
        $param['card_time'] = $param['validity_day'];
        return $param;
    }

    /**
     * @return array|\think\response\View
     * @throws \think\exception\DbException
     * @context 投放卡券
     */
    function launch_voucher()
    {
        if(input("get.action")=='ajax'){
            $where = "launch_status = 2 and status =1";
            $list = Db::table("merchants_voucher")->where($where)
                ->order("id desc")
                ->paginate(10, false, ['query' => request()->param()]);
            $list = $list->toArray()['data'];
            if(!empty($list)){
                foreach($list as &$v){
                    if($v['start_time']>date("Y-m-d H:i:s")){
                        $v['voucher_status']=1;
                    }else if($v['end_time']<=date("Y-m-d H:i:s")){
                        $v['voucher_status']=3;
                    }else{
                        $v['voucher_status']=2;
                    }
                }
            }
            $total = Db::table('merchants_voucher')->where($where)->count();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $list, 'total' => $total]];

        }else{
            return view();
        }
    }

    /**
     * @return bool[]|\think\response\View
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 修改投放卡券
     */
    function launch_save()
    {
        if(input("get.action")=='ajax'){
            $param = input("post.");
            if(!empty($param['id'])){
                # 修改
                $id = $param['id'];
                unset($param['id']);
               Db::table("merchants_voucher")->where(array("id"=>$id))->update($param);

            }else{
                # 添加
                $param['create_time'] = date("Y-m-d H:i:s");
                $param['surplus_num'] = $param['num'];
                $param['launch_status'] = 2;
                Db::table("merchants_voucher")->insertGetId($param);
            }
            return array("status"=>true);
        }else{
            $id = input("get.id");
            if(!empty($id)){
                $info = Db::table("merchants_voucher")->where(array("id"=>$id))->find();
                $this->assign("info",$info);
            }
            return view();
        }
    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除投放卡券
     */
    function DelLaunch(){
        $id = input("post.id");
        Db::table("merchants_voucher")->where(array("id"=>$id))->update(array("status"=>2));
        return array("status"=>true);
    }

    /**
     * @context 清空卡券缓存
     */
    function cleanVoucherSession(){
        Session::delete("VoucherInfo");
    }

}
