<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\EmployeeService;
use app\service\ResourceService;
use Redis\Redis;
use think\Db;

/**
 * 员工管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月16日11:33:06
 */
class Employee extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月16日11:33:06
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();
    }

    /**
     * [Index 员工列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function Index()
    {

        if (input('get.action') == 'ajax') {
            $redis = new Redis();
            $redis->hDel('employeeInfo', 'Del');
            $redis->hDel('employeeInfo', 'delIds');
            $redis->hDel('employeeInfo', 'RelevancyBiz');
            $redis->hDel('employeeInfo', 'RelevancyBizCount');
            $where = EmployeeService::EmployeeListWhere(input());
            $data_params = array(
                'page' => true,
                'number' => 10,
                'where' => $where,
                'table' => 'employee_sal',
                'order' => 'employee_create desc',

            );
            $data = EmployeeService::EmployeeDataList($data_params);
//            var_dump(Db::table("employee_sal")->getLastSql());
            $total = BaseService::DataTotal('employee_sal', $where);


            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        } else {
            $this->assign('department_arr', Db::name('employee_department')->where('is_del = 2')->field('id,title')->select());
            return $this->fetch();
        }

    }

    /**
     * [SaveInfo 员工添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日13:48:10
     */
    public function SaveData()
    {

        // 参数
        $params = input();
        // 数据
        $data = [
            'calling_status' => 2,
//            'employee_status'=>1,
        ];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where' => ['id' => $params['id']],
                'm' => 0,
                'n' => 1,
                'page' => false,
                'table' => 'employee_sal',
                'order' => 'employee_create desc',
            ];

            $ret = EmployeeService::EmployeeDataList($data_params);

            //
            $data = empty($ret[0]) ? [] : $ret[0];
        }
        //dump($data);exit;
        //多余图片处理
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'employee' . $params['id'];
        ResourceService::delUploadCache();
//
        $this->assign('department_arr', Db::name('employee_department')->where('higher_department_id=0 and is_del = 2')->field('id,title')->select());
        $this->assign('employee_marriage_status', lang('employee_marriage_status'));

        $this->assign('employee_education', lang('employee_education'));

        #看员工是否关联保险后台
        if($data['inquiry_status'] == 1){
            #是  查询  关联的账号
            $inquiry_login = Db::table('inquiry_login')->where(array('employee_id'=>$data['id']))->find();
            $data['login_access'] = $inquiry_login['login_access'];

        }

        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [Save 添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日14:43:59
     */
    public function Save()
    {

        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

        return EmployeeService::EmployeeDataSave($params);


    }

    /**
     * [删除]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelInfo()
    {

        // 是否ajax
        if (!IS_AJAX) {
            return $this->error('非法访问');
        }
        $params = $this->data_post;
        $params['table'] = 'employee_department';
        $params['soft'] = true;
        $params['soft_arr'] = ['is_del' => 1];
        $params['errorcode'] = 19013;
        $params['msg'] = '删除失败';

        BaseService::DelInfo($params);

        return DataReturn('删除成功', 0);
    }

    /**
     * [Index 员工列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function SearchEmployee()
    {
        $params = input();
        $where = EmployeeService::EmployeeListWhere($params);

        $data_params = array(
            'page' => false,
            'where' => $where,
            'table' => 'employee_sal',
            'order' => 'employee_create desc',

        );
        $data = EmployeeService::EmployeeDataList($data_params);
        if ($params['param']['mark'] == 'quit') {
            if (!empty($data)) {
                # 离职员工数据处理
                $str = $this->quitDataHandle($data);
                return array('status' => true, 'str' => $str);
            } else {
                return array('status' => false, 'msg' => '搜索结果为空');
            }
        } else {
            return DataReturn('ok', 0, $data);
        }

    }

    function quitDataHandle($info)
    {
        $str = '';
        if (!empty($info)) {
            # 缓存中删除的员工信息
            $redis = new Redis();
            $employeeRedis = $redis->hGetJson('employeeInfo', 'Del');
            if (empty($employeeRedis)) {
                $employeeRedis = array();
            }
            $delIds = $redis->hGetJson('employeeInfo', 'delIds');
            if (empty($delIds)) {
                $delIds = array();
            }
            $idInfo = array_column($info, 'id');
            $delIdInfo = array_unique(array_merge($delIds, $idInfo));
            $redis->hSetJson('employeeInfo', 'delIds', $delIdInfo);
            foreach ($info as $k => $v) {
                # 添加删除员工信息redis
                $employeeRedis[$v['id']] = array('id' => $v['id']);
                $str .= "<tr id='trInfo{$v['id']}'>
                                <td>{$v['employee_name']}</td>
                                <td>{$v['employee_phone']}</td>";
                # 查询该员工的关联门店/商户信息
                $relationInfo = Db::table('assign_staff')
                    ->field('id,biz_id,type,
                if(type=1,(select biz_title from biz where id = biz_id),(select title from merchants where id = biz_id)) title')
                    ->where(array('employee_id' => $v['id']))
                    ->select();
                if (!empty($relationInfo)) {
                    $str .= "<td><table>";
                    foreach ($relationInfo as $rk => $rv) {
                        if ($rv['type'] == 1) {
                            $type = '门店';
                        } else {
                            $type = '商户';
                        }
                        $str .= "
                            <tr>
                                <td>{$rv['title']}</td>
                                <td>{$type}</td>
                                <td>
                                    <button class=\"layui-btn \" type=\"button\" id='bizButton{$rv['id']}' onclick='Relevancy({$rv['id']})'>重新关联</button>
                                </td>
                            </tr>
                      ";
                    }
                    $str .= "</table></td>";
                } else {
                    $str .= "<td>无</td>";
                }
                $str .= "<td>
                        <input type=\"text\" data-maxDate='" . date('Y-m-d H:i') . "' data-id='{$v['id']}'   class=\"form-control datetime-picker flatpickr-input quit_time\" placeholder=\"请填写离职时间\" readonly=\"readonly\">
                    </td>
                    <!--<td>
                        <button class=\"layui-btn \" type=\"button\" onclick='deleteRow({$v['id']})'>删除</button>
                    </td>-->
                </tr>";
            }
            $redis->hSetJson('employeeInfo', 'Del', $employeeRedis);
        }
        return $str;
    }

    /**
     * [RelateEmployee 获取关联员工]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function RelateEmployee()
    {
        return EmployeeService::HandleDataToHtml(input());
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 员工进行离职操作页面
     */
    function employeeQuit()
    {
        $redis = new Redis();
//        $redis->hDel('employeeInfo', 'Del');
//        $redis->hDel('employeeInfo', 'delIds');
        $this->assign('time', date('Y-m-d 23:59:59'));
        $eid = input('get.eid');
        # 查询员工信息
        $employeeInfo = Db::table('employee_sal')->where(array('id' => $eid))->find();
        //所属岗位
        $employeeInfo['employee_station_title'] = EmployeeService::StationDataHandle($employeeInfo['employee_station_id']);

        //一级部门
        $employeeInfo['department_title'] = EmployeeService::DepartmentDataHandle($employeeInfo['employee_department_id']);

        //二级部门
        $employeeInfo['department_sectitle'] = EmployeeService::DepartmentDataHandle($employeeInfo['employee_department_secid']);
        $this->assign('employeeInfo', $employeeInfo);
        $this->assign('eid', $eid);
        # 缓存中删除的员工信息
        $redis->hSetJson('employeeInfo', 'delIds', $eid);
        $employeeRedis[$eid] = array('id' => $eid);
        $redis->hSetJson('employeeInfo', 'Del', $employeeRedis);
        return $this->fetch();
    }

    /**
     * @return mixed
     * #content 员工进行离职操作时,已重新关联的门店/商户 信息 页面
     */
    function Reassociated()
    {
        $this->assign('eid', input('get.eid'));
        return $this->fetch();
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询员工关联的门店/商户信息
     */
    function relationInfo()
    {
        $eid = input('get.eid');
        # 定义listRows 分页
        $listRows = input('post.limit');
        if (empty($listRows)) {
            $listRows = 10;
        }
        $redis = new Redis();
        $RelevancyBiz = $redis->hGetJson('employeeInfo', 'RelevancyBiz');
        if (is_array($RelevancyBiz)) {
            $RelevancyBizInfo = implode(',', array_keys($RelevancyBiz));
        } else {
            $RelevancyBizInfo = $RelevancyBiz;
        }
        $_where = '';
        if (!empty($RelevancyBizInfo)) {
            $_where = "assign_staff.id not in (" . $RelevancyBizInfo . ")";
        }
        # 已重新关联的数据
        $mark = input('get.mark');
        if (!empty($mark) and $mark == 'Reassociated') {
            if (!empty($RelevancyBizInfo)) {
                $_where = 'assign_staff.id in (' . $RelevancyBizInfo . ')';
            } else {
                $_where = 'assign_staff.id in (0)';
            }
        }
        # 门店/商户 名称搜索
        $searchInfo = input('post.searchInfo');
        $searchWhere = '';
        if (!empty($searchInfo)) {
            $searchWhere = 'if(type=1,b.biz_title  like "%' . $searchInfo . '%",m.title like "%' . $searchInfo . '%")';
        }
        # 查询该员工的关联门店/商户信息
        $relation = Db::table('assign_staff')
            ->field('assign_staff.id,assign_staff.biz_id,type,employee_name employee,
                if(type=1,(b.biz_title),(m.title)) title,
                if(type=1,(b.biz_address),(m.address)) address
                ')
            ->join('employee_sal', 'employee_sal.id=employee_id', 'left')
            ->join('biz b', 'b.id=assign_staff.biz_id', 'left')
            ->join('merchants m', 'm.id=assign_staff.biz_id', 'left')
            ->where(array('employee_id' => $eid))
            ->where($_where)
            ->where($searchWhere)
            ->paginate($listRows, false, ['query' => request()->param()])
            ->toArray();
        $relationInfo = $relation['data'];
        $total = $relation['total'];
        if ($total > 0 and $mark == 'Reassociated') {
            foreach ($relationInfo as $k => $v) {
                $relationInfo[$k]['employee'] = Db::table('employee_sal')->field('employee_name')
                    ->where(array('id' => $RelevancyBiz[$v['id']]))
                    ->find()['employee_name'];
            }
        }
        $reload = input('post.reload');
        if(input('post.page') != 1 or $listRows != 10){
            $reload = 'reload';
        }
        if ($reload != 'reload') {
            $redis = new Redis();
            $redis->hSetJson('employeeInfo', 'RelevancyBizCount', $total);
        }
        return ['code' => 0, 'msg' => '', 'data' => ['data' => $relationInfo, 'total' => $total]];
    }

    /**
     * @return array
     * @content 取消离职
     */
    function deleteEmployeeRow()
    {
        $id = input('post.id');
        if (empty($id)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        $redis = new Redis();
        $employeeRedis = $redis->hGetJson('employeeInfo', 'Del');
        $delIds = $redis->hGetJson('employeeInfo', 'delIds');
        unset($employeeRedis[$id]);
        unset($delIds[$id]);
        if (empty($employeeRedis)) {
            $redis->hDel('employeeInfo', 'Del');
            $redis->hDel('employeeInfo', 'delIds');
        } else {
            $redis->hSetJson('employeeInfo', 'Del', $employeeRedis);
            $redis->hSetJson('employeeInfo', 'delIds', $delIds);
        }
        return array('status' => true);
    }

    /**
     * @return array
     * @content 离职员工输入离职时间
     */
    function employeeQuitTime()
    {
        $id = input('post.id');
        $time = input('post.time');
        if (empty($id) or empty($time)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        $redis = new Redis();
        $employeeRedis = $redis->hGetJson('employeeInfo', 'Del');
        $employeeRedis[$id]['quit_time'] = $time;
        $redis->hSetJson('employeeInfo', 'Del', $employeeRedis);
        return array('status' => true);
    }

    /**
     * @return array
     * @content 门店重新关联员工
     */
    function RelevancyBiz()
    {
        $id = input('post.id');
        $eid = input('post.eid');
        if (empty($id) or empty($eid)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        $redis = new Redis();
        $RelevancyBiz = $redis->hGetJson('employeeInfo', 'RelevancyBiz');
        if (empty($RelevancyBiz)) {
            $RelevancyBiz = array();
        }
        $idInfo = explode(',', $id);
        foreach ($idInfo as $k => $v) {
            $RelevancyBiz[$v] = $eid;
        }
        $redis->hSetJson('employeeInfo', 'RelevancyBiz', $RelevancyBiz);
        return array('status' => true);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 员工进行离职
     */
    function delEmployee()
    {
        $quit_time = input('post.quit_time');
        $redis = new Redis();
        $RelevancyBiz = $redis->hGetJson('employeeInfo', 'RelevancyBiz');
        if (empty($RelevancyBiz)) {
            $RelevancyBiz = array();
        }
        $RelevancyBizCount = $redis->hGetJson('employeeInfo', 'RelevancyBizCount');
        if ($RelevancyBizCount > 0 and count($RelevancyBiz) < $RelevancyBizCount) {
            return array('status' => false, 'msg' => '还有门店/商户没有重新关联员工');
        }
        if (empty($quit_time)) {
            return array('status' => false, 'msg' => '请填写离职时间');
        }
        $employeeRedis = $redis->hGetJson('employeeInfo', 'Del');
        if (!empty($employeeRedis)) {
            foreach ($employeeRedis as $k => $v) {
                Db::table('employee_sal')->where(array('id' => $k))->update(array(
                    'quit_time' => $quit_time,
                    'employee_status' => 0
                ));
            }
        } else {
            return array('status' => false, 'msg' => '暂无离职信息');
        }
        if (!empty($RelevancyBiz)) {
            foreach ($RelevancyBiz as $k => $v) {
                Db::table('assign_staff')->where(array('id' => $k))->update(array(
                    'employee_id' => $v
                ));
                # 查询信息
                $info = Db::table('assign_staff')->where(array('id'=>$k))->find();
                if($info['type']==2){
                    Db::table('merchants')->where(array('id'=>$info['biz_id']))->update(array('relate_employee'=>$v));
                }
            }
        }
        $redis->hDel('employeeInfo', 'Del');
        $redis->hDel('employeeInfo', 'delIds');
        $redis->hDel('employeeInfo', 'RelevancyBiz');
        $redis->hDel('employeeInfo', 'RelevancyBizCount');
        return array('status' => true);
    }
}
