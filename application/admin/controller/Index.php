<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/11
 * Time: 18:32
 */

namespace app\admin\controller;


use app\service\FinanceService;
use app\service\ResourceService;

use app\service\StatisticalService;
use app\service\StatisticMemeberService;
use base\Excel;
use Redis\Redis;
use think\Controller;
use think\Db;
use think\Loader;

class Index extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }
    /*
    * 欢迎页面
    */
    public function WelcomeIndex(){
        return $this->fetch();
    }
   /*
    * 首页
    */
   public function index(){

    
       return $this->fetch();
   }



    /**
     * [DataCenter 数据中心]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
   public function DataCenter()
   {
       $time=explode('-',date('Y-m-d',time()));

       $redis=new Redis();



      //$re=Db::name('sysset')->select();
//

       //地区排名
        $user_ranking=StatisticalService::UserRegisterRanking()['data'];
        // 合作店创建服务/商品审核
       $audit_creation = Db::table('audit_creation')->where(array('state'=>1))->count();
       //加盟店申请
        $biz_franchiser=StatisticalService::BizCount(2);

       //合作店申请
       $biz_cooperation=StatisticalService::BizCount(3);

       //商家申请
       $biz_self_operate=StatisticalService::BizCount(1);

       //代理商申请
       $agent_apply=StatisticalService::BizCount(4);


       //今日新增合伙人
       $biz_partner=StatisticalService::PartnerCount();

       //提现审核
       $deposit_cooperate=StatisticalService::DepositBiz();
       //商户到期
       $merchangts_expiration = StatisticalService::merchangts_expiration();

       //今日总收入

       //$income_info=StatisticalService::IncomeSourceInfo(['year'=>$time[0],'month'=>$time[1],'day'=>$time[2]])['data'];

//       $income_total=0;
//
//       foreach ($income_info as $v)
//       {
//           $income_total+=$v['count'];
//       }
       $income_total = StatisticalService::dayIncomeData(array("switchStatic"=>1))['total'];


       //待提现总额
       $todepositprice= FinanceService::TypeUnDepositTotalPrice();
       //用户总积分
       $member_integral=StatisticalService::MemberData('member_integral')['data'];
       //今日余额使用总数
       $use_balance=StatisticalService::UseBalanceData();

       $use_balance_today=$use_balance['data']['today'];
       //昨日余额使用总数
       $use_balance_yesterday=$use_balance['data']['yesterday'];
       //合伙人提现
       $partner_deposit=StatisticalService::PartnerDeposit()['data'];

       #用户
       //今日新增用户
       $member_today = StatisticMemeberService::MemberCountInfo(1);

       //总用户数量
       $member_count = StatisticMemeberService::MemberCountInfo(2);

       //今日浏览量
       if (!$redis->sMembers('views_num'.date('Ymd'))) {

           $browse_today =0;

       }else{

           $browse_today = ceil(count($redis->sMembers('views_num'.date('Ymd')))/2);
           //dump($redis->sMembers('views_num'.date('Ymd')));exit;
       }

           //StatisticMemeberService::MemberCountInfo(3);

       //总会员数量
       //$member_count_number = StatisticMemeberService::MemberCountInfo(4);
       /*
       $currentDay = date('Y-m-d');
       $statistics = Db::table('member_mapping mm')
           ->field('
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where mm.member_level_id >2   ) vipCount,
            (select count(mm.id) from member_mapping mm left join member m on m.id=mm.member_id where (mm.member_level_id =1 or mm.member_level_id =2) and member_expiration >= "' . $currentDay . '" ) vip1Count
            ')
           ->join('member m', 'm.id=mm.member_id', 'left')
           ->where('mm.id > 0' )
           ->find();
       $member_count_number=$statistics['vipCount']+$statistics['vip1Count'];
       */
       $where_vip=[
           ['mm.member_level_id','>',0],
       ];
       $data_vip=Db::name('member_mapping')->alias('mm')
           ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
           ->where($where_vip)
           ->select();

       $vipCount_arr=[];

       foreach ($data_vip as $k=>$v)
       {
           if ($v['member_level_id']==1||$v['member_level_id']==2)
           {
               if (strtotime($v['member_expiration'])<time()) {
                   unset($data_vip[$k]);
               }else{
                   $vipCount_arr[$k]= $v['member_id'];
               }
           }else{
               $vipCount_arr[$k]= $v['member_id'];
           }
       }
       $member_count_number=count($vipCount_arr);

       //今日新增会员
       $member_today_add = StatisticMemeberService::MemberCountInfo(5);
       // 今日办理会员总金额
       $member_today_total = StatisticMemeberService::MemberHandleTotal(1);
       //实名认证会员
       $member_real = StatisticMemeberService::MemberCountInfo(6);

       //今日app下载总量
       $download_num=StatisticMemeberService::DownloadApp(['year'=>$time[0],'month'=>$time[1],'day'=>$time[2]]);

       //门店咨询数据 quotedprice
       $bizdata = StatisticMemeberService::BizStatisticInfo(1);

       //门店订货数据 dispath_ordergoods
       $bizorder_goods = StatisticMemeberService::BizStatisticInfo(2);

       //今日到店顾客数 biz_traffic
       $biz_traffic_tday = StatisticMemeberService::BizStatisticInfo(3);

       //昨日到店顾客数 biz_traffic
       $biz_traffic_yday = StatisticMemeberService::BizStatisticInfo(4);

       //今日到店会员 biz_traffic
       $biz_traffic_cardtday = StatisticMemeberService::BizStatisticInfo(5);

       //昨日到店会员 biz_traffic
       $biz_traffic_cardyday = StatisticMemeberService::BizStatisticInfo(6);

       //超时未到店会员 orders
       $biz_overtime = StatisticMemeberService::BizStatisticInfo(7);

       $this->assign('user_ranking',$user_ranking);
       $this->assign('agent_apply',$agent_apply['data']);
       $this->assign('download_num',$download_num);
       $this->assign('deposit_partner',$partner_deposit);
       $this->assign('use_balance_yesterday',$use_balance_yesterday);
       $this->assign('use_balance_today',$use_balance_today);
       $this->assign('member_integral',$member_integral);
       $this->assign('todepositprice',$todepositprice['total_price']);
       $this->assign('income_total',$income_total);
       $this->assign('deposit_cooperate',$deposit_cooperate['data']);
       $this->assign('biz_partner',$biz_partner['data']);
       $this->assign('audit_creation',$audit_creation);
       $this->assign('biz_franchiser',$biz_franchiser['data']);
       $this->assign('biz_cooperation',$biz_cooperation['data']);
       $this->assign('biz_self_operate',$biz_self_operate['data']);

       $this->assign('member_today',$member_today);
       $this->assign('member_count',$member_count);
       $this->assign('browse_today',$browse_today);
       $this->assign('member_count_number',$member_count_number);
       $this->assign('member_today_add',$member_today_add);
       $this->assign('member_today_total',$member_today_total);
       $this->assign('member_real',$member_real);
       $this->assign('merchangts_expiration',$merchangts_expiration);

       $this->assign('bizdata',$bizdata);
       $this->assign('bizorder_goods',$bizorder_goods);
       $this->assign('biz_traffic_tday',$biz_traffic_tday);
       $this->assign('biz_traffic_yday',$biz_traffic_yday);
       $this->assign('biz_traffic_cardtday',$biz_traffic_cardtday);
       $this->assign('biz_traffic_cardyday',$biz_traffic_cardyday);
       $this->assign('biz_overtime',$biz_overtime);

       return $this->fetch();
   }


   public function demo(){
       return $this->fetch();
   }

   public function demo2(){
       return $this->fetch();
   }

   private function uploadFileDemo()
   {
       #  上图片传示例
       $oldsrc="";// 要刪除的图片路径
       $params=[
           'field'=>'img',
           'oldSrc'=>$oldsrc,

       ];
       ResourceService::$session_suffix='';
       ResourceService::uploadFile($params);

       # 有上传图片的地方打开前先调用
       ResourceService::delUploadCache();

       # 上传完成保存后调用
       $arr="/20200612/xxx.png";
       # 或者是
       $arr=array(
           "/20200612/xxx.png",
           "/20200612/xxx1.png",
       );
       ResourceService::delCacheItem($arr);

       # base64 格式图片上传
       $base="base64格式的字符串";
       ResourceService::uploadBase($base);



   }

    function reWriteJs()
    {
        $str=input("post.str");
        $myfile = fopen(ROOT_PATH."static/demo/js/bg.js", "w") or die("Unable to open file!");
        fwrite($myfile, $str);
        fclose($myfile);
        return true;
    }






/************************ 门店数据 start*****************************/

    /*
     * @content ：门店数据
     * @params mark 统计的数据类型
     * */
    function bizDataInfo(){
        $mark = input("get.mark");

        if(input("get.action") == 'ajax'){
            $params['biz_type'] = input("post.biz_type");

            $params['biz_title'] = input("post.biz_title");

            $params['create_time'] = input("post.create_time");

            $biz = StatisticMemeberService::BizDataInfo($mark,$params);

            $total= StatisticMemeberService::BizDataInfoCount($mark);

            return ['code' => 0, 'msg' => '',  'data' => ['data'=>$biz,'total'=>$total]];

        }else{
            $this->assign('mark',$mark);

            return $this->fetch("/index/bizDataInfo");

        }

    }
    /*
     * @content ：门店咨询数据 回复
     * @params mark 统计的数据类型
     * */
    function bizDataReply(){
        $id = input("post.id");
        if(input("get.action") == 'ajax'){
            #修改信息
            $info = input("post.");
            $info['read_status'] = 2;
            Db::table('quotedprice')->where(array('id'=>$id))->update($info);
            return array('status'=>true);
        }else{
            $data = Db::table('quotedprice')->field('id,purchase_price,selling_price,settlement_price,message,oldfactory_price,vicefactory_price,dismancar_price,brand_price')
                ->where(array('id'=>$id))->find();
            return array('status'=>true,'data'=>$data);
        }


    }
    /**
     * [BizBalanceList 余额使用记录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2021年2月25日17:40:04
     */
    public function BizBalanceList()
    {
        if(input("get.action") == 'ajax'){
            $params['biz_type'] = input("post.biz_type");

            $params  = input();

            $biz = FinanceService::BizTrafficGroupData($params);


            return ['code' => 0, 'msg' => '',  'data' => $biz];

        }else{


            return $this->fetch();

        }
    }
    //删除
    function DelInfo(){
        $mark = input("get.mark");
        $id = input("post.id");
        if($mark == 1){
            Db::table('quotedprice')->where(array('id'=>$id))->delete();
        }
        return array('status'=>true);
    }
    /*
   * @content ：门店订货数据 查看详情
   * @params id
   * */
    function bizdetail(){
        $params['id'] = input('get.id');

        if(input('get.action') == 'ajax'){
            $arrival_time = input('post.arrival_time');
            $params = input("post.");

            #订货表 修改状态  和到货时间
            Db::table('dispath_ordergoods')->where(array('id'=>$params['id']))->update(array('arrival_time'=>$arrival_time,'read_status'=>2,'send_num'=>$params['need_num']));
            #发货单表 添加数据
            $dispathId = Db::table('dispath')->insertGetId(array(
                'biz_id'=>$params['biz_id'],
                'dispath_price'=>$params['checkingout_price'],
                'dispath_num'=>$params['need_num'],//默认需求数量
            ));
            if($dispathId){
                #订货表 修改发货单id
                Db::table('dispath_ordergoods')->where(array('id'=>$params['id']))->update(array('dispath_id'=>$dispathId));
                DB::table('dispath_detail')->insert(array(
                    'dispath_id'=>$dispathId,
                    'pro_id'=>$params['quotedprice_id'],
                    'pro_num'=>$params['need_num'],
                    'pro_purch'=>$params['purchase_price'],
                ));
            }
            return array('status'=>true);


        }else{
            #求门店订货数据 mark = 2
            $biz = StatisticMemeberService::BizDataInfo(2,$params);
            $this->assign('biz',$biz[0]);
            return $this->fetch();
        }

    }
    /*
   * @content ：门店订货数据点击下订单
   * @params id
   * */
    function bizorder(){
        $id = input('get.id');

        if(input('get.action') == 'ajax'){
            #求门店订货数据 mark = 2

            $arrival_time = input('post.arrival_time');
            $params = input("post.");
            $biz = StatisticMemeberService::BizDataInfo(2,$params);
            #订货表 修改状态  和到货时间
            Db::table('dispath_ordergoods')->where(array('id'=>$params['id']))->update(array('arrival_time'=>$arrival_time,'read_status'=>2,'send_num'=>$biz[0]['need_num']));
            #发货单表 添加数据
            $dispathId = Db::table('dispath')->insertGetId(array(
                'biz_id'=>$biz[0]['biz_id'],
                'dispath_price'=>$biz[0]['checkingout_price'],
                'dispath_num'=>$biz[0]['need_num'],//默认需求数量
                'pro_from'=>2,//询价商品表
            ));
            if($dispathId){
                #订货表 修改发货单id
                Db::table('dispath_ordergoods')->where(array('id'=>$params['id']))->update(array('dispath_id'=>$dispathId));
                DB::table('dispath_detail')->insert(array(
                    'dispath_id'=>$dispathId,
                    'pro_id'=>$biz[0]['quotedprice_id'],
                    'pro_num'=>$biz[0]['need_num'],
                    'pro_purch'=>$biz[0]['purchase_price'],
                ));
            }
            return array('status'=>true);


        }else{
            $this->assign('id',$id);

            return $this->fetch();
        }

    }
    /**
     * [ActionExcel 门店详情导出数据]
     * @version  1.0.0
     * @datetime 2020年6月24日13:23:09
     */
    public function ActionExcel(){
        $params = input();
        $info = StatisticMemeberService::BizDataInfo(2,$params);
        $info=StatisticMemeberService::BizDataInfoDealWith($info);



        $excel_params=[
            'filename'=>'门店订货详情',
            'title'=>lang('excel_action_dispath_ordergoods_list'),
            'data'=>$info
        ];
        $excel=new Excel($excel_params);
        return $excel->Export();

    }
    /************************ 门店数据 end *****************************/


}
