<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 9:38
 */

namespace app\admin\controller;

use app\service\BaseService;
use app\service\CommentService;
use app\service\NoiceService;
use think\Db;

/**
 * 评论管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月10日09:56:22
 */
class JzComment extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 门店评论统计表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function Index()
    {
        $time=date('Y-m', strtotime(date('Y-m-d', time()) . '-1 month'));

        if (input('get.action')=='ajax') {
            $params=input();

            $params['param']['year']=empty($params['param']['year'])?explode('-',$time)[0]:$params['param']['year'];

            $params['param']['month']=empty($params['param']['month'])?explode('-',$time)[1]:$params['param']['month'];

            $where=CommentService::CommentListWhere($params);
           /*$data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'group'     =>'evaluation_score.biz_id',
                'field'    =>'evaluation_score.biz_id,sum(score) total_score,count(score) comment_total'
            );*/
           $field='b.id,b.id biz_id,b.biz_type,b.biz_title,sum(score) total_score,count(score) comment_total';
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'group'     =>'b.id',
                'field'    =>$field
            );

            $data = CommentService::CommentList($data_params);

            $score_total=CommentService::TotalScore($where);
            //dump($data);exit;
            return ['code' => 0, 'msg' => '', 'count'=>$score_total, 'data' => $data];
        }else{
          /*  $data = Db::table('biz b')
                ->leftJoin(['evaluation_score'=>'evaluation_score'],'b.id=evaluation_score.biz_id')
                ->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')
                ->group('b.id')
                ->field('b.biz_title,sum(evaluation_score.score) total_score,count(score) comment_total')->select();
            dump($data);die;*/
            $biz_arr=Db::name('biz')->where('biz_status > 0')->field('id,biz_title')->select();

            $this->assign('biz_arr',$biz_arr);
            $this->assign('year',explode('-',$time)[0]);
            $this->assign('month',explode('-',$time)[1]);
            return $this->fetch();
        }
    }

    /**
     * [IndexHistory 门店员工评论，门店评论列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function IndexHistory(){
        if (input('get.action')=='ajax') {
            $params=input();

            $data_params = CommentService::SqlParser($params);


            $data = CommentService::CommentStaffList($data_params);

            $total = CommentService::DataStaffTotal($data_params);

            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            $this->assign('cate',input('cate'));
            return $this->fetch();
        }
    }

    /**
     * [IndexHistoryBiz 门店评论]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function IndexHistoryBiz(){

        if (input('get.action')=='ajax') {
            $params=input();


            $where=CommentService::CommentListWhere($params);
           // dump($where);exit;
            $field='order_id,create_time,score,orderserver_id,evaluation_score.biz_id';

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
              //  'group'     =>'orderserver_id',
                'field'    =>$field
            );


            $data = CommentService::CommentStaffList($data_params);



            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{



            return $this->fetch();
        }
    }


    /**
     * [IndexHistoryEmployee 门店员工评论]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function IndexHistoryEmployee(){

        if (input('get.action')=='ajax') {
            $params=input();

            $where=CommentService::CommentListWhere($params);

            $field='evaluation_score.create_time,sum(score) total_score,count(score) comment_total,evaluation_score.biz_id,evaluation_score.employee_id';

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'group'     =>'evaluation_score.employee_id',
                'field'    =>$field
            );


            $data = CommentService::CommentStaffList($data_params);



            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{



            return $this->fetch();
        }
    }

    /**
     * [IndexHistoryEmployeeDetail 员工个人评论]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function IndexHistoryEmployeeDetail(){

        if (input('get.action')=='ajax') {
            $params=input();

            $params['param']['year']=explode('-',$params['create_time'])[0];

            $params['param']['month']=explode('-',$params['create_time'])[1];

            $where=CommentService::CommentListWhere($params);

            $field='order_id,create_time,score,orderserver_id,evaluation_score.employee_id employ_id,evaluation_score.biz_id';

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'group'     =>'evaluation_score.orderserver_id',
                'field'    =>$field
            );


            $data = CommentService::CommentStaffList($data_params);



            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{



            return $this->fetch();
        }
    }

    /**
     * [UserComment 用户评论列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function IndexUserComment(){

        if (input('get.action')=='ajax') {
            $params=input();
            $data = CommentService::UserCommentList($params);

           // $total = CommentService::DataTotal($data_params);

            return ['code' => 0, 'msg' => '', 'data' => $data];
        }else{
            $this->assign('cate',input('cate'));
            return $this->fetch();
        }
    }
}