<?php


namespace app\admin\controller;


use app\service\BaseService;
use app\service\FinanceService;
use app\service\PropertyService;
use think\Db;
use think\facade\Session;

class Property extends Common
{
    function index(){
        if(input("get.action")=='ajax'){
           return PropertyService::dataList(input());
        }else{
            # 查询关联员工信息
            $employeeSal = Db::table('employee_sal')->field('id,employee_name name')
                ->where(array('relation_status'=>1))
                ->where('employee_status != 0')
                ->select();
            $this->assign('employeeSal',$employeeSal);
            return view();
        }
    }

    function saveData(){
        if(input("get.action")=="ajax"){
            return PropertyService::SaveData(input());
        }else{
            $id = input("get.id");
            $this->assign("id",$id);
            if(!empty($id)){
                // 获取列表
                $data_params = [
                    'where'				=> ['id'=>$id],
                    'm'					=> 0,
                    'n'					=> 1,
                    'page'			    => false,
                    'table'             =>'property_neighbourhood',
                    'field'     =>"*,(select  coalesce(count(id),0) from channel where channel=8 and biz_id  = property_neighbourhood.id and action_type = 2) handle_member"
                ];
                $ret = BaseService::DataList($data_params);
                $ret = PropertyService::DataDealWith($ret);
                $bannerArr = explode('*',$ret[0]['nh_detailimg']);
                $this->assign('bannerArr',$bannerArr);
                $data = empty($ret[0]) ? [] : $ret[0];
                $this->assign("data",$data);
            }
            Session::delete("building");
            return view();
        }
    }

    /**
     * @return bool[]
     * @context 将楼房信息存入数组
     */
    function SaveBuild()
    {
        $params = input();
        $_array=Session("building");
        if(empty($_array)){
            $_array=array();
        }
        # 楼房数组
        $build_array = array("build_title"=>$params['house_title'],"build_cate"=>$params['build_cate'],"build_cate_title"=>$params['build_cate_title'],
            "property_price"=>$params['property_price'],"unity_array"=>array());
        $unity_array=array();
        if(!empty($params['unit_title'])){
            $unit_title=NULL;
            foreach($params['unit_title'] as $k=>$v){
                if($v!=$unit_title){
                    $unit_title = $v;
                    $unity_array[$v]=array("title"=>$v,"house_array"=>array());
                }
                array_push($unity_array[$v]["house_array"],array("level"=>$params['floor_number'][$k],"house_code"=>$params['room_number'][$k],
                    "area"=>$params['room_area'][$k],"member_title"=>$params['member_title'][$k]));
            }
            $build_array["unity_array"]=$unity_array;
        }
        $_array[$params['house_title']]=$build_array;
        Session("building",$_array);
        return array("status"=>true);
    }

    function checkHouse(){
        $id = input("get.id");
        if(input("get.action")=='ajax'){
            $list = PropertyService::checkHouse(input());
            return ['code' => 0, 'msg' => '',  'data' => $list];
        }else{
            $this->assign("id",$id);
            return view();
        }
    }

    /**
     * @return mixed
     * @context 修改到期时间
     */
    function confirmUpdateTime()
    {
        return PropertyService::confirmUpdateTime(input());
    }
    /**
     * @return mixed
     * @context 修改到期时间
     */
    function confirmUpdateMember()
    {
        return PropertyService::confirmUpdateMember(input());
    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除房产
     */
    function deleteHouse()
    {
        return PropertyService::deleteHouse(input());
    }

    /**
     * @context 删除关联员工
     */
    function delStaffInfo()
    {

    }

    /**
     * @return bool[]
     * @context 修改物业点赞、转发、查看数量
     */
    function changePropertyNum()
    {
        return PropertyService::ChangePropertyNum(input());
    }

    /**
     * [CacheLog 提现记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CacheLog()
    {
        if (input('get.action')=='ajax') {

            $where[]=['source_type','=',4];
            $where[]=['biz_id','=',input('merchants_id')];
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'log_deposit'
            );
            $data=FinanceService::DepositList($data_params);
            $total_num = BaseService::DataTotal('log_deposit',$where);

            $total_price = FinanceService::DepositTotalPrice($where);
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data,'price'=>$total_price];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [CollectionsLog 收款记录表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CollectionsLog(){
        if (input('get.action')=='ajax') {
            $merchants_id = input('merchants_id');
            $_where = "merchants_id = $merchants_id ";
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'table'     =>'merchants_income',
                'where'     => $_where,
                'field'     =>'*'
            );
            $data= PropertyService::PropertyCollectionsList($data_params);
            $total_num = BaseService::DataTotal('merchants_income',$_where);
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [CardLog 推荐会员信息]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CardLog(){
        if (input('get.action')=='ajax') {

            $merchants_id = input('merchants_id');
            $where = "log_resource = 4 and biz_id=$merchants_id";
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'table'     =>'log_handlecard',
                'where'     => $where,
                'field'     =>'*'
            );
            $data= PropertyService::PropertyCardlogList($data_params);


            $total_num = BaseService::DataTotal('log_handlecard',$where);



            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [StaffList 员工列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function StaffList(){

        if (input('get.action')=='ajax') {

            $where=[
                ['merchants_id','=',input('merchants_id')],

            ];
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'table'     =>'merchants_staff',
                'where'     => $where,
                'field'     =>'*'
            );
            $data= PropertyService::MerchantsStaffList($data_params);


            $total_num = BaseService::DataTotal('merchants_staff  ',$where);


            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }

    /**
     * [CommentList 评论列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function CommentList(){
        if (input('get.action')=='ajax') {
            $where=PropertyService::PropertyCommentListWhere(input());
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>'me.create_time,me.evalua_level,me.evalua_comment,mo.actual_price,mo.cash_type'
            );
            $data= PropertyService::PropertyCommentDataList($data_params);
            $total_num = PropertyService::PropertyCommentDataTotal($where);
            return ['code' => 0, 'msg' => '', 'count' => $total_num, 'data' => $data];
        }else{
            $this->assign('cash_type',lang('cash_type'));
            return $this->fetch();
        }
    }

    public function financialChart()
    {
        $data = PropertyService::FinancialChartData();
        $this->assign("data",$data);
        return view();
    }

    public function BindChart()
    {
        $data = PropertyService::BindChartData(input());
        return $data;
    }
    public function payFeeChart()
    {
        $data = PropertyService::PayFeeChartData(input());
        return $data;
    }
    public function commissionChart()
    {
        $data = PropertyService::CommissionChartData(input());
        return $data;
    }
    public function subsidyChart()
    {
        $data = PropertyService::SubsidyChartData(input());
        return $data;
    }
    public function recommendChart()
    {
        $data = PropertyService::RecommendChartChartData(input());
        return $data;
    }


}