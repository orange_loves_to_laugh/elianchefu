<?php


namespace app\admin\controller;


use app\service\AdService;
use think\facade\Hook;

/**
 * 管理员
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 */
class Admin extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();
    }


    /**
     * [LoginInfo 登录页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     */
    public function LoginInfo()
    {
        //var_dump(session('admin'));

        // 是否已登录
        if(session('admin') !== null)
        {
            return redirect(url('admin/index/index'));
        }

        // 管理员登录页面钩子
//        $hook_name = 'plugins_view_admin_login_info';
//        $this->assign($hook_name.'_data', Hook::listen($hook_name,
//            [
//                'hook_name'     => $hook_name,
//                'is_backend'    => true,
//            ]));

        return $this->fetch();
    }

    /**
     * [Login 管理员登录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     */
    public function Login()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');
        return AdService::Login($params);
    }

    /**
     * [Logout 退出]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     */
    public function Logout()
    {
        session_start();

        session_destroy();
        return redirect(url('admin/admin/logininfo'));
    }
}