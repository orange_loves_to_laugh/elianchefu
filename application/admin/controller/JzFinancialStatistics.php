<?php


namespace app\admin\controller;

use app\api\ApiService\SubsidyService;
use app\service\StatisticalService;
use think\Db;

/**
 * 财务统计
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月21日10:37:36
 */
class JzFinancialStatistics extends Common
{
    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }
    /**
     * [IncomeStatistics 收入统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function IncomeStatistics(){
        //方式统计
//        $incometype=StatisticalService::IncomeTypeInfo();
//
//        $this->assign('incometype_today',$incometype['data']['today']);
//        $this->assign('incometype_month',$incometype['data']['month']);
//        //来源
//        $incomesource=StatisticalService::IncomeSourceInfo();
//         //dump($incomesource['data']['month']);exit;
//        $this->assign('incomesource_today',$incomesource['data']['today']);
//        $this->assign('incomesource_month',$incomesource['data']['month']);


        $this->assign('year',date('Y',time()));
        $this->assign('month',date('m',time()));
        return $this->fetch();
    }

    /**
     * [IncomeMonthType 月收入分布统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function IncomeMonthType(){
        $param=input();

        $incometype=StatisticalService::IncomeTypeMonthInfo($param);

        $this->assign('incometype',$incometype['data']);

        $this->assign('cash_type',array_slice(lang('cash_type_total_info'),0,4));
        return $this->fetch();
    }
    /**
     * [IncomeMonthType 日收入分布统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月7日15:06:55
     * @desc    description
     */
    public function IncomeDayType(){
        $param=input();

        $incometype=StatisticalService::IncomeTypeDayInfo($param);

        $this->assign('incometype',$incometype['data']);

        $this->assign('cash_type',array_slice(lang('cash_type_total_info'),0,4));
        return $this->fetch();
    }

    /**
     * [IncomeMonthType 月收入来源统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function IncomeSource(){
        $param=input();
        $incomesource=StatisticalService::IncomeSourceInfo($param);
        $this->assign('incomesource',$incomesource['data']);
        return $this->fetch();
    }
    /**
     * [IncomeMonthType 日收入来源统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function IncomeDaySource(){
        $param=input();
        $incomesource=StatisticalService::IncomeSourceInfo($param);
        $this->assign('incomesource',$incomesource['data']);
        return $this->fetch();

    }


    /**
     * [ExpenditureStatistics 支出统计页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpenditureStatistics()
    {
        $this->assign('year',date('Y',time()));
        $this->assign('month',date('m',time()));
        return $this->fetch();
    }

    /**
     * [ExpenditureStatisticsMonth 月支出统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpenditureStatisticsMonth()
    {
        $expenditure=StatisticalService::ExpenditureInfo(input());

        $this->assign('expenditure_month',$expenditure['data']);
        return $this->fetch();
    }

    /**
     * [ExpenditureStatisticsMonth 月支出统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpenditureStatisticsYear()
    {
        $expenditure=StatisticalService::ExpenditureInfo(input());

        $this->assign('expenditure_year',$expenditure['data']);
        return $this->fetch();
    }



    /**
     * [ProfitStatistics 利润统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ProfitStatistics(){

        $this->assign('year',date('Y',time()));
        $this->assign('month',date('m',time()));
        return  $this->fetch();
    }

    /**
     * [ProfitStatisticsMonth 月利润统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ProfitStatisticsMonth(){

        $data=StatisticalService::ProfitInfo(input());

        $this->assign('profit_month',$data);
        return  $this->fetch();
    }

    /**
     * [ProfitStatisticsMonth 年利润统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ProfitStatisticsYear(){
        $data=StatisticalService::ProfitInfo(input());
        $this->assign('profit_year',$data);
        return  $this->fetch();
    }

    /**
     * [ExpensesStatistics 市场费用统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpensesStatistics(){

        $this->assign('year',date('Y',time()));
        $this->assign('month',date('m',time()));
       return  $this->fetch();
    }
    /**
     * [ExpensesStatisticsMonth 月度市场费用统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpensesStatisticsMonth(){

        $data=StatisticalService::ExpensesInfo(input());

        $this->assign('expenses_month',$data);
        return  $this->fetch();
    }

    /**
     * [ExpensesStatisticsMonth 月度市场费用统计]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日10:37:36
     * @desc    description
     */
    public function ExpensesStatisticsYear(){

        $data=StatisticalService::ExpensesInfo(input());

        $this->assign('expenses_year',$data);
        return  $this->fetch();
    }

    /**
     * @return \think\response\View
     * @context 财务统计图表
     */
    public function FinancialChart()
    {
        return view();
    }

    /**
     * @return array
     * @context 收入统计图表
     */
    function IncomeChart(){
        $data = StatisticalService::incomeChartData(input());
        return $data;
    }


    /**
     * @return array|\think\response\View
     * @context 收入统计列表
     */
    function IncomeDetail()
    {
        $param = input();
        if(input("get.action")=='ajax'){
            $data = StatisticalService::incomeDetail($param);
            return ['code' => 0, 'msg' => '', 'count' => $data['list']['total'], 'data' => $data['list']['data'],"total_price"=>$data['total_price']];
        }else{
            $this->assign("income_type",$param['income_type']);
            $this->assign("switchStatic",$param['switchStatic']);
            $this->assign("start_time",$param['start_time']);
            $this->assign("end_time",$param['end_time']);
            return view();
        }
    }

    function SubsidyChart()
    {
        $data = StatisticalService::subsidyChartData(input());
        return $data;
    }
    function subsidyDetail()
    {
        $param = input();
        $this->assign('params',$param);
        if($param['subsidy_type']<=10){
            return view("subsidyMemberDetail");
        }elseif($param['subsidy_type']==11){
            return view("subsidyDiffDetail");
        }elseif($param['subsidy_type']==12){
            return view("subsidyVoucherDetail");
        }elseif($param['subsidy_type']==13){
            return view("subsidyBizTaskDetail");
        }elseif($param['subsidy_type']==14){
            return view("subsidyMerchantTaskDetail");
        }elseif($param['subsidy_type']==16){
            return view("subsidyBizRecommendDetail");
        }else{
            return view("subsidyMerchantPacketDetail");
        }
    }

    function subsidyMemberDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyMemberDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }

    function subsidyDiffDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyDiffDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }

    function subsidyVoucherDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyVoucherDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }

    function subsidyBizTaskDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyBizTaskDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }
    function subsidyMerchantTaskDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyMerchantTaskDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }
    function subsidyMerchantPacketDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyMerchantPacketDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }
    function subsidyBizRecommendDetail()
    {
        $param = input();
        $data = StatisticalService::subsidyBizRecommendDetail($param);
        return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
    }

    function disburseChart()
    {
        $data = StatisticalService::disburseChartData(input());
        return $data;
    }

    function profitChart()
    {
        $data = StatisticalService::profitChartData(input());
        return $data;
    }

    function MemberChart()
    {
        $data = StatisticalService::MemberTotalData();
        $this->assign("memberCount",$data['memberCount']);
        $this->assign("balanceCount",getsPriceFormat($data['balanceCount']));
        return view();
    }

    function MemberChatData()
    {
        $data = StatisticalService::memberChartData(input());
        return $data;
    }
    function MemberDistrictChatData()
    {
        $data = StatisticalService::memberDistrictChartData(input());
        return $data;
    }
    
    function IntegralChart(){
        $data = Db::table("member_mapping")->sum("member_integral");
        $this->assign("integralCount",$data);
        return view();
    }

    function IntegralChartData()
    {
        $data = StatisticalService::integralChartData(input());
        return $data;
    }

    function AddIntegralDetail()
    {
        $params = input();
        if(input("get.action")=='ajax'){
            $data = StatisticalService::addIntegralDetail($params);
            return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
        }else{
            $this->assign("params",$params);
            return view();
        }

    }

    function UseIntegralDetail()
    {
        $params = input();
        if(input("get.action")=='ajax'){
            $data = StatisticalService::useIntegralDetail($params);
            return ['code' => 0, 'msg' => '', 'count' => $data['total'], 'data' => $data['data']];
        }else{
            $this->assign("params",$params);
            return view();
        }

    }

    function IncomeDayChart()
    {
        $income_today = StatisticalService::dayIncomeData(array("switchStatic"=>1))['total'];
        $income_yesterday = StatisticalService::dayIncomeData(array("switchStatic"=>2))['total'];
        $income_month = StatisticalService::dayIncomeData(array("switchStatic"=>3))['total'];
        $income_lastmonth = StatisticalService::dayIncomeData(array("switchStatic"=>4))['total'];
        $total = StatisticalService::dayIncomeData([])['total'];
        $this->assign("today",$income_today);
        $this->assign("yesterday",$income_yesterday);
        $this->assign("month",$income_month);
        $this->assign("lastmonth",$income_lastmonth);
        $this->assign("total",$total);
       return view();

    }


}