<?php
/**
 * Created by PhpStorm.
 * User: 李闯
 * Date: 2020/9/22
 * Time: 13:54
 * Content:门店管理
 */

namespace app\admin\controller;


use app\service\BaseService;
use app\service\BizService;
use app\service\MemberService;
use app\service\ResourceService;
use app\service\StatisticalService;
use app\service\TimeTask;
use Overtrue\Socialite\Providers\Base;
use Redis\Redis;
use think\Db;
use think\facade\Session;

//use think\Session;

class Biz extends Common
{
    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

        // 权限校验
        $this->IsPower();
    }

    function storeSettlement(){
        # 门店下架结算
        $biz_id = input('post.bizId');
        # 查询门店状态
        $bizStatus = Db::table('biz')->where(array('id'=>$biz_id))->value('biz_status');
        if($bizStatus==1){
            # 还没下架
            $redis = new Redis();
            /* $status = $redis->get('settlementTask'.$bizId);
             if (!empty($status)) {
                 return false;
             }
             $redis->set('settlementTask'.$bizId, 'YES');*/
            $status = $redis->lock('storeSettlement' . $biz_id);
            if ($status) {
                # 先结算
                $res = TimeTask::settlementTask($biz_id);
                if ($res) {
                    # 查询门店信息
                    $bizInfo = Db::table('biz')
                        ->field('account_balance,security_price,deposit_account,biz_leader,province,city,area,biz_address,biz_phone')
                        ->where(array('id' => $biz_id))
                        ->find();
                    # 添加提现信息
                    $amount = $bizInfo['account_balance'] + $bizInfo['security_price'];
                    $logId = Db::table('log_deposit')->insertGetId(array(
                        'biz_id' => $biz_id,
                        'create_time' => date('Y-m-d H:i:s'),
                        'price' => $amount,
                        'username' => $bizInfo['biz_leader'],
                        'phone' => $bizInfo['biz_phone'],
                        'account_name' => $bizInfo['biz_leader'],
                        'account_code' => $bizInfo['deposit_account'],
                        'launch_cate' => 7,
                        'deposit_type' => 2,
                        'province' => $bizInfo['province'],
                        'city' => $bizInfo['city'],
                        'area' => $bizInfo['area'],
                        'address' => $bizInfo['biz_address']
                    ));
                    if ($logId) {
                        # 修改门店状态
                        Db::table('biz')
                            ->where(array('id' => $biz_id))
                            ->update(array('biz_status' => 0));
                    }
                    $redis->unlock('storeSettlement' . $biz_id);
                }
            }
            return array('status'=>true,'msg'=>'已经提交审核');
        }else{
            # 已经下架
            return array('status'=>false,'msg'=>'已经下架');
        }
    }

    function checkRelationBiz()
    {
        # 接收门店类型  1=>A类((可以关联 加盟店 (代理商区域下)))  2=>B类(可以关联 加盟店 A类合作店(代理商区域下的))
        $recommendStatus = input('post.recommend_status');
        # 接收门店地址(省市区)
        $province = input('post.province');
        $city = input('post.city');
        $area = input('post.area');
        if ($recommendStatus == 1) {
            # A类门店(查询区域下的加盟店)
            $_where = array('biz_type' => 2, 'biz_status' => 1, 'province' => $province, 'city' => $city, 'area' => $area);
            $_whereOr = array();
        } else {
            # B类门店(查询区域下的A类门店,加盟店)
            $_where = array('biz_type' => 3, 'recommend_status' => 1, 'biz_status' => 1, 'province' => $province, 'city' => $city, 'area' => $area);
            $_whereOr = array('biz_type' => 2, 'biz_status' => 1, 'province' => $province, 'city' => $city, 'area' => $area);
        }
        # 查询可关联的门店信息
        $info = Db::table('biz')
            ->where($_where)
            ->whereOr($_whereOr)
            ->select();
        # 查询已关联的门店数量
        if (!empty($info)) {
            $_return = BizService::RelationBizMould($info,'',10);
            return array('status' => true, 'msg' => '查询成功', 'data' => $_return);
        }
        return array('status' => false, 'msg' => '可关联信息为空');
    }

    /**
     * [BizIndex 门店列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function BizIndex()
    {
//        $biz_type  = input("get.biz_type");

        if (input("get.action") == 'ajax') {
            $searchParams = input("post.searchParams");
            $_where = "biz_type != 4 and biz_status !=2 and is_test = 2";
            //门店类型
            if (!empty($searchParams['type'])) {
                $_where .= " and biz_type = '" . $searchParams['type'] . "'";

            }
            //关键词
            if (!empty($searchParams['keywords'])) {
                $_where .= " and biz_title like '%" . $searchParams['keywords'] . "%'";

            }

            //关联员工状态
            if (!empty(input()['searchParams']['relate_employee'])) {

                $biz_arr = Db::name('assign_staff')->where(['type' => 1])->column('biz_id');

                if (input()['searchParams']['relate_employee'] == 1) //关联
                {
                    $relate = 'in';

                } else {
                    $relate = 'not in ';
                }
                if (!empty($biz_arr)) {

                    $biz_arr = join(',', $biz_arr);

                    $_where .= " and id $relate ($biz_arr)";

                } else {
                    $_where .= " and id < 0";

                }

            }
            # 关联员工
            if (!empty($searchParams['employee_sal'])) {
                $biz_arr = Db::name('assign_staff')->where(['type' => 1, 'employee_id' => $searchParams['employee_sal']])->column('biz_id');
                if (empty($biz_arr)) {
                    $_where .= " and id < 0";
                } else {
                    $biz_arr = join(',', $biz_arr);
                    $_where .= " and id in ($biz_arr)";
                }
            }

            //dump(input());exit;
            if (!empty(input()['searchParams']['province'])) {
                $_where .= " and province = '" . input()['searchParams']['province'] . "'";

            }

            if (!empty(input()['searchParams']['city'])) {
                $_where .= " and city = '" . input()['searchParams']['city'] . "'";

            }

            if (!empty(input() ['searchParams']['area'])) {
                $_where .= " and area = '" . input()['searchParams']['area'] . "'";

            }
            //
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = BizService::BizIndex($data_params);

            $total = BizService::BizIndexCount($_where);
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];

//            return DataReturn('获取成功','0',$data);
        } else {
            $url = getUrl();
            $this->assign('url', $url);
            # 查询关联员工信息
            $employeeSal = Db::table('employee_sal')->field('id,employee_name name')
                ->where(array('relation_status' => 1))
                ->where('employee_status != 0')
                ->select();
            $this->assign('employeeSal', $employeeSal);

            return $this->fetch('/biz/bizindex');

        }

    }

    /**
     * [BizIndex 门店创建]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function Bizadd()
    {
        $biz_type = input("get.biz_type");
        if (input("get.action") == 'ajax') {
            $params = array_filter(input("post."));

            if (input("post.biz_status") == 0) {
                $params['biz_status'] = 0;
            }


            if (!empty($params)) {
                #=接收参数不为空
                $data = BizService::BizInsertWrite($params);
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            #弹窗需要 创建工位
            $service_class = Db::table("service_class")->select();
            $service_team = Db::table("team")->select();
            $this->assign('service_class', $service_class);
            $this->assign('service_team', $service_team);
            #类型
            $this->assign('biz_type', $biz_type);
            #删除工位缓存
            Session::delete('stationInfoSession');
            Session::delete('bizproInfoSession');
            Session::delete('serviceInfoSession');
            Session::delete('bizId');


            $params['id'] = empty($biz_id) ? '' : $biz_id;
            ResourceService::$session_suffix = 'source_upload' . 'biz' . $params['id'];
            ResourceService::delUploadCache();
            return $this->fetch();

        }

    }

    /**
     * [Bizwrite 门店修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function Bizwrite()
    {
        $biz_type = input("get.biz_type");
        $biz_id = input("get.id");
//        $re=Db::name('station')->where('biz_id='.$biz_id)
//            ->field('station_title,biz_id,id')->select();
//
//        dump($re);exit;
        if (input("get.action") == 'ajax') {

            $params = input("post.");
            unset($params['thumb_img']);
            if (input("post.biz_status") == 0) {
                $params['biz_status'] = 0;
            }
            if (!empty($params)) {
                #=接收参数不为空

                $data = BizService::BizInsertWrite($params);
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $savemark = input("get.savemark");
            $this->assign("savemark", $savemark);
            $params['id'] = empty($biz_id) ? '' : $biz_id;

            ResourceService::$session_suffix = 'source_upload' . 'biz' . $params['id'];
            ResourceService::delUploadCache();

            $this->assign('biz_type', $biz_type);
            $this->assign('biz_id', $biz_id);
            #缓存存门店id
            Session::set('bizId', $biz_id);
            #修改查询门店数据
            $biz = Db::table("biz")->where("id = $biz_id")->find();
            //关联服务或商品
            $biz['servicerelation_num'] = Db::table("service_biz sb")
                ->join('service s','s.id = sb.service_id and s.biz_pid = 0','left')
                ->where("sb.biz_id = $biz_id and s.biz_pid = 0")->count();

            $biz['relateoption_num'] = Db::table("biz_goods")->where("biz_id = $biz_id")->count();

            $biz['relate_employee'] = Db::name('assign_staff')
                ->alias('as')
                ->leftJoin(['employee_sal' => 'e'], 'as.employee_id=e.id')
                ->where('as.biz_id=' . $biz_id)->field('as.employee_id,employee_name')->find();
            $biz['recommond_commission_platservice_show'] = ($biz['recommond_commission_platservice'] * 100) . '%';
            $biz['recommond_commission_platpro_show'] = ($biz['recommond_commission_platpro'] * 100) . '%';
            $biz['recommond_commission_selfpro_show'] = ($biz['recommond_commission_selfpro'] * 100) . '%';

            $biz['recommond_commission_selfservice_show'] = ($biz['recommond_commission_selfservice'] * 100) . '%';

            $biz['commission_platservice_show'] = ($biz['commission_platservice'] * 100) . '%';
            $biz['commission_platpro_show'] = ($biz['commission_platpro'] * 100) . '%';
            $biz['commission_selfpro_show'] = ($biz['commission_selfpro'] * 100) . '%';
            $biz['commission_selfservice_show'] = ($biz['commission_selfservice'] * 100) . '%';
            $biz['extract_platservice_show'] = ($biz['extract_platservice'] * 100) . '%';
            $biz['extract_selfservice_show'] = ($biz['extract_selfservice'] * 100) . '%';
            $biz['extract_selfpro_show'] = ($biz['extract_selfpro'] * 100) . '%';
            $biz['recharge_upgrade_ratio_show'] = ($biz['recharge_upgrade_ratio'] * 100) . '%';

            //
            $this->assign('biz', $biz);
            #执行修改
            $biz["biz_head"] = substr($biz["biz_head"], 0, 1) == ',' ? substr($biz["biz_head"], 1) : substr($biz["biz_head"], 0);
            $_bannerurl = explode(",", $biz["biz_head"]);
            $this->assign("bannerArr", $_bannerurl);

            #用来删除图片的 是个数组
            /* $biz["biz_headurl"] = implode(',', array_map("change_to_quotes", $_bannerurl));
             $this->assign('biz_headurl', $biz["biz_headurl"]);*/
            #查询工位
            $station = BizService::getStation($biz_id);


            $this->assign('station', $station);
            #查询门店注册会员数量
            $memberNum = Db::table('channel')->where(array('biz_id' => $biz_id, 'action_type' => 2))->count('member_id');

            $this->assign('memberNum', $memberNum);

            #弹窗需要 创建工位 和商品分类
            $service_class = Db::table("service_class")->select();
            $service_team = Db::table("team")->select();
            $pro_class = Db::table("pro_class")->select();
            $this->assign('pro_class', $pro_class);
            $this->assign('service_class', $service_class);
            $this->assign('service_team', $service_team);
            #类型
            #删除工位缓存
            Session::delete('stationInfoSession');
            Session::delete('bizproInfoSession');
            Session::delete('serviceInfoSession');
            return $this->fetch("/biz/bizwrite");

        }

    }

    /**
     * [biz_pcad 获取经纬度]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function biz_pcad()
    {
        $info = input("post.");
        if (!empty($info['province'] and !empty($info['city']) and !empty($info['area']) and !empty($info['biz_address']))) {
            $address = $info['province'] . $info['city'] . $info['area'] . $info['biz_address'];
            $result_local = getLonLat($address, '');    //获取地区编码
            $location = $result_local['geocodes'][0]['location'];
            $location_ay = explode(",", $location);
            $biz_lon = $location_ay[0];                //经度
            $biz_lat = $location_ay[1];                //纬度

            return array('status' => true, 'biz_lon' => $biz_lon, 'biz_lat' => $biz_lat);

        }

    }

    /**
     * [Bizrecord 门店查看记录]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function Bizrecord()
    {
        $type = input("get.type");
        $id = input("get.biz_id");

        if (input("get.action") == 'ajax') {
            //搜索
            $search = input("post.");
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            $data = BizService::BizDealRecord($type, $id, $search, $listRows);
            // dump($data);exit;
//            $total= Db::table('log_service')->where(array('biz_id'=>$id))->count();
            return ['code' => 0, 'msg' => '', 'data' => $data];
        } else {
            $this->assign('type', $type);
            $this->assign('biz_id', $id);

            return $this->fetch("/biz/bizrecord");
        }


    }

    /**
     * [StationPreCooperate 添加工位]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function StationPreCooperate()
    {

        $info = array_filter(input("post."));

        session('stationInfoSession', $info);

        return DataReturn('ok', 0);

    }

    /**
     * [StationPre 工位保存  存session]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function StationPre()
    {
        #创建
        $info = array_filter(input("post."));

        #获取到创建的数据  存到缓存
        #先看这个数组是否有  没有  存  有  插入
        $stationInfoSession = Session('stationInfoSession');
        if (empty($stationInfoSession))
            $stationInfoSession = array();

        if (!empty($info)) {
            if (!empty($stationInfoSession)) {
                array_push($stationInfoSession, $info);
                Session::set('stationInfoSession', $stationInfoSession);

            } else {
                Session::set('stationInfoSession', array($info));
            }

        }
        $_str = '';
        $biz_type = input('get.biz_type');

        $_bizId = input('get.bizId');


        if (!empty($_bizId)) {
            # 查询数据库中工位
            $_detail = Db::table('station s')
                ->field('s.*,sc.service_class_title')
                ->join('service_class sc', 'sc.id= s.service_class_id', 'left')
                ->where(array('s.biz_id' => $_bizId))->select();
            if (!empty($_detail)) {
                $_str .= $this->getArrInfo($_detail, 'DB', $biz_type);
            }

        }
//            dump(Session('stationInfoSession'));die;
        $_str .= $this->getArrInfo(Session('stationInfoSession'), 'S', $biz_type);


        return array('status' => true, 'info' => $_str);


    }

    /**
     * @return array
     * @content  信息查询
     */
    function checkStation()
    {
        $_str = '';
        # 判断是否存在statiton_id,
        $_bizId = input('post.id');
        $_mark = input('post.mark');
        $biz_type = input("get.biz_type");
        if (!empty($_bizId)) {
            # 查询数据库中工位
            $_detail = Db::table('station s')
                ->field('s.*,sc.service_class_title')
                ->join('service_class sc', 'sc.id= s.service_class_id', 'left')
                ->where(array('s.biz_id' => $_bizId))->select();
            if (!empty($_detail)) {
                $_str .= $this->getArrInfo($_detail, 'DB', $biz_type);
            }
        }
        # 查询session中的活动商品信息
        $_session = Session::get('stationInfoSession');
        if (!empty($_session)) {
            $_str .= $this->getArrInfo($_session, 'S', $biz_type);
        }
        return array('status' => true, 'info' => $_str, 'biz_id' => $_bizId, 'mark' => $_mark);
    }

    /**
     * [getArrInfo 处理缓存信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function getArrInfo($arr, $mark, $biz_type)
    {


        $_str = '';
        if (!empty($arr) and is_array($arr)) {
            if ($biz_type == 3) {
                #合作门店
                foreach ($arr as $k => $v) {
                    /*                            <td>" . $v['service_class_title'] . "</td>
                    */
                    #服务类型名称
                    $title = Db::table('service_class')->field('service_class_title')->where(array('id' => $v['service_class_id']))->find();
                    $v['service_class_title'] = $title['service_class_title'];
                    $_str .= "<tr id='detailTr" . $v['id'] . "'>
                            <td onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['station_title'] . "','" . $mark . "','1','" . $v['biz_id'] . "')\">" . $v['station_title'] . "</td>
                            <td onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['negative_price'] . "','" . $mark . "','7','" . $v['biz_id'] . "')\">" . $v['negative_price'] . "</td>
                       
                           
                           <td>
                                <button class=\"btn btn-primary\" type=\"button\"   onclick=\"getrecommond('" . $v['recommond_royalty'] . "','" . $v['recommond_custom_service'] . "','" . $v['recommond_pro_royalty'] . "','" . $v['recommond_custom_pro'] . "','$mark','" . $v['id'] . "','" . $k . "','" . $v['biz_id'] . "','recommond')\">查看</button>
                            </td>
                            <td>
                                <div class=\"recommond\" style=\"margin-bottom: 5px\">
                                    <div style=\"margin-right: 5px\" onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['service_deduct'] . "','" . $mark . "','3','" . $v['biz_id'] . "')\">系统服务提成:" . ($v['service_deduct'] * 100) . '%' . "</div>
                                    <div onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['custom_service_deduct'] . "','" . $mark . "','12','" . $v['biz_id'] . "')\">自定义服务提成:" . ($v['custom_service_deduct'] * 100) . '%' . "</div>
                                </div>

                            </td>
                            <td>
                                <div class=\"recommond\" style=\"margin-bottom: 5px\">
                                    <div style=\"margin-right: 5px\"  onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['biz_deduct'] . "','" . $mark . "','4','" . $v['biz_id'] . "')\">系统商品提成:" . ($v['biz_deduct'] * 100) . '%' . "</div>
                                    <div onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['custom_pro_deduct'] . "','" . $mark . "','13','" . $v['biz_id'] . "')\">自定义商品提成:" . ($v['custom_pro_deduct'] * 100) . '%' . "</div>
                                </div>

                            </td>
                            
                            
                            
                            
                            
                        
                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
                }
            } else {
                #加盟门店
                foreach ($arr as $k => $v) {
                    if ($v['timer'] == 1) {
//                    $_timer = "<button class=\"layui-btn layui-btn-xs layui-btn-radius layui-btn-normal\" >开启</button>";
                        $_timer = "开启";
                    } else {
//                    $_timer = "<button class=\"layui-btn layui-btn-xs layui-btn-radius layui-btn-danger\" >关闭</button>";
                        $_timer = "关闭";
                    }
                    #服务类型名称
                    $title = Db::table('service_class')->field('service_class_title')->where(array('id' => $v['service_class_id']))->find();
                    $v['service_class_title'] = $title['service_class_title'];
                    $_str .= "<tr id='detailTr" . $v['id'] . "'>
                            <td onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['station_title'] . "','" . $mark . "','1','" . $v['biz_id'] . "')\">" . $v['station_title'] . "</td>
                            <td onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['negative_price'] . "','" . $mark . "','7','" . $v['biz_id'] . "')\">" . $v['negative_price'] . "</td>
                            <td>
                                <button class=\"btn btn-primary\" type=\"button\"   onclick=\"getrecommond('" . $v['recommond_royalty'] . "','" . $v['recommond_custom_service'] . "','" . $v['recommond_pro_royalty'] . "','" . $v['recommond_custom_pro'] . "','$mark','" . $v['id'] . "','" . $k . "','" . $v['biz_id'] . "','recommond')\">查看</button>
                            </td>
                            <td>
                                <div class=\"recommond\" style=\"margin-bottom: 5px\">
                                    <div style=\"margin-right: 5px\" onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['service_deduct'] . "','" . $mark . "','3','" . $v['biz_id'] . "')\">系统服务提成:" . ($v['service_deduct'] * 100) . '%' . "</div>
                                    
                                    <div onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['custom_service_deduct'] . "','" . $mark . "','12','" . $v['biz_id'] . "')\">自定义服务提成:" . ($v['custom_service_deduct'] * 100) . '%' . "</div>
                                </div>

                            </td>
                            <td>
                                <div class=\"recommond\" style=\"margin-bottom: 5px\">
                                    <div style=\"margin-right: 5px\"  onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['biz_deduct'] . "','" . $mark . "','4','" . $v['biz_id'] . "')\">系统商品提成:" . ($v['biz_deduct'] * 100) . '%' . "</div>
                                    <div onclick=\"stationWrite('" . $v['id'] . "','" . $k . "','" . $v['custom_pro_deduct'] . "','" . $mark . "','13','" . $v['biz_id'] . "')\">自定义商品提成:" . ($v['custom_pro_deduct'] * 100) . '%' . "</div>
                                </div>

                            </td>
                        
                            <td onclick=\"stationStatus('" . $v['id'] . "','" . $k . "','" . $v['timer'] . "','" . $mark . "','5','" . $v['biz_id'] . "')\">" . $_timer . "</td>
                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
                }
            }


        }
        /*服务名称
                                    <td>" . $v['service_class_title'] . "</td>

        */
        return $_str;
    }

    //工位点击修改
    function stationWrite()
    {
        /*id  k  value  mark, vamrk*/
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 1) {

                    # 直接操作数据库 station_title 名称
                    Db::table("station")->where(array('id' => $info['id']))->update(array('station_title' => $info['value']));
                } else if ($info['vmark'] == 3) {
                    # 直接操作数据库 service_deduct 服务提成
                    Db::table("station")->where(array('id' => $info['id']))->update(array('service_deduct' => $info['value']));
                } else if ($info['vmark'] == 4) {
                    # 直接操作数据库 biz_deduct 商品提成
                    Db::table("station")->where(array('id' => $info['id']))->update(array('biz_deduct' => $info['value']));
                } else if ($info['vmark'] == 5) {
                    # 直接操作数据库 timer 状态
                    if ($info['value'] == 1) {
                        $value = 0;
                    } else {
                        $value = 1;

                    }
                    Db::table("station")->where(array('id' => $info['id']))->update(array('timer' => $value));
                } else if ($info['vmark'] == 7) {
                    # 直接操作数据库 negative_price 员工差评处罚
                    Db::table("station")->where(array('id' => $info['id']))->update(array('negative_price' => $info['value']));
                } else if ($info['vmark'] == 8) {
                    # 直接操作数据库 recommond_royalty 推荐提成
                    Db::table("station")->where(array('id' => $info['id']))->update(array('recommond_royalty' => $info['value']));
                } else if ($info['vmark'] == 9) {
                    # 直接操作数据库 recommond_royalty 请输入自定义推荐服务提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update(array('recommond_custom_service' => $info['value']));
                } else if ($info['vmark'] == 10) {
                    # 直接操作数据库 recommond_royalty 请输入系统推荐商品提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update(array('recommond_pro_royalty' => $info['value']));
                } else if ($info['vmark'] == 11) {
                    # 直接操作数据库 recommond_royalty 请输入自定义推荐商品提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update(array('recommond_custom_pro' => $info['value']));
                } else if ($info['vmark'] == 12) {
                    # 直接操作数据库 recommond_royalty 请输入自定义服务提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update(array('custom_service_deduct' => $info['value']));
                } else if ($info['vmark'] == 13) {
                    # 直接操作数据库 recommond_royalty 请输入自定义商品提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update(array('custom_pro_deduct' => $info['value']));
                } else if ($info['vmark'] == 'recommond') {
                    #所有推荐提成

                    $arr['recommond_royalty'] = $info['recommond_royalty'];
                    $arr['recommond_custom_service'] = $info['recommond_custom_service'];
                    $arr['recommond_pro_royalty'] = $info['recommond_pro_royalty'];
                    $arr['recommond_custom_pro'] = $info['recommond_custom_pro'];
                    # 直接操作数据库 recommond_royalty 请输入自定义商品提成比例
                    Db::table("station")->where(array('id' => $info['id']))->update($arr);
                }


                return array('status' => true);

            } else {
                # 操作session
                $_session = Session('stationInfoSession');


                if (!empty($_session)) {
                    if ($info['vmark'] == 1) {
                        #station_title 名称
                        $_session[$info['k']]['station_title'] = $info['value'];

                    } else if ($info['vmark'] == 3) {
                        #service_deduct 服务提成
                        $_session[$info['k']]['service_deduct'] = $info['value'];

                    } else if ($info['vmark'] == 4) {
                        #biz_deduct 商品提成
                        $_session[$info['k']]['biz_deduct'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        if ($info['value'] == 1) {
                            $value = 0;
                        } else {
                            $value = 1;

                        }
                        #biz_deduct 商品提成
                        $_session[$info['k']]['timer'] = $value;

                    } else if ($info['vmark'] == 7) {
                        #negative_price 差评处罚金额
                        $_session[$info['k']]['negative_price'] = $info['value'];

                    } else if ($info['vmark'] == 8) {
                        #recommond_royalty 推荐提成
                        $_session[$info['k']]['recommond_royalty'] = $info['value'];

                    } else if ($info['vmark'] == 10) {
                        #recommond_pro_royalty 系统推荐商品提成比列
                        $_session[$info['k']]['recommond_pro_royalty'] = $info['value'];

                    } else if ($info['vmark'] == 9) {
                        #recommond_custom_service 自定义推荐服务提成
                        $_session[$info['k']]['recommond_custom_service'] = $info['value'];

                    } else if ($info['vmark'] == 11) {
                        #recommond_custom_pro 自定义推荐商品提成
                        $_session[$info['k']]['recommond_custom_pro'] = $info['value'];

                    } else if ($info['vmark'] == 12) {
                        #custom_service_deduct 自定义服务提成
                        $_session[$info['k']]['custom_service_deduct'] = $info['value'];

                    } else if ($info['vmark'] == 13) {
                        #custom_pro_deduct 自定义商品提成
                        $_session[$info['k']]['custom_pro_deduct'] = $info['value'];

                    } else if ($info['vmark'] == 'recommond') {
                        #所有推荐提成
                        $_session[$info['k']]['recommond_royalty'] = $info['recommond_royalty'];
                        $_session[$info['k']]['recommond_custom_service'] = $info['recommond_custom_service'];
                        $_session[$info['k']]['recommond_pro_royalty'] = $info['recommond_pro_royalty'];
                        $_session[$info['k']]['recommond_custom_pro'] = $info['recommond_custom_pro'];

                    }

                    Session::set('stationInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [getArrInfo 工位删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function stationDel()
    {
        /*id k mark*/
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                DB::table('station')->where(array('id' => $info['id']))->delete();
                return array('status' => true);

            }
        } else {
            #删除缓存
            $_session = Session('stationInfoSession');
            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);


                    Session::set('stationInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }
    /************************商品权限start***************************************/
    /**
     * [bizproList 商品权限  商品列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizproList()
    {
        $biz_id = session('bizId');
        $_where = 'biz_pro_status != 3';
        $biz_pro_id_arr = array();
        if (!empty($biz_id)) {
            $biz_pro_id_arr = Db::table('biz_goods')->where(array('biz_id' => $biz_id))->column('pro_id');


        }
        #查询缓存中的 id   和数据库中  拼接
        $_session = session('bizproInfoSession');
        if (!empty($_session)) {
            foreach ($_session as $k => $v) {
                array_push($biz_pro_id_arr, $v['id']);
            }
        }


        #全新的数组 id  (数据库+缓存)
        if (!empty($biz_pro_id_arr)) {
            $biz_pro_id = implode(',', $biz_pro_id_arr);
            if (!empty($biz_pro_id)) {
                $_where .= " and b.id not in ($biz_pro_id)";

            }
        }
        # 查询商品名称
        $pro = Db::table('biz_pro b')
            ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
            ->join('pro_class pc', 'pc.id = b.biz_pro_class_id')
            ->where($_where)->select();

        $countBizpro = Db::table('biz_goods bg')
            ->where(array('bg.biz_id' => $biz_id))->count();
        if (!empty($pro)) {
            #求缓存的 数组个数
            $_session = session('bizproInfoSession');
            if (!empty($_session))
                $count_session = count($_session);
            $countBizpro = intval($countBizpro) + intval($count_session);
            #关联商品权限模板
            $str = BizService::BizProMould($pro, $biz_id, $countBizpro);
            return array('status' => true, 'data' => $str);

        } else {
            return array('status' => false, 'msg' => '暂时没有搜到你想要的商品！');

        }


    }

    /**
     * [bizproSession 商品权限  存session]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizproSession()
    {
        #商品id 字符串
        $str = input("post.value");
        if (!empty($str)) {
            $arr = explode(',', $str);
            $bizpro_arr = array();
            #获取商品 是否有重复的
            $bizproInfoSession = Session('bizproInfoSession');
            #缓存是否存在
            if (empty($bizproInfoSession)) {
                foreach ($arr as $k => $v) {
                    $bizpro = Db::table("biz_pro b")
                        ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
                        ->join('pro_class pc', 'pc.id = b.biz_pro_class_id')
                        ->where(array('b.id' => $v))->find();
                    array_push($bizpro_arr, $bizpro);

                }
            }
//            dump($bizpro_arr);die;
//            dump($str);
            #是否是第一次存的数组
            if (!empty($bizpro_arr)) {
                foreach ($bizpro_arr as $k => $v) {
                    $bizpro_arr[$v['id']] = $v;
                    unset($bizpro_arr[$k]);
                }
                #存session
                Session::set('bizproInfoSession', $bizpro_arr);
            } else {
                #不是第一次 证明有缓存
                #判断缓存中的商品id  是否跟数据库中的相同  相同则删除


                #获取数组之前遍历 把id 赋值在二维数组键名上
                foreach ($bizproInfoSession as $sk => $sv) {
                    $bizproInfoSession[$sv['id']] = $sv;
                    unset($bizproInfoSession[$sk]);
                }
                #$arr  id 数组
                foreach ($arr as $ak => $av) {
                    if (!isset($bizproInfoSession[$av])) {
                        $bizpro = Db::table("biz_pro b")
                            ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
                            ->join('pro_class pc', 'pc.id = b.biz_pro_class_id')
                            ->where(array('b.id' => $av))->find();
                        array_push($bizproInfoSession, $bizpro);
                    }

                }
                Session::set('bizproInfoSession', $bizproInfoSession);

            }


            #获取门店id
            $bizId = session('bizId');
            if (!empty($bizId)) {
                #查询数据库中的 商品id biz_goods
                $bizgoods = BizService::BizGoodsInfo($bizId);

                if (!empty($bizgoods)) {
                    #获取最新缓存
                    $_Session = Session('bizproInfoSession');
                    foreach ($bizgoods as $k => $v) {
                        if (isset($_Session[$v['pro_id']])) {
                            #证明数据库中有值  删除缓存为这个键值的id
                            unset($_Session[$v['pro_id']]);
                        }
                    }
                    Session::set('bizproInfoSession', $_Session);
                }
            }
            #获取最新缓存
            $_session = Session('bizproInfoSession');
            if (!empty($_session)) {
                return array('status' => true);

            } else {
                return array('status' => false, 'msg' => '已有关联商品！');

            }

        }

    }

    /**
     * [relateOption 商品权限  已关联列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function relateOption()
    {
        #先看这个数组是否有
        $bizproInfoSession = Session('bizproInfoSession');
        if (empty($bizproInfoSession))
            $bizproInfoSession = array();

        $_str = '';
        #门店id
        $_bizId = input('post.bizId');

        $_detail = [];
        if (!empty($_bizId)) {
            # 查询门店的的 商品权限
            $_detail = Db::table('biz_goods bg')
                ->field('bg.biz_id,bg.pro_id id,bg.goods_price,bg.goods_online biz_pro_online,bg.goods_purch biz_pro_purch,bg.settlement_min,bp.biz_pro_title,bp.biz_pro_number,pc.class_title')
                ->join('biz_pro bp', 'bp.id= bg.pro_id', 'left')
                ->join('pro_class pc', 'pc.id = bp.biz_pro_class_id')
                ->where(array('bg.biz_id' => $_bizId))->select();
            if (!empty($_detail)) {
                #已关联列表页面模板
                $_str .= BizService::getBizproArrInfo($_detail, 'DB', $_bizId);
            }

        }
        #已关联列表页面模板
        $_str .= BizService::getBizproArrInfo($bizproInfoSession, 'S', '');
        if (!empty($_str)) {
            return array('status' => true, 'data' => $_str, 'count' => count($_detail));

        } else {
            return array('status' => false, 'msg' => '暂无关联商品，请先关联！');

        }


    }

    //商品权限点击修改 售货
    function bizproWrite()
    {
        /*id  value  mark, vamrk*/
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {

                    # 直接操作数据库
                    Db::table("biz_goods")->where(array('pro_id' => $info['id'], 'biz_id' => $info['biz_id']))->update(array('goods_online' => $info['value']));
                } else if ($info['vmark'] == 5) {
                    #供货价格
                    Db::table("biz_goods")->where(array('pro_id' => $info['id'], 'biz_id' => $info['biz_id']))->update(array('goods_purch' => $info['value']));

                }

                return array('status' => true);

            } else {
                # 操作session
                $_session = Session('bizproInfoSession');

                if (!empty($_session)) {
                    if ($info['vmark'] == 4) {
                        #bizproInfoSession 线上价格  售货价格
                        $_session[$info['k']]['biz_pro_online'] = $info['value'];

                    }
                    Session::set('bizproInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [bizProDel 商品权限删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizProDel()
    {
        /*id k mark*/
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                DB::table('biz_goods')->where(array('pro_id' => $info['id'], 'biz_id' => $info['biz_id']))->delete();
                return array('status' => true);

            }
        } else {
            #删除缓存
            $_session = Session('bizproInfoSession');
            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);


                    Session::set('bizproInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    /**
     * [serviceWriteList 服务权限总体修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function bizProWriteList()
    {
        /*id k mark*/
        $info = input("get.");
        $_where = array('pro_id' => $info['id'], 'biz_id' => $info['biz_id']);
        if (input("get.action") == 'ajax') {

            #点击保存
            $_info = input("post.");

            if ($info['mark'] == 'DB') {

                #修改商品  biz_pro
                $bizpro_info = array();
                $bizpro_info['biz_pro_title'] = $_info['biz_pro_title'];
                $bizpro_info['biz_pro_number'] = $_info['biz_pro_number'];
                if (!empty($_info['biz_pro_class_id'])) {
                    $bizpro_info['biz_pro_class_id'] = $_info['biz_pro_class_id'];

                }
                $bizpro_info['biz_pro_purch'] = $_info['biz_pro_purch'];
                $bizpro_info['biz_pro_online'] = $_info['biz_pro_online'];
                #销售价格 就是线上价格
                #删除不需要的
                unset($_info['service_title'], $_info['service_class_id'], $_info['biz_pro_number']);
                unset($_info['id'], $_info['mark'], $_info['biz_id'], $_info['k']);

                if (!empty($info['id'])) {
                    #$_info  需要处理
                    #数据库
                    $service = DB::table('biz_goods')->where($_where)->update(array(
                        'goods_online' => $_info['biz_pro_online'],
                        'goods_purch' => $_info['biz_pro_purch'],
                        'settlement_min' => $_info['settlement_min'],
                    ));
                    #修改 服务
                    Db::table("biz_pro")->where(array('id' => $info['id']))->update($bizpro_info);
                }
            } else {

                #缓存
                $_session = Session('bizproInfoSession');
                if (isset($info['k'])) {
                    if (!empty($_session)) {
                        #修改服务名称 分类

                        $_session[$info['k']]['biz_pro_title'] = $_info['biz_pro_title'];
                        if (!empty($_info['biz_pro_class_id'])) {
                            $_session[$info['k']]['biz_pro_class_id'] = $_info['biz_pro_class_id'];
                        }
                        $_session[$info['k']]['biz_pro_number'] = $_info['biz_pro_number'];
                        $_session[$info['k']]['biz_pro_online'] = $_info['biz_pro_online']; //销售价格
                        $_session[$info['k']]['biz_pro_purch'] = $_info['biz_pro_purch'];
                    }
                }
                Session::set('bizproInfoSession', $_session);

            }
            return array('status' => true);
        } else {
            if ($info['mark'] == 'DB') {
                if (!empty($info['id'])) {

                    $service = BizService::getServiceWrite($_where, 'bizpro');
                }
            } else {
                #缓存
                $_session = Session('bizproInfoSession');
                if (isset($info['k'])) {
                    if (!empty($_session)) {
                        $service = $_session[$info['k']];

                    }
                }

            }

            return array('status' => true, 'info' => $service, 'service' => $info);

        }


    }



    /************************商品权限end***************************************/
    /************************服务权限start***************************************/
    /**
     * [serviceList 服务权限  服务列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function serviceList()
    {
        #查询这个门店关联的服务
        $biz_id = session("bizId");
        $_where = 's.is_del = 2 and s.service_status = 1';
        $biz_service_id_arr = array();
        if (!empty($biz_id)) {
            #查询数据库中
            $biz_service_id_arr = Db::table('service_biz')->where(array('biz_id' => $biz_id))->column('service_id');
        }
        #查询缓存中的 id   和数据库中  拼接
        $_session = session('serviceInfoSession');
        if (!empty($_session)) {
            foreach ($_session as $k => $v) {
                array_push($biz_service_id_arr, $v['id']);
            }
        }
        #全新的数组 id  (数据库+缓存)
        if (!empty($biz_service_id_arr)) {
            $biz_service_id = implode(',', $biz_service_id_arr);
            if (!empty($biz_service_id)) {
                $_where .= " and s.id not in ($biz_service_id)";

            }
        }
        # 查询服务名称
        $pro = Db::table('service s')
            ->field('s.id,s.service_title,s.service_time,s.appoint,s.settlement_min,sc.service_class_title')
            ->join('service_class sc', 'sc.id = s.service_class_id')
            ->where($_where)
            ->select();
        $countService = Db::table('service_biz sb')
            ->where(array('sb.biz_id' => $biz_id))->count();
        if (!empty($pro)) {
            #求缓存的 数组个数
            $_session = session('serviceInfoSession');
            if (!empty($_session))
                $count_session = count($_session);
            $countService = intval($countService) + intval($count_session);
            #关联商品权限模板
            $str = BizService::ServiceMould($pro, $biz_id, $countService);
            return array('status' => true, 'data' => $str);

        } else {
            return array('status' => false, 'msg' => '暂时没有搜到你想要的服务！');

        }


    }

    /**
     * [searchServiceName 搜索服务名称   和商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function searchServiceName()
    {
        $biz_id = session("bizId");
        #服务 商品  名称
        $search_title = input("post.search_title");
        if (empty($search_title))
            $search_title = input("post.searchTitle");
        #service :服务 pro :商品
        $activeType = input("post.activeType");
        if (empty($activeType))
            $activeType = input("post.searchType");
        #筛选的分类
        $select_biztype = input("post.select_biztype");
        if (empty($select_biztype))
            $select_biztype = input("post.selectValue");
        $_where = '';


        if ($activeType == 'service') {
            #在这查询门店关联了几个服务  数据库中
            $biz_service_id_arr = array();
            if (!empty($biz_id)) {
                #查询数据库中
                $biz_service_id_arr = Db::table('service_biz')->where(array('biz_id' => $biz_id))->column('service_id');

            }
            #查询缓存中的 id   和数据库中  拼接
            $_session = session('serviceInfoSession');
            if (!empty($_session)) {
                foreach ($_session as $k => $v) {
                    array_push($biz_service_id_arr, $v['id']);
                }
            }
            #全新的数组 id  (数据库+缓存)
            if (!empty($biz_service_id_arr)) {
                $biz_service_id = implode(',', $biz_service_id_arr);
                if (!empty($biz_service_id)) {
                    $_where .= " and s.id not in ($biz_service_id)";

                }
            }

            if (!empty($search_title))
                $_where .= " and (s.service_title like '%" . $search_title . "%' or s.service_number like '%".$search_title."%')";
            if (!empty($select_biztype))
                $_where .= " and s.service_class_id =$select_biztype";
            # 查询服务名称
            $service = Db::table('service s')
                ->field('s.id,s.service_title,s.service_time,s.appoint,s.settlement_min,sc.service_class_title')
                ->join('service_class sc', 'sc.id = s.service_class_id')
                ->where("s.is_del = 2 and s.service_status = 1" . $_where)->select();
            $countService = Db::table('service_biz sb')
                ->where(array('sb.biz_id' => $biz_id))->count();
            if (!empty($service)) {
                #求缓存的 数组个数
                $_session = session('serviceInfoSession');
                if (!empty($_session))
                    $count_session = count($_session);
                $countService = intval($countService) + intval($count_session);
                #关联服务权限模板
                $str = BizService::ServiceMould($service, $biz_id, $countService, $search_title, $select_biztype);
                return array('status' => true, 'data' => $str);

            } else {
                return array("status" => false, 'msg' => '暂无数据');
            }
        } else {
            #商品
            $biz_pro_id_arr = array();
            if (!empty($biz_id)) {
                $biz_pro_id_arr = Db::table('biz_goods')->where(array('biz_id' => $biz_id))->column('pro_id');
            }
            #查询缓存中的 id   和数据库中  拼接
            $_session = session('bizproInfoSession');
            if (!empty($_session)) {
                foreach ($_session as $k => $v) {
                    array_push($biz_pro_id_arr, $v['id']);
                }
            }


            #全新的数组 id  (数据库+缓存)
            if (!empty($biz_pro_id_arr)) {
                $biz_pro_id = implode(',', $biz_pro_id_arr);
                if (!empty($biz_pro_id)) {
                    $_where .= " and b.id not in ($biz_pro_id)";

                }
            }
            if (!empty($search_title))
                $_where .= " and b.biz_pro_title like '%" . $search_title . "%'";
            if (!empty($select_biztype))
                $_where .= " and b.biz_pro_class_id =$select_biztype";

            # 查询商品名称
            $pro = Db::table('biz_pro b')
                ->field('b.id,b.biz_pro_title,b.biz_pro_number,b.biz_pro_purch,b.biz_pro_class_id,b.biz_pro_price,b.settlement_min,b.biz_pro_online,pc.class_title')
                ->join('pro_class pc', 'pc.id = b.biz_pro_class_id')
                ->where("biz_pro_status != 3" . $_where)->select();

            $countBizpro = Db::table('biz_goods bg')
                ->where(array('bg.biz_id' => $biz_id))->count();
            if (!empty($pro)) {
                #求缓存的 数组个数
                $_session = session('bizproInfoSession');
                if (!empty($_session))
                    $count_session = count($_session);
                $countBizpro = intval($countBizpro) + intval($count_session);
                #关联服务权限模板
                $str = BizService::BizProMould($pro, $biz_id, $countBizpro, $search_title, $select_biztype);
                return array('status' => true, 'data' => $str);

            } else {
                return array("status" => false, 'msg' => '暂无数据');
            }
        }


    }

    /**
     * [serviceSession 服务权限  存session]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function serviceSession()
    {
        #商品id 字符串
        $str = input("post.value");
        #结算最低价格
        $carprice[0] = input("post.dcarprice");
        $carprice[1] = input("post.zcarprice");
        $carprice[2] = input("post.xcarprice");
        if (!empty($str)) {
            $arr = explode(',', $str);
            $bizpro_arr = array();
            #获取服务 是否有重复的
            $serviceInfoSession = Session('serviceInfoSession');
            #缓存是否存在
            if (empty($serviceInfoSession)) {
                foreach ($arr as $k => $v) {

                    $bizpro = Db::table('service s')
                        ->field('s.id,s.service_class_id,s.service_title,s.appoint,s.settlement_min,s.payment_mark,s.service_minprice,s.timer,s.service_refer,s.iscut,sc.service_class_title')
                        ->join('service_class sc', 'sc.id = s.service_class_id')
                        ->where(array('s.id' => $v))->find();
                    array_push($bizpro_arr, $bizpro);

                }
            }
//            dump($bizpro_arr);die;
//            dump($str);
            #是否是第一次存的数组
            if (!empty($bizpro_arr)) {
                foreach ($bizpro_arr as $k => $v) {
                    $bizpro_arr[$v['id']] = $v;
                    $bizpro_arr[$v['id']]['carprice'] = $carprice;
                    unset($bizpro_arr[$k]);
                }
                #存session
                Session::set('serviceInfoSession', $bizpro_arr);
            } else {
                #不是第一次 证明有缓存
                #判断缓存中的商品id  是否跟数据库中的相同  相同则删除


                #获取数组之前遍历 把id 赋值在二维数组键名上

                $arrInfo = array();
                #$arr  id 数组
                foreach ($arr as $ak => $av) {
//                    if (!isset($serviceInfoSession[$av])) {
                        $bizpro = Db::table('service s')
                            ->field('s.id,s.service_class_id,s.service_title,s.appoint,s.settlement_min,s.payment_mark,s.service_minprice,s.timer,s.service_refer,s.iscut,sc.service_class_title')
                            ->join('service_class sc', 'sc.id = s.service_class_id')
                            ->where(array('s.id' => $av))->find();
                        array_push($arrInfo, $bizpro);
//                    }

                }
                foreach ($arrInfo as $sk => $sv) {
                    #最低结算金额插入数组里
                    $serviceInfoSession[$sv['id']] = $sv;
                    $serviceInfoSession[$sv['id']]['carprice'] = $carprice;

                    unset($arrInfo[$sk]);
                }
                Session::set('serviceInfoSession', $serviceInfoSession);

            }


            #获取门店id
            $bizId = session('bizId');
            if (!empty($bizId)) {
                #查询服务中 服务id
                $servicegoods = BizService::ServiceGoodsInfo($bizId);


                if (!empty($servicegoods)) {
                    #获取最新缓存
                    $_Session = Session('serviceInfoSession');

                    foreach ($servicegoods as $k => $v) {
                        if (isset($_Session[$v['service_id']])) {
                            #证明数据库中有值  删除缓存为这个键值的id
                            unset($_Session[$v['service_id']]);
                        }
                    }


                    Session::set('serviceInfoSession', $_Session);
                }
            }
            #获取最新缓存
            $_session = Session('serviceInfoSession');
            if (!empty($_session)) {
                return array('status' => true);

            } else {
                return array('status' => false, 'msg' => '已有关联服务！');

            }

        }

    }

    /**
     * [relateOption 服务权限  已关联列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function ServiceRelation()
    {
        #先看这个数组是否有
        $serviceInfoSession = Session('serviceInfoSession');
//        dump($serviceInfoSession);die;
        if (empty($serviceInfoSession))
            $serviceInfoSession = array();

        $_str = '';
        #门店id
        $_bizId = input('post.bizId');


        if (!empty($_bizId)) {
            # 查询门店的的 服务权限
            $_detail = Db::table('service_biz sb')
                ->field('sb.biz_id,sb.service_id id,s.service_title,sb.appoint,sb.settlement_min,sb.payment_mark,sb.service_minprice,sb.timer,sb.service_refer,sb.iscut,sc.service_class_title')
                ->join('service s', 's.id = sb.service_id and s.biz_pid = 0')
                ->join('service_class sc', 'sc.id = s.service_class_id')
                ->where(array('sb.biz_id' => $_bizId))->select();
            #查询当前门店id的和服务id 的 结算价格
            $_detail = BizService::getCarPrice($_detail, $_bizId);
            if (!empty($_detail)) {
                #已关联列表页面模板
                $_str .= BizService::getServiceArrInfo($_detail, 'DB', $_bizId);
            }

        }

        #已关联列表页面模板
        $_str .= BizService::getServiceArrInfo($serviceInfoSession, 'S', '');
        if (!empty($_str)) {
            return array('status' => true, 'data' => $_str);

        } else {
            return array('status' => false, 'msg' => '暂无关联服务，请先关联！');

        }


    }

    //服务权限点击修改 结算价格
    function serviceWrite()
    {
        /*id  value  mark, vamrk*/
        $info = input("post.");
        if (!empty($info)) {
            if ($info['mark'] == 'DB') {
                if ($info['vmark'] == 4) {

                    # 直接操作数据库
                    Db::table("service_biz")->where(array('service_id' => $info['id'], 'biz_id' => $info['biz_id']))->update(array('settlement_min' => $info['value']));
                } else if ($info['vmark'] == 5) {
                    # 直接操作数据库 appoint 状态
                    if ($info['value'] == 1) {
                        $value = 0;
                    } else {
                        $value = 1;

                    }
                    Db::table("service_biz")->where(array('service_id' => $info['id'], 'biz_id' => $info['biz_id']))->update(array('appoint' => $value));
                } else if ($info['vmark'] == 'carprice') {
                    # 修改结算价格 根据 biz_id  和服务$info['id'] 修改 线上 线下 的结算价格
                    $carprice[0] = $info['dcarprice'];
                    $carprice[1] = $info['zcarprice'];
                    $carprice[2] = $info['xcarprice'];
                    BizService::getBizServiceprice($info['biz_id'], $info['id'], $carprice);

                }

                return array('status' => true);

            } else {
                # 操作session
                $_session = Session('serviceInfoSession');


                if (!empty($_session)) {
                    if ($info['vmark'] == 4) {
                        #bizproInfoSession 线上价格  售货价格
                        $_session[$info['k']]['settlement_min'] = $info['value'];

                    } else if ($info['vmark'] == 5) {
                        #bizproInfoSession 线上价格  售货价格
                        if ($info['value'] == 1) {
                            $value = 0;
                        } else {
                            $value = 1;

                        }
                        $_session[$info['k']]['appoint'] = $value;

                    } else if ($info['vmark'] == 'carprice') {
                        # 修改结算价格 根据 biz_id  和服务$info['id'] 修改 线上 线下 的结算价格
                        $_session[$info['k']]['carprice'][0] = $info['dcarprice'];
                        $_session[$info['k']]['carprice'][1] = $info['zcarprice'];
                        $_session[$info['k']]['carprice'][2] = $info['xcarprice'];

                    }
                    Session::set('serviceInfoSession', $_session);
                    $_session = Session('serviceInfoSession');
                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }
        }
    }

    /**
     * [serviceDel 服务权限删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function serviceDel()
    {
        /*id k mark*/
        $info = input("post.");
        if ($info['mark'] == 'DB') {
            if (!empty($info['id'])) {
                DB::table('service_biz')->where(array('service_id' => $info['id'], 'biz_id' => $info['biz_id']))->delete();
                #修改结算价格为0
                BizService::cleanPriceCar($info['id'], $info['biz_id']);


                return array('status' => true);

            }
        } else {
            #删除缓存
            $_session = Session('serviceInfoSession');
            if (isset($info['k'])) {

                if (!empty($_session)) {
                    unset($_session[$info['k']]);


                    Session::set('serviceInfoSession', $_session);

                    return array('status' => true);
                } else {
                    return array('status' => false);
                }
            }

        }


    }

    /**
     * [serviceWriteList 服务权限总体修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function serviceWriteList()
    {
        /*id k mark*/
        $info = input("get.");
        $_where = array('service_id' => $info['id'], 'biz_id' => $info['biz_id']);
        if (input("get.action") == 'ajax') {

            #点击保存
            $_info = input("post.");
            if ($info['mark'] == 'DB') {
                #修改 线上价格
                if (isset($_info['biz_discount_id'])) {
                    foreach ($_info['biz_discount_id'] as $k => $v) {
                        Db::table('service_car_mediscount')->where(array('id' => $v))->update(array('discount' => $_info['biz_discount'][$k]));

                    }
                }
                #修改门店价格
                if (isset($_info['online_discount_id'])) {
                    foreach ($_info['online_discount_id'] as $k => $v) {
                        Db::table('service_car_mediscount')->where(array('id' => $v))->update(array('discount' => $_info['online_discount'][$k]));

                    }
                }
                #修改服务  service
                $service_info = array();
                $service_info['service_title'] = $_info['service_title'];
                $service_info['service_class_id'] = $_info['service_class_id'];
                #删除不需要的
                unset($_info['online_discount_id'], $_info['online_discount'], $_info['biz_discount_id'], $_info['biz_discount'], $_info['service_title'], $_info['service_class_id']);
                unset($_info['id'], $_info['mark'], $_info['biz_id'], $_info['k']);

                if (!empty($info['id'])) {
                    #$_info  需要处理
                    #数据库
                    $service = DB::table('service_biz')->where($_where)->update($_info);
                    #修改 服务
                    Db::table("service")->where(array('id' => $info['id']))->update($service_info);
                }
            } else {

                #缓存
                $_session = Session('serviceInfoSession');
                // dump($_session);die;
                if (isset($info['k'])) {
                    if (!empty($_session)) {
                        #修改服务名称 分类
                        #修改 线上价格
                        if ($_info['biz_discount_id']) {
                            foreach ($_info['biz_discount_id'] as $k => $v) {
                                Db::table('service_car_mediscount')->where(array('id' => $v))->update(array('discount' => $_info['biz_discount'][$k]));

                            }
                        }
                        #修改门店价格
                        if ($_info['online_discount_id']) {
                            foreach ($_info['online_discount_id'] as $k => $v) {
                                Db::table('service_car_mediscount')->where(array('id' => $v))->update(array('discount' => $_info['online_discount'][$k]));

                            }
                        }
                        $_session[$info['k']]['service_title'] = $_info['service_title'];
                        $_session[$info['k']]['service_class_id'] = $_info['service_class_id'];
                        $_session[$info['k']]['settlement_min'] = $_info['settlement_min'];
                        $_session[$info['k']]['appoint'] = $_info['appoint'];
                    }
                }
                Session::set('serviceInfoSession', $_session);

            }
            return array('status' => true);
        } else {
            if ($info['mark'] == 'DB') {
                if (!empty($info['id'])) {

                    $service = BizService::getServiceWrite($_where, '');
                    /*#门店
                    $where_biz = array('scm.service_id'=>113,'scm.biz_id'=>2,'service_type'=>1);
                    #线上
                    $where_online = array('scm.service_id'=>113,'scm.biz_id'=>2,'service_type'=>2);*/
                    #门店
                    $where_biz = array('scm.service_id' => $info['id'], 'scm.biz_id' => $info['biz_id'], 'service_type' => 1);
                    #线上
                    $where_online = array('scm.service_id' => $info['id'], 'scm.biz_id' => $info['biz_id'], 'service_type' => 2);
                }
            } else {
                #缓存
                $_session = Session('serviceInfoSession');
                if (isset($info['k'])) {
                    if (!empty($_session)) {
                        $service = $_session[$info['k']];

                        #门店
                        $where_biz = array('scm.service_id' => $service['id'], 'scm.biz_id' => $info['biz_id'], 'service_type' => 1);
                        #线上
                        $where_online = array('scm.service_id' => $service['id'], 'scm.biz_id' => $info['biz_id'], 'service_type' => 2);
                    }
                }

            }
            #请求服务价格  门店价格 商品价格 按 汽车来

            $biz_price = BizService::getServicebizWrite($where_biz);
            $biz_online = BizService::getServicebizWrite($where_online);
            $this->assign('biz_price', $biz_price);
            $this->assign('biz_online', $biz_online);
            return array('status' => true, 'biz_price' => $biz_price, 'biz_online' => $biz_online, 'info' => $service, 'service' => $info);
        }


    }

    /**
     * [checkServiceCarPrice 判断服务价格显示页]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function servicePrice()
    {
        $id = input("get.service_id");
        //是service 查询的是服务本身的价格
        $mark = input("get.mark");
        if (!empty($id)) {
            #线上价格
            $oline_price = BizService::getServicePrice($id, '1', $mark);
            #门店价格
            $biz_price = BizService::getServicePrice($id, '2', $mark);
            $this->assign('biz_online', $oline_price);
            $this->assign('biz_price', $biz_price);
            return $this->fetch('/biz/servicePrice');


        }

    }

    /************************服务权限end***************************************/
    function StationList()
    {
        $str = '';

        $str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                      
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            <button class=\"btn btn-info\" onclick=\"relevanceOption('check-item')\" style=\"\">批量关联</button>
                                            <button class=\"btn btn-info\" onclick=\"relateOption()\" style=\"\">已关联列表</button>
                                        </div>
                                    </div>
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                                                <th>商品名称</th>
                                                <th>商品编号</th>
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                                <th>销售价格</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

        $str .= "
                </tbody>
            </table>
        </div>
            </div>";
        return array('status' => true, 'info' => $str);
    }


    /***************************** 门店 详情所有记录 start ****************************************************************/
    /*
     * @content: 门店服务记录
     * @params : bizid ： 门店id
     * */
    function bizServiceLog()
    {
        $bizId = input("get.bizId");

        if (input("get.action") == 'ajax') {
            $info = input("searchParams");

            $_where = [];
            if (!empty($info['keywords'])) {
                $_where[] = ['log_service_source', 'like', '%' . $info['keywords'] . '%'];

            }

            if (!empty($info['start_time'])) {
                $_where[] = ['log_service_create', '>=', $info['start_time']];
            }

            if (!empty($info['end_time'])) {
                $_where[] = ['log_service_create', '<=', $info['end_time']];
            }
            $_where[] = ['biz_id', '=', $bizId];
            /*
            if(!empty($info['start_time'])){
                $start_time =date_create($info['start_time']);

                $start_time =date_format($start_time,"Y-m-d H:i:s");
                $_where .=" and log_service_create >='".$start_time."'";
            }

            if(!empty($info['end_time'])){
                $end_time =date_create($info['start_time']);
                $end_time =date_format($end_time,"Y-m-d H:i:s");

                $_where .=" and log_service_create <='".$end_time."'";
            }
            */
            //dump($_where);exit;

            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
                'table' => 'log_service',//查找表名
                'field' => '*',//查找字段
                'order' => '',//排序
            );
            $data = BizService::BizLogIndex($data_params);


            foreach ($data['data'] as &$v) {
                $v['log_service_price'] = priceFormat($v['log_service_price']);

                $v['pay_type'] = BaseService::StatusHtml($v['pay_type'], lang('cash_type'), false);

                $v['service_title'] = $v['log_service_source'];

                $v['score_title'] = Db::name('evaluation_score')->where('orderserver_id=' . $v['order_id'])->value('score');

                $v['score_title'] = BaseService::StatusHtml($v['score_title'], lang('comment_level'), false);
                /*
                $order=Db::name('orders')->where('order_number='.$v['order_number'])->find();
                //dump($order);exit;
                $v['total_price'] =priceFormat($order['pay_price']);

                $v['voucher_price']=priceFormat($order['voucher_price']);

                $service_arr=Db::name('order_server')->alias('os')
                    ->leftJoin(['service'=>'s'],'os.server_id=s.id')
                    ->where([['os.order_number','=',$v['order_number']]])
                    ->column('s.service_title');

                if (count($service_arr)>0)
                {
                    $v['service_title']=join(',',$service_arr);
                }
                */
            }

//        $total= BizService::BizIndexCount();
//        return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];

            return DataReturn('获取成功', '0', $data);
        } else {
            $this->assign('bizId', $bizId);
            return $this->fetch();
        }


    }

    /*
     * @content: 门店收益记录
     * @params : bizid ： 门店id
     * */
    function bizIncomeLog()
    {
        $bizId = input("get.bizId");
        if (input("get.action") == 'ajax') {

            $data = BizService::BizIncomeData(input());

            return DataReturn('获取成功', '0', $data);
        } else {

            $income_type = [2 => '办理会员', 6 => '升级会员', 7 => '会员充值'];


            $this->assign('income_type', $income_type);

            $this->assign('bizId', $bizId);

            return $this->fetch();
        }


    }

    /*
     * @content: 门店收益记录-完成任务
     * @params : bizid ： 门店id
     * */
    function bizTaskLog()
    {
        $bizId = input("get.bizId");
        if (input("get.action") == 'ajax') {

            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            $info = Db::table('cooperative_task_list ctl')
                ->field('title,explain_info,completion_time,reward_time,convert(reward_balance,decimal(10,2)) reward_balance')
                ->join('cooperative_task ct', 'ct.id=ctl.task_id', 'left')
                ->where(array('ctl.status' => 3, 'biz_id' => $bizId))
                ->order(array('ctl.id' => 'desc'))
                ->paginate($listRows, false, ['query' => request()->param()])
                ->toArray();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $info['data'], 'total' => $info['total']]];
        } else {

            $this->assign('bizId', $bizId);

            return $this->fetch();
        }


    }

    /**
     * 门店销售列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2021年1月30日20:49:41
     * @desc    description
     */
    public function BizOrders()
    {
        $param = input();

        $redis = new Redis();


        //$re=$redis->hGet('order_arr','id') ;

        if (input('get.action') == 'ajax') {

            $order_id_arr = $redis->hGet('order_arr', 'id');

            $order_id_arr = explode(',', $order_id_arr);

            $biz_type = $param['biz_type'];

            if ($biz_type == 1) {
                goto orders;
            } else {
                goto biz_settlement;
            }
            /*
            if($param['mark'] ==3 or $param['mark'] == 5){

                $day = 'today';
            }else{
                #昨日
                $day = 'yesterday';

            }
            */
            orders:

            $where = [
                ['id', 'in', $order_id_arr],
                //['pay_type','=',7],
                // ['pay_price','=',0]

            ];

            $data = Db::name('orders')
                ->where($where)
                ->order('order_create desc')
                ->paginate(10, false, ['query' => request()->param()]);

            $data = $data->toArray();

            foreach ($data['data'] as &$v) {


                if (empty($v['car_liences'])) {
                    $v['car_liences'] = Db::name('log_service')->where('order_number=' . $v['order_number'])->find();
                }

                $v['car_liences'] = empty($v['car_liences']) ? '未获取到车牌号' : $v['car_liences'];

                $v['member_name'] = MemberService::SelectMemberName(['id' => $v['member_id']]);

                $v['member_phone'] = Db::name('member')->where('id=' . $v['member_id'])->value('member_phone');

                $v['member_phone'] = empty($v['member_phone']) ? '--' : $v['member_phone'];

                $v['pay_type_title'] = BaseService::StatusHtml($v['pay_type'], [1 => '微信', 2 => '支付宝', 3 => '银联', 4 => '现金', 5 => '余额', 6 => '卡券', 7 => '积分',], false);

                if ($v['pay_type'] == 6) {

                    $v['voucher_price'] = $v['voucher_price'] == 0 ? $v['pay_price'] : $v['voucher_price'];

                    $v['pay_price'] = 0;

                    $v['order_price'] = $v['order_price'] == 0 ? $v['voucher_price'] : $v['order_price'];

                } else {
                    $v['order_price'] = empty($v['order_price']) ? priceFormat($v['voucher_price'] + $v['pay_price']) : $v['order_price'];
                }

                $service_title_arr = Db::name('order_server')->alias('os')
                    ->leftJoin(['service' => 's'], 'os.server_id=s.id')
                    ->where('os.server_id > 0')
                    ->where('os.order_number=' . $v['order_number'])
                    ->field('s.service_title,os.server_id')->select();

                $v['aa'] = $service_title_arr;

                if (count($service_title_arr) > 0) {
                    foreach ($service_title_arr as $vv) {
                        if (strlen($vv['service_title']) > 0) {
                            $v['service_title'] .= $vv['service_title'] . ',';
                        }

                    }

                } else {
                    $v['service_title'] = '未获取服务名称';
                }

                $v['member_id'] = Db::name('member')->where('id=' . $v['member_id'])->value('id');
            }

            goto last;

            biz_settlement:

//            $where = [
//                ['id', 'in', $order_id_arr]
//
//            ];
//            $order_id_arr = Db::name('biz_settlement')
//                ->where($where)
//                ->column('order_number');
//            //dump($order_id_arr);exit;
//            $where = [
//                ['order_number', 'in', $order_id_arr]
//            ];
//            $order_id_arr = Db::name('orders')
//                ->where($where)
//                ->column('id');

            goto orders;

            /*
            foreach ($data['data'] as &$v)
            {

                $v['voucher_price']=$v['coupon_price'];

                $v['pay_type_title']= BaseService::StatusHtml($v['method'],lang('cash_type'),false);

                $v['service_title']='未获取服务名称';

                $v['m']=strpos($v['order_number'],'M');

                if ($v['m']) {
                    $v['member_id']=0;
                }else{
                    $v['member_id']=Db::name('orders')->where('order_number='.$v['order_number'])->value('member_id');

                }

                $v['member_name']='未获取到用户信息';

                $v['member_phone']='未获取到用户信息';

                if ($v['member_id']>0)
                {

                $v['member_name']=MemberService::SelectMemberName(['id'=>$v['member_id']]);

                $v['member_phone']=Db::name('member')->where('id='.$v['member_id'])->value('member_phone');

                }

            }


            goto last;
*/
            last:


            return ['code' => 0, 'msg' => '', 'data' => $data];
        } else {
            $redis->hSet('order_arr', 'id', $param['order_id_arr']);

            return $this->fetch();
        }


    }

    /**
     * 门店订单详情
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2021年2月2日21:34:44
     * @desc    description
     */
    public function BizOrderDetail()
    {
        $param = input();
        //$param['id']=1326;

        $data = Db::name('orders')->where('id = ' . $param['id'])->select();

        foreach ($data as &$v) {

            if (empty($v['car_liences'])) {
                $v['car_liences'] = Db::name('log_service')->where('order_number=' . $v['order_number'])->find();
            }

            $v['car_liences'] = empty($v['car_liences']) ? '未获取到车牌号' : $v['car_liences'];

            $v['member_name'] = $v['order_create'] . MemberService::SelectMemberName(['id' => $v['member_id']]);

            $v['member_phone'] = Db::name('member')->where('id=' . $v['member_id'])->value('member_phone');

            $v['member_phone'] = empty($v['member_phone']) ? '--' : $v['member_phone'];

            $v['pay_type_title'] = BaseService::StatusHtml($v['pay_type'], [1 => '微信', 2 => '支付宝', 3 => '银联', 4 => '现金', 5 => '余额', 6 => '卡券', 7 => '积分',], false);

            if ($v['pay_type'] == 6) {

                $v['voucher_price'] = $v['voucher_price'] == 0 ? $v['pay_price'] : $v['voucher_price'];

                $v['pay_price'] = 0;

                $v['order_price'] = $v['order_price'] == 0 ? $v['voucher_price'] : $v['order_price'];

            } else {
                $v['order_price'] = empty($v['order_price']) ? priceFormat($v['voucher_price'] + $v['pay_price']) : $v['order_price'];
            }

            $service_title_arr = Db::name('order_server')->alias('os')
                ->leftJoin(['service' => 's'], 'os.server_id=s.id')
                ->where('os.server_id > 0')
                ->where('os.order_number=' . $v['order_number'])
                ->field('s.service_title,os.server_id')->select();

            if (count($service_title_arr) > 0) {
                foreach ($service_title_arr as $vv) {
                    if (strlen($vv['service_title']) > 0) {
                        $v['service_title'] .= $vv['service_title'] . ',';
                    }

                }

            } else {
                $v['service_title'] = '未获取服务名称';
            }

            $v['server_title'] = $v['service_title'];

            $v['member_id'] = Db::name('member')->where('id=' . $v['member_id'])->value('id');
        }

        //dump($data[0]);exit;
        /*
        $data['pay_type_title']=BaseService::StatusHtml($data['pay_type'],lang('cash_type'),false);

        $data['server_arr']=Db::name('order_server')->alias('os')
            ->leftJoin(['service'=>'s'],'os.server_id=s.id')
            ->where(['os.order_number'=>$data['order_number']])
            ->column('s.service_title');

        if (count($data['server_arr'])>0) {
            $data['server_title'] =join(',',$data['server_arr']);
        }

        if (empty($data['car_liences']))
        {
            $data['car_liences']=Db::name('log_service')->where('order_number='.$data['order_number'])
                ->find();
        }

        $data['car_liences']=empty( $data['car_liences'])?'未获取到车牌号':$data['car_liences'];

        if ($data['pay_type']==6)
        {

            $data['voucher_price']=$data['voucher_price']==0?$data['pay_price']:$data['voucher_price'];

            $data['pay_price']=0;

            $data['order_price']=$data['order_price']==0?$data['voucher_price']:$data['order_price'];

        }
        else{
            $data['order_price']=priceFormat($data['voucher_price']+$data['pay_price']);
        }

        //$data['order_price']=priceFormat($data['voucher_price']+$data['pay_price']);
        */

        $this->assign('data', $data[0]);
        return $this->fetch();
    }

    /*
       * @content: 门店待结算   结算记录
       * @params : bizid ： 门店id
       * @params : mark ：  1 待结算 2 已结算
       * */
    function bizSettlementLog()
    {
        $bizId = input("get.bizId");
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            $_where = '';

            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true,
                'number' => $listRows,
                'where' => "tobe_settled > 0 and biz_id =$bizId and settlement_status=$mark" . $_where,//查找条件
                'table' => 'biz_settlement',
                'field' => '*',//查找字段
                'order' => 'create_time desc',//排序
            );
            $data = BizService::BizLogIndex($data_params);

            foreach ($data['data'] as &$v) {
                $v['car_liences'] = '--';

                $v['member_name'] = '--';

                $v['member_phone'] = '--';

                $v['member_level_title'] = '--';

                $v['pay_create'] = $v['create_time'];

                $v['order_id'] = 0;

                //结算金额
                $v['price'] = priceFormat($v['price']);

                //收入类型
                $v['type_title'] = BaseService::StatusHtml($v['type'], [1 => '服务收入', 2 => '办理会员', 3 => '会员升级', 4 => '会员充值'], false);

                //支付方式
                $v['pay_type'] = BaseService::StatusHtml($v['method'], lang('cash_type'), false);

                //$v['order_number']:M5506L1UPL2 str_replace(['M','L',"UP"],',',$v['order_number'])
                if (strlen(strpos($v['order_number'], 'M')) > 0) {
                    $v['member_id_arr'] = explode(',', str_replace(['M', 'L', "UP"], ',', $v['order_number']));

                    $v['member_id'] = $v['member_id_arr'][1];
                }

                if (strlen($v['order_number']) >= 11) {
                    //订单信息
                    $order = Db::name('orders')->where(['order_number' => $v['order_number']])->find();

                    if (!empty($order)) {
                        //车牌号
                        $v['car_liences'] = $order['car_liences'];

                        //结账时间
                        $v['pay_create'] = $order['pay_create'];

                        //用户id
                        $v['member_id'] = $order['member_id'];

                        //服务id
                        $v['order_id'] = $order['id'];
                    }
                }
                /**/
                //会员信息
                if ($v['member_id'] > 0) {
                    $member = Db::name('member')->alias('m')
                        ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=m.id')
                        ->where('m.id=' . $v['member_id'])
                        ->field('m.member_phone,mm.member_level_id')
                        ->find();

                    //用户昵称
                    $v['member_name'] = MemberService::SelectMemberName(['id' => $v['member_id']]);

                    //用户电话
                    $v['member_phone'] = $member['member_phone'];

                    //会员级别
                    if ($member['member_level_id'] > 0) {
                        $v['member_level_title'] = BaseService::StatusHtml($member['member_level_id'], lang('member_level'), false);

                    } else {
                        $v['member_level_title'] = '普通用户';
                    }
                }

            }


//        $total= BizService::BizIndexCount();
//        return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];

            return DataReturn('获取成功', '0', $data);
        } else {
            $this->assign('bizId', $bizId);
            $this->assign('mark', $mark);

            return $this->fetch();
        }


    }

    /*
    * @content: 门店待结算   结算记录
    * @params : bizid ： 门店id
    * @params : mark ：  1 待结算 2 已结算
    * */
    function bizDepositLog()
    {
        $bizId = input("get.bizId");
        if (input("get.action") == 'ajax') {

            $info = input("post.searchParams");

            /*
            if(!empty(strim($info['keywords']))){
                $_where = " and (username  like '%".strim($info['keywords'])."%' or  phone  like '%".strim($info['keywords'])."%')";

            }
            if(!empty($info['start_time'])){
                $start_time =date_create($info['start_time']);

                $start_time =date_format($start_time,"Y-m-d H:i:s");
                $_where .=" and create_time >='".$start_time."'";
            }

            if(!empty($info['end_time'])){
                $end_time =date_create($info['start_time']);
                $end_time =date_format($end_time,"Y-m-d H:i:s");

                $_where .=" and create_time <='".$end_time."'";
            }
            */
            $_where = [];

            if (!empty($info['keywords'])) {
                $_where[] = ['username|phone', 'like', '%' . $info['keywords'] . '%'];
            }

            if (!empty($info['start_time'])) {
                $_where[] = ['create_time', '>=', $info['start_time']];
            }

            if (!empty($info['end_time'])) {
                $_where[] = ['create_time', '<=', $info['end_time']];
            }

            $_where[] = ['biz_id', '=', $bizId];

            $_where[] = ['source_type', '=', 1];

            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
                'table' => 'log_deposit',//查找表名
                'field' => '*',//查找字段
                'order' => 'create_time desc',//排序
            );
            $data = BizService::BizLogIndex($data_params);
            //提现姓名 支付宝账号 提现发起时间 提现方式（默认支付宝）提现金额 提现状态

            foreach ($data['data'] as &$v) {
                //提现方式
                $v['deposit_type_title'] = BaseService::StatusHtml($v['deposit_type'], [1 => '支付宝', 2 => '微信'], false);

                //提现状态
                $v['deposit_status_title'] = BaseService::StatusHtml($v['deposit_status'], [1 => '等待审核', 2 => '等待提现', 3 => '成功', 4 => '失败', 5 => '审核驳回'], false);
            }

            return DataReturn('获取成功', '0', $data);
        } else {
            $this->assign('bizId', $bizId);

            return $this->fetch();
        }


    }

    /*
   * @content: 门店员工
   * @params : bizid ： 门店id
   * */
    function bizEmployee()
    {
        $bizId = input("get.bizId");
        if (input("get.action") == 'ajax') {
            $_where = '';
            $info = input("post.searchParams");


            if (!empty(strim($info['keywords']))) {
                $_where .= " and (employee_name  like '%" . strim($info['keywords']) . "%' or  employee_phone  like '%" . strim($info['keywords']) . "%')";

            }

            if ($info['search_sex'] != '') {

                $_where .= " and employee_sex =" . $info['search_sex'] . "";
            }

            if ($info['search_status'] != '') {
                $_where .= " and employee_status =" . $info['search_status'] . "";
            }


            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => "biz_id=$bizId" . $_where,//查找条件
                'table' => 'employee',//查找表名
                'field' => '*',//查找字段
                'order' => 'employee_create desc',//排序
            );
            $data = BizService::BizLogIndex($data_params);
//        $data=$data->toArray()['data'];


//        $total= BizService::BizIndexCount();
//        return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];

            return DataReturn('获取成功', '0', $data);
        } else {
            $this->assign('bizId', $bizId);

            return $this->fetch();
        }


    }

    /*
  * @content: 门店员工历史薪资
  * @params : id ： 员工id
  * */
    function bizEmployeeWages()
    {
        $id = input("get.id");
        if (input("get.action") == 'ajax') {
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => "employee_id=$id",//查找条件
                'table' => 'salary',//查找表名
                'field' => '*',//查找字段
                'order' => 'id desc',//排序
            );
            $data = BizService::BizLogIndex($data_params);

            foreach ($data['data'] as &$v) {
                //提成工资
                $v['commission_salary'] = priceFormat($v['commission_salary']);

                //发放时间
                $v['salary_time'] = date('Y-m', strtotime($v['salary_time']));
            }


//        $total= BizService::BizIndexCount();
//        return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];

            return DataReturn('获取成功', '0', $data);
        } else {
            $this->assign('id', $id);

            return $this->fetch();
        }


    }

    /*
   * @content: 门店综合评论
   * @params : bizid ： 门店id
   * */
    function bizEvaluationScore()
    {
        $bizId = input("bizId");

        $time = date('Y-m', strtotime(date('Y-m-d', time()) . '-1 month'));

        if (input("get.action") == 'ajax') {
            $where = [];

            $info = input();

            $info['searchParams']['year'] = empty($info['searchParams']['year']) ? explode('-', $time)[0] : $info['searchParams']['year'];

            $info['searchParams']['month'] = empty($info['searchParams']['month']) ? explode('-', $time)[1] : $info['searchParams']['month'];

            $re = StatisticalService::HandleTime($info['searchParams']);

            $where[] = ['create_time', '>', $re['data']['time_start']];

            $where[] = ['create_time', '<', $re['data']['time_end']];

            $where[] = ['biz_id', '=', $bizId];


//            if(!empty(strim($info['keywords'])))
//            {
//                $_where[]=['username|phone','like','"'.$info['keywords'].'"'];
//             }

            /*
            if(!empty($info['search_time']))
            {
                $search_time =date_create($info['search_time']);

                $search_time =date_format($search_time,"Y-m");

                $_where .=" and date_format(create_time,'%Y-%m') ='$search_time'";
            }else{

                $last_month_first_date = date('Y-m-1',strtotime('last month'));
                $search_time =date_create($last_month_first_date);

                $search_time =date_format($search_time,"Y-m");
                $_where .=" and date_format(create_time,'%Y-%m') ='$search_time'";

            }
            */


            # 定义listRows 分页
            $listRows = input('post.limit');

            if (empty($listRows)) {
                $listRows = 10;
            }
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $where,//查找条件
                'table' => 'evaluation_score',//查找表名
                'field' => '*',//查找字段
                'order' => 'create_time desc',//排序
            );
            $data = BizService::BizLogIndex($data_params);


            foreach ($data['data'] as &$v) {
                //服务名称
                $v['service_title'] = Db::name('evaluation_score')->alias('es')
                    ->leftJoin(['order_server' => 'os'], 'es.orderserver_id=os.id')
                    ->leftJoin(['service' => 's'], 'os.server_id=s.id')
                    ->where('os.server_id > 0 and es.id=' . $v['id'])
                    ->value('s.service_title');

                $v['service_title'] = empty($v['service_title']) ? '--' : $v['service_title'];

                //评论等级
                $v['evalua_level_title'] = BaseService::StatusHtml($v['evalua_level'], lang('comment_level'), false);

                //用户信息
                $v['member_id'] = Db::name('evaluation_score')->alias('es')
                    ->leftJoin(['orders' => 'o'], 'es.order_id=o.id')
                    ->where('es.id=' . $v['id'])
                    ->value('o.member_id');

                $v['member_name'] = MemberService::SelectMemberName(['id' => $v['member_id']]);
            }

            $data['total_num'] = ['bizScore' => 0, 'bizTotalNum' => 0, 'bizGoodNum' => 0, 'bizNegativeNum' => 0, 'bizMediumNum' => 0];
            //总得分
            $data['total_num']['bizScore'] = Db::table('evaluation_score')->where($where)->sum('score');

            //总评论次数
            $data['total_num']['bizTotalNum'] = Db::table('evaluation_score')->where($where)->count();

            //中评次数
            $data['total_num']['bizMediumNum'] = Db::table('evaluation_score')->where($where)->where([['evalua_level', 'in', [3, 4]]])->count();

            //好评次数
            $data['total_num']['bizGoodNum'] = Db::table('evaluation_score')->where([['evalua_level', '=', 5]])->where($where)->count();

            //差评次数
            $data['total_num']['bizNegativeNum'] = Db::table('evaluation_score')->where([['evalua_level', 'in', [1, 2]]])->where($where)->count();

            return DataReturn('获取成功', '0', $data);
        } else {
            /*
            #总评论得分
            $bizScore = Db::table('evaluation_score')
                ->where(array('biz_id'=>$bizId))->sum('score');
            $this->assign('bizScore',$bizScore);
            #总评论次数
            $bizTotalNum = Db::table('evaluation_score')->where(array('biz_id'=>$bizId))->count();
            #中评次数 3 4
            $bizMediumNum = Db::table('evaluation_score')->where("biz_id= $bizId and (evalua_level =3 or evalua_level=4)")->count();
            #好评次数 5
            $bizGoodNum = Db::table('evaluation_score')->where(array('biz_id'=>$bizId,'evalua_level'=>5))->count();
            #差评次数<3
            $bizNegativeNum = Db::table('evaluation_score')->where("biz_id= $bizId and evalua_level <3")->count();



            $this->assign('bizTotalNum',$bizTotalNum);
            $this->assign('bizMediumNum',$bizMediumNum);
            $this->assign('bizGoodNum',$bizGoodNum);
            $this->assign('bizNegativeNum',$bizNegativeNum);
            */
            $this->assign('year', explode('-', $time)[0]);

            $this->assign('month', explode('-', $time)[1]);

            $this->assign('bizId', $bizId);
            $last_month_first_date = date('Y-m-1', strtotime('last month'));

            $this->assign('last_month', $last_month_first_date);


            return $this->fetch();
        }


    }

    /***************************** 门店 详情所有记录 end ****************************************************************/

}
