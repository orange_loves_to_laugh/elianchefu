<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/24
 * Time: 10:37
 */

namespace app\admin\controller;


use think\Controller;
use think\Db;

class ProPublic extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日13:45:48
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
        // 登录校验
        $this->IsLogin();
    }

    function searchBiz()
    {
        $searchInfo=input("post.searchInfo");
        $list=Db::table("biz")->field("id,biz_title,biz_address,biz_phone")->where("biz_status=1 and biz_title like '%".$searchInfo."%'")->select();
        if(!empty($list)){
            return array("status"=>true,"data"=>$list);
        }else{
            return array("status"=>false,"msg"=>"未查询到符合条件的门店");
        }
    }

    function searchPro()
    {
        $searchPro=input("post.searchPro");
        $list=Db::table("biz_pro bp")
            ->field("bp.id,bp.biz_pro_title,pc.class_title,bp.biz_pro_paret,(case bp.biz_pro_paret when 1 then '商品' when 2 then '配件' when 3 then '耗材' end ) type_title,
            bp.biz_pro_purch")
            ->join("pro_class pc","pc.id = bp.biz_pro_class_id","left")
            ->where(array("bp.biz_pro_status"=>1,"bp.biz_pro_type"=>0))->where("bp.biz_pro_title like '%".$searchPro."%'")->select();
        if(!empty($list)){
            return array("status"=>true,"data"=>$list);
        }else{
            return array("status"=>false,"msg"=>"未查询到符合条件的商品");
        }
    }
}