<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/17
 * Time: 14:36
 */

namespace app\admin\controller;


use app\service\BaseService;
use app\service\MemberService;
use app\service\StatisticalService;
use base\Excel;
use Redis\Redis;

use think\Controller;
use think\Db;
use think\Model;


class Demo extends Controller
{

    function modifyBiz(){
        $re = Db::table('biz')
            ->where("id != 692 and id != 408 and area = '鲅鱼圈区'")
            ->update(array('create_pro'=>2,'open_custom'=>2));
        dump($re);
        $res = Db::table('service')
            ->where("biz_pid != 692 and biz_pid != 408 and biz_pid > 0 ")
            ->update(array('service_status'=>1));
        $resa = Db::table('service')
            ->where("biz_pid != 692 and biz_pid != 408 and biz_pid > 0 and biz_pid in (select id from biz where area = '鲅鱼圈区')")
            ->update(array('service_status'=>0));
        dump($resa);
    }

    function mergeVideo()
    {
         # 设置超时不过期
        set_time_limit(0);
        $file_a = 'E:\XiaoZhi\Pictures\video\a.mp4';
        $file_b = 'E:\XiaoZhi\Pictures\video\b.mp4';
        $file_new = 'E:\XiaoZhi\Pictures\video\new.mp4';
        $out = fopen($file_new,'wb');
        if(!$out){
            dump($out);
            return;
        }
        flock($out,LOCK_EX);
        $file_a_f = fopen($file_a,'rb');
        if(!$file_a_f){
            dump($file_a_f);
            return;
        }
        while ($buff_a = fread($file_a_f,94096)){
            fwrite($out,$buff_a);
        }
        fclose($file_a_f);
        $file_b_f = fopen($file_b,'rb');
        if(!$file_b_f){
            dump($file_b_f);
            return;
        }
        while ($buff_b = fread($file_b_f,94096)){
            fwrite($out,$buff_b);
        }
        fclose($file_b_f);
        flock($out,LOCK_UN);
        fclose($out);
        dump($out);
    }

    function rname()
    {
        $arr = glob("E:\Administrator\Desktop\img/*.jpg");
        foreach ($arr as $k => $v) {
            $all_name = pathinfo($v, PATHINFO_BASENAME);
            $name = pathinfo($v, PATHINFO_FILENAME);
            dump($v);
            if (strpos($all_name, '.jpg.jpg') !== false) {
                $name = str_replace('.jpg', '', $name);
                dump($all_name);
                dump($name);
//                rename($v,$name);
            }
        }
    }
    function getSessionInfo(){
        $info = Session('TaskPrizeInfo');
        dump($info);
    }

    function demo()
    {
        $redis = new Redis();
        $info = $redis->hGetJson('BizCreation', strval(32));
        dump($info);
        die;
        $_Session = Session('serviceInfoSession');
        dump($_Session);
        return;
        $time = date('Y-m', strtotime(date('Y-m-d', time()) . '-1 month'));


        $params['param']['year'] = empty($params['param']['year']) ? explode('-', $time)[0] : $params['param']['year'];

        $params['param']['month'] = empty($params['param']['month']) ? explode('-', $time)[1] : $params['param']['month'];

        $re = StatisticalService::HandleTime($params['param']);


        //$where[] = ['create_time', '>', $re['data']['time_start']];
        // $where[] = ['create_time', '<', $re['data']['time_end']];
        $re6 = Db::table('biz_settlement')
            ->where([['tobe_settled', '<', 0]])
            ->select();
        dump($re6);
        exit;

        $re6 = Db::table('orders')
            ->where([['car_liences', 'like', '%8662']])
            ->whereTime('order_create', 'today')
            ->column('order_number');

        $re7 = Db::table('order_server')
            ->where([['order_number', 'in', $re6],])
            ->select();


        $re8 = Db::table('orders')
            ->where([['order_number', '=', '1614237826303100036']])->find();


        $re3 = Db::table('order_server')
            ->where([['server_id', '=', 9], ['status', '=', 2]])
            ->where('length(order_number)>9')
            ->order('id desc')
            ->column('order_number');


        $re4 = Db::table('orders')
            ->where([['order_number', 'in', $re3]])
            ->whereTime('order_create', 'today')
            ->column('order_number');

        $re5 = Db::table('order_server')
            ->where([['server_id', '=', 9], ['status', '=', 2], ['order_number', 'in', $re4]])
            ->column('order_number');
        $re6 = Db::table('orders')
            ->where([['order_number', 'in', $re5]])
            ->select();
        dump($re6);
        exit;


        $where_vip = [
            ['m.id', 'in', $re2]
        ];
        $data_vip = Db::name('member_mapping')->alias('mm')
            ->leftJoin(['member' => 'm'], 'mm.member_id=m.id')
            ->field('m.register_time,m.member_phone,m.id member_id,mm.member_expiration,mm.member_level_id')
            ->order('m.id desc')
            ->where($where_vip)
            ->select();

        $vipCount_arr = [];

        foreach ($data_vip as $k => $v) {
            $data_vip[$k]['member_name'] = MemberService::SelectMemberName(['id' => $v['member_id']]);

            if ($v['member_level_id'] > 2) {
                continue;
            }

            if ($v['member_level_id'] < 1) {
                $vipCount_arr[$k] = $data_vip[$k];

                continue;
            }

            if ($v['member_level_id'] == 1 || $v['member_level_id'] == 2) {
                if (strtotime($v['member_expiration']) < time()) {
                    //dump($v['member_expiration']);
                    $vipCount_arr[$k] = $data_vip[$k];
                    continue;
                }
            }
            //$vipCount_arr[$k]= $v;
        }


        $excel_params = [
            'filename' => '用户数据',
            'title' => [

                ['name' => '用户姓名', 'field' => 'member_name'],
                ['name' => '用户手机号', 'field' => 'member_phone'],
                //['name'=>'会员等级','field'=>'member_level_title'],
                //['name'=>'用户余额','field'=>'member_balance'],
                ['name' => '注册时间', 'field' => 'register_time'],
            ],
            'data' => $vipCount_arr
        ];
        $excel = new Excel($excel_params);
        return $excel->Export();

        $re = Db::table("deduct")->where('biz_id = 166 and  type=5')->select();


        //$re2= Db::query('select * from ecars.admin');


    }

    function partnerBalance()
    {
        $memberId = input('get.member_id');
        $res = Db::table('member_partner')->where(array('member_id' => $memberId))->setInc('partner_balance', 6000);
        dump($res);
    }

    function demos()
    {
        $redis = new Redis();
        $mobile = input('get.phone');
        var_dump($redis->hGet('smsCode', strval($mobile)));
    }

    public function LocationInfo()
    {

        $param = input();
//        $ak = 'tpandveqFTpYCqKKWu6mS5ITaU3KUm4B';
        $longitude = 113.327800;
        $latitude = 23.137202;
        $param['member_id'] = 8084;
        $result = getAddressComponent($longitude, $latitude, 1);

        $area = $result['district'];
        $data = [
            'member_id' => $param['member_id'],
            'address' => $result["result"]["formatted_address"],
            "province" => $result["result"]["addressComponent"]["province"],
            "city" => $result["result"]["addressComponent"]["city"],
            "area" => $result["result"]["addressComponent"]["district"],
            'create_time' => TIMESTAMP

        ];

        Db::name('member_trip')->insertGetId($data);

    }

    function servicePrice()
    {
        return;
        ignore_user_abort(true);     // 忽略客户端断开
        set_time_limit(0);           // 设置执行不超时
        $info = Db::table('service_car_mediscount')
            ->where(array('biz_id' => 0))
            ->where('id > 10000 and id <= 20000')
            ->select();
        $i = 10000;
        foreach ($info as $k => $v) {
            $i++;
            Db::table('service_car_mediscount')->where(array(
                'service_id' => $v['service_id'],
                'car_level_id' => $v['car_level_id'],
                'service_type' => $v['service_type'],
            ))
                ->where('biz_id > 0')
                ->update(array('discount' => $v['discount']));
            dump($i);
        }
        dump('结束');
    }

    function serviceInfo()
    {
        $info = Db::table('service s')
            ->field('s.id,service_title,(select discount from service_car_mediscount where service_id=s.id and biz_id=0 limit 1) price,(select count(*) from service_car_mediscount where service_id=s.id and biz_id=1) num')
            ->having('num = 0')
            ->where(array('service_status' => 1, 'is_del' => 2))
            ->select();
        dump($info);
    }

    function redisDemo()
    {
        $redis = new Redis();
        for ($i = 1; $i <= 10; $i++) {
            $redis->lPush("aaa", $i); // 向头部添加
        }

    }

    function redisDemo2()
    {
        $redis = new Redis();
        $list = $redis->lRanges("aaa", 0, -1); // 从头到尾取全部
        dump($list);
    }

    function redisDemo3()
    {
        $redis = new Redis();
        $redis->lRem("aaa", 2, 1);
    }

    function bizSettlement()
    {
        $info = Db::query("select id,order_number,server_id,coupon_mark,
       if(custommark=1,(select service_title from service where id =os.server_id),
                (select custom_server_title from customserver where id=os.server_id)) serviceTitle,
       status,price,custommark,order_starttime,original_price,
(select biz_id from orders where order_number = os.order_number) biz_id,
if(custommark=1,(select biz_pid from service where id =os.server_id),0) biz_pid,
(select score from evaluation_score where orderserver_id = os.id limit 1) score,       
if(custommark=1,(select relation_eva from service where id =os.server_id),2) relation_eva,price_settlement,final_settlement       
 from order_server os where (select count(id) from order_server ob where os.order_number = ob.order_number) > 1 
 and order_number != 'undefined' and (select biz_id from orders where order_number = os.order_number)  != 408 and final_settlement > 0
and date(order_starttime) >= '2021-10-13'
 order by order_number desc");
        $settlement_medium = config('ecarcoo.comment_rate_settlement')['medium'];
        $bizArr = array();
        $res = array();
        foreach ($info as $k => $v) {
            # 获取门店信息
            $bizInfo = Db::table("biz")->where(array("id" => $v['biz_id']))->find();;
            $orderInfo = Db::table('orders')->where(array('order_number' => $v['order_number']))->find();
            $payType = $orderInfo['pay_type'];
            $orderNumber = $v['order_number'];
            $settlement_service_info = 0;
            if ($v['custommark'] == 1) {
                # 判断是否使用了卡券支付(判断卡券的结算金额)
                $memberVoucherInfo = Db::table('member_voucher')->field('id,settlement_price,voucher_cost')
                    ->where("id = (select voucher_id from log_cashcoupon where order_number = '" . $orderNumber . "' and workorder_id = " . $v['id'] . " and order_type = 2 limit 1)")
                    ->find();
                if (!empty($memberVoucherInfo)) {
                    $settlement_service_info = $memberVoucherInfo['settlement_price'];
                    if (!empty($settlement_service_info)) {
                        $settlement_service_info = json_decode($settlement_service_info, true);
                        $settlement_service_info = $settlement_service_info[$orderInfo['car_level']];
                        $info[$k]['settlement_type'] = 2;
                    }
                }
                if (empty($settlement_service_info)) {
                    # 系统服务
                    $settlement_service_info = Db::table('service_car_mediscount')->field('settlement_amount')
                        ->where(array('biz_id' => $orderInfo['biz_id'], 'service_id' => $v['server_id'], 'service_type' => 1, 'car_level_id' => $orderInfo['car_level']))
                        ->value('settlement_amount');
                    if (empty($settlement_service_info)) {
                        if ($v['biz_pid'] != 0) {
                            $settlement_service_info = $v['price'];
                        }
                    }
                }
            } else {
                # 自定义服务
                $settlement_service_info = $v['price'];
            }
            # 该服务使用卡券核销掉了,按门店服务设置的结算价格
            if ($v['coupon_mark'] == 2) {
                settleService:
                $should_settle = $settlement_service_info;
                # 抽成金额
                if ($v['custommark'] == 1) {
                    # 系统服务
                    $withdrawal_amount = $should_settle * $bizInfo['extract_platservice'];
                } else {
                    $withdrawal_amount = $should_settle * $bizInfo['extract_selfservice'];
                }
                # 实际结算价格
                $settlement_service = $should_settle - $withdrawal_amount;
                # 记录结算,计算提成时要使用的价格
                $info[$k]['price_settlement_real'] = $settlement_service_info;
            } else {
                if ($payType == 5) {
                    # 余额支付,
                    goto settleService;
                } elseif ($payType == 1 or $payType == 2) {
                    # 微信支付/支付宝支付
                    if ($bizInfo['settlement_method'] == 2) {
                        # 按订单价格结算
                        $settlement_service_info = $v['original_price'];
                    }
                    $should_settle = $settlement_service_info;
                    # 抽成金额
                    if ($v['custommark'] == 1) {
                        $withdrawal_amount = $should_settle * $bizInfo['extract_platservice'];
                    } else {
                        # 自定义服务
                        $withdrawal_amount = $should_settle * $bizInfo['extract_selfservice'];
                    }
                    # 实际结算价格
                    $settlement_service = $should_settle - $withdrawal_amount;
                    # 记录结算,计算提成时要使用的价格  $v['original_price']
                    $info[$k]['price_settlement_real'] = $settlement_service_info;
                }
            }
            if ($v['custommark'] == 1) {
                # 系统服务,最终结算价格
                if ($v['relation_eva'] == 1) {
                    # 该服务 关联用户评价
                    if ($v['score'] == 5) {
                        $settlement_medium = config('ecarcoo.comment_rate_settlement')['good'];
                    }
                    if ($v['score'] < 3) {
                        $settlement_medium = config('ecarcoo.comment_rate_settlement')['bad'];
                    }
                    $final_settlement = $settlement_service * $settlement_medium;
                } else {
                    # 不关联
                    $final_settlement = $settlement_service;
                }
            } else {
                # 自定义服务
                $final_settlement = $settlement_service;
            }
            # 修改最终结算价格
            $info[$k]['final_settlement_real'] = $final_settlement;
            $epsilon = 0.01;
            $title = $bizInfo['biz_title'];
            if (abs(floatval($v['final_settlement']) - floatval($final_settlement)) < $epsilon) {
                # 相等
                unset($info[$k]);
            } else {
                # 不相等
                $diff = floatval($v['final_settlement']) - floatval($final_settlement);
                $info[$k]['final_diff'] = $diff;
                if (array_key_exists($v['biz_id'], $bizArr)) {
                    $bizArr[$v['biz_id']]['diff'] += $diff;
                } else {
                    $bizArr[$v['biz_id']]['diff'] = $diff;
                    $bizArr[$v['biz_id']]['biz_id'] = $v['biz_id'];
                    $bizArr[$v['biz_id']]['title'] = $title;
                }
                $info[$k]['desposit_status'] = Db::table('biz_settlement')->where(array('order_number' => $v['order_number']))->value('desposit_status');
                $info[$k]['settlement_status'] = Db::table('biz_settlement')->where(array('order_number' => $v['order_number']))->value('settlement_status');
                if (array_key_exists($title, $res)) {
                    array_push($res[$title], $info[$k]);
                } else {
                    $res[$title][] = $info[$k];
                }
            }
        }
        dump($res);
        dump($bizArr);
        $et = [
            ['name' => '订单编号', 'field' => 'order_number'],
            ['name' => '服务名称', 'field' => 'serviceTitle'],
            ['name' => '服务价格', 'field' => 'price'],
            ['name' => '服务时间', 'field' => 'order_starttime'],
            ['name' => '评分', 'field' => 'score'],
            ['name' => '当前应结算价格', 'field' => 'price_settlement'],
            ['name' => '当前最终结算价格', 'field' => 'final_settlement'],
            ['name' => '实际应结算价格', 'field' => 'price_settlement_real'],
            ['name' => '实际最终结算价格', 'field' => 'final_settlement_real'],
            ['name' => '差值', 'field' => 'final_diff']
        ];
        $excel_params = [
            'filename' => '铭赫汽车服务中心',
            'title' => $et,
            'data' => $res['铭赫汽车服务中心']
        ];
        $excel_params1 = [
            'filename' => 'E联车服红福店',
            'title' => $et,
            'data' => $res['E联车服红福店']
        ];
        $excel_params2 = [
            'filename' => '八方汽车美容中心',
            'title' => $et,
            'data' => $res['八方汽车美容中心']
        ];
        $excel_params3 = [
            'filename' => '万达七号车馆',
            'title' => $et,
            'data' => $res['万达七号车馆']
        ];
        $excel = new Excel($excel_params);
        $excel1 = new Excel($excel_params1);
        $excel2 = new Excel($excel_params2);
        $excel3 = new Excel($excel_params3);
//        $excel->Export();
//        $excel1->Export();
//        $excel2->Export();
        $excel3->Export();
    }
}
