<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/29
 * Time: 9:47
 */

namespace app\admin\controller;


use app\reuse\controller\ResponseJson;
use app\service\ApplayService;
use app\service\BaseService;
use app\service\MerchantService;
use app\service\ResourceService;
use think\Db;

class Applayy extends Common
{
    use ResponseJson;

    /**
     * 构造方法
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [banner 首页banner]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function banner()
    {
        #banner 标识 首页banner  会员页 cardbanner 二手车 carbanner
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            $_where = array("application_mark" => $mark);
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => 10, //分页个数
                'where' => $_where,//查找条件
            );

            $data = ApplayService::ApplayIndex($data_params);
            return $this->jsonSuccessData($data);


        } else {
            $this->assign('mark', $mark);
            return $this->fetch();
        }

    }

    /**
     * [banner 首页banner添加 修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function savebanner()
    {
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空

                $params['mark']=$mark;
                $data = ApplayService::AppInsertWrite($params, $mark,'savebanner');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = Db::table("application")->where(array('application_mark' => $mark))->find();
//            $id = input("get.id");
            $info=[];
            if (!empty($id)) {
                $info = Db::table("application")->where(array('id' => $id))->find();
                #执行修改
                $_bannerurl = explode(",", $info["application_image"]);
                $this->assign("bannerArr", $_bannerurl);

                #用来删除图片的 是个数组
                $info["bannerurl"] = implode(',', array_map("change_to_quotes", $_bannerurl));

                $this->assign('bannerurl', $info["bannerurl"]);
                $this->assign('id', $id);
                $info['jump_type_title']='';
                $info['jump_type_detail_title']='';
                if ($info['is_jump']==1)
                {
                    $info['jump_type_title']=ApplayService::JumpTypeTitle($info)['type'];
                    $info['jump_type_detail_title']=ApplayService::JumpTypeTitle($info)['name'];
                }
            }

            $this->assign('info', $info);
            #banner 添加页
            $this->assign('mark', $mark);
            $this->assign('application_banner_type', lang('application_banner_type'));
            $this->assign('image_jump_type', lang('image_jump_type'));

            return $this->fetch();
        }
    }

    /**
     * [Delapplay 删除]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function Delapplay()
    {
        $id = input("post.id");
        if (!empty($id)) {
            Db::table("application")->where(array('id' => $id))->delete();
            return json(DataReturn('删除成功', 0));

        }
    }

    /**
     * [saveprotocol 协议 关于我们   声明管理  添加  修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function saveprotocol()
    {
        $mark = input("get.mark");
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空
                    $data = ApplayService::AppInsertWrite($params, $mark,'saveprotocol');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = Db::table("application")->where(array('application_mark' => $mark))->find();
            if (!empty($id)) {
                $info = Db::table("application")->where(array('id' => $id))->find();

                $this->assign('info', $info);
                $this->assign('id', $id);
            }

            #banner 添加页
            $this->assign('mark', $mark);
            return $this->fetch();
        }

    }
    /**
     * [baseadvert  招募底图管理   广告管理  添加  修改 展示]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月29日13:23:09
     */
    function baseadvert()
    {

        $mark = input("get.mark");

        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));

            if (!empty($params)) {

                #=接收参数不为空
                $params['mark']=$mark;

                $data = ApplayService::AppInsertWrite($params, $mark,'baseadvert');
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = Db::table("application")->where(array('application_mark' => $mark))->value('id');
            $info=[];
            if (!empty($id))
            {
                $info = Db::table("application")->where(array('id' => $id))->find();
                $info['jump_type_title']='';
                $info['jump_type_detail_title']='';
                if ($info['is_jump']==1)
                {
                    $info['jump_type_title']=ApplayService::JumpTypeTitle($info)['type'];
                    $info['jump_type_detail_title']=ApplayService::JumpTypeTitle($info)['name'];
                }

            }
            //多余图片处理
            $params['id']=empty($id['id'])?'':$id['id'];
            ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
            ResourceService::delUploadCache();

            $this->assign('info', $info);
            $image_type=BaseService::StatusHtml($mark,lang('application_banner_type'),false);
            $this->assign('image_jump_type', lang('image_jump_type'));
            $this->assign('image_type', $image_type);
            return $this->fetch();
        }

    }


    /**
     * [pop_index  首页弹窗管理  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function pop_index(){
        if (input("get.action") == 'ajax') {
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => 10, //分页个数
                'where' => "is_del = 2",//查找条件
            );

            $data = ApplayService::popIndex($data_params);
            return $this->jsonSuccessData($data);


        } else {
            return $this->fetch();
        }

    }
    /**
     * [pop_save  首页弹窗管理  添加 修改  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function pop_save(){
        if (input("get.action") == 'ajax') {
            #banner 执行添加
            $params = array_filter(input("post."));
            if (!empty($params)) {

                #=接收参数不为空
                $data = ApplayService::popInsertWrite($params,$params['id']);
                return $data;

            } else {
                return array('status' => false);
            }

        } else {
            $id = input("get.id");
            if (!empty($id)) {
                $info = Db::table("apop")->where(array('id' => $id))->find();

                $this->assign('info', $info);
                $this->assign('id', $id);
            }
            return $this->fetch();
        }
    }
    /**
     * [pop_save  首页弹窗管理  删除  ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function popdel(){
        $id = input("post.id");
        if(!empty($id)){
            Db::table('apop')->where(array('id'=>$id))->update(array('is_del'=>1));
            return array('status' => true);
        }

    }
    /********************积分设置**********************/
    /**
     * [pop_save  积分设置   ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function integral_set(){
        if (input("get.action") == 'ajax') {
            #查询积分
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }

            // 获取列表
            $data_params = array(
                'page'         => true, //是否分页
                'number'         => $listRows, //分页个数
//                'where'     => $_where,//查找条件
            );
            $data = ApplayService::IntegralIndex($data_params);

            $total=BaseService::DataTotal('integral_set',[['id','>',0]]);

            return ['code' => 0, 'msg' => '','data' => $data,'total'=>$total];

        }else{

            return $this->fetch();

        }

    }
    /**
     * [IntegralWrite  积分修改   ]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年10月12日13:23:09
     */
    function IntegralWrite(){
        $id = input("get.id");

        if (input("get.action") == 'ajax') {
            $params = input("post.");
           // dump($params);exit;
            if (isset($params['thumb_img']))
            {
                unset($params['thumb_img']);
                // dump($params);exit;
                ResourceService::$session_suffix='source_upload'.'integral_set'.$id;
                ResourceService::delCacheItem($params['share_thumb']);
            }


            Db::table('integral_set')->where(array('id'=>$id))->update($params);
            return array('status'=>true);

        }else{
            $info = Db::table('integral_set')->where(array('id'=>$id))->find();
            if(!empty($info['integral_type']))
            {
                $info['integral_type'] = getIntegralTitle( $info['integral_type']);
            }
            //多余图片处理
            ResourceService::$session_suffix='source_upload'.'integral_set'.$id;
            ResourceService::delUploadCache();
            $this->assign('app_client',lang('app_client'));
            $this->assign('info',$info);
            $this->assign('id',$id);
            return $this->fetch();
        }

    }

    /**
     * [SearchData 根据关键字筛选相应数据]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function SearchData()
    {
        return ApplayService::SearchByCateAndKeywords(input());
    }

}