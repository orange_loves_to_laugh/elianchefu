<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 9:26
 */

namespace app\admin\controller;

use app\service\AdminService;
use think\Db;

/**
 * 服务分类管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月23日09:27:10
 */
class JzServiceCategory extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();

        // 登录校验
        $this->IsLogin();


    }

    /**
     * [Index 分类列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function Index()
    {
        $_where="is_del = 2";
        $search=input("get.search");

        $this->assign('search',$search);
        if(!empty($search)){
            $_where.=" and service_class_title like '%".$search."%'";
        }


        // 获取数据列表
        $data_params = [

            'page'              =>true,
            'number'            =>10,
            'service_biz_num'   =>true,
            //'field'            =>'service_title'
            'where'=>$_where
        ];
        $ret=AdminService::ServiceCategory($data_params);

        $this->assign('page_html', $ret['data']['page_html']);
        unset($ret['data']['page_html']);
        $this->assign('data_list', $ret['data']);

        return $this->fetch();
    }



    /**
     * [SaveInfo 分类添加/编辑页面]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function SaveInfo()
    {

        // 参数
        $params = input();

        // 分类信息
        $data = [];
        if(!empty($params['id']))
        {
            $_where ="id='".$params['id']."'";


            $data_params = [
                'where'				=> $_where,
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'number'	         => 0,
                'is_category'		=> 1,
            ];
            $ret = AdminService::ServiceCategory($data_params);

            if(empty($ret['data'][0]))
            {
                return $this->error('分类信息不存在', url('admin/JzServiceCategory/index'));
            }
            $data = $ret['data'][0];



            $this->assign('data', $data);
        }
        $data_cate = [
            'where'				=> [['is_del','=',2],['service_class_pid',"=",0]],
            'page'			  => false,

        ];

        $categorys = AdminService::ServiceCategory($data_cate);

        $this->assign('categorys', $categorys['data']);
        return $this->fetch();
    }

    /**
     * [Save 分类添加/编辑]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function Save()
    {
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        // 开始操作
        $params = input('post.');

       return AdminService::ServiceCategorySave($params);


    }

    /**
     * [删除分类]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:57:30
     */
    public function DelCate(){
        // 是否ajax
        if(!IS_AJAX)
        {
            return $this->error('非法访问');
        }
        //判断是否有所属服务
        $id=input('post.id');
        $where_service=[
            ['service_class_id','=',$id],
            ['is_del','=',2],

        ];
        $services=Db::name('service')->where($where_service)->count();
        if($services>0)
        {
            return json(DataReturn('服务分类正在占用中', '20003'));
        }
        //判断是否有所属服务分类
        $where_service_class=[
            ['service_class_pid','=',$id],
            ['is_del','=',2],

        ];
        $service_classes=Db::name('service_class')->where($where_service_class)->count();
        if($service_classes>0)
        {
            return json(DataReturn('当前分类被其他分类占用', '20004'));
        }

        Db::startTrans();
        $where_del=['id'=>$id];
        $re=Db::name('service_class')->where($where_del)->delete();
            //->update(['is_del'=>1,'service_class_update'=>TIMESTAMP]);

        if(!$re)
        {
            Db::rollback();
            return json(DataReturn('删除失败', '20004'));
        }
        Db::commit();
        return json(DataReturn('删除成功', 0));

    }

}