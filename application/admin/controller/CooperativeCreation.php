<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/9/29
 * Time: 15:53
 */

namespace app\admin\controller;


use app\service\BizService;
use app\service\ServiceCategory;
use base\Excel;
use Redis\Redis;
use think\Db;

class CooperativeCreation extends Common
{
    /**
     * @return array|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 审核列表
     */
    function auditList()
    {
        if (input('get.action') == 'view') {
            # 页面加载
            return $this->fetch();
        } else {
            $where = array('ac.state' => 1);
            # 搜索条件
            $searchParams = input('post.searchParams');
            # 状态筛选
            if (!empty($searchParams['state'])) {
                $where['ac.state'] = $searchParams['state'];
            }
            # 类型筛选
            if (!empty($searchParams['type'])) {
                $where['ac.type'] = $searchParams['type'];
            }
            # 加载数据
            $list = Db::table('audit_creation ac')
                ->field('ac.*,b.biz_title bizTitle,b.biz_phone bizPhone,concat(b.province,b.city,b.area) bizArea,b.biz_type bizType,
                (select employee_name from employee_sal where id = (select employee_id from assign_staff where biz_id=ac.biz_id and type=1)) employeeName,
                (select service_title from service where id = ac.pid) title,
                (select is_discount from service where id = ac.pid) is_discount,
                (select discount_type from service where id = ac.pid) discount_type
                ')
                ->join('biz b', 'b.id=ac.biz_id', 'left')
                ->where($where)
                ->select();
            if(!empty($list)){
                $redis = new Redis();
                foreach ($list as $k=>$v){
                    if ($v['mark'] == 2) {
                        # 修改审核
                        $info = $redis->hGetJson('BizCreation', strval($v['id']));
                        $list[$k]['is_discount'] = $info['is_discount'];
                        $list[$k]['discount_type'] = $info['discount_type'];
                    }
                }
            }
            $total = Db::table('audit_creation')->where(array('state' => 1))->count();
            return ['code' => 0, 'msg' => '', 'data' => ['data' => $list, 'total' => $total]];
        }
    }

    /**
     * [DataExport 导出当前页的用户数据]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function DataExport()
    {
        $id = input('get.audit_id');
        $where = '';
        if(!empty($id)){
            $where = array('ac.id'=>$id);
        }
//        if (empty($idArr)) {
//            return array('status' => false, 'msg' => '请选择要导出的数据');
//        }
        # 查询对应信息
        $data = Db::table('audit_creation ac')
            ->field('ac.*,b.biz_title bizTitle,b.biz_phone bizPhone,concat(b.province,b.city,b.area) bizArea,b.biz_type bizType,s.is_car_level,
                (select employee_name from employee_sal where id = (select employee_id from assign_staff where biz_id=ac.biz_id and type=1)) employeeName,
                s.service_title  title,s.is_discount is_discount,s.discount_type,service_keyborder,service_statement,s.is_car_level,s.service_time,
                s.is_maintain,s.is_repair,s.is_warranty,is_display,s.add_type,
                s.service_image,s.service_picurl,s.service_context,
                (select service_class_title from service_class where id = s.service_class_id) class_second,
                (select service_class_title from service_class where id = s.class_pid) class_first
                ')
            ->join('biz b', 'b.id=ac.biz_id', 'left')
            ->join('service s', 's.id=ac.pid', 'left')
            ->where(array('ac.state' => 1))
            ->where($where)
//            ->where("id in ($idArr)")
            ->select();

        if (!empty($data)) {
            $redis = new Redis();
            foreach ($data as $k => $v) {
                if ($v['mark'] == 2) {
                    # 修改审核
                    $info = $redis->hGetJson('BizCreation', strval($v['id']));
                    $data[$k] = $info;
                    if ($v['is_car_level'] == 2 or $v['add_type']==1) {
                        $price[0]['discount']=$info['biz_price'];
                        $price[3]['discount']=$info['online_price'];
                    }else{
                        $price = array_merge_recursive($info['biz_price'], $info['online_price']);
                    }
                    $maintain_log = $info['maintain_log'];
                    $repair_log = $info['repair_log'];
                    $warranty_log = $info['warranty_log'];
                } else {
                    # 查询价格
                    $price = Db::table('service_car_mediscount')
                        ->field('id,car_level_id,discount,service_type')
                        ->where(array('biz_id' => $v['biz_id'], 'service_id' => $v['pid']))
                        ->order(array('car_level_id' => 'asc', 'service_type' => 'desc'))
                        ->select();
                    $maintain_log = Db::table('service_log')->where(array('service_id' => $v['pid'], 'log_type' => 1))->find();
                    $repair_log = Db::table('service_log')->where(array('service_id' => $v['pid'], 'log_type' => 2))->find();
                    $warranty_log = Db::table('service_log')->where(array('service_id' => $v['pid'], 'log_type' => 3))->find();
                }
                if ($v['is_car_level'] == 2 or $v['add_type']==1) {
                    $data[$k]['biz_price'] = $price[0]['discount'];
                    $data[$k]['online_price'] = $price[3]['discount'];
                } else {
                    $data[$k]['biz_price'] = "小型车：{$price[0]['discount']}
中型车：{$price[1]['discount']}
大型车：{$price[2]['discount']}";
                    $data[$k]['online_price'] = "小型车：{$price[3]['discount']}
中型车：{$price[4]['discount']}
大型车：{$price[5]['discount']}";
                }
                if (empty($v['employeeName'])) {
                    $data[$k]['employeeName'] = '无';
                }
                # 门店类型
                if ($v['bizType'] == 1) {
                    $data[$k]['bizType'] = '直营店';
                } elseif ($v['bizType'] == 2) {
                    $data[$k]['bizType'] = '加盟店';
                } else {
                    $data[$k]['bizType'] = '合作店';
                }
                # 类型
                if ($v['type'] == 1) {
                    $data[$k]['type'] = '商品';
                } else {
                    $data[$k]['type'] = '服务';
                }
                # 类型
                if ($v['mark'] == 1) {
                    $data[$k]['mark'] = '添加';
                } else {
                    $data[$k]['mark'] = '修改';
                }
                # 是否打折
                if ($v['is_discount'] == 1) {
                    $data[$k]['is_discount'] = '是';
                } else {
                    $data[$k]['is_discount'] = '否';
                }
                # 折扣类型
                $data[$k]['discount_type'] = getDiscountTypeTitle($v['discount_type']);
                # 是否展示用户
                if ($v['is_display'] == 1) {
                    $data[$k]['is_display'] = '是';
                } else {
                    $data[$k]['is_display'] = '否';
                }
                # 服务时长
                $data[$k]['service_time'] .= '分钟';
                # 关联保养记录
                if ($v['is_maintain'] == 1) {
                    $data[$k]['is_maintain'] = "保养有效时长：{$maintain_log['log_validity']}天
有效公里数：{$maintain_log['log_kilometers']}公里";
                } else {
                    $data[$k]['is_maintain'] = '无关联';
                }
                # 关联维修记录
                if ($v['is_repair'] == 1) {
                    $data[$k]['is_repair'] = "维修名称：{$repair_log['log_title']}";
                } else {
                    $data[$k]['is_repair'] = '无关联';
                }
                # 关联质保记录
                if ($v['is_warranty'] == 1) {
                    $data[$k]['is_warranty'] = "质保时长：{$warranty_log['log_validity']}天
质保公里数：{$warranty_log['log_kilometers']}公里
质保范围：{$warranty_log['log_scope']}";
                } else {
                    $data[$k]['is_warranty'] = '无关联';
                }
            }
        }
        $excel_params = [
            'filename' => '审核列表',
            'title' => lang('excel_action_creation_list'),
            'data' => $data
        ];
        $idInfo = implode(',', array_column($data, 'id'));
        if(!empty($id)){
            $idInfo = $id;
        }
        Db::table('audit_creation')->where("id in ($idInfo)")->update(array('is_export' => 1));
        $excel = new Excel($excel_params);
        return $excel->Export();
    }

    function serviceModify()
    {
        $serviceId = input('get.service_id');
        $auditId = input('get.audit_id');
        $redis = new Redis();
        $info = $redis->hGetJson('BizCreation', strval($auditId));
        $info['service_keyborder'] = implode('，', $info['label_info']);
        $where=[
            'where'=>"is_del = 2 and service_class_pid = 0",
        ];

        $info['category']=ServiceCategory::ServiceCategory($where)["data"];

            if(!is_array($info['biz_price'])){
                $info['biz_price'] = array(
                    array('discount'=>$info['biz_price'],'car_level_id'=>2),
                    array('discount'=>$info['biz_price'],'car_level_id'=>1),
                    array('discount'=>$info['biz_price'],'car_level_id'=>0)
                );
            }
            if(!is_array($info['online_price'])){
                $info['online_price'] = array(
                    array('discount'=>$info['online_price'],'car_level_id'=>2),
                    array('discount'=>$info['online_price'],'car_level_id'=>1),
                    array('discount'=>$info['online_price'],'car_level_id'=>0)
                );
            }

        $this->assign('data', $info);
        $this->assign('serviceId', $serviceId);
        return $this->fetch();
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 点击查看服务/商品  价格
     */
    function servicePrice()
    {
        $id = input("get.service_id");
        $type = input("get.type");
        $biz_id = input('get.biz_id');
        $audit_mark = input('get.audit_mark');
        $audit_id = input('get.audit_id');
        if (!empty($id)) {
//            if ($type != 2) {
            # 查询服务价格
            if ($audit_mark == 1) {
                #线上价格
                $oline_price = BizService::getServicePrice($id, '1', '', $biz_id);
                #门店价格
                $biz_price = BizService::getServicePrice($id, '2', '', $biz_id);
            } else {
                $redis = new Redis();
                $info = $redis->hGetJson('BizCreation', strval($audit_id));
                $oline_price = $info['online_price'];
                foreach ($oline_price as $k => $v) {
                    if ($v['car_level_id'] == 1) {
                        $oline_price[$k]['level_title'] = '小型车';
                    } elseif ($v['car_level_id'] == 2) {
                        $oline_price[$k]['level_title'] = '中型车/MPV';
                    } else {
                        $oline_price[$k]['level_title'] = '大型车/MPV';
                    }
                }
                #门店价格
                $biz_price = $info['biz_price'];
                foreach ($biz_price as $k => $v) {
                    if ($v['car_level_id'] == 1) {
                        $biz_price[$k]['level_title'] = '小型车';
                    } elseif ($v['car_level_id'] == 2) {
                        $biz_price[$k]['level_title'] = '中型车/MPV';
                    } else {
                        $biz_price[$k]['level_title'] = '大型车/MPV';
                    }
                }
            }
            $this->assign('biz_online', $oline_price);
            $this->assign('biz_price', $biz_price);
//            } else {
//                # 查询商品价格
//                $proInfo = Db::table('biz_pro')->where(array('id' => $id))->find();
//                $this->assign('biz_pro_online', $proInfo['biz_pro_online']);
//                $this->assign('biz_pro_price', $proInfo['biz_pro_price']);
//            }
            $this->assign('type', $type);
            return $this->fetch('/biz/servicePrice');
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content  审核
     */
    function auditApproved()
    {
        # 审核表id
        $auditId = input('post.id');
        # 服务/商品 id
        $pid = input('post.pid');
        # 门店id
        $bizId = input('post.biz_id');
        # 类型 1商品 2服务
        $type = input('post.type');
        # 审核结果
        $state = input('post.state');
        # 审核驳回时候填写的 拒绝理由
        $reasons = input('post.reasons');
        if (empty($auditId)) {
            return array('status' => false, 'msg' => '系统错误,请联系管理员');
        }
        # 判断是否操作过
        $auditInfo = Db::table('audit_creation')->where(array('id' => $auditId))->find();
        if ($auditInfo['state'] != 1) {
            return array('status' => false, 'msg' => '已经审核过,无需操作');
        }
        if ($state == 2) {
            # 审核通过
            if ($auditInfo['mark'] == 2) {
                #修改审核
                $redis = new Redis();
                $info = $redis->hGetJson('BizCreation', strval($auditId));
                Db::table('aaa')->insert(array(
                    'info' => json_encode($info),
                    'type' => 'BizCreation' . $auditId
                ));
//                $oldService = Db::table('service')->where(array('id' => $pid))->find();
                $service_keyborder = implode('，', $info['label_info']);
                if($info['addType']==1){
                    $info['is_car_level']= 2;
                }
                Db::table('service')
                    ->where(array('id' => $pid))
                    ->update(array(
                        'service_title' => $info['service_title'],//服务名称
                        'service_class_id' => $info['service_class_id'],//服务分类id
                        'class_pid' => $info['class_pid'],//最高级分类id
                        'service_keyborder' => $service_keyborder,//关键词
                        'service_statement' => $info['service_statement'],//声明
                        'is_car_level' => $info['is_car_level'],//是否按车型设置价格
                        'service_time' => $info['service_time'],//服务时长
                        'is_discount' => $info['is_discount'],//是否打折
                        'discount_type' => $info['discount_type'],//折扣类型
                        'is_display' => $info['is_display'],//展示状态
                        'is_maintain' => $info['is_maintain'],//是否关联保养记录
                        'is_repair' => $info['is_repair'],//是否关联维修记录
                        'is_warranty' => $info['is_warranty'],//是否关联质保记录
                        'audit_status' => 1,//审核状态 1=>审核中
                        'service_status' => 1,//上下架状态
                        'service_image' => $info['service_image'],//缩略图
                        'service_picurl' => $info['service_picurl'],//banner
                        'service_context' => $info['service_context'],//详情图
                        'add_type' => $info['addType']
                    ));
                if ($info['is_car_level'] == 1) {
                    # 按车型设置价格
                    # 修改门店服务价格
                    foreach ($info['biz_price'] as $k => $v) {
                        Db::table('service_car_mediscount')
                            ->where(array('id' => $v['id']))
                            ->update(array('discount' => $v['discount']));
                    }
                    # 修改线上服务价格
                    foreach ($info['online_price'] as $k => $v) {
                        Db::table('service_car_mediscount')
                            ->where(array('id' => $v['id']))
                            ->update(array('discount' => $info['biz_price'][$k]['discount']));
                    }
                } else {
                    if(is_array($info['biz_price'])){
                        $info['biz_price'] = $info['biz_price'][0]['discount'];
                    }
                    if(is_array($info['online_price'])){
                        $info['online_price'] = $info['online_price'][0]['discount'];
                    }
                    $info['online_price'] = $info['biz_price'];
                    Db::table('service_car_mediscount')
                        ->where(array('service_id' => $pid, 'biz_id' => $bizId, 'service_type' => 2))
                        ->update(array('discount' => $info['biz_price']));
                    Db::table('service_car_mediscount')
                        ->where(array('service_id' => $pid, 'biz_id' => $bizId, 'service_type' => 1))
                        ->update(array('discount' => $info['online_price']));
                }
                if ($info['is_discount'] == 1) {
                    # 打折
                    $level_discount = Db::table('level_discount')
                        ->field('level_id,type,discount')
                        ->where(array('type' => $info['discount_type']))->select();
                    foreach ($level_discount as $k => $v) {
                        Db::table('service_mediscount')->where(array(
                            'service_id' => $pid,
                            'member_level' => $v['level_id']))
                            ->update(array(
                                'discount' => $v['discount'],
                                'discount_type' => $info['discount_type']
                            ));
                    }
                } else {
                    Db::table('service_mediscount')->where(array(
                        'service_id' => $pid))
                        ->update(array(
                            'discount' => 1,
                            'discount_type' => $info['discount_type']
                        ));
                }
                # 关联保养记录信息添加
                if ($info['is_maintain'] == 1) {
                    if (array_key_exists('id', $info['maintain_log'])) {
                        Db::table('service_log')->where(array('id' => $info['maintain_log']['id']))->update(array(
                            'service_id' => $pid,
                            'log_title' => $info['maintain_log']['log_title'],
                            'log_validity' => $info['maintain_log']['log_validity'],
                            'log_kilometers' => $info['maintain_log']['log_kilometers'],
                            'log_type' => 1,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    } else {
                        Db::table('service_log')->insert(array(
                            'service_id' => $pid,
                            'log_title' => $info['maintain_log']['log_title'],
                            'log_validity' => $info['maintain_log']['log_validity'],
                            'log_kilometers' => $info['maintain_log']['log_kilometers'],
                            'log_type' => 1,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    }
                }
                # 关联维修记录信息添加
                if ($info['is_repair'] == 1) {
                    if (array_key_exists('id', $info['repair_log'])) {
                        Db::table('service_log')->where(array('id' => $info['repair_log']['id']))->update(array(
                            'service_id' => $pid,
                            'log_title' => $info['repair_log']['log_title'],
                            'log_type' => 2,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    } else {
                        Db::table('service_log')->insert(array(
                            'service_id' => $pid,
                            'log_title' => $info['repair_log']['log_title'],
                            'log_type' => 2,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    }
                }
                # 关联质保记录信息添加
                if ($info['is_warranty'] == 1) {
                    if (array_key_exists('id', $info['warranty_log'])) {
                        Db::table('service_log')->where(array('id' => $info['warranty_log']['id']))->update(array(
                            'service_id' => $pid,
                            'log_validity' => $info['warranty_log']['log_validity'],
                            'log_kilometers' => $info['warranty_log']['log_kilometers'],
                            'log_scope' => $info['warranty_log']['log_scope'],
                            'log_type' => 3,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    } else {
                        Db::table('service_log')->insert(array(
                            'service_id' => $pid,
                            'log_validity' => $info['warranty_log']['log_validity'],
                            'log_kilometers' => $info['warranty_log']['log_kilometers'],
                            'log_scope' => $info['warranty_log']['log_scope'],
                            'log_type' => 3,
                            'log_time' => date('Y-m-d H:i:s')
                        ));
                    }
                }
            }
            $auditRes = Db::table('audit_creation')->where(array('id' => $auditId))->update(array('state' => $state, 'audit_time' => date('Y-m-d H:i:s')));
        } else {
            # 审核驳回
            $auditRes = Db::table('audit_creation')->where(array('id' => $auditId))->update(array('state' => $state, 'audit_time' => date('Y-m-d H:i:s'), 'reasons' => $reasons));
        }
        if ($auditRes) {
            # 修改服务/商品 状态
            if ($type == 1) {
                # 服务表
                $table = 'service';
            } else {
                # 商品表
                $table = 'service';
            }
            Db::table($table)->where(array('id' => $pid))->update(array('audit_status' => $state));
        }
        # 给门店发通知消息
        if ($state == 2) {
            # 审核通过时的消息id
            $biz_news_id = 1;
        } else {
            $biz_news_id = Db::table('biz_news')->insertGetId(array(
                'content'=>$reasons,
                'type'=>3,
                'create_time'=>date('Y-m-d H:i:s'),
                'title'=>'审核驳回-'
            ));
        }
        $newsInfo = Db::table('biz_news_mapping')->where("biz_news_id = $biz_news_id and biz_id = $bizId")->select();
        if (empty($newsInfo)) {
            # 当没有未查看的通知消息是时添加
            Db::table('biz_news_mapping')->insert(array('biz_news_id' => $biz_news_id, 'biz_id' => $bizId, 'status' => 1, 'send_time' => date('Y-m-d H:i:s')));
        }
        # 首页弹窗通知状态判断
        $redis = new Redis();
        $auditState = $redis->hGet('CooperativeCreation', strval($bizId));
        if (empty($auditState)) {
            $redis->hSet('CooperativeCreation', strval($bizId), 1);
        }
        $redis->hDel('BizCreation', strval($auditId));
        return array('status' => true);
    }
}
