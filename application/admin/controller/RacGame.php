<?php


namespace app\admin\controller;


use app\reuse\controller\ResponseJson;
use app\service\RacGameService;
use think\Db;

class RacGame extends Common
{
    use ResponseJson;
    function __construct()
    {
        parent::__construct();
        // 登录校验
        $this->IsLogin();
    }

    function index(){
        if(input("get.action")=="ajax"){
            $RacGameService = new RacGameService();
            $prize=$RacGameService->racPrize();
            return ['code' => 0, 'msg' => '','count' => 1,'data' => array($prize)];
        }else{
            return $this->fetch();
        }

    }

    function saveData()
    {
        if(input("get.action")=='ajax'){
            $param = input("post.");
            $id = $param["id"];
            unset($param['id']);
            $res=Db::table("rac_prize")->where(array("id"=>$id))->update($param);
            if($res){
               return DataReturn('保存成功', 0);
            }else{
                throw new \BaseException(['code'=>403 ,'errorCode'=>4001,'msg'=>'保存失败','status'=>false,'debug'=>false]);
            }
        }else{
            $RacGameService = new RacGameService();
            $prize=$RacGameService->racPrize();
            $this->assign("data",$prize);
            return $this->fetch();
        }
    }

    function RacLog()
    {
        $RacGameService = new RacGameService();
        if(input("get.action")=="ajax"){
            $where = "rl.id >0";
            # 定义listRows 分页
            $listRows = input('post.limit');
            if (empty($listRows)) {
                $listRows = 10;
            }
            $searchParams = json_decode(input("post.searchParams"),true);
            if(!empty($searchParams)){
                if(!empty($searchParams['search'])){
                    $where.=" and m.member_phone = '".$searchParams['search']."'";
                }
                if(!empty($searchParams['start_time'])){
                    $where.=" and date(rl.log_time) = '".$searchParams['start_time']."'";
                }
                if(!empty($searchParams['type'])){
                    $where.=" and rl.rac_status = {$searchParams['type']}";
                }
            }

            $list = $RacGameService ->racLog(true,$listRows,$where);
            return $this->jsonSuccessData($list);
        }else{
            $static=$RacGameService->racStatic();
            $this->assign("static",$static);
            return $this->fetch();
        }
    }

    function logHelp()
    {
        $member_id = input("get.member_id");
        $member_repeat_num = input("get.member_repeat_num");
        if(input("get.action")=='ajax'){

            $where="help_repeat_num = {$member_repeat_num}";
            $RacGameService = new RacGameService();
            $list = $RacGameService->racHelp(true,$member_id,$where);
            return $this->jsonSuccessData($list);
        }else{
            $this->assign("member_repeat_num",$member_repeat_num);
            $this->assign("member_id",$member_id);
            return $this->fetch();
        }
    }

    function racShop()
    {
        if(input("get.action")=='ajax'){
            $RacGameService = new RacGameService();
            $list = $RacGameService->racStore(true,10);
            return $this->jsonSuccessData($list);
        }else{
            return $this->fetch();
        }
    }

    function relationStore()
    {
        if(input("get.action")=='ajax'){
            $searchParams = json_decode(input("post.searchParams"),true);
            $RacGameService = new RacGameService();
            $list = $RacGameService->getsSotre($searchParams);
            return ['code' => 0, 'msg' => '', 'count' =>count($list), 'data' => $list];
        }else{
            return $this->fetch();
        }
    }

    function startRelation()
    {
        $biz_id = input("post.biz_id");
        $RacGameService = new RacGameService();
        return $RacGameService->relationGameStore($biz_id);
    }

    function deleteRelation()
    {
        $biz_id = input("post.biz_id");
        $RacGameService = new RacGameService();
        return $RacGameService->deleteGameRelation($biz_id);
    }

}