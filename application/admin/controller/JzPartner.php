<?php


namespace app\admin\controller;

use app\service\BaseService;
use app\service\DistributionService;

use Redis\Redis;
use think\Db;


/**
 * 合伙人管理
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月23日09:27:10
 */
class JzPartner extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日09:56:22
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验
        $this->IsLogin();

    }

    /**
     * [Index 分销列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public function Index()
    {

        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where = DistributionService::ListWhere($params);
            $field='m.member_province,m.member_city,m.member_area,mp.id,mp.member_id,
                    mp.partner_recommender_id,m.member_name,m.nickname,mp.phone,m.member_phone,
                    mp.create_time,mp.status,mp.partner_balance_total,mp.partner_balance';
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>$field
            );
            $data = DistributionService::DataList($data_params);
            //dump($data);exit;


            return ['code' => 0, 'msg' => '', 'data' => $data];
        }else{
            return $this->fetch();
        }
    }
    /**
     * [Index 推荐列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public function RecommendList(){
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            if(!empty($params['param']['keywords']))
            {
                $where[] =['m.phone|m.member_name', 'like', '%'.$params["param"]['keywords'].'%'];
            }
            $where_channel=[
                ['pid','=',$params['recommender_id']],
                ['channel','in',[2,3,5]]
            ];
            $id_arr=Db::name('channel')->where($where_channel)->group('member_id')->column('member_id');

            $where[] = ['m.id','in',$id_arr];
            $field='m.id member_id,m.member_name,m.nickname,m.member_phone,
                     mp.status,mp.partner_balance_total,mp.partner_balance,lh.member_create handle_time';

            $data = Db::name('member')->alias('m')

                ->leftJoin(['member_partner'=>'mp'],'m.id=mp.member_id')
                ->leftJoin("log_handlecard lh","lh.member_id = m.id")
                ->where($where)
                ->field($field)->order('m.id')->group('m.id');



            //分页
            $data=$data->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray();
            $data= DistributionService::DataDealWith($data);



            return ['code' => 0, 'msg' => '', 'data' => $data];
        }else{
            return $this->fetch();
        }
    }
    /**
     * [DistributionDetail 分销商详细信息]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public  function DistributionDetail()
    {
        // 参数
        $params = input();
        // 数据
        $data = [];
        if (!empty($params['id'])) {
            // 获取列表
            $data_params = [
                'where'				=> ['id'=>$params['id']],
                'm'					=> 0,
                'n'					=> 1,
                'page'			  => false,
                'table'     =>'member_partner'
            ];
            $ret = BaseService::DataList($data_params);

            $ret= $data=DistributionService::DataDealWith($ret);;

            $data = empty($ret[0]) ? [] : $ret[0];
        }


        $this->assign('data', $data);

        return $this->fetch();
    }

    /**
     * [DelInfo 停用分销商]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月10日11:14:22
     */
    public function DelInfo(){
        // 是否ajax
        if (!IS_AJAX)
        {
            return $this->error('非法访问');
        }

        $params=$this->data_post;
        $params['table']='member_partner';
        $params['soft']=true;
        $params['soft_arr']=['status'=>$params['status']];
        $params['errorcode']=90001;
        $params['msg']='修改状态失败';

        BaseService::DelInfo($params);

        return DataReturn('修改成功', 0);
    }
    /**
     * [memberlist 获取用户列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function memberlist(){
        $params = input();
        $re= DistributionService::MemberListPage($params);

        return $re;
    }
    /**
     * [setDistributionPage 创建分销商页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function setDistributionPage()
    {
        $params = input();
        $re= DistributionService::SavePage($params);
        return DataReturn('ok',0,['html'=>$re['data']]);
    }
    /**
     * [setDistribution 创建分销商]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function setDistribution()
    {
        return DistributionService::SaveData(input());
    }
    /**
     * [PartnerSet 分销设置页面]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function PartnerSet()
    {


        $data=Db::name('sysset')->where(['tagname'=>'partner_set'])->value('desc');
        $data=json_decode($data,true);



        $this->assign('data',$data);
        $this->assign('partner_prise_type',lang('partner_prise_type'));
        return $this->fetch();
    }


    /**
     * [DoPartnerSet 执行分销设置]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function DoPartnerSet(){

        return DistributionService::SavePartnerSet($this->data_post);
    }
    /**
     * [PartnerPrise 查看分销奖励]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function PartnerPrise(){
        $data=Db::name('sysset')->where(['tagname'=>'partner_set'])->value('desc');

        $data=json_decode($data,true);

        $data=DistributionService::PartnerPriseDealWith($data);

        $this->assign('data',$data);
         return $this->fetch();
    }
    /**
     * [DelPartnerSet 删除分销奖励]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function DelPartnerSet(){
        return DistributionService::DelPartnerPrise(input());
    }
    /**
     * [PartnerSetDetail 分销奖励详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function PartnerSetDetail(){
      $data=Db::name('redpacket_detail')->where('redpacket_id='.input('red_id'))->select();
        foreach ($data as &$v)
        {
            $v['money_off']=priceFormat($v['money_off']);
            $v['voucher_price']=priceFormat($v['voucher_price']);
        }
      return DataReturn('ok',0,$data);
    }
    /**
     * [DoSaveRedDetail 修改红包详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function DoSaveRedDetail(){

        Db::name('redpacket_detail')->where('id='.input('id'))->update([input('field')=>input('value')]);
        $redis=new Redis();
        $redis->hDel('partner_set', 'info');
        return DataReturn('ok',0);
    }
    /**
     * [DelRedDetail 修改红包详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年11月11日10:38:18
     */
    public function DelRedDetail(){

        Db::name('redpacket_detail')->where('id='.input('id'))->delete();
        $redis=new Redis();
        $redis->hDel('partner_set', 'info');
        return DataReturn('ok',0);
    }

    /**
     * @return array|\think\response\View
     * @context 高级合伙人列表
     */
    function partnerManage(){
        if(input("get.action")=='ajax'){
            $params = input();
            // 条件
            $where = DistributionService::ListWhere($params);
            $where[]=['mp.senior_partner','=','2'];
            $field='m.member_province,m.member_city,m.member_area,mp.id,mp.member_id,
                    mp.partner_recommender_id,m.member_name,m.nickname,mp.phone,m.member_phone,
                    mp.create_time,mp.status,mp.partner_balance_total,mp.partner_balance';
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>$field
            );
            $data = DistributionService::DataList($data_params);
            if(!empty($data['data'])){
                foreach($data['data'] as $k=>$v){
                    $lowLevel = DistributionService::seniorLowerLevel(array("member_id"=>$v['member_id'],"member_type"=>2));
                    $data['data'][$k]['low_level'] = count($lowLevel);
                    $data['data'][$k]['team_coll'] = implode(',',array_column($lowLevel,"member_id"));
                }
            }
            return ['code' => 0, 'msg' => '', 'data' => $data];
        }else{
            return view();
        }
    }

    /**
     * @return array
     * @throws \BaseException
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 设置高级合伙人
     */
    function setSeniorPertner(){
        $param = input();
        return DistributionService::SaveSeniorData($param);
    }

    /**
     * @return array|\PDOStatement|string|\think\Model|\think\response\View|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 合伙人关联关系
     */
    function partnerRelation(){
        $member_id = input("get.member_id");
        if(input("get.action")=='ajax'){
            $mainInfo=Db::table("member m,member_mapping mm")->field("mm.member_id,mm.member_level_id,m.member_name,m.nickname,m.member_phone,m.member_province,m.member_city,m.member_area")
                ->where(array("m.id"=>$member_id))->where("m.id=mm.member_id")->find();
            $mainInfo['member_level_title'] = getsMemberLevelTitle($mainInfo['member_level_id']);
            $mainInfo['name'] = $mainInfo['member_name'];
            if(empty($mainInfo['member_name'])){
                $mainInfo['name'] = $mainInfo['nickname'];
                if(empty($mainInfo['nickname'])){
                    $mainInfo['name'] = "未知名称";
                }
            }
            $mainInfo['name'] = "\r\n".$mainInfo['name']."\r\n".$mainInfo['member_phone'];
            $mainInfo['source'] ="地区：{$mainInfo['member_province']}{$mainInfo['member_city']}{$mainInfo['member_area']}".
                "<br />"."会员等级：{$mainInfo['member_level_title']}"."<br />"."手机号：{$mainInfo['member_phone']}";
           $data = DistributionService::seniorLowerLevel(array("member_id"=>$member_id,"member_type"=>2));
            if(!empty($data)){
                $new_data=DistributionService::seniorDataHandle($data);
                $mainInfo['children'] = $new_data;
            }
            return $mainInfo;

        }else{
            $this->assign("member_id",$member_id);
            return view();
        }

    }

    /**
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 高级合伙人管理 地区统计人数
     */
    function ManageStatic($params = array())
    {
        if(!empty($params)){
            $where = "mp.id > 0";
            if(!empty($params['start_time'])){
                $where.=" and date(mp.create_time)>='".$params['start_time']."'";
            }
            if(!empty($params['end_time'])){
                $where.=" and date(mp.create_time)<='".$params['end_time']."'";
            }
            if(!empty($params['month'])){
                $where.=" and date_format(mp.create_time,'%Y-%m')='".$params['month']."'";
            }
            if(!empty($params['member_province'])){
                $where.=" and m.member_province='".$params['member_province']."'";
            }
            if(!empty($params['member_city'])){
                $where.=" and m.member_city='".$params['member_city']."'";
            }
            if(!empty($params['member_area'])){
                $where.=" and m.member_area='".$params['member_area']."'";
            }
        }
        $static = Db::table("member_partner mp")
            ->field("m.member_province,IFNULL(m.member_city,'') member_city,IFNULL(m.member_area,'未知区域') member_area
                ,count(mp.id) num")
            ->Join("member m","mp.member_id = m.id")
            ->where(array("mp.senior_partner"=>2,"mp.status"=>1))
            ->where("member_id!=25 and member_id != 9405")
            ->where($where)
            ->group("m.member_province,m.member_city,m.member_area")
            ->select();
        return $static;
    }

    /**
     * @return array|\think\response\View
     * @context 合伙人收入、推荐统计
     */
    function static_list()
    {
        if(input("get.action")=='ajax'){
            $params = input();
            // 条件
            $where = DistributionService::ListWhere($params);
            $where[]=['mp.senior_partner','=','2'];
            $field='m.member_province,m.member_city,m.member_area,mp.id,mp.member_id,
                    mp.partner_recommender_id,m.member_name,m.nickname,mp.phone,m.member_phone,
                    mp.create_time,mp.status,mp.partner_balance_total,mp.partner_balance';
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'field'     =>$field
            );
            $data = DistributionService::DataList($data_params);
            if(!empty($data['data'])){
                foreach($data['data'] as $k=>$v){
                   $handle=$this->staticListDataHandle($v['member_id'],$params);
                   foreach($handle as $hk=>$hv){
                        $data['data'][$k][$hk]=$hv;
                   }
                }
            }
            return ['code' => 0, 'msg' => '', 'data' => $data];
        }else{
            return view();
        }
    }

    /**
     * @param $member_id
     * @param $params
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 统计数据查询
     */
    function staticListDataHandle($member_id,$params)
    {
        $nowMonth = date("Y-m");
        if(!empty($params['start_time'])){
            $nowMonth = date("Y-m",$params['start_time']);
        }
        $lastMonth = date("Y-m",strtotime("-1 month",strtotime($nowMonth)));
        $allLowLevel = DistributionService::seniorLowerLevel(array("member_id"=>$member_id));
        $lowLevel = $this->lowLevelPartners($allLowLevel,$params); // 所有下级推广员
        # 下级推广员总数
        $lowLevelNum = count($lowLevel);
        # 下级推广员收益总和
        $lowLevelEarn = $this->partnerConsumpSum($lowLevel,$params);
        # 本月新增推广员
        $nowMonthAddList =$this->lowLevelPartners($allLowLevel,array("month"=>$nowMonth));
        $nowMonthAdd = count($nowMonthAddList);
        # 上月新增推广员
        $lastMonthAddList = $this->lowLevelPartners($allLowLevel,array("month"=>$lastMonth));
        $lastMonthAdd = count($lastMonthAddList);
        # 累计消费
        $cumulative = $this->lowLevelCumulative($allLowLevel,$nowMonth,$lastMonth);
        # 本月累计消费
        $nowCumulative = $cumulative['nowPrice'];
        # 上月累计消费
        $lastCumulative = $cumulative['lastPrice'];
        # 推广员本月总收益
        $nowMonthEarn = $this->partnerConsumpSum($lowLevel,array("month"=>$nowMonth));
        # 推广员上月总收益
        $lastMonthEarn = $this->partnerConsumpSum($lowLevel,array("month"=>$lastMonth));
        # 关联会员数量
        $relationMember = count($this->relationMember($allLowLevel,$params));
        # 本月关联会员数量
        $nowRelationMember = count($this->relationMember($allLowLevel,array("month"=>$nowMonth)));
        # 上月关联会员数量
        $lastRelationMember = count($this->relationMember($allLowLevel,array("month"=>$lastMonth)));
        return array("lowLevelNum"=>$lowLevelNum,"lowLevelEarn"=>$lowLevelEarn,"nowMonthAdd"=>$nowMonthAdd,"lastMonthAdd"=>$lastMonthAdd,
            "cumulative"=>$cumulative,"nowCumulative"=>$nowCumulative,"lastCumulative"=>$lastCumulative,"nowMonthEarn"=>$nowMonthEarn,
            "lastMonthEarn"=>$lastMonthEarn,"relationMember"=>$relationMember,"nowRelationMember"=>$nowRelationMember,"lastRelationMember"=>$lastRelationMember);
    }

    /**
     * @param $data
     * @param $params
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 下级推广员
     */
    function lowLevelPartners($data,$params)
    {
        $list = array();
        if(!empty($data)){
            $where = "id > 0";
            if(!empty($params['start_time'])){
                $where.=" and date(create_time)>='".$params['start_time']."'";
            }
            if(!empty($params['end_time'])){
                $where.=" and date(create_time)<='".$params['end_time']."'";
            }
            if(!empty($params['month'])){
                $where.=" and date_format(create_time,'%Y-%m')='".$params['month']."'";
            }
            $id_con = implode(',',array_column($data,'member_id'));
            $list = Db::table("member_partner")->field("member_id")->where("member_id in ({$id_con})")
                ->where($where)->select();
        }
        return $list;
    }

    /**
     * @param $member_id
     * @param $params
     * @return float
     * @context 推广员收益
     */
    function partnerConsump($member_id,$params)
    {
        $where = "id > 0";
        if(!empty($params['start_time'])){
            $where.=" and date(consump_time)>='".$params['start_time']."'";
        }
        if(!empty($params['end_time'])){
            $where.=" and date(consump_time)<='".$params['end_time']."'";
        }
        if(!empty($params['month'])){
            $where.=" and date_format(consump_time,'%Y-%m')='".$params['month']."'";
        }
        $consump = Db::table("log_consump")->where(array("member_id"=>$member_id,"log_consump_type"=>2))
            ->where("income_type in (8,9,10,11)")
            ->where($where)
            ->sum("consump_price");
        return $consump;
    }

    /**
     * @param $data
     * @param $params
     * @return float|int
     * @context 推广员收益总和
     */
    function partnerConsumpSum($data,$params)
    {
        $price = 0;
        if(!empty($data)){
            foreach($data as $k=>$v){
                $price+=$this->partnerConsump($v['member_id'],$params);
            }
        }
        return $price;
    }

    /**
     * @param $data
     * @param $nowMonth
     * @param $lastMonth
     * @return array
     * @context 本月 上月总消费
     */
    function lowLevelCumulative($data,$nowMonth,$lastMonth)
    {
        $now = $this->CumulativeData($data,$nowMonth);
        $last =$this->CumulativeData($data,$lastMonth);
        return array("nowPrice"=>$now,"lastPrice"=>$last);
    }

    /**
     * @param $data
     * @param $month
     * @return float|int
     * @context 月份 会员总消费
     *
     */
    function CumulativeData($data,$month)
    {
        $price = 0;
        $id_con = implode(',',array_column($data,'member_id'));
        if(!empty($data)){
            $price = Db::table("log_consump")->where(array("log_consump_type"=>1))
                ->where("member_id in ({$id_con}) and date_format(consump_time,'%Y-%m')='".$month."'")
                ->where("income_type not in (2,6,7,13)")
                ->sum("consump_price");
        }
        return $price;
    }

    /**
     * @param $data
     * @param $params
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 关联会员数量
     */
    function relationMember($data,$params)
    {
        $list =array();
        if(!empty($data)){
            $where = "lh.id > 0";
            if(!empty($params['start_time'])){
                $where.=" and date(lh.member_create)>='".$params['start_time']."'";
            }
            if(!empty($params['end_time'])){
                $where.=" and date(lh.member_create)<='".$params['end_time']."'";
            }
            if(!empty($params['month'])){
                $where.=" and date_format(lh.member_create,'%Y-%m')='".$params['month']."'";
            }
            $id_con = implode(',',array_column($data,'member_id'));
            $list = Db::table("log_handlecard lh,member_mapping mm")->field("lh.member_id")
                ->where("lh.member_id in ({$id_con}) and lh.member_id = mm.member_id")
                ->where($where)
                ->select();
        }
        return $list;
    }

    /**
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户列表 高级合伙人统计
     */
    function partnerDataStatic()
    {
        if(input("get.action")=='ajax'){
            $params = input();
            $list =$this->partnerDistend($this->ManageStatic($params['param']),$params);
            return ['code' => 0, 'msg' => '', 'data' =>["data"=>$list] ];
        }else{
            return view();
        }

    }

    /**
     * @param $list
     * @param $params
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 地区数据统计
     */
    function partnerDistend($list,$params)
    {
        if(!empty($list)){
            if(!empty($params)){
                $where = "mp.id > 0";
                if(!empty($params['start_time'])){
                    $where.=" and date(mp.create_time)>='".$params['start_time']."'";
                }
                if(!empty($params['end_time'])){
                    $where.=" and date(mp.create_time)<='".$params['end_time']."'";
                }
                if(!empty($params['month'])){
                    $where.=" and date_format(mp.create_time,'%Y-%m')='".$params['month']."'";
                }
                if(!empty($params['member_province'])){
                    $where.=" and m.member_province='".$params['member_province']."'";
                }
                if(!empty($params['member_city'])){
                    $where.=" and m.member_city='".$params['member_city']."'";
                }
                if(!empty($params['member_area'])){
                    $where.=" and m.member_area='".$params['member_area']."'";
                }
            }
            $nowMonth = date("Y-m");
            if(!empty($params['start_time'])){
                $nowMonth = date("Y-m",$params['start_time']);
            }
            $lastMonth = date("Y-m",strtotime("-1 month",strtotime($nowMonth)));
            foreach($list as $k=>$v){
                $list[$k]['area'] = $v['member_province'].$v['member_city'].$v['member_area'];
                $nowMonthAdd = 0;
                $lastMonthAdd = 0;
                $lowLevelEarn = 0;
                $nowCumulative =0;
                $lastCumulative = 0;
                $nowMonthEarn = 0;
                $lastMonthEarn =0;
                $relationMember =0;
                $nowRelationMember =0;
                $lastRelationMember =0;
                $static = Db::table("member_partner mp")
                    ->field("mp.member_id")
                    ->Join("member m","mp.member_id = m.id")
                    ->where(array("mp.senior_partner"=>2,"mp.status"=>1))
                    ->where("member_id!=25 and member_id != 9405")
                    ->where("m.member_province = '".$v['member_province']."'and m.member_city = '".$v['member_city']."' and m.member_area='".$v['member_area']."'")
                    ->where($where)
                    ->select();
                if(!empty($static)){
                    foreach($static as $sk=>$sv){
                        $allLowLevel = DistributionService::seniorLowerLevel(array("member_id"=>$sv['member_id']));
                        $lowLevel = $this->lowLevelPartners($allLowLevel,array()); // 所有下级推广员
                        if(empty($allLowLevel)){
                            $allLowLevel = array();
                        }
                        if(empty($lowLevel)){
                            $lowlevel =array();
                        }

                        # 本月新增推广员
                        $nowMonthAddList =$this->lowLevelPartners($allLowLevel,array("month"=>$nowMonth));
                        $nowMonthAdd += count($nowMonthAddList);
                        # 推广员本月总收益
                        $nowMonthEarn += $this->partnerConsumpSum($lowLevel,array("month"=>$nowMonth));
                        # 推广员上月总收益
                        $lastMonthEarn += $this->partnerConsumpSum($lowLevel,array("month"=>$lastMonth));
                        # 上月新增推广员
                        $lastMonthAddList = $this->lowLevelPartners($allLowLevel,array("month"=>$lastMonth));
                        $lastMonthAdd += count($lastMonthAddList);
                        # 关联会员数量
                        $relationMember += count($this->relationMember($allLowLevel,$params));
                        # 本月关联会员数量
                        $nowRelationMember += count($this->relationMember($allLowLevel,array("month"=>$nowMonth)));
                        # 上月关联会员数量
                        $lastRelationMember += count($this->relationMember($allLowLevel,array("month"=>$lastMonth)));
                        # 下级推广员收益总和
                        $lowLevelEarn += $this->partnerConsumpSum($lowLevel,$params);
                        //array_push($lowlevel,array("member_id"=>$sv['member_id'])); // 算上合伙人本身的
                       // array_push($allLowLevel,array("member_id"=>$sv['member_id'])); // 算上合伙人本身的

                        # 累计消费+ 合伙人本身
                        $cumulative = $this->lowLevelCumulative($allLowLevel,$nowMonth,$lastMonth);
                        # 本月累计消费+ 合伙人本身
                        $nowCumulative += $cumulative['nowPrice'];
                        # 上月累计消费+ 合伙人本身
                        $lastCumulative += $cumulative['lastPrice'];


                    }
                }
                $list[$k]['nowMonthAdd']= $nowMonthAdd;
                $list[$k]['lastMonthAdd'] = $lastMonthAdd;
                $list[$k]['lowLevelEarn'] = $lowLevelEarn;
                $list[$k]['nowCumulative'] =$nowCumulative;
                $list[$k]['lastCumulative'] = $lastCumulative;
                $list[$k]['nowMonthEarn'] = $nowMonthEarn;
                $list[$k]['lastMonthEarn'] =$lastMonthEarn;
                $list[$k]['relationMember'] =$relationMember;
                $list[$k]['nowRelationMember'] =$nowRelationMember;
                $list[$k]['lastRelationMember'] =$lastRelationMember;
            }
            return $list;
        }
    }
}