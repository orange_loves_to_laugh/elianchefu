<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/17 0017
 * Time: 17:49
 */

namespace app\calling\service;


use think\Db;

class CallService
{
    /**
     * [Callstatus 呼叫状态查看]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
    public static function Callstatus($member_id){

        //今日呼叫次数书否已达到上限
        $where=[
            ['member_id','=',$member_id],
            ['create_time','>',date('Y-m-d 00:00:00')],
            ['create_time','<',date('Y-m-d 23:59:59')],
        ];
        $count=Db::name('employee_calling')->where($where)->count();
        if ($count>=5) {
            return 'top';
        }
        //今日他人是否完成呼叫
        $sysset_call=Db::name('sysset')->where(['tagname'=>'call_set',])->value('desc');
        $where=[
            ['duration','>=',json_decode($sysset_call,true)['billsec']],
            ['member_id','=',$member_id],
            ['create_time','>',date('Y-m-d 00:00:00')],
            ['create_time','<',date('Y-m-d 23:59:59')],
        ];
        $tell=Db::name('employee_calling')->where($where)->find();

        if ($tell['employee_id']>0&&$tell['employee_id']!=session('calling')['id']) {
            return 'called';
        }
        //当前是否有人正在呼叫
        return true;
    }
}