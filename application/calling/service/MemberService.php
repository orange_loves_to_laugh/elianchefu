<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/28 0028
 * Time: 15:55
 */

namespace app\calling\service;

use app\service\BaseService;
use app\service\OssService;
use PHPQRcode\QRcode;
use think\Db;

/**
 * 用户数据服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 */
class MemberService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {


//       $re= Db::query('mysqldump -uroot -p ecars >  all.sql');

        $where = [];
        $member_arr=[];
        //关键词
        if(!empty($params['param']['keywords']))
        {
            $where[] =['m.member_phone|mc.car_licens', 'like', '%'.$params["param"]['keywords'].'%'];
            $arr=Db::name('member')->alias('m')
                ->leftJoin(['member_car'=>'mc'],'m.id=mc.member_id')
                ->where([['m.member_phone|mc.car_licens', 'like', '%'.$params["param"]['keywords'].'%']])
                ->column('m.id');
            array_push($member_arr,$arr);

        }

        //m.register_time 注册时间
        if(!empty($params['param']['register_time']))
        {
            $time=explode(' to ',$params['param']['register_time']);
            $where[] =['m.register_time', '>', $time[0]];
            $where[] =['m.register_time', '<', $time[1]];
        }

        //m.register_time 会员创建时间
        if(!empty($params['param']['member_create']))
        {
            $time=explode(' to ',$params['param']['member_create']);
            $member_create[] =['member_create', '>', $time[0]];
            $member_create[] =['member_create', '<', $time[1]];
            $id_arr=Db::name('member_mapping')->where($member_create)->column('member_id');
            $where[]=['m.id','in',$id_arr];
        }
        //会员等级
        if(!empty($params['param']['member_level']))
        {
            $where[] =['member_level_id', '=', $params["param"]['member_level']];
            $arr=Db::name('member_mapping')->where($where)->column('member_id');
            array_push($member_arr,$arr);
        }

        //用户性别
        if(strlen($params['param']['member_sex'])>0)
        {
            $where[] =['m.member_sex', '=', $params["param"]['member_sex']];
        }

        //呼叫次数
        if(!empty($params['param']['calling_time']))
        {

            if (intval($params['param']['calling_time'])<7) {
                $id_arr=Db::name('employee_calling')
                    ->group('member_id')
                    ->having('count(member_id)='.$params['param']['calling_time'])
                    ->column('member_id');

            }else{

                $id_arr=Db::name('employee_calling')
                    ->group('member_id')
                    ->having('count(member_id) > 6')
                    ->column('member_id');
            }
            $id_arr=empty($id_arr)?[0]:$id_arr;

            $where[] =['m.id', 'in', $id_arr];
        }

        //体验卡激活
        if(!empty($params['param']['experience_card_status']))
        {
            $id_arr=Db::name('preferent_code')
                ->leftJoin(['member'],'preferent_code.pre_phone=member.member_phone')
                ->group('pre_phone')
                ->column('member.id');
            $in_notin=$params['param']['experience_card_status']==1?'in':'not in';

            $where[] =['m.member_phone', $in_notin, $id_arr];
        }


        //是否参加游戏
        if(!empty($params['param']['play_game_status']))
        {
            if ($params['param']['play_game_status']<5) {
                $id_arr=Db::name('rac_log')->where('rac_status='.$params['param']['play_game_status'])->column('member_id');
                $where[]=['m.id','in',$id_arr];

            }else{
                $id_arr=Db::name('rac_log')->column('member_id');
                $where[]=['m.id','not in',$id_arr];
            }


        }

        //会员余额
        if(!empty($params['param']['member_balance']))
        {
           $member_balance= lang('member_balance')[$params['param']['member_balance']];
            if ($params['param']['member_balance']<12) {

                $where[] =['member_balance', '>', explode('-',$member_balance)[0]];
                $where[] =['member_balance', '<=', explode('-',$member_balance)[1]];
            }else{
                $where[] =['member_balance', '>', explode('-',$member_balance)[0]];
            }
            $arr=Db::name('member_mapping')->where($where)->column('member_id');
            array_push($member_arr,$arr);

        }

        //累计消费金额
        if(!empty($params['param']['cumulative_amount']))
        {
            $cumulative_amount=lang('cumulative_amount')[$params['param']['cumulative_amount']];
            $cumulative_amount_start=explode('-',$cumulative_amount)[0];
            $cumulative_amount_end=explode('-',$cumulative_amount)[1];

            if (intval($params['param']['cumulative_amount'])<8) {
                $id_arr=Db::name('log_consump')
                    ->where('log_consump_type=1')
                    ->group('member_id')
                    ->having('sum(consump_price) > '.$cumulative_amount_start.' and sum(consump_price) <'.$cumulative_amount_end)
                    ->column('member_id');
            }else{
                $cumulative_amount_start=explode('以上',$cumulative_amount)[0];
                $id_arr=Db::name('log_consump')
                    ->where('log_consump_type=1')
                    ->group('member_id')
                    ->having('sum(consump_price) > '.$cumulative_amount_start)
                    ->column('member_id');
            }
            array_push($member_arr,$id_arr);


        }
        //外呼坐席
        if(!empty($params['param']['position']))
        {
            $id_arr=Db::name('employee_calling')->where('employee_id='.$params['param']['position'])->column('member_id');

            $where[]=['m.id','in',$id_arr];
        }
        //是否购买保险
        if(!empty($params['param']['insurance_status']))
        {
            $where[] =['m.insurance_status', '=', $params["param"]['insurance_status']];
        }

        if(count($member_arr)>0){
            $where=[['id','in',$member_arr[0]]];
        }

        return $where;
    }

    /**
     * 获取评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DataList($params)
    {

        $where = empty($params['where']) ? [['m.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'm.register_time desc';
        $field = empty($params['field']) ? 'm.*,mm.*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        // 获取列表
        $data = Db::name('member')
            ->alias('m')
//            ->leftJoin(['member_car'=>'mc'],'m.id=mc.member_id')
//            ->leftJoin(['member_mapping'=>'mm'],'m.id=mm.member_id')
//            ->leftJoin(['employee_calling'=>'ec'],'m.id=ec.member_id')
//
            ->where($where)
            ->field($field)
            ->order($order);
            //->group('m.id')



        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);

            $data=$data->toArray();


        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }



        return self::DataDealWith($data);


    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $data [处理的数据]
     * @param    [array]          $params [输入参数]
     */
    public static function DataDealWith($data){

        if(!empty($data['data']))
        {

            foreach($data['data'] as &$v)
            {


                //用户昵称
//                if(empty($v['nickname']))
//                {
//                    $v['nickname']=empty($v['member_name'])?'未获取到用户昵称':$v['member_name'];
//                }

                if (isset($v['nickname'])||isset($v['member_name']))
                {


                    $v['nickname']=empty($v['member_name'])?$v['nickname']:$v['member_name'];

                    $v['nickname']=CheckBase64($v['nickname']);

                    $v['nickname']=empty($v['nickname'])?'未获取到用户昵称':$v['nickname'];
                }

                //
                $v['member_level_id']=Db::name('member_mapping')->where('member_id='.$v['id'])->value('member_level_id');
                //所在地区

                $v['total_address']=$v['member_province'].$v['member_city'].$v['member_area'];
                //用户性别
                if(isset($v['member_sex']))
                {

                    $v['member_sex_title']=BaseService::StatusHtml($v['member_sex'],lang('member_sex'),false);
                }

                //h会员等级
                $v['member_level_title']='普通用户';
                if(!empty($v['member_level_id']))
                {

                    $v['member_level_title']=BaseService::StatusHtml($v['member_level_id'],lang('member_level'),false);
//                    if (empty($v['member_level_title']))
//                    {
//
//                    }
                }
                //到店次数 呼叫次数
                if(isset($v['member_id'])&&intval($v['member_id'])>0)
                {
                    $v['arrive_times']=BaseService::DataTotal('employee_calling',[['member_id', '=', $v['member_id'] ],]);

                }
                //体验卡激活状态
                if (!empty($v['member_phone']))
                {

                    $experience_card_status=Db::name('preferent_code')->where('pre_phone='.$v['member_phone'])->value('id');
                    $v['experience_card_status_title']=empty($experience_card_status)?'未激活':'激活';
                }

                //用户手机
                if(isset($v['member_phone']))
                {
                    $v['member_phone']= substr($v['member_phone'],0,3).'***'.substr($v['member_phone'],6);

                }
                //calling_time

            }

        }

        return $data;
    }

    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function DataTotal(){

        //今日外呼次数
        $where=[
            ['employee_id','=',\session('calling')['id']],
            ['create_time','>',date('Y-m-d 00:00:00',time())],
            ['create_time','<',date('Y-m-d 23:59:00',time())]
        ];
        $call_count=Db::name('employee_calling')->where($where)->count();
        //今日通话时长
        $tell_count=Db::name('employee_calling')->where($where)->sum('duration');
        //有效通话数量
        $where=[
            ['employee_id','=',\session('calling')['id']],
            ['create_time','>',date('Y-m-d 00:00:00',time())],
            ['create_time','<',date('Y-m-d 23:59:00',time())],
            ['is_connect','=',1],
        ];
        $is_connect=Db::name('employee_calling')->where($where)->sum('duration');
        //赠送积分数量
        $giving_integral=Db::name('employee_calling')->where($where)->sum('giving_integral');
        //办理会员数量
        $handle_vip_count=0;
        //办理会员金额
        $handle_vip_price=0.00;

        $count=[
            'call_count' =>$call_count,
            'tell_count'=>$tell_count,
            'is_connect' =>$is_connect,
            'giving_integral' =>$giving_integral,
            'handle_vip_count'=>$handle_vip_count,
            'handle_vip_price'=>$handle_vip_price,
        ];
        return $count;

    }

    /**
     * 用户数据详情
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [int]          $id [用户id]
     */
    public static function  MemberDetail($id){
        # 查询用户信息、账户信息
        $field='m.id,m.member_name name,m.nickname,m.member_age age,m.member_sex sex,mm.register_time,mm.member_code ID,m.member_phone phone,
                m.member_idcard idcard,m.member_address address,ml.level_title,mm.member_create handling_time,c.channel,c.pid,
                c.biz_id,mm.member_balance balance,mm.member_integral integral,m.headimage,ml.id level_id,ml.lineup_car_limit,
                m.insurance_status,m.use_app,mm.member_level_id,ml.level_title';
        $memberInfo = Db::table('member m')
            ->join('member_mapping mm', 'm.id=mm.member_id', 'left')
            ->join('channel c', 'c.member_id=m.id', 'left')
            ->join('member_level ml', 'mm.member_level_id=ml.id', 'left')
            ->field($field)
            ->where(array('m.id' => $id))
            ->find();

        //会员等级
        $memberInfo['level_title']=empty($memberInfo['level_title'])?'普通用户':$memberInfo['level_title'];
        //用户名
        $memberInfo['name']=empty($memberInfo['name'])?$memberInfo['nickname']:$memberInfo['name'];
        //免排队车辆
        $memberInfo['lineup_car_limit']=intval($memberInfo['lineup_car_limit'])<1?'未开启免排队限制':intval($memberInfo['lineup_car_limit']);
        //账户余额
        $memberInfo['balance']=intval($memberInfo['balance'])==0 ? '0.00' :$memberInfo['balance'];
        //会员办理渠道
        $memberInfo['channel']=BaseService::StatusHtml($memberInfo['channel'],lang('member_source_type'),false);
        //升级次数
        $memberInfo['upgrade_count']=BaseService::DataTotal('log_consump',[['member_id', '=', $id ],['income_type','=',6]]);
        //充值次数
        $memberInfo['recharge_count']=BaseService::DataTotal('log_consump',[['member_id', '=', $id ],['income_type','in',[2,6,7]]]);
        //绑定车辆
        $memberInfo['member_cars']=Db::name('member_car')->where('member_id='.$id)->count('id');
        //到店次数
        $memberInfo['arrive_count']=BaseService::DataTotal('biz_traffic',[['member_id', '=', $id ]]);
        //累计消费金额
        $memberInfo['cumulative_amount']=Db::name('log_consump')->where([['member_id', '=', $id ],['log_consump_type','=',1]])->sum('consump_price');
            //BaseService::DataTotal('log_consump',[['member_id', '=', $id ],['log_consump_type','=',1]]);
        //是否激活体验卡
        $memberInfo['experience_card_status_title']='未激活';

        if (strlen($memberInfo['phone'])>1)
        {
            $experience_card_status=Db::name('preferent_code')->where('pre_phone='.$memberInfo['phone'])->value('id');
            $memberInfo['experience_card_status_title']=empty($experience_card_status)?'未激活':'激活 ';
        }

        //  呼叫次数
        $memberInfo['calling_time']=BaseService::DataTotal('employee_calling',[['member_id', '=', $id ],]);
       //是否购买保险
        $memberInfo['insurance_status_title']=BaseService::StatusHtml($memberInfo['insurance_status'],lang('yes_no_status'),false);
        //是否下载app
        $memberInfo['use_app_title']=BaseService::StatusHtml($memberInfo['use_app'],lang('yes_no_status'),false);
        // 是否参与游戏
        $rac_log=Db::name('rac_log')->where('member_id='.$id)->value('rac_status');
        $memberInfo['game_title']='未参与';

        if ($rac_log>0) {
            $memberInfo['game_title']=BaseService::StatusHtml($rac_log,[ 1=>'已参与',  2=>'进行中', 3=>'已成功', 4=>'已领奖'],false);
        }

        //是否领取红包
        $red_envelope=Db::name('redpacket_log')->alias('rl')
            ->leftJoin(['redpacket_detail'=>'rd'],'')
            ->where('rl.member_id='.$id)
            ->count();
        $memberInfo['red_envelope_title']=$red_envelope<1 ?'未领取':'已领取';

        $memberInfo['call_status']=CallService::Callstatus($id);

        $memberInfo['phone']= substr($memberInfo['phone'],0,3).'***'.substr($memberInfo['phone'],6);
        return $memberInfo;
    }

    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceListWhere($params = [])
    {
       // $where = [['o.order_type','=',2]];

        //member_id
        if(!empty($params['param']['member_id']))
        {
            $where[] =['o.member_id', '=',  $params["param"]['member_id']];
        }
        if(!empty($params['member_id']))
        {
            $where[] =['o.member_id', '=',  $params['member_id']];
        }
        //支付方式
        if(!empty($params['param']['cash_type']))
        {
            $where[] =['o.pay_type', '=',  $params["param"]['cash_type']];
        }
//        dump(input());exit;
        //是否使用卡券
        if(!empty($params['param']['use_voucher']))
        {
            if ($params['param']['use_voucher']==1) {
                $where[] =['o.voucher_price', '>',  0];
            }else{
                $where[] =['o.voucher_price', '=',  0];
            }

        }
        //评分 1好评 2中评 3差评
        if(!empty($params['param']['comment_level'])&&intval($params['param']['comment_level'])>0)
        {
            switch ($params['param']['comment_level']){
                case 1:
                    $where[] =['es.score', '>',  3];
                    break;
                case 2:
                    $where[] =['es.score', '=',  3];
                    break;
                case 3:
                    $where[] =['es.score', '<',  3];
                    break;
            }
        }

        return $where;
    }

    /**
     * 用户服务
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function ServiceDataList($where){

        //门店名称 日期时间 车牌 服务名称 消费金额 是否使用卡卷 实际支付 支付方式 用户评价
        $field='s.class_pid,s.service_class_id,o.pay_create,o.car_liences,o.biz_id,
                o.order_price,o.pay_type,o.pay_price,s.service_title,os.coupon_price,es.score';
        $data=Db::name('order_server')->alias('os')
            ->leftJoin(['orders'=>'o'],'os.id=o.id')
           ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
//            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->group('os.id')
           ->field($field)
            ->where($where)
           // ->select();

            ->paginate(10, false, ['query' => request()->param()]);
        $data=$data->toArray();

        return self::ServiceDataDealWith($data);

    }

    /**
     * 用户服务数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     * @param    [array]          $where [查询条件]
     */

    public static function ServiceDataDealWith($data)
    {
        if(!empty($data['data']))
        {

            foreach($data['data'] as &$v)
            {
                //是否使用卡卷
                if (isset($v['coupon_price']))
                {
                    $v['voucher_title'] =$v['coupon_price']>0?'是':'否';
                }
                //支付方式
                if (isset($v['pay_type']))
                {
                    $v['pay_type_title'] =BaseService::StatusHtml($v['pay_type'],lang('cash_type'),false);
                }
                //用户评价
                if (isset($v['score']))
                {
                    $v['score_title'] ='中评';
                    if ($v['score']>3) {
                        $v['score_title'] ='好评';
                    }elseif ($v['score']<3){
                        $v['score_title'] ='差评';
                    }else{
                        $v['score_title'] ='未评论';
                    }
                }
                //门店名称
                if (isset($v['biz_id']))
                {
                    $v['biz_title']=Db::name('biz')->where('id='.$v['biz_id'])->value('biz_title');
                    $v['biz_title']=empty($v['biz_title'])?'未获取到门店名称':$v['biz_title'];
                }


            }


        }

        return $data;
    }

    /**
     * 用户服务总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function ServiceDataTotal($where){
        return (int)  Db::name('orders')->alias('o')
            ->leftJoin(['order_server'=>'os'],'os.id=o.id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->where($where)
            ->count();
    }

    /**
     * 用户服务根据分类划分总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function ServiceByCateDataTotal($where){
        //汽车美容


//        $one= Db::name('orders')->alias('o')
//            ->leftJoin(['order_server'=>'os'],'os.id=o.id')
//            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
//
//            ->where($where)
//            ->where([['s.service_pid','=',2]])
//            ->count();



        $where_one=$where;
        $where_one[]=[
            ['s.class_pid','=',2]
        ];
        $one=Db::name('orders')->alias('o')
            ->leftJoin(['order_server'=>'os'],'os.id=o.id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->where($where_one)
            ->group('os.id')
            ->count();


        //汽车装饰
        $where_two=$where;
        $where_two[]=[
            ['s.class_pid','=',3]
        ];
        $two= Db::name('orders')->alias('o')
            ->leftJoin(['order_server'=>'os'],'os.id=o.id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->group('os.id')
            ->where($where_two)
            ->count();

        //维护养护
        $where_three=$where;
        $where_three[]=[
            ['s.class_pid','=',4]
        ];
        $three= Db::name('orders')->alias('o')
            ->leftJoin(['order_server'=>'os'],'os.id=o.id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->group('os.id')
            ->where($where_three)
            ->count();

        //钣金喷漆
        $where_four=$where;
        $where_four[]=[
            ['s.class_pid','=',1]
        ];
        $four= Db::name('order_server')->alias('os')
            ->leftJoin(['orders'=>'o'],'os.id=o.id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->group('os.id')
            ->where($where_four)
            ->count();
        $re=['mr'=>$one,'zs'=>$two,'yh'=>$three,'pq'=>$four];

        return $re;
    }

    /**
     * 用户车辆数据列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [int]          $id [用户id]
     */

    public static function MemberCarData($id){
        $field='mc.queue_status,mc.id,mc.car_licens,cl.title logoTitle,cs.sort_title sortTitle,
                clv.level_title levelTitle,car_mileage mileage,create_time,car_recogin_code code,
                car_engine_number number,
                (select order_create from orders o where o.car_liences=mc.car_licens order by o.id desc limit 1) latest_arrival_time
                ';
        $carInfo = Db::table('member_car mc')
            ->field($field)
            ->join('car_sort cs', 'mc.car_type_id=cs.id', 'left')
            ->join('car_logo cl', 'cs.logo_id=cl.id', 'left')
            ->join('car_level clv', 'clv.id=cs.level', 'left')
            ->where(array('member_id' => $id))
            ->where('status !=3')
            ->paginate(10, false, ['query' => request()->param()]);
        $carInfo=$carInfo->toArray();
        return self::CarDataDealWith($carInfo);
    }


    /**
     * 用户车辆数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     * @param    [array]          $where [查询条件]
     */

    public static function CarDataDealWith($data)
    {
        if(!empty($data['data']))
        {
            //queue_status_title order_num
            foreach($data['data'] as &$v)
            {
                //是否免排队
                if (isset($v['queue_status']))
                {
                    $v['queue_status_title'] =BaseService::StatusHtml($v['queue_status'],lang('yes_no_status'),false);
                }

                //车辆公里数
                if (!isset($v['mileage']))
                {
                    $v['mileage'] = '未获取到数据';
                }
                //服务次数
                if (isset($v['car_licens']))
                {
                   $v['order_num']= Db::name('order_server')->alias('os')
                        ->leftJoin(['orders'=>'o'],'os.order_number=o.order_number')
                        ->leftJoin(['service'=>'s'],'s.id=os.server_id')
                        ->group('os.id')
                        ->where([['o.car_liences','=',$v['car_licens']]])
                        ->count('os.id');
                }

            }

            //dump($data);exit;
        }

        return $data;
    }


    /**
     * 用户充值列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日10:29:24
     * @param    [array]          $params [输入参数]
     */
    public static function  ConsumpListWhere($params = [])
    {
        $where = [];

        //时间段
        if(!empty($params['param']['start_time']))
        {
            $where[] =['consump_time', '>=',  $params["param"]['start_time'] ];
        }
        if(!empty($params['param']['end_time']))
        {
            $where[] =['consump_time', '<=',  $params["param"]['end_time'] ];
        }
        if(!empty($params['param']['consump_time']))
        {
            $time=explode(' to ',$params['param']['consump_time']);
            $where[] =['consump_time', '>=',  $time[0] ];
            $where[] =['consump_time', '<=',  $time[1]];
        }
        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['member_id', '=',  $params['member_id'] ];
        }
        //income_type
        if(!empty($params['param']['income_type']))
        {
            $where[] =['income_type', '=',  $params['param']['income_type'] ];
        }


        return $where;
    }

    /**
     * 用户充值数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function ConsumpDataDealWith($data){
        if(!empty($data))
        {

            foreach($data as &$v)
            {
                // 充值类型
                if(isset($v['income_type']))
                {
                    $v['income_type_title'] = BaseService::StatusHtml($v['income_type'],lang('member_fee_aim'),false);
                }
                //金额
                if(isset($v['consump_price']))
                {
                    $v['consump_price'] = priceFormat($v['consump_price']);
                }
                //充值渠道
                if(isset($v['consump_type']))
                {
                    if($v['consump_type'] == 1){
                        $v['consump_type'] = '线上支付';

                    }else{
                        $v['consump_type'] = '门店支付';
                        #查询门店名称
                        $v['biz_title'] = Db::table('biz')->where(array('id'=>$v['biz_id']))->value('biz_title');
                        if(empty($v['biz_title'])){
                            $v['biz_title'] ='无';
                        }

                    }
                }
                //门店名称
                if(isset($v['biz_id'])){
                    $v['biz_title'] = Db::table('biz')->where(array('id'=>$v['biz_id']))->value('biz_title');
                }
                //支付方式
                if(isset($v['consump_pay_type']))
                {

                    $v['consump_pay_type_title']=BaseService::StatusHtml($v['consump_pay_type'],lang('cash_type'),false);
                }


            }
        }

        return $data;
    }


    /**
     * 用户卡券列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日16:12:55
     * @param    [array]          $params [输入参数]
     */
    public static function VoucherListWhere($params = [])
    {
        $where = [];

        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['mv.member_id', '=',  $params['member_id'] ];
        }
        //时间段
        if(!empty($params['param']['start_time']))
        {
            $where[] =['start_time', '>=',  $params["param"]['start_time'] ];
        }
        if(!empty($params['param']['end_time']))
        {
            $where[] =['start_time', '<=',  $params["param"]['end_time'] ];
        }
        return $where;
    }


    /**
     * 获取用户卡券列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function VoucherDataList($params)
    {

        $where = empty($params['where']) ? [['mv.id','>',0]] : $params['where'];
        $field = empty($params['field']) ? '*' : $params['field'];


        // 获取列表
        $data = Db::name('member_voucher mv')

            ->where($where)->field($field)->order('start_time desc');


        $data=$data->paginate(10, false, ['query' => request()->param()]);




        return self::VoucherDataDealWith($data->toArray());


    }

    /**
     * 用户卡券数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function VoucherDataDealWith($data){
        if(!empty($data['data']))
        {

            foreach($data['data'] as &$v)
            {
                if (empty($v['card_title']))
                {
                    $v['card_title'] = '未获取卡券名称';
                }

                // 卡卷来源
                if(isset($v['voucher_source']))
                {
                    $v['voucher_source_title'] = BaseService::StatusHtml($v['voucher_source'],lang('voucher_source'),false);
                }
                if ($v['merchants_id']>0) {
                    $v['card_type_title']='商户卡券';
                }elseif($v['redpacket_id']>0){
                    $v['card_type_title']='红包卡券';
                }elseif($v['voucher_id']>0){
                    $type_arr=[
                    1=>'商品通用抵值券',
                    2=>'服务通用抵值券',
                    3=>'单独商品抵值券',
                    4=>'单独服务抵值券',
                    5=>'商品服务通用抵值券',
                    6=>'现金通用券',
                ];
                $card_type = Db::name('card_voucher')->where('id='.$v['voucher_id'])->value('card_type_id');
                $v['card_type_title']=BaseService::StatusHtml($card_type,[1=>'服务卡券',2=>'现金抵用卡券',3=>'商品卡券'],false);
                $v['card_type_title']=empty($v['card_type_title'])?'未获取卡券类型':$v['card_type_title'];
                }


            }
        }

        return $data;
    }
    /**
     * 修改用户积分
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function SaveMemberIntegral($member_id){
        $id=Db::name('member_mapping')->where('member_id='.$member_id)->value('id');
        $sysset_call=Db::name('sysset')->where(['tagname'=>'call_set',])->value('desc');
        $sysset_call=json_decode($sysset_call,true);
        $where=[
            ['member_id','=',$member_id],
            ['integral_number','=',$sysset_call['give_integral']],
            ['integral_source','=',19],
            ['integral_type','=',1],
            ['integral_time','>',date('Y-m-d 00:00:00',time())],
            ['integral_type','<',TIMESTAMP],
        ];
        $log=Db::name('log_integral')->where($where)->value('id');
        if ($log>0) {
            return DataReturn('已赠送积分',-1);
        }

        if ($id>0) {
            Db::startTrans();
            $re=Db::name('member_mapping')->where('member_id='.$member_id)->setInc('member_integral',500);
            if (!$re)
            {
                Db::rollback();
                throw new \BaseExceptionBaseException(['code'=>403 ,'errorCode'=>14001,'msg'=>'修改余额或积分失败','status'=>false,'data'=>null]);
            }
            $data=[
                'member_id'=>$member_id,
                'integral_number'=>$sysset_call['give_integral'],
                'integral_source'=>19,
                'integral_type'=>1,
                'integral_time'=>TIMESTAMP,
            ];
            $re=Db::name('log_integral')->insertGetId($data);
            if (!$re)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>14002,'msg'=>'添加用户积分记录失败','status'=>false,'data'=>null]);
            }
            Db::commit();
        }else{
            return DataReturn('未获取到用户积分信息',-1);
        }
        return DataReturn('ok',0);
    }


    /**
     * 用户呼叫列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日16:12:55
     * @param    [array]          $params [输入参数]
     */
    public static function CallingListWhere($params = [])
    {
        $where = [];

        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['member_id', '=',  $params['member_id'] ];
        }
        //时间段
        if(!empty($params['param']['create_time']))
        {

            $time=explode(' to ',$params['param']['create_time']);
            $where[] =['create_time', '>=',  $time[0] ];
            $where[] =['create_time', '<=',  $time[1] ];
        }

        return $where;
    }

    /**
     * 用户呼叫列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日16:12:55
     * @param    [array]          $params [输入参数]
     */
    public static function CallingDataList($params = [])
    {
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        $data = Db::name('employee_calling')->where($where)->field($field)->order($order)->group($group);


        // 获取列表

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }
        foreach ($data['data'] as &$v)
        {
            $v['is_connect_title']=$v['is_connect']==1?'接通':'未接通';
            $v['action']='办理会员';
            $v['action_num']='金卡会员';
        }
        return $data;
    }
    /**
     * 获取图片
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日16:12:55
     * @param    [array]          $params [输入参数]
     */
    public static function showimgsrc($params){
        $member_id=$params['member_id'];

        $img=Db::name('employee_memberqrcode')->where($params)->find();
        if (!empty($img))
        {
         return DataReturn('ok',0,['src'=>$img['img']]);
        }

        $member_phone=Db::name('member')->where('id='.$member_id)->value('member_phone');


        //显示文字
        $txt4 = substr($member_phone,0,3).'***'.substr($member_phone,6);

        //建立图象
        $image = imagecreatefrompng ( ROOT_PATH."/static/img/employee_qrcode.png" );

        //定义颜色
        $white = ImageColorAllocate ( $image, 255, 255, 255 );
        //写入文字
        imagettftext ( $image, 33, 0, 125, 320, $white, '/usr/share/fonts/wqy-zenhei/wqy-zenhei.ttc', $txt4 );

        if ($params['img_tag']=='handle_card') {
            $url='/takemember/index?assignBizId=0&from=calling&employeeId='.session('calling')['id'].'&member_id='.$member_id;
        }else{
            $url='http://mobile.busypi.com/#/upgrade&from=calling&employee_id='.session('calling')['id'].'&member_id='.$member_id;
        }
        $url=urlencode($url);
        $img_name=$member_id.session('calling')['id'].$params['img_tag'].'qrcode';

        $qrcode = self::scerweima1($url,$img_name,9.5);

        $srcqrcode = imagecreatefromstring(file_get_contents($qrcode));

        list($src_w, $src_h) = getimagesize($qrcode);
        imagecopymerge($image, $srcqrcode, 100, 400, 0, 0, $src_w, $src_h, 90);


        imagepng ( $image, ROOT_PATH."/upload/employee/".$member_id.session('calling')['id'].$params['img_tag'].".png" );

        $oss=new OssService();
        $re=$oss->OssUpload("/upload/employee/".$member_id.session('calling')['id'].$params['img_tag'].".png");
        $params['img']=$re;
        Db::name('employee_memberqrcode')->insertGetId($params);
        unlink($qrcode);
        unlink(ROOT_PATH."/upload/employee/".$member_id.session('calling')['id'].$params['img_tag'].".png");
        //删除二维码 删除本地图片
        return DataReturn('ok',0,['src'=>$re]);

    }


    /**
     * @param string $url
     * @param $agentId
     * @param int $matrixPointSize 生成图片大小
     * @param $logoMark
     * @param $type
     * @return string
     * @content 带logo二维码
     */
    function scerweima1($url = '', $img_name, $matrixPointSize = 4, $logoMark = false, $type = '')
    {
        $filename = ROOT_PATH .'/upload/employee/'.$img_name.'.png' ;
        $errorCorrectionLevel = 'L';//纠错级别：L、M、Q、H
        /*
        $value = $url;         //二维码内容
        $errorCorrectionLevel = 'H';  //容错级别
        //生成二维码图片
        $filename = ROOT_PATH .'/upload/employee/'.$img_name.'.png' ;


        if (!file_exists($filename)) {
            QRcode::png($value, $filename, $errorCorrectionLevel,
                $matrixPointSize, 0);

            $QR = imagecreatefromstring(file_get_contents( $filename ));
            //输出图片
            imagepng($QR, $filename);
            imagedestroy($QR);
        }

        return $filename;
        */

        if (!file_exists($filename))
        {
            QRcode::png ( $url, $filename, $errorCorrectionLevel, $matrixPointSize, 0 );
            $logo = ROOT_PATH .'/static/img/logo_qrcode.png';
            $QR = imagecreatefromstring ( file_get_contents ( $filename ) );
            $logo = imagecreatefromstring ( file_get_contents ( $logo ) );
            $QR_width = imagesx ( $QR );
            $logo_width = imagesx ( $logo );
            $logo_height = imagesy ( $logo );
            $logo_qr_width = $QR_width / 5;
            $scale = $logo_width / $logo_qr_width;
            $logo_qr_height = $logo_height / $scale;
            $from_width = ($QR_width - $logo_qr_width) / 2;
            imagecopyresampled ( $QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height );

            imagepng ( $QR, $filename );//带Logo二维码的文件名
            imagedestroy($QR);
        }
        return $filename;
    }
}