<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/27 0027
 * Time: 14:47
 */

namespace app\calling\service;
use AlibabaCloud\SDK\OSS\OSS\GetBucketWebsiteResponse\websiteConfiguration\routingRules\routingRule\redirect;
use think\Db;
use think\facade\Session;


/**
 * 外呼专员登录服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 */
class AdService
{
    /**
     * 外呼元登录
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     * @param    [array]          $params [输入参数]
     */
    public static function Login($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '用户名不能为空',
                'error_code'         => 16001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'password',
                'error_msg'         => '密码不能为空',
                'error_code'         => 16002,
            ],
            [
                'checked_type'      => 'fun',
                'key_name'          => 'username',
                'checked_data'      => 'CheckUserName',
                'error_msg'         => '用户名格式 5~18 个字符（可以是字母数字下划线）',
                'error_code'         => 16003,
            ],
            [
                'checked_type'      => 'fun',
                'key_name'          => 'password',
                'checked_data'      => 'CheckLoginPwd',
                'error_msg'         => '密码格式 6~18 个字符',
                'error_code'         => 16004,
            ],
        ];
        $ret = ParamsChecked($params, $p);
        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

        }

        // 获取管理员
        $calling = Db::name('employee_sal')->field('id,calling_password,calling_account,phone_code')
            ->where(['calling_account'=>$params['username']])->find();
       // dump($calling);exit;
        if(empty($calling))
        {

            throw new \BaseException(['code'=>403 ,'errorCode'=>23001,'msg'=>'数据不存在','status'=>false,'debug'=>false]);
        }

        // 密码校验

        if($params['password'] != $calling['calling_password'])
        {

            throw new \BaseException(['code'=>403 ,'errorCode'=>23002,'msg'=>'密码错误','status'=>false,'debug'=>false]);
        }

        // 校验成功
        // session存储

        session('calling', $calling);

        // 返回数据,更新数据库
        if(session('calling') != null)
        {

            return DataReturn('登录成功',0);
        }

        // 失败
        session('calling', null);
        throw new \BaseException(['code'=>403 ,'errorCode'=>23003,'msg'=>'登录失败，请稍后再试！','status'=>false,'debug'=>false]);

    }
}