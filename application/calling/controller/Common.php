<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/23
 * Time: 18:58
 */

namespace app\calling\controller;


use app\service\OssService;
use app\service\ResourceService;
use think\Controller;
use think\Db;
use think\facade\Request;

class Common extends Controller
{
    protected $data_post;
    protected $data_request;

    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年9月30日09:30:32
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
        $this->data_post = input('post.');
        $this->data_request = input();

        // 视图初始化
        $this->ViewInit();
        //权限初始化
        $this->IsPower();


    }

    /**
     * [IsLogin 登录校验]
     * @author   Devil
     * @blog     http://gong.gg/
     * @version  0.0.1
     * @datetime 2016-12-03T12:42:35+0800
     */
    protected function IsLogin()
    {

        if(session('calling') === null)
        {
            if(IS_AJAX)
            {

                exit(json_encode(DataReturn('登录失效，请重新登录', -400)));
            } else {

                die('<script type="text/javascript">if(self.frameElement && self.frameElement.tagName == "IFRAME"){parent.location.reload();}else{window.location.href="'.url('calling/Admin/LoginInfo').'";}</script>');
            }
        }
    }


    protected function IsPower()
    {
        $calling=session('calling');

        $this->assign('calling',$calling);
    }


    /**
     * 上传文件方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年7月2日10:47:45
     */
    public function uploadfile(){
        $params=Request::param();
        $src=ResourceService::uploadFile($params);
        return json($src);
    }



    /**
     * [ViewInit 视图初始化]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年7月2日10:47:45
     */
    public function ViewInit()
    {
        // 当前操作名称
        $module_name = strtolower(request()->module());

        $controller_name = strtolower(request()->controller());
        $action_name = strtolower(request()->action());
        // 控制器静态文件状态css,js
//        $module_css = $module_name.DS.'css'.DS.$controller_name;
//        $module_css .= file_exists(ROOT_PATH.'static'.DS.$module_css.'.'.$action_name.'.css') ? '.'.$action_name.'.css' : '.css';
//        $this->assign('module_css', file_exists(ROOT_PATH.'static'.DS.$module_css) ? $module_css : '');

        $module_js = 'js'.DS.'module'.DS.$module_name.DS.$controller_name;
        $module_js .= file_exists(ROOT_PATH.'static'.DS.$module_js.DS.$action_name.'.js') ? DS.$action_name.'.js' : '.js';
        $this->assign('module_js', file_exists(ROOT_PATH.'static'.DS.$module_js) ? $module_js : '');
        $this->assign('level3',4);
        $this->assign('level4',8);


    }

    /**
     * 处理多余图片
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年7月2日10:47:45
     */
    public function DelRemainder($in_use,$in_all){


        if(strlen($in_use[0])<1)
        {
            unset($in_use[0]);
        }
        if(strlen($in_all[0])<1)
        {
            unset($in_all[0]);
        }

        foreach ($in_all as $k=>$v){

            if(in_array($v,$in_use))
            {
                unset($in_all[$k]);
            }
        }

        if(count($in_all)>0)
        {
            foreach ($in_all as $v){
                ## 从缓存中删除
                ResourceService::delCacheItem($v)  ;
                ## 从路径中删除
                ResourceService::delFile($v);
            }
            return json(DataReturn('删除成功',0));
        }else{
            //throw new \BaseException(array("code"=>403,"msg"=>"数据格式错误","errorcode"=>00000,"status"=>false));
            return json(DataReturn('未发现多余图片',0));
        }

    }

}