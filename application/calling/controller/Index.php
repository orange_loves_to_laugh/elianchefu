<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/11
 * Time: 18:32
 */

namespace app\calling\controller;


use app\calling\service\MemberService;
use app\service\BaseService;
use app\service\ResourceService;

use app\service\StatisticalService;
use app\service\StatisticMemeberService;
use base\Excel;

use think\Controller;
use think\Db;
use think\facade\Session;
use think\Loader;

class Index extends Common
{
    /**
     * 构造方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月24日13:23:09
     */
    public function __construct()
    {
        // 调用父类前置方法
        parent::__construct();

        // 登录校验

        $this->IsLogin();

    }



    /**
     * [Index 用户列表]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
   public function index()
   {

       if (input('get.action')=='ajax') {



           $where=MemberService::ListWhere(input());

           $data_params = array(
               'page'         => true,
               'number'         => 10,
               'where'       => $where,
               'order'       =>'m.register_time desc',
               'field'       =>
                   'm.*,m.id member_id'
//                   'm.member_name,m.calling_time,mm.member_id,m.member_sex,m.member_phone,
//                   m.nickname,m.headimage,member_province,member_city,member_area,member_level_id,
//                   m.register_time',
           );


           $data = MemberService::DataList($data_params);
           $count=MemberService::DataTotal();

           return ['code' => 0, 'msg' => '','count'=>$count,  'data' => $data];
       }else{
           //search_field
           $this->assign('search_field',lang('search_field'));

           $this->assign('member_level',lang('member_level'));
           $this->assign('calling_time',lang('calling_time'));
           $this->assign('play_game_status',lang('play_game_status'));
           $this->assign('member_balance',lang('member_balance'));
           $this->assign('cumulative_amount',lang('cumulative_amount'));
           $this->assign('position_arr',Db::name('employee_sal')->where('calling_status=1')->field('id,employee_name')->select());
           return $this->fetch();
       }

   }
    /**
     * [MemberDetail 用户详情]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
   public function MemberDetail()
   {


       $sysset_call=Db::name('sysset')->where(['tagname'=>'call_set',])->value('desc');

       $id = input('get.id');
       if (!empty($id)) {
           $memberInfo=MemberService::MemberDetail($id);

           $this->assign('sysset_call', json_decode($sysset_call,true));
           $this->assign('info', $memberInfo);

           return $this->fetch();
       } else {
           echo("<script>history.back();</script>");
       }
   }

    /**
     * [MemberServiceLog 用户服务记录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
    public function MemberServiceLog(){

        if (input('get.action')=='ajax') {

            $where= MemberService::ServiceListWhere(input());


            $info = MemberService::ServiceDataList($where);

          //  $total=MemberService::DataTotal($where);

            $other_total=MemberService::ServiceByCateDataTotal($where);

            return ['code' => 0, 'msg' => '', 'count' => $info['total'], 'data' => $info,'other_total'=>$other_total];
        }else{

            $this->assign('cash_type',lang('cash_type'));
            return $this->fetch();
        }
    }

    /**
     * [MemberCarLog 用户服务记录]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年12月29日13:31:39
     */
    public function MemberCarLog(){
        if (input('get.action')=='ajax') {


            $info = MemberService::MemberCarData(input("member_id"));



            return ['code' => 0, 'msg' => '',  'data' => $info,];
        }else{


            return $this->fetch();
        }
    }

    /**
     * [MemberRechargeLog 充值记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function MemberRechargeLog(){

        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where =  MemberService::ConsumpListWhere($params);
            $where[]=['income_type','in',[2,6,7]];
            // dump($where);exit;
            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                'table'     =>'log_consump',
                'order'     =>'consump_time desc'
            );
            $data = BaseService::DataList($data_params);

            $data=MemberService::ConsumpDataDealWith($data);
            $total = BaseService::DataTotal('log_consump',$where);
//            dump($total);
//            dump($where);
//            exit;
            return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
        }else{
            return $this->fetch();
        }
    }


    /**
     * [MemberVoucher 用户卡券]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function MemberVoucher(){
            $memberId = input("get.memberId");

            if (input('get.action')=='ajax') {
                $params = input();

                // 条件
                $where =  MemberService::VoucherListWhere($params);
                $where[]=['mv.status','=',1];

                $data_params = array(
                    'page'         => true,
                    'number'         => 10,
                    'where'     => $where,
                    'field'     =>'merchants_id,voucher_type,card_title,id,voucher_id,start_time,voucher_cost,card_time,create,voucher_source'
                );
                $data = MemberService::VoucherDataList($data_params);

                return ['code' => 0, 'msg' => '',  'data' => $data];
            }else{
                $this->assign('memberId',$memberId);
                return $this->fetch();
            }
        }

    /**
     * [MemberGivingIntegral 用户卡券]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function MemberGivingIntegral(){
            $member_id=input('member_id');
           return MemberService::SaveMemberIntegral($member_id);
    }
    /**
     * [MemberCallLog 用户的通话记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function MemberCallLog(){
        $member_id=input('member_id');
        if (input('get.action')=='ajax') {
            $params = input();

            // 条件
            $where =  MemberService::CallingListWhere($params);

            $data_params = array(
                'page'         => true,
                'number'         => 10,
                'where'     => $where,
                 'table'    =>'employee_calling',
            );
            $data = MemberService::CallingDataList($data_params);

            return ['code' => 0, 'msg' => '',  'data' => $data];
        }else{

            return $this->fetch();
        }
    }

    /**
     * [MemberQrcode 办理会员]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function MemberQrcode(){
        $param=[
            'member_id'=>input('member_id'),
            'employee_id'=>\session('calling')['id'],
            'img_tag'=>'handle_card'
                //input('img_tag'),
        ];
        return MemberService::showimgsrc($param);
    }
    /**
     * [UpgradeQrcode 升级充值]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月17日16:55:34
     */
    public function UpgradeQrcode(){
        $param=[
            'member_id'=>8085,
            'employee_id'=>\session('calling')['id'],
            //input('memberId'),
            'img_tag'=>'upgrade'
            //input('img_tag'),
        ];
        return MemberService::showimgsrc($param);
    }

}