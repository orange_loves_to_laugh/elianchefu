<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/11 0011
 * Time: 20:03
 */

namespace app\calling\controller;

use app\calling\service\CallService;
use Redis\Redis;
use think\Controller;
use think\Db;
use think\facade\Request;
use think\facade\Session;

class Yuncall extends Controller
{
    private $pass=null;
    private $redis=null;
    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年9月30日09:30:32
     * @desc    description
     */
    public function __construct()
    {
        parent::__construct();
        $this->pass=$this-> http_get('http://cc501.wei-fu.cn/webAPI/pe.php?pass=test2020&key=test20200908');
        $this->redis=new Redis();

    }

    function http_get($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $result = curl_exec($ch);
        return $result;
    }
    /**
     * [Docall 执行呼叫]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function Docall(){
       //$this->redis->hDel('callinfo','1590121783.88');
       //return DataReturn('ok',0,['status'=>true,'uniquid'=>1590121783.88]);
        $member_id=input('member_id');
        $member_phone= Db::name('member')->where('id='.$member_id)->value('member_phone');
        if (empty($member_phone))
        {
            return DataReturn('无法获取到用户手机号码',-1);
        }

        $re=CallService::Callstatus($member_id);
        if ($re=='called') {
            return DataReturn('用户当日以完成呼叫',-1);
        }
        if ($re=='top') {
            return DataReturn('用户当日呼叫次数已达上限',-1);
        }

       //dump( \session('calling')['phone_code']);exit;
        $data['userName'] = 'test';
        $data['passWord'] = $this->pass;
        $data['model'] = 'OnClickCallUniqueid';
        $data['exten'] = \session('calling')['phone_code'];
        $data['toTel'] = $member_phone;
        $data['actionId'] = 'test';//OnClickCallLocalUniqueid

        $url = 'http://cc501.wei-fu.cn/webAPI/webHttpAPI.php?'.http_build_query($data);

        $result =$this->http_get($url);

        return DataReturn('ok',0,json_decode($result,true));
        //return DataReturn('ok',0,$result);

    }
    /**
     * [CallLog 查询通话记录]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function CallLog(){

        $uid=input('uid');
        $data['userName'] = 'test';
        $data['passWord'] = $this->pass;
        $data['model'] = 'getRecodeByUID';
        $data['date']=date("Y-m-d",time());
        $data['uid'] = $uid;
            //'1610369320.5201';
        //1610445755.52528 2021-01-12
        $url = 'http://cc501.wei-fu.cn/webAPI/webHttpAPI.php?'.http_build_query($data);
        $result =$this->http_get($url);
        return DataReturn('ok',0,json_decode($result));
    }


    /**
     * [AnswerPhone 接听回调]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function AnswerPhone(){
        //$re=Db::name('aaa')->where('id=167')->value('info');

       $this->redis->hSet('callinfo',Request::post()['uniqueid'],json_encode(Request::post()));

       return DataReturn('ok',0);

    }

    /**
     * [HangupPhone 挂断回调]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function HangupPhone(){

        $response=Request::post();

         $this->redis->hSet('callinfo',$response['uniqueid'],json_encode($response));
        $member_id=Db::name('member')->where('member_phone='.$response['dst'])->value('id');
        $sysset_call=Db::name('sysset')->where(['tagname'=>'call_set',])->value('desc');
        $sysset_call=json_decode($sysset_call,true);
        $data=[
            'member_id'=>$member_id,
            'duration'=>$response['billsec'],
            'is_connect'=>$response['disposition']=='ANSWERED'?1:2,
            'giving_integral'=>$response['billsec']>=$sysset_call['billsec']?$sysset_call['give_integral']:0,
            'employee_id'=>Db::name('employee_sal')->where('phone_code='.$response['src'])->value('id'),
            'phone_code'=>$response['src'],
        ];
        Db::name('employee_calling')->insertGetId($data);
        return DataReturn('ok',0);



    }
    /**
     * [GetPhoneInfoByUid 根据uid获取接听状态]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function GetPhoneInfoByUid(){

        $uniqueid=input('uniqueid');
        //$this->redis->hDel('callinfo',$uniqueid);
        $phone_info=$this->redis->hGet('callinfo',$uniqueid);

        if (empty(json_decode($phone_info,true))) {
            return DataReturn('no',-1);
        }else{
            //
            $re=json_decode($phone_info,true);
            if (isset($re['disposition']))
            {
                $this->redis->hDel('callinfo',$uniqueid);
            }
            return DataReturn('ok',0,json_decode($phone_info,true));
        }
    }

    /**
     * [Stopcall 挂断电话]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月12日16:09:04
     */
    public function Stopcall(){

        $data['userName'] = 'test';
        $data['passWord'] = $this->pass;
        $data['model'] = 'hangUp';
        $data['agent']=session('calling')['phone_code'];
        $url = 'http://cc501.wei-fu.cn/webAPI/webHttpAPI.php?'.http_build_query($data);
        $result =$this->http_get($url);
        return json_decode($result,true);
    }

    public function test(){
        //单条通话记录
        /*
         *
         */
        $data['userName'] = 'test';
        $data['passWord'] = $this->pass;
        $data['model'] = 'getRecodeByUID';
        $data['uid'] = '1610369320.5201';
        $data['date'] = '2021-01-11';
        $url = 'http://cc501.wei-fu.cn/webAPI/webHttpAPI.php?'.http_build_query($data);
        $result =$this->http_get($url);
        dump(json_decode($result));exit;
        /*
        //普通点击呼叫同步接听(返回uniqueid)
        $data['userName'] = 'test';//调用的方法
        $data['passWord'] = $this->pass;//密码

        $data['model'] = 'OnClickCallUniqueid';//调用的方法
        $data['exten'] = '8001';//坐席的分机
        $data['toTel'] = 13080745917;//要呼叫的号码
        $url = 'http://cc501.wei-fu.cn/webAPI/webHttpAPI.php?'.http_build_query($data);
        $result =$this->http_get($url);
        */
        //1610369320.5201

        //点击呼叫同步接听（手机回拨返回uniqueid）

    }
}