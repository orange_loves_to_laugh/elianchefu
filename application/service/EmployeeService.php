<?php


namespace app\service;

use Redis\Redis;
use think\Db;

/**
 * 员工管理服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年12月16日11:47:57
 */
class EmployeeService
{


    /**
     * 岗位列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function StationListWhere($params = [])
    {
        $where = [['is_del', '=', 2]];


        //一级部门
        if (!empty($params['param']['department_id'])) {
            $where[] = ['department_id', '=', $params["param"]['department_id']];
        }
        if (!empty($params['param']['higher_department_id'])) {
            $where[] = ['department_id', '=', $params["param"]['higher_department_id']];
        }


        //二级部门
        if (!empty($params['param']['department_secid'])) {
            $where[] = ['department_secid', '=', $params["param"]['department_secid']];
        }


        //岗位
        if (!empty($params['param']['id'])) {
            $where[] = ['id', '=', $params["param"]['id']];
        }
        return $where;
    }

    /**
     * 获取岗位列表数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function StationDataList($params)
    {
        $data = BaseService::DataList($params);
        return self::StationDataDealWith($data);
    }

    /**
     * 岗位数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function StationDataDealWith($data)
    {
        if (!empty($data)) {

            foreach ($data as &$v) {
                //一级部门
                if (isset($v['department_id']) && $v['department_id'] > 0) {
                    $v['department_title'] = Db::name('employee_department')->where('id=' . $v['department_id'])->value('title');
                }
                //二级部门
                $v['department_sectitle'] = '未选择';
                if (isset($v['department_secid']) && $v['department_secid'] > 0) {
                    $v['department_sectitle'] = Db::name('employee_department')->where('id=' . $v['department_secid'])->value('title');
                }
                $v['department_title'] = $v['department_title'] . '-' . $v['department_sectitle'];
                //是否参与考核
                if (isset($v['is_check'])) {

                    $v['is_check_title'] = BaseService::StatusHtml($v['is_check'], lang('yes_no_status'), false);
                }

            }

        }


        return $data;
    }

    /**
     * 岗位保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function StationDataSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'title',
                'error_msg' => '名称不能为空',
                'error_code' => 19002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'department_id',
                'error_msg' => '请选择所属部门',
                'error_code' => 19003,
            ],
//            [
//                'checked_type'      => 'min',
//                'checked_data'      => '0',
//                'key_name'          => 'share_profit_month',
//                'error_msg'         => '月度收益均摊必须大于0',
//                'error_code'         => 19004,
//            ],
//
//            [
//                'checked_type'      => 'min',
//                'checked_data'      => '0',
//                'key_name'          => 'share_profit_season',
//                'error_msg'         => '季度收益均摊必须大于0',
//                'error_code'         => 19005,
//            ],
//            [
//                'checked_type'      => 'min',
//                'checked_data'      => '0',
//                'key_name'          => 'share_profit_year',
//                'error_msg'         => '年度收益均摊必须大于0',
//                'error_code'         => 19006,
//            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'is_check',
                'error_msg' => '请选择是否参与考核',
                'error_code' => 19007,
            ],


        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);

            throw new \BaseException(['code' => 403, 'errorCode' => $error_arr[1], 'msg' => $error_arr[0], 'status' => false, 'debug' => false]);
        }


        // 添加/编辑
        if (empty($params['id'])) {
            $re = Db::name('employee_station')->insertGetId($params);
        } else {

            $re = Db::name('employee_station')->where(['id' => intval($params['id'])])->update($params);

        }
        if (!$re) {
            throw new \BaseException(['code' => 403, 'errorCode' => 19008, 'msg' => '保存数据失败', 'status' => false, 'debug' => false]);
        }
        return DataReturn('保存成功', 0);

    }

    /**
     * 部门列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function DepartmentListWhere($params = [])
    {
        $where = [['is_del', '=', 2]];


        //等级
        if (!empty($params['param']['higher_department_id'])) {
            $where[] = ['higher_department_id', '=', $params["param"]['higher_department_id']];
        }

        return $where;
    }


    /**
     * 获取部门列表数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DepartmentDataList($params)
    {
        $data = BaseService::DataList($params);
        return self::DepartmentDataDealWith($data);
    }

    /**
     * 部门数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function DepartmentDataDealWith($data)
    {
        if (!empty($data)) {
            foreach ($data as &$v) {
                //负责人
                $v['manager_employee_title'] = '未指定负责人';
                if (isset($v['manager_employee_id']) && $v['manager_employee_id'] > 0) {
                    $v['manager_employee_title'] = Db::name('employee_sal')->where('id=' . $v['manager_employee_id'])->value('employee_name');
                }
                //上级部门
                $v['higher_department_title'] = '公司';
                if (isset($v['higher_department_id']) && $v['higher_department_id'] > 0) {
                    $v['higher_department_title'] = Db::name('employee_department')->where('id=' . $v['higher_department_id'])->value('title');
                }
            }

        }


        return $data;
    }

    /**
     * 部门数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DepartmentDataSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'title',
                'error_msg' => '名称不能为空',
                'error_code' => 19010,
            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'manager_employee_id',
//                'error_msg'         => '请选择部门负责人',
//                'error_code'         => 19011,
//            ],


        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);

            throw new \BaseException(['code' => 403, 'errorCode' => $error_arr[1], 'msg' => $error_arr[0], 'status' => false, 'debug' => false]);
        }


        // 添加/编辑
        if (empty($params['id'])) {
            $re = Db::name('employee_department')->insertGetId($params);
        } else {

            $re = Db::name('employee_department')->where(['id' => intval($params['id'])])->update($params);

        }
        if (!$re) {
            throw new \BaseException(['code' => 403, 'errorCode' => 19009, 'msg' => '保存部门数据失败', 'status' => false, 'debug' => false]);
        }
        return DataReturn('保存成功', 0);

    }


    /**
     * 获取员工列表数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function EmployeeDataList($params)
    {
        $data = BaseService::DataList($params);

        return self::EmployeeDataDealWith($data);
    }

    /**
     * 员工数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function EmployeeDataDealWith($data)
    {
        if (!empty($data)) {
            foreach ($data as &$v) {
                //所属岗位
                $v['employee_station_title'] = self::StationDataHandle($v['employee_station_id']);

                //一级部门
                $v['department_title'] = self::DepartmentDataHandle($v['employee_department_id']);

                //二级部门
                $v['department_sectitle'] = self::DepartmentDataHandle($v['employee_department_secid']);

                //性别
                if (isset($v['employee_sex'])) {
                    $v['employee_sex_title'] = BaseService::StatusHtml($v['employee_sex'], lang('member_sex'), false);
                }
                //员工状态
                if (isset($v['employee_status'])) {
                    $v['employee_status_title'] = BaseService::StatusHtml($v['employee_status'], [1 => '试用期', 2 => '正式员工', 0 => '离职'], false);
                    if ($v['employee_status'] == 2) {
                        $time = strtotime(SalaryService::GetRecentTime()['time_pre']);
                        if ($time < strtotime($v['regular_time'])) {
                            $v['base_salary'] = $v['regular_base_salary'];
                            $v['station_salary'] = $v['regular_station_salary'];
                        }

                    }
                }


                //异动次数
                $v['change_num'] = Db::name('employee_dispatch')->where('employee_id=' . $v['id'])->count();

                //异动前所属部门 department_before_title
                $v['department_before_title'] = self::DepartmentDataHandle($v['department_id_before']);

                //异动前二级部门
                $v['department_before_sectitle'] = self::DepartmentDataHandle($v['department_seid_before']);

                //异动前所属岗位  station_before_title
                $v['station_before_title'] = self::StationDataHandle($v['station_id_before']);

                //异动后一级部门
                $v['department_after_title'] = self::DepartmentDataHandle($v['department_id_after']);

                //异动后二级部门
                $v['department_after_sectitle'] = self::DepartmentDataHandle($v['department_secid_after']);

                //异动后所属岗位
                $v['station_after_title'] = self::StationDataHandle($v['station_id_after']);


            }

        }


        // dump($data);exit;
        return $data;
    }

    /*
     * 获取所属部门
     */
    public static function DepartmentDataHandle($id)
    {
        //所属部门
        $department_title = '公司';
        //employee_station_title
        if (isset($id) && $id > 0) {
            $department_title = Db::name('employee_department')->where('id=' . $id)->value('title');
        } else {
            $department_title = '未选择二级部门';
        }
        return $department_title;
    }

    /*
   * 获取所属岗位
   */
    public static function StationDataHandle($id)
    {
        //所属岗位
        $employee_station_title = '未指定岗位';
        if (isset($id) && $id > 0) {
            $employee_station_title = Db::name('employee_station')->where('id=' . $id)->value('title');
        }
        return $employee_station_title;
    }

    /**
     * 员工数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function EmployeeDataSave($params = [])
    {

        // 请求参数 除了社保 毕业 专业 都是必填
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_name',
                'error_msg' => '名称不能为空',
                'error_code' => 19002,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'employee_head',
                'error_msg' => '头像不能为空',
                'error_code' => 19011,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'employee_idcard',
                'error_msg' => '员工身份证号不能为空',
                'error_code' => 19012,
            ],
            [
                'checked_type' => 'fun',
                'key_name' => 'employee_idcard',
                'checked_data' => 'CheckIdCard',
                'error_msg' => '清输入正确的身份证号',
                'error_code' => 19015,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_phone',
                'error_msg' => '联系方式不能为空',
                'error_code' => 19013,
            ],
            [
                'checked_type' => 'fun',
                'key_name' => 'employee_phone',
                'checked_data' => 'CheckMobile',
                'error_msg' => '清输入正确的联系方式',
                'error_code' => 19016,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'employee_code',
                'error_msg' => '员工工号不能为空',
                'error_code' => 19014,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'emergency_phone',
                'error_msg' => '紧急联系人电话不能为空',
                'error_code' => 19017,
            ],
            [
                'checked_type' => 'fun',
                'key_name' => 'emergency_phone',
                'checked_data' => 'CheckMobile',
                'error_msg' => '清输入正确的紧急联系人电话',
                'error_code' => 19018,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'emergency_name',
                'error_msg' => '紧急联系人不能为空',
                'error_code' => 19019,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'employee_code',
                'error_msg' => '员工工号不能为空',
                'error_code' => 19021,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'province',
                'error_msg' => '家庭住址不能为空',
                'error_code' => 19022,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'city',
                'error_msg' => '家庭住址不能为空',
                'error_code' => 19022,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'area',
                'error_msg' => '家庭住址不能为空',
                'error_code' => 19022,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'address',
                'error_msg' => '家庭住址不能为空',
                'error_code' => 19022,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'registered_province',
                'error_msg' => '户口所在地不能为空',
                'error_code' => 19023,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'registered_city',
                'error_msg' => '户口所在地不能为空',
                'error_code' => 19023,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'nation',
                'error_msg' => '民族不能为空',
                'error_code' => 19024,
            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'employee_sex',
//                'error_msg'         => '员工性别不能为空',
//                'error_code'         => 19025,
//            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'political_landscape',
                'error_msg' => '政治面貌不能为空',
                'error_code' => 19026,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'registered_cate',
                'error_msg' => '户籍类型不能为空',
                'error_code' => 19026,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'marriage_status',
                'error_msg' => '婚姻状态不能为空',
                'error_code' => 19027,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'education',
                'error_msg' => '学历不能为空',
                'error_code' => 19028,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_department_id',
                'error_msg' => '一级部门不能为空',
                'error_code' => 19029,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_station_id',
                'error_msg' => '岗位不能为空',
                'error_code' => 19030,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_hugh',
                'error_msg' => '月休天数不能为空',
                'error_code' => 19031,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'job_remark',
                'error_msg' => '工作描述不能为空',
                'error_code' => 19032,
            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'contract_code',
//                'error_msg'         => '劳动合同号不能为空',
//                'error_code'         => 19033,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'bank',
//                'error_msg'         => '工资卡开户行不能为空',
//                'error_code'         => 19034,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'bank_code',
//                'error_msg'         => '工资卡号不能为空',
//                'error_code'         => 19035,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'bank_name',
//                'error_msg'         => '收款人姓名不能为空',
//                'error_code'         => 19036,
//            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'base_salary',
                'error_msg' => '基础工资不能为空',
                'error_code' => 19037,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'station_salary',
                'error_msg' => '岗位工资不能为空',
                'error_code' => 19038,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'fullattendance_salary',
                'error_msg' => '满勤工资不能为空',
                'error_code' => 19039,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'employee_create',
                'error_msg' => '入职时间不能为空',
                'error_code' => 19040,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'regular_time',
                'error_msg' => '转正时间不能为空',
                'error_code' => 19041,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'regular_base_salary',
                'error_msg' => '转正后基础工资不能为空',
                'error_code' => 19042,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'regular_station_salary',
                'error_msg' => '转正后岗位基础工资不能为空',
                'error_code' => 19043,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'regular_fullattendance_salary',
                'error_msg' => '转正后满勤工资不能为空',
                'error_code' => 19044,
            ],
            //  employee_create  regular_time

        ];
        if ($params['employee_status'] < 1) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'quit_time',
                'error_msg' => '离职时间不能为空',
                'error_code' => 19020,
            ];
        }


        $ret = ParamsChecked($params, $p);


        if ($ret !== true) {
            $error_arr = explode(',', $ret);

            throw new \BaseException(['code' => 403, 'errorCode' => $error_arr[1], 'msg' => $error_arr[0], 'status' => false, 'debug' => false]);
        }


        //删除无用图片:单图
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'employee' . $params['id'];
        ResourceService::delCacheItem($params['employee_head']);
        if (empty($params['regular_time'])) {
            unset($params['regular_time']);
        }
        unset($params['thumb_img']);
        unset($params['old_imgurl']);
        if ($params['employee_status'] > 0) {
            unset($params['quit_time']);
        }

        // 添加/编辑
        if (empty($params['id'])) {
            //分机号判断
            if ($params['calling_status'] == 1) {
                if (empty($params['phone_code']) || empty($params['calling_account']) || empty($params['calling_password'])) {
                    throw new \BaseException(['code' => 403, 'errorCode' => 1921, 'msg' => '外呼信息不能为空', 'status' => false, 'debug' => false]);

                }
                $phone_code = Db::name('employee_sal')->where('phone_code=' . $params['phone_code'])->value('employee_status');
                if ($phone_code > 0) {
                    throw new \BaseException(['code' => 403, 'errorCode' => 1920, 'msg' => '分机号已被占用', 'status' => false, 'debug' => false]);
                }
            }

            #判断是否是 保险询价
            if($params['inqury_status'] == 1){
                #判断  密码是否 为空 是  没有修改   不是  判断是否 和确认密码一致
                if(!empty($params['login_pwd'])){
                    #判断保险后密码是否是大于6位 小于 18位
                    if($params['login_pwd']<6 or $params['login_pwd']>18){
                        throw new \BaseException(['code' => 403, 'errorCode' => 1920, 'msg' => '保险后台密码应大于等于6位小于等于18位', 'status' => false, 'debug' => false]);

                    }
                    if($params['login_pwd'] != $params['login_sucpwd']){
                        throw new \BaseException(['code' => 403, 'errorCode' => 1920, 'msg' => '保险后台密码与确认密码不一致', 'status' => false, 'debug' => false]);
                    }

                }


            }
            $login_access = $params['login_access'];
            $login_pwd = $params['login_pwd'];

            unset($params['login_access']);
            unset($params['login_pwd']);
            unset($params['login_sucpwd']);
            $re = Db::name('employee_sal')->insertGetId($params);

            if($re){
                #是否关联保险后台
                if($params['inquiry_status'] == 1){
                    #添加 保险后台密码
                    Db::table('inquiry_login')->insertGetId(array(
                        'employee_id'=>$re,
                        'login_access'=>$login_access,
                        'login_pwd'=>$login_pwd,
                        'login_mpwd'=>md5($login_pwd),
                    ));
                }

            }
        } else {
            unset($params['calling_account']);
            unset($params['calling_password']);
            unset($params['phone_code']);

            $params['employee_update'] = date('Y-m-d H:i:s');

            #判断是否是 保险询价
            if($params['inquiry_status'] == 1){
                #判断  密码是否 为空 是  没有修改   不是  判断是否 和确认密码一致
                if(!empty($params['login_pwd'])){
                    #判断保险后密码是否是大于6位 小于 18位
                    if(strlen($params['login_pwd'])<6 or strlen($params['login_pwd'])>18){
                        throw new \BaseException(['code' => 403, 'errorCode' => 1920, 'msg' => '保险后台密码应大于等于6位小于等于18位', 'status' => false, 'debug' => false]);

                    }
                    if($params['login_pwd'] != $params['login_sucpwd']){
                        throw new \BaseException(['code' => 403, 'errorCode' => 1920, 'msg' => '保险后台密码与确认密码不一致', 'status' => false, 'debug' => false]);
                    }
                    #看原來是否有賬號   有 修改 没有 则添加
                    $_where = array('employee_id'=>$params['id']);
                    $login = Db::table('inquiry_login')->where($_where)->find();
                    if(!empty($login)){
                        #修改
                        Db::table('inquiry_login')->where($_where)->update(array(
                            'login_access'=>$params['login_access'],
                            'login_pwd'=>$params['login_pwd'],
                            'login_mpwd'=>md5($params['login_pwd']),
                            'update_time'=>date("Y-m-d H:i:s")
                        ));
                    }else{
                        #添加
                        Db::table('inquiry_login')->insertGetId(array(
                            'employee_id'=>$params['id'],
                            'login_access'=>$params['login_access'],
                            'login_pwd'=>$params['login_pwd'],
                            'login_mpwd'=>md5($params['login_pwd']),
                        ));
                    }
                }


            }
            unset($params['login_access']);
            unset($params['login_pwd']);
            unset($params['login_sucpwd']);
            $re = Db::name('employee_sal')->where(['id' => intval($params['id'])])->update($params);

        }
        if (!$re) {
            throw new \BaseException(['code' => 403, 'errorCode' => 19009, 'msg' => '保存员工数据失败', 'status' => false, 'debug' => false]);
        }
        return DataReturn('保存成功', 0);

    }


    /**
     * 员工列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function EmployeeListWhere($params = [])
    {
        $where = [];

        //关键字
        if (!empty($params['param']['keywords'])) {
            $where[] = ['employee_name|employee_phone', 'like', '%' . $params["param"]['keywords'] . '%'];
        }

        //性别
        if (strlen($params['param']['employee_sex']) > 0) {

            $where[] = ['employee_sex', '=', $params["param"]['employee_sex']];
        }
        // dump($where);exit;
        //部门
        if (!empty($params['param']['employee_department_id'])) {
            $where[] = ['employee_department_id', '=', $params["param"]['employee_department_id']];
        }

        //二级部门
        if (!empty($params['param']['employee_department_secid'])) {
            $where[] = ['employee_department_secid', '=', $params["param"]['employee_department_secid']];
        }


        //岗位
        if (!empty($params['param']['employee_station_id'])) {
            $where[] = ['employee_station_id', '=', $params["param"]['employee_station_id']];
        }
        //状态
        if (!empty($params['param']['employee_status'])) {
            $where[] = ['employee_status', '=', $params["param"]['employee_status']];
        }
        if (!empty($params['param']['employeeStatus'])) {
            if ($params['param']['employeeStatus'] == 'onTheJob') {
                $where[] = ['employee_status', 'in', [1, 2]];
            }
        }
        //排除门店关联

        if (!empty($params['param']['notin'])) {
            $where[] = ['id', 'not in', Db::name('assign_staff')->where('type=1')->column('employee_id')];
        }
        //排除商户关联员工
        if (!empty($params['param']['notin_merchants'])) {
            $where[] = ['id', 'not in', Db::name('assign_staff')->where('type=2')->column('employee_id')];
        }

        //排除绩效考核员工
        if (!empty($params['param']['notin_employee_check'])) {

            $re = StatisticalService::HandleTime($params['param']['check_time'])['data'];
            $where_check = [
                ['check_time', '>=', $re['time_start']],
                ['check_time', '<=', $re['time_end']],
            ];

            $where[] = ['id', 'not in', Db::name('employee_check')->where($where_check)->column('employee_id')];
            //dump($where);exit;
        }

        //id in 查询
        if (!empty($params['param']['in_id'])) {
            $where[] = ['id', 'in', []];
        }
        //排除指定员工
        if (!empty($params['param']['related_employee'])) {
            $where[] = ['id', 'not in', [$params['param']['related_employee']]];
        }
        $redis = new Redis();
        $delIds = $redis->hGetJson('employeeInfo', 'delIds');
        if (!empty($delIds)) {
            $where[] = ['id', 'not in', $delIds];
        }

        // 排除不可以关联状态
        if (!empty($params['param']['relation_status'])) {
            # 是否可以关联状态
            $where[] = ['relation_status', '=', 1];
        }
        // 删除状态排除
        if (!empty($params['param']['employee_search_status'])) {
            if ($params['param']['employee_search_status'] == 1) {
                # 是否是删除状态
                $where[] = ['employee_status', 'neq', 0];
            } else {
                $where[] = ['employee_status', '=', 0];
            }
        } else {
            # 是否是删除状态
            $where[] = ['employee_status', 'neq', 0];
        }

        return $where;
    }


    /**
     * 员工调度数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function EmployeeDispatchDataDealWith($data)
    {
        if (!empty($data)) {
            foreach ($data as &$v) {
                //异动前所属岗位
                $v['station_before_title'] = '未指定岗位';
                if (isset($v['station_id_before']) && $v['station_id_before'] > 0) {
                    $v['station_before_title'] = Db::name('employee_station')->where('id=' . $v['station_id_before'])->value('title');
                }
                //异动前所属岗位
                $v['department_before_title'] = '公司';

                if (isset($v['department_id_before']) && $v['department_id_before'] > 0) {
                    $v['department_before_title'] = Db::name('employee_department')->where('id=' . $v['department_id_before'])->value('title');
                }

                //异动后所属岗位
                $v['station_after_title'] = '未指定岗位';
                if (isset($v['station_id_after']) && $v['station_id_after'] > 0) {
                    $v['station_after_title'] = Db::name('employee_station')->where('id=' . $v['station_id_after'])->value('title');
                }
                //异动后所属岗位
                $v['department_after_title'] = '公司';

                if (isset($v['department_id_after']) && $v['department_id_after'] > 0) {
                    $v['department_after_title'] = Db::name('employee_department')->where('id=' . $v['department_id_after'])->value('title');
                }

            }


            // dump($data);exit;
            return $data;
        }
    }

    /**
     * 员工数据拼接成html
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function HandleDataToHtml($param)
    {
        # 可关联状态
//        $param['param']['relation_status'] = 1;
        $where = self::EmployeeListWhere($param);

        $data_params = array(
            'page' => false,
            'where' => $where,
            'table' => 'employee_sal',
            'order' => 'employee_create desc',
            'field' => 'id,employee_name'

        );
        $data = self::EmployeeDataList($data_params);
        $button = empty($param['param']['button']) ? '关联' : $param['param']['button'];

        $table_html = '
       
            <div class="card-body">
                    
                        <div class="form-group">
                            <input type="text" id="search_title"  class="form-control" onkeyup="searchRelevanceStore()" placeholder="请输入员工名称" autocomplete="off">
                            <i class="form-group__bar"></i>
                        </div>
                    
            </div>
          
<table class="table table-bordered mb-0">
                        <thead>
                        <tr>
                            <th width="80%">名称</th>
                            
                             <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="storebody">';
        foreach ($data as $v) {

            $table_html .= '<tr class="lump' . $v['id'] . '">
                            <td  >' . $v['employee_name'] . '</td>
                             
                            <td><button class="btn btn-primary connect_detail" data-title="' . $v['employee_name'] . '"  data-id="' . $v['id'] . '" onclick=connect_detail_callback(this)   type="button" name="connect_detail"    >' . $button . '</button></td>
                        </tr>';
        }
        $table_html .= '
                        </tbody>
                    </table>';

        return ['status' => true, 'data' => $table_html, 'datalist' => $data];
        //DataReturn('ok',0,$table_html);
    }


}
