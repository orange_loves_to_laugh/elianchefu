<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/24
 * Time: 13:44
 */

namespace app\service;
use think\Db;
use think\Request;
use think\Response;
use think\route\Resource;

/**
 * 消息服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月24日13:23:09
 */
class InforService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function InforListWhere($params = [])
    {
        $where = [['is_del','=',2]];

        //接收方
        if(!empty($params['param']['news_accept']))
        {
            $where[] =['news_accept', '=',  $params["param"]['news_accept']];
        }
        //发送状态
        if(!empty($params['param']['news_status']))
        {
            $where[] =['news_status', '=',  $params["param"]['news_status']];
        }
        return $where;
    }

    /**
     * 获取消息列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function InfoList($params)
    {

        $data=BaseService::DataList($params);
        return self::InforDataDealWith($data,$params);


    }


    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $data [处理的数据]
     * @param    [array]          $params [输入参数]
     */
    public static function InforDataDealWith($data,$params){

        if(!empty($data))
        {
            $re=[];
            foreach($data as &$v)
            {
                //消息接收人 news_accept news_status
                if(isset($v['news_accept']))
                {
                    $v['news_accept_title']=BaseService::StatusHtml($v['news_accept'],lang('accepter_type_news'),false);
                }

                if(isset($v['news_status']))
                {

                    $v['news_status_title']=BaseService::StatusHtml($v['news_status'],lang('send_status'),false);
                }
                $v['check_num']=Db::name('news_accepter')->where('is_del=2 and news_id='.$v['id'])->count();



                // 图片
//                if(isset($v['news_thumb']))
//                {
//                    if(!empty($v['news_thumb']))
//                    {
//
//                        $v['news_thumb_show'] = ResourceService::AttachmentPathViewHandle($v['news_thumb']);
//
//                    }
//                }


            }
            $re['data']=$data;
            //分页
            if($params['page'])
            {
                $re['page_html'] = $params['page_html'];
            }else{
                $re['page_html'] = null;
            }
        }

//        else{
//            return DataReturn('获取失败',0,$data);
//        }

        return $data;


    }


    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function InforTotal($where){
        return (int) Db::name('news')->where($where)->count();
    }
    /**
     * 消息保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function InforSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'news_title',
                'error_msg'         => '消息标题不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'news_author',
                'error_msg'         => '发布人不能为空',
                'error_code'         => 30003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'news_accept',
                'error_msg'         => '接收人不能为空',
                'error_code'         => 30004,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'news_sendtime',
                'error_msg'         => '发送时间不能为空',
                'error_code'         => 30005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'news_thumb',
                'error_msg'         => '缩略图不能为空',
                'error_code'         => 30006,
            ],

        ];
        /*
        # 添加是判断标题是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'news',
                'key_name'          => 'news_title',
                'error_msg'         => '消息标题不能重复',
                'error_code'         => 30001,
            ];
        }
        */
        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=self::HandleData($params);


        // 添加/编辑
        if(empty($params['id']))
        {
            $re = Db::name('news')->insertGetId($data);
            if($params['news_accept']==2){
                $IGeTuiService = new IGeTuiService();
                $IGeTuiService->MessageNotification(0,13,['title'=>$params['news_title']],true,false);
            }
        } else {

            $re= Db::name('news')->where(['id'=>intval($params['id'])])->update($data);

        }

        if (!$re)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>30007,'msg'=>'保存消息失败','status'=>false,'debug'=>false]);
        }
        return DataReturn('保存成功', 0);
    }

    /**
     * 消息群发
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     * @data    [array]          $data [需处理数据]
     */
    public static function MassInfo($params = [],$data){
        $accept=[];

        //1所有人 2合作门店 3所有用户 4所有会员 5所有员工



        if($params['news_accept']==2)
        {
            $accept['table']='biz';
            $accept['field']='id';
            $accept['where']=[['biz_status','=',1]];
        }

        if($params['news_accept']==3)
        {
            $accept['table']='member';
            $accept['field']='id';
            $accept['where']=[['id','>',0]];
        }

        if($params['news_accept']==4)
        {
            $accept['table']='member_mapping';
            $accept['field']='member_id';
            $accept['where']=[['member_status','=',1]];
        }

        if($params['news_accept']==5)
        {
            $accept['table']='employee';
            $accept['field']='id';
            $accept['where']=[['employee_status','>',0]];
        }
        if(!empty($accept)){
            $id_arr=Db::name($accept['table'])->where($accept['where'])->column($accept['field']);
            //field($accept['field'])->select();
        }else{
          //所有人
            $id_arr=[0];
        }
        //news表

        Db::startTrans();
        $news_id = Db::name('news')->insertGetId($data);
        if(!$news_id)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>30007,'msg'=>'添加消息失败','status'=>false,'debug'=>false]);

        }
        /*
        //news_accepter表
        $data = [];
        foreach ($id_arr as $v){

            $data[]  = [
                'news_id'=>$news_id,
                'accepter_type'=>$params['news_accept'],
                'accepter_id' => $v,
            ] ;

        }

        $re=Db::name('news_accepter')->insertAll($data);
        if(!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>30008,'msg'=>'批量添加消息接受记录失败','status'=>false,'debug'=>false]);
        }

        //news_accepter_log表
        $data_news_accepter_log=[
            'news_id'=>$news_id,
            'accepter_type'=>$params['news_accept'],
            'context'=>$params['news_context'],
            'send_time'=>$data['news_sendtime'],
            'accepter_colle'=>join(',',$id_arr)
        ];
        $re=Db::name('news_accepter_log')->insertGetId($data_news_accepter_log);
        if(!$re)
        {
            Db::rollback();

            throw new \BaseException(['code'=>403 ,'errorCode'=>30009,'msg'=>'批量添加消息日志失败','status'=>false,'debug'=>false]);

        }
        */
        Db::commit();
        return json(DataReturn('添加成功',0));
    }
    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $data [输入参数]
     */
    public static function HandleData($params){



        //删除无用图片:单图
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'news'.$params['id'];
        ResourceService::delCacheItem($params['news_thumb']);

        unset($params['thumb_img']);
        unset($params['old_imgurl']);

        //删除无用图片:编辑器
        if (isset($params["news_context"]))
        {
            //删除无用图片
            $params['table']='news';
            $params['ueditor_content']= $params['news_context'];
            $re=ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }
        }
        $data=$params;
        return $data;

    }
}