<?php


namespace app\service;

use think\Db;
use Redis\Redis;

/**
 * 分销服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月10日11:28:50
 */
class DistributionService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月10日11:29:16
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {

        $where=[];
        //创建时间
        if(!empty($params['create_time']))
        {
            $where[] =['create_time', '>',  date('Y-m-d 00:00:00',strtotime("-1 day",time()))];
            $where[] =['create_time', '<',  date('Y-m-d 23:59:59',strtotime("-1 day",time()))];
        }
        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['mp.phone|m.member_name', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //推荐人
        if(!empty($params['recommender_id'] ))
        {
            $where[] =['mp.partner_recommender_id', '=',  $params["recommender_id"]];
        }
        //合伙人账户状态
        if(!empty($params['param']['status'])&&intval($params['param']['status'])>0)
        {
            $where[] =['mp.status', '=',  $params['param']["status"]];
        }
        //省
        if(!empty($params['param']['member_province'] ))
        {
            $where[] =['m.member_province', '=',  $params['param']["member_province"]];
        }
        //市
        if(!empty($params['param']['member_city'] ))
        {
            $where[] =['m.member_city', '=',  $params['param']["member_city"]];
        }
        //区
        if(!empty($params['param']['member_area'] ))
        {
            $where[] =['m.member_area', '=',  $params['param']["member_area"]];
        }
        $where[] = ['m.id','neq',25];
        $where[] = ['m.id','neq',9405];
        $where[] = ['m.id','neq',1733];
        $where[] = ['m.id','neq',9900];
        return $where;
    }
    /**
     * 分销列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月10日11:29:16
     * @param    [array]          $params [输入参数]
     */
    public static function DataList($params)
    {
        $where = empty($params['where']) ? [['mp.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'mp.create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'mp.id';
        $field = empty($params['field']) ? 'mp.*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;
        $data = Db::name('member_partner')->alias('mp')
            ->leftJoin(['member'=>'m'],'m.id=mp.member_id')
            ->where($where)
            ->field($field)->order($order)->group($group);



        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }
        return self::DataDealWith($data);
    }
    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月10日11:29:16
     * @param    [array]          $data [输入参数]
     */
    public static function DataDealWith($data){

        if(!empty($data['data']))
        {
            foreach($data['data'] as &$v)
            {
                //用户地区
                $v['total_address']=$v['member_province'].$v['member_city'].$v['member_area'];
                $v['total_address']=empty($v['total_address'])?'未获取到地区':$v['total_address'];
                //推荐人数
                $where=[
                    ['pid','=',$v['member_id']],
                    ['channel','in',[2,3,5]]
                ];
                $id_arr=Db::name('channel')->where($where)->group('member_id')->column('member_id');

                $v['recommend_num'] =Db::name('member_mapping')->where([['member_id','in',$id_arr],['member_level_id','>',0]])->count();

                // 推荐人名称
                $v['recommender_name']='无';
                $v['recommender_channel']=0;
                $channel = Db::table("channel")->field("channel,pid")->where(array("member_id"=>$v['member_id'],"action_type"=>2))->order("id desc")->find();
                if (!empty($channel))
                {
                    $v['partner_recommender_id'] = $channel['pid'];
                    $v['recommender_name']=self::recommendChannel($channel['pid'],$channel['channel']);
                    $v['recommender_channel'] = $channel['channel'];
                }

                //dump($v);exit;

                //状态
                if(isset($v['status']))
                {

                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('open_close_status'),false);
                }

                // 收益余额 推广总收益

                $v['partner_balance_total']=priceFormat($v['partner_balance_total']);

                $v['partner_balance']=priceFormat($v['partner_balance']);

                ///名称
                if (isset($v['nickname'])||isset($v['member_name']))
                {

                    $v['member_name']=empty($v['member_name'])?$v['nickname']:$v['member_name'];

                    $v['member_name']=CheckBase64($v['member_name']);

                    $v['member_name']=empty($v['member_name'])?'未获取到用户昵称':$v['member_name'];
                }


                //手机号
                if (isset($v['phone'])||isset($v['member_phone']))
                {
                    $v['member_phone']=empty($v['phone'])?$v['member_phone']:$v['phone'];
                }

                //注册时间 办理会员时间 会员等级
                if (isset($v['member_id'])&&intval($v['member_id'])>0)
                {
                    $v['register_time']='--';

                    $v['member_create']='--';

                    $v['member_level_title']='普通用户';

                    $v['member_role']='普通用户';

                    $info=Db::name('member_mapping')->where('member_id='.$v['member_id'])->field('register_time,member_create,member_level_id')->find();

                    if (!empty($info))
                    {
                        $v['register_time']=$info['register_time'];

                        $v['member_create']=$info['member_create'];

                        $v['member_level_id']=$info['member_level_id'];

                        $v['member_role']='会员';

                        $v['member_level_title']=BaseService::StatusHtml($info['member_level_id'],lang('member_level'),false);
                    }

                    $info=Db::name('member_partner')->where('member_id='.$v['member_id'])->value('id');

                    if (!empty($info))
                    {
                        $v['member_role']='合伙人';
                    }

                }


            }
        }
       // dump($data);exit;

        return $data;
    }
    /**
     * 用户列表html页面
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月11日11:10:41
     * @param    [array]          $params [输入参数]
     */
    public static function MemberListPage($params){
        $html='<div class="card">
                    <div class="card-body">
                        <table class="table table-bordered mb-0">
                            <thead>
                            <tr>
                            
                                <th>昵称</th>
                                <th>手机号</th>
                                <th>所在地区</th>
                                <th>等级</th>
                            </tr>
                            </thead>
                            <tbody>';
        $tbody='';

        $ret=self::SelectMemberData($params);
        if (!$ret['status'])
        {
            return DataReturn($ret['msg'],-1);
        }
        $ret=$ret['data'];

        if (!empty($ret)) {

            $ret['nickname']=empty($ret['member_name'])?$ret['nickname']:$ret['member_name'];
            $ret['nickname']=empty($ret['nickname'])?'未获取用户昵称':$ret['nickname'];
            $tbody.='<tr >
                    
                     <td>'.$ret['nickname'].'</td>
                     <td>'.$ret['member_phone'].'</td>
                     <td>'.$ret['member_province'].$ret['member_city'].$ret['member_area'].'</td>
                     <td>'.BaseService::StatusHtml($ret['member_level_id'],lang('member_level'),false).'</td>
                     </tr></tbody></table></div></div>
                     <button onclick="setDistributionPage('.$ret['member_id'].')" style="position: fixed;top:60%;left: 48%;" type="button" class="layui-btn layui-btn-lg layui-btn-normal">创建</button>
                     ';

        }else{
            $tbody.=  '</tbody></table></div></div>';
        }

        $html=$html.$tbody;
        return DataReturn('ok',0,$html);
    }
    /**
     * 查询用户数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月11日11:10:41
     * @param    [array]          $params [输入参数]
     */
    public static function SelectMemberData($params){
        $where=[
            ['m.member_phone', 'like', '%'.$params['phone'].'%'],
        ];
        $ret = Db::name('member')->alias('m')->leftJoin(['member_mapping'=>'mm'],'m.id=mm.member_id')
            ->where($where)->field('mm.member_id,mm.member_level_id,m.member_name,m.nickname,m.member_phone,m.member_province,m.member_city,m.member_area')->find();

        if ($ret['member_level_id']<2)
        {
            return DataReturn('用户级别不满足分销要求',-1);
        }
        if(!empty($params['senior_partner'])){
            # 查询是否是高级合伙人
            $partner=Db::name('member_partner')->field("senior_partner")->where(array("member_id"=>$ret['member_id']))->find();
            if(empty($partner)){
                return DataReturn('该用户不是普通合伙人',-1);
            }else{
                if($partner['senior_partner']==2){
                    return DataReturn('该用户已经是高级合伙人',-1);
                }
            }
        }else{
            $partner=Db::name('member_partner')->where('member_id='.$ret['member_id'])->find();
            if (!empty($partner))
            {
                return DataReturn('该用户已经是分销用户',-1);
            }
        }

        return DataReturn('ok',0,$ret);
    }

    /**
     * 编辑数据页面
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月11日11:10:41
     * @param    [array]          $params [输入参数]
     */
    public static function SavePage($params){
        $data_params = [
            'where'				=> ['id'=>$params['member_id']],
            'm'					=> 0,
            'n'					=> 1,
            'page'			  => false,
            'table'     =>'member',
            'order'     =>'id desc'
        ];
        $ret = BaseService::DataList($data_params);

        $data = empty($ret[0]) ? [] : $ret[0];
        $data['nickname']=empty($data['nickname'])?'未获取用户昵称':$data['nickname'];
        $data['nickname']=empty($data['member_name'])?$data['nickname']:$data['member_name'];
        $data['nickname']=CheckBase64($data['nickname']);
        $html='
                <div class="card" >
                    <div class="card-body">
                        <table class="table table-bordered mb-0">
                             
                            <tbody>
                            <tr>
                                <th scope="row">昵称</th>
                                <td>'.$data['nickname'].'</td>
                              
                            </tr>
                            <tr>
                                <th scope="row">手机号</th>
                                <td>'.$data['member_phone'].'</td>
                               
                            </tr>
                            <tr>
                                <th scope="row">所在地区</th>
                                <td>'.$data['member_province'].$data['member_city'].$data['member_area'].'</td>
                               
                            </tr>
                             
                            </tbody>
                        </table>
                    </div>
                   
                </div>
                <div class="card">
                <div class="card-body">
                    <div class="btn-demo" style="margin-left: 35%">
                       <button onclick="cancelDistribution('.$data['id'].')"    type="button"  class="layui-btn layui-btn-lg layui-btn-primary">取消</button>
                        <button onclick="setDistribution('.$data['id'].')"      type="button" class="layui-btn layui-btn-lg layui-btn-normal">创建合伙人</button>
                    </div>
                </div>
                </div>
                 ';
        return DataReturn('ok',0,$html);
    }

    /**
     * 插入数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月11日11:10:41
     * @param    [array]          $params [输入参数]
     */
    public static function SaveData($params){

       # 判断是否重复
        $p[]=   [
            'checked_type'      => 'unique',
            'checked_data'      => 'member_partner',
            'key_name'          => 'member_id',
            'error_msg'         => '合伙人已经存在',
            'error_code'         => 90003,
        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=[
            'member_id'=>$params['member_id'],
            'phone'=>Db::name('member')->where('id='.$params['member_id'])->value('member_phone'),

        ];
        Db::startTrans();
        $re= Db::name('member_partner')->insertGetId($data);
        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90002,'msg'=>'创建合伙人失败','status'=>false,'debug'=>false]);
        }
        $re=Db::name('member_mapping')->where('member_id='.$params['member_id'])->update(['member_type'=>2]);

        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90010,'msg'=>'修改合伙人状态失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }


    /**
     * 设置合伙人利润参数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SavePartnerSet_old($params = [])
    {
        // 编辑
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'pay_price',
                'error_msg'         => '合伙人支付金额不能为空',
                'error_code'         => 22001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'prise_price',
                'error_msg'         => '推荐合伙人奖励金额不能为空',
                'error_code'         => 22002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_commission',
                'error_msg'         => '汽车服务类收益比例不能为空',
                'error_code'         => 22003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'consumption_commission',
                'error_msg'         => '生活消费类收益比例不能为空',
                'error_code'         => 22004,
            ],
        ];
        $ret = ParamsChecked($params, $p);
        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

        }
        $data=json_encode($params);
        Db::startTrans();


        if(Db::name('sysset')->where(['tagname'=>'partner_set'])->update(['desc'=>$data]))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90006,'msg'=>'设置合伙人参数失败','status'=>false,'debug'=>false]);
        }
        Db::commit();

        return DataReturn('保存成功', 0);
    }
    /**
     * 设置合伙人利润参数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SavePartnerSet($params = []){
        $data=Db::name('sysset')->where(['tagname'=>'partner_set'])->value('desc');
        $data=json_decode($data,true);

        $field=input('type');
        $param=input();
        Db::startTrans();
       // $data['prise_info']=[];
        if ($field=="prise_info")
        {
            $prize_type=$param["data"]['prize_type'];

            //赠送服务
            if ($prize_type=='service')
            {
                $where=[
                    'card_type_id'=>1,
                    'server_id'=>$param["data"]["pro_service_id"],
                ];
                $id=Db::name('card_voucher')->where($where)->value('id');

                $prise_info_val=[
                    'type'=>1,
                    'id'=>$id,
                    'card_time'=>$param["data"]["card_time"],
                    'num'=> $param["data"]["giving_number"],
                ];
                $data['prise_info'][time()]=$prise_info_val;
            }
            //赠送商品
            if ($prize_type=='pro'){
                $where=[
                    'card_type_id'=>3,
                    'server_id'=>$param["data"]["pro_service_id"],
                ];
                $id=Db::name('card_voucher')->where($where)->value('id');

                $prise_info_val=[
                    'type'=>2,
                    'id'=>$id,
                    'card_time'=>$param["data"]["card_time"],
                    'num'=> $param["data"]["giving_number"],
                ];
                $data['prise_info'][time()]=$prise_info_val;
            }
            //赠送红包
            if ($prize_type=='red')
            {

                /*
                $red_data=[
                    'create_time'=>TIMESTAMP,
                    'price'      =>$params["data"]["price"],
                    'packet_title'      =>$params["data"]["packet_title"],
                    'ground_num'      =>$params["data"]["ground_num"],
                ];

                $red_id= Db::name('redpacket')->insertGetId($red_data);

                if($red_id<0)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>90007,'msg'=>'添加红包失败','status'=>false,'debug'=>false]);
                }
                */

                $red_detaildata=[
                    'redpacket_id'=>0,
                    'voucher_create'=>TIMESTAMP,
                    'voucher_id'    =>$params["data"]['voucher_id'],
                    'voucher_title'=>$params["data"]['voucher_title'],
                    'voucher_type'=>$params["data"]['voucher_type'],
                    'voucher_price'=>$params["data"]['voucher_price'],
                    'money_off'=>$params["data"]['money_off'],
                    'voucher_over'=>$params["data"]['voucher_over'],
                    'voucher_number'=>$params["data"]['voucher_number'],
                    'voucher_start'=>$params["data"]['voucher_start'],
                ];

                $red_detailid=Db::name('redpacket_detail')->insertGetId($red_detaildata);

                if($red_detailid<0)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>90007,'msg'=>'添加红包卡券失败','status'=>false,'debug'=>false]);
                }

                $prise_info_val=[
                    'type'=>3,
                    'id'=>$red_detailid,
                    'num'=> 1,
                ];
                $data['prise_info'][time()]=$prise_info_val;


            }
        }
        else{
            $data[$field]= $param['data'];
        }

        $data=json_encode($data);

        $re=Db::name('sysset')->where(['tagname'=>'partner_set'])->update(['desc'=>$data]);

        if ($re<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90006,'msg'=>'设置合伙人参数失败', 'status'=>false,'debug'=>false]);
        }
        Db::commit();

        $redis=new Redis();
        $redis->hDel('partner_set', 'info');

        return DataReturn('保存成功', 0);

    }
    /** 处理合伙人奖励数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $data [奖励数据]
     */
    public static function PartnerPriseDealWith($data)
    {
        $data=$data['prise_info'];

        foreach ($data as &$v)
        {
            $v['type_title']=BaseService::StatusHtml($v['type'],[1=>'赠送服务',2=>'赠送商品',3=>'赠送红包'],false);
            switch ($v['type']){
                case 1:
                    $info=Db::name('card_voucher')->where('id='.$v['id'])->find();

                    $v['card_title']=$info['card_title'];
                    $v['card_price']=priceFormat($info['card_price']);
                    $v['voucher_over']=$v['card_time'];
                    $v['money_off']='--';
                   break;
                case 2:
                    $info=Db::name('card_voucher')->where('id='.$v['id'])->find();
                    $v['card_title']=$info['card_title'];
                    $v['card_price']=priceFormat($info['card_price']);
                    $v['voucher_over']=$v['card_time'];
                    $v['money_off']='--';
                    break;
                case 3:
                    $info=Db::name('redpacket_detail')->where('id='.$v['id'])->find();
                    $v['card_title']=$info['voucher_title'];
                    $v['card_price']=priceFormat($info['voucher_price']);
                    $v['voucher_over']=$info['voucher_over'];
                    $v['money_off']=$info['money_off'];
                    break;

            }
        }

        return $data;
    }
    /** 处理合伙人奖励数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $param [输入参数]
     */
    public static function DelPartnerPrise($param){
        $data=Db::name('sysset')->where(['tagname'=>'partner_set'])->value('desc');

        $data=json_decode($data,true);
        Db::startTrans();
        foreach ($data['prise_info'] as $k=>$v)
        {
            if ($k==$param['key'])
            {
                if ($v["type"]==3)
                {
//                    $re=Db::name('redpacket')->where('id='.$v["id"])->delete();
//                    if ($re<0)
//                    {
//                        Db::rollback();
//                        throw new \BaseException(['code'=>403 ,'errorCode'=>90009,'msg'=>'删除红包失败', 'status'=>false,'debug'=>false]);
//                    }
                    $re=Db::name('redpacket_detail')->where('id='.$v["id"])->delete();
                    if ($re<0)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>90009,'msg'=>'删除红包详情失败', 'status'=>false,'debug'=>false]);
                    }


                }

                unset($data['prise_info'][$k]);
            }
        }

        $data=json_encode($data);

        $re=Db::name('sysset')->where(['tagname'=>'partner_set'])->update(['desc'=>$data]);

        if ($re<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90008,'msg'=>'删除合伙人奖励失败', 'status'=>false,'debug'=>false]);
        }
        Db::commit();
        $redis=new Redis();
        $redis->hDel('partner_set', 'info');
        return DataReturn('保存成功', 0);
    }

    /**
     * @param $params
     * @return array
     * @throws \BaseException
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 保存高级合伙人
     */
    public static function SaveSeniorData($params){

        # 判断是否重复
        $ret = Db::table("member_partner")->where(array("member_id"=>$params['member_id'],"senior_partner"=>2))->find();
        if(!empty($ret))
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>90003,'msg'=>"该高级合伙人已存在",'status'=>false,'debug'=>false]);
        }
        Db::startTrans();
        $re=Db::name('member_partner')->where('member_id='.$params['member_id'])->update(['senior_partner'=>2]);

        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90010,'msg'=>'创建高级合伙人失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }

    /**
     * @param $params
     * @return mixed
     * @context 查询高级合伙人下的推广员或会员
     */
    static function seniorLowerLevel($params)
    {
        $where = " and ch.id > 0";
        if(key_exists('member_type',$params) and !empty($params['member_type'])){
            $where .= " and mm.member_type ={$params['member_type']}";
        }
        if(key_exists('start_time',$params)  and !empty($params['start_time'])){
            $where .= " and date(mp.create_time) >={$params['start_time']}";
        }
        if(key_exists('end_time',$params)  and !empty($params['end_time'])){
            $where .= " and date(mp.create_time) >={$params['start_time']}";
        }
        if(key_exists('month',$params)  and !empty($params['month'])){
            $where .= " and date_format(mp.create_time,'%Y-%m') ={$params['month']}";
        }
        $list = Db::query("SELECT `category_id`.level, ch.*,mm.member_level_id,m.member_name,m.nickname,m.member_phone,m.member_province,m.member_city,m.member_area
                    FROM
                    (
                    SELECT
                    @ids as _ids,
                    ( SELECT @ids := GROUP_CONCAT(member_id) FROM channel WHERE FIND_IN_SET(pid, @ids)) as cids,
                    @l := @l+1 as level
                    
                    FROM channel,(SELECT @ids :={$params['member_id']}, @l := 0 ) b WHERE @ids IS NOT NULL
                    
                    ) `category_id`, channel ch 
                    left join member_mapping mm on mm.member_id = ch.member_id
                    left join member m on m.id = mm.member_id
                    left join member_partner mp on mp.member_id = mm.member_id
                    WHERE FIND_IN_SET(ch.member_id, `category_id`._ids) and ch.action_type=2 and ch.pid!=0 and ch.channel in (2,3,5) and ch.member_id!={$params['member_id']} {$where}
                    
                    ORDER BY level");
        return $list;
    }

    /**
     * @param $data
     * @return array
     * @context 格式化推广员数据
     */
    static function seniorDataHandle($data)
    {
        # 先将数组按等级分成若干数组
        # 获取数组最大值
        $max =0;
        $new_data= array();
        foreach ($data as $key => $info) {
            $info['name'] = $info['member_name'];
            if(empty($info['member_name'])){
                $info['name'] = $info['nickname'];
                if(empty($info['nickname'])){
                    $info['name'] = "未知名称";
                }
            }
            $info['name']="\r\n".$info['name']."\r\n".$info['member_phone'];
            $info['source'] ="地区：{$info['member_province']}{$info['member_city']}{$info['member_area']}".
                "<br />"."会员等级：{$info['member_level_title']}"."<br />"."手机号：{$info['member_phone']}";
            $new_data[$info['level']][$info['member_id']] = $info;
            if($info['level']>$max){
                $max = $info['level'];
            }
        }
        $_return =array();
        for($i=$max;$i>=2;$i--){
            foreach($new_data[$i] as $k=>$v){
                if($i==2){
                    array_push($_return,$v);
                }else{
                    if(!empty($new_data[$i-1][$v['pid']])){
                        $new_data[$i-1][$v['pid']]["children"][]=$v;
                    }
                }
            }
        }
        return $_return;
    }

    /**
     * @param $recommend_id 推荐人id
     * @param $channel  推荐渠道
     * @return mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 推荐渠道
     */
    static function recommendChannel($recommend_id,$channel)
    {
        $recommend_title = "无";
        if($recommend_id){
            switch ($channel){
                case 1:
                    // 门店推荐
                    $recommend_title = Db::table("biz")->field("biz_title")->where(array("id"=>$recommend_id))->find()['biz_title'];
                    break;
                case 2:
                    // 会员推荐
                    $recommend_info = Db::table("member")->field("member_name,nickname")->where(array("id"=>$recommend_id))->find();
                    $recommend_title = empty($recommend_info['member_name']) ? empty($recommend_info['nickname']) ? '暂无名称' : $recommend_info['nickname'] : $recommend_info['member_name'];
                    break;
                case 3:
                    // 合伙人推荐
                    $recommend_info = Db::table("member")->field("member_name,nickname")->where(array("id"=>$recommend_id))->find();
                    $recommend_title = empty($recommend_info['member_name']) ? empty($recommend_info['nickname']) ? '暂无名称' : $recommend_info['nickname'] : $recommend_info['member_name'];
                    break;
                case 4:
                    // 员工推荐
                    if($recommend_id<0){
                        $recommend_title = Db::table("employee_sal")->field("employee_name")->where(array("id"=>abs($recommend_id)))->find()['employee_name'];
                    }else{
                        $recommend_title = Db::table("employee")->field("employee_name")->where(array("id"=>$recommend_id))->find()['employee_name'];
                    }
                    break;
                case 5:
                    // 用户推荐
                    $recommend_info = Db::table("member")->field("member_name,nickname")->where(array("id"=>$recommend_id))->find();
                    $recommend_title = empty($recommend_info['member_name']) ? empty($recommend_info['nickname']) ? '暂无名称' : $recommend_info['nickname'] : $recommend_info['member_name'];
                    break;
                case 6:
                    // 商户推荐
                    $recommend_title = Db::table("merchants")->field("title")->where(array("id"=>$recommend_id))->find()['title'];
                    break;
                case 7:
                    // 商户员工推荐
                    break;
            }
        }
        return $recommend_title;
    }
}
