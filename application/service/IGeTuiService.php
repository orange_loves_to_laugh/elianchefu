<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/8/24
 * Time: 13:11
 */

namespace app\service;


use Protocol\Curl;
use think\Db;

class IGeTuiService
{
    private $host = 'http://sdk.open.api.igexin.com/apiex.htm';
    //测试
    private $appkey = '';
    private $appid = '';
    private $mastersecret = '';

    private function init()
    {
        // header("Content-Type: text/html; charset=utf-8");
        $this->appid = config('getui.appid');
        $this->appkey = config('getui.appkey');
        $this->mastersecret = config('getui.mastersecret');
        $this->host = config('getui.host');
    }

    private function merchantsInit()
    {
        $this->appid = config('merchantConfig.appid');
        $this->appkey = config('merchantConfig.appkey');
        $this->mastersecret = config('merchantConfig.mastersecret');
        $this->host = config('merchantConfig.host');
    }

    public function __construct($APP=1)
    {
        if($APP==2){
            $this->merchantsInit();
        }else{
            $this->init();
        }
        $this->__loader();
    }

    private function __loader()
    {
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/IGt.Push.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/IGt.AppMessage.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/IGt.TagMessage.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/IGt.APNPayload.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/template/IGt.BaseTemplate.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/IGt.Batch.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/utils/AppConditions.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/template/notify/IGt.Notify.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/igetui/IGt.MultiMedia.php');
        require_once(ROOT . '/vendor/getuilaboratory/getui-pushapi-php-client/payload/VOIPPayload.php');
//        require_once ('igetui/template/IGt.RevokeTemplate.php');
//        require_once ('igetui/template/IGt.StartActivityTemplate.php');

    }

    /**
     * @param int $memberId 用户id
     * @param int $type 类型 1=>预约成功  2=>预约到期  3+>服务评价  4=>办理会员  5=>会员升级 6=>会员充值 7=>活动上新  8=>会员到期 9=>积分兑换  10=>红包到账  11卡券过期  12文章上新  13首页消息提醒
     * @param array $parameter
     * @param bool $app
     * @param bool $wx
     * @content 消息提醒
     */
    function MessageNotification($memberId, $type, $parameter = [], $app = true, $wx = true)
    {
        $port = 1;
        $rules_url='';
        if ($type == 1) {
            # 预约成功
            $config = config('templateMsg.reservation');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'K6ib4Uv0qKUO6qy8DORCvwBmnOYBFkQ6LZ0JKUSNlPM';
            # 查询门店信息
            $bizInfo = CommissionService::getBizCommissionInfo($parameter['biz_id']);
            # 查询服务名称
            $serviceTitle = getActiveTitle(2, $parameter['service_id'])['title'];
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['appoint_time'], 'color' => '#000000'),//预约时间
                'keyword2' => array('value' => $bizInfo['biz_title'], 'color' => '#000000'),// 预约门店
                'keyword3' => array('value' => $serviceTitle, 'color' => '#000000'),//预约项目
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
        } elseif ($type == 2) {
            # 预约到期提醒
            $config = config('templateMsg.reservation_expired');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = '-DLPxjr6rEC1Jmixeftyc2jgtR6B8od6l9ajpVmyotY';
            # 查询用户信息
            $memberService = new \app\api\ApiService\MemberService();
            $memberInfo = $memberService->MemberInfoCache($memberId);
            # 查询服务名称
            $serviceTitle = getActiveTitle(2, $parameter['service_id'])['title'];
            # 查询门店信息
            $bizInfo = CommissionService::getBizCommissionInfo($parameter['biz_id']);
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['order_number'], 'color' => '#000000'),// 订单编号
                'keyword2' => array('value' => $memberInfo['member_name'] ? $memberInfo['member_name'] : $memberInfo['nickname'], 'color' => '#000000'), // 客户
                'keyword3' => array('value' => $serviceTitle, 'color' => '#000000'), // 项目信息
                'keyword4' => array('value' => $parameter['appoint_time'], 'color' => '#000000'), // 预约时间
                'keyword5' => array('value' => $bizInfo['biz_address'], 'color' => '#000000'), // 地址
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
        } elseif ($type == 3) {
            # 服务评价
            $config = config('templateMsg.comment');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = '2FlURfQREBqmhE_K0CfhRUZXVyD-A-gtuNKD4QX1PTA';
            # 查询门店信息
            $bizInfo = CommissionService::getBizCommissionInfo($parameter['biz_id']);
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $bizInfo['biz_title'], 'color' => '#000000'),//门店名称
                'keyword2' => array('value' => $parameter['time'], 'color' => '#000000'), //消费时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
        } elseif ($type == 4) {
            # 办理会员成功
            $config = config('templateMsg.level');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'JqUvidxTyo9_aIIk9nR17KYxEsNSP5uNEXMtuugvSlk';
            # 查询用户信息
            $memberService = new \app\api\ApiService\MemberService();
            $memberInfo = $memberService->MemberInfoCache($memberId);
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $memberInfo['member_name'] ? $memberInfo['member_name'] : $memberInfo['nickname'], 'color' => '#000000'),//姓名
                'keyword2' => array('value' => $memberInfo['member_phone'], 'color' => '#000000'),//手机
                'keyword3' => array('value' => $parameter['level_title'], 'color' => '#000000'),//会员等级
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
        } elseif ($type == 5) {
            # 会员升级
            $config = config('templateMsg.level');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'n61b27ieWRlpr21IxEPms15Z8xHCffzvQpA_gFoUvjY';
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['level_title'], 'color' => '#000000'),// 当前会员等级
                'keyword2' => array('value' => date('Y-m-d H:i'), 'color' => '#000000'),//升级时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
        } elseif ($type == 6) {
            # 会员充值
            $config = config('templateMsg.recharge');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'DIXuUskja3c6ciwnY7w-2J7dXW00arvLNBhQL4R6FsA';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['phone'], 'color' => '#000000'),//会员卡号
                'keyword2' => array('value' => priceFormat($parameter['price']), 'color' => '#000000'),//充值金额
                'keyword3' => array('value' => priceFormat($parameter['giveBalance']), 'color' => '#000000'),//赠送金额
                'keyword4' => array('value' => priceFormat($parameter['balance']), 'color' => '#000000'),//当前余额
                'keyword5' => array('value' => date("Y-m-d H:i"), 'color' => '#000000'),//充值时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
        } elseif ($type == 7) {
            # 新活动提醒
            $config = config('templateMsg.newActive');
            $title = $parameter['title'];
            $text = $config['remark'];
        } elseif ($type == 8) {
            # 会员到期
            $config = config('templateMsg.vipExpiration');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'T3ktqQ3xSiUlNQJx7yNAOUEZGum-nW0FbaNY14dFf7c';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'name' => array('value' => $parameter['level_title'], 'color' => '#000000'),//会员卡
                'expDate' => array('value' => $parameter['expiration'], 'color' => '#000000'),//过期时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
        } elseif ($type == 9) {
            # 积分兑换
            $config = config('templateMsg.integralExchange');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = 'FZjnA1oyPo003sBRX37jrMm8uxlNLSVICPiGxbrSksY';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            # 查询用户信息
            $memberService = new \app\api\ApiService\MemberService();
            $memberInfo = $memberService->MemberInfoCache($memberId);
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $memberInfo['member_name'] ? $memberInfo['member_name'] : $memberInfo['nickname'], 'color' => '#000000'),//会员昵称
                'keyword2' => array('value' => $parameter['order_number'], 'color' => '#000000'),//订单号
                'keyword3' => array('value' => date("Y-m-d H:i"), 'color' => '#000000'),//交易时间
                'keyword4' => array('value' => $parameter['title'], 'color' => '#000000'),//兑换品
                'keyword5' => array('value' => $parameter['price'], 'color' => '#000000'),//消费积分
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
        } elseif ($type == 10) {
            # 红包到账
            $config = config('templateMsg.redPacket');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = '7rwf7m2yb1rTk358sUHwMIqoygfY6uARlbXXgwdA-Ms';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['price'], 'color' => '#000000'),// 收入金额
                'keyword2' => array('value' => $parameter['title'], 'color' => '#000000'),// 收入类型
                'keyword3' => array('value' => date("Y-m-d H:i"), 'color' => '#000000'),//到账时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
        } elseif ($type == 11) {
            #  卡券过期提醒
            $config = config('templateMsg.coupon_expired');
            $title = $config['first'];
            $text = $config['remark'];
            $template_id = '';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            $data = array(
                'first' => array('value' => $config['first'], 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['title'], 'color' => '#000000'),// 名称
                'keyword3' => array('value' => $parameter['expiration'], 'color' => '#000000'),//过期时间
                'remark' => array('value' => $config['remark'], 'color' => '#000000'),
            );
        } elseif ($type == 12) {
            # 新文章提醒
            $config = config('templateMsg.newClub');
            $title = $parameter['title'];
            $text = $config['remark'];
        } elseif ($type == 13) {
            # 新消息提醒
            $config = config('templateMsg.newNews');
            $title = $parameter['title'];
            $text = $config['remark'];
        } elseif ($type == 14) {
            # 合伙人收益提醒
            $title = '您好,你已获得'.$parameter['price'].'元的收益金额';
            $text = '点击查看详情';
            $template_id = 'Z9bOmB21uEQQwmQmSIAVM8OXis_CDe9sPAFluaLoMnM';
            $url = 'http://web.elianchefu.com/#/pages/mine/index';
            $data = array(
                'first' => array('value' => $title, 'color' => '#000000'),
                'keyword1' => array('value' => $parameter['title'], 'color' => '#000000'),// 收入类型
                'keyword2' => array('value' => $parameter['price'], 'color' => '#000000'),// 收入金额
                'keyword3' => array('value' => date("Y-m-d H:i"), 'color' => '#000000'),//到账时间
                'keyword4' => array('value' => $parameter['balance'], 'color' => '#000000'),//到账时间
                'remark' => array('value' => $text, 'color' => '#000000'),
            );
        }elseif($type==15){
            $title = $parameter['title'];
            $text = $parameter['text'];
            $rules_url = $parameter['rules_url'];
            $port = 2;
        }elseif($type==16){
            #给用户端用户推送
            $title = $parameter['title'];
            $text = $parameter['text'];
//            $rules_url = $parameter['rules_url'];
            $port = 1;
        } else {
            return false;
        }

        if ($app) {
            if ($type == 7 or $type == 12 or $type == 13) {
                # 群推
                return $this->pushMessageToApp($title, $text);
            } else {
                # 单推

               return $this->pushMessageToSingle($title, $text, $memberId,$port,$rules_url);

            }
        }
        if ($wx) {
//            dump('模板消息');
            return $this->sendTemplate($memberId, $template_id, $url, $data);
        }
    }

    /**
     * @param $memberId
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询用户状态
     */
    function getUserStatus($memberId)
    {
        # 通过用户id查询cid
        $memberService = new \app\api\ApiService\MemberService();
        $memberInfo = $memberService->MemberInfoCache($memberId);
        $cid = $memberInfo['equipment_id'];
        $igt = new \IGeTui($this->host, $this->appkey, $this->mastersecret);
        $rep = $igt->getClientIdStatus($this->appid, $cid);
//        var_dump($rep);
        return $rep;
    }

    //单推接口案例
    function pushMessageToSingle($title, $text, $memberId,$port=1,$rules_url='')
    {

        if($port==2){
            # 查找商户信息
            $cid = Db::table("merchants")->field("equipment_id")->where(array("id"=>$memberId))->find()['equipment_id'];
        }else{
            # 通过用户id查询cid
            $memberService = new \app\api\ApiService\MemberService();
            $memberInfo = $memberService->MemberInfoCache($memberId);
            $cid = $memberInfo['equipment_id'];
        }
        if(empty($cid)){
            return array('status'=>false,'msg'=>'设备id为空');
        }

//        dump($cid);
        $igt = new \IGeTui($this->host, $this->appkey, $this->mastersecret);
        //消息模版：
        // 1.TransmissionTemplate:透传功能模板
        // 2.LinkTemplate:通知打开链接功能模板
        // 3.NotificationTemplate：通知透传功能模板
        // 4.NotyPopLoadTemplate：通知弹框下载功能模板

//    	$template = IGtNotyPopLoadTemplateDemo();
//    	$template = IGtLinkTemplateDemo();
//    	$template = IGtNotificationTemplateDemo();
        $template = $this->IGtTransmissionTemplate($title, $text,$rules_url,$port);


//        $template = $this->IGtLinkTemplate($content, $title, $text);
//        $template = SmsDemo();
        //个推信息体
        $message = new \IGtSingleMessage();

        $message->set_isOffline(false);//是否离线
        $message->set_offlineExpireTime(3600 * 12 * 1000);//离线时间
        $message->set_data($template);//设置推送消息类型
        $message->set_PushNetWorkType(0);//设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
        //接收方
        $target = new \IGtTarget();
//        dump($this->appid);
        $target->set_appId($this->appid);
        $target->set_clientId($cid);
//    $target->set_alias(Alias);


        try {
            $rep = $igt->pushMessageToSingle($message, $target);
//            dump('成功');
//            dump($rep);
            return ($rep);
//            echo("<br><br>");

        } catch (RequestException $e) {
            $requstId = $e->getRequestId();
            $rep = $igt->pushMessageToSingle($message, $target, $requstId);
            return ($rep);
//            var_dump('異常');
//            var_dump($rep);
            echo("<br><br>");

        }

    }

    public function pushMessageToApp($title, $text)
    {
        $igt = new \IGeTui($this->host, $this->appkey, $this->mastersecret);

        $template = $this->IGtTransmissionTemplate($title, $text);
        //个推信息体
        //基于应用消息体
        $message = new \IGtAppMessage();
        $message->set_isOffline(true);
        $message->set_offlineExpireTime(10 * 60 * 1000);//离线时间单位为毫秒，例，两个小时离线为3600*1000*2
        $message->set_data($template);
        //    $message->setPushTime("201808011537");
        $appIdList = array($this->appid);
        $message->set_appIdList($appIdList);
        $rep = $igt->pushMessageToApp($message);
//        dump($rep);
        return ($rep);
        echo("<br><br>");
    }

    public function IGtTransmissionTemplateDemo($content, $title, $text)
    {
        $template = new \IGtTransmissionTemplate();
        $template->set_appId($this->appid); //应用appid
        $template->set_appkey($this->appkey); //应用appkey
        //透传消息类型
        $template->set_transmissionType(2);
        $payload = [
            'title' => $title,
            'content' => $text,
//            'payload'=>'测试的啦'
        ];
        //透传内容
        $template->set_transmissionContent(json_encode($payload, JSON_UNESCAPED_UNICODE));
        // $template->set_duration(BEGINTIME,ENDTIME); //设置ANDROID客户端在此时间区间内展示消息
        //这是老方法，新方法参见iOS模板说明(PHP)*/
        //$template->set_pushInfo("actionLocKey","badge","message",
        //"sound","payload","locKey","locArgs","launchImage");

        //  APN高级推送
        // $apn = new \IGtAPNPayload();
        // $alertmsg=new \DictionaryAlertMsg();
        // $alertmsg->body="body";
        // $alertmsg->actionLocKey="ActionLockey";
        // $alertmsg->locKey="LocKey";
        // $alertmsg->locArgs=array("locargs");
        // $alertmsg->launchImage="launchimage";
        return $template;
    }

    function IGtTransmissionTemplate($title, $text,$rules_url='',$port=1)

    {

//        $payload = "{'title':'" . $title . "','content':'" . $text . "','sound':'default','payload':'test'}";
        $arr = array('title' => $title, 'content' => $text,'rules_url'=>$rules_url);


        $payload = json_encode($arr, JSON_UNESCAPED_UNICODE);
//        $intent = 'intent:#Intent;launchFlags=0x04000000;action=android.intent.action.oppopush;package=uni.UNI7BB36DB;component=uni.UNI7BB36DB/pages/index/index; S.UP-OL-SU=true;S.title=' . $title . ';S.content=' . $text . ';S.payload=' . $payload . ';end';
        if($port==1) {
            $intent = "intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;component=uni.UNI7BB36DB/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title={$title};S.content={$text};S.payload={$payload};end";
        }else {
            $intent = "intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;component=uni.UNIF77E396/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title={$title};S.content={$text};S.payload={$payload};end";
        }
//        $intent = 'intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;component=uni.UNI7BB36DB/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title=红包来晚了;S.content=来了个大粑粑;S.payload=test;end';

        $template = new \IGtTransmissionTemplate();
        $template->set_appId($this->appid);//应用appid
        $template->set_appkey($this->appkey);//应用appkey
        $template->set_transmissionType(2);//透传消息类型
        $template->set_transmissionContent($payload);//透传内容

        //注意：如果设备离线（安卓），一定要设置厂商推送，不然接收不到推送（比如华为、小米等等）
        //S.title=的值为推送消息标题，对应5+ API中PushMessage对象的title属性值；
        //S.content=的值为推送消息内容，对应5+ API中PushMessage对象的content属性值；
        //S.payload=的值为推送消息的数据，对应5+ API中PushMessage对象的payload属性值；
//        $intent = 'intent:#Intent;action=android.intent.action.oppopush;launchFlags=0x14000000;component=uni.UNI7BB36DB/io.dcloud.PandoraEntry;S.UP-OL-SU=true;S.title='.$title.';S.content='.$text.';S.payload='.json_encode($listId).';end';
//        $intent = "intent:#Intent;component=uni.UNI7BB36DB/pages/index/index;S.title=红包来了;S.text=哈哈哈哈;S.content=嘿嘿额黑和;S.payload=".$payload.";end";
        $notify = new \IGtNotify();
        $notify->set_title($title);
        $notify->set_content($text);
        $notify->set_intent($intent);
        $notify->set_type(\NotifyInfo_type::_intent);
        $template->set3rdNotifyInfo($notify);

        //下面这些是苹果需要设置的，只要是ios系统的，都要设置这个，不然离线收不到
        //APN高级推送
        $alertmsg = new \DictionaryAlertMsg();
        $alertmsg->body = $text;
        $alertmsg->actionLocKey = "查看";
        $alertmsg->locKey = $text;
        $alertmsg->locArgs = array("locargs");
        $alertmsg->launchImage = "launchimage";
//        IOS8.2 支持
        $alertmsg->title = $title;
        $alertmsg->titleLocKey = $text;
        $alertmsg->titleLocArgs = array("TitleLocArg");

        $apn = new \IGtAPNPayload();
        $apn->alertMsg = $alertmsg;
        $apn->badge = 0;
        $apn->sound = "http://back.elianchefu.com/shareimg/9f69150e3259f2d610bd20ce97e1ee3dfile.wav";
        $apn->add_customMsg("payload", "payload");
        $apn->contentAvailable = 0;
        $apn->category = "ACTIONABLE";
        $template->set_apnInfo($apn);

        return $template;
    }

    //通知栏显示 点击跳转url
    function IGtLinkTemplate($title, $text)
    {
        $template = new \IGtLinkTemplate();
        $template->set_appId($this->appid);//应用appid
        $template->set_appkey($this->appkey);//应用appkey
        //透传消息类型
        $template->set_transmissionType(2);
        $payload = [
            'title' => $title,
            'content' => $text,
//            'payload'=>'测试的啦'
        ];
        //透传内容
        $template->set_transmissionContent(json_encode($payload, JSON_UNESCAPED_UNICODE));
        $template->set_title($title);//通知栏标题
        $template->set_text($text);//通知栏内容
        $template->set_logo("http://wwww.igetui.com/logo.png");//通知栏logo
        $template->set_isRing(true);//是否响铃
        $template->set_isVibrate(true);//是否震动
        $template->set_isClearable(true);//通知栏是否可清除
        $template->set_url("https://www.8a1.top/pages/index/index");//打开连接地址
        //$template->set_duration(BEGINTIME,ENDTIME); //设置ANDROID客户端在此时间区间内展示消息
        return $template;
    }
    /**
     * @param string $memberId 用户的d
     * @param string $template_id 模板id
     * @param array $data 模板消息的内容(二维数组)
     * @param string $url 跳转地址(自动拼接)
     * 例如 :  array(
     * 'first' => array('value' => "值", 'color' => '#000000'),
     * 'keyword1' => array('value' => "值", 'color' => '#000000'),
     * 'keyword2' => array('value' => "值", 'color' => '#000000'),
     * 'keyword3' => array('value' => "值", 'color' => '#000000'),
     * 'keyword4' => array('value' => "值", 'color' => '#000000'),
     * 'remark' => array('value' => "值", 'color' => '#000000'),
     * );
     * @content 发送模板消息 (多文字时,使用"\n" 进行换行显示)
     * @return bool
     */
    function sendTemplate($memberId, $template_id, $url, $data)
    {
//        dump('用户id'.$memberId);
        if (empty($memberId)) {
            return true;
        } else {
            $memberService = new \app\api\ApiService\MemberService();
            $memberInfo = $memberService->MemberInfoCache($memberId);
            $openid = $memberInfo['member_openid'];
//            dump('openid=>'.$openid);
            if (empty($openid)) {
                return false;
            }
            # 获取token
            $token = $this->getAccess_token();
//            dump($token);
            $_curl = new Curl();
            # 模板消息接口地址
            $_url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $token;
            # 拼接模板数据
            $data_msg = json_encode(array(
                'touser' => $openid,
                'template_id' => $template_id,
                'url' => $url,
                'data' => $data
            ));
            # 发送
            $_receipt = $_curl->post($_url, $data_msg, 'json');
            # 对返回结果进行处理(json 转 array)
            $_receipt = json_decode($_receipt, true);
//            dump($_receipt);
            if ($_receipt['errcode'] == 0 and $_receipt['errmsg'] == 'ok') {
                return true;
            } else {
                return true;
            }
        }
    }

    /**
     * @return mixed|null
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 获取公众号access_token
     */
    function getAccess_token()
    {
        $access_token = Db::table('access_token')->order('id desc')->find();
        if (empty($access_token['access_token'])) {
            $accessToken = $this->requestAccessToken();
            if (!empty($accessToken)) {
                Db::table('access_token')->insert(array('access_token' => $accessToken['access_token'], 'time' => strtotime('+110 minutes')));
                return $accessToken['access_token'];
            } else {
                return null;
            }
        } else {
            if (time() > $access_token['time']) {
                $accessToken = $this->requestAccessToken();
                if (!empty($accessToken)) {
                    Db::table('access_token')->where(array('id' => $access_token['id']))->update(array('access_token' => $accessToken['access_token'], 'time' => strtotime('+110 minutes')));
                    return $accessToken['access_token'];
                } else {
                    return null;
                }
            } else {
                return $access_token['access_token'];
            }
        }
    }

    /**
     * @access public
     * @return null
     * @context  重新申请公众号access_token
     */
    function requestAccessToken()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx281132202d144816&secret=9e9a552eb147972ac5ebd23e4b5580ff";
        $_curl = new Curl();
        $_receipt = json_decode($_curl->get($url), true);
        if (!empty($_receipt['access_token'])) {
            return $_receipt;
        }
    }
}
