<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/7/20
 * Time: 14:21
 */

namespace app\service;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use AlibabaCloud\SDK\ViapiUtils\ViapiUtils;
use Protocol\Curl;
use Redis\Redis;
use Sms\Sms;

class SmsCode
{
    # 焦云
    const APP_ID = 'd291296b4836fbd653f61279b387b91f'; // appid 请更换为您自己的应用appId)
    const API_KEY = '36e92cfbd6a642d4b1ea8d59602627a2'; // appKey 请更换为您自己的应用appKey
    # 云之讯
    const ACCOUNTS_ID = '139ea1b7201e3153ee7ae45cdd7b0285'; // appid 请更换为您自己的应用appId
    const TOKEN = '4f734d3928a3643148369c4847d7dd94'; // appKey 请更换为您自己的应用appKey

    /**
     * @param $phone
     * @return array
     * @content 焦云发送短信验证码
     */
    function smsCode($phone)
    {
        if (!empty($phone)) {
            $redis = new Redis();
            $_code = $redis->hGet('smsCode', strval($phone));
            $mark = false;
            if (!empty($_code)) {
                $code = $_code;
            } else {
                $mark = true;
                $code = randCodes();
            }
            $sms = new Sms(self::APP_ID, self::API_KEY);
            #验证码短信模板id
            $tpl_id = '3305';
            #模板变量
            $tpl_value = "#code#=" . $code;
            $t = $sms->sendSms($phone, $tpl_id, $tpl_value);
//            dump($t);
            if ($t['code'] == '100') {
                # 发送成功,存入redis
                if ($mark)
                    $redis->hSet('smsCode', strval($phone), strval($code), '1800');
                return array('status' => true, 'code' => $code);
            } else {
                return array('status' => false, 'msg' => '发送失败');
            }
        } else {
            return array('status' => false, 'msg' => '手机号为空');
        }
    }

    /**
     * @param null $mobile 手机号
     * @param null $content 发送内容(发送短信验证码时不用传)
     * @param bool $mark true 为发送验证码 , false为发送通知
     * @return array
     */
    function Ysms($mobile = null, $content = null, $mark = true, $codes = '',$sendtime='')
    {
        if (!empty($mobile)) {
            $_status = false;
            $redis = new Redis();
            if ($mark) {
                # 发送短信验证码
                $code = $redis->hGet('smsCode', strval($mobile));
                if (!empty($code)) {
                    $content = $code;
                } else {
                    $_status = true;
                    $content = randCodes();
                }
                if (!empty($codes)) {
                    $content = $codes;
                }
                $sendInfo = "您的验证码为：" . $content . "。30分钟内有效，请勿向他人泄露验证码。";
            } else {
                $sendInfo = $content;
            }
            if(!empty($sendtime)) {
                $BaseUrl = "http://request.ucpaas.com/sms-partner/access/nuochepai@126.com/timer_send_sms";
                $body_json = array(
                    "clientid" => "b020r8",
                    "password" => md5("by123456"),
                    "mobilelist" => base64_encode(gzencode($mobile)),
                    "smstype" => "0",
                    "content" => '【E联车服】' . $sendInfo,
                    "sendtime" => $sendtime,
                    "extend" => "",
                    "uid" => "",
                    "compress"=>0//默认为gzip压缩
                );
            }else {
                $BaseUrl = "http://request.ucpaas.com/sms-partner/access/nuochepai@126.com/sendsms";
                $body_json = array(
                    "clientid" => "b020r8",
                    "password" => md5("by123456"),
                    "mobile" => $mobile,
                    "smstype" => "0",
                    "content" => '【E联车服】' . $sendInfo,
                    "sendtime" => $sendtime,
                    "extend" => "",
                    "uid" => ""
                );
            }
            if ($mark) {
                $ali = $this->AliSms($mobile, $content, $_status);
                if ($ali['status']) {
                    return array('status' => true, 'msg' => '发送成功','code' => $content, 'res' => $ali);
                }
            }
            $curl = new Curl();
            $data = $curl->post($BaseUrl, $body_json, 'json');
            if ($data) {
                if ($mark and $_status) {
                    # 发送验证码,存入redis
                    $redis->hSet('smsCode', strval($mobile), strval($content), '1800');
                }
                return array('status' => true, 'code' => $content, 'data' => $data);
            }else{
                return array('status' => false, 'msg' => '发送失败','data'=>$data);
            }
        } else {
            return array('status' => false, 'msg' => '手机号为空');
        }
    }

    function AliSms($mobile = null, $content = null, $_status = false)
    {
        AlibabaCloud::accessKeyClient('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $mobile,
                        'SignName' => "E联车服",
                        'TemplateCode' => "SMS_201723359",
                        'TemplateParam' => "{code:'" . $content . "'}",
                    ],
                ])
                ->request();
            $res = $result->toArray();
            if ($_status and $res['Code'] == 'OK') {
                $redis = new Redis();
                # 发送验证码,存入redis
                $redis->hSet('smsCode', strval($mobile), strval($content), '1800');
            }
            return array('status' => true, 'msg' => '发送成功', 'res' => $res);
//                return $result->toArray();
        } catch (ClientException $e) {
            return array('status' => false, 'msg' => '发送失败', 'client' => $e->getErrorMessage());
//                return $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            return array('status' => false, 'msg' => '发送失败', 'server' => $e->getErrorMessage());
//                return $e->getErrorMessage() . PHP_EOL;
        }
    }
}
