<?php


namespace app\service;

use think\Db;

/**
 * 发货单服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月9日14:07:47
 */
class DispathService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月9日16:39:15
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['biz_phone|biz_title', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //门店类型
        if(!empty($params['param']['biz_type']))
        {
            $where[] =['b.biz_type', '=', $params["param"]['biz_type']];
        }
        return $where;

    }
    /**
     * 获取数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月9日16:39:15
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DataList($params = []){
        $where = empty($params['where']) ? [['d.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'd.create_time desc';
        $field = empty($params['field']) ? 'd.*,b.*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        // 获取列表
        $data = Db::name('dispath')->alias('d')->leftJoin(['biz'=>'b'],'d.biz_id=b.id')
            ->where($where)->field($field)->order($order);
        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }
        return self::DataDealWith($data);
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月9日14:42:19
     * @desc    description
     * @param    [array]          $data [输入参数]
     */
    public static function DataDealWith($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                //车型
                if(isset($v['brandmodel_id']))
                {
                    $v['brandmodel_title']=Db::name('brand')->where('id='.$v['brandmodel_id'])->value('brand_title');
                }
                //售价
                if(isset($v['selling_price']))
                {
                    $v['selling_price_show']=priceFormat($v['selling_price']).'w';
                }
                //进价
                if(isset($v['purchase_price']))
                {
                    $v['purchase_price_show']=priceFormat($v['purchase_price']).'w';
                }
                //车牌
                if(isset($v['brand_id']))
                {
                    $v['brand_title']=Db::name('brand')->where('id='.$v['brand_id'])->value('brand_title');
                }
                //处理状态
                if(isset($v['read_status']))
                {
                    $v['read_status_title']=BaseService::StatusHtml($v['read_status'],lang('read_status'));
                }
                //处理状态
                if(isset($v['biz_type']))
                {
                    $v['biz_type_title']=BaseService::StatusHtml($v['biz_type'],lang('biz_type'),false);

                }
                //配置
                $v['configuration_show']='未填写参数';
                if(isset($v['configuration'])&&!empty($v['configuration']))
                {
                    $v['configuration_show']=join(',',json_decode($v['configuration'],true));
                }



            }
        }


        return $data;
    }

    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月9日14:42:19
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function DataTotal($where){

        $data = Db::name('dispath')->alias('d')->leftJoin(['biz'=>'b'],'d.biz_id=b.id')->where($where)->count();

        return (int) $data;
    }
}