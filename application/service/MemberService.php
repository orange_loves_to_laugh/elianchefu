<?php


namespace app\service;

use think\Db;

/**
 * 活动管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
class MemberService
{
    public static function MemberPartnerListWhere($params = []){
        $where = [];

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['m.member_name|m.member_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //推荐人id
        if(!empty($params['member_id']))
        {
            $where[] =['mp.partner_recommender_id', '=',$params["member_id"]];
        }

        return $where;
    }
    /**
     * 获取数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function MemberPartnerDataList($params){

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['m.member_name|m.member_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }

        $wherechannel=[
            ['pid','=',$params["member_id"]],
            ['channel','in',[2,3,5]]
        ];
        $id_arr=Db::name('channel')->where($wherechannel)->column('member_id');
        $where[]=['m.id','in',$id_arr];

        $data=Db::name('member')->alias('m')

           ->leftJoin(['member_mapping'=>'mm'],'mm.member_id=m.id')
           ->where($where)
           ->group('m.id')
           ->field('mm.member_level_id,mm.member_balance,m.member_name,m.member_phone,mm.member_level_id,m.nickname,m.id member_id')
           ->paginate(10, false, ['query' => request()->param()]);
       $data=$data->toArray();

        foreach ($data['data'] as &$v)
        {
            $partner_status=Db::name('member_partner')->where('member_id='.$v['member_id'])->field('id,partner_balance')->find();

            $v['nickname']=empty($v['nickname'])?'未获取到用户名':$v['nickname'];

            $v['member_name']=empty($v['member_name'])?$v['nickname']:$v['member_name'];

            $v['member_name']=CheckBase64($v['member_name']);

            $v['partner_status_title']='普通用户';

            if ($v['member_level_id']>0)
            {
                $v['partner_status_title']='会员';
            }
            $v['partner_balance']='0.00';
            if ($partner_status['id']>0)
            {
                $v['partner_status_title']='合伙人';

                $v['partner_balance']=$partner_status['partner_balance'];
            }

            $v['level_title']=$v['member_level_id']<1?'普通用户':BaseService::StatusHtml($v['member_level_id'],lang('member_level'),false);

            $v['member_balance']=priceFormat($v['member_balance']);



        }
       return $data;

    }
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceListWhere($params = [])
    {
        $where = [];

        //member_id
//        if(!empty($params['param']['member_id']))
//        {
//            $where[] =['o.member_id', '=',  $params["param"]['member_id']];
//        }
        if(!empty($params['member_id']))
        {
            $where[] =['o.member_id', '=',  $params['member_id']];
        }
        //关键字
        if(!empty($params['param']['service_title']))
        {
            $where[] =['s.service_title', 'like', '%'.$params["param"]['service_title'].'%'];

        }

        //时间段
        if(!empty($params['param']['pay_create']))
        {
            $start=$params["param"]['pay_create'];

            $end=date('Y-m-d 23:59:59',strtotime($start));

            $where[] =['o.pay_create', '>=',  $start];

            $where[] =['o.pay_create', '<=',  $end ];

        }


        //门店id
        if(!empty($params['param']['biz_id'])&&intval($params['param']['biz_id'])>0)
        {
            $where[] =['o.biz_id', '=',  $params["param"]['biz_id']];
        }
        //评分 1好评 2中评 3差评
        if(!empty($params['param']['comment_level'])&&intval($params['param']['comment_level'])>0)
        {
           switch ($params['param']['comment_level']){
               case 1:
                   $where[] =['es.score', '>',  3];
                   break;
               case 2:
                   $where[] =['es.score', '=',  3];
                   break;
               case 3:
                   $where[] =['es.score', '<',  3];
                   break;
           }
        }

        return $where;
    }
    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function DataTotal($where){
       return (int)  Db::name('orders')->alias('o')
           ->leftJoin(['order_server'=>'os'],'os.id=o.id')
           ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
           ->leftJoin(['biz'=>'b'],'b.id=o.biz_id')
           ->leftJoin(['service'=>'s'],'s.id=os.server_id')
           ->where($where)
           ->group('car_liences')
           ->count();

    }
    /**
     * 用户服务
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function ServiceDataList($where){


//   车牌号 门店名称 车型 车系 结算时间  支付方式 是否使用凯泉 卡券抵用金额 实际支付
        $field='o.id,o.member_id,car_liences,o.pay_price,o.pay_type,o.order_appoin_time,o.order_type,
                o.voucher_price,o.pay_create,o.biz_id,o.order_number,o.evaluation_status,o.order_status';

        $data=Db::name('orders')->alias('o')
            ->leftJoin(['order_server'=>'os'],'os.order_number=o.order_number')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->leftJoin(['evaluation_score'=>'es'],'os.id=es.orderserver_id')
            ->field($field)
            ->where($where)
            ->group('o.id')
            ->order("o.pay_create desc")
            ->paginate(10, false, ['query' => request()->param()]);

         $data=$data->toArray();

        //dump($where);exit;
         $re=self::ServiceDataDealWith($data,$where);

         return $re;

    }
    /**
     * 用户服务数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     * @param    [array]          $where [查询条件]
     */

    public static function ServiceDataDealWith($data,$where)
    {
        if(!empty($data['data']))
        {

            foreach($data['data'] as &$v)
            {

                // 项目 金额 支付方式 门店
                $v['car_liences']=empty($v['car_liences'])?'无':$v['car_liences'];

                $v['pay_type_title']=BaseService::StatusHtml($v['pay_type'],lang('cash_type'),false);

                $v['biz_title']=Db::name('biz')->where('id='.$v['biz_id'])->value('biz_title');

                $car=Db::table('member_car mc')
                    ->field('cl.title logoTitle,cs.sort_title sortTitle')
                    ->join('car_sort cs', 'mc.car_type_id=cs.id', 'left')
                    ->join('car_logo cl', 'cs.logo_id=cl.id', 'left')
                    ->where([['mc.car_licens','=',$v['car_liences']],['member_id','=',$v['member_id']]])
                    ->where('status !=3')->find();

                $v['logo_title']=empty($car['logoTitle'])?'无':$car['logoTitle'];

                $v['sort_title']=empty($car['sortTitle'])?'无':$car['sortTitle'];

                $v['server_arr']=Db::name('order_server')
                    ->alias('os')
                    ->leftJoin(['service'=>'s'],'os.server_id=s.id')
                    ->where([['os.order_number','=',$v['order_number']]])
                    ->column('s.service_title');
                if (count($v['server_arr'])>0) {
                    $v['server_arr']=join(',',$v['server_arr']);
                }
                $v['refund_status_title'] = "无退款";
                $v['refund_price'] = "0.00";
                if(!empty($v['order_status']) and $v['order_type']==3){
                    switch ($v['order_status']){
                        case 2:
                            $v['order_status_title'] = "即将过期";
                            break;
                        case 3:
                            $v['order_status_title'] = "已超时";
                            break;
                        case 4:
                            $v['order_status_title'] = "未到店";
                            break;
                        case 5:
                            $v['order_status_title'] = "已完成";
                            break;
                        case 6:
                            $v['order_status_title'] = "已到店";
                            break;
                        default:
                            $v['order_status_title'] = "等待到店";
                            break;
                    }
                    if($v['order_status']==4){
                        $refund=Db::table("log_consump")->field("convert(consump_price,decimal(10,2)) refund_price")->where(array("order_number"=>$v['order_number'],"log_consump_type"=>2,"income_type"=>15))->find();
                        if(!empty($refund)){
                            $v['refund_status_title'] = "已退款";
                            $v['refund_price'] = $refund['refund_price'];
                        }
                    }
                }
//                $where[]=[
//                ['o.car_liences','=',$v['car_liences']],
//                ];

//                $v['service_arr']=Db::name('evaluation_score')->alias('es')
//                    ->leftJoin(['order_server'=>'os'],'os.id=es.orderserver_id')
//                    ->leftJoin(['orders'=>'o'],'o.order_number=os.order_number')
//                    ->leftJoin(['service'=>'s'],'s.id=os.server_id')
//                    ->where($where)
//                    ->field('s.service_title,price,es.score,server_id')
//                    ->select();

//                if (count($v['service_arr'])>0)
//                {
//                    foreach ($v['service_arr'] as &$vv)
//                    {
//                        $vv['pay_type_title']=BaseService::StatusHtml($vv['pay_type'],lang('cash_type'),false);
//
//                    }
//                }
            }

            //dump($data);exit;
        }

        return $data;
    }

    /**
     * 用户基本信息保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveData($params){
        //dump($params);exit;
        $data_key=[
            'member_name',
            'member_age',
            'member_sex',
            'member_phone',
            'member_idcard',
            'member_address',
            'member_province',
            'member_city',
            'member_area',
            'account_status',
        ];
        $data_member=[];
        foreach ($data_key as $v)
        {
            if (isset($params[$v])) {
                $data_member[$v]= $params[$v];
            }
        }
        // 编辑
//        $data_member=[
//            'member_name'=>$params['member_name'],
//            'member_age'=>$params['member_age'],
//            'member_sex'=>$params['member_sex'],
//            'member_phone'=>$params['member_phone'],
//            'member_idcard'=>$params['member_idcard'],
//            'member_address'=>$params['member_address'],
//            'member_province'=>$params['member_province'],
//            'member_city'=>$params['member_city'],
//            'member_area'=>$params['member_area'],
//            'account_status'=>$params['account_status'],
//        ];
        if (count($data_member)>0) {
            $re= Db::name('member')->where(['id'=>intval($params['id'])])->update($data_member);
        }



        $data_member_mapping=[];
        $data_key=[
            'register_time',
            'member_code',
            'member_level_id',
            'member_expiration'
        ];
        foreach ($data_key as $v)
        {
            if (isset($params[$v])) {
                $data_member_mapping[$v]= $params[$v];
            }
        }
        //dump($data_member_mapping);exit;
        if (count($data_member_mapping)>0)
        {
            $re= Db::name('member_mapping')->where(['member_id'=>intval($params['id'])])->update($data_member_mapping);
        }

//        if (!$re)
//        {
//            throw new \BaseException(['code'=>403 ,'errorCode'=>14002,'msg'=>'保存用户基本信息失败','status'=>false,'debug'=>false]);
//        }
        return DataReturn('保存成功', 0);
    }

    /**
     * 用户商家消费记录数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年12月31日11:56:09
     * @desc    description
     * @param   [array]          $data [输入参数]
     */
    public static function ConsumptionDataList($params){
        $data = BaseService::DataList($params);
        if(!empty($data))
        {

            foreach($data as &$v)
            {
                //商家名称
                if(isset($v['merchants_id'])){
                    $v['merchants_title'] = Db::table('merchants')->where(array('id'=>$v['merchants_id']))->value('title');
                }

                // 收入类型
                if(isset($v['pay_type']))
                {
                    $v['pay_type_title'] = BaseService::StatusHtml($v['pay_type'],lang('merchants_pay_type'),false);
                }
                //付款方式 cash_type_title
                if(isset($v['cash_type']))
                {
                    $v['cash_type_title'] = BaseService::StatusHtml($v['cash_type'],lang('cash_type'),false);
                }

                //结算状态
                if(isset($v['order_status']))
                {
                    $v['order_status_title'] = BaseService::StatusHtml($v['order_status'],lang('merchants_order_status'),false);
                }

            }
        }

        return $data;
    }
    /**
     * 用户红包领取记录数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年12月31日11:56:09
     * @desc    description
     * @param   [array]          $data [输入参数]
     */
    public static function RedEnvelopeDataDealWith($data){
        //红包名称 领取时间 红包类型 红包金额 红包状态
        if(!empty($data))
        {

            foreach($data as &$v)
            {
                //商家名称
                if(isset($v['redpacket_id'])){

                    $info = Db::table('redpacket')->where(array('id'=>$v['redpacket_id']))->find();


                    $v['price']=$info['price'];

                    if (empty($v['price'])&&$v['redpacket_title_id']>0)
                    {
                        $v['price']=Db::table('redpacket_title')->where(['id'=>$v['redpacket_title_id']])
                            ->value('price');
                    }

                    $v['packet_title']=$info['packet_title'];

                    $v['type_title']=BaseService::StatusHtml($info['type'],[1=>'普通红包',2=>'大转轮红包',3=>'积分兑换商品的红包'],false);

//                    $v['status_title']=BaseService::StatusHtml($info['status'],[1=>'开启',0=>'关闭'],false);
                }


            }
        }

        return $data;
    }
    /**
     * 获取用户昵称
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年12月31日11:56:09
     * @desc    description
     * @param   [array]          $where [输入参数]
     */
    public static function SelectMemberName($where){
        $member_info=Db::name('member')->where($where)->field('nickname,member_name')->find();

        $name=empty($member_info['member_name'])?$member_info['nickname']:$member_info['member_name'];

        $name=CheckBase64($name);

        $name=empty($name)? '未获取到用户名':$name;

        return $name;
    }

}
