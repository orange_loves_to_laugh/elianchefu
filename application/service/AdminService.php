<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 2020/6/22
 * Time: 14:06
 */

namespace app\service;
use BaseException;
use Redis\Redis;
use think\Db;
use think\facade\Session;

/**
 * 服务管理层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0git
 * @datetime 2020年10月22日13:45:48
 */
class AdminService
{

    /**
     * 后台管理服务列表条件
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $params [输入参数]
     */
    public static function GetAdminIndexWhere($params = [])
    {
        $where = [
            ['is_del', '=', 2],
            ['audit_status', '=', 2]
        ];
        // 模糊
        if(!empty($params['param']['keywords']))
        {
            $where[] = ['service_title|service_keyborder|service_number', 'like', '%'.$params['param']['keywords'].'%'];
        }



        //分类id
        if(isset($params['service_class_id'])&&intval($params['service_class_id'])>0)
        {
            $where[] = ['service_class_id', '=', $params['service_class_id']];
        }
//        if(!empty($params['param']['class_type']))
//        {
//            $where[] = ['class_pid', '=', $params['param']['class_type']];
//        }
        if(isset($params["param"]['class_pid'])&&intval($params["param"]['class_pid'])>0)
        {
            $where[] = ['class_pid', '=', $params["param"]['class_pid']];
        }

        //折扣类型
        if(!empty($params['param']['discount_type']))
        {
            $where[] = ['discount_type', '=', $params['param']['discount_type']];
        }
        //推荐
        if(isset($params['service_top'])&&intval($params['service_top'])>-1)
        {
            $where[] = ['service_top', '=', $params['service_top']];
        }
        if(!empty($params['param']['service_status']))
        {
            if($params['param']['service_status']==1){
                $where[] = ['service_status', '=', 1];
            }else{
                $where[] = ['service_status', '=', 0];
            }
        }
        //预约
        if(isset($params['appoint'])&&intval($params['appoint'])>-1)
        {
            $where[] = ['appoint', '=', $params['appoint']];
        }
        //id 集合
        if(!empty($params['id_arr']))
        {
            $where[] = ['id','in',explode(',',$params['id_arr'])];
        }
        // 合作门店创建的服务 筛选
        if(!empty($params['biz_id']))
        {
            $where[] = ['biz_pid','=',$params['biz_id']];
        }else{
            $where[] = ['biz_pid','=',0];
        }

      //  dump($where);exit;
        // 服务门店
//        if(!empty($params['biz_id']) && $params['biz_id'] > 0)
//        {
//            $category_ids = self::ServiceBizItemsIds([intval($params['category_id'])], 1);
//            $goods_ids = Db::name('GoodsCategoryJoin')->where(['category_id'=>$category_ids])->column('goods_id');
//            if(!empty($goods_ids))
//            {
//                $where[] = ['id', 'in', $goods_ids];
//            } else {
//                // 避免空条件造成无效的错觉
//                $where[] = ['id', '=', 0];
//            }
//        }

        return $where;
    }



    /**
     * 获取服务列表
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $params [输入参数]
     */
    public static function ServicesList($params = [])
    {

        $where = empty($params['where']) ? ['is_del'=>2] : $params['where'];
        $field = empty($params['field']) ? '*' : $params['field'];
        $page = $params['page'] ? true : false;
        $order_by = empty($params['order_by']) ? 'service_create desc' : trim($params['order_by']);

        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 10;

        $number=isset($params['number']) ? intval($params['number']) : 10;
        // 获取列表
        $data = Db::name('service')->where($where)->field($field)->order($order_by);
        //分页

        if($page)
        {
            $data=$data->paginate(10, false, ['query' => request()->param()]);
           /* dump($data);exit;*/
            $data=$data->toArray()['data'];

        }else{
            if ($n<1) {
                $data=$data->select();
            }else{
                $data=$data->limit($m, $n)->select();
            }

        }



        //return DataReturn('处理成功', 0, $data);
        return self::ServiceDataHandle($params, $data);
    }

    /**
     * 服务数据处理
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]                   $params [输入参数]
     * @param    [array]                   $data   [列表]
     */
    public static function ServiceDataHandle($params, $data)
    {
        if(!empty($data))
        {
            // 其它额外处理


            // 开始处理数据
            foreach($data as &$v)
            {

                if(isset($v['id']))
                {
                    # 是否首页展示
                    if(isset($v['service_top']))
                    {
                        $v['service_top_title']=($v['service_top'] >0) ? '是' : '否';
                    }

                    # 是否开启计时
                    if(isset($v['timer']))
                    {
                        $v['timer_title']=($v['timer'] >0) ? '是' : '否';
                    }

                                       # 服务二级分类
                    if(isset($v['service_class_id']))
                    {
                        $v['service_class_name']=Db::name('service_class')->where('id='.$v['service_class_id'])->value('service_class_title');
                    }
                    # 服务一级分类
                    if(isset($v['class_pid']))
                    {
                        $v['class_pid_name']=Db::name('service_class')->where('id='.$v['class_pid'])->value('service_class_title');
                    }

                    # 服务时长
                    if(isset($v['service_time']))
                    {
                        $v['service_time_show']=$v['service_time'].'小时';
                    }


                    # 上下架状态
                    if(isset($v['service_status']))
                    {
                        $v['service_status_title']=($v['service_status']>0) ? '上架' : '下架';
                        //$v['service_status_title']=$v['service_status'];
                    }

                    #推荐提成
                    if(isset($v['service_refer']))
                    {
                        $v['service_refer_show']=$v['service_refer']*100;
                    }



                    # 赠送余额 关联门店
                    if (isset($v['id']))
                    {

                        $v['giving_balance']='0.00';

                       $giving_balance=Db::name('service_giving')->where(['service_id'=>$v['id'],'giving_type'=>5])->group('service_id')->value('giving_number');
                       $giving_integral=Db::name('service_giving')->where(['service_id'=>$v['id'],'giving_type'=>4])->group('service_id')->value('giving_number');

                       $v['giving_balance']=floatval($giving_balance) >0 ? priceFormat($giving_balance) : $v['giving_balance'];
                       $v['giving_integral']=floatval($giving_integral) >0 ? priceFormat($giving_integral) : $v['giving_integral'];

                       //关联门店数量，门店id 门店结算价格 array
                       $biz_id=Db::name('service_biz')->alias('sb')
                           ->leftJoin(['biz'=>'b'],'b.id=sb.biz_id')
                           ->where('b.biz_type >1 and sb.service_id='.$v['id'])->field('sb.biz_id')->select();

                       $v['biz_id']='';
                       $v['biz_id_price']='';
                       foreach ($biz_id as $b)
                       {
                           $v['biz_id'].=$b['biz_id'].',';
                       }
                       $v['biz_id']=substr($v['biz_id'],0,-1);

                       $v['biz_id_price']=substr($v['biz_id_price'],0,-1);

                       $v['service_biz_num']=count($biz_id);
                       /**/
                        //门店价格:service_car_price_offline
                        $v['service_car_price_offline']= Db::name('service_car_mediscount')
                            ->alias('scm')->leftJoin(['car_level'=>'cl'],'scm.car_level_id=cl.id')
                            ->where([['scm.service_id','=',$v['id']],['biz_id','=',0],['service_type','=',2]])
                            ->field('scm.id service_car_mediscount_id,discount discount_biz,service_id,car_level_id,level_title')
                            ->select();

                        //线上价格:service_car_price_online
                        $v['service_car_price_online']=
                            Db::name('service_car_mediscount')
                                ->alias('scm')->leftJoin(['car_level'=>'cl'],'scm.car_level_id=cl.id')
                                ->where([['scm.service_id','=',$v['id']],['biz_id','=',0],['service_type','=',1]])
                                ->field('scm.id service_car_mediscount_id,discount discount_online,service_id,car_level_id,level_title')
                                ->select();

                        //会员折扣：service_mediscount
                        $v['service_mediscount']=Db::name('service_mediscount')
                            ->alias('sm')->leftJoin(['member_level'=>'ml'],'sm.member_level=ml.id')
                            ->where([['sm.service_id','=',$v['id']],])
                            ->field('sm.id,discount,member_level,level_title')
                            ->select();

                        //赠送服务:giving_service
                        $v['giving_service']=Db::name('service_giving')
                            ->alias('sg')->leftJoin(['service'=>'s'],'sg.giving_id=s.id')
                            ->where([['giving_type','=',1],['sg.service_id','=',$v['id']]])
                            ->field('sg.id,giving_number,s.service_title')
                            ->group('sg.service_id')
                            ->select();

                        //dump($v);exit;



                    }

                    //banner上传：service_picurl_arr
                    $v['service_picurl_arr']=strlen($v['service_picurl'])>0 ?explode(',',$v['service_picurl']):[];
                    //详情上传：service_context_arr
                    $v['service_context_arr']=strlen($v['service_context'])>0 ?explode(',',$v['service_context']):[];
//                    dump($v['service_context_arr']);exit;
                }

            }

        }
       return $data;
    }

    /**
     * 关联门店列表
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceBizList($params){
        $data = Db::name('service_biz')->alias('sb')
            ->leftJoin(['biz'=>'b'],'sb.biz_id=b.id')
            ->where([
                ['sb.service_id','=',$params['service_id'],],
                ['b.biz_type','>',1,]
            ])
            ->field('b.biz_title,b.biz_address,b.biz_phone,b.biz_leader')
            ->paginate(10, false, ['query' => request()->param()]);
        $data=$data->toArray();
        return DataReturn('获取成功',0,$data);
    }

    /**
     * 获取分类
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function ServiceCategory($params = [])
    {
        // 获取分类

        $where = empty($params['where']) ? [['is_del','=',2]] : $params['where'];

        $page = $params['page'] ? true : false;

        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 10;

        $number=isset($params['number']) ? intval($params['number']) : 10;

        // 获取列表
        $data = Db::name('service_class')->where($where)->field('*')->order('id desc');
        //分页
        $page_html=null;
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);

            $page_html=$data->render();

            $data=$data->toArray()['data'];

        }else{
            $data=$data->limit($m, $n)->select();
        }

        return self::ServiceCategoryDataDealWith($data,['page_html'=>$page_html]);




    }

    /**
     * 根据pid获取分类列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $where [条件]
     */
    public static function ServiceCategoryList($where = [])
    {

        $data = Db::name('service_class')->field('*')->where($where)->order('id asc')->find();
        return $data;
        //return self::GoodsCategoryDataDealWith($data);
    }
    /**
     * 分类数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $data [分类数据]
     */
    public static function ServiceCategoryDataDealWith($data,$params){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                if($v['service_class_pid'])
                {
                    $where_next['id'] = $v['service_class_pid'];
                    $v['service_class_level'] = self::ServiceCategoryList($where_next);
                    if(!empty($v['service_class_level']))
                    {
                        $v['service_class_level']=$v['service_class_level']['service_class_title'];
                    }
                }else{

                    $v['service_class_level']='最高级';
                }
            }
        }
        if($params['page_html']){
            $data['page_html']=$params['page_html'];
        }


        return DataReturn('处理成功', 0, $data);
    }



    /**
     * 分类保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceCategorySave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_class_title',
                'error_msg'         => '分类名称不能为空',
                'error_code'         => 20002,
            ],

        ];
        # 添加是判断名称是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'service_class',
                'key_name'          => 'service_class_title',
                'error_msg'         => '分类名称不能重复',
                'error_code'         => 20001,
            ];
        }

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);

            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        $data=$params;
        $data['service_outtime'] = sprintf("%.2f",$params['service_outtime']);
        // 添加/编辑
        if(empty($params['id']))
        {

            $data['service_class_create'] = TIMESTAMP;
            $class_id = Db::name('service_class')->insertGetId($data);
        } else {

            $data['service_class_update'] = TIMESTAMP;

            if(Db::name('service_class')->where(['id'=>intval($params['id'])])->update($data))
            {
                $class_id = $params['id'];
            }
        }
        $redis=new Redis();
        $redis->hDel("service_class","all");

        return json(DataReturn('保存成功', 0));
    }

    /**
     * 服务保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceStatusSave($params){
        //
        $where=self::GetAdminIndexWhere($params);
        $update=[];
        if (isset($params['action']['appoint'])) {
            $update['appoint']= $params['action']['appoint'];
        }

        if (isset($params['action']['service_top'])) {
            $update['service_top']= $params['action']['service_top'];
        }
//        dump($update);
//        dump($where);
//        exit;
        Db::name('service')->where($where)->update($update);
        return DataReturn('保存成功', 0);
    }
    /**
     * 获取最后一个服务id
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月14日14:22:21
     * @desc    description
     */
    public static function GetLastServiceId(){
        $key=Db::name('service')->order('id desc')->value('id');
        return $key;
    }

    /**
     * 获取缓存中的关联门店
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月14日14:22:21
     * @desc    description
     */
    public static function RedisBizInfo($id=''){

        if (empty($id))
        {
            $id=self::GetLastServiceId();
            $id=$id+1;
        }
        $redis=new Redis();
        $service_biz_arr=empty($redis->sMembers($id))?[]:$redis->sMembers($id);

//        dump($service_biz_arr);exit;
        $bizservice=new BizService();
        $where=[['id','in',$service_biz_arr]];
        $params=[
            'where'=>$where,
        ];
        $biz_arr=$bizservice->BizIndex($params);
        return DataReturn('获取成功', 0,$biz_arr);
    }

    /**
     * 获取门店数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function BizStr($param){
        $where=[['id','>',0]];
        if(!empty($param['keywords']))
        {
            $where[] = ['biz_title', 'like', '%'.$param['param']['keywords'].'%'];
        }
        $data_params = array(
            'where'     => $where,
            'table'=>'biz',
            'field'=>'id,biz_title,biz_address,biz_phone,biz_type',
            'order'=>'biz_create desc'
        );
        $data=BaseService::DataList($data_params);
        if (empty($data))
        {
            throw new BaseException(['code'=>403 ,'errorCode'=>60001,'msg'=>'获取门店数据失败','status'=>false,'debug'=>false ]);
        }
        return self::DealWithpro($data);
    }
    /**
     * 拼接门店数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param   [array]          $data [门店数据 二维数组]
     */
    public static function DealWithpro($param){
        $_str = '<div class="layui-card-body ">
  <button type="button" class="layui-btn">批量关联</button>
                        <table class="layui-table layui-form">
                        <thead>
                        <tr>
                            <th><input style="display: inherit" type="checkbox" name="all" ></th>
                            <th>门店名称</th>
                            <th>门店电话</th>
                            <th>门店类型</th>
                            <th>门店地区</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>';
        foreach ($param as $k => &$v)
        {
            switch ($v['biz_type']){
                case 1:
                    $v['biz_type_title']='自营';
                    break;
                case 2:
                    $v['biz_type_title']='加盟';
                    break;
                case 3:
                    $v['biz_type_title']='合作';
                    break;
            }
            $action_detail = '<a class="layui-btn layui-btn-danger layui-btn-radius" onclick="ChooseInfo(this)" >关联门店</a>';
            $_str .= "<tr style='cursor:pointer' id='trInfo".$v['id']."'>
                            <td><input style='display: inherit' data-id='".$v['id']."' type='checkbox' value='".$v['id']."'></td>
                            <td>".$v['biz_title']."</td>
                            <td>".$v['biz_phone']."</td>
                            <td>".$v['biz_type_title']."</td>
                            <td>".$v['biz_address']."</td>
                            <td>".$action_detail."</td>
                            </tr>";
        }
        $_str .= '</tbody></table></div>';
        return DataReturn('拼接成功','0',$_str);
    }



    /**
     * 拼接赠送服务字符串
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     */
    public static function ServiceListStr($list){

        $select_str='<select id="selectPointOfInterest" name="category_id"   onchange="searchRelevanceService()">';
        $option="<option value=''>全部</option>";
        foreach ($list['service_class'] as $scv)
        {
            $option.="<option value='".$scv['id']."'>".$scv['service_class_title']."</option>";
        }
        $select_str.=$option.'</select>';
        //dump($select_str);exit;
        $str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                        <div class=\"row\" style=\"width: 100%;float: left\">
                                            <div class=\"col-sm-6\">
                                                <div class=\"form-group\">
                                                ".$select_str."
                                                    <i class=\"form-group__bar\"></i>
                                                </div>
                                            </div>
                                        </div>
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            
                                        </div>
                                    </div>
                                    <table   class=\"table  table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                
                                                <th>服务名称</th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
        foreach($list['service'] as $k=>$v){
            $str.="<tr onclick='Chooseservice(this)'    data-id='".$v['id']."'>";
            $str.=" <td>{$v['service_title']}</td></tr>";
        }
        $str.="
                </tbody>
            </table>
        </div>
            </div>";
        return $str;
    }

    /**
     * 获取赠送服务数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     */
    public static function RelevanceServiceModel(){
        $list=[];
        $list['service_class']=Db::name('service_class')->field('service_class_title,id')->where('is_del = 2')->select();
        $list['service']=Db::name('service')->field('service_title,id')->where('is_del = 2')->select();

        $model= self::ServiceListStr($list);
        return DataReturn('ok','0',$model);
    }

    /**
     * 初始化服务数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     */
    public static function InitServiceInfo(){
        $data = [];
        //服务banner
        $data['service_picurl']='';
        //服务详情图
        $data['service_context']='';
        //赠送服务
        $data['giving_service']=[];
        //门店价格 线下价格
        $car_level=Db::name('car_level')->field('id car_level_id,id,level_title')->select();;
        $data['service_car_price_offline']=$car_level;
        $data['service_car_price_online']=$car_level;
        //会员折扣
        $data['service_mediscount']=Db::name('member_level')->field('level_title,id')->select();
        foreach ($data['service_mediscount'] as &$v) {
            $v['discount']=1;
        }
        return DataReturn('ok','0',$data);
    }

    /**
     * 执行保存服务数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function SaveInfo($params){
        //添加服务
        // 请求参数

        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_title',
                'error_msg'         => '标题不能为空',
                'error_code'         => 60002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_class_id',
                'error_msg'         => '分类不能为空',
                'error_code'         => 60003,
            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'service_time',
//                'error_msg'         => '服务时长不能为空',
//                'error_code'         => 60004,
//            ],

//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'service_refer',//门店价格
//                'error_msg'         => '推荐提成不能为空',
//                'error_code'         => 60005,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'service_commission',//线上价格
//                'error_msg'         => '服务提成不能为空',
//                'error_code'         => 60006,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'service_minprice',//线上价格
//                'error_msg'         => '最低提成价格不能为空',
//                'error_code'         => 60006,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'discount_type',//线上价格
//                'error_msg'         => '请选择折扣类型',
//                'error_code'         => 60007,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'relation_eva',//是否关联评价
//                'error_msg'         => '请选择是否关联评价',
//                'error_code'         => 60008,
//            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_image',//线上价格
                'error_msg'         => '缩略图不能为空',
                'error_code'         => 60008,
            ],


        ];
        if($params['is_parts'] == 2){
            $p[] = [
                'checked_type'      => 'empty',
                'key_name'          => 'service_offline_price',//门店价格
                'error_msg'         => '门店价格',
                'error_code'         => 60005,
            ];
//            $p[] =  [
//                'checked_type'      => 'empty',
//                'key_name'          => 'service_online_price',//线上价格
//                'error_msg'         => '线上价格',
//                'error_code'         => 60006,
//            ];
        }

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }



        //缓存清除在用的图片地址
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'service'.$params['id'];

        if (substr($params['service_picurl'],0,1)==',')
        {
            $params['service_picurl']=substr($params['service_picurl'],0);
        }
        if (substr($params['service_context'],0,1)==',')
        {
            $params['service_context']=substr($params['service_context'],0);
        }
        ResourceService::delCacheItem(explode(',',$params['service_picurl']));
        ResourceService::delCacheItem(explode(',',$params['service_context']));
        ResourceService::delCacheItem($params['service_advert']);
        ResourceService::delCacheItem($params['service_image']);
        #删除广告图字段
        if(isset($params['oldservice_advert']))
            unset($params['oldservice_advert']);
        $_redis=new Redis();


        Db::startTrans();
        // 添加/编辑
        if(empty($params['id']))
        {


             $service_title_re=Db::name('service')
                 ->where('service_title="'.$params['service_title'].'"')->value('id');

            if ($service_title_re>0)
            {
                throw new \BaseException(['code'=>403 ,'errorCode'=>60009,'msg'=>'服务名称已被占用','status'=>false,'debug'=>false]);
            }

            //service表
            $data_service=self::ClassifyData($params,'service');

            $params['service_id']=Db::name('service')->insertGetId($data_service);

            if ($params['service_id'])
            {
                #看是否存在缓存 关联服务id
                $serviceInfoId = Session('getJzServiceInfo');
                if(!empty($serviceInfoId)){
                    #修改关联服务id
                    $da = Db::table('service')->where(array('id'=>$params['service_id']))->update(array('service_pid'=>$serviceInfoId['service_id']));
                    if(!$da){
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60009,'msg'=>'修改关联服务数据失败','status'=>false,'debug'=>false]);
                    }

                }
                #创建卡券  添加一个服务 之后 同时创建一个卡券，默认有效期30天
                $card_voucher = Db::table('card_voucher')->insert(array(
                    'card_title'=>$params['service_title'].'券',
                    'card_detile'=>$params['service_title'].'服务卡券',
                    'card_time'=>90,
                    'card_price'=>$params['service_online_price'][0],
                    'card_create'=>date('Y-m-d H:i:s'),
                    'server_id'=>$params['service_id'],
                    'card_type_id'=>1
                ));
                if(!$card_voucher){
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60009,'msg'=>'添加服务卡券数据失败','status'=>false,'debug'=>false]);
                }

            }
            //service_biz表
            $data_service_biz=self::ClassifyData($params,'service_biz');
            if(!empty($data_service_biz)){
                $service_biz_re=Db::name('service_biz')->insertAll($data_service_biz);
                if (!$service_biz_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60010,'msg'=>'保存服务门店数据失败','status'=>false,'debug'=>false]);
                }
            }


            //service_giving表
            $data_service_giving=self::ClassifyData($params,'service_giving');
            if(!empty($data_service_giving)){
                $service_giving_re=Db::name('service_giving')->insertAll($data_service_giving);
                if (!$service_giving_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60011,'msg'=>'保存服务赠送数据失败','status'=>false,'debug'=>false]);
                }
            }

            //log_servicepro  表  关联商品表
            $data_log_servicepro=self::ClassifyData($params,'log_servicepro');
            if(!empty($data_log_servicepro)){
                $log_servicepro_re=Db::name('log_servicepro')->insertAll($data_log_servicepro);
                if (!$log_servicepro_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60014,'msg'=>'保存关联商品数据失败','status'=>false,'debug'=>false]);
                }
            }

            //log_servicepro  表   关联配件表
            $data_log_servicepropart=self::ClassifyData($params,'log_servicepropart');
            if(!empty($data_log_servicepropart)){
                $data_log_servicepropart_re=Db::name('log_servicepro')->insertAll($data_log_servicepropart);
                if (!$data_log_servicepropart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联配件数据失败', 'status' => false, 'debug' => false]);
                }
            }
            //service_log 服务关联 保养记录(保养名称、有效期、有效公里数)  ,维修记录(维修服务名称)  ,质保记录   (质保时间、质保公里数，质保范围填写)
            $data_service_logpart=self::ClassifyData($params,'service_log');

            #保养记录
            if(!empty($data_service_logpart['maintain']['log_type'])){
                $data_service_logpart_re=Db::name('service_log')
                    ->insert($data_service_logpart['maintain']);
                if (!$data_service_logpart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联保养记录数据失败', 'status' => false, 'debug' => false]);
                }
            }

            #维修记录
           if(!empty($data_service_logpart['repair']['log_type'])){
               $data_service_logpart_re=Db::name('service_log')->insert($data_service_logpart['repair']);
               if (!$data_service_logpart_re) {
                   Db::rollback();
                   throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联维修记录数据失败', 'status' => false, 'debug' => false]);
               }
           }
            #质保记录
            if(!empty($data_service_logpart['warranty']['log_type'])){
                   $data_service_logpart_re=Db::name('service_log')->insert($data_service_logpart['warranty']);
                   if (!$data_service_logpart_re) {
                       Db::rollback();
                       throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联质保记录数据失败', 'status' => false, 'debug' => false]);
                   }
               }

             //service_mediscount
            $data_service_mediscount=self::ClassifyData($params,'service_mediscount');

            $service_mediscount_re=Db::name('service_mediscount')->insertAll($data_service_mediscount);
            if(!$service_mediscount_re){
                #折扣类型
                session::delete('DiscountType');
                #服务折扣缓存
                session::delete('ServiceDiscountType');
                if (!$service_mediscount_re)
                {

                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60012,'msg'=>'保存会员折扣数据失败','status'=>false,'debug'=>false]);
                }
            }


            //service_car_mediscount表：加两次数据 门店价格 和线上价格

            //service_car_mediscount
            $data_service_car_mediscount_se=self::ClassifyData($params,'service_car_mediscount');

            if(!empty($data_service_car_mediscount_se)){
                $service_car_mediscount_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['offlinecarlevel1']);
                if (!$service_car_mediscount_se_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务价格数据失败，请填写门店价格','status'=>false,'debug'=>false]);
                }

                $onlinecarlevel2_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['onlinecarlevel2']);
                if (!$onlinecarlevel2_se_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务价格数据失败，请填写门店价格线上价格','status'=>false,'debug'=>false]);
                }
                if(!empty($data_service_car_mediscount_se['biz_arr_online'])){
                    $biz_arr_online_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['biz_arr_online']);
                    if (!$biz_arr_online_se_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务门店结算线上价格数据失败','status'=>false,'debug'=>false]);
                    }
                }
                if(!empty($data_service_car_mediscount_se['biz_arr_offline'])){
                    $biz_arr_offline_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['biz_arr_offline']);
                    if (!$biz_arr_offline_se_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务门店结算线下价格数据失败','status'=>false,'debug'=>false]);
                    }
                }


            }
            #创建服务时关联所有直营店
            getRelationServiceBiz('service', $params['service_id']);




           // return  self::MassInfo($params,$data);
        }
        else {
            //清除缓存服务价格
            $_redis->hDel("servicePrice",$params['id']);

            $params['service_id']=$params['id'];
            #看是否存在缓存 关联服务id
            $serviceInfoId = Session('getJzServiceInfo');
            if(!empty($serviceInfoId)){
                #修改关联服务id
                $params['serviceorder_id'] = $serviceInfoId['service_id'];


            }
            #修改卡券  修改一个服务 之后 同时修改卡券，默认有效期30天
            $card_voucher = Db::table('card_voucher')->where(array('server_id'=> $params['service_id'],'card_type_id'=>1))->update(array(
                'card_title'=>$params['service_title'].'券',
                'card_detile'=>$params['service_title'].'服务卡券',
                'card_price'=>$params['service_online_price'][0],
            ));

            //service表
            $data_service=self::ClassifyData($params,'service');
           // dump($params);exit;
            $service_re=Db::name('service')->where(['id'=>intval($params['id'])])->update($data_service);
            if (!$service_re)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>60009,'msg'=>'保存服务数据失败','status'=>false,'debug'=>false]);
            }

            //service_biz表
            $data_service_biz=self::ClassifyData($params,'service_biz');
            if(!empty($data_service_biz)){
                $service_biz_re=Db::name('service_biz')->insertAll($data_service_biz);
                if (!$service_biz_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60010,'msg'=>'保存服务门店数据失败','status'=>false,'debug'=>false]);
                }
            }
            //service_giving表:giving_type=1
            $data_service_giving=self::ClassifyData($params,'service_giving');
            if(!empty($params['service_id'])){
                #查询当前服务是否有赠送
                $balance = Db::name('service_giving')->where(['service_id'=>intval($params['service_id']),'giving_type'=>'5'])->value('id');
                if(!empty($balance)){
                    #修改余额
                    $service_giving_re=Db::name('service_giving')->where(['service_id'=>intval($params['service_id']),'giving_type'=>'5'])->update(array('giving_number'=>$params['balan']));
                }else{
                    #添加余额
                    if($params['balan'] > 0) {
                        $service_giving_re = Db::name('service_giving')->insert([
                            'giving_id' => 0,
                            'giving_type' => 5,
                            'giving_number' => $params['balan'],
                            'work_time' => 365,
                            'service_id' => $params['service_id'],
                        ]);
                        if (!$service_giving_re) {
                            Db::rollback();
                            throw new \BaseException(['code' => 403, 'errorCode' => 60011, 'msg' => '保存服务赠送余额数据失败', 'status' => false, 'debug' => false]);
                        }
                    }
                }
                $integral = Db::name('service_giving')->where(['service_id'=>intval($params['service_id']),'giving_type'=>'4'])->value('id');
                if(!empty($integral)){
                    #修改积分
                    $service_giving_re=Db::name('service_giving')->where(['service_id'=>intval($params['service_id']),'giving_type'=>'4'])->update(array('giving_number'=>$params['integral']));
                }else{
                    if($params['integral'] > 0) {
                        #添加积分
                        $service_giving_re = Db::name('service_giving')->insert([
                            'giving_id' => 0,
                            'giving_type' => 4,
                            'giving_number' => $params['integral'],
                            'work_time' => 365,
                            'service_id' => $params['service_id'],
                        ]);
                        if (!$service_giving_re) {
                            Db::rollback();
                            throw new \BaseException(['code' => 403, 'errorCode' => 60011, 'msg' => '保存服务赠送积分数据失败', 'status' => false, 'debug' => false]);
                        }
                    }
                }
                #修改余额  积分



            }
            else{
                #添加 余额 积分
                if(!empty($data_service_giving)){
                    #修改余额
                    /*$service_giving_re=Db::name('service_giving')->where(['service_id'=>intval($params['service_id']),'giving_type'=>'5'])->update(array('giving_number'=>$params['balan']));
                    if (!$service_giving_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60011,'msg'=>'保存服务赠送数据失败','status'=>false,'debug'=>false]);
                    }*/
                    #添加赠送服务
                    $service_giving_re=Db::name('service_giving')->insertAll($data_service_giving);
                    if (!$service_giving_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60011,'msg'=>'保存服务赠送数据失败','status'=>false,'debug'=>false]);
                    }
                }
            }


            //log_servicepro  表  关联商品表
            $data_log_servicepro=self::ClassifyData($params,'log_servicepro');
            if(!empty($data_log_servicepro)){
                $log_servicepro_re=Db::name('log_servicepro')->insertAll($data_log_servicepro);
                if (!$log_servicepro_re)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>60014,'msg'=>'保存关联商品数据失败','status'=>false,'debug'=>false]);
                }
            }
            //service_log 服务关联 保养记录(保养名称、有效期、有效公里数)  ,维修记录(维修服务名称)  ,质保记录   (质保时间、质保公里数，质保范围填写)
            $data_service_logpart=self::ClassifyData($params,'service_log');
            #保养记录

            if(!empty($data_service_logpart['maintain']['log_type'])){
                #查询 是否 有 有 修改  没有 创建
                $res = Db::table('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>1))->find();
                if(!empty($res)){
                    $data_service_logpart_re=Db::name('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>1))->update($data_service_logpart['maintain']);
                }else{
                    $data_service_logpart_re=Db::name('service_log')->insert($data_service_logpart['maintain']);
                }

                if (!$data_service_logpart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联保养记录数据失败', 'status' => false, 'debug' => false]);
                }
            }

            #维修记录
            if(!empty($data_service_logpart['repair']['log_type'])){
                $res = Db::table('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>2))->find();
                if(!empty($res)){
                    $data_service_logpart_re=Db::name('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>2))->update($data_service_logpart['repair']);

                }else{
                    $data_service_logpart_re=Db::name('service_log')->insert($data_service_logpart['repair']);
                }
                if (!$data_service_logpart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联维修记录数据失败', 'status' => false, 'debug' => false]);
                }
            }
            #质保记录
            if(!empty($data_service_logpart['warranty']['log_type'])){
                $res = Db::table('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>3))->find();
                if(!empty($res)){
                    $data_service_logpart_re=Db::name('service_log')->where(array('service_id'=>$params['service_id'],'log_type'=>3))->update($data_service_logpart['warranty']);

                }else{
                    $data_service_logpart_re=Db::name('service_log')->insert($data_service_logpart['warranty']);
                }

                if (!$data_service_logpart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联质保记录数据失败', 'status' => false, 'debug' => false]);
                }
            }

            //log_servicepro  表   关联配件表
            $data_log_servicepropart=self::ClassifyData($params,'log_servicepropart');
            if(!empty($data_log_servicepropart)){
                $data_log_servicepropart_re=Db::name('log_servicepro')->insertAll($data_log_servicepropart);
                if (!$data_log_servicepropart_re) {
                    Db::rollback();
                    throw new \BaseException(['code' => 403, 'errorCode' => 60014, 'msg' => '保存关联配件数据失败', 'status' => false, 'debug' => false]);
                }
            }

            //service_mediscount
         /*   $data_service_mediscount=self::ClassifyData($params,'service_mediscount');
            $service_mediscount_re=Db::name('service_mediscount')->insertAll($data_service_mediscount);
            if (!$service_mediscount_re)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>60012,'msg'=>'保存会员折扣数据失败','status'=>false,'debug'=>false]);
            }*/
            //service_mediscount表 折扣

            $data_service_mediscount=self::ClassifyData($params,'service_mediscount');

            if(!empty($data_service_mediscount)){
                foreach ($data_service_mediscount as $dsmk=>$dsmv)
                {


                    if($dsmv['member_level'] == 1){
                        $a = Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>1))->update($dsmv);

                    }else if($dsmv['member_level'] == 2){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>2))->update($dsmv);

                    }else if($dsmv['member_level'] == 3){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>3))->update($dsmv);

                    }else if($dsmv['member_level'] == 4){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>4))->update($dsmv);

                    }else if($dsmv['member_level'] == 5){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>5))->update($dsmv);

                    }else if($dsmv['member_level'] == 6){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>6))->update($dsmv);

                    }else if($dsmv['member_level'] == 7){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>7))->update($dsmv);

                    }else if($dsmv['member_level'] == 8){
                        Db::name('service_mediscount')->where(array('service_id'=>$params['service_id'],'member_level'=>8))->update($dsmv);

                    }

                }
            }
            //service_car_mediscount 服务车型价格(大型车,中型车,小型车)
            $data_service_car_mediscount_se=self::ClassifyData($params,'service_car_mediscount');


            if(!empty($data_service_car_mediscount_se)){

                #服务自己的价格 线下 修改
                if(!empty($data_service_car_mediscount_se['offlinecarlevel1'])){
                    #查询是否有自己的服务价格、
                    $price = Db::table('service_car_mediscount')->where(array('service_type'=>2,'service_id'=>$params['service_id'],'biz_id'=>0))->select();

                    if(!empty($price)){
                        #修改大型车 价格
                        $car_dprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>3,'service_id'=>$params['service_id'],'service_type'=>2))
                            ->update(array('discount'=>$data_service_car_mediscount_se['offlinecarlevel1'][0]['discount']));

                        #修改中型车 价格
                        $car_zprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>2,'service_id'=>$params['service_id'],'service_type'=>2))
                            ->update(array('discount'=>$data_service_car_mediscount_se['offlinecarlevel1'][1]['discount']));

                        #修改小型车 价格
                        $car_xprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>1,'service_id'=>$params['service_id'],'service_type'=>2))
                            ->update(array('discount'=>$data_service_car_mediscount_se['offlinecarlevel1'][2]['discount']));

                    }
                    else{
                        #添加
                        $service_car_mediscount_se_re=Db::name('service_car_mediscount')
                            ->insertAll($data_service_car_mediscount_se['offlinecarlevel1']);
                        if (!$service_car_mediscount_se_re)
                        {
                            Db::rollback();
                            throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务价格数据失败','status'=>false,'debug'=>false]);
                        }
                    }

                }
                #服务自己的价格 线上 修改
                if(!empty($data_service_car_mediscount_se['onlinecarlevel2'])){
                    #查询是否有自己的服务价格、
                    $price = Db::table('service_car_mediscount')->where(array('service_type'=>1,'service_id'=>$params['service_id'],'biz_id'=>0))->select();
                    if(!empty($price)){
                        #修改大型车 价格 'biz_id'=>0,
                        $car_dprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>3,'service_id'=>$params['service_id'],'service_type'=>1))
                            ->update(array('discount'=>$data_service_car_mediscount_se['onlinecarlevel2'][0]['discount']));

                        #修改中型车 价格
                        $car_zprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>2,'service_id'=>$params['service_id'],'service_type'=>1))
                            ->update(array('discount'=>$data_service_car_mediscount_se['onlinecarlevel2'][1]['discount']));

                        #修改小型车 价格
                        $car_xprice=Db::name('service_car_mediscount')
                            ->where(array('car_level_id'=>1,'service_id'=>$params['service_id'],'service_type'=>1))
                            ->update(array('discount'=>$data_service_car_mediscount_se['onlinecarlevel2'][2]['discount']));

                    }else{
                        $onlinecarlevel2_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['onlinecarlevel2']);
                        if (!$onlinecarlevel2_se_re)
                        {
                            Db::rollback();
                            throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务价格数据失败','status'=>false,'debug'=>false]);
                        }
                    }
                }

                #门店 结算价格 线上
                if(!empty($data_service_car_mediscount_se['biz_arr_online'])){
                    $biz_arr_online_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['biz_arr_online']);
                    if (!$biz_arr_online_se_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务门店结算线上价格数据失败','status'=>false,'debug'=>false]);
                    }
                }

                #门店 结算价格 线下
                if(!empty($data_service_car_mediscount_se['biz_arr_offline'])){

                    $biz_arr_offline_se_re=Db::name('service_car_mediscount')->insertAll($data_service_car_mediscount_se['biz_arr_offline']);
                    if (!$biz_arr_offline_se_re)
                    {
                        Db::rollback();
                        throw new \BaseException(['code'=>403 ,'errorCode'=>60013,'msg'=>'保存服务门店结算线下价格数据失败','status'=>false,'debug'=>false]);
                    }
                }

            }


        }
        //dump($data_service_car_mediscount_se['biz_arr_offline']);exit;
        Db::commit();
        return DataReturn('保存成功', 0);

    }

    /**
     * 将数据进行分类
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     * @param   [array]          $params [输入参数]
     * @param   [string]          $table [数据表名]
     */
    public static function ClassifyData($params,$table){
        $data=[];
        switch ($table){
            case 'service':
                $data= self::FormServiceData($params);
                break;
            case 'service_biz' :
               $data=self::FormServiceBizData($params);
                break;
            case 'service_car_mediscount':
                $data=self::FormServiceCarMediscountData($params);
                break;
            case 'service_giving':
                $data=self::FormServiceGivingData($params);
//                dump($data);exit;
                break;
            case 'service_mediscount':
                 $data=self::FormServiceMediscountData($params);
                break;
            case 'log_servicepro':
                $data=self::FormServiceLogServiceproData($params);
                break;
            case 'log_servicepropart':
                $data=self::FormServiceLogServiceprotypeData($params);
                break;
            case 'service_log':
                $data=self::FormServiceLogservicelogData($params);
                break;



        }
        return $data;
    }
    /*
     * 组建service表数据
     */
    public static function FormServiceData($params){
        $params['class_pid'] = $params['service_class_id'];
        if($params['service_refer'] != 0 and $params['service_refer']>=1){
            $params['service_refer'] =   $params['service_refer']/100;
        }
        if($params['service_commission'] != 0 and $params['service_commission'] >= 1){
            $params['service_commission'] =   $params['service_commission']/100;
        }
        if(empty($params['service_minprice'])){
            $params['service_minprice'] =   0;
        }


        if(!empty($params['second_class_id'])){
            #证明选择二级分类了

            $params['service_class_id'] = $params['second_class_id'];
        }
        # 判断是否为门店创建的
        $bizPid = Db::table('service')->where(array('id'=>$params['id']))->value('biz_pid');
        if(!empty($bizPid)){
            if(empty($params['serviceorder_id'])){
                $params['serviceorder_id'] = 9;
            }
        }

        $data=[
            'service_picurl'=>substr($params['service_picurl'],0,1)==',' ? substr($params['service_picurl'],1):substr($params['service_picurl'],0),
            'service_context'=>substr($params['service_context'],0,1)==',' ? substr($params['service_context'],1):substr($params['service_context'],0),
            'service_title'=>$params['service_title'],
            'service_top'=>$params['service_top'],
            'service_keyborder'=>$params['service_keyborder'],
            'service_class_id'=>$params['service_class_id'],
            'class_pid'=>$params['class_pid'],
            'service_time'=>$params['service_time'],
            'service_statement'=>$params['service_statement'],
            'service_refer'=>$params['service_refer'],
            'service_minprice'=>$params['service_minprice'],
            'timer'=>$params['timer'],
            'service_status'=>$params['service_status'],
            'service_image'=>$params['service_image'],
            'service_advert'=>$params['service_advert'],
            'is_relation'=>$params['is_relation'],
            'service_pid'=>$params['serviceorder_id'],
            'discount_type'=>$params['discount_type'],
            'service_commission'=>$params['service_commission'],
            'service_update'=>TIMESTAMP,
            'is_warranty'=>$params['is_warranty'],
            'is_maintain'=>$params['is_maintain'],
            'is_repair'=>$params['is_repair'],
            'repeat_order'=>$params['repeat_order'],
            'repeat_cycle'=>$params['repeat_cycle'],
            'relation_eva'=>$params['relation_eva'],
        ];


//        $sp_re=ResourceService::DelOssFile($params['service_picurl_old'],$params['service_picurl']);
//        $sc_re=ResourceService::DelOssFile($params['service_context_old'],$params['service_context']);
//        if ($sp_re&&$sc_re)
//        {
//
//        }
        return $data;
    }
    /*
     * 组建service_biz表数据
     */
    public static function FormServiceBizData($params)
    {
        if($params['service_refer'] != 0 and $params['service_refer'] >= 1){
            $params['service_refer'] =   $params['service_refer']/100;
        }
        if($params['service_commission'] != 0 and $params['service_commission'] >= 1){
            $params['service_commission'] =   $params['service_commission']/100;
        }
        if(empty($params['service_minprice'])){
            $params['service_minprice'] =   0;
        }
        $biz_id_arr = explode(',',$params['biz_id']);
        if(!empty($biz_id_arr[0])){
            foreach ($biz_id_arr as $k=>$v) {
                $biz_id_arr[$k]=[
                    'appoint'=>0,
                    'biz_id'=>$v,
                    'service_id'=>$params['service_id'],
                    'service_minprice'=>$params['service_minprice'],
                    'timer'=>$params['timer']=='on'?1:0,
                    'service_refer'=>$params['service_refer'],

                ];
            }
            return $biz_id_arr;

        }


    }
    /*
     * 组建service_car_mediscount表数据
     */
    public static function FormServiceCarMediscountData($params){
        $data=[];
        #先看 是否  关联门店  $params['biz_id']  关联 的话说明  就有结算价格
        #获取门店id
        $biz_id = $params['biz_id'];
        $_biz_id_arr = explode(',',$biz_id);
        $biz_arr_online = array();
        $biz_arr_offline = array();

        if(!empty($biz_id)){
            $biz_id_price =$params['biz_id_price'];
            #拆分结算价格  获取价格   获取门店id  价格 数组， 先按 线上和线下结算价格相同 计算
            $biz_id_price_arr = explode(',',$biz_id_price);
            #门店数组
            foreach($_biz_id_arr as $bzk=>$bzv){
                #门店价格数组
                foreach($biz_id_price_arr as $bk=>$bv){
                    $price = explode('-',$bv);
                    if($price['1'] == '3d'){

                        #大型车
                        $car = 3;
                        #线上 service_online_price
                        $biz_arr_online[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_online[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_online[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_online[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][0]) ? 0 : $params['service_offline_price'][0];
                        $biz_arr_online[$bzk.$price['1']]['service_type'] = 1;
                        if($bzv == $price[0])
                            $biz_arr_online[$bzk.$price['1']]['settlement_amount'] = $price[2];


                        #线下
                        $biz_arr_offline[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_offline[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_offline[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_offline[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][0]) ? 0 : $params['service_offline_price'][0];
                        $biz_arr_offline[$bzk.$price['1']]['service_type'] = 2;
                        if($bzv == $price[0])
                            $biz_arr_offline[$bzk.$price['1']]['settlement_amount'] = $price[2];


                    }
                    else if($price['1'] == '2z'){
                        #中型车
                        $car = 2;
                        #线上
                        $biz_arr_online[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_online[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_online[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_online[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][1]) ? 0 : $params['service_offline_price'][1];
                        $biz_arr_online[$bzk.$price['1']]['service_type'] = 1;
                        if($bzv == $price[0])
                            $biz_arr_online[$bzk.$price['1']]['settlement_amount'] = $price[2];

                        #线下
                        $biz_arr_offline[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_offline[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_offline[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_offline[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][1]) ? 0 : $params['service_offline_price'][1];
                        $biz_arr_offline[$bzk.$price['1']]['service_type'] = 2;
                        if($bzv == $price[0])
                            $biz_arr_offline[$bzk.$price['1']]['settlement_amount'] = $price[2];

                    }
                    else{

                        #小型车
                        $car = 1;
                        #线上
                        $biz_arr_online[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_online[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_online[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_online[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][2]) ? 0 : $params['service_offline_price'][2];
                        $biz_arr_online[$bzk.$price['1']]['service_type'] = 1;
                        if($bzv == $price[0])
                            $biz_arr_online[$bzk.$price['1']]['settlement_amount'] = $price[2];


                        #线下
                        $biz_arr_offline[$bzk.$price['1']]['biz_id'] = $bzv;
                        $biz_arr_offline[$bzk.$price['1']]['car_level_id'] = $car;
                        $biz_arr_offline[$bzk.$price['1']]['service_id'] = $params['service_id'];
                        $biz_arr_offline[$bzk.$price['1']]['discount'] = empty($params['service_offline_price'][2]) ? 0 : $params['service_offline_price'][2];
                        $biz_arr_offline[$bzk.$price['1']]['service_type'] = 2;
                        if($bzv == $price[0])
                            $biz_arr_offline[$bzk.$price['1']]['settlement_amount'] = $price[2];


                    }

                }
            }
        }

        #关联门店时 线上  结算价格数组
        $data['biz_arr_online'] = $biz_arr_online;
        #关联门店时 线下  结算价格数组
        $data['biz_arr_offline'] = $biz_arr_offline;

/*************************************  存自己平台的车型 服务价格 start *********************************************************/
        #门店价格  线下价格： service_offline_price   线上价格 ： service_online_price
        $car_level_arr = $params['car_level_id'];
        foreach ($car_level_arr as $k=>$v){
            if($k == 0){
                #大型车 线下
                $data['offlinecarlevel1'][0]['discount'] = empty($params['service_offline_price'][0]) ? 0 : $params['service_offline_price'][0];
                $data['offlinecarlevel1'][0]['car_level_id'] =$v;
                $data['offlinecarlevel1'][0]['service_id'] =$params['service_id'];
                $data['offlinecarlevel1'][0]['service_type'] =2;
                $data['offlinecarlevel1'][0]['biz_id'] ='0';

                #大型车 线上
                $data['onlinecarlevel2'][0]['discount'] =empty($params['service_offline_price'][0]) ? 0 : $params['service_offline_price'][0];
                $data['onlinecarlevel2'][0]['car_level_id'] =$v;
                $data['onlinecarlevel2'][0]['service_id'] =$params['service_id'];
                $data['onlinecarlevel2'][0]['service_type'] =1;
                $data['onlinecarlevel2'][0]['biz_id'] ='0';


            }
            if($k == 1){
                #中型车线下
                $data['offlinecarlevel1'][1]['discount'] =empty($params['service_offline_price'][1]) ? 0 : $params['service_offline_price'][1];
                $data['offlinecarlevel1'][1]['car_level_id'] =$v;
                $data['offlinecarlevel1'][1]['service_id'] =$params['service_id'];
                $data['offlinecarlevel1'][1]['service_type'] =2;
                $data['offlinecarlevel1'][1]['biz_id'] ='0';
                #中型车 线上
                $data['onlinecarlevel2'][1]['discount'] =empty($params['service_offline_price'][1]) ? 0 : $params['service_offline_price'][1];
                $data['onlinecarlevel2'][1]['car_level_id'] =$v;
                $data['onlinecarlevel2'][1]['service_id'] =$params['service_id'];
                $data['onlinecarlevel2'][1]['service_type'] =1;
                $data['onlinecarlevel2'][1]['biz_id'] ='0';
            }
            if($k == 2){
                #小型车线下
                $data['offlinecarlevel1'][2]['discount'] =empty($params['service_offline_price'][2]) ? 0 : $params['service_offline_price'][2];
                $data['offlinecarlevel1'][2]['car_level_id'] =$v;
                $data['offlinecarlevel1'][2]['service_id'] =$params['service_id'];
                $data['offlinecarlevel1'][2]['service_type'] =2;
                $data['offlinecarlevel1'][2]['biz_id'] ='0';
                #小型车线上
                $data['onlinecarlevel2'][2]['discount'] =empty($params['service_offline_price'][2]) ? 0 : $params['service_offline_price'][2];
                $data['onlinecarlevel2'][2]['car_level_id'] =$v;
                $data['onlinecarlevel2'][2]['service_id'] =$params['service_id'];
                $data['onlinecarlevel2'][2]['service_type'] =1;
                $data['onlinecarlevel2'][2]['biz_id'] ='0';
            }
        }
        /************************************* 存自己平台的服务价格end *********************************************************/


       /* for($clak=1;$clak<=count($car_level_arr);$clak++){
            for($bizidk=1;$bizidk<=count($params['biz_id']);$bizidk++)
            {
//                        //线下价格
                $data[randCodes(6)] =[
                    'service_id'=>$params['service_id'],
                    'car_level_id'=>$car_level_arr[$clak-1],
                    'discount'=>$params['service_offline_price'][$clak-1],
                    'service_type'=>2,
                    'biz_id'=>$params['biz_id'][$bizidk-1]
                ];
//                        //线上价格
                $data[randCodes(10)] =[
                    'service_id'=>$params['service_id'],
                    'car_level_id'=>$car_level_arr[$clak-1],
                    'discount'=>$params['service_online_price'][$clak-1],
                    'service_type'=>1,
                    'biz_id'=>$params['biz_id'][$bizidk-1]
                ];
            }

        }*/
        return $data;
    }

    /*
    * 组建service_giving表数据
    */
    public static function FormServiceGivingData($params){
       #看缓存中是否有
        $data = session('giveJzServiceInfo');
        if(!empty($data)){
            foreach($data as $k=>$v){
                $data[$k]['service_id']= $params['service_id'];
                if(isset($v['prize_title']))
                    unset($data[$k]['prize_title']);
            }


        }
        if(!empty($params['service_id'])){
            #赠送余额
            if (intval($params['balan'])>0) {
                $data[]=[
                    'giving_id'=>0,
                    'giving_type'=>5,
                    'giving_number'=>$params['balan'],
                    'work_time'=>365,
                    'service_id'=>$params['service_id'],
                ];
            }
            if (intval($params['integral'])>0) {
                $data[]=[
                    'giving_id'=>0,
                    'giving_type'=>4,
                    'giving_number'=>$params['integral'],
                    'work_time'=>365,
                    'service_id'=>$params['service_id'],
                ];
            }
        }
        session::delete('giveJzServiceInfo');
        return $data;
    }
    /*
    * 组建 log_servicepro   表数据 关联商品
    */
    public static function FormServiceLogServiceproData($params){

       #看缓存中是否有
        $data = session('bizProInfoSession');
        if(!empty($data)){
            foreach($data as $k=>$v){
                $data[$k]['service_id']= $params['service_id'];
                $data[$k]['pro_id']= $v['id'];
                    unset(
                        $data[$k]['biz_pro_title'],
                        $data[$k]['class_title'],
                        $data[$k]['biz_pro_class_id'],
                        $data[$k]['biz_pro_price'],
                        $data[$k]['biz_pro_number'],
                        $data[$k]['biz_pro_count'],
                        $data[$k]['biz_pro_purch'],
                        $data[$k]['supply_price'],
                        $data[$k]['id']
                    );
            }
        }
        session::delete('bizProInfoSession');

        return $data;
    }
    /*
      * 组建 log_servicepro   表数据  关联配件商品
      */
    public static function FormServiceLogServiceprotypeData($params){
        $data='';
        if($params['is_parts'] == 1){
            #看缓存中是否有
            $data = session('bizProInfoSession2');
            if(!empty($data)){
                foreach($data as $k=>$v){
                    $data[$k]['service_id']= $params['service_id'];
                    $data[$k]['pro_id']= $v['id'];
                    $data[$k]['biz_pro_paret']= 2;
                    unset(
                        $data[$k]['biz_pro_title'],
                        $data[$k]['class_title'],
                        $data[$k]['biz_pro_class_id'],
                        $data[$k]['biz_pro_price'],
                        $data[$k]['biz_pro_number'],
                        $data[$k]['biz_pro_count'],
                        $data[$k]['biz_pro_purch'],
                        $data[$k]['supply_price'],
                        $data[$k]['id']
                    );

                }


            }
        }


        session::delete('bizProInfoSession2');

        return $data;
    }
    /*
     * 组建 service_log   表数据  服务关联 保养记录(保养名称、有效期、有效公里数)  ,维修记录(维修服务名称)  ,质保记录   (质保时间、质保公里数，质保范围填写)
     */
    public static function FormServiceLogservicelogData($params){
        $data=[];
        #看缓存中是否有
        #保养记录
        $data['maintain'] = session('getJzServiceLogInfo1');
        $data['repair'] = session('getJzServiceLogInfo2');
        $data['warranty'] = session('getJzServiceLogInfo3');
        foreach($data as $k=>$v){
            $data[$k]['service_id']= $params['service_id'];
            $data[$k]['log_type']= $v['type'];
            unset(  $data[$k]['type']);



        }


        session::delete('getJzServiceLogInfo1');
        session::delete('getJzServiceLogInfo2');
        session::delete('getJzServiceLogInfo3');

        return $data;
    }
    /*
   * 组建service_mediscount表数据
   */
    public static function FormServiceMediscountData($params){
        #
        $data = session('ServiceDiscountType');


        if(!empty($data)){
            foreach($data as $k=>$v){
                $data[$k]['service_id']= $params['service_id'];
                $data[$k]['member_level']= $v['level_id'];
                $data[$k]['discount_type']=  $v['type'];
                    unset($data[$k]['level_id'],$data[$k]['type'],$data[$k]['level_id']);
            }


        }

        return $data;
    }

    /**
     *  serviceSession 服务
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    static function serviceSession($params,$serviceId=''){
        $info = array();
        $info['service_title'] = $params['bstitle'];
        $info['service_id'] = $params['pro_service_id'];

        Session::set('getJzServiceInfo', $info);

        $_str = '';
     /*   # 判断是否存在$privilegeId
        if(!empty($serviceId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($privilegeId);

            $_str= self::getPrivilegeInfo($pro, 'DB');

        }*/
        /*getActiveArrInfo*/
        $_sessionActiveTypeInfo = Session('getJzServiceInfo');

        $_str .= self::getServiceInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getServiceInfo($arr,$mark){

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

                $_str .= "<tr id='serviceDetailTr" . $arr['service_id'] . "'>
                            <td ><input type='hidden' value='".$arr['service_id']."' name='service_pid' >" . $arr['service_title'] . "</td>
                        
                             
                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick=\"delServiceInfo('" . $arr['service_id'] . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
        }
        return $_str;

    }
    /*
     * @content:查询服务二级分类
     * @params: $service_class_id 服务id   $second_classs_id: 二级分类id
     * */

    static function getServicePrice($service_class_id='',$second_classs_id=''){

        if(!empty($service_class_id)){
            $data = Db::table('service_class')->field('id,service_class_title')->where("service_class_pid = $service_class_id")->select();
            $str ='<option value="">请选择服务二级分类</option>';
            if(!empty($data)){

                foreach($data as $k=>$v){

                    $str.=" <option value=\"{$v['id']}\"";
                    if(!empty($second_classs_id) and $second_classs_id==$v['id']){
                        $str.=" selected";
                    }
                    $str.=" >".$v['service_class_title']."</option>";
                }

            }
            return $str;

        }else{
            $str ='';
            return $str;
        }

    }
    /**
     * @content 服务  商品  会员 积分 余额 存session
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    static function giveServiceSession($params,$serviceId){
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
         if($type == 'service'){
            $info['giving_id']=$params['pro_service_id'];

            #服务
            $info['prize_title']=$params['prize_title'];

            #奖励类型 5 服务
            $info['giving_type']=1;
            #奖品数量
            $info['giving_number']=$params['giving_number'];

            #有效期
            $info['work_time']=$params['work_time'];



        }

        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('giveJzServiceInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        if(!empty($_sessionActiveTypeInfo)){
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' .$type. $info['giving_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' .$type. $info['giving_id']]['giving_number'] = intval($_sessionActiveTypeInfo['S' .$type. $info['giving_id']]['giving_number']) + intval($info['giving_number']);
            } else {

                $_sessionActiveTypeInfo['S' . $type.$info['giving_id']] = $info;
            }
        }else{
            $_sessionActiveTypeInfo['S' .$type. $info['giving_id']] = $info;

        }

        Session::set('giveJzServiceInfo', $_sessionActiveTypeInfo);
        $_str = '';
        # 判断是否存在$taskId
        if(!empty($serviceId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::giveServiceContent($serviceId);

            $_str= self::giveServicePrizeInfo($pro, 'DB');

        }
        /*getActiveArrInfo*/

        $_str .= self::giveServicePrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }
    /**
     * @param $type  1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function giveServiceTypeList(){
        $_sessionActiveTypeInfo = Session('giveJzServiceInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        #获取服务id 赠送服务 修改时存
        $serviceId = Session('serviceId');
        # 判断是否存在 $serviceId
        if(!empty($serviceId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::giveServiceContent($serviceId);

            $_str= self::giveServicePrizeInfo($pro, 'DB');

        }
        /*getActiveArrInfo*/

        $_str .= self::giveServicePrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;


    }
    /**
     * @param $serviceId 服务id
     * @content 查询服务
     */
    static function giveServiceContent($serviceId){
        $task_prize = Db::table('service_giving')->where(array('service_id'=>$serviceId,'giving_type'=>'1'))->select();
        foreach ($task_prize as $k=>$v) {
            #服务 名称
            $title= Db::table('service')->field('service_title')->where(array('id'=>$v['service_id']))->find();
            $task_prize[$k]['prize_title'] = $title['service_title'];

        }
        return $task_prize;

    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function giveServicePrizeInfo($arr,$mark){

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {

                $_str .= "<tr id='serviceDetailTr" . $v['id'] . "'>
                            <td >" . $v['prize_title'] . "</td>
                            <td onclick=\"giveServiceWrite('" . $v['id'] . "','" . $k . "','" . $v['giving_number'] . "','" .$mark . "','2','giveService')\">" . $v['giving_number'] . "</td>
                            <td onclick=\"giveServiceWrite('" . $v['id'] . "','" . $k . "','" . $v['work_time'] . "','" .$mark . "','3','giveService')\">" . $v['work_time'] . "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delServiceInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','giveService')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;
    }

    /**
     * 推荐服务列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月14日14:22:21
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecommendServiceList($params)
    {
        $params['table']='service_recommend';
        $data=BaseService::DataList($params);

        return self::RecommendServiceHandle($data);
    }
    /**
     * 推荐服务数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日14:08:45
     * @desc    description
     * @param   [array]          $data [推荐服务数据]
     */
    public static function RecommendServiceHandle($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                 //D、服务分类、
                if (isset($v['service_class_pid']))
                {
                    $v['class_pid_title']=Db::name('service_class')->where('id='.$v['service_class_pid'])->value('service_class_title');
                }

                if (isset($v['position']))
                {
                    $v['position_title']=BaseService::StatusHtml($v['position'],lang('recommond_serviced_position'),false);
                }
                //当前状态（显示中、待上架、已下架）， 'sell_status'  =>[1=>'未上架', 2=>'已下架',3=>'展示中']
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                   // dump($v['start_time']>TIMESTAMP);exit;
                    $v['status']=3;
                    if (strtotime($v['start_time'])>time())
                    {
                        $v['status']=1;
                    }
                    if (strtotime($v['end_time'])<time())
                    {
                        $v['status']=2;
                    }

                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('sell_status'),false);
                    $v['show_time']=$v['start_time'].'至'.$v['end_time'];
                }


            }
        }

        return DataReturn('处理成功', 0, $data);
    }
    /**
     * 推荐服务查询条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月14日14:22:21
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecommendServiceWhere($params){
        $where=[];


//        if(isset($params['class_pid'])&&intval($params['class_pid'])>0)
//        {
//
//            $where[] = ['service_class_pid', '=', $params['class_pid']];
//        }

        //服务分类
        if(isset($params["param"]['service_class_pid'])&&intval($params["param"]['service_class_pid'])>0)
        {
            $where=[];
            $where[] = ['service_class_pid', '=', $params["param"]['service_class_pid']];
        }
        if(isset($params["param"]['position'])&&intval($params["param"]['position'])>0)
        {

            $where[] = ['position', '=', $params["param"]['position']];
        }
        //dump($where);exit;
        return $where;
    }
    /**
     * 保存推荐服务
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecommendServiceSaveData($params){
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'position',
                'error_msg'         => '位置不能为空',
                'error_code'         => 60006,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_class_pid',
                'error_msg'         => '分类不能为空',
                'error_code'         => 60007,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_id',
                'error_msg'         => '请选择服务',
                'error_code'         => 60011,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '开始时间不能为空',
                'error_code'         => 60012,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'end_time',
                'error_msg'         => '结束时间不能为空',
                'error_code'         => 60013,
            ],
        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }


        $data=$params;

        self::RecommendServiceDataCheck($data);

        // 添加/编辑
        if(empty($params['id']))
        {
            return   $news_id = Db::name('service_recommend')->insertGetId($data);
        } else {

            $re= Db::name('service_recommend')->where(['id'=>intval($params['id'])])->update($data);
            if (!$re)
            {
                throw new \BaseException(['code'=>403 ,'errorCode'=>20014,'msg'=>'保存推荐数据失败','status'=>false,'debug'=>false]);
            }
            return DataReturn('保存成功', 0);
        }
    }

    /**
     * 推荐服务数据校验
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecommendServiceDataCheck($param){
        $id=$param['id'];

        //位置重复
        $where=[
            ['service_class_pid','=',$param['service_class_pid']],
            ['position','=',$param['position']],
        ];

        $po=Db::name('service_recommend')->where($where)->find();

        if (!empty($po)&&$id!=$po['id'])
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>20007,'msg'=>'此位置在当前分类已被占用','status'=>false,'debug'=>false]);
        }
        //推荐服务数量
        $where=[
            ['service_class_pid','=',$param['service_class_pid']],
        ];
        $count=Db::name('service_recommend')->where($where)->count();
        if ($count==3&&$id<1)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>20008,'msg'=>'当前分类下推荐服务已达到上限','status'=>false,'debug'=>false]);
        }
        //服务重复
        $where=[
            ['service_class_pid','=',$param['service_class_pid']],
            ['service_id','=',$param['service_id']],
        ];
        //dump($param);exit;
        $ser=Db::name('service_recommend')->where($where)->find();
        if (!empty($ser)&&$id!=$ser['id'])
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>20009,'msg'=>'当前服务已被此分类推荐','status'=>false,'debug'=>false]);
        }

        //时间不符
//        $where=[
//            ['service_class_pid','=',$param['service_class_pid']],
//        ];
//
//        $time=Db::name('service_recommend')->where($where)->find();
//
//        if (empty($time))
//        {
//            return true;
//        }
//        if ($time['start_time']!=$param['start_time']||$time['end_time']!=$param['end_time'])
//        {
//            throw new \BaseException(['code'=>403 ,'errorCode'=>20010,'msg'=>'起止时间应为:'.$time['start_time'].' to '.$time['end_time'],'status'=>false,'debug'=>false]);
//        }


    }

    /**
     * 计算推荐服务开始时间
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecommendServiceCalcStartTime($param)
    {
        $param['position']= $param['position']-1;
        $end_time=Db::name('service_recommend')->where($param)->value('end_time');
        return DataReturn('ok',0,$end_time);
    }
    /**
     * [ServiceDiscountList 查看折扣列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function ServiceDiscountList($arr,$mark,$discount_type,$serviceId=''){


        $str = '';
        $discount_type_title = getDiscountTypeTitle($discount_type);



        if (!empty($arr) and is_array($arr)) {

            $str=" <div class=\"card-body\">
                                      <div class=\"col - sm - 12\" style='justify-content: center;height:10%;align-items: center'><h3>{$discount_type_title}</h3>
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>会员卡名称</th>
                                                <th>服务折扣</th>
                                             
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

            foreach($arr as $k=>$v){
                $level_title = getsMemberLevelTitle($v['level_id']);


                $str.="
                    <tr class='lump".$v['id']."'>
                    
                        <td>".$level_title."</td>
                        <td onclick=\"ServiceDiscountWrite('{$v['id']}','$k','" .$v['discount'] . "','" .$mark . "','".$serviceId."')\">{$v['discount']}</td>
                    
                     

                    </tr>";
            }
            $str.="
                </tbody>
            </table>
        </div>
            </div>";
        }

        return $str;

    }
    /**
     * 预约服务筛选条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    static function AppointserviceListWhere($params){

        $where=[['sb.biz_id', '>', 0]];
        $where[]=['b.biz_type', '>', 1];
        //关联门店
        if(isset($params["param"]['biz_id'])&&intval($params["param"]['biz_id'])>0)
        {
            $where[] = ['sb.biz_id', '=', $params["param"]['biz_id']];
        }
        //关联服务
        if(isset($params["param"]['service_id'])&&intval($params["param"]['service_id'])>0)
        {
            $where[] = ['sb.service_id', '=', $params["param"]['service_id']];
        }
        //模糊查询
        if(isset($params["param"]['keywords']))
        {
            $where[] = ['s.service_title|b.biz_title', 'like', '%'.$params['param']['keywords'].'%'];
        }
       // dump($where);exit;
        return $where;
    }
    /**
     * 预约服务数据获取
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function AppointserviceList($params)
    {
        $where = empty($params['where']) ? [['sb.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $order = isset($params['order']) ? $params['order'] : 'sb.id desc';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        // 获取列表
        $data = Db::name('service_biz')->alias('sb')
            ->leftJoin(['biz'=>'b'],'sb.biz_id=b.id')
            ->leftJoin(['service'=>'s'],'sb.service_id=s.id')
//            ->leftJoin(['station'=>'st'],'st.biz_id=b.id')
            ->where($where)
//            ->group('s.id')
            ->field($field)
            ->order($order);
     // dump(  $data->select());exit;
        //分页
        if($page)
        {
            $data=$data->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }

        return self::AppointserviceDataHandle($data);
    }

    /**
     * 预约服务总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function AppointserviceDataTotal($where){
        return (int) Db::name('service_biz')->alias('sb')
            ->leftJoin(['biz'=>'b'],'sb.biz_id=b.id')
            ->leftJoin(['service'=>'s'],'sb.service_id=s.id')
//            ->leftJoin(['station'=>'st'],'st.biz_id=b.id')
            ->where($where)->count();
    }

    /**
     * 预约服务数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param      [array]          $data [预约服务数据]
     */
    public static function AppointserviceDataHandle($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {

                //结算价格
                $where=[
                    ['service_id','=',$v['service_id']],
                    ['biz_id','=',$v['biz_id']],
                    ['service_type','=',1],
                ];

                $v['settlement_amount']=Db::name('service_car_mediscount')->where($where)->select();
                foreach ($v['settlement_amount'] as &$vv)
                {
                    $vv['car_level_title']=BaseService::StatusHtml($vv['car_level_id'],lang('car_level'),false);
                }
                //推荐提成
                if (empty($v['recommond_royalty']))
                {
                    $v['recommond_royalty']='0';
                }
                //服务提成

                if (empty($v['service_deduct']))
                {
                    $v['service_deduct']='0';
                }

                //商品提成

                if (empty($v['biz_deduct']))
                {
                    $v['biz_deduct']='0';
                }
                //、服务分类、
                if (isset($v['class_pid']))
                {
                    $v['class_pid_title']=Db::name('service_class')->where('id='.$v['class_pid'])->value('service_class_title');
                    $v['class_pid_title']=empty($v['class_pid_title'])?'未获取到分类名称':$v['class_pid_title'];
                }
                //门店名称
                if (isset($v['biz_title']))
                {
                   $v['biz_title']=empty($v['biz_title'])?'未获取到门店名称':$v['biz_title'];
                }
                //省市区
                if (isset($v['province'])&&isset($v['city'])&&isset($v['area']))
                {
                    $v['total_address']=$v['province'].$v['city'].$v['area'];
                    $v['total_address']=empty($v['total_address'])?'未获取到省市区':$v['total_address'];
                }
                //门店类型
                if (isset($v['biz_type']))
                {
                    $v['biz_type_title']=BaseService::StatusHtml($v['biz_type'],lang('biz_type'),false);
                }
                //服务名称
                if (isset($v['service_title']))
                {
                    $v['service_title']=empty($v['service_title'])?'未获取到服务名称':$v['service_title'];
                }

            }
        }

        return DataReturn('处理成功', 0, $data);
    }


    /**
     * 预约服务修改
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月2日17:52:57
     * @desc    description
     * @param      [array]          $param [输入参数]
     */
    public static function AppointserviceSaveData($param){

        // 添加/编辑
        if(empty($param['id']))
        {
            return   $news_id = Db::name('station')->insertGetId($param);
        } else {

            $re= Db::name('station')->where(['id'=>intval($param['id'])])->update($param);
            if (!$re)
            {
                throw new \BaseException(['code'=>403 ,'errorCode'=>20015,'msg'=>'保存提成数据失败','status'=>false,'debug'=>false]);
            }
            return DataReturn('保存成功', 0);
        }


    }

    /**
     * 搜索服务名称数据处理
     */
    static function quickService($value){
        $_where ='';
        $_str='';
        if(!empty($value))
            $_where .= " and service_title like '%".$value."%'";
        $data = self::searchServiceName($_where);
        if(!empty($data)){
        /*    $_str = '<div class="row" style="width: 100 %;float: left">
                                            <div class="col - sm - 6">
                                                <div class="form - group">
                                                    <input type="text" id="search_title"  class="form - control" onkeyup="searchRelevanceService()" placeholder="请输入服务名称，回车键搜索" autocomplete="off">
                                                    <i class="form - group__bar"></i>
                                                </div>
                                            </div>
                                        </div> ';*/
            foreach ($data as $k => $v) {
                $_str .= "

<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 400px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 20px' class='lump".$v['id']."' ><button class='btn btn-info'  onclick=\"getServiceSort('" . $v['id'] . "')\">" . $v['service_title'] . "</button></div>";

            }

        }
        return $_str;


   }
   /*
    * 搜索服务名称
    * */
    static function searchServiceName($_where){
        $data = Db::name('service')->where('is_del=2 and service_status != 0'.$_where)->field('service_title,id')->select();
        return $data;
    }
    /*
      * 查询快捷服务名称
      * */
    static function serviceSort(){
        $data = Db::table('service_quick sq')
            ->field('sq.sort,sq.id,s.service_title')
            ->join('service s','s.id = sq.service_id','left')
            ->order('sort desc')
            ->select();
        return $data;

    }
    /*
   * 查询快捷服务列表
   * */
    static function serviceList($arr){
        $str ='';
        if(!empty($arr)){
            foreach($arr as $k=>$v){

                $str.="
                    <tr class='serviceDetailTr".$v['id']."'>
                    
                           <td style=\"width:30%\">{$v['service_title']}</td>
                            <td style=\"width:30%\" onclick='getServiceQuickWrite(\"{$v['id']}\",\"{$v['sort']}\")'>{$v['sort']}</td>

                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick='delQuickServiceInfo(\"{$v['id']}\")'>删除</button>
                            </td>

                    </tr>";
            }
        }

        return $str;
    }


    /**************************** 关联服务记录 start *******************************/
    /**
     * @content 关联记录存session
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    static function serviceLogSession($params){
        #类型 type 1 保养记录 2 维修记录 3 质保记录
        $type = $params['type'];
        $info = array();

        if($type == 1){
            #保养记录
            #保养名称
            $info['log_title']=$params['log_title'];
            #有效期 天
            $info['log_validity']=$params['log_validity'];
            #类型
            $info['type']=$type;
            #有效公里数
            $info['log_kilometers']=$params['log_kilometers'];


        }else if($type == 2){
            #维修记录
            #维修名称
            $info['log_title']=$params['log_title'];
            #类型
            $info['type']=$type;

        }else if($type == 3){
            #质保记录
            #质保时间
            $info['log_validity']=$params['log_validity'];
            #质保公里数
            $info['log_kilometers']=$params['log_kilometers'];
            #质保范围
            $info['log_scope']=$params['log_scope'];

            #类型
            $info['type']=$type;

        }

        Session::set('getJzServiceLogInfo'. $type, $info);

        $_str = '';

        $_sessionActiveTypeInfo = Session('getJzServiceLogInfo'.$type);

        $_str .= self::getServiceLogInfo($_sessionActiveTypeInfo, 'S', $type);
        return $_str;



    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  1 保养记录 2 维修记录 3 质保记录
     * @return string
     * @content 1 保养记录 2 维修记录 3 质保记录  关联记录
     */
    static function getServiceLogInfo($arr,$mark,$type){


        $_str = '';
        if (!empty($arr) and is_array($arr)) {
            if($type == 1){
                $_str .= "<tr id='serviceLogDetailTr" . $arr['id'] . "$type'>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_title']."','1')\">" . $arr['log_title'] . "</td>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_validity']."','2')\">" . $arr['log_validity'] . "</td>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_kilometers']."','3')\">" . $arr['log_kilometers'] . "</td>
                        
                             
                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick=\"delServiceLogInfo('" . $arr['id'] . "','" . $mark . "','$type')\">删除</button>
                            </td>
                        </tr>";

            }else if($type == 2){
                $_str .= "<tr id='serviceLogDetailTr" . $arr['id'] . "$type'>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_title']."','1')\">" . $arr['log_title'] . "</td>
                        
                             
                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick=\"delServiceLogInfo('" . $arr['id'] . "','" . $mark . "','$type')\">删除</button>
                            </td>
                        </tr>";
            }else if($type == 3){
                $_str .= "<tr id='serviceLogDetailTr" . $arr['id'] . "$type'>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_validity']."','1')\">" . $arr['log_validity'] . "</td>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_kilometers']."','2')\">" . $arr['log_kilometers'] . "</td>
                            <td onclick=\"logServiceWrite('" . $arr['id'] . "','" . $mark . "','$type','".$arr['log_scope']."','3')\">" . $arr['log_scope'] . "</td>
                        
                             
                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick=\"delServiceLogInfo('" . $arr['id'] . "','" . $mark . "','$type')\">删除</button>
                            </td>
                        </tr>";

            }


        }
        return $_str;

    }

    /**
     * @param $type  1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function logServiceTypeList($type){
        $_sessionActiveTypeInfo = Session('getJzServiceLogInfo'.$type);
        $_str = '';

        if(empty($_sessionActiveTypeInfo)){
            #获取服务id 赠送服务 修改时存
            $serviceId = Session('serviceId');
            # 判断是否存在 $serviceId
            if(!empty($serviceId)){
                #查询 关联类型
                $pro = self::logServiceContent($serviceId,$type);

                $_str= self::getServiceLogInfo($pro[0], 'DB',$type);

            }
        }else{
            #缓存
            $_str .= self::getServiceLogInfo($_sessionActiveTypeInfo, 'S',$type);

        }


        return $_str;


    }
    /**
     * @param $serviceId 服务id
     * @content 查询服务
     */
    static function logServiceContent($serviceId,$type){
        $task_prize = Db::table('service_log')->where(array('service_id'=>$serviceId,'log_type'=>$type))->select();

        return $task_prize;

    }
    /**************************** 关联服务记录 end *******************************/
    /**
     * @param $serviceId 服务id
     * @param $car_id  服务车型价格(大型车,中型车,小型车) 的id
     * @content 查询服务车型价格
     */
    static function ServicePrice($serviceId,$car_id,$service_type,$biz_id=0){
        $data = Db::table('service_car_mediscount')
            ->where(array('service_id'=>$serviceId,'car_level_id'=>$car_id,'service_type'=>$service_type,'biz_id'=>$biz_id))->select();

        return $data;
    }
    /**
     * @param $serviceId 服务id
     * @content 查询服务关联门店个数
     */
    static function  relationServiceNum($serviceId){
        #统计 没有自营店  biz_type >1
        $count=Db::name('service_biz')->alias('sb')
            ->leftJoin(['biz'=>'b'],'b.id=sb.biz_id')
            ->where('b.biz_type >1 and sb.service_id='.$serviceId)->count('sb.biz_id');
        return $count;

    }

}
