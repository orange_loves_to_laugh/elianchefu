<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/29
 * Time: 11:15
 */

namespace app\service;

use Redis\Redis;
use think\Db;

/**
 * 应用管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
class ApplayService
{
    /**
     * [ApplayIndex 获取应用用管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function  ApplayIndex($params)
    {
        #where条件  写成活的
//        $where = empty($params['where']['biz_type']) ? array('biz_type'=>1) : $params['where'];
        $where = $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
        #不用tp 5 的 分页时 用
        /*        $m = isset($params['m']) ? intval($params['m']) : 0;
                $n = isset($params['n']) ? intval($params['n']) : 10;*/
        #应用管理列表信息
        $data = Db::table('application')->field("*")->where($where)->order('id desc');
        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);

        } else {
            $data = $data->select();
        }
        return $data;

        #处理方法
//        return self::InforDataDealWith($data,$params);
    }

    /**
     * [AppInsertWrite 添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function AppInsertWrite($params, $mark,$type)
    {
        $data = $params;
        // 请求参数
//        $p = [
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'application_image',
//                'error_msg'         => '请上传banner图',
//                'error_code'         => 30002,
//            ],
//
//        ];
        if (empty($params['id'])) {
            if ($mark == 'banner' or $mark == 'cardbanner' or $mark == 'carbanner' or $mark == 'clubbanner') {
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'application_image',
                    'error_msg' => '请上传banner图',
                    'error_code' => 17001,
                ];
                $ret = ParamsChecked($params, $p);

                if ($ret !== true) {
                    $error_arr = explode(',', $ret);

                    //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
                    return json(DataReturn($error_arr[0], $error_arr[1]));
                }
            }
        }


        $data = self::HandleData($params);

        $_redis= new Redis();

        // 添加/编辑
        if (empty($params['id']))
        {
//            $where=array(
//                'application_mark' => $mark,
////                'application_start'=>$params['application_start'],
////                'application_end'=>$params['application_end'],
//            );
//            $app = Db::table('application')->where($where)->find();
//
//            if (!empty($app))
//            {
//                return ['code' => 0, 'msg' => '你已经添加过了,不能重复添加'];
//
//            }

            $data['application_create'] = TIMESTAMP;
            $data['application_title'] = lang('application_banner_type')[$data['mark']];
            $data['application_class'] = getApplayClass($data['mark']);
            $data['application_mark'] = $data['mark'];

            unset($data['mark']);
            #添加处理
            $re = Db::table("application")->insertGetId($data);

            if ($re) {
//                if(isset($params['application_image'])) {
//                    $bannerArr = explode(',', $params['application_image']);
//                }
                #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
                $params['id']=empty($params['id'])?'':$params['id'];

                if($type == 'savebanner'){
//                    ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
//                    #删除多图(数组形式)
//                    ResourceService::delCacheItem($bannerArr);
                    ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
                    #删除单图
                    ResourceService::delCacheItem($params['application_image']);
                }else if($type == 'baseadvert'){
                    ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
                    #删除单图
                    ResourceService::delCacheItem($params['application_image']);
                }
                #清除前端缓存
                $_redis->hDel("banner","index");
                #首页广告
                $_redis->hDel("banner","indexadvert");
                $_redis->hDel("advance",$mark);
                $_redis->hDel("banner",$mark);

                return json(DataReturn('保存成功', 0));

            }

        }
        else {
            if(isset($params['application_image']))
            {
                $bannerArr = explode(',', $params['application_image']);
            }
            $data['application_update'] = TIMESTAMP;
            #保存图片之后  删除单图 多图上传 缓存 application:表名
            $params['id']=empty($params['id'])?'':$params['id'];
            if($type == 'savebanner'){
                ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
                #删除多图(数组形式)
                ResourceService::delCacheItem($bannerArr);
            }else if($type == 'baseadvert'){
                ResourceService::$session_suffix='source_upload'.'application'.$params['id'];
                #删除单图
                ResourceService::delCacheItem($params['application_image']);
            }
            #清除前端缓存
            $_redis->hDel("banner","index");
            #首页广告
            $_redis->hDel("banner","indexadvert");
            $_redis->hDel("banner",$mark);
            $_redis->hDel("advance",$mark);
            #修改处理
            return self::applayWriteInfo($params, $data);
        }
    }

    static function applayWriteInfo($params, $data)
    {
        unset($data['mark'], $data['oldapplication_image']);

       // dump($data);exit;
        Db::table("application")->where(array('id' => $data['id']))->update($data);
        return json(DataReturn('保存成功', 0));
    }


    /**
     * [popIndex 获取首页弹窗管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public static  function popIndex($params)
    {
        #where条件  写成活的
        $where = empty($params['where']['is_del']) ? array('is_del' => 2) : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
        #不用tp 5 的 分页时 用
        /*        $m = isset($params['m']) ? intval($params['m']) : 0;
                $n = isset($params['n']) ? intval($params['n']) : 10;*/
        #应用管理列表信息
        $data = Db::table('apop')->field("*")->where($where)->order('id desc');
        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);

        } else {
            $data = $data->select();
        }
        return $data;

        #处理方法
//        return self::InforDataDealWith($data,$params);
    }

    /**
     * [popInsertWrite 添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function popInsertWrite($params, $mark)
    {
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'pop_title',
                'error_msg' => '请填写弹窗标题',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'start_time',
                'error_msg' => '请填写上架时间',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'end_time',
                'error_msg' => '请填写下架时间',
                'error_code' => 30002,
            ],

        ];
      /*  if ($params['pop_type'] == 1) {
            #文字
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'desc',
                'error_msg' => '请填写文字描述',
                'error_code' => 30001,
            ];

        } else {
            #图片
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'pop_img',
                'error_msg' => '请上传图片',
                'error_code' => 30001,
            ];

        }*/
        if(empty($params['id'])){
            #图片
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'pop_img',
                'error_msg' => '请上传图片',
                'error_code' => 30001,
            ];
        }
        #是否跳转
        if ($params['is_skip'] == 1 and $params['is_type']==1) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'pop_detail',
                'error_msg' => '请上传详情数据',
                'error_code' => 30001,
            ];


        }

        $ret = ParamsChecked($params, $p);
        if ($ret !== true) {
            $error_arr = explode(',', $ret);

            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }

        $data = self::HandleData($params);
        #把 详情图数组 变成*号拆分数组
//        if(isset($params['pop_detail'])){
//            $remarks = explode(',',$params['pop_detail']);
//            #*号拼接成字符串
//            $data['pop_detail'] = implode('*',$remarks);
//        }

        // 添加/编辑
        if (empty($params['id'])) {
            #查询当前时间是否符合相同的条件
            $time = date("Y-m-d H:i:s");
            $app = Db::table('apop')->where("'$time'>=start_time and '$time'<= end_time and is_del=2")->find();

            if (!empty($app)) {
//                    throw new \BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'你已经添加过了,不能重复添加','status'=>false,'data'=>null]);
                return ['code' => 0, 'msg' => '已有一条正在弹窗的数据,不能重复添加'];
            }
            if ($params['pop_type'] == 1) {
                #文字
                unset($data['pop_img']);

            } else {
                #图片
                unset($data['desc']);
            }

            #添加处理
            $re = Db::table("apop")->insertGetId($data);
//            return  self::applayInsertInfo($params,$data);


        } else {
            unset($data['oldpop_img']);
            if ($params['pop_type'] == 1) {
                #文字
                unset($data['pop_img']);

            } else {
                #图片
                unset($data['desc']);
            }
            #修改处理
            Db::table("apop")->where(array('id' => $data['id']))->update($data);

        }

        #保存图片之后  删除单图 多图上传 缓存 apop:表名
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'apop'.$params['id'];
        #删除单图
        ResourceService::delCacheItem($params['pop_img']);
        #清除前端缓存
        $_redis = new Redis();
        $_redis->hDel("pop","homePop");
        return json(DataReturn('保存成功', 0));


    }

    /**
     * 保存前数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params)
    {

        /*
        if (isset($params["pop_detail"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'apop';
            $params['ueditor_content'] = $params['pop_detail'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params["application_context"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'application';
            $params['ueditor_content'] = $params['application_context'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        */
        if (substr($params['application_image'],0,1)==',')
        {
            $params['application_image']=substr($params['application_image'],1)  ;
        }

        if (isset($params['thumb_img'])) {
            unset($params['thumb_img']);
        }
        if (isset($params['old_imgurl'])) {
            unset($params['old_imgurl']);
        }
        return $params;
    }
    /*****************积分设置**********************/
    /**
     * [IntegralIndex 积分设置 列表]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function IntegralIndex($params)
    {
        #where条件  写成活的
        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 20;

        #应用管理列表信息
        $data = Db::table('integral_set')->field("*");
        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
            foreach ($data['data'] as &$v)
            {
//                $v['app_client_title']=BaseService::StatusHtml($v['app_client'],lang('app_client'),false);
                $v['integral_type_title']=BaseService::StatusHtml($v['integral_type'],lang('integral_type'),false);
            }

        } else {
            $data = $data->select();
            foreach ($data as &$v)
            {
//                $v['app_client_title']=BaseService::StatusHtml($v['app_client'],lang('app_client'),false);
                $v['integral_type_title']=BaseService::StatusHtml($v['integral_type'],lang('integral_type'),false);
            }
        }
        return $data;

        #处理方法
//        return self::InforDataDealWith($data,$params);
    }

    /**
     * 根据跳转类型组建table数据
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $params [输入参数]
     */
    public static function SearchByCateAndKeywords($params){

        $data=self::SearchByCateAndKeywordsGetData($params);

        if (empty($data)) {
            return DataReturn('ok',-1);
        }
        else{
            $table_html='<table class="table table-bordered mb-0">
                        <thead>
                        <tr>
                            <th width="80%">名称</th>
                            
                             <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>';
            foreach ($data as $v)
            {

                $table_html.='<tr>
                            <td  >'.$v['title'].'</td>
                             
                            <td><button class="btn btn-primary connect_detail" data-title="'.$v['title'].'"  data-id="'.$v['id'].'" onclick=connect_detail_callback(this)   type="button" name="connect_detail"    >关联</button></td>
                        </tr>';
            }
            $table_html.='
                        </tbody>
                    </table>';

            return DataReturn('ok',0,$table_html);
        }


    }

    /**
     * 根据类型查询相关数据
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $params [查询参数]
     */
    public static function SearchByCateAndKeywordsGetData($params)
    {
         //1=>'服务', 2=>'活动',3=>'会员',4=>'二手车',5=>'联盟商家',6=>'招募',
        $type_arr=[
            5=>[
                'service'=>'MerchantService',
                'service_func'=>'MerchantsListWhere',
                'table'=>'merchants',
            ]
        ];
        //服务
        if ($params['param']['type']==1)
        {
            // 条件
            $where = [['service_title', 'like', '%'.$params['param']['keywords'].'%']];
            $where[] = [['is_del', '=', '2']];
            if (isset($params['param']['fromrecommend'])&&$params['param']['fromrecommend']) {
//                $id_arr=Db::name('service_recommend')->column('service_id');
//                $where[] = [['id', 'not in', $id_arr]];
            }

            $data_params = array(
                'page'         => false,
                'table'     =>'service',
                'where'     =>$where,
                'order'     =>"service_create desc",
                'field'     =>'id,service_title title,service_class_id,service_time,appoint'
            );
            $data = BaseService::DataList($data_params);

        }

        //活动
        if ($params['param']['type']==2)
        {
            // 条件
            $where = [
                ['active_title', 'like', '%'.$params['param']['keywords'].'%'],
                ['active_start', '<', TIMESTAMP],
                ['active_end', '>', TIMESTAMP],
               ];

            $data_params = array(
                'page'         => false,
                'table'     =>'active',
                'where'     =>$where,
                'order'     =>"id desc",
                'field'     =>'id,active_title title,active_start,active_end,active_biz,active_online'
            );
            $data = BaseService::DataList($data_params);
        }

        //会员
        if ($params['param']['type']==3)
        {
            // 条件
            $where = [['level_title', 'like', '%'.$params['param']['keywords'].'%'],];

            $data_params = array(
                'page'         => false,
                'table'     =>'member_level',
                'where'     =>$where,
                'order'     =>"id desc",
                'field'     =>'id,level_title title'
            );
            $data = BaseService::DataList($data_params);
        }
        //二手车
        if ($params['param']['type']==4)
        {
            // 条件
            $where = [['level_title', 'like', '%'.$params['param']['keywords'].'%'],];

            $data_params = array(
                'page'         => false,
                'table'     =>'member_level',
                'where'     =>$where,
                'order'     =>"id desc",
                'field'     =>'id,level_title title'
            );
            $data = BaseService::DataList($data_params);
        }
        //商户
        if ($params['param']['type']==5 or $params['param']['type']==11)
        {
            if($params['param']['type']==5){
                $params['param']['is_type']==2;
            }else{
                $params['param']['is_type']==1;
            }
            $where=MerchantService::MerchantsListWhere($params);
            //dump($where);exit;
            $data_params = array(
                'page'         => false,
                'table'     =>'merchants',
                'where'     =>$where,


            );
            $data = BaseService::DataList($data_params);
            $data=MerchantService::DataDealWith($data);

        }
        //招募
        if ($params['param']['type']==6)
        {
            // 条件
            $where = [['title', 'like', '%'.$params['param']['keywords'].'%'],];

            $data_params = array(
                'page'         => false,
                'table'     =>'recruitment',
                'where'     =>$where,
                'order'     =>"id desc",
                'field'     =>'id, title'
            );
            $data = BaseService::DataList($data_params);
        }
        return $data;
    }
    /**
     * 跳转类型和名称查询
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年10月22日13:45:48
     * @param    [array]          $info [banner详细信息]
     */
    public static function JumpTypeTitle($info)
    {
     //1=>'服务', 2=>'活动',3=>'会员',4=>'二手车',5=>'联盟商家',6=>'招募',
        $data=[];
        if ($info['jump_type']==4&&$info['jump_id']<1)
        {
            $data['type']='列表关联';
            $data['name']='二手车列表';
        }elseif ($info['jump_type']==5&&$info['jump_id']<1)
        {
            $data['type']='列表关联';
            $data['name']='商户列表';
            if($info['cate_id']>0){
                $data['type']='联盟商家分类';
                $data['name']=Db::table("merchants_cate")->where(array("id"=>$info['cate_id']))->value("title");
            }
        }elseif ($info['jump_type']==11&&$info['jump_id']<1)
        {
            $data['type']='列表关联';
            $data['name']='本地生活列表';
            if($info['cate_id']>0){
                $data['type']='本地生活分类';
                $data['name']=Db::table("merchants_cate")->where(array("id"=>$info['cate_id']))->value("title");
            }
        }else{
            if($info['jump_id']>0){

                $data['type']=lang('image_jump_type_title')[$info['jump_type']]['title'];
                $data['name']=Db::name(lang('image_jump_type_title')[$info['jump_type']]['table'])->where('id='.$info['jump_id'])->value(lang('image_jump_type_title')[$info['jump_type']]['field']);
            }
        }

        return $data;
    }

    /********************商户端 *************************/
    /**
     * [MerInsertWrite 添加  修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function MerInsertWrite($params, $mark,$type)
    {
        $data = $params;
        if ($mark == 'aboutmine') {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'phone',
                'error_msg' => '请填写联系电话!',
                'error_code' => 17001,
            ];
            $ret = ParamsChecked($params, $p);

            if ($ret !== true) {
                $error_arr = explode(',', $ret);

                //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
                return json(DataReturn($error_arr[0], $error_arr[1]));
            }
            #判断 联系电话  公司简介  公司优势
            $_infomark_phone = $params['phone_mark'];
            #公司简介
            $_infomark_brief =$params['brief_mark'];
            #公司优势
            $_infomark_advantage =$params['advantage_mark'];
            #联系电话
            if(empty($params['phone_id'])){
                $where=array(
                    'mark' => $_infomark_phone,

                );
                $app = Db::table('merchants_application')->where($where)->find();

                if (!empty($app))
                {
                    return ['code' => 0, 'msg' => '你已经添加过了,不能重复添加'];

                }
                #添加处理
                $re = Db::table("merchants_application")->insertGetId(
                    array(
                        'content'=>$params['phone'],
                        'mark'=>$_infomark_phone,
                    )
                );
            }else{
                $re = Db::table("merchants_application")->where(array('mark' => $_infomark_phone))->update(
                    array(
                        'content'=>$params['phone'],
                    )
                );
            }
            #公司简介
            if(empty($params['brief_id'])){
                $where=array(
                    'mark' => $_infomark_brief,

                );
                $app = Db::table('merchants_application')->where($where)->find();


                if (!empty($app))
                {
                    return ['code' => 0, 'msg' => '你已经添加过了,不能重复添加'];

                }
                #添加处理
                $re = Db::table("merchants_application")->insertGetId(
                    array(
                        'content'=>$params['brief'],
                        'mark'=>$_infomark_brief,
                    )
                );
            }else{
                $re = Db::table("merchants_application")->where(array('mark' => $_infomark_brief))->update(
                    array(
                        'content'=>$params['brief'],
                    )
                );
            }
            #公司优势
            if(empty($params['advantage_id'])){
                $where=array(
                    'mark' => $_infomark_advantage,

                );
                $app = Db::table('merchants_application')->where($where)->find();

                if (!empty($app))
                {
                    return ['code' => 0, 'msg' => '你已经添加过了,不能重复添加'];

                }
                #添加处理
                $re = Db::table("merchants_application")->insertGetId(
                    array(
                        'content'=>$params['advantage'],
                        'mark'=>$_infomark_advantage,
                    )
                );
            }else{
                $re = Db::table("merchants_application")->where(array('mark' => $_infomark_advantage))->update(
                    array(
                        'content'=>$params['advantage'],
                    )
                );
            }
        }else{
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'content',
                'error_msg' => '请填写协议!',
                'error_code' => 17001,
            ];
            $ret = ParamsChecked($params, $p);

            if ($ret !== true) {
                $error_arr = explode(',', $ret);

                //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
                return json(DataReturn($error_arr[0], $error_arr[1]));
            }
            if(empty($params['id'])){
                $where=array(
                    'mark' => $mark,

                );
                $app = Db::table('merchants_application')->where($where)->find();

                if (!empty($app))
                {
                    return ['code' => 0, 'msg' => '你已经添加过了,不能重复添加'];

                }
                #添加处理
                $re = Db::table("merchants_application")->insertGetId(
                    array(
                        'content'=>$params['content'],
                        'mark'=>$mark,
                    )
                );
            }else{
                $re = Db::table("merchants_application")->where(array('id' => $params['id']))->update(
                    array(
                        'content'=>$params['content']
                    )
                );
            }
        }







        return json(DataReturn('保存成功', 0));


    }

    /**
     * @param $data
     * @return array
     * @context 商家分类关联
     */
    function merchantsCateFormat($data,$is_type){
        $table_html='<table class="table table-bordered mb-0">
                        <thead>
                        <tr>
                            <th width="80%">名称</th>
                            
                             <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>';
        foreach ($data as $v)
        {

            $table_html.='<tr>
                            <td  >'.$v['name'].'</td>
                             
                            <td><button class="btn btn-primary connect_detail" data-type="'.$is_type.'" data-title="'.$v['name'].'"  data-id="'.$v['id'].'" onclick=connect_detail_callback(this)   type="button" name="connect_detail"    >关联</button></td>
                        </tr>';
        }
        $table_html.='
                        </tbody>
                    </table>';

        return DataReturn('ok',0,$table_html);
    }

}