<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/4
 * Time: 11:24
 */
/**
 * 活动管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
namespace app\service;
use think\Db;
use think\db\Expression;
use think\facade\Session;

class RunnerService
{
    /**
     * [runnerIndex 获取大转轮管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function runnerIndex($params){
        #where条件  写成活的
        $where = empty($params['where']) ? ['wheel_status'=>'1'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('game_wheel')
          ->order('id desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];

        } else {
            $data = $data->select();
        }

        #处理方法
        return self::InforDataDealWith($data,$params);



    }
    /*
     * @content ：处理方法
     * */
    function InforDataDealWith($data,$params){
        $time = date("Y-m-d H:i:s");
        if(!empty($data)){
            foreach($data as $k=>$v){
                if($time < $v['wheel_start'] and $time<$v['wheel_end']){
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['active_status'] = '待上架';

                }else if($time >= $v['wheel_start'] and $time<=$v['wheel_end']){
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['active_status'] = '已上架';

                }else if($time > $v['wheel_start'] and $time>$v['wheel_end']){
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['active_status'] = '已下架';

                }
                #查询一个活动 对应的详情
//                $prize = Db::table('wheel_prize')->where(array('wheel_id'=>$v['id']))->select();
//                dump($prize);die;
//                if(!empty($prize)){
//                    foreach($prize as $pk=>$pv){
//
//                    }
//                }
                $date = '2020-02';
                #查询每个奖项的参与人数
                $memberCount = Db::query("select count(distinct ld.member_id) as num,date_format(ld.draw_time,'%Y-%m-%d') as days
                                          from log_draw as ld,wheel_prize as wp where  ld.bear_status =1 and wp.wheel_id ={$v['id']} and ld.prize_id = wp.id 
                                         ");
                $data[$k]['memberCount'] = $memberCount[0]['num'];
            }
        }
        return $data;
    }
    /**
     * 总数
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function runnerIndexCount(){
        return (int) Db::name('game_wheel')->where('wheel_status = 1')->count();
    }
    //查询 大转轮表详情  奖项
    function getRunnerContent($exchangeId){
        $redpacket_detail = Db::table('wheel_prize')->where(array('wheel_id'=>$exchangeId))->select();
        if(!empty($redpacket_detail)){
            $percent = 0;
            foreach ($redpacket_detail as $v){
                $percent = $percent +$v['prize_probability'];

            }
        }

        return array('redpacket_detail'=>$redpacket_detail,'percent'=>$percent);
    }
    /**
     * [runnerSave 大转轮添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function runnerSave($params){
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'wheel_title',
                'error_msg'         => '活动名称不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'wheel_start',
                'error_msg'         => '开始时间不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'wheel_end',
                'error_msg'         => '结束时间不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'wheel_explain',
                'error_msg'         => '活动说明不能为空',
                'error_code'         => 30002,
            ],



        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }



        // 添加/编辑
        if (empty($params['id'])) {

            #添加处理
            $activeId = Db::table("game_wheel")->insertGetId($data);
            if($activeId){
                #获取缓存
                $taskPrizeInfo = Session('RunnerPrizeInfo');
                if(!empty($taskPrizeInfo)){
                    #获取镜像  spoil_id：卡券或红包id
                    foreach ($taskPrizeInfo as $k=>$v){
                        $taskPrizeInfo[$k]['wheel_id']= $activeId;
                        $taskPrizeInfo[$k]['spoil_id']= $taskPrizeInfo[$k]['pro_service_id'];


                        if($v['coupon_type'] == 1){
                            #卡券表中card_type_id 商品 3
                            $card_type_id = 3;

                        }else if($v['coupon_type'] == 2){
                            #卡券表中card_type_id 服务 1
                            $card_type_id = 1;

                        }
                        #类型为卡券时 上 card_voucher  查下卡券id 存上
                        $voucher_id  = Db::table('card_voucher')->where(array('card_type_id'=>$card_type_id,'server_id'=>$v['pro_service_id']))->value('id');
                        if(!empty($voucher_id)){
                            $taskPrizeInfo[$k]['spoil_id']= $voucher_id;

                        }
                        if(isset($v['pro_service_id']))
                            unset($taskPrizeInfo[$k]['pro_service_id']);
                        if($v['prize_type'] == 4){
                            #存 redpacket 红包
                            $voucherId = Db::table('redpacket')->insertGetId(array(
                                'price'=>$v['intebal_num'],
                                'packet_title'=>$v['prize_title'],

                                'create_time'=>date("Y-m-d H:i:s"),
                            ));
                            if($voucherId){
                                #task_prize 表 插入红包id
                                $taskPrizeInfo[$k]['spoil_id']= $voucherId;
                                foreach($v['redpacket_detail'] as $rk=>$rv){
                                    $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                    $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                                }

                                Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                            }
                        }else if($v['prize_type'] == 7 or $v['prize_type'] == 8 or $v['prize_type'] == 9){
                            $giving_id = Db::table("merchants_voucher")->insertGetId(array("title"=>$v['title'],"price"=>$v['price'],"min_consume"=>$v['min_consume'],
                                "num"=>1,"validity_day"=>$v['validity_day'],"is_type"=>$v['is_type'],"cate_pid"=>$v['cate_pid'],"voucher_start"=>$v['voucher_start']));
                            unset($taskPrizeInfo[$k]['title'],$taskPrizeInfo[$k]['price'],$taskPrizeInfo[$k]['min_consume'],$taskPrizeInfo[$k]['num'],$taskPrizeInfo[$k]['validity_day'],
                                $taskPrizeInfo[$k]['is_type'],$taskPrizeInfo[$k]['voucher_start'],$taskPrizeInfo[$k]['cate_pid'],$taskPrizeInfo[$k]['active_types']
                            );
                            $taskPrizeInfo[$k]['spoil_id']=$giving_id;
                            $taskPrizeInfo[$k]['prize_explain']='';
                            $taskPrizeInfo[$k]['coupon_type']=$v['prize_type'];
                        }
                        unset($taskPrizeInfo[$k]['redpacket_detail']);
                        ksort($taskPrizeInfo[$k]);
                    }
                    #任务奖励 表
                    Db::table('wheel_prize')->insertAll($taskPrizeInfo);
                }
            }

        } else {
            #修改

            Db::table("game_wheel")->where(array('id' => $params['id']))->update($data);

            #获取缓存
            $taskPrizeInfo = Session('RunnerPrizeInfo');
            if(!empty($taskPrizeInfo)){
                #获取镜像  spoil_id：卡券或红包id
                foreach ($taskPrizeInfo as $k=>$v){
                    $taskPrizeInfo[$k]['wheel_id']= $params['id'];
                    if(isset($v['pro_service_id'])){
                        $taskPrizeInfo[$k]['spoil_id']= $taskPrizeInfo[$k]['pro_service_id'];
                        if($v['coupon_type'] == 1){
                            #卡券表中card_type_id 商品 3
                            $card_type_id = 3;

                        }else if($v['coupon_type'] == 2){
                            #卡券表中card_type_id 服务 1
                            $card_type_id = 1;

                        }
                        #类型为卡券时 上 card_voucher  查下卡券id 存上
                        $voucher_id  = Db::table('card_voucher')->where(array('card_type_id'=>$card_type_id,'server_id'=>$v['pro_service_id']))->value('id');
                        if(!empty($voucher_id)){
                            $taskPrizeInfo[$k]['spoil_id']= $voucher_id;

                        }
                        unset($taskPrizeInfo[$k]['pro_service_id']);

                    }else{
                        $taskPrizeInfo[$k]['spoil_id'] = 0;
                    }
                    if($v['prize_type'] == 4){
                        #存 redpacket 红包
                        $voucherId = Db::table('redpacket')->insertGetId(array(
                            'price'=>$v['intebal_num'],
                            'packet_title'=>$v['prize_title'],

                            'create_time'=>date("Y-m-d H:i:s"),
                        ));
                        if($voucherId){
                            #task_prize 表 插入红包id
                            $taskPrizeInfo[$k]['spoil_id']= $voucherId;
                            foreach($v['redpacket_detail'] as $rk=>$rv){
                                $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                            }

                            Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                        }
                    }else if($v['prize_type'] == 7 or $v['prize_type'] == 8 or $v['prize_type'] == 9){
                        $giving_id = Db::table("merchants_voucher")->insertGetId(array("title"=>$v['title'],"price"=>$v['price'],"min_consume"=>$v['min_consume'],
                            "num"=>1,"validity_day"=>$v['validity_day'],"is_type"=>$v['is_type'],"cate_pid"=>$v['cate_pid'],"voucher_start"=>$v['voucher_start']));
                        unset($taskPrizeInfo[$k]['title'],$taskPrizeInfo[$k]['price'],$taskPrizeInfo[$k]['min_consume'],$taskPrizeInfo[$k]['num'],$taskPrizeInfo[$k]['validity_day'],
                            $taskPrizeInfo[$k]['is_type'],$taskPrizeInfo[$k]['voucher_start'],$taskPrizeInfo[$k]['cate_pid'],$taskPrizeInfo[$k]['active_types']
                        );
                        $taskPrizeInfo[$k]['spoil_id']=$giving_id;
                    }
                    unset($taskPrizeInfo[$k]['redpacket_detail']);
                }
                #任务奖励 表
                Db::table('wheel_prize')->insertAll($taskPrizeInfo);
            }




        }
        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('taskId');
        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('RunnerPrizeInfo');
        return json(DataReturn('保存成功', 0));

    }
    /**
     *  runnerSession 服务  商品  会员 积分 余额 存session
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    function runnerSession($params,$taskId){

        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];

        if($type == 'pro'){
            #商品
            #名称
            $info['prize_title']=$params['prize_title'];
            $info['pro_service_id']=$params['pro_service_id'];
            #奖励积分 价值
            $info['intebal_num']=$params['detail_price'];
            #奖励类型 3商品
            $info['prize_type']=3;
            $info['coupon_type']=1;
            #奖品数量
            $info['give_num']=$params['give_num'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=$params['prize_indate'];
            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];
            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']=$params['prize_unit'];
            #奖励说明
            $info['prize_explain']='';
        }
        else if($type == 'service'){
            $info['pro_service_id']=$params['pro_service_id'];

            #服务
            $info['prize_title']=$params['prize_title'];
            #奖励积分 价值
            $info['intebal_num']=$params['detail_price'];
            #奖励类型 5 服务
            $info['prize_type']=3;
            $info['coupon_type']=2;
            #奖品数量
            $info['give_num']=$params['give_num'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=$params['prize_indate'];

            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];
            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']=$params['prize_unit'];
            #奖励说明
            $info['prize_explain']='';
        }
        else if($type == 'packet'){
            #红包
            $info['prize_title']=$params['packet_title'];
            #奖励积分 价值
            $info['intebal_num']=$params['price'];
            #奖励类型 5 服务
            $info['prize_type']=4;
            $info['coupon_type']=0;
            #奖品数量
            $info['give_num']=1;
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']='365';
            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];
            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']='张';
            #奖励说明
            $info['prize_explain']='';
            $voucher = session('VoucherInfo');
            if(!empty($voucher)){
                #存详情字段  创建时 填到redpacket_detail  红包详情表
                $info['redpacket_detail'] = $voucher;
            }

        }
        else if($type == 'integral'){
            #积分  $id 是积分
            #名称
            $info['prize_title']=$params['prize_title'];

            #奖励积分 价值
            $info['intebal_num']=$params['intebal_num'];
            #奖励类型 1 积分
            $info['prize_type']=1;
            $info['coupon_type']=0;
            #奖品数量
            $info['give_num']=1;
            #有效期
            #上架数量
            $info['ground_num']=$params['ground_num'];
            $info['prize_indate']=365;
            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];

            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']=$params['prize_unit'];
            #奖励说明
            $info['prize_explain']='';


        }
        else if($type == 'balance'){
            #余额 $id 是传过来的余额
            #名称
            $info['prize_title']=$params['prize_title'];

            #奖励积分 价值
            $info['intebal_num']=$params['intebal_num'];
            #奖励类型 2 余额
            $info['prize_type']=2;
            $info['coupon_type']=0;

            #奖品数量
            $info['give_num']=1;
            #上架数量
            $info['ground_num']=$params['ground_num'];

            #有效期
            $info['prize_indate']=365;
            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];
            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']=$params['prize_unit'];
            #奖励说明
            $info['prize_explain']='';
        }
        else if($type == 'other'){
            #名称
            $info['prize_title']=$params['prize_title'];

            #奖励积分 价值
            $info['intebal_num']=$params['intebal_num'];
            #奖励类型 2 余额
            $info['prize_type']=2;
            $info['coupon_type']=0;

            #奖品数量
            $info['give_num']=1;
            #上架数量
            $info['ground_num']=$params['ground_num'];

            #有效期
            $info['prize_indate']=365;
            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            #中奖概率
            $info['prize_probability']=$params['prize_probability'];
            #单人中奖次数
            $info['winnumber']=$params['winnumber'];
            #单位
            $info['prize_unit']=$params['prize_unit'];

            #奖励说明
            $info['prize_explain']=$params['prize_explain'];
        }else if($type == 'merchantVoucher' or $type == 'lifeVoucher'){
            $info = $params;
            $info['prize_type']=$params['is_type']==2 ? 7 :$params['is_type']==3 ? 9 : 8;
        }

        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('RunnerPrizeInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        if(!empty($_sessionActiveTypeInfo)){
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' .$type. $info['pro_service_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['give_num'] = intval($_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['give_num']) + intval($info['give_num']);
            } else {

                $_sessionActiveTypeInfo['S' . $type.$info['pro_service_id']] = $info;
            }
        }
        else{
            $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']] = $info;

        }

        Session::set('RunnerPrizeInfo', null);
        Session::set('RunnerPrizeInfo', $_sessionActiveTypeInfo);
        $_str = '';
        # 判断是否存在$taskId  大转轮id
        if(!empty($taskId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getRunnerContent($taskId);

            $_str= self::getTaskPrizeInfo($pro['redpacket_detail'], 'DB');
            $_DbCount = self::getTaskPrizeCount($pro['redpacket_detail']);

        }
        /*getActiveArrInfo*/

        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        $_Scount = self::getTaskPrizeCount($_sessionActiveTypeInfo);
        $_count = intval($_DbCount)+intval($_Scount);
        return array('str'=>$_str,'count'=>$_count);
//        return $_str;

    }
    /*
     * @content 统计中奖概率
     * */
    static function getTaskPrizeCount($arr){
        $_str = '';
        $count = 0;

        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                $count = intval($count)+intval($v['prize_probability']);


            }
        }
        return $count ;
    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    function getTaskPrizeInfo($arr,$mark){

        $_str = '';
        $data = array();

        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                if ($v['prize_type'] == 1) {
                    $_active_type_title = "积分";
                    $intbal = $v['intebal_num'].'积分';
                } else if($v['prize_type'] == 2){
                    $_active_type_title = "余额";
                    $intbal = $v['intebal_num'].'余额';


                }else if($v['prize_type'] == 3) {
                    if($v['coupon_type'] == 1){
                        $_active_type_title= "商品卡券";
                        $intbal = sprintf("%.2f",  $v['intebal_num']);
                    }else{
                        $_active_type_title= "服务卡券";
                        $intbal = sprintf("%.2f",  $v['intebal_num']);
                    }


                }else if($v['prize_type'] == 4) {
                    $_active_type_title= "红包";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);

                }else if($v['prize_type'] == 5) {

                    $_active_type_title= "其他物品";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);
                }else if($v['prize_type'] == 7) {

                    $_active_type_title= "商家消费券";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);
                }else if($v['prize_type'] == 8) {

                    $_active_type_title= "有派生活消费券";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);
                }

                if($v['prize_type'] == 4){
                    $button = "   <button class=\"layui-btn layui-btn-xs\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['prize_type'] . "')\">删除</button>  
    <button class=\"layui-btn layui-btn-xs\" type=\"button\" onclick=\"getRedpacket('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['prize_type'] . "')\">查看</button>";

                }else{
                    $button = " <button class=\"layui-btn layui-btn-xs\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['prize_type'] . "')\">删除</button>";

                }

                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['prize_title'] . "</td>
                            <td >$intbal</td>
                            <td>$_active_type_title</td>
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['give_num'] . "','" .$mark . "','4','" . $v['prize_type']. "','')\">" . $v['give_num'] . "</td>
                             <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['ground_num'] . "','" .$mark . "','5','" . $v['prize_type']. "','" . $v['surplus_num'] . "')\">" . $v['ground_num'] . "</td>
                             
                              <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['prize_probability'] . "','" .$mark . "','6','" . $v['prize_type']. "','')\">" . $v['prize_probability'] . "%</td>
                             
                             
                             
                             
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['prize_indate'] . "','" .$mark . "','7','" . $v['prize_type'] . "','')\">" . $v['prize_indate'] . "</td>
                              <td >" . $v['surplus_num'] . "</td>

                            <td>
                                                        $button

                            </td>
                        </tr>";
            }
        }
        return $_str ;

    }
    /**
     * @param $type  1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    function getRunnerTypeList(){
        #获取缓存活动id 只有修改时候有
        $runnerId = Session('runnerId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('RunnerPrizeInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在$activeId
        if(!empty($runnerId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getRunnerContent($runnerId);


            $_str= self::getTaskPrizeInfo($pro['redpacket_detail'], 'DB');
            $_DbCount = self::getTaskPrizeCount($pro['redpacket_detail']);
        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        $_Scount = self::getTaskPrizeCount($_sessionActiveTypeInfo);
        $_count = intval($_DbCount)+intval($_Scount);
        return array('str'=>$_str,'count'=>$_count);

    }
}
