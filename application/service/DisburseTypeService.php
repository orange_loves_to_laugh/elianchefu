<?php


namespace app\service;

use think\Db;

/**
 * 费用类型服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月20日17:33:34
 */
class DisburseTypeService
{
    /**
     * 获取费用类型列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月20日17:33:34
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function TypeList($params){
        $data=BaseService::DataList($params);
        return $data;
    }


    /**
     * 消息保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月20日17:33:34
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function TypeSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'type_title',
                'error_msg'         => '标题不能为空',
                'error_code'         => 70002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'type_cate',
                'error_msg'         => '状态不能为空',
                'error_code'         => 70003,
            ],
        ];

        # 添加是判断标题是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'costtype',
                'key_name'          => 'type_title',
                'error_msg'         => '标题不能重复',
                'error_code'         => 70001,
            ];
        }

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=$params;

       // dump($data);exit;
        // 添加/编辑
        $data['update_time']=TIMESTAMP;
        if(empty($params['id']))
        {
            Db::name('costtype')->insert($data);
        } else {

            Db::name('costtype')->where(['id'=>intval($params['id'])])->update($data);

        }

        return DataReturn('保存成功', 0);

    }
    /**
     * 删除类型
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月20日17:33:34
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DelType($params){

         //判断类型是否正在被使用中
        $data_params = [
            'where'				=> ['type_id'=>$params['id']],
            'm'					=> 0,
            'n'					=> 1,
            'page'			  => false,
            'order'                =>'id desc',
            'table'=>'expenses'
        ];

        $ret = self::TypeList($data_params);
        if (!empty($ret))
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>70004,'msg'=>'当前类型正在使用中','status'=>false,'debug'=>false]);
        }

        $params['table']='costtype';
        $params['soft']=false;
        $params['errorcode']=70005;
        $params['msg']='删除失败';
//        dump($params);exit;
        BaseService::DelInfo($params);
    }


}