<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/10/29
 * Time: 16:35
 */

/**
 * 红包管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

namespace app\service;

use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Session;

class PacketService
{
    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function packetIndex($params)
    {
        #where条件  写成活的
        $where = empty($params['where']) ? ['type' => '1', 'status' => 1] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('redpacket')
            ->where($where)->order('id desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data = $data->toArray()['data'];

        } else {
            $data = $data->select();
        }

        $time = date("Y-m-d H:i:s");
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if ($time < $v['create_time'] and $time < $v['end_time']) {
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['packet_status'] = '待开始';

                } else if ($time >= $v['create_time'] and $time <= $v['end_time']) {
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['packet_status'] = '进行中';

                } else if ($time > $v['create_time'] and $time > $v['end_time']) {
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['packet_status'] = '已结束';

                } else {
                    $data[$k]['packet_status'] = '关闭';
                }
                #获取领取成功人数
                $data[$k]['receivenum'] = Db::table('redpacket_log')->where(array('redpacket_id' => $v['id']))->count('member_id');
            }
        }
        return $data;


    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function PacketIndexCount()
    {
        return (int)Db::name('redpacket')->where('type =1 and status=1')->count();
    }

    /**
     * [packetSave 任务添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function packetSave($params)
    {
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'packet_title',
                'error_msg' => '红包名称不能为空',
                'error_code' => 30002,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'create_time',
                'error_msg' => '请填写开始时间',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'end_time',
                'error_msg' => '请填写结束时间',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'icon',
                'error_msg' => '请上传图标',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'img_style',
                'error_msg' => '请上传弹窗样式',
                'error_code' => 30002,
            ],


        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }


        unset($data['thumb_img']);

        // 添加/编辑
        if (empty($params['id'])) {

            $data['surplus_num'] = $data['ground_num'];
            unset($data['oldground_num'], $data['oldicon'], $data['oldimg_style']);
            #添加处理
            $redpacketId = Db::table("redpacket")->insertGetId($data);
            if ($redpacketId) {
                #大的红包表添加完之后  添加小的 红包名称表 redpacket_title
                #获取 红包名称缓存

                $redpacketIdInfo = Session('redpacketTitlePrizeInfo');
                if (!empty($redpacketIdInfo)) {
                    #添加红包名称 表  获取对应添加的id  来存红包详情表
                    foreach ($redpacketIdInfo as &$v) {

                        #添加红包名称表
                        $redpacket_title_id = Db::table('redpacket_title')->insertGetId(array(
                            'redpacket_id' => $redpacketId,
                            'person' => $v['person'],
                            'person_level' => $v['person_level'],
                            'price' => $v['price'],
                            'redpacket_title' => $v['redpacket_title'],
                        ));
                        if (!empty($v['redpacket_detail'])) {
                            #获取红包名称详情  spoil_id：卡券或红包id  但是 当是商品卡券 和服务卡券时  spoil_id 为商品id  服务id
                            foreach ($v['redpacket_detail'] as $rk => $rv) {
                                $v['redpacket_detail'][$rk]['redpacket_id'] = $redpacket_title_id;
                                $v['redpacket_detail'][$rk]['type'] = 1;
                                $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                                if (!empty($rv['is_type'])) {
                                    $arr = array("title" => $rv['title'], "price" => $rv['price'], "min_consume" => $rv['min_consume'],
                                        "num" => 1, "validity_day" => $rv['validity_day'], "is_type" => $rv['is_type'], "cate_pid" => $rv['cate_pid'], "voucher_start" => $rv['voucher_start']);
                                    if ($rv['is_type'] == 2) {
                                        $arr['second_cate_pid'] = $rv['second_cate_pid'];
                                    }
                                    $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
                                    unset($v['redpacket_detail'][$rk]['title'], $v['redpacket_detail'][$rk]['price'], $v['redpacket_detail'][$rk]['min_consume'],
                                        $v['redpacket_detail'][$rk]['num'], $v['redpacket_detail'][$rk]['validity_day'],
                                        $v['redpacket_detail'][$rk]['is_type'],$v['redpacket_detail'][$rk]['days_id'],
                                        $v['redpacket_detail'][$rk]['cate_pid'], $v['redpacket_detail'][$rk]['active_types'],$v['redpacket_detail'][$rk]['second_cate_pid']
                                    );
                                    $v['redpacket_detail'][$rk]['voucher_id'] = $giving_id;
                                }
                                ksort($v['redpacket_detail'][$rk]);
                            }
                            Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                        }

                    }

                }

            }


        } else {
            #修改  传一个未修改之前的上架数量（旧的上架数量 5）  剩余数量（？） = 新值（10）- 旧的上架数量（5） = 5
            #查询 未修改之前的剩余数量
            $surplus_num = Db::table('redpacket')->where(array('id' => $params['id']))->value('surplus_num');
            $data['surplus_num'] = $data['ground_num'] - $data['oldground_num'] + $surplus_num;
            unset($data['oldground_num'], $data['oldicon'], $data['oldimg_style']);
            Db::table("redpacket")->where(array('id' => $params['id']))->update($data);

            #大的红包表添加完之后  添加小的 红包名称表 redpacket_title
            #获取 红包名称缓存

            $redpacketIdInfo = Session('redpacketTitlePrizeInfo');

            if (!empty($redpacketIdInfo)) {
                #添加红包名称 表  获取对应添加的id  来存红包详情表
                foreach ($redpacketIdInfo as &$v) {
                    #添加红包名称表
                    $redpacket_title_id = Db::table('redpacket_title')->insertGetId(array(
                        'redpacket_id' => $params['id'],
                        'person' => $v['person'],
                        'person_level' => $v['person_level'],

                        'price' => $v['price'],
                        'redpacket_title' => $v['redpacket_title'],
                    ));

                    if (!empty($v['redpacket_detail'])) {
                        #获取镜像  spoil_id：卡券或红包id
                        foreach ($v['redpacket_detail'] as $rk => $rv) {
                            $v['redpacket_detail'][$rk]['redpacket_id'] = $redpacket_title_id;
                            $v['redpacket_detail'][$rk]['type'] = 1;
                            $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                            if (!empty($rv['is_type'])) {
                                $arr = array("title" => $rv['title'], "price" => $rv['price'], "min_consume" => $rv['min_consume'],
                                    "num" => 1, "validity_day" => $rv['validity_day'], "is_type" => $rv['is_type'], "cate_pid" => $rv['cate_pid'], "voucher_start" => $rv['voucher_start']);
                                if ($rv['is_type'] == 2) {
                                    $arr['second_cate_pid'] = $rv['second_cate_pid'];
                                }
                                $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
                                unset($v['redpacket_detail'][$rk]['title'], $v['redpacket_detail'][$rk]['price'], $v['redpacket_detail'][$rk]['min_consume'],
                                    $v['redpacket_detail'][$rk]['num'], $v['redpacket_detail'][$rk]['validity_day'],
                                    $v['redpacket_detail'][$rk]['is_type'],$v['redpacket_detail'][$rk]['days_id'],
                                    $v['redpacket_detail'][$rk]['cate_pid'], $v['redpacket_detail'][$rk]['active_types'],$v['redpacket_detail'][$rk]['second_cate_pid']
                                );
                                $v['redpacket_detail'][$rk]['voucher_id'] = $giving_id;
                            }
                            ksort($v['redpacket_detail'][$rk]);
                        }
                        Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                    }
                }

            }
        }
        #删除红包缓存 前端  上传同一时间段 只能有一个
        $_redis = new Redis();

        $_redis->hDel("redpacket", "bigRedpacket");

        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('repacketId');
        #删除添加 卡券  缓存
        Session::delete('VoucherInfo');
        Session::delete('redpacketTitlePrizeInfo');
        return json(DataReturn('保存成功', 0));

    }

    /**
     * @param $packetId 红包id
     * @param $type 1 普通 红包  其他  查询type= 2 的
     * @content 红包详情 查询普通红包 type=1
     */
    static function getPacketContent($packetId, $type = '')
    {
        if ($type == 1) {
            $redpacket_detail = Db::table('redpacket_detail')->where(array('redpacket_id' => $packetId, 'status' => 1, 'type' => 1))->select();

        } else {
            $redpacket_detail = Db::table('redpacket_detail')->where(array('redpacket_id' => $packetId, 'status' => 1, 'type' => 2))->select();

        }

        return $redpacket_detail;

    }

    /**
     *  taskSession 服务  商品  会员 积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function packetSession($params, $redpacketId)
    {
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
        if ($type == 'pro') {
            #商品
            #名称
            $info['prize_title'] = $params['prize_title'];
            $info['pro_service_id'] = $params['pro_service_id'];
            #奖励积分 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 3商品
            $info['prize_type'] = 3;
            #奖品数量
            $info['prize_num'] = $params['prize_num'];
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = $params['prize_indate'];
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];


        } else if ($type == 'service') {
            $info['pro_service_id'] = $params['pro_service_id'];

            #服务
            $info['prize_title'] = $params['prize_title'];
            #奖励积分 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 5 服务
            $info['prize_type'] = 5;
            #奖品数量
            $info['prize_num'] = $params['prize_num'];
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = $params['prize_indate'];

            #剩余数量
            $info['surplus_num'] = $params['ground_num'];

        } else if ($type == 'packet') {
            #红包
            $info['redpacket_title'] = $params['redpacket_title'];
            #奖励积分 价值
            $info['price'] = $params['price'];
            #领取人群
            $info['person'] = $params['person'];
            /*if(isset($params['person_level'])){
                $info['person_level'] = implode(',',$params['person_level']);
            }*/
            $voucher = session('VoucherInfo');
            if (!empty($voucher)) {
                #存详情字段  创建时 填到redpacket_detail  红包详情表
                $info['redpacket_detail'] = $voucher;
                #删除缓存
                session::delete('VoucherInfo');
            }

        } else if ($type == 'integral') {
            #积分  $id 是积分
            #名称
            $info['prize_title'] = $params['prize_title'];

            #奖励积分 价值
            $info['intbal'] = $params['intbal'];
            #奖励类型 1 积分
            $info['prize_type'] = 1;
            #奖品数量
            $info['prize_num'] = 1;
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = 365;
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];


        } else if ($type == 'balance') {
            #余额 $id 是传过来的余额
            #名称
            $info['prize_title'] = $params['prize_title'];

            #奖励积分 价值
            $info['intbal'] = $params['intbal'];
            #奖励类型 2 余额
            $info['prize_type'] = 2;
            #奖品数量
            $info['prize_num'] = 1;
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = 365;
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];

        }

        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('redpacketTitlePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            #没有缓存情况 存上数组
            $_sessionActiveTypeInfo = array();
            Session::set('redpacketTitlePrizeInfo', array($info));
        }
        if (!empty($_sessionActiveTypeInfo)) {
            array_push($_sessionActiveTypeInfo, $info);
            Session::set('redpacketTitlePrizeInfo', $_sessionActiveTypeInfo);
        }

        $_str = '';
        # 判断是否存在红包id $redpacketId
        if (!empty($redpacketId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($redpacketId);

            $_str = self::getTaskPrizeInfo($pro, 'DB');

        }
        /*getActiveArrInfo*/
        $_sessionActiveTypeInfo = Session('redpacketTitlePrizeInfo');
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getTaskPrizeInfo($arr, $mark)
    {

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                if ($v['person'] == -2) {
                    $_person = "未注册用户";
                } else if ($v['person'] == 2) {
                    $_person = "已注册用户";
                } else if ($v['person'] == 3) {
                    $_person = "所有会员";
                }
                if (isset($v['price']))
                    $intbal = sprintf("%.2f", $v['price']);


                $_str .= "<tr id='redpacketTitleTr" . $v['id'] . "'>
                            <td onclick=\"redpacketWrite('" . $v['id'] . "','" . $k . "','" . $v['redpacket_title'] . "','" . $mark . "','1')\">" . $v['redpacket_title'] . "</td>
                            <td onclick=\"redpacketWrite('" . $v['id'] . "','" . $k . "','" . $v['price'] . "','" . $mark . "','2')\">$intbal</td>
                            <td>$_person</td>
                          
                            <td>
                            <button class=\"layui-btn layui-btn-xs\" type=\"button\" onclick=\"redpacketDelInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                             <button class=\"layui-btn  layui-btn-xs\" type=\"button\" onclick=\"getVoucherSessionList('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['redpacket_title'] . "','" . $v['price'] . "','" . $v['person'] . "',)\">详情</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /**
     * @param $type 1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function getRedpacketList()
    {
        #获取缓存红包id 只有修改时候有
        $repacketId = Session('repacketId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('redpacketTitlePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # $repacketId
        if (!empty($repacketId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($repacketId);


            $_str = self::getTaskPrizeInfo($pro, 'DB');

        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $redpacketId 红包名称id
     * @content 红包名称 表 redpacket_title
     */
    static function getTaskContent($redpacketId)
    {
        #根据红包id  查询 红包名称  领取人群 价格  redpacket_title表
        $task_prize = Db::table('redpacket_title')->where(array('redpacket_id' => $redpacketId, 'status' => '1'))->select();
        return $task_prize;

    }
}