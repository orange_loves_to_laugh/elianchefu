<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/10/16
 * Time: 16:20
 */

/**
 * 活动管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

namespace app\service;

use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Session;

class ActiveService
{

    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function ActiveIndex($params)
    {
        #where条件  写成活的
        $where = empty($params['where']) ? ['active_putstatus' != '4'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('active a')
            ->field("*,
            (a.active_num - (select count(id) from log_active l where active_id = a.id)) surplus_num,(select ac.content_detail from active_content ac where ac.active_id = a.id) content_detail,
            a.active_mold activeType,a.active_limit
            ")
            ->where($where)->order('a.id desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data = $data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        #处理方法
        return self::InforDataDealWith($data, $params);
    }

    /*
      * @content ：处理方法
      * */
    static function InforDataDealWith($data, $params)
    {
        $time = date("Y-m-d H:i:s");
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if ($time < $v['active_start'] and $time < $v['active_end']) {
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['active_status'] = '待上架';

                } else if ($time >= $v['active_start'] and $time <= $v['active_end']) {
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['active_status'] = '进行中';

                } else if ($time > $v['active_start'] and $time > $v['active_end']) {
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['active_status'] = '已完成';

                }
                $data[$k]['activeGive'] = Db::table('active_giving')->where(array('active_id' => $v['id']))->count();
                if ($v['active_mold'] == 1) {
                    $data[$k]['active_mold_title'] = '商品';
                } else if ($v['active_mold'] == 2) {
                    $data[$k]['active_mold_title'] = '服务';
                } else {
                    $data[$k]['active_mold_title'] = '会员';
                }
            }
        }
        return $data;
    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function activetipsIndexCount()
    {
        return (int)Db::name('active')->where('active_putstatus != 4')->count();
    }

    /**
     * [ActiveSave 活动添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function ActiveSave($params)
    {
        $data = $params;
        ResourceService::$session_suffix = 'source_upload' . 'active' . $params['id'];
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'active_title',
                'error_msg' => '活动名称不能为空',
                'error_code' => 30002,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'active_title',
                'error_msg' => '活动原价不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'active_biz',
                'error_msg' => '活动价格不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'active_start',
                'error_msg' => '上架时间不能为空',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'active_end',
                'error_msg' => '下架时间不能为空',
                'error_code' => 30004,
            ],


        ];
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'active_imgurl',
                'error_msg' => '请上传缩略图图',
                'error_code' => 30001,
            ];
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'active_picurl',
                'error_msg' => '请上传banner图',
                'error_code' => 30001,
            ];
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'content_detail',
                'error_msg' => '请上传详情图',
                'error_code' => 30001,
            ];

        }
        $ret = ParamsChecked($params, $p);

//        if ($ret !== true) {
//            $error_arr = explode(',', $ret);
//
//
//            return json(DataReturn($error_arr[0], $error_arr[1]));
//        }
        #处理百度编辑器删除多余图片问题（上传了但是没有点击保存 或者删除了 又重新上传）
        //$data = self::HandleData($params);
        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        }
        if (isset($data['oldactive_imgurl'])) {
            unset($data['oldactive_imgurl']);
        }
        if (isset($data['content_detail'])) {
            $content_detail = $data['content_detail'];

            unset($data['content_detail']);
        }
        // 添加/编辑

        //dump($params);exit;
        if (empty($params['id'])) {


            #添加
            $data['update_status'] = 1;
            #添加处理
//            Db::startTrans();
            $activeId = Db::table("active")->insertGetId($data);

            if ($activeId) {
                Db::table("active_content")->insertGetId(array('active_id' => $activeId, 'content_detail' => $content_detail));
                #获取缓存
                $activeTypePro = Session('ActiveTypeInfo' . 'pro');
                if (!empty($activeTypePro)) {
                    $activeType = $activeTypePro;
                }
                $activeTypeService = Session('ActiveTypeInfo' . 'service');
                if (!empty($activeTypeService)) {
                    $activeType = $activeTypeService;
                }
                $activeTypeMember = Session('ActiveTypeInfo' . 'member');
                if (!empty($activeTypeMember)) {
                    $activeType = $activeTypeMember;
                }
                $activeGive = Session('ActiveGiveInfo');

                #创建活动
                if (!empty($activeType)) {
                    foreach ($activeType as $k => $v) {
                        $activeType[$k]['active_id'] = $activeId;
                        $activeType[$k]['active_title'] = $v['title'];
                        $activeType[$k]['settlement_price'] = json_encode($v['settlement_price']);
                        if ($v['expiration'] == '--') {
                            $activeType[$k]['expiration'] = 0;
                        }
                        unset($activeType[$k]['title']);

                    }
                    Db::table('active_detail')->insertAll($activeType);
                }

                if (!empty($activeGive)) {
                    foreach ($activeGive as $k => $v) {
                        $activeGive[$k]['active_id'] = $activeId;
                        if ($v['expiration'] == '--') {
                            $activeGive[$k]['expiration'] = 0;
                        }
                        if (isset($v['detail_id']))
                            $activeGive[$k]['giving_id'] = $activeGive[$k]['detail_id'];
                        if ($v['active_type'] == 1) {
                            #卡券表中card_type_id 商品 3
                            $card_type_id = 3;

                        } else if ($v['active_type'] == 2) {
                            #卡券表中card_type_id 服务 1
                            $card_type_id = 1;

                        } else if ($v['active_type'] == 7 or $v['active_type'] == 8 or $v['active_type'] == 9) {
                            # 商家消费券 先添加商家卡券
                            $giving_id = Db::table("merchants_voucher")->insertGetId(array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                                "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']));
                            unset($activeGive[$k]['title'], $activeGive[$k]['price'], $activeGive[$k]['min_consume'], $activeGive[$k]['num'], $activeGive[$k]['validity_day'],
                                $activeGive[$k]['is_type'], $activeGive[$k]['voucher_start'], $activeGive[$k]['cate_pid'], $activeGive[$k]['active_types'], $activeGive[$k]['prize_type'],
                                $activeGive[$k]['mark'], $activeGive[$k]['money_off'], $activeGive[$k]['active_title']
                            );
                            $activeGive[$k]['giving_id'] = $giving_id;
                        }
                        #类型为卡券时 上 card_voucher  查下卡券id 存上
                        $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => $card_type_id, 'server_id' => $activeGive[$k]['detail_id']))->value('id');
                        if (!empty($voucher_id)) {
                            $activeGive[$k]['giving_id'] = $voucher_id;

                        }
                        $activeGive[$k]['giving_type'] = $activeGive[$k]['active_type'];
                        $activeGive[$k]['giving_number'] = $activeGive[$k]['detail_number'];
                        $activeGive[$k]['settlement_price'] = json_encode($activeGive[$k]['settlement_price']);
                        if (isset($v['detail_price']))
                            $activeGive[$k]['giving_price'] = $activeGive[$k]['detail_price'];

                        if ($v['active_type'] == 5) {

                            $activeGive[$k]['expiration'] = 0;

                        }
                        unset($activeGive[$k]['title'], $activeGive[$k]['detail_id'], $activeGive[$k]['detail_number'], $activeGive[$k]['detail_price'], $activeGive[$k]['active_type']);
                    }
                    Db::table('active_giving')->insertAll($activeGive);
                }


            }


        } else {

            #修改
            $data['update_status'] = 2;
            if (empty($data['active_mold'])) {
                unset($data['active_mold']);

            }
            Db::table("active")->where(array('id' => $params['id']))->update($data);
            if (!empty($content_detail)) {
                Db::table("active_content")->where(array('active_id' => $params['id']))->update(array('content_detail' => $content_detail));

            }

            #获取缓存
            $activeTypePro = Session('ActiveTypeInfo' . 'pro');
            if (!empty($activeTypePro)) {
                $activeType = $activeTypePro;
                $_active_type = 1;
            }
            $activeTypeService = Session('ActiveTypeInfo' . 'service');
            if (!empty($activeTypeService)) {
                $activeType = $activeTypeService;
                $_active_type = 2;

            }
            $activeTypeMember = Session('ActiveTypeInfo' . 'member');
            if (!empty($activeTypeMember)) {
                $activeType = $activeTypeMember;
                $_active_type = 3;
                Db::table('active_detail')->where("active_id = '" . $params['id'] . "' and active_type =$_active_type ")->delete();
            }
            $activeGive = Session('ActiveGiveInfo');


            if (!empty($activeType)) {
                foreach ($activeType as $k => $v) {
                    $activeType[$k]['active_id'] = $params['id'];
                    $activeType[$k]['active_title'] = $v['title'];
                    if ($v['expiration'] == '--') {
                        $activeType[$k]['expiration'] = 0;
                    }
                    unset($activeType[$k]['title']);

                }
                Db::table('active_detail')->where("active_id = '" . $params['id'] . "' and active_type !=$_active_type ")->delete();
                $a = Db::table('active_detail')->insertAll($activeType);
            }

            if (!empty($activeGive)) {
                foreach ($activeGive as $k => $v) {
                    $activeGive[$k]['active_id'] = $params['id'];
                    if ($v['expiration'] == '--') {
                        $activeGive[$k]['expiration'] = 0;
                    }
                    if (isset($v['detail_id']))
                        $activeGive[$k]['giving_id'] = $activeGive[$k]['detail_id'];
                    if ($v['active_type'] == 1) {
                        #卡券表中card_type_id 商品 3
                        $card_type_id = 3;

                    } else if ($v['active_type'] == 2) {
                        #卡券表中card_type_id 服务 1
                        $card_type_id = 1;

                    } else if ($v['active_type'] == 7 or $v['active_type'] == 8) {
                        # 商家消费券 先添加商家卡券
                        $giving_id = Db::table("merchants_voucher")->insertGetId(array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                            "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']));
                        unset($activeGive[$k]['title'], $activeGive[$k]['price'], $activeGive[$k]['min_consume'], $activeGive[$k]['num'], $activeGive[$k]['validity_day'],
                            $activeGive[$k]['is_type'], $activeGive[$k]['voucher_start'], $activeGive[$k]['cate_pid'], $activeGive[$k]['active_types'], $activeGive[$k]['prize_type'],
                            $activeGive[$k]['mark'], $activeGive[$k]['money_off'], $activeGive[$k]['active_title']
                        );
                        $activeGive[$k]['giving_id'] = $giving_id;
                    } else {
                        $activeGive[$k]['giving_id'] = 0;
                        $activeGive[$k]['giving_price'] = 0;
                    }
                    #类型为卡券时 上 card_voucher  查下卡券id 存上
                    $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => $card_type_id, 'server_id' => $activeGive[$k]['detail_id']))->value('id');
                    if (!empty($voucher_id)) {
                        $activeGive[$k]['giving_id'] = $voucher_id;

                    }

                    $activeGive[$k]['giving_type'] = $activeGive[$k]['active_type'];
                    $activeGive[$k]['giving_number'] = $activeGive[$k]['detail_number'];
                    if (isset($v['detail_price']))
                        $activeGive[$k]['giving_price'] = $activeGive[$k]['detail_price'];

                    if ($v['active_type'] == 5) {
                        $activeGive[$k]['expiration'] = 0;

                    }
                    unset($activeGive[$k]['title'], $activeGive[$k]['detail_id'], $activeGive[$k]['detail_number'], $activeGive[$k]['detail_price'], $activeGive[$k]['active_type']);
                }
                Db::table('active_giving')->insertAll($activeGive);
            }
            #修改活动的时候  清一下redis
            $_redis = new Redis();
            $_redis->hDel('activeInfo', $params['id']);
        }

        // dump($params);exit;
        if (isset($params['active_picurl']) && strlen($params['active_picurl'])) {
            $bannerArr = BaseService::HandleImg($params['active_picurl']);

            //$bannerArr = explode(',', $params['active_picurl']);
            ResourceService::delCacheItem($bannerArr);
        }

        if (isset($params['content_detail']) && strlen($params['content_detail'])) {
            $bannerArr = BaseService::HandleImg($params['content_detail']);

            ResourceService::delCacheItem($bannerArr);
        }
        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id'] = empty($params['id']) ? '' : $params['id'];

        #删除多图(数组形式)

        #删除单图
        ResourceService::delCacheItem($params['active_imgurl']);
        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('activeId');
        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('ActiveTypeInfo' . 'pro');
        Session::delete('ActiveTypeInfo' . 'service');
        Session::delete('ActiveTypeInfo' . 'member');
        Session::delete('ActiveGiveInfo');

        return json(DataReturn('保存成功', 0));


    }

    /**
     * 保存前数据处理
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    public static function HandleData($params)
    {

        if (isset($params["content_detail"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'active';
            $params['ueditor_content'] = $params['content_detail'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params['thumb_img'])) {
            unset($params['thumb_img']);
        }
        if (isset($params['oldactive_imgurl'])) {
            unset($params['oldactive_imgurl']);
        }
        return $params;
    }
    /******商品 服务  会员  弹窗***********/
    /**
     * activeTypeService 点击创建 商品 服务  会员  处理(二级分类)
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function activeTypeService($params, $mtype)
    {

        #创建活动类型
        $class = self::activeService($params['type'], $params['id'], $mtype);

        if (!empty($params['id'])) {

            #分类弹窗html 第二次点击
            $data = self::activePopupTwo($class, $params['type'], $params['mark'], $mtype);
        } else {

            #分类弹窗html 第一次点击
            $data = self::activeTypePopup($class, $params['type'], $params['mark'], $mtype);
        }

        return $data;
    }

    /**
     *  activeBizproName 根据二级个分类id 查询商品名称显示
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function activeBizproName($params, $mtype)
    {
        $class = self::activeBizService($params['type'], $params['id'], $params['mark']);

        #分类弹窗html 第三次点击
        $data = self::activePopupThree($class, $params['type'], $params['mark'], $mtype);

        return $data;

    }

    #查询赠送服务  和创建
    static function activeService($type, $id, $mtype)
    {

        if ($type == 'pro') {
            #查询商品分类
            $_where = empty($id) ? array('status' => '1', 'class_id' => '0') : array('status' => '1', 'class_id' => $id);
            $class = Db::table('pro_class')->field('id,class_title,1 classtype')->where($_where)->select();
            if (empty($class)) {
                $_where = "biz_pro_class_id=$id and biz_pro_status = 1 and biz_pro_type = 0 and biz_pro_paret =1";
                $class = Db::table('biz_pro')->field('id,biz_pro_title class_title,2 classtype')->where($_where)->select();
            }

        } else if ($type == 'service') {
            if (!empty($id)) {
                if ($mtype == 'relevance_service') {
                    #查询服务名称  跟据服务分类id  和缓存中的服务id    这是在服务修改是点击关联其他服务进来的（不能关联自己）
                    $serviceId = session('serviceId');
                    $class = Db::table('service')->field('id,service_title class_title')->where(array('is_del' => '2', 'service_class_id' => $id))->where("id != $serviceId")->select();
                } else {
                    #查询服务名称  跟据服务分类id
                    $class = Db::table('service')->field('class_pid,id,service_title class_title')->where(array('is_del' => '2', 'class_pid' => $id))->select();

                }


            } else {
                #查询服务分类
                $class = Db::table('service_class')->field('id,service_class_title class_title')->where(array('is_del' => '2', 'service_class_pid' => '0'))->select();
            }
        } else if ($type == 'member') {
            #查询会员分类
            $class = Db::table('member_level')->field('level_title class_title,id')->select();

        }
        return $class;
    }

    #查询赠送服务give  和创建 type
    static function activeBizService($type, $id, $mark)
    {

        if ($type == 'pro') {
            #查询商品分类
            if ($mark == 'type') {
                #创建
                $_where = "biz_pro_class_id=$id and biz_pro_status = 1 and biz_pro_type = 0";

            } else {
                #赠送
                $_where = "biz_pro_class_id='$id' and biz_pro_status = 1 ";

            }

            $class = Db::table('biz_pro')->field('id,biz_pro_title class_title')->where($_where)->select();
        }
        return $class;
    }

    # 分类弹窗 第一次(商品 服务 会员都有) $mtype:active:活动管理  task :任务管理
    static function activeTypePopup($arr, $type, $mark, $mtype)
    {

        $_str = '';
        if (!empty($arr)) {
            foreach ($arr as $k => $v) {
                $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 110px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"activeCreateGive('" . $v['id'] . "','" . $type . "','" . $mark . "','" . $mtype . "')\">" . $v['class_title'] . "</button></div>";

            }
            return $_str;

        }
    }

    # 分类弹窗 第二次（商品 服务 有  会员没有）
    static function activePopupTwo($arr, $type, $mark, $mtype)
    {

        $pid = '';
        if (count($arr) > 0) {
            $pid = $arr[0]['class_pid'];
        }

        $_str = ' <div class="card-body" id="ppp">
                    
                        <div class="form-group">
                            <input type="text" id="search_title"  data-pid="' . $pid . '" class="form-control" onkeyup="searchRelevanceStore()" placeholder="请输入名称" autocomplete="off">
                            <i class="form-group__bar"></i>
                        </div>
                    
                ';

        if (!empty($arr)) {
            if ($type == 'pro') {
                foreach ($arr as $k => $v) {
                    $_str .= "<div class='lump" . $v['id'] . "'   style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 110px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"activeBizproName('" . $v['id'] . "','" . $type . "','" . $mark . "','" . $mtype . "','" . $v['classtype'] . "')\">" . $v['class_title'] . "</button></div>";
                }
            } else {
                foreach ($arr as $k => $v) {
                    $_str .= "<div class='lump" . $v['id'] . "'   style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 400px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 20px' ><button class='btn btn-info'  onclick=\"activeBizproName('" . $v['id'] . "','" . $type . "','" . $mark . "','" . $mtype . "')\">" . $v['class_title'] . "</button></div>";

                }
            }
            $_str .= '</div>';
            return $_str;

        }
    }

    # 分类弹窗 第三次（商品 有  服务，会员没有） $mtype  task 是从业务页面进来的  active 是从活动管理页面进来的
    static function activePopupThree($arr, $type, $mark, $mtype)
    {


        $_str = '';
        if (!empty($arr)) {
            if ($mtype == 'task') {
                #任务管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"taskSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }

            } else if ($mtype == 'active') {
                #活动管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"activeSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }
            } else if ($mtype == 'redPacket') {
                #活动管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"voucherSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }
            } else if ($mtype == 'sign') {
                #签到管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"signSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }
            } else if ($mtype == 'upgrade') {
                #升级管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"upgradeSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }
            } else if ($mtype == 'member_voucher') {
                #卡券管理
                foreach ($arr as $k => $v) {
                    $_str .= "<div  style='padding: 5px 8px;display: flex;box-sizing: border-box;width: 350px;float: left;justify-content: center;margin: 2px;background-color: #17a2b8;margin-left: 0px' ><button class='btn btn-info'  onclick=\"memberVoucherSession('" . $v['id'] . "','" . $type . "','" . $mark . "')\">" . $v['class_title'] . "</button></div>";

                }
            }

            return $_str;

        }
    }

    /**
     *  activeSession 服务  商品  会员 积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function activeSession($params, $activeId)
    {
        $type = $params['prize_type'];
        $id = $params['id'];
        $mark = $params['mark'];

        if ($type == 'pro') {
            $_type = 1;
            #商品
            #查询商品信息 名称 线上价格
            $info = ActiveService::BizServiceTitle($id, $type);

            $info['active_type'] = 1;
            $info['detail_number'] = $params['detail_number'];
            $info['settlement_price'] = $params['settlement_price'];


        } else if ($type == 'service') {
            $_type = 2;
            #服务
            $info = ActiveService::BizServiceTitle($id, $type);
            $info['active_type'] = 2;
            $info['detail_number'] = $params['detail_number'];
            $info['settlement_price'] = $params['settlement_price'];

        } else if ($type == 'member') {
            $_type = 3;

            #会员
            $info = Db::table('member_level')->field('level_title title,id detail_id,level_practical detail_price')->where(array('id' => $id))->find();
            $info['active_type'] = 3;
            $info['detail_number'] = 1;
            $info['settlement_price'] = 0;

        } else if ($type == 'integral') {
            #积分  $id 是积分
            $info['title'] = '积分';
            $info['active_type'] = 4;
            $info['detail_number'] = $id;
            $info['detail_price'] = $id;
            $info['settlement_price'] = 0;


        } else if ($type == 'balance') {
            #余额 $id 是传过来的余额
            $info['title'] = '余额';
            $info['active_type'] = 5;
            $info['detail_number'] = $id;
            $info['detail_price'] = $id;
            $info['settlement_price'] = 0;

        } else if ($type == 'merchantVoucher' or $type == 'lifeVoucher' or $type == "propertyVoucher") {
            $info = $params;
        }
        if ($type == 'balance' or $type == 'member' or $type == 'integral') {
            $info['expiration'] = '--';

        } else {
            $info['expiration'] = $params['expiration'];
            $info['title'] = $params['active_title'] . '券';

        }

        ###########判断显示活动赠送的 还是活动创建的################
        if ($mark == 'type') {
            #拼接活动说明
            $active_explain = '';
            #创建商品 服务 会员
            #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上
            if ($type == 'pro') {
                Session::delete('ActiveTypeInfo' . 'service');
                Session::delete('ActiveTypeInfo' . 'member');
            } else if ($type == 'service') {
                Session::delete('ActiveTypeInfo' . 'pro');
                Session::delete('ActiveTypeInfo' . 'member');

            } else if ($type == 'member') {
                Session::delete('ActiveTypeInfo' . 'pro');
                Session::delete('ActiveTypeInfo' . 'service');

            }

            $_sessionActiveTypeInfo = Session("ActiveTypeInfo" . $type);
            if ($type == 'member') {
                Session::set('ActiveTypeInfo' . $type, array($info));
                $_sessionActiveTypeInfo = Session("ActiveTypeInfo" . $type);
            } else {
                if (empty($_sessionActiveTypeInfo)) {
                    $_sessionActiveTypeInfo = array();
                }
                #每个类型 都存一个缓存  点击下一个  同时 删除 哪2 个 缓存
                if (!empty($_sessionActiveTypeInfo)) {
                    # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
                    if (array_key_exists('S' . $type . $info['detail_id'], $_sessionActiveTypeInfo)) {
                        $_sessionActiveTypeInfo['S' . $type . $info['detail_id']]['detail_number'] = intval($_sessionActiveTypeInfo['S' . $type . $info['detail_id']]['detail_number']) + intval($info['detail_number']);
                    } else {

                        $_sessionActiveTypeInfo['S' . $type . $info['detail_id']] = $info;
                    }
                } else {
                    $_sessionActiveTypeInfo['S' . $type . $info['detail_id']] = $info;

                }
                #
                Session::set('ActiveTypeInfo' . $type, $_sessionActiveTypeInfo);
                $_str = '';
                # 判断是否存在$activeId
                if (!empty($activeId)) {
                    $active_explain = Db::table('active')->where(array('id' => $activeId))->value('active_explain');
                    #查询 商品 服务会员  统一放在数组里  返回
                    $pro = Db::table('active_detail ad')->where(array('active_id' => $activeId, 'active_type' => $_type))->select();
                    #当创建类型和数据库的相同  才显示数据库数据
                    foreach ($pro as $k => $v) {
                        if ($v['active_type'] == 1) {
                            #商品
                            $title = Db::table('biz_pro')->field('biz_pro_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['biz_pro_title'];


                        } else if ($v['active_type'] == 2) {

                            $title = Db::table('service')->field('service_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['service_title'];


                        } else {
                            #会员
                            $title = Db::table('member_level')->field('level_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['level_title'];
                        }

                        #拼接活动说明  在这里 说明 不为空  先获取数据库中拼接的   在拼接上缓存的   如果数据库没有 直接缓存拼接


                    }


                    $_str = self::getActiveArrInfo($pro, 'DB', $type, $mark);
                    $DbTotalPrice = self::getTotalPrice($pro);


                }

            }
            $active_explain .= self::getActiveExplain($_sessionActiveTypeInfo, $activeId, $type);


        } else {
            #赠送商品 服务
            #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

            $_sessionActiveTypeInfo = Session('ActiveGiveInfo');
            if (empty($_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo = array();
            }
            if (!empty($_sessionActiveTypeInfo)) {
                # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
                if (array_key_exists('S' . $type . $info['detail_id'], $_sessionActiveTypeInfo)) {
                    $_sessionActiveTypeInfo['S' . $type . $info['detail_id']]['detail_number'] = intval($_sessionActiveTypeInfo['S' . $type . $info['detail_id']]['detail_number']) + intval($info['detail_number']);
                } else {

                    $_sessionActiveTypeInfo['S' . $type . $info['detail_id']] = $info;
                }
            } else {
                $_sessionActiveTypeInfo['S' . $type . $info['detail_id']] = $info;

            }

            Session::set('ActiveGiveInfo', $_sessionActiveTypeInfo);
            $_str = '';
            # 判断是否存在$activeId
            if (!empty($activeId)) {
                #查询 商品 服务会员  统一放在数组里  返回
                #赠送表
                $pro = Db::table('active_giving')->field('id,giving_type active_type,giving_id detail_id,giving_number detail_number,giving_price detail_price,expiration')->where(array('active_id' => $activeId))->select();
                foreach ($pro as $k => $v) {
                    if ($v['active_type'] == 1) {
                        #商品 $v['detail_id'] 为商品卡券id
                        $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['detail_id'], 'pro');


                    } else if ($v['active_type'] == 2) {
                        #服务 $v['detail_id'] 为服务卡券id
                        $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['detail_id'], 'service');


                    } else if ($v['active_type'] == 4) {
                        #积分
                        $pro[$k]['title'] = '积分';
                    } else if ($v['active_type'] == 5) {
                        #余额
                        $pro[$k]['title'] = '余额';


                    }

                }
                $_str = self::getActiveArrInfo($pro, 'DB', $type, $mark);
            }
        }
        $StotalPrice = self::getTotalPrice($_sessionActiveTypeInfo);

        #获取这些商品  服务 办理会员的  价格总和
        $totalPrice = intval($DbTotalPrice) + intval($StotalPrice);
        #type  :  表示创建  give  表示赠送

        $_str .= self::getActiveArrInfo($_sessionActiveTypeInfo, 'S', $type, $mark);


        return array('status' => true, 'str' => $_str, 'totalPrice' => $totalPrice, 'active_explain' => $active_explain);

//        return $_str;

    }

    /*
     * @content 拼接 活动说明
     * */
    static function getActiveExplain($_session, $activeId, $type)
    {
        /*购买(活动名称);赠送-----赠送的内容 数量价值   赠送洗车券1张价值78元*/
        $count = count($_session);
        $dou = '';
        if ($count > 1) {
            $dou = ',';
        }
        if (!empty($_session)) {
            if ($activeId) {
                $active_explain = '';
                foreach ($_session as $k => $v) {
                    if ($type == 'member') {
                        $active_explain .= '办理E联车服' . $v['title'] . '余额到账' . $v['detail_price'] . '元';

                    } else {
                        $price = $v['detail_number'] * $v['detail_price'];

                        $active_explain .= $dou . '赠送' . $v['title'] . $v['detail_number'] . '张价值' . $price . '元';

                    }
                }
            } else {
                $active_explain = '';

                foreach ($_session as $k => $v) {
                    if ($type == 'member') {
                        $active_explain .= '办理E联车服' . $v['title'] . '余额到账' . $v['detail_price'] . '元';

                    } else {
                        $price = $v['detail_number'] * $v['detail_price'];
                        $active_explain .= $dou . '赠送' . $v['title'] . $v['detail_number'] . '张价值' . $price . '元';

                    }
                }
            }

            return $active_explain;
        }

    }

    /*
     * @contetn:获取卡券中的 商品名称  或 服务名称
     * */
    static function getCardVoucherProServiceTitle($voucher_id, $mark, $markType = true)
    {

        if ($mark == 'pro') {
            #商品名称
            $title = Db::table('card_voucher c')->join('biz_pro b', 'b.id = c.server_id', 'left')->where(array('c.id' => $voucher_id))->value('biz_pro_title');
        } elseif ($mark == 'service') {
            $title = Db::table('card_voucher c')->join('service s', 's.id = c.server_id', 'left')->where(array('c.id' => $voucher_id))->value('service_title');
        } else {
            if ($markType) {
                $title = Db::table('merchants_voucher c')->where(array('c.id' => $voucher_id))->value('title');
            } else {
                $title = Db::table('merchants_voucher c')->where(array('c.id' => $voucher_id))->find();
            }
        }
        return $title;

    }

    /**
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function getActiveTypeList($id, $type, $activeMark)
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('activeId');
        if ($activeMark == 'type') {
            if ($type == 1) {
                $activeType = 'pro';

            } else if ($type == 2) {
                $activeType = 'service';

            } else if ($type == 3) {
                $activeType = 'member';

            }
            #创建商品 服务 会员
            #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上
            $_sessionActiveTypeInfo = Session('ActiveTypeInfo' . $activeType);
            if (empty($_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo = array();
            }
            #不是会员
            if ($type != 3) {
                $_str = '';
                # 判断是否存在$activeId
                if (!empty($activeId)) {
                    #查询 商品 服务会员  统一放在数组里  返回
                    $pro = Db::table('active_detail ad')->where(array('active_id' => $activeId, 'active_type' => $type))->select();
                    foreach ($pro as $k => $v) {
                        if ($v['active_type'] == 1) {
                            #商品
                            $title = Db::table('biz_pro')->field('biz_pro_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['biz_pro_title'];
                        } else if ($v['active_type'] == 2) {
                            #服务
                            $title = Db::table('service')->field('service_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['service_title'];
                        } else {
                            #会员
                            $title = Db::table('member_level')->field('level_title')->where(array('id' => $v['detail_id']))->find();
                            $pro[$k]['title'] = $title['level_title'];
                        }

                    }


                    $_str = self::getActiveArrInfo($pro, 'DB', $type, $activeMark);
                    $DbTotalPrice = self::getTotalPrice($pro);

                }
            }


        } else {
            #赠送商品 服务
            #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

            $_sessionActiveTypeInfo = Session('ActiveGiveInfo');
            if (empty($_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo = array();
            }
            $_str = '';
            # 判断是否存在$activeId
            if (!empty($activeId)) {
                #查询 商品 服务会员  统一放在数组里  返回
                #赠送表
                $pro = Db::table('active_giving')->field('id,giving_type active_type,giving_id detail_id,giving_number detail_number,giving_price detail_price,expiration,settlement_price')->where(array('active_id' => $activeId))->select();
                foreach ($pro as $k => $v) {
                    if ($v['active_type'] == 1) {
                        #商品
                        $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['detail_id'], 'pro');

                    } else if ($v['active_type'] == 2) {
                        #服务
                        #服务 $v['detail_id'] 为服务卡券id
                        $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['detail_id'], 'service');

                    } else if ($v['active_type'] == 4) {
                        #积分
                        $pro[$k]['title'] = '积分';
                    } else if ($v['active_type'] == 5) {
                        #余额
                        $pro[$k]['title'] = '余额';


                    }

                }
                $_str = self::getActiveArrInfo($pro, 'DB', $type, $activeMark);

            }
        }
        $StotalPrice = self::getTotalPrice($_sessionActiveTypeInfo);

        #获取这些商品  服务 办理会的  价格总和
        $totalPrice = intval($DbTotalPrice) + intval($StotalPrice);
        #type  :  表示创建  give  表示赠送
        $_str .= self::getActiveArrInfo($_sessionActiveTypeInfo, 'S', $type, $activeMark);
        return array('status' => true, 'str' => $_str, 'totalPrice' => $totalPrice);
//        return array('status'=>true,'str'=>$_str,'totalPrice'=>$totalPrice);
//        return $_str;

    }


    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  会员  积分 卡券 信息整理
     */
    static function getActiveArrInfo($arr, $mark, $type, $activeMark)
    {

        $_str = '';
        $totalPrice = 0;
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                $v['totalPrice'] .= intval($totalPrice) + intval($v['detail_price']);
                if ($v['active_type'] == 1) {
                    $_active_type_title = "商品";
                } else if ($v['active_type'] == 2) {
                    $_active_type_title = "服务";

                } else if ($v['active_type'] == 3) {
                    $_active_type_title = "会员";
                } else if ($v['active_type'] == 4) {
                    $_active_type_title = "积分";
                } else if ($v['active_type'] == 5) {
                    $_active_type_title = "余额";
                } else if ($v['active_type'] == 7) {
                    $_active_type_title = "商家消费券";
                } else if ($v['active_type'] == 8) {
                    $_active_type_title = "有派生活消费券";
                }
                if (!is_array($v['settlement_price'])) {
                    $_settlement = json_decode($v['settlement_price'], true);
                    $_settlementPrice = $v['settlement_price'];
                } else {
                    $_settlement = $v['settlement_price'];
                    $_settlementPrice = json_encode($v['settlement_price']);
                }
                $_settlementPrice = htmlentities($_settlementPrice);
                $expiration = " <td onclick=\"activeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['expiration'] . "','" . $mark . "','5','" . $v['active_type'] . "','" . $activeMark . "','" . $v['active_type'] . "')\">" . $v['expiration'] . "</td>";
//                    $settlement_price = " <td onclick=\"activeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['settlement_price'] . "','" .$mark . "','6','" . $v['active_type'] . "','" . $activeMark. "','" . $v['active_type']. "')\">" . $v['settlement_price'] . "</td>";
                if ($activeMark == 'type' and $v['active_type'] != 3) {
                    $settlement_price = " <td>
 小型车:<input onclick=\"activeTypeWrite('{$v['id']}','{$k}','{$_settlement[1]}','{$mark}','6','{$v['active_type']}','{$activeMark}','{$v['active_type']}','{$_settlementPrice}','1')\" type='number' value='{$_settlement[1]}' readonly class='form-control form-control-lg pricehtml'>
 中型车:<input onclick=\"activeTypeWrite('{$v['id']}','{$k}','{$_settlement[2]}','{$mark}','6','{$v['active_type']}','{$activeMark}','{$v['active_type']}','{$_settlementPrice}','2')\" type='number' value='{$_settlement[2]}' readonly class='form-control form-control-lg pricehtml'>
 大型车:<input onclick=\"activeTypeWrite('{$v['id']}','{$k}','{$_settlement[3]}','{$mark}','6','{$v['active_type']}','{$activeMark}','{$v['active_type']}','{$_settlementPrice}','3')\" type='number' value='{$_settlement[3]}' readonly class='form-control form-control-lg pricehtml'>
</td>";
                }


                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['title'] . "</td>
                            <td>$_active_type_title</td>
                            <td onclick=\"activeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['detail_number'] . "','" . $mark . "','3','" . $v['active_type'] . "','" . $activeMark . "','" . $v['active_type'] . "')\">" . $v['detail_number'] . "</td>
                            <td >" . sprintf("%.2f", $v['detail_price']) . "</td>
                           $expiration
                           $settlement_price
                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['active_type'] . "','" . $activeMark . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /*
     * @content ：获取商品服务总金额
     * */
    static function getTotalPrice($arr)
    {

        $totalPrice = 0;
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                $totalPrice = (intval($v['detail_number']) * intval($totalPrice)) + (intval($v['detail_number']) * intval($v['detail_price']));
            }
        }
        return $totalPrice;
    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 活动内容详情  赠送表
     */
    function getActiveContent($activeId, $mark)
    {
        if ($mark == 'type') {
            $pro = Db::table('active_detail ad')->where(array('active_id' => $activeId))->select();
            foreach ($pro as $k => $v) {
                if ($v['expiration'] == 0) {
                    $pro[$k]['expiration'] = '--';
                }
                if ($v['active_type'] == 1) {
                    #商品
                    $title = Db::table('biz_pro')->field('biz_pro_title')->where(array('id' => $v['detail_id']))->find();
                    $pro[$k]['title'] = $title['biz_pro_title'];


                } else if ($v['active_type'] == 2) {
                    #服务
                    $title = Db::table('service')->field('service_title')->where(array('id' => $v['detail_id']))->find();
                    $pro[$k]['title'] = $title['service_title'];

                } else {
                    #会员
                    $title = Db::table('member_level')->field('level_title')->where(array('id' => $v['detail_id']))->find();
                    $pro[$k]['title'] = $title['level_title'];
                }
                $pro[$k]['title'] = $v['active_title'];
                $pro[$k]['settlement'] = json_decode($v['settlement_price'], true);


            }


        } else {
            #赠送表
            $pro = Db::table('active_giving')->where(array('active_id' => $activeId))->select();

            foreach ($pro as $k => $v) {
                if ($v['expiration'] == 0) {
                    $pro[$k]['expiration'] = '--';
                }
                if ($v['giving_type'] == 1) {
                    #商品
                    $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['giving_id'], 'pro');


                } else if ($v['giving_type'] == 2) {
                    #服务
                    #服务 $v['detail_id'] 为服务卡券id
                    $pro[$k]['title'] = self::getCardVoucherProServiceTitle($v['giving_id'], 'service');


                } else if ($v['giving_type'] == 4) {
                    #积分
                    $pro[$k]['title'] = '积分';
                } else if ($v['giving_type'] == 5) {
                    #余额
                    $pro[$k]['title'] = '余额';


                } else if ($v['giving_type'] == 7 or $v['giving_type'] == 8) {
                    # 商家卡券
                    $title = Db::table('merchants_voucher')->field('title')->where(array('id' => $v['giving_id']))->find();
                    $pro[$k]['title'] = $title['title'];

                }
                $pro[$k]['settlement'] = json_decode($v['settlement_price'], true);
            }

        }
        return $pro;

    }

    /**
     * @param $arr
     * @return array
     * @content 获取商品名称   服务信息 价格  价格查询的是 大型车的服务价格
     */
    static function BizServiceTitle($id, $type, $mtype = '')
    {
        if ($type == 'pro') {
            #查询商品名称
            $_where = "id='$id' and biz_pro_status = 1 ";
            $info = Db::table('biz_pro')->field('biz_pro_title title,id detail_id,biz_pro_online detail_price')->where($_where)->find();
        } else {

            if ($mtype == 'relevance_service') {
                $_where = "s.id='$id' and service_status = 1 and scm.service_type = 1 ";

                $info = Db::table('service s')
                    ->join('service_car_mediscount scm', 'scm.service_id = s.id', 'left')
                    ->field('s.service_title title,s.id detail_id')
                    ->where($_where)
                    ->find();
            } else {
                $_where = "s.id='$id' and service_status = 1 and scm.service_type = 1 and scm.car_level_id=3";
                $info = Db::table('service s')
                    ->field('s.service_title title,s.id detail_id,scm.discount detail_price')
                    ->join('service_car_mediscount scm', 'scm.service_id = s.id', 'left')
                    ->where($_where)
                    ->find();
            }

        }
        return $info;
    }

}
