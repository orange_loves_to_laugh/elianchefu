<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/28 0028
 * Time: 20:45
 */

namespace app\service;

use think\Db;


/**
 * 权限管理层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2021年1月30日14:35:01
 */
class PowerService
{

    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月30日14:35:01
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {
        $where = [];

        //上级模块
        if(!empty($params['param']['pid']))
        {
            $where[] =['pid', '=', $params["param"]['pid']];
        }
        //权限类型
        if(!empty($params['param']['type']))
        {
            $where[] =['type', '=', $params["param"]['type']];
        }
        //权限级别
        if(!empty($params['param']['level']))
        {
            $where[] =['level', '=', $params["param"]['level']];
        }
        //url
        if(isset($params['param']['url']))
        {
            $where[] =['url', '=', $params["param"]['url']];
        }
        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['title', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        return $where;
    }
    /**
     * 权限数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PowerDataSave($params = []){

        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '名称不能为空',
                'error_code'         => 23001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'type',
                'error_msg'         => '类型不能为空',
                'error_code'         => 23002,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);

            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        // 添加/编辑
        if(empty($params['id']))
        {
            $re = Db::name('power')->insertGetId($params);
        } else {

            $re= Db::name('power')->where(['id'=>intval($params['id'])])->update($params);

        }
        if (!$re)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>23001,'msg'=>'保存权限数据失败','status'=>false,'debug'=>false]);
        }
        return DataReturn('保存成功', 0);
    }
    /**
     * 权限数据查询
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PowerDataList($params){
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        $data = Db::name('power')->where($where)->field($field)->order($order)->group($group);

        // 获取列表

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
            self::PowerDataDealWith($data['data']);

            return $data;
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
            return self::PowerDataDealWith($data);
        }

    }
    /**
     * 权限数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [带处理数据]
     */
    public static function PowerDataDealWith(&$data)
    {

        if(!empty($data))
        {
            foreach($data as &$v)
            {
                //上级模块
                if (isset($v['pid']))
                {
                    if ($v['pid']>0) {
                        $v['pid_title']=Db::name('power')->where('id='.$v['pid'])->value('title');
                    }  else{
                        $v['pid_title']='顶级模块';
                    }
                }
                //权限级别
                if (isset($v['level'])) {
                    $v['level_title']=BaseService::StatusHtml($v['level'],[1=>'一级',2=>'二级',3=>'三级',4=>'四级'],false);
                }
            }
        }

        return $data;
    }

    /**
     * 账号列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月30日14:35:01
     * @param    [array]          $params [输入参数]
     */
    public static function AccountListWhere($params = [])
    {
        $where = [['id','>',1]];

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['username', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        return $where;
    }

    /**
     * 账号数据查询
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function AccountDataList($params){
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        $data = Db::name('admin')->where($where)->field($field)->order($order)->group($group);



        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();

           self::AccountDealWith($data['data']);
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
           self::AccountDealWith($data);
        }
        return DataReturn('ok',0,$data);
    }
    /**
     * 修改/添加账号
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2021年2月6日14:40:33
     * @desc    description
     * @param    [array]          $data [待处理数据]
     */
    public static function AccountDealWith(&$data){

        if (!empty( $data))
        {
            foreach ($data as &$v) {
                $employee=Db::name('employee_sal')->where('id='.$v['employee_id'])->find();

                $v['employee_name']=$v['role_id']==1?'超级管理员':$employee['employee_name'];
                //岗位信息
                $v['employee_station_title']=EmployeeService::StationDataHandle($employee['employee_station_id']);

                //一级部门
                $v['employee_department_title']=EmployeeService::DepartmentDataHandle($v['employee_department_id']);

                //二级部门
                $v['employee_department_sectitle']=EmployeeService::DepartmentDataHandle($v['employee_department_secid']);
                //角色
                $v['role_title']=$v['role_id']==1?'超级管理员':'普通管理员账号';

                //权限列表
                $v['power_arr']= Db::name('power_admin')->where('admin_id='.$v['id'])->column('power_id');
//                    Db::name('power')->alias('p')
//                    ->leftJoin(['power_admin'=>'pa'],'pa.power_id=p.id')
//                    ->where('pa.admin_id='.$v['id'])
//                    ->column('p.id');

                //用户权限开启判断
                $v['power_status_member']=Db::name('power_admin')
                    ->where([['admin_id','=',$v['id']],['power_id','=',15]])
                    ->count();
                $v['power_status_member']=$v['power_status_member']>0?1:2;

                //数据中心权限开启判断
                $v['power_status_datacenter']=Db::name('power_admin')
                    ->where([['admin_id','=',$v['id']],['power_id','=',14]])
                    ->count();
                $v['power_status_datacenter']=$v['power_status_datacenter']>0?1:2;

            }
        }
        //dump($data);exit;
        return $data;
    }

    /**
     * 修改/添加账号
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2021年2月6日14:40:33
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveAccount($params){
        // 请求参数
        $p = [
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'name',
//                'error_msg'         => '账号名称不能为空',
//                'error_code'         => 23004,
//            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'mobile',
                'error_msg'         => '手机号不能为空',
                'error_code'         => 23005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '登录账号不能为空',
                'error_code'         => 23006,
            ],



        ];
        if ($params['id']<1)
        {
            $p[]=[
                'checked_type'      => 'empty',
                'key_name'          => 'login_pwd',
                'error_msg'         => '登录密码不能为空',
                'error_code'         => 23007,
            ];

        }
        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }

        Db::startTrans();
        if(empty($params['id']))
        {
            //账号重复判断
            $id=Db::name('admin')->where(['username'=>$params['username']])->value('id');

            if ($id > 0)
            {
                throw new \BaseException(['code'=>403 ,'errorCode'=>23010,'msg'=>'登录账号重复添加','status'=>false,'debug'=>false]);
            }

            //添加账户信息
            $data_power=[
                'username'=>$params['username'],
                'login_pwd'=>LoginPwdEncryption($params['login_pwd'], $params['login_salt']),
                'login_salt'=>$params['login_salt'],
                'mobile'=>$params['mobile'],
                'add_time'=>TIMESTAMP,
                'employee_id'=>$params['employee_id'],
                'employee_station_id'=>$params['employee_station_id'],
                'employee_department_id'=>$params['employee_department_id'],
                'employee_department_secid'=>$params['employee_department_secid'],
            ];
            $id=Db::name('admin')->insertGetId($data_power);

            if (!$id)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>23008,'msg'=>'添加账号失败','status'=>false,'debug'=>false]);
            }

            $power_admin=self::AccountPowerHandle($params,$id);

            $id=Db::name('power_admin')->insertAll($power_admin);

            if (!$id)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>23009,'msg'=>'添加权限失败','status'=>false,'debug'=>false]);
            }

        } else {

            //修改账户信息
            $data_power=[
                'username'=>$params['username'],
                'mobile'=>$params['mobile'],
                'upd_time'=>TIMESTAMP,
                'employee_id'=>$params['employee_id'],
                'employee_station_id'=>$params['employee_station_id'],
                'employee_department_id'=>$params['employee_department_id'],
                'employee_department_secid'=>$params['employee_department_secid'],
            ];

            if (!empty($params['login_pwd']))
            {
              $data_power['login_pwd']=LoginPwdEncryption($params['login_pwd'], $params['login_salt']);

                $data_power['login_salt']=$params['login_salt'];
            }

            $id=Db::name('admin')->where('id='.$params['id'])->update($data_power);

            if (!$id)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>23008,'msg'=>'修改账号失败','status'=>false,'debug'=>false]);
            }


            //删除原有权限
            $id=Db::name('power_admin')->where(['admin_id'=>$params['id']])->delete();

//            if (!$id)
//            {
//                Db::rollback();
//                throw new \BaseException(['code'=>403 ,'errorCode'=>23011,'msg'=>'修改权限失败','status'=>false,'debug'=>false]);
//            }

            //添加新权限
            $power_admin=self::AccountPowerHandle($params,$params['id']);
           // dump($power_admin);exit;
            $id=Db::name('power_admin')->insertAll($power_admin);

            if (!$id)
            {
                Db::rollback();
                throw new \BaseException(['code'=>403 ,'errorCode'=>23009,'msg'=>'添加权限失败','status'=>false,'debug'=>false]);
            }
        }

        Db::commit();
        return DataReturn('ok',0);

    }

    /**
     * 组建权限列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2021年2月6日14:40:33
     * @desc    description
     * @param    [array]          $params [输入参数]
     * @param    [int]          $admin_id [管理员id]
     */
    public static function AccountPowerHandle($params,$admin_id)
    {
        $power_admin=[];
        //数据中心列表权限
        if ($params['power_status_datacenter']==1)
        {
            $power_admin[]=[
                'power_id'=>14,
                'admin_id'=>$admin_id
            ];
        }

        //用户列表权限
        if ($params['power_status_member']==1)
        {
            $power_admin[]=[
                'power_id'=>15,
                'admin_id'=>$admin_id
            ];
        }

        //其他权限
        if (isset($params['power_id']))
        {
            foreach ($params['power_id'] as &$v)
            {
                $power_admin[]=[
                    'power_id'=>$v,
                    'admin_id'=>$admin_id,
                ];
            }
        }

        return $power_admin;
    }
}