<?php


namespace app\service;


use OSS\Core\OssException;
use OSS\OssClient;
use think\Db;

/**
 * 图片存储管理层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年9月29日09:44:29
 */
class OssService
{

        // 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
        private  $accessKeyId = 'LTAI4G3tkh4nbuTCMCxoUbLi';
            //"LTAI4G3tkh4nbuTCMCxoUbLi";
        private  $accessKeySecret = "";
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        private  $endpoint = "http://oss-cn-beijing.aliyuncs.com";
        // 存储空间名称
        private  $bucket= "ecars";
        public  $successurl="http://ecars.oss-cn-beijing.aliyuncs.com/";
        // <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg

        /**
         * 构造方法
         * @author   juzi
         * @blog    https://blog.csdn.net/juziaixiao
         * @version 1.0.0
         * @date    2020年9月30日09:30:50
         * @desc    description
         */
        public function __construct()
        {
            $info=Db::name('sysset')->where('id=1')->find();
            $info['desc']=json_decode($info['desc'],true);
            $this->accessKeyId=$info['desc']['accessKeyId'];
            $this->accessKeySecret=$info['desc']['accessKeySecret'];
        }
        /**
         * oss简单上传文件
         * @author   juzi
         * @blog     https://blog.csdn.net/juziaixiao
         * @version  1.0.0
         * @datetime 2020年9月29日11:18:09
         */
        public  function OssUpload($src){
            try {
                $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);

//                dump($src);
//                exit;
                if (file_exists(GetDocumentRoot().DS.'upload'.DS.$src)) {
                    //单图或多图上传:src:20201019/1603092055946QQorSTlBWT.jpg
                    $ossClient->uploadFile($this->bucket, $src, GetDocumentRoot().DS.'upload'.DS.$src);
                }else{
                    //ueditor:src:/upload/ueditor/images/2020/10/19/1603092006808670.jpg
                    $ossClient->uploadFile($this->bucket, substr($src,1), GetDocumentRoot().DS.$src);
                }

            } catch (OssException $e) {
                $msg= $e->getMessage();

                throw new \BaseException(['code'=>403 ,'errorCode'=>50001,'msg'=>$msg,'status'=>false,'debug'=>false]);
            }
            return $this->successurl.$src;
        }

        /**
         * oss删除文件
         * @author   juzi
         * @blog     https://blog.csdn.net/juziaixiao
         * @version  1.0.0
         * @datetime 2020年9月29日11:18:09
         */
        public  function OssDel($src){

            try{
                $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
                $exist = $ossClient->doesObjectExist($this->bucket, $src);
                if ($exist)
                {
                    $ossClient->deleteObject($this->bucket, $src);
                }
            } catch(OssException $e) {
                $msg= $e->getMessage();
                throw new \BaseException(['code'=>403 ,'errorCode'=>50001,'msg'=>$msg,'status'=>false,'debug'=>false]);
            }
            return true;
        }
}