<?php


namespace app\service;


use think\Db;

class RacGameService
{
    public $GameId;
    function __construct()
    {
        $this->GameId =1 ;
    }

    /**
     * @return array|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 游戏奖品
     */
    function racPrize()
    {
        $prize = Db::table("rac_prize")->where(array("id"=>$this->GameId))->order("id desc")->find();
        $prize['prize_val']=getsPriceFormat($prize['prize_val']);
        return $prize;
    }

    /**
     * @param $page
     * @param $pageNum
     * @param string $where
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection|\think\Paginator
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 游戏记录
     */
    function racLog($page,$pageNum,$where="")
    {
        $list = Db::table("rac_log rl,member m")->field("rl.*,m.nickname,m.member_phone")
            ->where("m.id = rl.member_id and rl.game_id = {$this->GameId}")->where($where)->order("rl.id desc");
        if($page){
            # 带分页
            $list = $list->paginate($pageNum, false, ['query' => request()->param()]);
        }else{
            $list = $list->limit($pageNum)->select();
        }
        return $list;
    }

    /**
     * @return mixed
     * @context 参与活动统计
     */
    function racStatic()
    {
        $static[] = Db::table("rac_log")->where("rac_status =1 and game_id = {$this->GameId}")->count();
        $static[] = Db::table("rac_log")->where("rac_status =2 and game_id = {$this->GameId}")->count();
        $static[] = Db::table("rac_log")->where("rac_status =3 and game_id = {$this->GameId}")->count();
        $static[]= Db::table("rac_log")->where("rac_status =4 and game_id = {$this->GameId}")->count();
        return $static;
    }

    function racHelp($page,$member_id,$where="")
    {
        //$repeat_num = Db::table("rac_log")->field("member_repeat_num")->where(array("member_id"=>$member_id,"game_id"=>$this->GameId))->find()['member_repeat_num'];
        $list = Db::table("rac_loghelp")->where(array("member_id"=>$member_id,"game_id"=>$this->GameId))->where($where);//"help_repeat_num"=>$repeat_num,
        if($page){
            $list = $list->paginate(10, false, ['query' => request()->param()]);
        }else{
            $list = $list->select();
        }
        return $list;
    }

    /**
     * @param $page
     * @param $pageNum
     * @param $data
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection|\think\Paginator
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 门店信息
     */
    function racStore($page,$pageNum,$data=array())
    {
        $where='';
        $field="";
        $order = "id asc";
        if(!empty($data['lon']) and !empty($data['lat'])){
            $field = ",ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$data['lat']}*PI()/180-b.biz_lat*PI()/180)/2),2)+COS({$data['lat']}*PI()/180)*COS(b.biz_lat*PI()/180)*POW(SIN((({$data['lon']}*PI()/180)-(b.biz_lon*PI()/180))/2),2))),2) AS distance";
            $where = "b.province like '%".$data['province']."%' and b.city like '%".$data['city']."%'";
            $order = "distance asc";
        }
        $list = Db::table("rac_store rb,biz b")
            ->field("b.id,b.biz_title,b.biz_address,b.biz_phone,b.biz_lon,b.biz_lat,b.biz_img".$field)
            ->where("rb.biz_id = b.id and b.biz_status =1")
            ->where($where)
            ->order($order);
        if($page){
            # 带分页
            $list = $list->paginate($pageNum, false, ['query' => request()->param()]);
        }else{
            $list = $list->limit($pageNum)->select();
        }
        return $list;
    }

    /**
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取可关联的门店信息
     */
    function getsSotre($data)
    {
        $where = "";
        if(!empty($data)){
            if(!empty($data['search'])){
                $where = "b.biz_title like '%".$data['search']."%'";
            }
        }
        $list = Db::table("biz b")
            ->field("b.id,b.biz_title,b.biz_address,b.biz_phone,b.biz_lon,b.biz_lat,b.biz_img")
            ->where("b.biz_status =1 and (select count(rs.id) from rac_store rs where rs.biz_id = b.id)=0")
            ->where($where)
            ->select();
        return $list;
    }

    /**
     * @param $biz_id
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 添加关联门店
     */
    function relationGameStore($biz_id)
    {
        $is_repeat=Db::table("rac_store")->where(array("biz_id"=>$biz_id,"game_id"=>$this->GameId))->find();
        if(!empty($is_repeat)){
            return false;
        }
        Db::table("rac_store")->insert(array("biz_id"=>$biz_id,"game_id"=>$this->GameId));
        return true;
    }

    function deleteGameRelation($biz_id)
    {
        Db::table("rac_store")->where(array("biz_id"=>$biz_id,"game_id"=>$this->GameId))->delete();
        return true;
    }
}