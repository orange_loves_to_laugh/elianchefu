<?php


namespace app\service;

use Redis\Redis;
use think\Db;

/**
 * 招募服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月9日10:01:25
 */
class RecruitmentService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function RecruitmentListWhere($params = [])
    {
        $where = [];

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['title', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        return $where;
    }
    /**
     * 获取招募列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:01:25
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function RecruitmentList($params)
    {

        $data=BaseService::DataList($params);
        return $data;

    }




    /**
     * 招募保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function RecruitmentDataSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '标题不能为空',
                'error_code'         => 18001,
            ],


        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);

            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }

        $data=self::HandleData($params);


        // 添加/编辑
        if(empty($params['id']))
        {
            $id = Db::name('recruitment')->insertGetId($data);

        } else {

            if(Db::name('recruitment')->where(['id'=>intval($params['id'])])->update($data))
            {
                $id = $params['id'];
            }

        }
        if ($id>0) {
            return DataReturn('保存成功', 0);
        }

        throw new \BaseException(['code'=>403 ,'errorCode'=>18002,'msg'=>'保存招募信息失败','status'=>false,'debug'=>false]);
    }

    /**
     * 保存前数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params){

        if (isset($params["content"]))
        {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table']='recruitment';
            $params['ueditor_content']= $params['content'];
            $re=ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }

       return $params;
    }
}