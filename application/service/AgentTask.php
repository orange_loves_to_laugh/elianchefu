<?php


namespace app\service;


use Redis\Redis;
use think\Db;

class AgentTask
{
    /**
     * @param $pid
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队今日任务完成 17
     */
    static function teamDayTaskFinish($pid){
        $middle_id = $pid;
        $num = 1;
        for($i=1;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    # 查询今日任务是否是团队任务今日完成数量
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>17,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+$num>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+$num,"task_status"=>2);
                            $num+=1;
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+$num);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联办理会员任务 1
     */
    static function applyMemberTask($agent_id){
        # 推荐会员点亮任务
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                # 查询点亮任务关联代理
                $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>2,"task_type"=>1,"task_status"=>1))->find();
                if(!empty($taskInfo)){
                    if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                        $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                    }else{
                        $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    # 查询该代理所有点亮任务是否已经完成
                    $is_finish = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>2,"task_status"=>1))->find();
                    if(empty($is_finish)){
                        Db::connect("ealspell")->table("open_task")->where(array("agent_id"=>$agent_id,"status"=>1,"task_type"=>1))->update(array("status"=>2));
                    }
                }
                # 推荐会员每日任务
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>1,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }

                if(!empty($agentInfo['pid'])){
                    self::teamApplyMemberTask($agentInfo['pid']);
                }

            }
        }
        return true;
    }

    /**
     * @param $pid
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队新增会员 10
     */
    static function teamApplyMemberTask($pid){
        $middle_id = $pid;
        for($i=1;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    # 查询推荐团队优质商家数量
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>10,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队新增用户 9
     */
    static function teamNewMermber($agent_id){
        $middle_id = $agent_id;
        for($i=0;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    # 查询推荐团队优质商家数量
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>9,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成新增平台优质商家 2
     */
    static function highGradeMerchantsTask($agent_id){
        # 推荐优质商家点亮任务
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                # 查询点亮任务推荐优质商家
                $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>2,"task_type"=>2,"task_status"=>1))->find();
                if(!empty($taskInfo)){
                    if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                        $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                    }else{
                        $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    # 查询该代理所有点亮任务是否已经完成
                    $is_finish = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>2,"task_status"=>1))->find();
                    if(empty($is_finish)){
                        Db::connect("ealspell")->table("open_task")->where(array("agent_id"=>$agent_id,"status"=>1,"task_type"=>1))->update(array("status"=>2));
                    }
                }
                # 推荐优质商家每日任务
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>1,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }
                # 推荐团队优质商家数量 11
                if(!empty($agentInfo['pid'])){
                    self::teamHighGradeMerchantsTask($agentInfo['pid']);
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @context 推荐团队优质商家数量 11
     */
    static function teamHighGradeMerchantsTask($pid){
        $middle_id = $pid;
        for($i=1;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    # 查询推荐团队优质商家数量
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>11,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成平台优惠套餐推荐 4
     */
    static function merchantsProTask($agent_id,$price){
        # 每日平台优惠套餐推荐
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                # 每日平台优惠套餐推荐
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>4,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }
                if(!empty($agentInfo['pid'])){
                    self::teamMerchantsProTask($agentInfo['pid']);
                }
            }
            # 升级任务
            $upgradeInfo = Db::table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>3,"task_type"=>20,"task_status"=>1))->order("id desc")->find();
            if(!empty($upgradeInfo)){
                if($upgradeInfo['task_plan']+$price>=$upgradeInfo['task_amount']){
                    $_array = array("task_plan"=>$upgradeInfo['task_plan']+$price,"task_status"=>2);
                }else{
                    $_array = array("task_plan"=>$upgradeInfo['task_plan']+$price);
                }
                Db::table("open_tasklog")->where(array("id"=>$upgradeInfo['id']))->update($_array);
            }
        }
        return true;
    }

    /**
     * @param $pid
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队平台套餐销售 14
     */
    static function teamMerchantsProTask($pid){
        #关联团队平台套餐销售 14
        $middle_id = $pid;
        for($i=1;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    # 查询推荐团队优质商家数量
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>14,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成平台用户下载推荐 5
     */
    static function downLoadTask($agent_id){
        #完成平台用户下载推荐
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                # 每日平台优惠套餐推荐
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>5,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成商家入驻 6
     */
    static function merchantsAddTask($agent_id){
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                # 每日平台优惠套餐推荐
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>6,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联用户消费（人数） 7
     */
    static function memberConsumpNumTask($agent_id,$price){
        $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
        if(!empty($agentInfo)){
            if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>7,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+1>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+1);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }


                # 关联消费金额
                $dayTaskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>1,"task_type"=>8,"task_status"=>1))
                    ->where("date(create_time)='".date("Y-m-d")."'")->find();
                if(!empty($taskInfo)){
                    if($dayTaskInfo['task_plan']+$price>=$dayTaskInfo['task_amount']){
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+$price,"task_status"=>2);
                        # 上级完成关联团队今日任务完成 17
                        if(!empty($agentInfo['pid'])){
                            self::teamDayTaskFinish($agentInfo['pid']);
                        }
                    }else{
                        $_array = array("task_plan"=>$dayTaskInfo['task_plan']+$price);
                    }
                    Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$dayTaskInfo['id']))->update($_array);
                }
                # 关联团队消费次数 12
                if(!empty($agentInfo['pid'])){
                    self::teamMemberConsumpNumTask($agentInfo['pid']);
                }
            }
        }
        return true;
    }

    /**
     * @param $pid
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队用户消费（次数) 12
     */
    static function teamMemberConsumpNumTask($pid){
        $middle_id = $pid;
        for($i=1;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>12,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @param $agent_id
     * @param $price
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 关联团队商家收款（金额）13  关联团队商家收款（次数）16
     */
    static function merchantsPayeeTask($agent_id,$price){
        #关联团队商家收款（金额）13
        $middle_id = $agent_id;
        for($i=0;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']>0 and $agentInfo['mark']<4){
                    #关联团队商家收款（次数）16
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>16,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+1>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+1);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                    #关联团队商家收款(金额) 13
                    $taskInfo = Db::connect("ealspell")->table("open_tasklog")->where(array("agent_id"=>$middle_id,"task_mark"=>1,"task_type"=>13,"task_status"=>1))
                        ->where("date(create_time)='".date("Y-m-d")."'")->find();
                    if(!empty($taskInfo)){
                        if($taskInfo['task_plan']+$price>=$taskInfo['task_amount']){
                            $_array = array("task_plan"=>$taskInfo['task_plan']+$price,"task_status"=>2);
                            if(!empty($agentInfo['pid'])){
                                self::teamDayTaskFinish($agentInfo['pid']);
                            }
                        }else{
                            $_array = array("task_plan"=>$taskInfo['task_plan']+$price);
                        }
                        Db::connect("ealspell")->table("open_tasklog")->where(array("id"=>$taskInfo['id']))->update($_array);
                    }
                }
                if($agentInfo['pid']>0){
                    $middle_id = $agentInfo['pid'];
                }else{
                    break;
                }
            }
        }
        return true;
    }

    /**
     * @return bool
     * @throws \think\Exception
     * @context 完成任务提成奖励
     */
    static function dayTaskCommission(){
        # 每日0点结算
        $date = date("Y-m-d",strtotime('-1 day'));
        $today_date = date("Y-m-d H:i:s");
        $_redis = new Redis();
        $_date_mark = $_redis->hGet("agentTask",$date);
        if(empty($_date_mark)){
            $is_repeat = $_redis->lock("agentTaskLock",60);
            if($is_repeat){
                # 查询是否已经结算过
                $is_already = Db::connect('ealspell')->table('deduct_record')->where(array("type"=>17))
                    ->where("date(create_time)='".$date."'")->find();
                if(!empty($is_already)){
                    return false;
                }
                # 查询今日完成任务的代理及开启任务的时间
                $community_finish = Db::connect("ealspell")->table("open_tasklog ot")
                    ->field("ca.mark,ot.agent_id,ca.pid,otk.create_time")
                    ->join(array("community_agent"=>"ca"),"ca.id = ot.agent_id")
                    ->join(array("open_task"=>'otk'),"otk.agent_id = ot.agent_id and otk.status=2 and otk.task_type=1")
                    ->where(array("ot.task_mark"=>1,"ot.task_status"=>2))
                    ->where("date(ot.create_time)='".date("Y-m-d")."'")->select();
                $opera_array  = array(); // 运营中心
                $community_array =array();// 社区代理
                $area_array = array(); // 区域代理
                $city_array = array();// 城市代理
                if(!empty($community_finish)){
                    # 关联代理会员消费的1%及商家抽成金额20%总和
                    $vip_comsump = Db::table("log_consump")->where(array("log_consump_type"=>1))
                        ->where("date(consump_time)='".$date."'")->sum("consump_price");
                    $merchants_commsision = Db::table("merchants_order")
                            ->where("date(create_time)='".$date."'")->sum("commission_price");
                    $commission_total = round($vip_comsump*0.01,2)+round($merchants_commsision*0.2,2);
                    if(!empty($commission_total)){
                        foreach($community_finish as $k=>$v){
                            if($v['mark']==1){
                                array_push($community_array,$v);
                            }elseif($v['mark']==2){
                                array_push($area_array,$v);
                            }else{
                                array_push($city_array,$v);
                            }
                        }
                        if(!empty($community_array)){
                            # 社区代理每人的提成
                            $community_price = ($commission_total*0.2)/count($community_array);
                            $community_log_array = array();
                            foreach($community_array as $ck=>$cv){
                                # 添加分红奖励
                                array_push($community_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$community_price,"type"=>17,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$community_price);
                                # 添加每日奖励 反3个月 每天13.8
                                if(date("Y-m-d",strtotime($cv['create_time']))>date("Y-m-d",strtotime('-3 month'))){
                                    $day_price = 13.8;
                                    array_push($community_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$day_price,"type"=>18,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                    Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$day_price);
                                    # 上级返现10%;
                                    if(!empty($cv['pid'])){
                                        $p_price = $day_price*0.1;
                                        array_push($community_log_array,array("ragent_id"=>$cv['pid'],"price"=>$p_price,"type"=>19,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                        Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['pid']))->setInc("balance",$p_price);
                                    }
                                }
                            }
                            Db::connect("ealspell")->table('deduct_record')->insertAll($community_log_array);
                        }
                        if(!empty($area_array)){
                            # 区域代理每人的提成
                            $community_price = ($commission_total*0.3)/count($area_array);
                            $area_log_array = array();
                            foreach($area_array as $ck=>$cv){
                                # 添加分红奖励
                                array_push($area_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$community_price,"type"=>17,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$community_price);
                                # 添加每日奖励 反3个月 每天13.8
                                if(date("Y-m-d",strtotime($cv['create_time']))>date("Y-m-d",strtotime('-6 month'))){
                                    $day_price = 68;
                                    array_push($area_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$day_price,"type"=>18,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                    Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$day_price);
                                    # 上级返现10%;
                                    if(!empty($cv['pid'])){
                                        $p_price = $day_price*0.1;
                                        array_push($area_log_array,array("ragent_id"=>$cv['pid'],"price"=>$p_price,"type"=>19,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                        Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['pid']))->setInc("balance",$p_price);
                                    }
                                }
                            }
                            Db::connect("ealspell")->table('deduct_record')->insertAll($area_log_array);
                        }
                        if(!empty($city_array)){
                            # 城市代理每人的提成
                            $community_price = ($commission_total*0.4)/count($city_array);
                            $city_log_array = array();
                            foreach($city_array as $ck=>$cv){
                                # 添加分红奖励
                                array_push($city_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$community_price,"type"=>17,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$community_price);
                                # 添加每日奖励 反3个月 每天13.8
                                if(date("Y-m-d",strtotime($cv['create_time']))>date("Y-m-d",strtotime('-9 month'))){
                                    $day_price = 138;
                                    array_push($city_log_array,array("ragent_id"=>$cv['agent_id'],"price"=>$day_price,"type"=>18,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                    Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['agent_id']))->setInc("balance",$day_price);
                                    # 上级返现10%;
                                    if(!empty($cv['pid'])){
                                        $p_price = $day_price*0.1;
                                        array_push($city_log_array,array("ragent_id"=>$cv['pid'],"price"=>$p_price,"type"=>19,"create_time"=>$today_date,"status"=>2,"source"=>9));
                                        Db::connect("ealspell")->table("community_agent")->where(array("id"=>$cv['pid']))->setInc("balance",$p_price);
                                    }
                                }
                            }
                            Db::connect("ealspell")->table('deduct_record')->insertAll($city_log_array);
                        }
                    }
                }
            }
            $_redis->hSet("agentTask",$date,1,86400);
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return int
     * @throws \think\Exception
     * @context
     */
    static function  checkOperaCenter($agent_id){
        $middle_id = $agent_id;
        $return_id = 0;
        for($i=0;$i<3;$i++){
            $agentInfo = Db::connect("ealspell")->table("community_agent")->field("mark,pid")
                ->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($agentInfo)){
                if($agentInfo['mark']==4){
                    $return_id = $middle_id;
                    break;
                }
                if(!empty($agentInfo['pid'])){
                    $middle_id = $agentInfo['pid'];
                }
            }else{
                break;
            }
        }
        return $return_id;
    }

    /**
     * @return bool
     * @throws \think\Exception
     * @context 代理未完成任务 取消身份
     */
    static function agentTaskFail(){
        $date = date("Y-m-d");
        $_redis = new Redis();
        $_date_mark = $_redis->hGet("agentTaskFail",$date);
        if(empty($_date_mark)){
            $is_repeat = $_redis->lock("agentTaskFailLock",60);
            if($is_repeat){
                #  查询是否有3个月以上未完成任务的代理
                $list = Db::connect('ealspell')->table("open_task ot")
                    ->field("ot.*,lc.pay_price")
                    ->join(array("log_community"=>"lc"),"lc.community_id = ot.agent_id")
                    ->where(array("ot.task_type"=>1,"ot.status"=>1))
                    ->where("date(ot.create_time)<'".date("Y-m-d",strtotime("-3 month"))."'")
                    ->select();
                if(!empty($list)){
                    foreach($list as $k=>$v){
                        # 返还金额的40%；
                        if(!empty($v['pay_price'])){
                            $price = $v['pay_price']*0.4;
                            Db::connect("ealspell")->table("community_agent")->where(array("id"=>$v['agent_id']))->setInc("balance",$price);
                        }
                        # 取消代理
                        Db::connect("ealspell")->table("community_agent")->where(array("id"=>$v['agent_id']))->update(array("status"=>3));
                    }
                }
            }
            $_redis->hSet("agentTaskFail",$date,1);
        }
        return true;
    }

    /**
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 升级任务门店入驻
     */
    static function bizRecommendTask($agent_id){
        # 升级任务
        $upgradeInfo = Db::table("open_tasklog")->where(array("agent_id"=>$agent_id,"task_mark"=>3,"task_type"=>19,"task_status"=>1))->order("id desc")->find();
        if(!empty($upgradeInfo)){
            if($upgradeInfo['task_plan']+1>=$upgradeInfo['task_amount']){
                $_array = array("task_plan"=>$upgradeInfo['task_plan']+1,"task_status"=>2);
            }else{
                $_array = array("task_plan"=>$upgradeInfo['task_plan']+1);
            }
            Db::table("open_tasklog")->where(array("id"=>$upgradeInfo['id']))->update($_array);
        }
        return true;
    }



}