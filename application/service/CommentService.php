<?php


namespace app\service;

use think\Db;

/**
 * 评论服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月28日13:26:43
 */
class CommentService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function CommentListWhere($params = [])
    {

        $where = [];

        //id_arr
        if(!empty($params['id_arr']))
        {

            $where = json_decode($params['id_arr'],true);

        }
        //时间
        if(!empty($params['param']['year'])&&!empty($params['param']['month']))
        {
            $re=StatisticalService::HandleTime($params['param']);


            $where[] = ['evaluation_score.create_time', '>', $re['data']['time_start']];
            $where[] = ['evaluation_score.create_time', '<', $re['data']['time_end']];
        }
        //dump($where);exit;
        //员工id
        if(!empty($params['employ_id'])&&intval($params['employ_id'])>0)
        {

            $where[] = ['evaluation_score.employee_id', '=', $params['employ_id']];
        }
        //门店id

        if(!empty($params['param']['biz_id'])&&intval($params['param']['biz_id'])>0)
        {

            $where[] = ['evaluation_score.biz_id', '=', $params['param']['biz_id']];
        }

        if(!empty($params['biz_id'])&&intval($params['biz_id'])>0)
        {

            $where[] = ['evaluation_score.biz_id', '=', $params['biz_id']];
        }

        //member_id
        if(!empty($params['member_id'])&&intval($params['member_id'])>0)
        {

            $where[] = ['member_id', '=', $params['member_id']];
        }
        //默认条件
      /*  if (empty($where))
        {
            $time_start=date('Y-m-01', strtotime(date('Y-m-d', time()) . '-1 month'));
            $time_end=date('Y-m-d', strtotime($time_start. '+1 month -1 day'));

            $where[] = ['b.biz_create', '>',$time_start];
            $where[] = ['b.biz_create', '<', $time_end];
        }*/
        //关键词
        if(!empty($params['param']['keywords']))
        {
            $where[] =['orders.car_liences', 'like', '%'.$params["param"]['keywords'].'%'];
        }

        return $where;
    }

    /**
     * 组建sql语句
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function SqlParser($params = []){

        $where=self::CommentListWhere($params);

        $cate=$params['cate'];
        if ($cate==1)//门店评论
        {
            //车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值 门店名称
            $group='order_id';
            $field='order_id,create_time,score,orderserver_id,evaluation_score.biz_id';
        }



        if ($cate==3)
        {
            //车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值 门店名称
            $group='';
            $field='order_id,create_time,score,orderserver_id,evaluation_score.employee_id employ_id,evaluation_score.biz_id';
        }

        if ($cate==2)//员工评论
        {
            $group='evaluation_score.employee_id';
            //员工姓名 岗位 总得分 平均得分 好评量 中评量 差评量 评论总数 门店名称
            $field='sum(score) total_score,count(score) comment_total,evaluation_score.employee_id,evaluation_score.biz_id';
        }



        $data_params = array(
            'page'         => true,
            'number'         => 10,
            'where'     => $where,
            'group'     =>$group,
            'field'    =>$field
        );
        return $data_params;
    }
    /**
     * 获取评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function CommentList_old($params)
    {

        $where = empty($params['where']) ? [['b.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'b.id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;


        // 获取列表
       /* $data = Db::name('evaluation_score')
            ->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')
            ->where($where)->field($field)->order($order)->group($group);*/
        $data = Db::name('biz b')
            ->leftJoin(['evaluation_score'=>'evaluation_score'],'b.id=evaluation_score.biz_id')
            ->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')
            ->where($where)
            ->field($field)->order($order)->group($group);

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }

        foreach ($params['where'] as $k=>$v)
        {
            if ($v[0]=='orders.car_liences')
            {
                unset($params['where'][$k]);
                break;
            }
        }

        return self::DataDealWith($data,$params);


    }

    /**
     * 获取评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年01月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function CommentList($params){
        $where = empty($params['where']) ? [['b.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'b.id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;


        // 获取列表

        $data = Db::name('evaluation_score')->alias('es')
            ->leftJoin(['biz'=>'b'],'b.id=evaluation_score.biz_id')
            ->where($where)
            ->field($field)->order($order)->group($group);
        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }



        foreach ($data['data'] as &$v) {


            $v['id_arr']=urlencode(json_encode($where));

            //门店类型
            $v['biz_type_title']=BaseService::StatusHtml($v['biz_type'],lang('biz_type'),false);

            //平均得分
            $v['score_even']=round($v['total_score']/$v['comment_total']);

            //好评数量-评论率
            $where_top=$where;

            $where_top[]=['biz_id','=',$v['biz_id'],];

            $where_top[]=['score','=',5];

            $v['top_comment_num']= self::RateCount($where_top,$v['comment_total']);

            //中评数量-评论率
            $where_mid=$where;

            $where_mid[]=['biz_id','=',$v['biz_id'],];

            $where_mid[]=['score','in',[3,4]];

            $v['mid_comment_num']=self::RateCount($where_mid,$v['comment_total']);

            //差评数量-评论率

            $where_bottom=$where;

            $where_bottom[]=['biz_id','=',$v['biz_id'],];

            $where_bottom[]=['score','<',3];

            $v['bottom_comment_num']=self::RateCount($where_bottom,$v['comment_total']);

        }
        return $data;


    }



    /**
     * 门店员工评论
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function CommentStaffList($params)
    {

        $where = empty($params['where']) ? [['evaluation_score.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'create_time desc';
        $group = isset($params['group']) ? $params['group'] : 'evaluation_score.id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        // 获取列表
        // 门店名称 车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值
        $data = Db::name('evaluation_score')->alias('evaluation_score')
            ->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')
            ->where($where)->field($field)->order($order)->group($group);

            //->order($order)->group($group);

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray();
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();
            }
        }


        return self::CommentStaffDataDealWith($data);


    }

    /**
     * 门店员工评论数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $data [待处理参数]
     */
    public static function CommentStaffDataDealWith($data){

        if (!empty($data['data'])) {
            foreach ($data['data'] as &$v) {
                //门店名称
                if (isset($v['biz_id']))
                {
                    $v['biz_title']=Db::name('biz')->where('id='.$v['biz_id'])->value('biz_title');

                }

                //员工信息
                if (isset($v['employee_id']))
                {
                $v['create_url']=date('Y-m',strtotime($v['create_time']));

                $employee=Db::name('employee')->where('id='.$v['employee_id'])->find();

                $v['employee_name']=empty($employee['employee_name'])?'未获取到员工信息':$employee['employee_name'];

                //平均得分
                $v['score_even']=round($v['total_score']/$v['comment_total']);

                //好评数量-评论率
                $param=[
                    'month'=>date('m',strtotime($v['create_time'])),
                    'year'=>date('Y',strtotime($v['create_time'])),
                ];
                $re=StatisticalService::HandleTime($param);
                $where=[
                    ['employee_id','=',$v['employee_id'],],
                    ['biz_id','=',$v['biz_id'],],
                    ['score','=',5],
                    ['create_time', '>', $re['data']['time_start']],
                    ['create_time', '<', $re['data']['time_end']]
                ];

                $v['top_comment_num']=self::RateCount($where,$v['comment_total']);

                //中评数量-评论率.
                $where=[
                    ['employee_id','=',$v['employee_id'],],
                    ['biz_id','=',$v['biz_id'],],
                    ['score','in',[3,4]],
                    ['create_time', '>', $re['data']['time_start']],
                    ['create_time', '<', $re['data']['time_end']]
                ];
                $v['mid_comment_num']=self::RateCount($where,$v['comment_total']);

                //差评数量-评论率
                $where=[
                    ['employee_id','=',$v['employee_id'],],
                    ['biz_id','=',$v['biz_id'],],
                    ['score','<',3],
                    ['create_time', '>', $re['data']['time_start']],
                    ['create_time', '<', $re['data']['time_end']]
                ];
                $v['bottom_comment_num']=self::RateCount($where,$v['comment_total']);

                }


                //订单信息: 门店名称 车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值
                if (isset($v['order_id']))
                {
                    $orders=Db::name('orders')->where('id='.$v['order_id'])->find();

                    $v['create_url']=date('Y-m',strtotime($v['create_time']));
                    //车牌号
                    $v['car_liences']=empty($orders['car_liences'])?'未获取车辆信息':$orders['car_liences'];

                    //用户id
                    $v['member_id']=empty($orders['member_id'])?'':$orders['member_id'];

                    //车辆信息
                    $car_info=Db::table('member_car mc')
                        ->field('cl.title logoTitle,cs.sort_title sortTitle,clv.level_title levelTitle')
                        ->join('car_sort cs', 'mc.car_type_id=cs.id', 'left')
                        ->join('car_logo cl', 'cs.logo_id=cl.id', 'left')
                        ->join('car_level clv', 'clv.id=cs.level', 'left')
                        ->where([['mc.car_licens','=',$v['car_liences'],],])->find();
                    //车辆品牌
                    $v['car_model']=empty($car_info['logoTitle'])?'未获取车辆信息':$car_info['logoTitle'];

                    //车系
                    $v['car_level']=empty($car_info['sortTitle'])?'未获取车辆信息':$car_info['sortTitle'];

                    //车型
                    $v['levelTitle']=empty($car_info['levelTitle'])?'未获取车辆信息':$car_info['levelTitle'];

                    $v['score_title']=BaseService::StatusHtml($v['score'],lang('comment_level'),false);



                    //服务名称
                    $v['service_title']=Db::name('order_server')->alias('os')
                        ->leftJoin(['service'=>'s'],'os.server_id=s.id')
                        ->where('os.id='.$v['orderserver_id'])->value('s.service_title');

                    $v['service_title']=empty($v['service_title'])?'未获取服务信息':$v['service_title'];
                }
            }
        }

        return $data;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $data [处理的数据]
     * @param    [array]          $params [输入参数]
     */
    public static function DataDealWith($data,$params=[]){

        if(!empty($data))
        {
            foreach($data as &$v)
            {
                if($v['total_score'] == ''){
                    $v['total_score'] = 0;

                }
                if (isset($v['score']))
                {

                    $v['score_title']=BaseService::StatusHtml($v['score'],lang('comment_level'),false);
                }

                if (isset($v['biz_id']))
                {
                    if($v['comment_total'] == 0){
                        $v['comment_total'] = 1;
                    }
                    //门店名称 类型
                    $biz_info=Db::name('biz')->where('id='.$v['biz_id'])->field('biz_title,biz_type')->find();
                    $v['biz_title']=$biz_info['biz_title'];
                    $v['biz_type_title']=BaseService::StatusHtml( $v['biz_type'],lang('biz_type'),false);

                    //总数据表：总得分 评论总数 平均得分
                    $v['score_even']  = isset($v['total_score'])||isset($v['comment_total'])?intval($v['total_score']/$v['comment_total']):0;
                    //好评 中评 差评数据
                    $where=isset($params['where'])?$params['where']:[];

                    $where[]=['biz_id','=',$v['biz_id']];

                    $re=self::GetCommentData($where,$v['comment_total']);

                    $v=array_merge($v,$re);

                }

                //车牌号 车辆品牌型号 车型 评论时间 服务名称 评论分值
                if (isset($v['order_id'])&&intval($v['order_id'])>0)
                {
                $order=Db::name('orders')->alias('o')
                    ->leftJoin(['car_logo'=>'cl'],'o.car_logo=cl.id')
                    ->leftJoin(['car_sort'=>'cs'],'o.car_sort=cs.id')
                    ->leftJoin(['car_level'=>'cle'],'o.car_level=cle.id')
                    ->field('cl.title car_logo,cs.sort_title car_sort,o.car_liences,cle.level_title car_level,o.member_id')
                    ->where('o.id='.$v['order_id'])
                    ->find();
               // $order=Db::name('orders')->where('id='.$v['order_id'])->field('car_liences,car_logo,car_sort')->find();
                $v['member_id']=empty($order['member_id'])?'':$order['member_id'];

                $v['car_liences']=empty($order['car_liences'])?'未获取车辆信息':$order['car_liences'];

                $v['car_level']=empty($order['car_level'])?'未获取车辆信息':$order['car_level'];

                $v['car_model']=empty($order['car_level'])?'未获取车辆信息':$order['car_logo'].'-'.$order['car_sort'];

                $v['service_title']=Db::name('order_server')->alias('os')->leftJoin(['service'=>'s'],'os.server_id=s.id')->where('os.id='.$v['orderserver_id'])->value('s.service_title');

                $v['service_title']=empty($v['service_title'])?'未获取服务信息':$v['service_title'];
                }

                //员工评论数据表：员工姓名 岗位 总得分 平均得分 好评量 中评量 差评量 评论总数
                if (isset($v['employee_id']))
                {
                    $employee=Db::name('employee')->alias('e')
                     ->leftJoin(['jobs'=>'j'],'e.employee_station_id=j.id')->where('e.id='.$v['employee_id'])
                     ->field('employee_name,jobs_title')->find();
                    $v['employee_name']=isset($employee['employee_name'])?$employee['employee_name']:'获取数据失败';
                    $v['employee_jobs_title']=isset($employee['jobs_title'])?$employee['jobs_title']:'获取数据失败';
                    $v['score_even']  =intval($v['total_score']/$v['comment_total']);
                    //获取 好评 中评 差评数据
                    $re=self::GetCommentData([['employee_id','=',$v['employee_id']]],$v['comment_total']);
                    $v=array_merge($v,$re);
                }



            }

        }
//        else{
//            return DataReturn('获取失败',0,$data);
//        }
//
        return $data;
    }


    /**
     * 数据百分比处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [string]          $top [分子]
     * @param    [string]          $bottom [分母]
     */
    public static function DataFormate($top,$bottom){

        $re=$bottom>0?number_format($top/$bottom,4)*100:0;
        return $re.'%';
    }

    /**
     * 计算评论率和数量
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [查询条件]
     * @param    [int]          $comment_total [评论总量]
     */
    public static function RateCount($where,$comment_total){

        $count=Db::name('evaluation_score')->where($where)->count();
        $ratecount=[
            'count'=> $count,
            'rate'=>self::DataFormate($count,$comment_total),
        ];

        return $ratecount;
    }

    /**
     * 计算总得分和好评总次数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:3
     * @desc    description
     * @param    [array]          $where [查询条件]
     */
    public static function TotalScore($where){
        //好评总次数
        $where_count=$where;
        $where_count[]=['score','>',3];
        $count=Db::name('evaluation_score')->where($where)->count();
        //总得分
        $where_score=$where;
        $where_score[]=['id','>',0];
        $score=Db::name('evaluation_score')->where($where_score)->sum('score');

        return ['count'=>$count,'score'=>$score];
    }

    /**
     * 好评 中评 差评数据获取
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where_child [子查询条件]
     * @param    [array]          $where_child [子查询条件]
     */
    public static function GetCommentData($where,$comment_total){

        $v=[];
        //好评量和率
        $where_all=$where;
        $where_all[]=['evalua_level','>',3];
        $v['favorable'] = self::RateCount($where_all,$comment_total);
        //中评评量和率
        $where_all=$where;
        $where_all[]=['evalua_level','=',3];

        $v['secondary'] = self::RateCount($where_all,$comment_total);
        //差评评评量和率
        $where_all=$where;
        $where_all[]= ['evalua_level','<',3];
        $v['negative'] = self::RateCount($where_all,$comment_total);
        return $v;
    }
    /**
     * 数据百分比处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [string]          $top [分子]
     * @param    [string]          $bottom [分母]
     */
    public static function DataTotal($params){
        $where = empty($params['where']) ? [['b.id','>',0]] : $params['where'];
        $group = isset($params['group']) ? $params['group'] : 'b.id';
       /* return (int)    Db::name('evaluation_score')->alias('es')->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')->where($where)->group($group)->count();*/
        return (int)     Db::name('biz b')
            ->leftJoin(['evaluation_score'=>'evaluation_score'],'b.id=evaluation_score.biz_id')
            ->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')
            ->where($where)
            ->group($group)->count();
//        return (int) Db::name('evaluation_score')->where($where)->group($group)->count();
    }
    /*
     * 员工 总数
     * */
    public static function DataStaffTotal($params){
        $where = empty($params['where']) ? [['es.id','>',0]] : $params['where'];
        $group = isset($params['group']) ? $params['group'] : 'es.id';
        return (int)    Db::name('evaluation_score')->alias('es')->leftJoin(['orders'=>'orders'],'orders.id=evaluation_score.order_id')->where($where)->group($group)->count();

//        return (int) Db::name('evaluation_score')->where($where)->group($group)->count();
    }
    /**
     * 获取评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function UserCommentList($params){
        //车牌号 日期 服务名称 评论等级 分数
        $where[]=['o.member_id','=',$params['member_id']];

        if (intval($params['create_time'])>0)
        {
            $params['month']=date('m',strtotime($params['create_time']));

            $params['year']=date('Y',strtotime($params['create_time']));

            $re=StatisticalService::HandleTime($params);

            $where[] = ['es.create_time', '>', $re['data']['time_start']];

            $where[] = ['es.create_time', '<', $re['data']['time_end']];
        }

        //dump($where);exit;
        $data=Db::name('evaluation_score')->alias('es')
            ->leftJoin(['orders'=>'o'],'o.id=es.order_id')
            ->leftJoin(['order_server'=>'os'],'os.id=es.orderserver_id')
            ->leftJoin(['service'=>'s'],'s.id=os.server_id')
            ->where($where)
            ->field('es.orderserver_id os_id,es.evalua_level,es.score,es.create_time,o.car_liences,o.member_id,s.service_title,es.biz_id')
            ->paginate(10, false, ['query' => request()->param()]);

        $data=$data->toArray();
        //es.evalua_level,es.score,es.create_time,o.car_liences,s.service_title,o.member_id
        $data['data']= self::DataDealWith($data['data']);

        foreach ($data['data'] as &$v)
        {
            if (empty($v['service_title']))
            {
                $v['service_title']=Db::name('order_server')->alias('os')
                    ->leftJoin(['service'=>'s'],'os.server_id=s.id')
                    ->where('os.id='.$v['os_id'])->value('s.service_title');

                $v['service_title']=empty($v['service_title'])?'未获取服务信息':$v['service_title'];
            }
        }
        return $data;
    }
}