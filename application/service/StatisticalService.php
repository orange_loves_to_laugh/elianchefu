<?php


namespace app\service;


use app\api\ApiService\LevelService;
use app\api\ApiService\VoucherService;
use think\Db;
use think\db\exception\DataNotFoundException;

/**
 * 数据统计服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年10月22日11:51:06
 */
class StatisticalService
{
    // 近3天,近7天,近15天,近30天
    private static $nearly_three_days;
    private static $nearly_seven_days;
    private static $nearly_fifteen_days;
    private static $nearly_thirty_days;

    // 近30天日期
    private static $thirty_time_start;
    private static $thirty_time_end;

    // 近15天日期
    private static $fifteen_time_start;
    private static $fifteen_time_end;

    // 近7天日期
    private static $seven_time_start;
    private static $seven_time_end;

    // 昨天日期
    private static $yesterday_time_start;
    private static $yesterday_time_end;

    // 今天日期
    private static $today_time_start;
    private static $today_time_end;

    // 本月日期
    private static $month_time_start;
    private static $month_time_end;

    //当年日期
    private static $year_time_start;
    private static $year_time_end;
    public $aaa;


    /**
     * 初始化
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月22日11:51:06
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function Init($params = [])
    {
        static $object = null;
        if(!is_object($object))
        {
            // 初始化标记对象，避免重复初始化
            $object = (object) [];

            // 近30天日期
            self::$thirty_time_start = strtotime(date('Y-m-d 00:00:00', strtotime('-30 day')));
            self::$thirty_time_end = TIMESTAMP;

            // 近15天日期
            self::$fifteen_time_start = strtotime(date('Y-m-d 00:00:00', strtotime('-15 day')));
            self::$fifteen_time_end = TIMESTAMP;

            // 近7天日期
            self::$seven_time_start = strtotime(date('Y-m-d 00:00:00', strtotime('-7 day')));
            self::$seven_time_end = TIMESTAMP;

            // 昨天日期
            self::$yesterday_time_start = date('Y-m-d 00:00:00', strtotime('-1 day'));
            self::$yesterday_time_end = date('Y-m-d 23:59:59', strtotime('-1 day'));

            // 今天日期
            self::$today_time_start =date('Y-m-d 00:00:00');
            self::$today_time_end = date('Y-m-d 23:59:59',time());
                //TIMESTAMP;
            //本月
            self::$month_time_start = date('Y-m-01 00:00:00', time());
            self::$month_time_end =date('Y-m-d 00:00:00', strtotime(self::$month_time_start.'+1month -1day'));
//            dump(self::$month_time_end );exit;
                //TIMESTAMP;
            //当年
            self::$year_time_start =date('Y-01-01 00:00:00', time());
            self::$year_time_end =date('Y-12-31 23:59:59', time());


        }
    }


    /**
     * 月份时间计算
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月7日11:57:29
     * @param    [array]          $params [输入参数]
     */
    public static function HandleTime($params){

        if(isset($params['day'])&&intval($params['day'])>0)
        {

            if (empty($params))
            {
                $time_start =  self::$today_time_start;
                $time_end = self::$today_time_end;
            }else{

                $time_start =  $params['year'].'-'. $params['month'].'-'.$params['day'].' 00:00:00';
                $time_end =date('Y-m-d 23:59:59', strtotime($time_start));
            }
        }
        if(!isset($params['day'])&&intval($params['month'])>0&&intval($params['year'])>0)
        {
            if (empty($params))
            {
                $time_start =  self::$month_time_start;
                $time_end = self::$month_time_end;
            }else{

                $params['month']=strlen($params['month'])<2?'0'.$params['month']:$params['month'];
                $time_start =  $params['year'].'-'. $params['month'].'-01 00:00:00';
                $time_end =date('Y-m-d 23:59:59', strtotime($time_start.'+1month -1day'));
            }
        }
        if(!isset($params['day'])&&!isset($params['month']))
        {
            if (empty($params))
            {
                $time_start =  self::$year_time_start;
                $time_end = self::$year_time_end;
            }else{
                $time_start =  $params['year'].'-01-01 00:00:00';
                $time_end =date('Y-m-d 00:00:00', strtotime($time_start.'+1year'));
            }
        }

        $data=[
            'time_start'=>$time_start,
            'time_end'=>$time_end,
        ];

        return DataReturn('ok',0,$data);
    }
    /**
     * 计算比率
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月7日11:57:29
     * @param    [array]          $params [输入参数]
     */
    public static function CalcRate($params){
        $result=$params['bottom']>0?number_format($params['top']/$params['bottom'],2):0;
        return $result;
    }
    /**
     * 收入方式统计公共方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $time_type [查询时间类型]
     * @param    [int]          $income_type [收入方式]
     * @param    [float]          $total_count [收入方式总数]
     * @param    [array]          $params [日期或时间参数]
     */
    public static function IncomeTypeInfoSql($time_type,$income_type,$total_count,$params)
    {
        $data=[];
        $time_start=self::HandleTime($params)['data']['time_start'];
        $time_end=self::HandleTime($params)['data']['time_end'];

        $where = [
            ['pay_create', '>=', $time_start],
            ['pay_create', '<=', $time_end],
            ['pay_type','=',$income_type],
        ];
        $typetotal_count = Db::name('biz_income')->where($where)->sum('biz_income');
        $total_count_rate=self::CalcRate(['top'=>$typetotal_count,'bottom'=>$total_count]);
        $data['rate']=$total_count_rate;
        $data['count']=ceil($typetotal_count);
        //如果是微信或支付宝 ：线上，线下金额
        if (in_array($income_type,[1,2]))
        {
            $where = [
                ['pay_create', '>=', $time_start],
                ['pay_create', '<=', $time_end],
                ['pay_type','=',$income_type],
                ['pay_mode','=',1],
            ];
            $total_count_rate_online = Db::name('biz_income')->where($where)->sum('biz_income');

            $where = [
                ['pay_create', '>=', $time_start],
                ['pay_create', '<=', $time_end],
                ['pay_type','=',$income_type],
                ['pay_mode','=',2],
            ];
            $total_count_rate_offline = Db::name('biz_income')->where($where)->sum('biz_income');
            $data['online']=ceil($total_count_rate_online);
            $data['offline']=ceil($total_count_rate_offline);
        }
        return $data;
    }
    /**
     * 收入统计：收入方式统计（月）
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [输入参数]
     */
    public static function IncomeTypeMonthInfo($params = [])
    {
//        $where=[
//            ['pay_create','>','2020-10-23'],
//            ['pay_create','<','2020-11-01']
//        ];
//        $data=Db::name('biz_income')->where($where)->select();
//        dump( $data);exit;
//        foreach ($data as $k=>$v)
//        {
//
////            if ($v['pay_type']>2) {}
//                Db::name('biz_income')->where('id=' . $v['id'])
//                    ->update(['pay_create' => date('Y-m-d H:i:s', strtotime('+1 year',strtotime($v['pay_create'])))]);
//
//
//        }

        //dump(date('Y-m-d H:i:s', strtotime('+1 year',strtotime($v['pay_create']))));

        // 初始化
        self::Init($params);

        //本月
         $month_time_start=self::HandleTime($params)['data']['time_start'];
         $month_time_end=self::HandleTime($params)['data']['time_end'];
        $where = [
            ['pay_create', '>=', $month_time_start],
            ['pay_create', '<=', $month_time_end],
        ];
        //本月总数
        $total_count_month = Db::name('biz_income')->where($where)->sum('biz_income');

        //dump($total_count_month);exit;
        // 本月-wx:线上 线下 总数 比率
        $month_count_total_wx=self::IncomeTypeInfoSql('month',1,$total_count_month,$params)['count'];
        $month_count_rate_wx=self::IncomeTypeInfoSql('month',1,$total_count_month,$params)['rate'];
        $month_count_online_wx=self::IncomeTypeInfoSql('month',1,$total_count_month,$params)['online'];
        $month_count_offline_wx=self::IncomeTypeInfoSql('month',1,$total_count_month,$params)['offline'];

        // 本月-ali:online offline total rate
        $month_count_total_ali=self::IncomeTypeInfoSql('month',2,$total_count_month,$params)['count'];
        $month_count_rate_ali=self::IncomeTypeInfoSql('month',2,$total_count_month,$params)['rate'];
        $month_count_online_ali=self::IncomeTypeInfoSql('month',2,$total_count_month,$params)['online'];
        $month_count_offline_ali=self::IncomeTypeInfoSql('month',2,$total_count_month,$params)['offline'];

        // 本月-现金
        $month_count_total_cash=self::IncomeTypeInfoSql('month',4,$total_count_month,$params)['count'];
        $month_count_rate_cash=self::IncomeTypeInfoSql('month',4,$total_count_month,$params)['rate'];
        // 本月-银联

        $month_count_total_unionpay=self::IncomeTypeInfoSql('month',3,$total_count_month,$params)['count'];
        $month_count_rate_unionpay=self::IncomeTypeInfoSql('month',3,$total_count_month,$params)['rate'];

        $result = [
            'wx'=>[
                'count'=>$month_count_total_wx,
                'rate'=>$month_count_rate_wx,
                'online'=>$month_count_online_wx,
                'offline'=>$month_count_offline_wx
            ],
            'ali'=>[
                'count'=>$month_count_total_ali,
                'rate'=>$month_count_rate_ali,
                'online'=>$month_count_online_ali,
                'offline'=>$month_count_offline_ali
            ],
            'unionpay'=>[
                'count'=>$month_count_total_unionpay,
                'rate'=>$month_count_rate_unionpay,
            ],
            'cash'=>[
                'count'=>$month_count_total_cash,
                'rate'=>$month_count_rate_cash,
            ],
        ];
        //dump($result);exit;
        return DataReturn('处理成功', 0, $result);
    }


    /**
     * 收入统计：收入方式统计（天）
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [输入参数]
     */
    public static function IncomeTypeDayInfo($params = [])
    {

        // 初始化
        self::Init($params);

        $day_time_start=self::HandleTime($params)['data']['time_start'];
        $day_time_end=self::HandleTime($params)['data']['time_end'];
        $where = [
            ['pay_create', '>=', $day_time_start],
            ['pay_create', '<=', $day_time_end],
        ];
        //今天总数
        $total_count = Db::name('biz_income')->where($where)->sum('biz_income');
        // 今天-wx：线上，线下，总数，比率

        $day_count_total_wx=self::IncomeTypeInfoSql('day',1,$total_count,$params)['count'];
        $day_count_rate_wx=self::IncomeTypeInfoSql('day',1,$total_count,$params)['rate'];
        $day_count_online_wx=self::IncomeTypeInfoSql('day',1,$total_count,$params)['online'];
        $day_count_offline_wx=self::IncomeTypeInfoSql('day',1,$total_count,$params)['offline'];

        // 今天-ali：线上，线下，总数，比率
        $day_count_total_ali=self::IncomeTypeInfoSql('day',2,$total_count,$params)['count'];
        $day_count_rate_ali=self::IncomeTypeInfoSql('day',2,$total_count,$params)['rate'];
        $day_count_online_ali=self::IncomeTypeInfoSql('day',2,$total_count,$params)['online'];
        $day_count_offline_ali=self::IncomeTypeInfoSql('day',2,$total_count,$params)['offline'];

        //dump($day_count_ali);exit;
        // 今天-现金:总数 比率
        $day_count_total_cash=self::IncomeTypeInfoSql('day',4,$total_count,$params)['count'];
        $day_count_rate_cash=self::IncomeTypeInfoSql('day',4,$total_count,$params)['rate'];

        // 今天-银联：总数 比率
        $day_count_total_unionpay=self::IncomeTypeInfoSql('day',3,$total_count,$params)['count'];
        $day_count_rate_unionpay=self::IncomeTypeInfoSql('day',3,$total_count,$params)['rate'];

        $result = [
            'wx'=>[
                'count'=>$day_count_total_wx,
                'rate'=>$day_count_rate_wx,
                'online'=>$day_count_online_wx,
                'offline'=>$day_count_offline_wx
            ],
            'ali'=>[
                'count'=>$day_count_total_ali,
                'rate'=>$day_count_rate_ali,
                'online'=>$day_count_online_ali,
                'offline'=>$day_count_offline_ali
            ],
            'unionpay'=>[
                'count'=>$day_count_total_unionpay,
                'rate'=>$day_count_rate_unionpay,
            ],
            'cash'=>[
                'count'=>$day_count_total_cash,
                'rate'=>$day_count_rate_cash,
            ],

        ];
        //dump($result);exit;
        return DataReturn('处理成功', 0, $result);
    }

    /**
     * 收入统计：收入来源统计（月）
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [输入参数]
     */
    public static function IncomeSourceInfo($params)
    {
        self::Init([]);

        //1：服务收入 2 办理会员，3，商品销售  4,活动收入5 会员充值 6会员升级
        //当月总数

        $time_start=self::HandleTime($params)['data']['time_start'];
        $time_end=self::HandleTime($params)['data']['time_end'];
//        dump($time_start);
//        dump($time_end);
//        exit;
        $where = [
            ['pay_create', '>=', $time_start],
            ['pay_create', '<=', $time_end],
        ];
        $total_count = Db::name('biz_income')->where($where)->sum('biz_income');

        //服务总数
        $service=self::IncomeSourceInfoSql(1,$total_count,$params);
        //办理会员
        $handlemember=self::IncomeSourceInfoSql(2,$total_count,$params);
        //商品销售
        $product=self::IncomeSourceInfoSql(3,$total_count,$params);
        //活动收入
        $active=self::IncomeSourceInfoSql(4,$total_count,$params);
        //会员充值
        $recharge=self::IncomeSourceInfoSql(7,$total_count,$params);
        //会员升级
        $upgrade=self::IncomeSourceInfoSql(6,$total_count,$params);
        $data=[
            'service'=>$service,
            'handlemember'=>$handlemember,
            'product'=>$product,
            'active'=>$active,
            'recharge'=>$recharge,
            'upgrade'=>$upgrade,
        ];
        return DataReturn('ok',0,$data);
    }

    /**
     * 收入统计：收入来源统计（天）
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [输入参数]
     */
    public static function IncomeSourceDayInfo($params)
    {
        self::Init([]);
        //1：服务收入 2 办理会员，3，商品销售  4,活动收入5 会员充值 6会员升级

        $time_start=self::HandleTime($params)['data']['time_start'];
        $time_end=self::HandleTime($params)['data']['time_end'];

        $where = [
            ['pay_create', '>=', $time_start],
            ['pay_create', '<=', $time_end],
        ];
        //当天总数
        $today_total_count = Db::name('biz_income')->where($where)->sum('biz_income');

        //当天服务总数
        $today_service=self::IncomeSourceInfoSql(1,$today_total_count);

        //当天办理会员
        $today_handlemember=self::IncomeSourceInfoSql(2,$today_total_count);

        //当天商品销售
        $today_product=self::IncomeSourceInfoSql(3,$today_total_count);
        //当天活动收入
        $today_active=self::IncomeSourceInfoSql(4,$today_total_count);
        //当天会员充值
        $today_recharge=self::IncomeSourceInfoSql('today',7,$today_total_count);
        //当天会员升级
        $today_upgrade=self::IncomeSourceInfoSql('today',6,$today_total_count);

        $data=[
            'service'=>$today_service,
            'handlemember'=>$today_handlemember,
            'product'=>$today_product,
            'active'=>$today_active,
            'recharge'=>$today_recharge,
            'upgrade'=>$today_upgrade,

        ];

        return DataReturn('ok',0,$data);
    }
    /**
     * 收入统计：收入来源统计（当天 当月）
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $time [查询时间]
     * @param    [string]          $income_type [收入来源]
     * @param    [float]          $today_total_count [收入总数]
     * @param    [array]          $params [查询参数]
     */
    public static function IncomeSourceInfoSql($income_type,$today_total_count,$params){

        $time_start=self::HandleTime($params)['data']['time_start'];
        $time_end=self::HandleTime($params)['data']['time_end'];

        $where = [
            ['pay_create', '>=', $time_start],
            ['pay_create', '<=', $time_end],
            ['income_type', '=', $income_type],
        ];
        $today_handlemember_count = Db::name('biz_income')->where($where)->sum('biz_income');

        $today_handlemember_count=priceFormat($today_handlemember_count);

        $today_handlemember_rate = $today_total_count<1?0:number_format($today_handlemember_count/$today_total_count,2);



        $data=[
            'count'=>$today_handlemember_count,
            'rate'=>$today_handlemember_rate,
            'title'=>lang('member_fee_aim')[$income_type]
        ];
        return $data;
    }
    /**
     * 支出统计：当月 当年
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [查询参数]
     */
    public static function ExpenditureInfo($params){
        self::Init([]);
        //计算时间
        $time=self::HandleTime($params);

        $where = [
            ['create_time', '>=', $time['data']['time_start']],
            ['create_time', '<=', $time['data']['time_end']],
        ];
        //总和
        $total = Db::name('expenses')->where($where)->sum('price');
        //分类费用总和
        $info = Db::name('expenses')->where($where)->group('type_id')->field('sum(price) price,type_id')->select();
        foreach ($info as &$v)
        {
            $v['cate_title']=Db::name('costtype')->where('id='.$v['type_id'])->value('type_title');
            $v['cate_rate']=self::CalcRate(['top'=>$v['price'],'bottom'=>$total]);
                //number_format($v['price']/$total,2);
        }
//
//        //年度费用计算
//        $where = [
//            ['create_time', '>=', self::$year_time_start],
//            ['create_time', '<=', self::$year_time_end],
//            ['category', '=', 2],
//        ];
//        //总和
//        $year_total = Db::name('expenses')->where($where)->sum('price');
//        //分类费用总和
//        $year_info = Db::name('expenses')->where($where)->group('type_id')->field('sum(price) price,type_id')->select();
//        foreach ($year_info as &$v) {
//            $v['cate_title']=Db::name('costtype')->where('id='.$v['type_id'])->value('type_title');
//            $v['cate_rate']=number_format($v['price']/$year_total,2);
//        }

        return DataReturn('ok',0,$info);
    }

    /**
     * 市场费用统计：当月 当年
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [查询参数]
     */
//    public static function ExpensesInfo($params){
//
//
//
//        self::Init([]);
//        //计算时间
//        $time=self::HandleTime($params);
//
//        $where = [
//            ['create_time', '>=', $time['data']['time_start']],
//            ['create_time', '<=', $time['data']['time_end']],
//        ];
//        //月度市场费用计算:余额 积分 卡卷 门店
//        $month_info=self::ExpensesInfoTypeData('month');
//
//        $year_info=self::ExpensesInfoTypeData('year');
//
//        return DataReturn('ok',0,['year'=>$year_info,'month'=>$month_info]);
//        //年度市场费用计算:余额 积分 卡卷 门店
//    }
    /**
     * 市场费用统计:获取分类（月 年）数据
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [array]          $params [查询参数]
     */
    public static function ExpensesInfo($params){


        $time=self::HandleTime($params);

        $where = [
            ['subsidy_create', '>=', $time['data']['time_start']],
            ['subsidy_create', '<=', $time['data']['time_end']],
        ];

        //总和
        $total = Db::name('subsidy')->where($where)->sum('subsidy_number');
        //余额赠送
        $balance=self::ExpensesInfoCommon($params,1,$total);
        //积分
        $integral=self::ExpensesInfoCommon($params,2,$total);
        //卡卷
        $coupon=self::ExpensesInfoCommon($params,3,$total);
        //门店
        $store=self::ExpensesInfoCommon($params,4,$total);
        $data=[
            'balance'=>$balance,
            'integral'=>$integral,
            'coupon'=>$coupon,
            'store'=>$store,
        ];
        return $data;
    }
    /**
     * 市场费用统计公用方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $params [参数]
     * @param    [string]          $income_type [费用类型]
     * @param    [float]          $total_count [费用总数]
     */
    private static function ExpensesInfoCommon($params,$expenses_type,$total_count){

        $time=self::HandleTime($params);
        $time_start=$time['data']['time_start'];
        $time_end=$time['data']['time_end'];

        $title='';
        switch ($expenses_type){
            case 1:
                $title='余额赠送';
                break;
            case 2:
                $title='积分兑换';
                break;
            case 3:
                $title='卡卷使用';
                break;
            case 4:
                $title='门店补贴';
                break;
        }
        $where = [
            ['subsidy_create', '>=', $time_start],
            ['subsidy_create', '<=', $time_end],
            ['subsidy_type', '=', $expenses_type],
        ];
        $total_cate = Db::name('subsidy')->where($where)->sum('subsidy_number');
        $total_cate = empty($total_cate)?0.00:priceFormat($total_cate);
        $rate = self::CalcRate(['top'=>$total_cate,'bottom'=>$total_count]);

        $data=[
            'total_cate'=>$total_cate,
            'rate'=>$rate,
            'total'=>$total_count,
            'title'=>$title
        ];
        return $data;
    }

    /**
     * 利润统计：共用方法
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param   [string]      $param  [查询参数]
     */
    public static function ProfitInfo($param){
        $time_start=self::HandleTime($param)['data']['time_start'];
        $time_end=self::HandleTime($param)['data']['time_end'];
        $where = [
            ['pay_create', '>=', $time_start],
            ['pay_create', '<=', $time_end],
        ];
        //收入
        $income = Db::name('biz_income')->where($where)->sum('biz_income');

        $income = empty($income)? 0.00:priceFormat($income);
        //支出
        $where = [
            ['create_time', '>=', $time_start],
            ['create_time', '<=', $time_end],
        ];
        $expenses = Db::name('expenses')->where($where)->sum('price');
        $expenses = empty($expenses)? 0.00:priceFormat($expenses);
        $rate_expenses = number_format($expenses/$income,3);
        //利润
        $profit=$income-$expenses;

        $rate_profit = number_format($profit/$income,2);
        $info=[
            'expenses'=>[
                'rate'=>$rate_expenses,
                'total'=>$expenses
            ],
            'profit'=>[
                'rate'=>$rate_profit,
                'total'=>$profit
            ]
        ];

        return $info;
    }
    /**
     * 门店申请统计
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [int]          $biz_status [门店状态]
     * @param    [int]          $biz_type [门店类型]
     */
    public static function BizCount($biz_type)
    {

        if ($biz_type==1) {
            $count=Db::name('merchants')->where('status=3')->count();
        }else{
            $where=[
                'biz_type'=>$biz_type,
                'read_status'=>1,
            ];
            $count=Db::name('biz_apply')->where($where)->count();
        }

        return DataReturn('ok',0,$count);
    }

    /**
     * 合伙人申请统计
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     */
    public static function PartnerCount()
    {
//        $re=Db::name('member')->where('id < 20')->select();
//        foreach ($re as $v) {
//            Db::name('member_partner')->where('member_id='.$v['id'])->update(['phone'=>$v['member_phone']]);
//        }


        $where="date(create_time) = '".date('Y-m-d',strtotime("-1 day"))."'";
        $count=Db::name('member_partner')->where($where)->count();
        return DataReturn('ok',0,$count);
    }
    /**
     * 门店提现审核统计
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     */
    public static function DepositBiz(){
        //门店提现 等待审核
        $where=[
            ['source_type','in',[1,3]],
            ['deposit_status','in',[1,2]],
        ];
        $count=Db::name('log_deposit')->where($where)->count();
        return DataReturn('ok',0,$count);
    }
    /**
     * 会员数据计算
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $field [需要计算的字段]
     */
    public static function MemberData($field='member_balance'){
        //member_balance 会员总余额
        $where=[
            ['id','>',0]
        ];
        if ($field=='member_balance')
        {
            $where=[
                ['member_level_id','>',0]
            ];
        }

        $count=Db::name('member_mapping')->where($where)->sum($field);
        return DataReturn('ok',0,$count);
    }
    /**
     * 会员余额使用情况计算
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $field [需要计算的字段]
     */
    public static function UseBalanceData(){
        self::Init([]);
        //今日余额使用总数:门店消费记录+商户消费记录
        $where = [
            ['consump_time', '>=',  self::$today_time_start],
            ['consump_time', '<=', self::$today_time_end],
            ['log_consump_type', '=',1],
            ['consump_pay_type','=',5],
            ['income_type','<>',20]
        ];
        $today_total_biz = Db::name('log_consump')->where($where)->sum('consump_price');

        $where = [
            ['create_time', '>=',  self::$today_time_start],
            ['create_time', '<=', self::$today_time_end],
            ['cash_type', '=', 5],
        ];
        $today_total_merchants = Db::name('merchants_order')->where($where)->sum('actual_price');
        $today_total=priceFormat($today_total_biz);//+$today_total_merchants
        //昨日余额使用总数

        $where = [
            ['consump_time', '>=',  self::$yesterday_time_start],
            ['consump_time', '<=', self::$yesterday_time_end],
            ['log_consump_type', '=',1],
            ['consump_pay_type','=',5]
        ];
        $yesterday_total_biz = Db::name('log_consump')->where($where)->sum('consump_price');

        $where = [
            ['create_time', '>=',  self::$yesterday_time_start],
            ['create_time', '<=', self::$yesterday_time_end],
            ['cash_type', '=', 5],
        ];
        $yesterday_total_merchants = Db::name('merchants_order')->where($where)->sum('actual_price');
        $yesterday_total=priceFormat($yesterday_total_biz);//+$yesterday_total_merchants


        return DataReturn('ok',0,['today'=>$today_total,'yesterday'=>$yesterday_total]);
    }
    /**
     * 合伙人提现
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $field [需要计算的字段]
     */
    public static function PartnerDeposit(){
        $where=[
            ['source_type', '=',  2],
            ['create_time', '>',  date('Y-m-d 00:00:00',time())],
            ['create_time', '<',  TIMESTAMP],
            ['member_id', 'neq',  25],
        ];
        $count=Db::name('log_deposit')->where($where)->sum('price');
        return DataReturn('ok',0,$count);
    }
    /**
     * 用户排名
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月22日11:51:06
     * @param    [string]          $field [需要计算的字段]
     */
    public static function UserRegisterRanking(){

        $re=Db::name('member_mapping')
            ->alias('mm')
            ->leftJoin(['member'=>'m'],'mm.member_id=m.id')
            ->field('member_area,member_city,member_province,count(*) num,concat(m.member_province,m.member_city,m.member_area) address_all')
            ->where('length(member_area) >0')
            ->group('address_all')
            ->order('num desc')
            ->select();
//        $re=Db::name('member_trip')
//            ->field('count(*) num,concat(province,city,area) address_all,province,city,area')
//            ->group('address_all')
//            ->order('num desc')
//            ->select();

        $data=[];
        $k_data=0;
        foreach ($re as $k=>$v) {
            if (count($data)<10)
            {
                if (!empty($v['address_all']))
                {

                    $data[$k_data] =$v;
//                    $where=[
//                        ['province','=',$v['province']],
//                        ['city','=',$v['city']],
//                        ['area','=',$v['area']],
//                    ];
//                    $data[$k_data]['num']=Db::name('member_trip')->where($where)->group('member_id')->count();
                    $k_data+=1;

                }

            } else{
                break;
            }
        }

//        if (count($data)<10) {
//            for($i=count($data);$i<10;$i++){
//                $data[$i]=[
//                    'num'=>'',
//                    'address_all'=>''
//                ];
//            }
//        }

         return DataReturn('ok',0,$data);
    }
    /*
     * @content 商户到期个数 查询 15 天以内的
     * @param renew_status 是否续费 0 是  1 否  数据中心展示数据状态
     * */
    static function merchangts_expiration(){
        $time = date("Y-m-d");
        $datetime = date('Y-m-d',strtotime(" + 15 day"));
        $data = Db::table('merchants')->where("renew_status = 0 and status = 1 and level<3 and '$time'<= DATE_FORMAT(expire_time,'%Y-%m-%d') and  '$datetime'>= DATE_FORMAT(expire_time,'%Y-%m-%d') ")->count('id');

        return $data;

    }

    /**
     * @param $data
     * @return array
     * @throws DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 收入统计数据
     */
    static function incomeChartData($data){
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(pay_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(pay_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(pay_create)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(pay_create)<='".$data['end_time']."'";
                }
            }
        }
        # 计算总收入
        $total = Db::table("biz_income")
            ->where("income_type in (12,13,14,3,15,16,17)")
            ->where($where)->sum("biz_income");
        $list = Db::table("biz_income")
            ->field("income_type,convert(coalesce(sum(biz_income),0),decimal(10,2)) price,biz_id")
            ->where("income_type in (12,13,14,3,15,16,17)")
            ->where($where)
            ->group("income_type")
            ->select();
        $_type_array = lang("biz_income_type");
        $title_array = array(12=>$_type_array[12],13=>$_type_array[13],14=>$_type_array[14],3=>$_type_array[3],
            15=>$_type_array[15],16=>$_type_array[16],17=>$_type_array[17]);
        $return_array = array(
            12=>array("title"=>$_type_array[12],"income_type"=>12,"value"=>0.00,"ratio"=>0),
            13=>array("title"=>$_type_array[13],"income_type"=>13,"value"=>0.00,"ratio"=>0),
            14=>array("title"=>$_type_array[15],"income_type"=>14,"value"=>0.00,"ratio"=>0),
            3=>array("title"=>$_type_array[3],"income_type"=>3,"value"=>0.00,"ratio"=>0),
            15=>array("title"=>$_type_array[15],"income_type"=>15,"value"=>0.00,"ratio"=>0),
            16=>array("title"=>$_type_array[16],"income_type"=>16,"value"=>0.00,"ratio"=>0),
            17=>array("title"=>$_type_array[17],"income_type"=>17,"value"=>0.00,"ratio"=>0)
        );
        if(!empty($list)){
            foreach($list as $k=>$v){
                $return_array[$v['income_type']]['value'] = $v['price'];
                $return_array[$v['income_type']]['ratio'] = getsPriceFormat($v['price']/$total*100);
            }
        }
        return array("data"=>array_values($return_array),"title"=>array_values($title_array),"total"=>getsPriceFormat($total));
    }

    /**
     * @param $data
     * @return array
     * @throws \think\exception\DbException
     * @context 收入统计详情
     */
    static function incomeDetail($data){
        $where = " biz_income.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(pay_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(pay_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(pay_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(pay_create)<='".$data['end_time']."'";
                    }
                }
            }

        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(pay_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(pay_create)<='".$data['param']['end_time']."'";
            }
        }
        if(!empty($data['param']['income_type'])){
            $where.=" and income_type={$data['param']['income_type']}";
        }else{
            if(!empty($data['income_type'])){
                $where.=" and income_type={$data['income_type']}";
            }
        }
        if(!empty($data['param']['keywords'])){
            $where.="biz_income.member_id in (select id form member where member_phone like '%".$data['param']['keywords']."%')";
        }
        $list = Db::table("biz_income")
            ->field("income_type,convert(coalesce(biz_income,0),decimal(10,2)) price,pid,biz_id,pay_create,pay_type,biz_income.member_id,order_number,m.nickname,m.member_name,m.member_phone")
            ->leftJoin("member m","m.id=biz_income.member_id")
            ->leftJoin("member_mapping mm","m.id=mm.member_id")
            ->where("income_type in (12,13,14,3,15,16,17)")
            ->where($where)
            ->order("pay_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        $total_price = 0;
        if(!empty($list['data'])){
            $_type_array = lang("biz_income_type");
            foreach($list['data'] as $k=>$v){
                $total_price+=$v["price"];
                $list['data'][$k]['title'] = $_type_array[$v['income_type']];
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $list['data'][$k]["income_channel"] = self::incomeChannel($v['income_type']);
                $list['data'][$k]['member_mark'] = 1;// 跳用户
                $list['data'][$k]['pay_type_title'] = getpay_type($v['pay_type']);
                if(in_array($v['income_type'],array(13,14))){
                    # 查询商家订单
                    $info = Db::table("merchants_order")
                        ->join("merchants","merchants.id = merchants_order.merchants_id")
                        ->field("total_price,actual_price,merchants.title,merchants.contacts_phone,merchants_id,is_type")
                        ->where(array("order_number"=>$v['order_number']))->find();
                    $list['data'][$k]['order_price'] = $info['total_price'];
                    $list['data'][$k]['pay_price'] = $info['actual_price'];
                    $list['data'][$k]['payee'] = $info['title'];
                    $list['data'][$k]['voucher_price'] =getsPriceFormat($info['total_price']-$info['actual_price']) ;

                    if($v['income_type']==14){
                        $list['data'][$k]["member_title"] = $info['title'];
                        $list['data'][$k]["member_phone"] = $info['contacts_phone'];
                        $list['data'][$k]['payee'] = "平台";
                        $list['data'][$k]['member_id'] = $info['merchants_id'];
                        $list['data'][$k]['is_type'] = $info['is_type'];
                        $list['data'][$k]['member_mark'] = 2;//跳商户
                    }
                }elseif($v['income_type']==17){
                    # 查询平台收入
                    $info = Db::table("expenses")->where(array("id"=>$v['pid']))->find();
                    $list['data'][$k]['order_price'] = $info['price'];
                    $list['data'][$k]['pay_price'] = $info['price'];
                    $list['data'][$k]['payee'] = $info['username'];
                    $list['data'][$k]['voucher_price'] =0 ;
                    $list['data'][$k]['income_channel'] ="其他渠道";
                    $list['data'][$k]['title'] =$info['title'];
                }else{
                    $info = Db::table("orders")->field("order_price,pay_price")->where(array("order_number"=>$v['order_number']))->find();
                    # 查询卡券抵用
                    $log_cashpon = Db::table("log_cashcoupon")->where(array("order_number"=>$v['order_number']))->sum("voucher_price");
                    $list['data'][$k]['order_price'] = $info['order_price'];
                    $list['data'][$k]['pay_price'] = $info['pay_price'];
                    $list['data'][$k]['voucher_price'] = empty($log_cashpon) ? 0:getsPriceFormat($log_cashpon);
                        # 查询门店类型
                    $bizInfo = Db::table("biz")->field("biz_type,biz_title")->where(array("id"=>$v['biz_id']))->find();
                    $list['data'][$k]['biz_type'] = $info['biz_type'];
                    if($v['income_type']!=12){
                        if(!empty($bizInfo)){
                            if($bizInfo['biz_type']==1){
                                $list['data'][$k]["income_channel"] = "直营门店";
                            }elseif($bizInfo['biz_type']==3){
                                $list['data'][$k]["income_channel"] = "合作门店";
                            }else{
                                $list['data'][$k]["income_channel"] = "加盟门店";
                            }
                        }
                    }
                    $list['data'][$k]['payee'] = $bizInfo['biz_title'];

                }

            }
        }
        return array("list"=>$list,"total_price"=>getsPriceFormat($total_price));
    }
    static function incomeChannel($incomeType){
        if($incomeType==12){
            return "合作门店";
        }elseif($incomeType==13 or $incomeType==14){
            return "联盟商家";
        }else{
            return "平台收入";
        }
    }

    static function subsidyChartData($data)
    {
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(subsidy_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(subsidy_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(subsidy_create)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(subsidy_create)<='".$data['end_time']."'";
                }
            }
        }

        # 计算总收入
        $total = Db::table("subsidy")
            ->where($where)->sum("subsidy_number");
        $list = Db::table("subsidy")
            ->field("subsidy_type,convert(coalesce(sum(subsidy_number),0),decimal(10,2)) price,biz_id,member_id,pid")
            ->where($where)
            ->group("subsidy_type")
            ->select();
        $_type_array = lang("subsidy_type");
        $return_array = array(
            1=>array("title"=>$_type_array[1],"subsidy_type"=>1,"value"=>0.00,"ratio"=>0),
            2=>array("title"=>$_type_array[2],"subsidy_type"=>2,"value"=>0.00,"ratio"=>0),
            3=>array("title"=>$_type_array[3],"subsidy_type"=>3,"value"=>0.00,"ratio"=>0),
            4=>array("title"=>$_type_array[4],"subsidy_type"=>4,"value"=>0.00,"ratio"=>0),
            5=>array("title"=>$_type_array[5],"subsidy_type"=>5,"value"=>0.00,"ratio"=>0),
            6=>array("title"=>$_type_array[6],"subsidy_type"=>6,"value"=>0.00,"ratio"=>0),
            7=>array("title"=>$_type_array[7],"subsidy_type"=>7,"value"=>0.00,"ratio"=>0),
            8=>array("title"=>$_type_array[8],"subsidy_type"=>8,"value"=>0.00,"ratio"=>0),
            9=>array("title"=>$_type_array[9],"subsidy_type"=>9,"value"=>0.00,"ratio"=>0),
            10=>array("title"=>$_type_array[10],"subsidy_type"=>10,"value"=>0.00,"ratio"=>0),
            11=>array("title"=>$_type_array[11],"subsidy_type"=>11,"value"=>0.00,"ratio"=>0),
            12=>array("title"=>$_type_array[12],"subsidy_type"=>12,"value"=>0.00,"ratio"=>0),
            13=>array("title"=>$_type_array[13],"subsidy_type"=>13,"value"=>0.00,"ratio"=>0),
            14=>array("title"=>$_type_array[14],"subsidy_type"=>14,"value"=>0.00,"ratio"=>0),
            15=>array("title"=>$_type_array[15],"subsidy_type"=>15,"value"=>0.00,"ratio"=>0),
            16=>array("title"=>$_type_array[16],"subsidy_type"=>15,"value"=>0.00,"ratio"=>0),
        );
        if(!empty($list)){
            foreach($list as $k=>$v){
                $return_array[$v['subsidy_type']]['value'] = $v['price'];
                $return_array[$v['subsidy_type']]['ratio'] = getsPriceFormat($v['price']/$total*100);
            }
        }
        return array("data"=>array_values($return_array),"title"=>array_values($_type_array),"total"=>getsPriceFormat($total));

    }
    
    function subsidyMemberDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }

        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,
                          m.nickname,m.member_name,m.member_phone,mm.member_level_id,convert(coalesce(mm.member_balance,0),decimal(10,2)) member_balance,mm.member_expiration")
            ->leftJoin("member m","m.id=s.member_id")
            ->leftJoin("member_mapping mm","m.id=mm.member_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
            }
        }
        return $list;
    }

    function subsidyDiffDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }

        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,
                          m.nickname,m.member_name,m.member_phone,mm.member_level_id,convert(coalesce(mm.member_balance,0),decimal(10,2)) member_balance,mm.member_expiration,
                          b.biz_title,b.biz_address,b.biz_phone,os.service_title,os.price server_price,os.final_settlement,b.biz_type
                          ")
            ->leftJoin("order_server os","os.id = s.pid")
            ->leftJoin("biz b","b.id = s.biz_id")
            ->leftJoin("member m","m.id=s.member_id")
            ->leftJoin("member_mapping mm","m.id=mm.member_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
            }
        }
        return $list;
    }

    function subsidyVoucherDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }

        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,s.order_type,
                          m.nickname,m.member_name,m.member_phone,mm.member_level_id,convert(coalesce(mm.member_balance,0),decimal(10,2)) member_balance,mm.member_expiration,
                          if(s.order_type=3,mc.title,b.biz_title) biz_title,
                          if(s.order_type=3,concat(mc.province,mc.city,mc.area),b.biz_address) biz_address,
                          if(s.order_type=3,mc.contacts_phone,b.biz_phone) biz_phone,b.biz_type,mc.is_type,
                          mv.card_title,convert(coalesce(mv.voucher_cost,0),decimal(10,2)) voucher_cost,mv.voucher_source,
                          if(s.order_type=3,convert(coalesce(mo.total_price,0),decimal(10,2)),convert(coalesce(o.order_price,0),decimal(10,2))) order_price, 
                          if(s.order_type=3,convert(coalesce(mo.actual_price,0),decimal(10,2)),convert(coalesce(o.pay_price,0),decimal(10,2))) pay_price, 
                          if(s.order_type=3,mo.cash_type,o.pay_type) pay_type")
            ->leftJoin("orders o","o.order_number = s.order_number and s.order_type!=3")
            ->leftJoin("log_cashcoupon lc","lc.order_number = s.order_number and lc.order_type = s.order_type and lc.workorder_id = s.pid and s.order_type!=3")
            ->leftJoin("merchants_order mo","mo.id = s.pid and s.order_type=3")
            ->leftJoin("member_voucher mv","mv.id = lc.voucher_id and s.order_type!=3")
            ->leftJoin("member_voucher mv","mv.id = s.voucher_id and s.order_type=3")
            ->leftJoin("biz b","b.id = s.biz_id and s.order_type!=3")
            ->leftJoin("merchants mc","mc.id = s.biz_id and s.order_type=3")
            ->leftJoin("member m","m.id=s.member_id")
            ->leftJoin("member_mapping mm","m.id=mm.member_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        if(!empty($list['data'])){
            $voucherService = new VoucherService();
            foreach($list['data'] as $k=>$v){
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $list['data'][$k]['voucher_source_title'] = $voucherService->voucherChannel($v['voucher_source']);
                $list['data'][$k]['pay_type_title'] = getpay_type($v['pay_type']);
            }
        }
        return $list;
    }

    function subsidyBizTaskDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }

        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,
                          b.biz_title,b.biz_address,b.biz_phone,b.biz_type,ct.title task_title,(case ct.type 
                          when 1 then \'办理会员数量\'
                        when 2 then \'销售额\'
                        when 3 then \'服务车辆次数\'
                        when 4 then \'好评数量\'
                        when 5 then \'办理会员金额\'
                        end ) typeInfo,ct.explain_info
                          ")
            ->leftJoin("cooperative_task ct","ct.id = s.pid")
            ->leftJoin("biz b","b.id = s.biz_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        return $list;
    }

    function subsidyBizRecommendDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])){
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }

        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.pid,s.subsidy_create,
                          b.biz_title,b.biz_address,b.biz_phone,b.biz_type,rld.title
                          ")
            ->leftJoin("recommend_log_deposit rld","rld.id = s.pid")
            ->leftJoin("biz b","b.id = s.biz_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        return $list;
    }

    function subsidyMerchantTaskDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])) {
            if (!empty($data['switchStatic'])) {
                if ($data['switchStatic'] < 5) {
                    switch ($data['switchStatic']) {
                        case 1 :
                            $where .= " and date(s.subsidy_create)='" . date("Y-m-d") . "'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='" . date("Y-m-d", strtotime("-1 day")) . "'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='" . date("Y-m") . "'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='" . date("Y-m", strtotime("-1 month")) . "'";
                            break;
                    }
                } else {
                    if (!empty($data['start_time'])) {
                        $where .= " and date(s.subsidy_create)>='" . $data['start_time'] . "'";
                    }
                    if (!empty($data['end_time'])) {
                        $where .= " and date(s.subsidy_create)<='" . $data['end_time'] . "'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
            }
        }
        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,
                          m.title biz_title,concat(m.province,m.city,m.area) biz_address,m.contacts_phone biz_phone,m.is_type,ct.task_title,ct.task_type
                          ,ct.task_describe explain_info
                          ")
            ->leftJoin("merchants_task ct","ct.id = s.pid")
            ->leftJoin("merchants m","m.id = s.biz_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        $type_title = lang("task_type");
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                $list['data'][$k]['typeInfo'] = $type_title[$v['task_type']];
            }
        }
        return $list;
    }

    function subsidyMerchantPacketDetail($data)
    {
        $where = " s.id > 0 " ;
        if(empty($data['param'])) {
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(s.subsidy_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(s.subsidy_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(s.subsidy_create)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(s.subsidy_create)<='".$data['end_time']."'";
                    }
                }
            }
    }else{
        if(!empty($data['param']['start_time'])){
        $where.=" and date(s.subsidy_create)>='".$data['param']['start_time']."'";
        }
        if(!empty($data['param']['end_time'])){
            $where.=" and date(s.subsidy_create)<='".$data['param']['end_time']."'";
        }
    }
        $list = Db::table("subsidy s")
            ->field("s.subsidy_type,convert(coalesce(s.subsidy_number,0),decimal(10,2)) price,s.biz_id,s.member_id,s.pid,s.subsidy_create,
                          mc.title biz_title,concat(mc.province,m.city,m.area) biz_address,mc.contacts_phone biz_phone,mc.is_type,
                          (case mc.level when 1 then '优质商家' when 2 then '推荐商家' when 3 then '诚信商家' when 4 then '普通商家' end) level_title,
                          (select group_concat(mct.title) from merchants_cate_relation mcr,merchants_cate mct where mcr.merchants_id = s.biz_id and mcr.cate_id = mct.id) cate_title,
                          m.nickname,m.member_name,m.member_phone,mm.member_level_id,convert(coalesce(mm.member_balance,0),decimal(10,2)) member_balance,mm.member_expiration,
                          convert(coalesce(mo.total_price,0),decimal(10,2)) total_price,mo.create_time,mo.cash_type
                         
                          ")
            ->leftJoin("merchants_order mo","mo.order_number = s.order_number")
            ->leftJoin("merchants mc","mc.id = s.biz_id")
            ->leftJoin("member m","m.id=s.member_id")
            ->leftJoin("member_mapping mm","m.id=mm.member_id")
            ->where("s.subsidy_type = {$data['subsidy_type']}")
            ->where($where)
            ->order("s.subsidy_create desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        $type_title = lang("task_type");
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                $list['data'][$k]['typeInfo'] = $type_title[$v['task_type']];
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $list['data'][$k]['pay_type_title'] = getpay_type($v['cash_type']);
            }
        }
        return $list;
    }

    function disburseChartData($data)
    {
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(create_time)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(create_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(create_time,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(create_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(create_time)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(create_time)<='".$data['end_time']."'";
                }
            }
        }

        # 计算总支出
        $total = Db::table("expenses")->where(array("category"=>2))
            ->where($where)->sum("price");
        $list = Db::table("expenses")
            ->field("type_id,convert(coalesce(sum(price),0),decimal(10,2)) price,create_time")
            ->where(array("category"=>2))
            ->where($where)
            ->group("type_id")
            ->select();
        $_array = Db::table("costtype")->field("*,0 value,0 ratio")->where(array("type_cate"=>2))->select();
        $_type_array = array_column($_array,"type_title");
        $return_array = array_column($_array,NULL,'id');
        if(!empty($list)){
            foreach($list as $k=>$v){
                $return_array[$v['type_id']]['value'] = $v['price'];
                $return_array[$v['type_id']]['ratio'] = getsPriceFormat($v['price']/$total*100);
            }
        }
        return array("data"=>array_values($return_array),"title"=>array_values($_type_array),"total"=>getsPriceFormat($total));
    }

    function profitChartData($data)
    {
        $disbursewhere = " id > 0 " ;
        $incomewhere = " id > 0 " ;
        $subsidywhere = " id > 0 " ;
        if(empty($data['param'])) {
            if (!empty($data['switchStatic'])) {
                if ($data['switchStatic'] < 5) {
                    switch ($data['switchStatic']) {
                        case 1 :
                            $disbursewhere .= " and date(create_time)='" . date("Y-m-d") . "'";
                            $incomewhere .= " and date(pay_create)='" . date("Y-m-d") . "'";
                            $subsidywhere .= " and date(subsidy_create)='" . date("Y-m-d") . "'";
                            break;
                        case 2 :
                            $disbursewhere .= " and date(create_time)='" . date("Y-m-d", strtotime("-1 day")) . "'";
                            $incomewhere .= " and date(pay_create)='" . date("Y-m-d", strtotime("-1 day")) . "'";
                            $subsidywhere .= " and date(subsidy_create)='" . date("Y-m-d", strtotime("-1 day")) . "'";
                            break;
                        case 3 :
                            $disbursewhere .= " and date_format(create_time,'%Y-%m')='" . date("Y-m") . "'";
                            $incomewhere .= " and date_format(pay_create,'%Y-%m')='" . date("Y-m") . "'";
                            $subsidywhere .= " and date_format(subsidy_create,'%Y-%m')='" . date("Y-m") . "'";
                            break;
                        case 4 :
                            $disbursewhere .= " and date_format(create_time,'%Y-%m')='" . date("Y-m", strtotime("-1 month")) . "'";
                            $incomewhere .= " and date_format(pay_create,'%Y-%m')='" . date("Y-m", strtotime("-1 month")) . "'";
                            $subsidywhere .= " and date_format(subsidy_create,'%Y-%m')='" . date("Y-m", strtotime("-1 month")) . "'";
                            break;
                    }
                } else {
                    if (!empty($data['start_time'])) {
                        $disbursewhere .= " and date(create_time)>='" . $data['start_time'] . "'";
                        $incomewhere .= " and date(pay_create)>='" . $data['start_time'] . "'";
                        $subsidywhere .= " and date(subsidy_create)>='" . $data['start_time'] . "'";
                    }
                    if (!empty($data['end_time'])) {
                        $disbursewhere .= " and date(create_time)<='" . $data['end_time'] . "'";
                        $incomewhere .= " and date(pay_create)<='" . $data['end_time'] . "'";
                        $subsidywhere .= " and date(subsidy_create)<='" . $data['end_time'] . "'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $disbursewhere.=" and date(create_time)>='".$data['param']['start_time']."'";
                $incomewhere.=" and date(pay_create)>='".$data['param']['start_time']."'";
                $subsidywhere.=" and date(subsidy_create)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $disbursewhere.=" and date(create_time)<='".$data['param']['end_time']."'";
                $incomewhere.=" and date(pay_create)<='".$data['param']['end_time']."'";
                $subsidywhere.=" and date(subsidy_create)<='".$data['param']['end_time']."'";
            }
        }
        # 计算总收入
        $income_total = Db::table("biz_income")
            ->where("income_type in (12,13,14,3,15,16,17)")
            ->where($incomewhere)->sum("biz_income");
        # 计算总补贴
        $subsidy_total = Db::table("subsidy")
            ->where($subsidywhere)->sum("subsidy_number");
        # 计算总支出
        $disburse_total = Db::table("expenses")->where(array("category"=>2))
            ->where($disbursewhere)->sum("price");
        # 计算利润
        $profit_total=getsPriceFormat($income_total-$subsidy_total-$disburse_total);

        $return_array = array(

            array("name"=>"利润","value"=>getsPriceFormat($profit_total)),
            array("name"=>"收入","value"=>getsPriceFormat($income_total)),
            array("name"=>"支出","value"=>-getsPriceFormat($disburse_total)),
            array("name"=>"补贴","value"=>-getsPriceFormat($subsidy_total)),
        );
        return array("data"=>array_values($return_array),"total"=>getsPriceFormat($profit_total));
    }

    function MemberTotalData()
    {
        $memberCount = Db::table("member_mapping mm")
            ->join("member m","mm.member_id=m.id")
            ->where("member_level_id>2 or ((member_level_id>=1 or member_level_id=2) and member_expiration>='".date("Y-m-d")."')")->count();
        $balanceCount = Db::table("member_mapping")
            ->where("member_level_id>2 or (member_level_id>1 and member_level_id<=2 and member_expiration>='".date("Y-m-d")."')")->sum("member_balance");
        return array("memberCount"=>intval($memberCount),"balanceCount"=>$balanceCount);
    }

    function memberChartData($data)
    {
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(member_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(member_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(member_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(member_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(member_create)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(member_create)<='".$data['end_time']."'";
                }
            }
        }
        # 查所有会员等级
        $level_title = Db::table("member_level")->field("level_title title,id,0 value,0 price,0 ratio")->select();
        $return_title = array_column($level_title,"title");
        $return_array = array_column($level_title,NULL,'id');
        # 当前时段新增数量
        $add_count = Db::table("log_handlecard")->where($where)->count();
        # 当前时段办理总额
        $add_price =  Db::table("log_handlecard")->where($where)->sum("level_price");
        # 数据统计
        $list = Db::table("log_handlecard")->field("count(id) num,convert(coalesce(sum(level_price),0),decimal(10,2)) price,level_id")
            ->where($where)->group("level_id")->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                $return_array[$v['level_id']]['value'] = $v['num'];
                $return_array[$v['level_id']]['price'] = getsPriceFormat($v['price']);
                $return_array[$v['level_id']]['ratio'] = getsPriceFormat($v['num']/$add_count*100);
            }
        }
        return array("data"=>array_values($return_array),"title"=>$return_title,"add_price"=>getsPriceFormat($add_price),"add_count"=>getsPriceFormat($add_count));
    }

    function memberDistrictChartData($data)
    {
        $where = " lh.id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(lh.member_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(lh.member_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(lh.member_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(lh.member_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(lh.member_create)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(lh.member_create)<='".$data['end_time']."'";
                }
            }
        }
        # 当前时段新增数量
        $add_count = Db::table("log_handlecard lh")->where($where)->count();
        # 数据统计
        $list = Db::table("log_handlecard lh")
            ->field("concat(m.member_province,m.member_city,m.member_area) district,count(lh.id) value")
            ->join("member m","m.id = lh.member_id")
            ->where($where)->group("m.member_province,m.member_city,m.member_area")->order("value desc")->limit(10)->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                if(empty($v['district'])){
                    $list[$k]['district']="未知区域";
                }
                $list[$k]['ratio'] = getsPriceFormat($v['value']/$add_count*100);
            }
        }
        return array("data"=>array_values($list),"title"=>array_column($list,"district"));
    }

    function integralChartData($data){
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(integral_time)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(integral_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(integral_time)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(integral_time)<='".$data['end_time']."'";
                }
            }
        }
        # 新增积分
        $add_integral = Db::table("log_integral")->where(array("integral_type"=>1))->where($where)->sum("integral_number");
        # 使用积分
        $use_integral = Db::table("log_integral")->where(array("integral_type"=>0))->where($where)->sum("integral_number");
        $type_array = lang("integral_source_type");
        unset($type_array[5]);
        $return_array=array();
        foreach($type_array as $k=>$v){
            $return_array[$k]=array("title"=>$v,"value"=>0,"ratio"=>0,"integral_source"=>$k);
        }
        # 查询数据
        $list = Db::table("log_integral")->field("sum(integral_number) num,integral_source")->where("integral_type=1 and integral_source!=5")->where($where)->group("integral_source")->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                $return_array[$v['integral_source']]['value']=intval($v['num']);
                $return_array[$v['integral_source']]['ratio']=getsPriceFormat(intval($v['num'])/$add_integral*100);
            }
        }
        return array("data"=>array_values($return_array),"title"=>array_values($type_array),"add_integral"=>$add_integral,"use_integral"=>$use_integral);
    }

    function addIntegralDetail($data)
    {
        $where = " lg.id > 0 " ;
        if(!empty($data['integral_source'])){
            $where.=" and lg.integral_source = {$data['integral_source']}";
        }
        if(empty($data['param'])) {
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(integral_time)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(integral_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(integral_time)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(integral_time)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(integral_time)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(integral_time)<='".$data['param']['end_time']."'";
            }
        }

        # 新增积分
        $list = Db::table("log_integral lg")
            ->field("lg.member_id,lg.integral_number,lg.integral_source,lg.integral_time,
             m.nickname,m.member_name,m.member_phone,mm.member_level_id,mm.member_integral,mm.member_expiration")
            ->leftJoin("member m","m.id=lg.member_id")
            ->leftJoin("member_mapping mm","mm.member_id = m.id")
            ->where(array("integral_type"=>1))
            ->where($where)
            ->order("integral_time desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }else{
                    $list['data'][$k]['level_title'] = "普通用户";
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
                $list['data'][$k]["integral_source_title"] =getIntegralType($v['integral_source']);
            }
        }
        return $list;

    }

    function useIntegralDetail($data)
    {
        $where = " lg.id > 0 " ;
        if(empty($data['param'])) {
            if(!empty($data['switchStatic'])){
                if($data['switchStatic']<5){
                    switch ($data['switchStatic']){
                        case 1 :
                            $where .= " and date(integral_time)='".date("Y-m-d")."'";
                            break;
                        case 2 :
                            $where .= " and date(integral_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                            break;
                        case 3 :
                            $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m")."'";
                            break;
                        case 4 :
                            $where .= " and date_format(integral_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                            break;
                    }
                }else{
                    if(!empty($data['start_time'])){
                        $where.=" and date(integral_time)>='".$data['start_time']."'";
                    }
                    if(!empty($data['end_time'])){
                        $where.=" and date(integral_time)<='".$data['end_time']."'";
                    }
                }
            }
        }else{
            if(!empty($data['param']['start_time'])){
                $where.=" and date(integral_time)>='".$data['param']['start_time']."'";
            }
            if(!empty($data['param']['end_time'])){
                $where.=" and date(integral_time)<='".$data['param']['end_time']."'";
            }
        }

        # 使用积分
        $list = Db::table("log_integral lg")
            ->field("lg.member_id,lg.integral_number,lg.integral_time,
             m.nickname,m.member_name,m.member_phone,mm.member_level_id,mm.member_integral,mm.member_expiration,ie.exchange_title")
            ->leftJoin("member m","m.id=lg.member_id")
            ->leftJoin("member_mapping mm","mm.member_id = m.id")
            ->leftJoin("integral_exchange ie","ie.id=lg.biz_pro_id")
            ->where(array("integral_type"=>0))
            ->where($where)
            ->order("integral_time desc")
            ->paginate(10, false, ['query' => request()->param()])
            ->toArray();
        if(!empty($list['data'])){
            foreach($list['data'] as $k=>$v){
                # 判断会员是否过期
                if ($v['member_level_id'] == 1 or $v['member_level_id'] == 2) {
                    # 判断是否过期
                    if ($v['member_expiration'] < date("Y-m-d")) {
                        $v['member_level_id'] = 0;
                        $v['expiration_status'] = 2;
                    }
                }
                if($v['member_level_id']>0){
                    $list['data'][$k]['level_title'] = LevelService::memberLevelInfo($v['member_level_id'])['level_title'];
                }else{
                    $list['data'][$k]['level_title'] = "普通用户";
                }
                # 判断名称
                $list['data'][$k]["member_title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];
            }
        }
        return $list;

    }

    function dayIncomeData($data)
    {
        $where = " id > 0 " ;
        if(!empty($data['switchStatic'])){
            if($data['switchStatic']<5){
                switch ($data['switchStatic']){
                    case 1 :
                        $where .= " and date(pay_create)='".date("Y-m-d")."'";
                        break;
                    case 2 :
                        $where .= " and date(pay_create)='".date("Y-m-d",strtotime("-1 day"))."'";
                        break;
                    case 3 :
                        $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m")."'";
                        break;
                    case 4 :
                        $where .= " and date_format(pay_create,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                        break;
                }
            }else{
                if(!empty($data['start_time'])){
                    $where.=" and date(pay_create)>='".$data['start_time']."'";
                }
                if(!empty($data['end_time'])){
                    $where.=" and date(pay_create)<='".$data['end_time']."'";
                }
            }
        }
        # 计算总收入
        $total = Db::table("biz_income")
            ->where("income_type in (12,13,14,3,15,16,17)")
            ->where($where)->sum("biz_income");
        return array("total"=>getsPriceFormat($total));
    }

}
