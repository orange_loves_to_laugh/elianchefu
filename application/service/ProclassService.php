<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/17
 * Time: 13:54
 */

namespace app\service;


use think\Db;
use think\Session;

class ProclassService
{
    /**
     * @param $type
     * @param int $format
     * @return array|mixed|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取所有商品分类
     */
    static function getsProClass($type,$format=1)
    {
        $class_array=Session("proClass".$type.$format);
        if(empty($class_array)){
            $class_array=self::systemProClass($type,$format);
            Session("proClass".$type.$format,$class_array);
        }
        return $class_array;
    }

    /**
     * @param $type
     * @param int $format
     * @param array $list
     * @param int $number
     * @param int $id
     * @return array|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商品分类格式化
     */
    private static function systemProClass($type,$format=1,&$list = array(), $number = 0, $id = 0)
    {
        $number++;
        $result = Db::table('pro_class')->field("id value,class_title label,class_id,status")
                        ->where(array('class_id' => $id, 'status' => 1,"class_type"=>$type))->order('id asc')->select();
        if (!empty($result)) {
            foreach ($result as $k => $v) {
                if($format==1){
                    $v['label']=str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', intval($number - 1)) . $v['label'];
                    $list[]=$v;
                    $_return=$list;
                }elseif($format==2){
                    array_push($list,$v);
                    if(!isset($list[$k]['children'])){
                        $list[$k]['children']=array();
                    }
                    $_return=$list[$k]['children'];
                }
                self::systemProClass($type,$format,$_return, $number, $v['id']);
            }
        } else {
            return null;
        }
        return $list;
    }

    /**
     * @param $type  商品分类类型
     * @return bool
     * @context 更新类型数组
     */
    static function updateProClass($type)
    {
        $_format_array=array(1,2);
        foreach($_format_array as $k=>$v){
            if(!empty(Session("proClass".$type.$v))){
                Session("proClass".$type.$v,null);
            }
        }
        return true;
    }

}