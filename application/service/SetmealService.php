<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/14
 * Time: 13:40
 */

namespace app\service;
/**
 * 套餐管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

namespace app\service;

use app\api\ApiService\LevelService;
use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Session;

class SetmealService
{
    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function SetmealIndex($params)
    {

        #where条件  写成活的
        $where = empty($params['where']) ? ['ml.id' > 0] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;


        #应用管理列表信息
        $data = Db::table('member_level ml')
            ->field('ml.giving_integral,ml.id ,ml.level_title,ml.level_price,ml.level_content level_statement,
            ml.level_time,ml.level_practical,ml.level_img,ml.level_limit,ml.car_limit,l.level_content level_context,
            l.level_send,l.level_privilege,ml.lineup_car_limit,ml.extra_integral_time,ml.extra_giving_integral')
            ->join('level_content l', 'ml.id = l.level_id', 'left')
            ->where($where);

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data = $data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        #处理方法
        return self::InforDataDealWith($data, $params);
    }

    /*
    * @content ：处理方法
    * */
    static function InforDataDealWith($data, $params)
    {
//        dump($data);die;
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if ($v['id'] == 1 or $v['id'] == 2) {
                    $data[$k]['level_limit_title'] = $v['level_time'] . '天';


                } else {
                    $data[$k]['level_limit_title'] = '永久';

                }


            }
        }
        return $data;
    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function SetmealIndexCount()
    {
        return (int)Db::name('member_level')->count();
    }

    /**
     * [SetmealSave 套餐详情 折扣查询]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function SetmealDiscount($levelId)
    {
        $member_discount = array();
        $data = Db::table('level_discount')->where(array('level_id' => $levelId))->select();
        foreach ($data as &$v) {
            if ($v['type'] == 1) {
                #商品
                $member_discount['pro_discount'] = $v['discount'];
                $member_discount['pro_discount_id'] = $v['id'];

            } else if ($v['type'] == 2) {
                #服务
                $member_discount['service_discount'] = $v['discount'];
                $member_discount['service_discount_id'] = $v['id'];

            } else if ($v['type'] == 3) {
                #轮胎
                $member_discount['lt_discount'] = $v['discount'];
                $member_discount['lt_discount_id'] = $v['id'];

            } else if ($v['type'] == 4) {
                #洗车
                $member_discount['car_discount'] = $v['discount'];
                $member_discount['car_discount_id'] = $v['id'];

            } else if ($v['type'] == 5) {
                #喷漆
                $member_discount['pq_discount'] = $v['discount'];
                $member_discount['pq_discount_id'] = $v['id'];

            } else if ($v['type'] == 6) {
                #电瓶
                $member_discount['dp_discount'] = $v['discount'];
                $member_discount['dp_discount_id'] = $v['id'];

            } else if ($v['type'] == 7) {
                #玻璃
                $member_discount['bl_discount'] = $v['discount'];
                $member_discount['bl_discount_id'] = $v['id'];

            }
        }
        return $member_discount;


    }

    /**
     * [SetmealSave 套餐详情 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function SetmealSave($params)
    {
        //套餐说明 是会员卡表的 level_content
        //会员特权  会员卡赠送， 套餐详情 是会员卡详情表 level_privilege  level_send level_content
        //套餐说明 是会员卡表的 level_content
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'level_title',
                'error_msg' => '会员卡名称不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'level_price',
                'error_msg' => '会员卡价格不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'level_practical',
                'error_msg' => '会员卡实际价格不能为空',
                'error_code' => 30002,
            ],
            /* [
                 'checked_type'      => 'empty',
                 'key_name'          => 'car_limit',
                 'error_msg'         => '车辆限制数量',
                 'error_code'         => 30002,
             ],*/
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'level_send',
//                'error_msg'         => '请填写会员卡赠送说明',
//                'error_code'         => 30003,
//            ],
//            [
//                'checked_type' => 'empty',
//                'key_name' => 'level_statement',
//                'error_msg' => '请填写套餐说明',
//                'error_code' => 30003,
//            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'pro_discount',
                'error_msg' => '请填写会员商品折扣',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'service_discount',
                'error_msg' => '请填写会员服务折扣',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'lt_discount',
                'error_msg' => '请填写会员轮胎折扣',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'car_discount',
                'error_msg' => '请填写会员洗车折扣',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'pq_discount',
                'error_msg' => '请填写会员喷漆折扣',
                'error_code' => 30003,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        #处理百度编辑器删除多余图片问题（上传了但是没有点击保存 或者删除了 又重新上传）
        $data = self::HandleData($params);

        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        }
        if (isset($data['oldlevel_img'])) {
            unset($data['oldlevel_img']);
        }

        // 添加/编辑
        if (empty($params['id'])) {


        } else {
            #修改会员折扣
            #商品折扣
            self::SetmealLevelDiscount($params, 1, $params['pro_discount']);
            #服务折扣
            self::SetmealLevelDiscount($params, 2, $params['service_discount']);
            #轮胎折扣
            self::SetmealLevelDiscount($params, 3, $params['lt_discount']);
            #洗车折扣
            self::SetmealLevelDiscount($params, 4, $params['car_discount']);
            #喷漆折扣
            self::SetmealLevelDiscount($params, 5, $params['pq_discount']);
            #电瓶折扣
            self::SetmealLevelDiscount($params, 6, $params['dp_discount']);
            #玻璃折扣
            self::SetmealLevelDiscount($params, 7, $params['bl_discount']);


            #套餐说明字段
            $data['level_content'] = $data['level_statement'];

            if (isset($data['level_context'])) {
                unset($data['level_context']);
            }
            unset(
                $data['level_send'],
                $data['level_privilege'],
                $data['level_statement'],
                $data['member_privilege'],
                $data['pro_discount'],
                $data['service_discount'],
                $data['lt_discount'],
                $data['car_discount'],
                $data['pq_discount'],
                $data['dp_discount'],
                $data['bl_discount']
            );
            //dump($data);exit;
            #修改
            Db::table("member_level")->where(array('id' => $params['id']))->update($data);
            Db::table("level_content")->where(array('level_id' => $params['id']))->update(
                array(
                    'level_content' => $params['level_context'],
                    'level_send' => $params['level_send'],
                    'level_privilege' => implode(',', $params['member_privilege'])
                ));

            #获取缓存
            $activeType = Session('ActiveTypeInfo');
            $activeGive = Session('ActiveGiveInfo');


            if (!empty($activeType)) {
                foreach ($activeType as $k => $v) {
                    $activeType[$k]['active_id'] = $params['id'];
                    unset($activeType[$k]['title']);

                }
                Db::table('active_detail')->insertAll($activeType);
            }

            # 获取推荐赠送
            $recommendSession = Session("recommendPresent");
            if (!empty($recommendSession)) {
                Db::table("level_recommend")->insertAll($recommendSession);
                Session::delete("recommendPresent");
            }

        }
        $_redis = new Redis();
        #删除前端 缓存会员折扣类型
        $_redis->delete("servicePrice");
        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'level_content' . $params['id'];
        #删除单图
        ResourceService::delCacheItem($params['level_img']);
        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('levelId');
        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('ActiveTypeInfo');
        return json(DataReturn('保存成功', 0));


    }

    #会员折扣 按类型 添加 修改  $type ;折扣类型   1 商品 2 服务 3 轮胎 4 洗车 5 喷漆
    static function SetmealLevelDiscount($params, $type, $level_discount = '')
    {

        #计算会员折扣表是否有 有 修改  没有 则添加
        $discount = Db::table('level_discount')->where(array('level_id' => $params['id'], 'type' => $type))->find();

        if (!empty($discount)) {
            #修改对应的折扣
            $a = Db::table('level_discount')->where(array('level_id' => $params['id'], 'type' => $type))->update(array('discount' => $level_discount));
        } else {
            #添加新的折扣
            Db::table('level_discount')->where(array('level_id' => $params['id'], 'type' => $type))->insert(array(
                'discount' => $level_discount,
                'level_id' => $params['id'],
                'type' => $type,
            ));
        }
        return array('status' => true);
    }

    /**
     * 保存前数据处理(编辑器图片)
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    public static function HandleData($params)
    {

        if (isset($params["lelvel_context"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'lelvel_content';
            $params['ueditor_content'] = $params['lelvel_context'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params["privilege_content"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'privileget';
            $params['ueditor_content'] = $params['privilege_content'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params['thumb_img'])) {
            unset($params['thumb_img']);
        }

        return $params;
    }


    /*
     * 显示会员套餐列表
     * */
    static function getSetmealList($level_id)
    {
        $result = Db::table("setmeal")->where(array("level_id" => $level_id))->select();
        $str = null;
        if (!empty($result)) {

            foreach ($result as $v) {
                $str .= "<div style=\"cursor: pointer;padding:0 10px;height:2rem;border: solid 1px #000000;border-radius: 10px;text-align: center;line-height: 2rem;margin-right:2rem;color: #f2f2f2;\" onclick='checkSetMeal(\"{$v['id']}\")'>{$v['title']}</div>";
            }
        }
        return array('status' => true, 'param' => $str);

    }

    /*
      * @access public
      * @return bool
      * @context 显示套餐详情
      * */
    static function checkSetMeal($setMeal_id)
    {
        if (!empty($setMeal_id)) {
            # 查询套餐名称
            $setMealtitle = Db::table("setmeal")->field(array('title'))->where(array("id" => $setMeal_id))->select();
            # 查询套餐详情
            $detail = Db::table("level_giving")->where(array("setmeal_id" => $setMeal_id))->select();
            //dump($detail);exit;
            $_str = '';
            if (!empty($setMeal_id)) {
                $_str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>奖励名称</th>
                                                <th>抵值金额</th>
                                                <th>消费标准</th>
                                                <th>有效期(天)</th>
                                                <th>类型</th>
                                                <th>是否当天使用</th>
                                                <th>平台承担比例</th>
                                                <th>赠送数量</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                if (!empty($detail) and is_array($detail)) {
                    foreach ($detail as $k => $v) {
                        # 消费标准
                        $min_consume = '--';
                        # 有效期
                        $validity_day = '--';
                        # 是否当天使用
                        $voucher_start = '--';
                        # 平台承担比例
                        $platform_ratio = 0;
                        if ($v['giving_type'] == 1) {
                            # 类型
                            $_active_type_title = "商品卡券";
                            # 抵值金额
                            $intbal = sprintf("%.2f", $v['intbal']);
                            # 名称
                            $title = getActiveTitle($v['giving_type'], $v['giving_id']);
                        } else if ($v['giving_type'] == 2) {
                            $_active_type_title = "服务卡券";
                            $intbal = sprintf("%.2f", $v['intbal']);
                            $title = getActiveTitle($v['giving_type'], $v['giving_id']);
                        } else if ($v['giving_type'] == 3) {
                            $_active_type_title = "红包卡券";
                            $intbal = sprintf("%.2f", $v['intbal']);
                            $title = Db::name('redpacket')->where('id=' . $v['giving_id'])->field('packet_title title')->find();
                        } else if (in_array($v['giving_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                            $_active_type_title = givingTypeTitle($v['giving_type']);
                            $intbal = priceFormat($v['intbal']);
                            $name = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], "merchantVoucher", false);
                            $title['title'] = $name['title'];
                            $validity_day = $name['validity_day'];
                            $min_consume = priceFormat($name['min_consume']);
                            $voucher_start = $name['voucher_start'] == 1 ? '是' : '否';
                            $platform_ratio = ($name['platform_ratio'] * 100) . '%';
                            # 查询对应的服务分类
                            $first = getServiceClassTitle($name['cate_pid']);
                            if (!empty($first)) {
                                $_active_type_title .= '-' . $first;
                            }
                            $second = getServiceClassTitle($name['second_cate_pid']);
                            if (!empty($second)) {
                                $_active_type_title .= '-' . $second;
                            }
                        }

                        $_str .= "<tr id='setmealDetailTr" . $v['id'] . "'>
                            <td >" . $title['title'] . "</td>
                            <td >$intbal</td>
                            <td >$min_consume</td>
                            <td >$validity_day</td>
                            <td >$_active_type_title</td>
                            <td >$voucher_start</td>
                            <td >$platform_ratio</td>
                             <td onclick=\"upgradeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['giving_number'] . "','DB','4','" . $setMeal_id . "')\">" . $v['giving_number'] . "</td>
                            
                            <td>
                            <button class=\"layui-btn layui-btn-sm\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','DB','" . $setMeal_id . "')\">删除</button>
                            </td>
                        </tr>";
                    }
                }

                $_str .= "
                </tbody>
            </table>
        </div>
            </div>
             <div style=\"display: flex;justify-content: center;height:10%;align-items: center\">
            
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送汽车服务类消费券\" onclick=\"showAddVoucherFirst(7,'',$setMeal_id)\" style=\"margin-left:2px;\">
            
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送生活服务类消费券\" onclick=\"showAddConsumpVoucher(7,'',$setMeal_id)\" style=\"margin-left:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-danger layui-btn-sm\" value=\"套餐删除\" onclick=\"setmealDel($setMeal_id)\" style=\"margin-left:2px;\">
			
        </div>";
            }
// <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送红包\" onclick=\"activeGiveService('red', $setMeal_id)\" >
//            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送商品\" onclick=\"activeGiveService('pro',$setMeal_id)\" style=\"margin-left:2px;\">
            return array("status" => true, 'param' => $_str, 'title' => $setMealtitle[0]['title']);
        }
    }
    /********************* 套餐特权 start ************************************/
    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function PrivilegeIndex($params)
    {

        #where条件  写成活的
        $where = empty($params['where']) ? ['ml.id' > 0] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('privilege')
            ->where($where)->order('id desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
        } else {
            $data = $data->select();
        }
        #处理方法
        return $data;
//        return self::InforDataDealWith($data,$params);
    }

    /**
     * [PrivilegeSave 套餐特权详情 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function PrivilegeSave($params)
    {
        //套餐说明 是会员卡表的 level_content
        //会员特权  会员卡赠送， 套餐详情 是会员卡详情表 level_privilege  level_send level_content
        //套餐说明 是会员卡表的 level_content
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'title',
                'error_msg' => '特权名称不能为空',
                'error_code' => 30002,
            ],
            /* [
                 'checked_type'      => 'empty',
                 'key_name'          => 'privilege_mark',
                 'error_msg'         => '请选择特权标识',
                 'error_code'         => 30002,
             ],*/

            [
                'checked_type' => 'empty',
                'key_name' => 'privilege_content',
                'error_msg' => '请填写特权详情',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'biztypemark',
                'error_msg' => '请选择门店使用类型',
                'error_code' => 30003,
            ],

        ];
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'privilege_icon',
                'error_msg' => '请上传特权图标',
                'error_code' => 30003,
            ];

        }

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        #处理百度编辑器删除多余图片问题（上传了但是没有点击保存 或者删除了 又重新上传）
        $data = self::HandleData($params);

        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        }
        if (isset($data['oldprivilege_icon'])) {
            unset($data['oldprivilege_icon']);
        }
        if (isset($data['biztypemark'])) {
            $data['biztypemark'] = implode(',', $data['biztypemark']);

        }

        // 添加/编辑
        if (empty($params['id'])) {
            $privilegeId = Db::table('privilege')->insertGetId($data);
            #获取缓存
            $PrivilegePrizeInfo = Session('PrivilegePrizeInfo');
            if (!empty($PrivilegePrizeInfo)) {
                foreach ($PrivilegePrizeInfo as $k => $v) {
                    $PrivilegePrizeInfo[$k]['privilege_id'] = $privilegeId;
                    $PrivilegePrizeInfo[$k]['service_id'] = $PrivilegePrizeInfo[$k]['pro_service_id'];
                    $PrivilegePrizeInfo[$k]['price'] = $PrivilegePrizeInfo[$k]['detail_price'];
                    if (isset($v['pro_service_id']))
                        unset($PrivilegePrizeInfo[$k]['pro_service_id']);
                    if (isset($v['detail_price']))
                        unset($PrivilegePrizeInfo[$k]['detail_price']);
                    if (isset($v['prize_num']))
                        unset($PrivilegePrizeInfo[$k]['prize_num']);
                    if (isset($v['title']))
                        unset($PrivilegePrizeInfo[$k]['title']);

                }
                #特权关联服务表 表
                Db::table('relevance_service')->insertAll($PrivilegePrizeInfo);
            }


        } else {
            #套餐说明字段


            #修改
            Db::table("privilege")->where(array('id' => $params['id']))->update($data);
#获取缓存
            $PrivilegePrizeInfo = Session('PrivilegePrizeInfo');
            if (!empty($PrivilegePrizeInfo)) {
                foreach ($PrivilegePrizeInfo as $k => $v) {
                    $PrivilegePrizeInfo[$k]['privilege_id'] = $params['id'];
                    $PrivilegePrizeInfo[$k]['service_id'] = $PrivilegePrizeInfo[$k]['pro_service_id'];
                    $PrivilegePrizeInfo[$k]['price'] = $PrivilegePrizeInfo[$k]['detail_price'];
                    if (isset($v['pro_service_id']))
                        unset($PrivilegePrizeInfo[$k]['pro_service_id']);
                    if (isset($v['detail_price']))
                        unset($PrivilegePrizeInfo[$k]['detail_price']);
                    if (isset($v['prize_num']))
                        unset($PrivilegePrizeInfo[$k]['prize_num']);
                    if (isset($v['title']))
                        unset($PrivilegePrizeInfo[$k]['title']);

                }
                #先删除 在存
                Db::table('relevance_service')->where(array('privilege_id' => $params['id']))->delete();
                #特权关联服务表 表
                Db::table('relevance_service')->insertAll($PrivilegePrizeInfo);
            }
        }

        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'privilege' . $params['id'];
        #删除单图
        ResourceService::delCacheItem($params['privilege_icon']);
        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('privilegeId');
        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('PrivilegePrizeInfo');
        return json(DataReturn('保存成功', 0));


    }

    /**
     *  privilegeSession 服务  商品  会员 积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function privilegeSession($params, $privilegeId)
    {
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
        if ($type == 'service') {
            unset($params['prize_type']);


        }
        $info = $params;
        $info['prize_num'] = 1;
        Session::set('PrivilegePrizeInfo', array($info));
        $_sessionActiveTypeInfo = Session('PrivilegePrizeInfo');
        $_str = '';
        /* if(empty($_sessionActiveTypeInfo)){
             $_sessionActiveTypeInfo = array();
         }
         if(!empty($_sessionActiveTypeInfo)){
             # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
             if (array_key_exists('S' .$type. $info['pro_service_id'], $_sessionActiveTypeInfo)) {
                 $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['prize_num'] = intval($_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['prize_num']) + intval($info['prize_num']);
             } else {

                 $_sessionActiveTypeInfo['S' . $type.$info['pro_service_id']] = $info;
             }
         }else{
             $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']] = $info;

         }

         Session::set('PrivilegePrizeInfo', $_sessionActiveTypeInfo);
         $_str = '';
         # 判断是否存在$privilegeId
         if(!empty($privilegeId)){
             #查询 商品 服务会员  统一放在数组里  返回
             $pro = self::getTaskContent($privilegeId);

             $_str= self::getPrivilegeInfo($pro, 'DB');

         }*/
        /*getActiveArrInfo*/

        $_str .= self::getPrivilegeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getPrivilegeInfo($arr, $mark)
    {

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                $intbal = sprintf("%.2f", $v['detail_price']);

                #判断折扣  和车辆
                if ($v['discount'] == 0) {
                    $discount = '免费';
                } else if ($v['discount'] > 0 and $v['discount'] < 1) {
                    $discount = ($v['discount'] * 100) . '%';
                } else {
                    $discount = '无折扣';
                }
                if ($v['car_impose'] == 0) {
                    $car_impose = '无限制';
                } else {
                    $car_impose = $v['car_impose'] . '辆';
                }

                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['title'] . "</td>
                            <td >$intbal</td>
                            <td >$discount</td>
                            <td >$car_impose</td>
                             
                            <td>
                                <button class=\"layui-btn\" type=\"button\" onclick=\"delPrivilegeInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /**
     * @param $type 1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function getPrivilegeTypeList()
    {
        #获取缓存活动id 只有修改时候有
        $privilegeId = Session('privilegeId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('PrivilegePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在 $privilegeId
        if (!empty($privilegeId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($privilegeId);


            $_str = self::getPrivilegeInfo($pro, 'DB');

        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getPrivilegeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $privilegeId 奖励id
     * @content 会升级关联服务奖励详情
     */
    static function getTaskContent($privilegeId)
    {
        $task_prize = Db::table('relevance_service rs')
            ->field('rs.*,s.service_title title')
            ->join('service s', 's.id = rs.service_id', 'left')
            ->where(array('privilege_id' => $privilegeId))
            ->select();
        if (!empty($task_prize)) {
            foreach ($task_prize as $k => $v) {
                $task_prize[$k]['discout_price'] = $v['discount'] * 100;
            }
        }
        return $task_prize;

    }

    /**
     *  upgradeSession 服务  商品  积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function setmealSession($params, $setMeal_id)
    {
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
        if ($type == 'pro') {
            #商品
            $info['giving_id'] = $params['pro_service_id'];

            $info['title'] = $params['title'];
            #奖励 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 1服务
            $info['giving_type'] = 1;
            #奖品数量
            $info['giving_number'] = $params['giving_number'];

        } else if ($type == 'service') {
            $info['giving_id'] = $params['pro_service_id'];
            #服务
            $info['title'] = $params['title'];
            #奖励 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 52服务
            $info['giving_type'] = 2;
            #奖品数量
            $info['giving_number'] = $params['giving_number'];
        } else if ($type == 'merchantVoucher' or $type == 'lifeVoucher' or $type == "propertyVoucher" or $type == "youpaiVoucher") {
            $arr = array("title" => $params['title'], "price" => $params['price'], "min_consume" => $params['min_consume'],
                "num" => 1, "validity_day" => $params['validity_day'], "is_type" => $params['is_type'], "cate_pid" => $params['cate_pid'], "voucher_start" => $params['voucher_start']);
            if ($params['is_type'] == 2) {
                $arr['second_cate_pid'] = $params['second_cate_pid'];
            }
            $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
            $giving_types = $params['is_type'] == 2 ? 7 : ($params['is_type'] == 3 ? 9 : ($params['is_type'] == 4 ? 10 : 8));
            unset($params['price'], $params['min_consume'], $params['num'], $params['validity_day'], $params['days_id'], $params['prize_type'], $params['card_time'],
                $params['is_type'], $params['voucher_start'], $params['cate_pid'], $params['active_types'], $params['second_cate_pid']
            );
            $info = $params;
            $info['giving_type'] = $giving_types;
            $info['giving_id'] = $giving_id;
        } else if ($type == 'addVoucher') {
            # 添加汽车服务类型卡券  11-服务通用 12-商品通用 13-全部通用 14-服务分类券 15-商品分类券 16-全部分类券
            $arr = array("title" => $params['title'], "price" => $params['price'], "min_consume" => $params['min_consume'],
                "num" => 1, "validity_day" => $params['validity_day'], "is_type" => $params['is_type'],
                "voucher_start" => $params['voucher_start'], 'platform_ratio' => $params['platform_ratio']);
            if ($params['is_type'] == 14 or $params['is_type'] == 15 or $params['is_type'] == 16) {
                $arr['cate_pid'] = $params['cate_pid'];
                $arr['second_cate_pid'] = $params['second_cate_pid'];
            }
            $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
            $giving_types = $params['is_type'];
            unset($params['price'], $params['min_consume'], $params['num'], $params['validity_day'], $params['days_id'],
                $params['prize_type'], $params['card_time'],
                $params['is_type'], $params['voucher_start'], $params['cate_pid'], $params['active_types'], $params['second_cate_pid'], $params['platform_ratio']
            );
            $info = $params;
            $info['giving_type'] = $giving_types;
            $info['giving_id'] = $giving_id;
        }
        # 判断是否存在$upgradeId  存在直接 修改数据库
        if (!empty($setMeal_id)) {
            unset($info['title']);
            $info['setmeal_id'] = $setMeal_id;
            #赠送 表
            $a = Db::table('level_giving')->insert($info);
            return array('status' => true);

        }


    }

    /********************* 套餐特权 end ************************************/
    /********************* 会员升级 start ************************************/
    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function UpgradeIndex($params)
    {

        #where条件  写成活的
        $where = empty($params['where']) ? ['id' > 0] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('upgrade')->where($where);

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data = $data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        #处理方法
        return self::UpgradeInforDataDealWith($data, $params);
    }

    /*
    * @content ：处理方法
    * */
    static function UpgradeInforDataDealWith($data, $params)
    {
        if (!empty($data)) {
            foreach ($data as &$v) {
                $v['now_level'] = getsMemberLevelTitle($v['now_level']);
                $v['up_level'] = getsMemberLevelTitle($v['up_level']);
                $v['giving_num'] = Db::table('upgrade_giving')->where(array('upgrade_id' => $v['id']))->count();

            }
        }
        return $data;
    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function UpgradeIndexCount($where)
    {
        return (int)Db::name('upgrade')->where($where)->count();
    }

    /**
     * [PrivilegeSave 套餐特权详情 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function UpgradeSave($params)
    {
        //套餐说明 是会员卡表的 level_content
        //会员特权  会员卡赠送， 套餐详情 是会员卡详情表 level_privilege  level_send level_content
        //套餐说明 是会员卡表的 level_content
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'now_level',
                'error_msg' => '请选择当前等级',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'up_level',
                'error_msg' => '请选择升级后等级',
                'error_code' => 30002,
            ],


            [
                'checked_type' => 'empty',
                'key_name' => 'price',
                'error_msg' => '请填写升级价格',
                'error_code' => 30003,
            ],


        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }


        // 添加/编辑
        if (empty($params['id'])) {
            $upgradeId = Db::table('upgrade')->insertGetId($data);
            #获取缓存
            $PrivilegePrizeInfo = Session('UpgradePrizeInfo');
            if (!empty($PrivilegePrizeInfo)) {
                #先看赠送的是什么类型  如果 是服务卡券  商品卡券  需要 先存入  card_voucher  表   在存upgrade_giving
                foreach ($PrivilegePrizeInfo as $k => $v) {
                    $PrivilegePrizeInfo[$k]['upgrade_id'] = $upgradeId;
                    if (isset($v['title']))
                        unset($PrivilegePrizeInfo[$k]['title']);
                    if ($v['giving_type'] == 2) {
                        #服务  添加服务卡券
                        #类型为卡券时 上 card_voucher  查下卡券id 存上card_type_id=1 服务
                        $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 1, 'server_id' => $params['pro_service_id']))->value('id');

                    } else if ($v['giving_type'] == 1) {
                        #商品 card_type_id=3 是商品
                        $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 3, 'server_id' => $params['pro_service_id']))->value('id');
                    }

                    $PrivilegePrizeInfo[$k]['giving_id'] = $voucher_id;
                }
                #任务奖励 表
                Db::table('upgrade_giving')->insertAll($PrivilegePrizeInfo);
            }
        }


        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('levelId');
        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('ActiveTypeInfo');
        return json(DataReturn('保存成功', 0));


    }

    /**
     *  upgradeSession 服务  商品  积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function upgradeSession($params, $upgradeId)
    {
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
        $voucher_id = 0;
        if ($type == 'pro') {
            #商品
            $info['giving_id'] = $params['pro_service_id'];
            $info['title'] = $params['title'];
            #奖励 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 1服务
            $info['giving_type'] = 1;
            #奖品数量
            $info['giving_number'] = $params['giving_number'];
            //有效期
            $info['card_time'] = $params['card_time'];
        } else if ($type == 'service') {
            $info['giving_id'] = $params['pro_service_id'];
            #服务
            $info['title'] = $params['title'];
            #奖励 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 52服务
            $info['giving_type'] = 2;
            #奖品数量
            $info['giving_number'] = $params['giving_number'];
            //有效期
            $info['card_time'] = $params['card_time'];
        } else if ($type == 'integral') {
            #名称
            $info['title'] = '积分赠送';
            #奖励类型 3 积分
            $info['giving_type'] = 3;
            #数量 积分
            $info['intbal'] = $params['val'];
            #赠送数量 默认为1
            $info['giving_number'] = 1;
            #赠送的卡券id
            $info['giving_id'] = 0;

        } else if ($type == 'balance') {
            #余额 $id 是传过来的余额
            #名称
            $info['title'] = '余额赠送';
            #奖励类型 4 余额
            $info['giving_type'] = 4;
            #数量
            $info['intbal'] = $params['val'];
            #赠送数量
            $info['giving_number'] = 1;
            #赠送的卡券id
            $info['giving_id'] = 0;
        } else if ($type == 'merchantVoucher' or $type == 'lifeVoucher' or $type == "propertyVoucher" or $type == "youpaiVoucher") {
            $arr = array("title" => $params['title'], "price" => $params['price'], "min_consume" => $params['min_consume'],
                "num" => 1, "validity_day" => $params['validity_day'], "is_type" => $params['is_type'], "cate_pid" => $params['cate_pid'],
                "voucher_start" => $params['voucher_start']);
            if ($params['is_type'] == 2) {
                $arr['second_cate_pid'] = $params['second_cate_pid'];
            }
            $voucher_id = Db::table("merchants_voucher")->insertGetId($arr);
            $giving_types = $params['is_type'] == 2 ? 7 : ($params['is_type'] == 3 ? 9 : ($params['is_type'] == 4 ? 10 : 8));
            $validity_day = $params['validity_day'];
            unset($params['price'], $params['min_consume'], $params['num'], $params['validity_day'], $params['days_id'], $params['prize_type'], $params['card_time'],
                $params['is_type'], $params['voucher_start'], $params['cate_pid'], $params['active_types'], $params['second_cate_pid']
            );
            $info = $params;
            $info['giving_type'] = $giving_types;
            $info['giving_id'] = $voucher_id;
            $info['card_time'] = $validity_day;
        } else if ($type == 'addVoucher') {
            # 添加汽车服务类型卡券  11-服务通用 12-商品通用 13-全部通用 14-服务分类券 15-商品分类券 16-全部分类券
            $arr = array("title" => $params['title'], "price" => $params['price'], "min_consume" => $params['min_consume'],
                "num" => 1, "validity_day" => $params['validity_day'], "is_type" => $params['is_type'],
                "voucher_start" => $params['voucher_start'], 'platform_ratio' => $params['platform_ratio']);
            if ($params['is_type'] == 14 or $params['is_type'] == 15 or $params['is_type'] == 16) {
                $arr['cate_pid'] = $params['cate_pid'];
                $arr['second_cate_pid'] = $params['second_cate_pid'];
            }
            $voucher_id = Db::table("merchants_voucher")->insertGetId($arr);
            $giving_types = $params['is_type'];
            $validity_day = $params['validity_day'];
            unset($params['price'], $params['min_consume'], $params['num'], $params['validity_day'], $params['days_id'],
                $params['prize_type'], $params['card_time'],
                $params['is_type'], $params['voucher_start'], $params['cate_pid'], $params['active_types'], $params['second_cate_pid'], $params['platform_ratio']
            );
            $info = $params;
            $info['giving_type'] = $giving_types;
            $info['giving_id'] = $voucher_id;
            $info['card_time'] = $validity_day;
        }
        # 判断是否存在$upgradeId  存在直接 修改数据库
        if (!empty($upgradeId)) {
            unset($info['title']);
            $info['upgrade_id'] = $upgradeId;
            if ($info['giving_type'] == 2) {
                #服务  添加服务卡券
                #类型为卡券时 上 card_voucher  查下卡券id 存上card_type_id=1 服务
                $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 1, 'server_id' => $params['pro_service_id']))->value('id');
            } else if ($info['giving_type'] == 1) {
                #商品 card_type_id=3 是商品
                $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 3, 'server_id' => $params['pro_service_id']))->value('id');
            }
            $info['giving_id'] = $voucher_id;
            //dump($info);exit;
            #赠送 表
            $a = Db::table('upgrade_giving')->insert($info);
            return array('status' => true);
        }
        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上
        $_sessionActiveTypeInfo = Session('UpgradePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        if (!empty($_sessionActiveTypeInfo)) {
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' . $type . $info['giving_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' . $type . $info['giving_id']]['giving_number'] = intval($_sessionActiveTypeInfo['S' . $type . $info['giving_id']]['giving_number']) + intval($info['giving_number']);
            } else {
                $_sessionActiveTypeInfo['S' . $type . $info['giving_id']] = $info;
            }
        } else {
            $_sessionActiveTypeInfo['S' . $type . $info['giving_id']] = $info;
        }
        Session::set('UpgradePrizeInfo', $_sessionActiveTypeInfo);
        $_str = '';


        $_str .= self::getUpgradeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ，积分 余额
     * @return string
     * @content 商品 服务  积分 卡券  信息整理
     */
    static function getUpgradeInfo($arr, $mark, $_upgradeId = '')
    {

        $_str = '';
        if (!empty($_upgradeId)) {
            $_str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                               <th>奖励名称</th>
                                                <th>价值</th>
                                                <th>消费标准</th>
                                                <th>类型</th>
                                                <th>有效期(单位:天)</th>
                                                <th>是否可当天使用</th>
                                                <th>平台承担比例</th>
                                                <th>赠送数量</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='upgradebody'>";
            if (!empty($arr) and is_array($arr)) {
                foreach ($arr as $k => $v) {
                    $min_consume = '--';
                    # 是否当天使用
                    $voucher_start = '--';
                    # 平台承担比例
                    $platform_ratio = '--';
                    if ($v['giving_type'] == 3) {
                        $_active_type_title = "积分";
                        $intbal = $v['intbal'] . '积分';
                        $card_time = '--';
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 4) {
                        $_active_type_title = "余额";
                        $intbal = $v['intbal'] . '余额';
                        $card_time = '--';
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 1) {
                        $_active_type_title = "商品卡券";
                        $intbal = priceFormat($v['intbal']);
                        $card_time = $v['card_time'];
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 2) {
                        $_active_type_title = "服务卡券";
                        $intbal = priceFormat($v['intbal']);
                        $card_time = $v['card_time'];
                        $title = $v['title'];
                    } else if (in_array($v['giving_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                        $_active_type_title = givingTypeTitle($v['giving_type']);
                        $intbal = priceFormat($v['intbal']);
                        $card_time = $v['card_time'];
                        $info = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'merchantVoucher', false);
                        $title = $info['title'];
                        $min_consume = $info['min_consume'];
                        $voucher_start = $info['voucher_start'] == 1 ? '是' : '否';
                        $platform_ratio = ($info['platform_ratio'] * 100) . '%';
                        # 查询对应的服务分类
                        $first = getServiceClassTitle($info['cate_pid']);
                        if (!empty($first)) {
                            $_active_type_title .= '-' . $first;
                        }
                        $second = getServiceClassTitle($info['second_cate_pid']);
                        if (!empty($second)) {
                            $_active_type_title .= '-' . $second;
                        }
                    }
                    $_str .= "<tr id='upgradeDetailTr" . $v['id'] . "'>
                            <td >" . $title . "</td>
                            <td >$intbal</td>
                            <td >$min_consume</td>
                            <td>$_active_type_title</td>
                             <td >$card_time</td>
                             <td >$voucher_start</td>
                             <td >$platform_ratio</td>
                             <td onclick=\"upgradeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['giving_number'] . "','" . $mark . "','4')\">" . $v['giving_number'] . "</td>
                            <td>
                            <button class=\"layui-btn layui-btn-sm\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $_upgradeId . "')\">删除</button>
                            </td>
                        </tr>";
                }
            }

            $_str .= "
                </tbody>
            </table>
        </div>
            </div>
             <div style=\"display: flex;justify-content: center;height:10%;align-items: center\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送汽车服务类消费券\" onclick=\"showAddVoucherFirst(8,'',$_upgradeId)\" style=\"margin-right:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送积分\" onclick=\"activeGiveService('integral',$_upgradeId)\" style=\"margin-left:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送余额\" onclick=\"activeGiveService('balance',$_upgradeId)\" style=\"margin-left:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送生活服务类消费券\" onclick=\"showAddConsumpVoucher(8,'',$_upgradeId)\" style=\"margin-left:2px;\">
			
        </div>";
        } else {
            if (!empty($arr) and is_array($arr)) {
                foreach ($arr as $k => $v) {
                    if ($v['giving_type'] == 3) {
                        $_active_type_title = "积分";
                        $intbal = $v['intbal'] . '积分';
                    } else if ($v['giving_type'] == 4) {
                        $_active_type_title = "余额";
                        $intbal = $v['intbal'] . '余额';


                    } else if ($v['giving_type'] == 1) {
                        $_active_type_title = "商品卡券";
                        $intbal = sprintf("%.2f", $v['intbal']);

                    } else if ($v['giving_type'] == 2) {
                        $_active_type_title = "服务卡券";
                        $intbal = sprintf("%.2f", $v['intbal']);

                    }


                    $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['title'] . "</td>
                            <td >$intbal</td>
                            <td>$_active_type_title</td>
                             <td onclick=\"upgradeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['giving_number'] . "','" . $mark . "','4')\">" . $v['giving_number'] . "</td>
                         
                            <td>
                            <button class=\"layui-btn layui-btn-sm\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
                }

            }

        }

        return $_str;

    }

    #会员赠送列表  添加时  修改时 执行的页面
    static function getUpgradeInfoWrite($arr, $mark, $_upgradeId = '')
    {
        $_str = '';
        if (!empty($_upgradeId)) {
            if (!empty($arr) and is_array($arr)) {
                foreach ($arr as $k => $v) {
                    $min_consume = '--';
                    # 是否当天使用
                    $voucher_start = '--';
                    # 平台承担比例
                    $platform_ratio = '--';
                    if ($v['giving_type'] == 3) {
                        $_active_type_title = "积分";
                        $intbal = $v['intbal'] . '积分';
                        $card_time = '--';
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 4) {
                        $_active_type_title = "余额";
                        $intbal = $v['intbal'] . '余额';
                        $card_time = '--';
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 1) {
                        $_active_type_title = "商品卡券";
                        $intbal = sprintf("%.2f", $v['intbal']);
                        $card_time = $v['card_time'];
                        $title = $v['title'];
                    } else if ($v['giving_type'] == 2) {
                        $_active_type_title = "服务卡券";
                        $intbal = sprintf("%.2f", $v['intbal']);
                        $card_time = $v['card_time'];
                        $title = $v['title'];
                    }  else if (in_array($v['giving_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                        $_active_type_title = givingTypeTitle($v['giving_type']);
                        $intbal = priceFormat($v['intbal']);
                        $card_time = $v['card_time'];
                        $info = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'merchantVoucher', false);
                        $title = $info['title'];
                        $min_consume = $info['min_consume'];
                        $voucher_start = $info['voucher_start'] == 1 ? '是' : '否';
                        $platform_ratio = ($info['platform_ratio'] * 100) . '%';
                        # 查询对应的服务分类
                        $first = getServiceClassTitle($info['cate_pid']);
                        if (!empty($first)) {
                            $_active_type_title .= '-' . $first;
                        }
                        $second = getServiceClassTitle($info['second_cate_pid']);
                        if (!empty($second)) {
                            $_active_type_title .= '-' . $second;
                        }
                    }
                    $_str .= "<tr id='upgradeDetailTr" . $v['id'] . "'>
                            <td >" . $title . "</td>
                           <td >$intbal</td>
                            <td >$min_consume</td>
                            <td>$_active_type_title</td>
                             <td >$card_time</td>
                             <td >$voucher_start</td>
                             <td >$platform_ratio</td>
                             <td onclick=\"upgradeTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['giving_number'] . "','" . $mark . "','4')\">" . $v['giving_number'] . "</td>
                            <td>
                            <button class=\"layui-btn layui-btn-sm\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $_upgradeId . "')\">删除</button>
                            </td>
                        </tr>";
                }
            }

            $_str .= "
               
            
             <div style=\"display: flex;justify-content: center;height:10%;align-items: center\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送服务\" onclick=\"activeGiveService('service',$_upgradeId)\" style=\"margin-right:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送商品\" onclick=\"activeGiveService('pro',$_upgradeId)\" style=\"margin-left:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送积分\" onclick=\"activeGiveService('integral',$_upgradeId)\" style=\"margin-left:2px;\">
            <input type=\"button\" class=\"layui-btn layui-btn-info layui-btn-sm\" value=\"赠送余额\" onclick=\"activeGiveService('balance',$_upgradeId)\" style=\"margin-left:2px;\">
			
        </div>";
        }

        return $_str;

    }

    /**
     * @param $type 1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function getUpgradeList()
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('taskId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('UpgradePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在$activeId
        if (!empty($activeId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($activeId);


            $_str = self::getUpgradeInfo($pro, 'DB');

        }

        $_str .= self::getUpgradeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $upgradeId 奖励id
     * @content 会员升级赠送详情
     */
    static function getUpgradeContent($upgradeId)
    {
        $task_prize = Db::table('upgrade_giving')->where(array('upgrade_id' => $upgradeId))->select();
        foreach ($task_prize as $k => $v) {
            if ($v['giving_type'] == 3) {
                #积分
                $task_prize[$k]['title'] = '积分赠送';
            } else if ($v['giving_type'] == 4) {
                #余额
                $task_prize[$k]['title'] = '余额赠送';
            } else if ($v['giving_type'] == 1) {
                #商品 名称  根据卡券id 查询商品名称
                $task_prize[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'pro');
            } else if ($v['giving_type'] == 2) {
                #服务 名称  根据卡券id 查询服务名称
                $task_prize[$k]['title'] = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], 'service');
            }
        }
        return $task_prize;

    }
    /********************* 会员升级 end ************************************/
    /********************* 会员充值 start ************************************/
    /*
     * @params price 金额 id:recharge表id
     * @params type price:充值金额, giveing:赠送金额
     * @content 修改价格
     * */
    static function UpgradeChangePrice($params)
    {
        if ($params['type'] == 'price') {
            #充值金额
            Db::table('recharge')->where(array('id' => $params['id']))->update(array('price' => $params['price']));
        } else {
            #赠送金额
            Db::table('recharge')->where(array('id' => $params['id']))->update(array('giving' => $params['price']));

        }
        return true;

    }

    /**
     *  rechargeSession 充值信息
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function rechargeSession($params)
    {
        $info = $params;


        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('RechargePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
            Session::set('RechargePrizeInfo', array($info));
        }
        if (!empty($_sessionActiveTypeInfo)) {
            array_push($_sessionActiveTypeInfo, $info);
            Session::set('RechargePrizeInfo', $_sessionActiveTypeInfo);

        }
        $_str = '';
        $_sessionRechare = Session('RechargePrizeInfo');
        $_str .= self::getTaskPrizeInfo($_sessionRechare, 'S');
        return $_str;

    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getTaskPrizeInfo($arr, $mark)
    {

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td onclick=\"taskTypeWrite('" . $k . "','" . $v['price'] . "','" . $mark . "','1')\">" . $v['price'] . "</td>
                            <td onclick=\"taskTypeWrite('" . $k . "','" . $v['giving'] . "','" . $mark . "','2')\">" . $v['giving'] . "</td>
                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /**
     * @param $type 1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    function getRechareTypeList()
    {

        $_sessionActiveTypeInfo = Session('RechargePrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * [RechareSave 会员充值 添加]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function RechareSave($params)
    {
        //套餐说明 是会员卡表的 level_content
        //会员特权  会员卡赠送， 套餐详情 是会员卡详情表 level_privilege  level_send level_content
        //套餐说明 是会员卡表的 level_content
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'level',
                'error_msg' => '请选择当前等级',
                'error_code' => 30002,
            ],
        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }


        // 添加/编辑
        if (empty($params['id'])) {
            #查询缓存
            $_session = session('RechargePrizeInfo');
            if (!empty($_session)) {
                foreach ($_session as &$v) {
                    $v['level'] = $data['level'];
                }

            }
            Db::table('recharge')->insertAll($_session);
        }


        #删除添加 商品 服务 会员 赠送积分 余额  缓存
        Session::delete('RechargePrizeInfo');
        return json(DataReturn('保存成功', 0));


    }

    /**
     * 获取充值列表
     * @param   [array]          $params [输入参数]
     * @version 1.0.0
     * @date    2020年12月14日16:05:44
     * @desc    description
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     */
    public static function RechargeList($params)
    {
        $data = BaseService::DataList($params);
        return self::RechargeDataDealWith($data);

    }

    /**
     * 充值总数
     * @param    [array]          $where [条件]
     * @version 1.0.0
     * @date    2020年12月14日17:11:44
     * @desc    description
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     */
    public static function RechargeTotal($where)
    {
        return (int)Db::name('recharge')->where($where)->group('level')->count();
    }

    /**
     * 级别充值数据处理
     * @param    [array]          $data [处理的数据]
     * @param    [array]          $params [输入参数]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月14日16:23:23
     * @desc    description
     */
    public static function RechargeDataDealWith($data)
    {
        if (!empty($data)) {
            foreach ($data as &$v) {
                $v['recharge_arr'] = Db::name('recharge')->where('level=' . $v['level'])->order('price asc')->select();
                foreach ($v['recharge_arr'] as &$vv) {
                    $vv['price'] = priceFormat($vv['price']);
                    $vv['giving'] = priceFormat($vv['giving']);
                }
                $v['level_title'] = Db::name('member_level')->where('id=' . $v['level'])->value('level_title');
            }
        }
        return DataReturn('ok', 0, $data);
    }
    /********************* 会员充值 end ************************************/
}
