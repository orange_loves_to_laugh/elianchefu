<?php


namespace app\service;


use think\Controller;
use think\Db;

class EalspellCommissionService extends Controller
{
    static function RecommendCommission(){
        return array(
            "member"=>[5,10,15,20,25],
            "user" =>[2,2,2,2,2],
            "agent" => [0,0.2,0.3,0.4,0.5],
            "setmeal" => [0.5,0.6,0.7,0.8,0.9],
            "youpipro" => [0.9,0.1,0.1,0.1,0.1],
            "upgrade"=>[0,0.2,0.3,0.4,0.5],
            "merchants"=>[0,0.2,0.25,0.30,0.35],
            "merchants_upgrade"=>[0,0.1,0.15,0.2,0.25],
            "merchants_consump"=>[0,0.05,0.1,0.15,0.2],
            "first_consump" => 2,
            "second_consump"=>2,
            "third_consump"=>5,
            "twelf_consump"=>38,
            "integral"=>3,
            "member_merchantspro"=>3,
            "sign"=>5,
            "recharge"=>5,
        );
    }

    static function addCommission($type,$price,$commander_id,$order_number=null){
        if($type==2){
            return self::addRegisterCommission($commander_id);
        }elseif ($type==3){
            return self::addBandMember($commander_id);
        }elseif($type==4){
            return self::addMemberConsump($commander_id,$price);
        }elseif($type==5){
            return self::addMerchantsPro($commander_id,$price,$order_number);
        }elseif($type==6){
            return self::updateMerchantsProCommission($order_number);
        }elseif($type==7){

        }
    }

    /**
     * @param $commander_id
     * @return bool
     * @throws \think\Exception
     * @context 添加用户注册佣金
     */
    static function addRegisterCommission($commander_id,$member_id){
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $first_info = Db::connect("ealspell")->table("community_agent")
            ->field("mark,pid")->where(array("id"=>$commander_id,"status"=>2))->find();
        if(!empty($first_info)){
            $first_price = 2;
            array_push($_return_array,array("ragent_id"=>$commander_id,"price"=>$first_price,
                "type"=>1,"create_time"=>$date,"status"=>2,"source"=>5,"cid"=>0,"member_id"=>$member_id,"relation_type"=>1));
            Db::connect("ealspell")->table("community_agent")
                ->where(array("id"=>$commander_id))->setInc("balance",$first_price);
            if(!empty($first_info['pid'])){
                $second_info = Db::connect("ealspell")->table("community_agent")
                    ->field("mark,pid")->where(array("id"=>$first_info['pid'],"status"=>2))->find();
                $second_price = 0.2;
                array_push($_return_array,array("ragent_id"=>$first_info['pid'],"price"=>$second_price,"type"=>11,"create_time"=>$date,
                    "status"=>2,"source"=>5,"cid"=>$commander_id,"member_id"=>$member_id,"relation_type"=>1));
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$first_info['pid']))->setInc("balance",$second_price);
                if($second_info['mark']==4){
                    array_push($_return_array,array("ragent_id"=>$first_info['pid'],"price"=>$second_price,"type"=>11,
                        "create_time"=>$date,"status"=>2,"source"=>5,"cid"=>$commander_id,"member_id"=>$member_id,"relation_type"=>1));
                    Db::connect("ealspell")->table("community_agent")
                        ->where(array("id"=>$first_info['pid']))->setInc("balance",$second_price);
                }else{
                    # 查询上级是否有运营中心
                    $middle_id = $first_info['pid'];
                    for($i=0;$i<4;$i++){
                        $middle_info = Db::connect("ealspell")->table("community_agent")
                            ->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
                        if(!empty($middle_info)){
                            if($middle_info['mark']==4){
                                array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$second_price,"type"=>11,
                                    "create_time"=>$date,"status"=>2,"source"=>5,"cid"=>$commander_id,"member_id"=>$member_id,"relation_type"=>1));
                                Db::connect("ealspell")->table("community_agent")
                                    ->where(array("id"=>$middle_id))->setInc("balance",$second_price);
                                break;
                            }else{
                                if(!empty($middle_info['pid'])){
                                    $middle_id = $middle_info['pid'];
                                    continue;
                                }else{
                                    break;
                                }

                            }
                        }else{
                            break;
                        }
                    }
                }
            }
        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;

    }

    /**
     * @param $commander_id
     * @return bool
     * @throws \think\Exception
     * @context 推荐会员代理得佣金
     */
    static function addBandMember($commander_id,$member_id){
        $arr = self::RecommendCommission()['member'];
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $middle_id = $commander_id;
        $middle_price = 0;
        $type = 2;
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid,point_mark")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                $add_price = getsPriceFormat($arr[$middle_info['mark']]-$middle_price);
                $middle_price = $arr[$middle_info['mark']];
                array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$add_price,"type"=>$type,
                    "create_time"=>$date,"status"=>2,"source"=>2,"member_id"=>$member_id,"relation_type"=>1));
                $type = 10;
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$middle_id))->setInc("balance",$add_price);
                if(!empty($middle_info['pid'])){
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }

        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;

    }

    /**
     * @param $commander_id
     * @param $price
     * @return bool
     * @throws \think\Exception
     * @context 会员消费
     */
    static function addMemberConsump($commander_id,$price,$member_id,$order_number){
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $deduct = 0.01;
        array_push($_return_array,array("ragent_id"=>$commander_id,"price"=>getsPriceFormat($price*$deduct),
            "type"=>8,"create_time"=>$date,"status"=>1,"source"=>9,"member_id"=>$member_id,"relation_type"=>1,
            "order_number"=>$order_number,"cid"=>0));
//        Db::connect("ealspell")->table("community_agent")
//            ->where(array("id"=>$commander_id))->setInc("balance",getsPriceFormat($price*$deduct));
        $middle_id = $commander_id;
        # 上级得千分之一
        $yunying_deduct = 0.001;
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                if(!empty($middle_info['pid'])){
                    array_push($_return_array,array("ragent_id"=>$middle_info['pid'],"price"=>getsPriceFormat($price*$yunying_deduct),
                        "type"=>10,"create_time"=>$date,"status"=>1,"source"=>9,"member_id"=>$member_id,"relation_type"=>1,
                        "order_number"=>$order_number,"cid"=>$middle_id));
//                Db::connect("ealspell")->table("community_agent")
//                    ->where(array("id"=>$middle_id))->setInc("balance",getsPriceFormat($price*$yunying_deduct));
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;

    }

    /**
     * @param $order_number
     * @return bool
     * @throws \think\Exception
     * @context 会员消费完成给佣金+商家给的抽成（商家部分）
     */
    static function finishMemberConsumpCommission($order_number){
        # 查询是否有代理提成
        $info = Db::connect("ealspell")->table("deduct_record")
            ->where(array("order_number"=>$order_number,"status"=>1))
            ->where("type=8 or type=10 or type=9 or type=12 ")
            ->select();
        if(!empty($info)){
            foreach($info as $v){
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$v['ragent_id']))->setInc("balance",getsPriceFormat($v['price']));
                Db::connect("ealspell")->table("deduct_record")
                    ->where(array("id"=>$v['id']))->update(array("status"=>2));
            }
            Db::connect("ealspell")->table("deduct_record")->where(array("order_number"=>$order_number))->update(array("status"=>2));
        }
        return true;

    }

    /**
     * @param $commander_id
     * @param $price
     * @param $order_number
     * @throws \think\Exception
     * @context 卖商家套餐得佣金
     */
    static function addMerchantsPro($commander_id,$price,$order_number,$merchants_id){
        $arr = self::RecommendCommission()['setmeal'];
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $middle_id = $commander_id;
        $middle_price = 0;
        $type = 7;
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid,point_mark")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                $add_price = getsPriceFormat($price*($arr[$middle_info['mark']]-$middle_price));
                $middle_price = $arr[$middle_info['mark']];
                array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$add_price,"type"=>$type,
                    "create_time"=>$date,"status"=>1,"source"=>6,"order_number"=>$order_number,"merchants_id"=>$merchants_id,
                    "relation_type"=>2));
                $type = 10;
                if(!empty($middle_info['pid'])){
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }

        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    static function updateMerchantsProCommission($order_number){
        # 查询是否有代理提成
        $info = Db::connect("ealspell")->table("deduct_record")
            ->where(array("order_number"=>$order_number,"status"=>1))
            ->where("type=7 or type=10")
            ->select();
        if(!empty($info)){
            foreach($info as $v){
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$v['ragent_id']))->setInc("balance",getsPriceFormat($v['price']));
                Db::connect("ealspell")->table("deduct_record")
                    ->where(array("id"=>$v['id']))->update(array("status"=>2));
            }
            Db::connect("ealspell")->table("deduct_record")->where(array("order_number"=>$order_number,"type"=>6,"source"=>6))->update(array("status"=>2));
        }
        return true;

    }

    /**
     * @param $commander_id
     * @param $price
     * @param $merchants_id
     * @return bool
     * @throws \think\Exception
     * @context 商家充值
     */
    static function merchantsCharge($commander_id,$price,$merchants_id)
    {
        $arr = self::RecommendCommission()['merchants'];
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $middle_id = $commander_id;
        $middle_price = 0;
        $type = 4;
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid,point_mark")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                $add_price = getsPriceFormat($price*($arr[$middle_info['mark']]-$middle_price));
                $middle_price = $arr[$middle_info['mark']];
                array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$add_price,"type"=>$type,"create_time"=>$date,"status"=>2,
                    "source"=>10,"pro_id"=>$merchants_id,"merchants_id"=>$merchants_id,"relation_type"=>2));
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$middle_id))->setInc("balance",getsPriceFormat($add_price));
                if(!empty($middle_info['pid'])){
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $commander_id
     * @param $price
     * @param $merchants_id
     * @return bool
     * @throws \think\Exception
     * @context 商家升级
     */
    static function merchantsUpgrade($commander_id,$price,$merchants_id)
    {
        $arr = self::RecommendCommission()['merchants_upgrade'];
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $middle_id = $commander_id;
        $middle_price = 0;
        $type = 5;
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid,point_mark")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                $add_price = getsPriceFormat($price*($arr[$middle_info['mark']]-$middle_price));
                $middle_price = $arr[$middle_info['mark']];
                array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$add_price,"type"=>$type,
                    "create_time"=>$date,"status"=>2,"source"=>10,"pro_id"=>$merchants_id,"merchants_id"=>$merchants_id,"relation_type"=>2));
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$middle_id))->setInc("balance",getsPriceFormat($add_price));
                if(!empty($middle_info['pid'])){
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $commander_id
     * @param $price
     * @param $merchants_id
     * @return bool
     * @throws \think\Exception
     * @context 商家抽成
     *
     */
    static function merchantsConsump($commander_id,$price,$merchants_id,$order_number){
        # 这里的price 已经是抽成金额了不需要*商家抽成比例
        $arr = self::RecommendCommission()['merchants_consump'];
        $_return_array = array();
        $date = date("Y-m-d H:i:s");
        $middle_id = $commander_id;
        $middle_price = 0;
        $type = 9;
        # 查询商家抽成比例
//        $merchantInfo = Db::table("merchants")->field("commission")->where(array("id"=>$merchants_id))->find();
//        if(empty($merchantInfo['commission'])){
//            $merchantInfo['commission'] = 0;
//        }
        for($i=0;$i<=4;$i++){
            $middle_info = Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid,point_mark")->where(array("id"=>$middle_id,"status"=>2))->find();
            if(!empty($middle_info)){
                $add_price = getsPriceFormat($price*($arr[$middle_info['mark']]-$middle_price));
                $middle_price = $arr[$middle_info['mark']];
                if($add_price>0){
                    array_push($_return_array,array("ragent_id"=>$middle_id,"price"=>$add_price,"type"=>$type,
                        "create_time"=>$date,"status"=>1,"source"=>10,"pro_id"=>$merchants_id,
                        "merchants_id"=>$merchants_id,"relation_type"=>2,"order_number"=>$order_number));
                }
                $type = 12;

                if(!empty($middle_info['pid'])){
                    $middle_id = $middle_info['pid'];
                    continue;
                }else{
                    break;
                }
            }else{
                break;
            }

        }
        if(!empty($_return_array)){
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $commander_id
     * @param $merchants_id
     * @return bool
     * @throws \think\Exception
     * @context 商家首次达成销售额给代理加提成
     */
    static function merchantSalesAchieved($commander_id,$merchants_id){
        $price = 20;
        $sale_price = 300;
        $date = date("Y-m-d H:i:s");
        # 查询是否有该类型的记录
        //Db::table("merchants_logconsump")->
        $is_receive = Db::connect("ealspell")->table('deduct_record')
            ->where(array("ragent_id"=>$commander_id,"type"=>29,"pro_id"=>$merchants_id,"source"=>29))->find();
        if(empty($is_receive)){
            # 查询该商家收款是否达成金额
            $merchant_log=Db::table("merchants_logconsump")->where(array("merchants_id"=>$merchants_id))->sum("pract_price");
            if($merchant_log>=$sale_price){
                Db::connect("ealspell")->table('deduct_record')->insert(array("ragent_id"=>$commander_id,"price"=>$price,
                    "type"=>29,"create_time"=>$date,"status"=>2,"source"=>29,"pro_id"=>$merchants_id,
                    "merchants_id"=>$merchants_id,"relation_type"=>2));
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$commander_id))->setInc("balance",getsPriceFormat($price));
            }
        }
        return true;
    }

    /**
     * @param $member_id
     * @param $agent_id
     * @param $order_number
     * @return bool
     * @throws \think\Exception
     * @context 用户第1，2，3 次消费给提成 不满100或使用卡券不给提
     */
    static function memberConsumpNum($member_id,$agent_id,$order_number){
        $date = date("Y-m-d H:i:s");
        # 判断汽车服务订单和商家订单总和是否是第一次
        # 汽车订单必须包含系统服务或商品
        $car_orders =  Db::table('orders')->where(array("member_id"=>$member_id))
            ->where("order_status in (1,5,6)")
            ->where("(select count(id) from order_biz where order_biz.order_number = orders.order_number and order_biz.custommark=1)>0
            or (select count(id) from order_server where order_server.order_number = orders.order_number and order_server.custommark=1)>0")
            ->count();
        $merchants_orders = Db::table("merchants_order")->where(array("member_id"=>$member_id))
            ->where("(actual_price>50 and (select count(id) from merchants_voucher_order where order_id = merchants_order.id)=0)
                        or (actual_price<=50)")
            ->where("order_status in (1,2) and pay_type in (1,2,3,4)")->count();
        $comm = self::RecommendCommission();
        if(($car_orders+$merchants_orders)<=12){
            if(($car_orders+$merchants_orders)==1){
                $price = $comm['first_consump'];
                $type = 21;
            }elseif (($car_orders+$merchants_orders)==2) {
                $price = $comm['second_consump'];
                $type = 22;
            }elseif(($car_orders+$merchants_orders)==3){
                $price = $comm['third_consump'];
                $type = 23;
            }elseif(($car_orders+$merchants_orders)==12){
                $price = $comm['twelf_consump'];
                $type = 24;
            }
            # 给代理加待结算佣金
            $_return_array = array(
                array("ragent_id"=>$agent_id,"price"=>$price,"type"=>2,
                "create_time"=>$date,"status"=>1,"source"=>$type,"pro_id"=>0,"order_number"=>$order_number,"member_id"=>$member_id,
                "relation_type"=>1,"cid"=>0)
            );
            $pinfo=Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
            # 给上级10%；
            if(!empty($pinfo['pid'])){
                array_push($_return_array,array("ragent_id"=>$pinfo['pid'],"price"=>$price*0.1,"type"=>10,
                    "create_time"=>$date,"status"=>1,"source"=>$type,"pro_id"=>0,"order_number"=>$order_number,"member_id"=>$member_id,
                    "relation_type"=>1,"cid"=>$agent_id)) ;
            }
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $order_number
     * @param $member_id
     * @return bool
     * @throws \think\Exception
     * @context 消费完成后提成到账
     */
    static function memberConsumpNumFinish($order_number,$member_id){
        # 查询是否有代理提成
        $info = Db::connect("ealspell")->table("deduct_record")
            ->where(array("order_number"=>$order_number,"status"=>1,"member_id"=>$member_id))
            ->where("type=2 or type=10")
            ->where("source in (21,22,23,24)")
            ->select();
        if(!empty($info)){
            foreach($info as $v){
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$v['ragent_id']))->setInc("balance",getsPriceFormat($v['price']));
            }
            Db::connect("ealspell")->table("deduct_record")
                ->where(array("order_number"=>$order_number,"member_id"=>$member_id))
                ->where("type in (2,10) and source in (21,22,23,24)")
                ->update(array("status"=>2));
        }
        return true;
    }

    /**
     * @param $member_id
     * @param $agent_id
     * @return bool
     * @throws \think\Exception
     * @context 用户首次积分兑换给提成
     */
    static function memberIntegralExchange($member_id,$agent_id)
    {
        # 判断这个好友是否已经给这个代理提成过积分兑换
        $is_repeat = Db::connect("ealspell")->table("deduct_record")->where(array("ragent_id"=>$agent_id,
            "member_id"=>$member_id,"type"=>2,"source"=>25))->find();
        if(empty($is_repeat)){
            $date = date("Y-m-d H:i:s");
            $price = self::RecommendCommission()['integral'];
            $_return_array = array(
                array("ragent_id"=>$agent_id,"price"=>$price,"type"=>2,
                    "create_time"=>$date,"status"=>2,"source"=>25,"pro_id"=>0,"member_id"=>$member_id,
                    "relation_type"=>1,"cid"=>0)

            );
            Db::connect("ealspell")->table("community_agent")
                ->where(array("id"=>$agent_id))->setInc("balance",getsPriceFormat($price));
            $pinfo=Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
            # 给上级10%；
            if(!empty($pinfo['pid'])){
                array_push($_return_array,array("ragent_id"=>$pinfo['pid'],"price"=>$price*0.1,"type"=>10,
                    "create_time"=>$date,"status"=>2,"source"=>25,"pro_id"=>0,"member_id"=>$member_id,
                    "relation_type"=>1,"cid"=>$agent_id)) ;
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$pinfo['pid']))->setInc("balance",getsPriceFormat($price*0.1));
            }
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $member_id
     * @param $agent_id
     * @param $order_number
     * @return bool
     * @throws \think\Exception
     * @context 首次购买商家套餐
     */
    static function memberMerchantsPro($member_id,$agent_id,$order_number){
        # 判断这个好友是否已经给这个代理提成过积分兑换
        $is_repeat = Db::connect("ealspell")->table("deduct_record")->where(array("ragent_id"=>$agent_id,
            "member_id"=>$member_id,"source"=>26,"type"=>2))->find();
        if(empty($is_repeat)){
            $date = date("Y-m-d H:i:s");
            $price = self::RecommendCommission()['member_merchantspro'];
            $_return_array = array(
                array("ragent_id"=>$agent_id,"price"=>$price,"type"=>2,
                    "create_time"=>$date,"status"=>1,"source"=>26,"pro_id"=>0,"order_number"=>$order_number,
                    "member_id"=>$member_id,"relation_type"=>1,"cid"=>0)

            );
            $pinfo=Db::connect("ealspell")->table("community_agent")
                ->field("mark,pid")->where(array("id"=>$agent_id,"status"=>2))->find();
            # 给上级10%；
            if(!empty($pinfo['pid'])){
                array_push($_return_array,array("ragent_id"=>$pinfo['pid'],"price"=>$price*0.1,"type"=>10,
                    "create_time"=>$date,"status"=>1,"source"=>26,"pro_id"=>0,"order_number"=>$order_number,
                    "member_id"=>$member_id,"relation_type"=>1,"cid"=>$agent_id)) ;

            }
            Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
        }
        return true;
    }

    /**
     * @param $order_number
     * @param $member_id
     * @return bool
     * @throws \think\Exception
     * @context 首次购买商家套餐完成后到账
     */
    static function memberMerchantsProFinish($order_number,$member_id){
        # 查询是否有代理提成
        $info = Db::connect("ealspell")->table("deduct_record")
            ->where(array("order_number"=>$order_number,"status"=>1,"member_id"=>$member_id))
            ->where("type=2 or type=10")
            ->where("source=26")
            ->select();
        if(!empty($info)){
            foreach($info as $v){
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$v['ragent_id']))->setInc("balance",getsPriceFormat($v['price']));
            }
            Db::connect("ealspell")->table("deduct_record")
                ->where(array("order_number"=>$order_number,"member_id"=>$member_id))
                ->where("type in (2,10) and source=26")
                ->update(array("status"=>2));
        }
        return true;
    }

    /**
     * @param $member_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户连续7天签到给佣金
     */
    static function memberSign($member_id){
        $channel = Db::table("channel")->where(array("member_id"=>$member_id,"action_type"=>1,"channel"=>10))->order("id desc")->find();
        if(!empty($channel) and $channel['pid']>0){
            $is_repeat = Db::connect("ealspell")->table("deduct_record")->where(array("ragent_id"=>$channel['pid'],
                "member_id"=>$member_id,"source"=>27,"type"=>2))->find();
            if(empty($is_repeat)){
                $today = date("Y-m-d");
                $sevenday = date("Y-m-d",strtotime("-7 days"));
                $num=Db::table('log_signs')
                    ->where(array("member_id"=>$member_id))
                    ->where("date(log_time) > '2022-04-04' and date(log_time)<='".$today."' and date(log_time)>='".$sevenday."'")
                    ->count();
                if($num>=7){
                    $date = date("Y-m-d H:i:s");
                    $price = self::RecommendCommission()['sign'];
                    $_return_array = array(
                        array("ragent_id"=>$channel['pid'],"price"=>$price,"type"=>2,
                            "create_time"=>$date,"status"=>2,"source"=>27,"pro_id"=>0,
                            "member_id"=>$member_id,"relation_type"=>1,"cid"=>0)

                    );
                    Db::connect("ealspell")->table("community_agent")
                        ->where(array("id"=>$channel['pid']))->setInc("balance",getsPriceFormat($price));
                    $pinfo=Db::connect("ealspell")->table("community_agent")
                        ->field("mark,pid")->where(array("id"=>$channel['pid'],"status"=>2))->find();
                    # 给上级10%；
                    if(!empty($pinfo['pid'])){
                        array_push($_return_array,array("ragent_id"=>$pinfo['pid'],"price"=>$price*0.1,"type"=>10,
                            "create_time"=>$date,"status"=>2,"source"=>27,"pro_id"=>0,
                            "member_id"=>$member_id,"relation_type"=>1,"cid"=>$channel['pid'])) ;
                        Db::connect("ealspell")->table("community_agent")
                            ->where(array("id"=>$pinfo['pid']))->setInc("balance",getsPriceFormat($price*0.1));
                    }
                    Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
                }
            }
        }
        return true;

    }

    /**
     * @param $member_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户首次充值给提成
     */
    static function memberCharge($member_id){
        $channel = Db::table("channel")->where(array("member_id"=>$member_id,"action_type"=>1,"channel"=>10))->order("id desc")->find();
        if(!empty($channel) and $channel['pid']>0){
            $is_repeat = Db::connect("ealspell")->table("deduct_record")->where(array("ragent_id"=>$channel['pid'],
                "member_id"=>$member_id,"source"=>28,"type"=>2))->find();
            if(empty($is_repeat)){
                $date = date("Y-m-d H:i:s");
                $price = self::RecommendCommission()['recharge'];
                $_return_array = array(
                    array("ragent_id"=>$channel['pid'],"price"=>$price,"type"=>2,
                        "create_time"=>$date,"status"=>2,"source"=>28,"pro_id"=>0,
                        "member_id"=>$member_id,"relation_type"=>1,"cid"=>0)

                );
                Db::connect("ealspell")->table("community_agent")
                    ->where(array("id"=>$channel['pid']))->setInc("balance",getsPriceFormat($price));
                $pinfo=Db::connect("ealspell")->table("community_agent")
                    ->field("mark,pid")->where(array("id"=>$channel['pid'],"status"=>2))->find();
                # 给上级10%；
                if(!empty($pinfo['pid'])){
                    array_push($_return_array,array("ragent_id"=>$pinfo['pid'],"price"=>$price*0.1,"type"=>10,
                        "create_time"=>$date,"status"=>2,"source"=>28,"pro_id"=>0,
                        "member_id"=>$member_id,"relation_type"=>1,"cid"=>$channel['pid'])) ;
                    Db::connect("ealspell")->table("community_agent")
                        ->where(array("id"=>$pinfo['pid']))->setInc("balance",getsPriceFormat($price*0.1));
                }
                Db::connect("ealspell")->table('deduct_record')->insertAll($_return_array);
            }
        }
        return true;
    }

    /**
     * @param $order_number
     * @return bool
     * @throws \think\Exception
     * @context 用户退款后给代理提成减掉
     */
    static function refundDeduct($order_number){
        Db::connect("ealspell")->table('deduct_record')->where(array("order_number"=>$order_number))->update(array("status"=>3));
        return true;
    }









}