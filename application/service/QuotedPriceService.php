<?php


namespace app\service;

use think\Db;

/**
 * 报价询价服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月9日14:07:47
 */
class QuotedPriceService
{
    /**
     * 报价列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月10日11:58:22
     * @param    [array]          $params [输入参数]
     */
    public static function QuotedPriceListWhere($params = [])
    {


        //车型id
        if(!empty($params['param']['brandmodel_id'])&&intval($params['param']['brandmodel_id'])>0)
        {
            $where[] = ['brandmodel_id', '=', $params['param']['brandmodel_id']];
        }
        //车牌id
        if(!empty($params['param']['brand_id'])&&intval($params['param']['brand_id'])>0)
        {
            $where[] = ['brand_id', '=', $params['param']['brand_id']];
        }
        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['title|identification', 'like', '%'.$params["param"]['keywords'].'%'];
        }

        //创建时间
        if(!empty($params['param']['create_time']))
        {
            $start=date('Y-m-d 00:00:00',strtotime($params["param"]['create_time']));
            $end=date('Y-m-d 23:59:59',strtotime($params["param"]['create_time']));

            $where[] =['create_time', '>', $start];
            $where[] =['create_time', '<', $end];
        }
        return $where;

    }
    /**
     * 报价数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月9日14:42:19
     * @desc    description
     * @param    [array]          $data [输入参数]
     */
    public static function DataDealWith($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                //售价
                if(isset($v['selling_price']))
                {
                    $v['selling_price_show']= $v['selling_price']==0.00?'0.00':priceFormat($v['selling_price']);
                }
                //进价
                if(isset($v['purchase_price']))
                {
                    $v['purchase_price_show']= $v['purchase_price']==0.00?'0.00':priceFormat($v['purchase_price']);
                }
                //原厂价格
                if(isset($v['oldfactory_price']))
                {
                    $v['oldfactory_price_show']= $v['oldfactory_price']==0.00?'0.00':priceFormat($v['oldfactory_price']);
                }
                //副厂价格
                if(isset($v['vicefactory_price']))
                {
                    $v['vicefactory_price_show']= $v['vicefactory_price']==0.00?'0.00':priceFormat($v['vicefactory_price']);
                }
                //拆车价格
                if(isset($v['dismancar_price']))
                {
                    $v['dismancar_price_show']= $v['dismancar_price']==0.00?'0.00':priceFormat($v['dismancar_price']);
                }
                //品牌价格
                if(isset($v['brand_price']))
                {
                    $v['brand_price_show']= $v['brand_price']==0.00?'0.00':priceFormat($v['brand_price']);
                }

                //下线配件价格
                if(isset($v['offline_price']))
                {
                    $v['offline_price_show']= $v['offline_price']==0.00?'0.00':priceFormat($v['offline_price']);
                }

                //车型
                if(isset($v['brandmodel_id']))
                {
                  $v['brandmodel_title']=Db::name('brand')->where('id='.$v['brandmodel_id'])->value('brand_title');
                }

                //车牌
                if(isset($v['brand_id']))
                {
                    $v['brand_title']=Db::name('brand')->where('id='.$v['brand_id'])->value('brand_title');
                }
                //处理状态
                if(isset($v['read_status']))
                {
                    $v['read_status_title']='待报价';

                    if ($v['read_status']==2)
                    {
                        $v['read_status_title']='已报价';;
                    }
                }
                //配置
//                dump($v['configuration']);exit;
//                $v['configuration_show']='未填写参数';
//                if(isset($v['configuration'])&&!empty($v['configuration']))
//                {
//
//                    $v['configuration_show']=join(',',json_decode($v['configuration'],true));
//                }

                //车辆型号
                if (isset($v['brandmodel_id'])) {
                    $v['brandmodel_id_title']=Db::name('brand')->where('id='.$v['brandmodel_id'])->value('brand_title');
                }
                //dump($v['brandmodel_id_title']);exit;
            }
        }


        return $data;
    }

    /**
     * 报价保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月30日16:59:54
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveQuotedPrice($params){
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '名称不能为空',
                'error_code'         => 15002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'brand_id',
                'error_msg'         => '品牌不能为空',
                'error_code'         => 15003,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'brandmodel_id',
                'error_msg'         => '车型不能为空',
                'error_code'         => 15004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'identification',
                'error_msg'         => '识别码不能为空',
                'error_code'         => 15005,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=$params;
        //dump($data);exit;

        // 编辑
        Db::startTrans();
        if(Db::name('quotedprice')->where(['id'=>intval($params['id'])])->update($data))
        {
            $id = $params['id'];
        }
        
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>15001,'msg'=>'修改报价信息失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }


}