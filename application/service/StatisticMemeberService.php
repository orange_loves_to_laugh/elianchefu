<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/9
 * Time: 14:01
 */

namespace app\service;
use think\Db;
/**
 * 数据统计服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年10月22日11:51:06
 */
class StatisticMemeberService
{
   /*
    * @content:app下载数量
    * @date: 2020年10月22日11:51:06
    * @retrun : array()
    * */
    public static function DownloadApp($params){
       $start=StatisticalService::HandleTime($params)['data']['time_start'];
        $end=StatisticalService::HandleTime($params)['data']['time_end'];
        $where = [
            ['integral_source', '=', 22]
        ];
        $total_count = Db::name('log_integral')->where($where)->where("date(integral_time)='".date("Y-m-d")."'")->count();

        return $total_count;
    }
    /*
     * @content:用户统计
     * @date: 2020年10月22日11:51:06
     * @retrun : array()
     * */
    static function MemberCountInfo($mark)
    {
        $where = null;
        // 初始化
        if ($mark == 4) {
            #总会员数量
            $where = "member_level_id > 0";
        } else if ($mark == 5) {
            #今日新增会员
            $where = [

                ['member_create', '>', date('Y-m-d 00:00:00')],
                ['member_create', '<', TIMESTAMP],
            ];

        } else if ($mark == 6) {
            $where = [
                ['member_status', '=', 2],
            ];


        }

        if ($mark == 1) {
            #查询今日新增用户：新增用户+老用户首次登陆

            $data_reg = Db::table('member')->where($where)
                ->whereTime('register_time', 'today')

                ->count();

            $where_login=[
                ['mm.register_time', '<', '2021-01-15'],
                ['ml.create_time', '>', date('Y-m-d 00:00:00')],
                ['ml.create_time', '<', TIMESTAMP],
            ];
            $data_login_log=Db::table('member_login_log')->alias('ml')
                ->leftJoin(['member'=>'mm'],'mm.id=ml.member_id')
                ->where($where_login)
                ->field('ml.member_id,ml.create_time,ml.id')
                ->select();
            //dump($data_login_log);exit;
            $data_login=0;
            foreach ($data_login_log as $v) {

                $re=Db::table('member_login_log')
                    ->where('member_id='.$v['member_id'])->count();

                if ($re==1)
                {
                    $data_login+=1;
                }
            }
            $data=$data_reg+$data_login;

        } else if ($mark == 2) {
            #总用户数量
            $data = Db::table('member_mapping')->where("member_status!=0")->count('id');
        } else if ($mark == 3) {
            #今日浏览量 (今日在线人数 今天有多少人访问了平台 log_mbrowse  用户进入到 这个E 联车服时 不是新用户 添加数据成功之后 把这个用户添加到 log_browse 按时间天添加 当天这个表没有这个用户  加记录 反之不加)
            $data = Db::name('sysset')->where(['tagname'=>'views_num'])->value('desc');
                //Db::table('log_mbrowse')->whereTime('create_time', 'today')->count('id');
        } else if ($mark == 4) {
            #总会员数量
            $data = Db::table('member_mapping')->where($where)->count('id');
        } else if ($mark == 5) {
            #今日新增会员
            $data = Db::name('log_handlecard')
                ->where($where)->count('id');

        } else if ($mark == 6) {
            #实名认证会员

            $data = Db::table('member_mapping')->where($where)->count('id');
        }
        return $data;

    }
    /*
     * @content 查询今日浏览量  有多少用户
     * @return string
     * */
     static function memberString(){

        $data = Db::table('member_login_log')->whereTime('create_time', 'today')
            ->column('member_id');
        $data = implode(',',$data);

        return $data;

    }
    /*
     * @content: 门店统计
     * @date: 2020年10月22日11:51:06
     * @retrun : str
     * */
    static function BizStatisticInfo($mark)
    {
        if ($mark == 1) {
            #门店咨询数据
            $data = Db::table('quotedprice')->count('id');
        }
        else if ($mark == 2) {
            #门店订货数据
            $data = Db::table('dispath_ordergoods')->count('id');
        }
        else if ($mark == 3) {
            #今日到店顾客数
            $data = Db::table('biz_traffic')
                ->whereTime('create_time', 'today')
              // ->group('concat(member_id,biz_id)')
                ->count('id');
        }
        else if ($mark == 4) {
            #昨日到店顾客数
            $data = Db::table('biz_traffic')
                ->whereTime('create_time', 'yesterday')
                //->group('concat(member_id,biz_id)')
                ->count('id');

        }
        else if ($mark == 5) {
            #今日到店会员

            $data=Db::name('biz_traffic')->alias('bt')
                ->leftJoin(['member_mapping'=>'mm'],'bt.member_id=mm.member_id')
                ->where([['mm.member_level_id','>',0]])
                ->whereTime('bt.create_time', 'today')
                ->field('mm.member_level_id,member_expiration')->select();

            foreach ($data as $k=>$v)
            {
                if ($v['member_level_id']==1||$v['member_level_id']==2)
                {
                    if (strtotime($v['member_expiration'])< time())
                    {
                        unset($data[$k]);
                    }
                }
            }

            $data=count($data);
            /*
            $data = Db::table('biz_traffic bt')
                ->field("(select count(mm.member_id) from member_mapping mm where mm.member_id = bt.member_id and member_status !=0 and  member_level_id >0 ) memberid")
                ->whereTime('create_time', 'today')
                ->select();

            $_arr = array();
            if(!empty($data))
            {
                foreach ($data as $k=>$v){
                    if($v['memberid'] == 1){
                        array_push($_arr,$v['memberid']);
                    }
                }
                $data = array_sum($_arr);
            }else{
                $data=0;
            }
            */
        }
        else if ($mark == 6) {

            #昨日到店会员
            $data=Db::name('biz_traffic')->alias('bt')
                ->leftJoin(['member_mapping'=>'mm'],'bt.member_id=mm.member_id')
                ->where([['mm.member_level_id','>',0]])
                ->whereTime('bt.create_time', 'yesterday')
                ->field('mm.member_level_id,member_expiration')->select();
            foreach ($data as $k=>$v)
            {
                if ($v['member_level_id']==1||$v['member_level_id']==2)
                {
                    if (strtotime($v['member_expiration'])< time())
                    {
                        unset($data[$k]);
                    }
                }
            }

            $data=count($data);
//            $data = Db::table('biz_traffic bt')
//                ->field("(select  count(mm.member_id) from member_mapping mm where mm.member_id = bt.member_id and member_status !=0 and  member_level_id >0 ) memberid")
//                ->whereTime('create_time', 'yesterday')
//                ->select();
//            $_arr = array();
//            if(!empty($data))
//            {
//                foreach ($data as $k=>$v){
//                    if($v['memberid'] == 1){
//                        array_push($_arr,$v['memberid']);
//                    }
//                }
//                $data = array_sum($_arr);
//            }else{
//                $data=0;
//            }
        }
        else if ($mark == 7) {
            #超时未到店会员
            $data = Db::table('orders')
                ->where(['order_status'=>3,'order_type'=>3  ])
                ->whereTime('order_create', 'today')
                ->group('member_id')
                ->column('member_id');
            $data = self::BizOrdersInfoDetail($data);
            #返回的是数组  获取当中需要的数据
            $data = $data['data'];
            if(empty($data)){
                $data = 0;
            }
        }
        return $data;

    }
    /*
    * @content: 门店统计数据  列表
    * @date: 2020年10月22日11:51:06
    * @retrun : array()
    * */
    static function BizDataInfo($mark,$params){
        #门店名称
        $_where =[];
        if(!empty($params['biz_title']))
        {
            $_where[] =['b.biz_title', 'like', '%'.$params['biz_title'].'%'];
        }
        #门店类型
        if(isset($params['biz_type'])&&intval($params['biz_type'])>0)
        {
            $_where[] = ['b.biz_type', '=', $params['biz_type']];
        }
        #门店订货数据 查看详情
        if(isset($params['id']))
        {
            $_where[] = ['q.id', '=', $params['id']];
        }



        if ($mark == 1) {
            #门店咨询数据
            $data = Db::table('quotedprice q')
                ->field('q.*,b.biz_title,b.biz_address,b.biz_phone,
                (select brand_title from brand b where b.id = q.brand_id) brand_title,
                (select brand_title from brand b where b.id = q.brandmodel_id) brandmodel_title')
                ->join('biz b','b.id = q.biz_id','left');
            $data = $data->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];

        }
        else if ($mark == 2) {
            #门店订货数据
            $data = Db::table('dispath_ordergoods q')
                ->field('q.*,b.biz_title,b.biz_address,b.biz_phone,
                (select brand_title from brand b where b.id = q.brand_id) brand_title,
                (select brand_title from brand b where b.id = q.brandmodel_id) brandmodel_title')
                ->where($_where)
                ->join('biz b','b.id = q.biz_id','left');
            $data = $data->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];

        }
        else if ($mark == 3||$mark == 5) {
              #今日到店会员 今日到店用户
            $_where[]=['bt.id','>',0];

            if(!empty($params['create_time']))
            {
                $_where[] = ['bt.create_time', '>', $params['create_time']];

                $_where[] = ['bt.create_time', '<', date('Y-m-d 23:59:59',strtotime($params['create_time']))];
            }else{
                $_where[] = ['bt.create_time', '>', date("Y-m-d",time())];

                $_where[] = ['bt.create_time', '<', TIMESTAMP];

                $params['create_time']=date("Y-m-d",time());
            }


            $data = Db::table('biz_traffic bt')
                ->field('bt.*,b.biz_type,b.biz_title,b.biz_address,b.biz_phone')
                ->join('biz b','b.id = bt.biz_id','left')

                ->where($_where)
                ->group('bt.biz_id')
                ->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];

            #处理数据
            $data = self::BizDataInfoDetail($data,$params['create_time']);


        }
        else if ($mark == 4 or $mark == 6) {
            #昨日到店顾客数  #昨日到店会员
            //时间判断
            $_where[]=['bt.id','>',0];

            if(!empty($params['create_time']))
            {
                $_where[] = ['bt.create_time', '>', $params['create_time']];

                $_where[] = ['bt.create_time', '<', date('Y-m-d 23:59:59',strtotime($params['create_time']))];
            }else{
                $_where[] = ['bt.create_time', '>', date("Y-m-d",time()-86400)];

                $_where[] = ['bt.create_time', '<', date('Y-m-d 23:59:59',time()-86400)];

                $params['create_time']=date("Y-m-d",time()-86400);
            }

            $data = Db::table('biz_traffic bt')
                ->field('bt.*,b.biz_title,b.biz_address,b.biz_phone,b.biz_type')
                ->join('biz b','b.id = bt.biz_id','left')
                ->where($_where)
                ->group('bt.biz_id')
                ->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];

            #处理数据
            $data = self::BizDataInfoDetail($data,$params['create_time']);
        }

        return $data;
    }
    /**
     * 计算门店销售额
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月5日16:58:32
     * @desc    description
     * @param   [array]          $param [输入参数]
     */
    public static function CalcSelPrice($param){
        $id_arr=[];

        if ($param['biz_type']==1) {//直营：所有订单总额包含卡券-计算没核销卡券之前
            $where=[
                ['order_status','=',5],
                ['biz_id','=',$param['biz_id']],
                ['order_create','>',$param['day']],
                ['order_create','<',date('Y-m-d 23:59:29',strtotime($param['day']))],
            ];
            $orders=Db::name('orders')->where($where)->select();

            $total=0;

            foreach ($orders as &$v)
            {
                if ($v['pay_type']==6)
                {
                    if ($v['pay_price']>0)
                    {
                        $total+=$v['pay_price'];
                    }else{
                        $total+=$v['order_price'];
                    }
                }else{
                    $total+=$v['pay_price']+$v['voucher_price'];
                }
            }

            /*
            $voucher_price=Db::name('orders')
                ->where($where)
                ->sum('voucher_price');


            $pay_price=Db::name('orders')
                ->where($where)
                ->sum('pay_price');

            $total=$pay_price+$voucher_price;
            */
            $id_arr=Db::name('orders')
                ->where($where)
                ->column('id');



        }
        else{//合作店 加盟店

//            $where=[
//                ['biz_id','=',$param['biz_id']],
//                ['create_time','>',$param['day']],
//                ['create_time','<',date('Y-m-d 23:59:29',strtotime($param['day']))],
//            ];
//
//            $re=Db::name('biz_settlement')
//                ->whereTime('create_time', $param['day'])
//                ->where($where)
//                ->select();
            $where=[
                ['order_status','=',5],
                ['biz_id','=',$param['biz_id']],
                ['order_create','>',$param['day']],
                ['order_create','<',date('Y-m-d 23:59:29',strtotime($param['day']))],
            ];
            $re=Db::name('orders')->where($where)->select();

            $total=0;

            foreach ($re as $k=>$v)
            {
                //$total+=$v['tobe_settled'];

                $id_arr[$k]=$v['id'];
                if ($v['pay_type']==6)
                {
                    if ($v['pay_price']>0)
                    {
                        $total+=$v['pay_price'];
                    }else{
                        $total+=$v['order_price'];
                    }
                }else{
                    $total+=$v['pay_price']+$v['voucher_price'];
                }
                /*
                if (in_array( $v['method'],[1,2,5]))//余额支付 微信 支付宝：按结算价格
                {
                    $price=Db::name('order_server')->where(['order_number'=>$v['order_number']])
                        ->sum('final_settlement');
                    $total+=$price;

                }elseif (in_array( $v['method'],[3,4])){//现金 银联 ：按实际收款价格算 去红包
                    $total+=$v['pay_price'];
                }
                */
            }

        }
        return ['price'=>priceFormat($total),'id_arr'=>join(',',$id_arr)];
    }
    /*
    * @content: 数据处理
    * @date: 2020年10月22日11:51:06
    * @retrun : array()
    * */
    static function BizDataInfoDetail($params,$day){
        if(!empty($params)){
            foreach($params as &$v){
                //门店类型
                $v['biz_type_title']=BaseService::StatusHtml($v['biz_type'],lang('biz_type'),false);

                $sell_info=self::CalcSelPrice(['biz_id'=>$v['biz_id'],'day'=>$day,'biz_type'=>$v['biz_type']]);
                //今日销售额
                $v['amount']=$sell_info['price'];

                //订单id
                $v['order_id_arr']=$sell_info['id_arr'];

                #求到店顾客数

                $where=[
                    ['create_time','>',$day],
                    ['create_time','<',date('Y-m-d 23:59:29',strtotime($day))],
                    ['biz_id','=',$v['biz_id']],

                ];
                $v['bizmemberCount_arr'] = Db::table('biz_traffic')
                        ->where($where)
                        ->column('member_id');

                $v['bizmemberCount']=count( $v['bizmemberCount_arr']);

                /*
                # 求到店会员
                $bizdataCount = Db::table('biz_traffic')->field('member_id')
                    ->where(array('biz_id'=>$v['biz_id']))
                    ->whereTime('create_time', 'today')->column('member_id');


                #判断所有会员用户中  是否在到店会员的用户中  在不在 删除所有会员用户中  的会员建值（用户）
                if(!empty($bizdataCount)){
                    #查询是会员的所有用户
                    $cardMenber = Db::table('member_mapping')
                        ->field('member_id')
                        ->where('member_level_id > 0')
                        ->select();

                    foreach($cardMenber as $ck=>$cv){
                        if(!in_array($cv['member_id'],$bizdataCount)){
                            unset($cardMenber[$ck]);

                        }
                    }
                }
                $v['cardMenberCount'] = count($cardMenber);
                */
                //到店会员
                /*
                $where=[
                    ['member_id','in',$v['bizmemberCount_arr'] ],
                    ['member_level_id','>',0],
                ];

                $v['cardMenberCount_arr'] =Db::name('member_mapping')->where($where)
                    ->field('member_level_id,member_id,member_expiration')->select();

                $cardMenberCount_arr=[];

                foreach ($v['cardMenberCount_arr'] as $k=>$vv)
                {
                    if ($vv['member_level_id']==1||$vv['member_level_id']==2)
                    {
                        if (strtotime($vv['member_expiration'])<time())
                        {
                            unset($v['cardMenberCount_arr'][$k]);
                        }else{
                            $cardMenberCount_arr[$k]=$vv['member_id'];
                        }
                    }else{
                        $cardMenberCount_arr[$k]=$vv['member_id'];
                    }
                }

                $v['cardMenberCount_arr']=$cardMenberCount_arr;

                $v['cardMenberCount']=count( $v['cardMenberCount_arr']);
                  */
                    $where=[
                    ['bt.biz_id','=',$v['biz_id']],
                    ['mm.member_level_id','>',0],
                    ['bt.create_time','>',$day],
                    ['bt.create_time','<',date('Y-m-d 23:59:29',strtotime($day))],
                    ];
                $cardMenberCount_arr=Db::table('biz_traffic')->alias('bt')
                    ->leftJoin(['member_mapping'=>'mm'],'mm.member_id=bt.member_id')
                    ->where($where)
                    ->field('bt.member_id,mm.member_level_id,bt.create_time,mm.member_expiration')
                    ->select();

                foreach ($cardMenberCount_arr as $k=>$vv)
                {
                    if ($vv['member_level_id']==1||$vv['member_level_id']==2)
                    {
                        if (strtotime($vv['member_expiration'])<time())
                        {
                            unset($cardMenberCount_arr[$k]);
                        }else{
                            $cardMenberCount_arr[$k]=$vv['member_id'];
                        }
                    }else{
                        $cardMenberCount_arr[$k]=$vv['member_id'];
                    }
                }
                $v['cardMenberCount_arr']=$cardMenberCount_arr;

                $v['cardMenberCount']=count( $v['cardMenberCount_arr']);




                #查询办理会员数量
                $where=[
                    ['biz_id','=',$v['biz_id']],
//                    ['log_resource','=',2],
                ];
                $v['memberCountNum_arr'] = Db::name('log_handlecard')
                    ->where($where)
                    ->where("date(member_create)='".$day."'")
                    ->column('member_id');
                $v['memberCountNum']=count($v['memberCountNum_arr']);

//                Db::name('log_handlecard')
//                    ->whereTime('member_create', $day)
//                    ->where(array('biz_id'=>$v['biz_id']))
//                    ->count('id');

            }

            return $params;

        }

    }
    /*
  * @content: 数据处理
  * @date: 2020年10月22日11:51:06
  * @retrun : array()
  * */
    static function BizDataInfoDealWith($params){
        if(!empty($params)){
            foreach($params as &$v){
               if($v['read_status'] == 1){
                   $v['read_status']='未读';

               }else{
                   $v['read_status']='已读';

               }
            }
            return $params;

        }

    }
    /*
    * @content: 超时未到店会员数据处理
    * @date: 2020年10月22日11:51:06
    * @retrun : array()
    * */
    static function BizOrdersInfoDetail($params){
        if(!empty($params)){
            #查询是会员的所有用户
            $cardMenber = Db::table('member_mapping')->field('member_id')->where('member_level_id > 0')->column('member_id');
            foreach($cardMenber as $ck=>$cv){
                if(!in_array($cv,$params)){
                    unset($cardMenber[$ck]);

                }
            }
            $data = count($cardMenber);
            $cardMenber = implode(',',$cardMenber);

            return array('data'=>$data,'cardMenber'=>$cardMenber);

        }

    }
    /*
   * @content: 统计总数
   * @date: 2020年10月22日11:51:06
   * @retrun : array()
   * */
    static function BizDataInfoCount($mark){
        if ($mark == 1) {
            #门店咨询数据
            $data = Db::table('quotedprice q')
                ->count();

        } else if ($mark == 2) {
            #门店订货数据
            $data = Db::table('dispath_ordergoods q')
                ->count();

        } else if ($mark == 3 or $mark == 5) {
            #今日到店顾客数   #今日到店会员
            $data = Db::table('biz_traffic bt')

                ->whereTime('create_time', 'today')
                ->group('biz_id')
                ->count();

        } else if ($mark == 4 or $mark == 6) {
            #昨日到店顾客数  #昨日到店会员
            $data = Db::table('biz_traffic bt')
                ->whereTime('create_time', 'yesterday')
                ->group('biz_id')
                ->count();

        } else if ($mark == 7) {
            #超时未到店会员
            $data = Db::table('orders')->where(array('order_status'=>4))->count();

        }

        return $data;
    }
    /*
    * @content: 办理会员数量的用户
    * @date: 2020年10月22日11:51:06
    * @retrun : str
    * */
    static function memberCardString($biz_id,$mark){
        if($mark ==3 or $mark == 5){
            #今日到店顾客数   #今日到店会员
            $day = 'today';
        }else{
            #昨日
            $day = 'yesterday';

        }
        $data =  Db::table('biz_traffic')->where(array('biz_id'=>$biz_id))
            ->whereTime('create_time', $day)
            ->where('member_id > 0')
            ->group('concat(member_id,biz_id)')
            ->column('member_id');

        $data = implode(',',$data);
        return $data;

    }
    static function BizMembertring($biz_id){
        #查询会员数量
        $data = Db::table('channel')->where("biz_id = $biz_id and action_type = 2")
            ->column('member_id');

        $data = implode(',',$data);
        return $data;
    }

    static function MemberHandleTotal($mark,$data=array()){
        $price=0;
        if($mark==1){
            # 今日办理会员总金额
            $price = Db::table("log_handlecard")->where("date(member_create)='".date("Y-m-d")."'")->sum("level_price");
        }else{
            if(!empty($data['start']) or !empty($data['end'])){
                $where="id>0 ";
                if(!empty($data['start'])){
                    $where.=" and date(member_create)>='".$data['start']."'";
                }
                if(!empty($data['end'])){
                    $where.=" and date(member_create)<='".$data['end']."'";
                }
                $price = Db::table("log_handlecard")->where($where)->sum("level_price");
            }

        }
        return getsPriceFormat($price);

    }

}