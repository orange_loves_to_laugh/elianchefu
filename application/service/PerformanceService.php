<?php


namespace app\service;

use think\Db;

/**
 * 绩效服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0git
 * @datetime 2020年10月22日13:45:48
 */
class PerformanceService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {


        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['e.employee_name|e.employee_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //类型
        if(!empty($params['param']['type']))
        {
            $where[] =['level', '=',  $params["param"]['type']];
        }
        return $where;
    }

    /**
     * 获取员工考核列表数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DataList($params,$_where=''){

        $where = empty($params['where']) ? [['ec.id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'ec.create_time desc';

        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        $data = Db::name('employee_check')
            ->alias('ec')
            ->leftJoin(['employee_sal'=>'e'],'ec.employee_id=e.id')
//            ->leftJoin(['employee_check_relate'=>'ecr'],'ec.id=ecr.employee_check_id')
            ->where($where)->where($_where)->field($field)->order($order);

        // 获取列表

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            //$data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }
        $re=self::DataDealWith($data->toArray());

        return $re;

    }

    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月24日16:32:38
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function DataTotal($table,$where){


        return (int) Db::name($table)->where($where)->count();
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月24日16:32:38
     * @param    [array]          $data [输入参数]
     */
    public static function DataDealWith($data){

        if(!empty($data['data']))
        {
            foreach($data['data'] as &$v)
            {
                //姓名
                if ($v['employee_id']>0)
                {
                   $info= Db::name('employee_sal')->where('id='.$v['employee_id'])->field('employee_name,employee_department_id,employee_station_id')->find();

                   $v['employee_name']=$info['employee_name'];

                    //所属部门
                   $v['department_title']=EmployeeService::DepartmentDataHandle($v['employee_department_id']);

                   $v['department_sectitle']=EmployeeService::DepartmentDataHandle($v['employee_department_secid']);

                   $v['department_title']= $v['department_title'].'-'.$v['department_sectitle'];

                   //所属岗位
                   $v['employee_station_title']=EmployeeService::StationDataHandle($info['employee_station_id']);
                }

                //考核时间
                $v['check_time']=date('Y-m',strtotime($v['check_time']));

               //任务阶段 任务金额 已完成奖励 未完成处罚 完成状态 获得奖励
                if (isset($v['id'])&&intval($v['id'])>0)
                {
                   $employee_check_relate=Db::name('employee_check_relate')
                       ->where('employee_check_id='.$v['id'])->select();
                   $check_type_arr=lang('check_type');
                    foreach ($check_type_arr as $ctk=>$ctv)//考核类型
                    {
                        foreach ($employee_check_relate as $ecrk=>$ecrv)//类型等级
                        {
                            if ($ecrv['type']==$ctk) {

                                $v['type'][$ctk][$ecrk]=$ecrv;

                                $complete_status=self::HandleEmployeeCheck($ecrv['id']);

                                $v['type'][$ctk][$ecrk]['complete_status_title']=$complete_status['status_title'];

                                $v['type'][$ctk][$ecrk]['reward_price']=$complete_status['reward_price'];

                                $v['type'][$ctk][$ecrk]['complete_status']=$complete_status['status'];

                                $v['type'][$ctk][$ecrk]['status_num']=$complete_status['status_num'];
                                $v['type'][$ctk][$ecrk]['applyMember']=$complete_status['applyMember'];

                                if(!empty($ecrv['relate_check_str']))
                                {

                                    $v['type'][$ctk][$ecrk]['relate_check_arr']=explode(',',$ecrv['relate_check_str']);

                                    $v['type'][$ctk][$ecrk]['relate_reward']=0;
                                }
                            }
                        }
                    }
                }



            }


            /**/
            if (isset($data['data'][0]["type"]))
            {
                if (count($data['data'][0]["type"])>0)
                {

                    foreach ($data['data'][0]["type"] as &$v)//考核类型
                    {

                        foreach ($v as &$vv)//类型等级
                        {

                            if (isset($vv['relate_check_arr'])&&!empty($vv['relate_check_arr']))
                            {

                                foreach ($vv['relate_check_arr'] as $vvv) //关联其他考核
                                {
                                    $relate_type=explode('-',$vvv)[0];
                                    //$relate_reward=explode('-',$vvv)[1];

                                    if (isset($data['data'][0]["type"][$relate_type])&&count($data['data'][0]["type"][$relate_type])>0) {

                                      $re= arraySort($data['data'][0]["type"][$relate_type],'todo_num','asc');

                                      if (isset($re[1]["id"])) {

                                        $rre=self::HandleEmployeeCheck($re[1]["id"])['status'];

                                        $vv['relate_reward']=$rre;

                                        if ($rre==2)
                                        {
                                            break;
                                        }
                                      }
                                    }
                                }

                            }
                        }
                    }

                    foreach ($data['data'][0]["type"] as &$v)//考核类型
                    {

                        $v=arraySort($v,'todo_num','asc');

                        foreach ($v as &$vv)//类型等级
                        {
                            if ($vv['status_num']<=$vv['todo_num'])
                            {
                                $vv['position']=1;

                                break;
                            }
                        }
                    }

                }

            }


            //dump($data['data']);exit;
            //dump($data['data'][0]["type"]);
        }


        return $data;
    }

    /**
     * 查询员工完成考核状态
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2016-12-03T21:58:54+0800
     * @param    [int]  $employee_check_relate_id  [employee_check_relate数据表id]
     *
     */

    public static function HandleEmployeeCheck($employee_check_relate_id){

        $employee_check_relate_info= Db::name('employee_check_relate')
            ->alias('ecr')
            ->leftJoin(['employee_check'=>'ec'],'ecr.employee_check_id=ec.id')
            ->field('ecr.*,ec.check_time,ec.employee_id')
            ->where('ecr.id='.$employee_check_relate_id)->find();


        $employee_id=$employee_check_relate_info['employee_id'];

        $check_time_start=$employee_check_relate_info['check_time'];

        $check_time_end=date('Y-m-d 23:59:00',strtotime($check_time_start.'+1month -1day'));

        $todo_num=$employee_check_relate_info['todo_num'];

        $employee_check_relate_info['status_title']='未完成';

        $employee_check_relate_info['status']=2;

        $biz_id_arr=Db::name('assign_staff')->where('type=1 and employee_id='.$employee_id)->column('biz_id');

        $merchants_id_arr=Db::name('assign_staff')->where('type=2 and employee_id='.$employee_id)->column('biz_id');

        # 办理会员数量
        $applyMember = array();
        switch ($employee_check_relate_info['type']){
            case 1://销售额考核
                /*
                $where=[
                    ['os.employee_id','REGEXP',"($employee_id,)|(,$employee_id)|$employee_id"],
                    ['order_create','>=',$check_time],
                    ['order_create','<=',date('Y-m-d 23:59:00',strtotime($check_time.'+1month -1day'))],
                ];
                $sell_info=Db::name('order_server')
                    ->alias('os')
                    ->leftJoin(['orders'=>'o'],'os.order_number=o.order_number')
                    ->where($where)
                    ->sum('pay_price');

               */
                $where=[
                    ['employee_sal','=',$employee_id],
                    ['create_time','>=',$check_time_start],
                    ['create_time','<=',$check_time_end],
                ];

                $sell_info_biz=Db::name('biz_settlement')->where($where)->sum('price');

              /*  $where=[
                    ['employee_sal','=',$employee_id],
                    ['checkout_time','>=',$check_time_start],
                    ['checkout_time','<=',$check_time_end],
                ];

                $sell_info_merchants=Db::name('merchants_income')->where($where)->sum('price');*/

                $sell_info=priceFormat($sell_info_biz);

                break;

            case 2: //2办卡额：关联门店办卡 关联商户办卡 外呼平台办卡

                $where=[
                    ['employee_sal','=',$employee_id],
                    ['member_create','>=',$check_time_start],
                    ['member_create','<=',$check_time_end],
                ];

                $sell_info_employee=Db::name('log_handlecard')->where($where)->sum('level_price');
/*
                $where=[
                    ['biz_id','in',$biz_id_arr],
                    ['biz_id','<>',0],
                    ['employee_id','<>',$employee_id],
                    ['member_create','>=',$check_time_start],
                    ['member_create','<=',$check_time_end],
                ];

                $sell_info_biz=Db::name('log_handlecard')->where($where)->sum('level_price');*/

                $sell_info=priceFormat($sell_info_employee);

                break;

            case 3: //3办卡数量

                $where=[
                    ['employee_sal','=',$employee_id],
                    ['member_create','>=',$check_time_start],
                    ['member_create','<=',$check_time_end],
                ];

                $sell_info_employee=Db::name('log_handlecard')->where($where)->count();

                # 办卡数量详情 level_title,count(lh.id) num,
            $applyMember = Db::table('log_handlecard lh')
                ->field('group_concat(member_id) member_id')
//                ->join('member_level ml','ml.id=lh.level_id','left')
                ->where($where)
//                ->group('lh.level_id')
                ->select();

               /* $where=[
                    ['biz_id','in',$biz_id_arr],
                    ['biz_id','<>',0],
                    ['employee_id','<>',$employee_id],
                    ['member_create','>=',$check_time_start],
                    ['member_create','<=',$check_time_end],
                ];

                $sell_info_biz=Db::name('log_handlecard')->where($where)->count();*/

                $sell_info=$sell_info_employee;

                break;

            case 4: //4 到店率:关联商户+关联门店所有订单

               /* $where=[
                    ['employee_sal','=',$employee_id],
                    ['order_status','=',2],
                    ['pay_type','<>',5],
                    ['create_time','>=',$check_time_start],
                    ['create_time','<=',$check_time_end],
                ];

                $sell_info_merchants=Db::name('merchants_order')->where($where)->count();*/

                $where=[
                    ['employee_sal','=',$employee_id],
                    ['order_status','=',  5],
                    ['order_over','>=',$check_time_start],
                    ['order_over','<=',$check_time_end],
                ];

                $sell_info_biz=Db::name('orders')->where($where)->count();

                $sell_info=$sell_info_biz;

                break;

            case 5: //5 好评率:关联商户和门店好评订单
                $sell_info_biz = Db::table('orders o')
                    ->join('evaluation_score es', 'o.id=es.order_id', 'left')
                    ->where(array('employee_sal' => $employee_id, 'order_status' => 5, 'score' => 5))
                    ->where("date_format(order_over,'%Y-%m') = '" . date('Y-m',strtotime($check_time_start)) . "'")
                    ->group('orderserver_id')
                    ->count();

                /*$sell_info_merchants = Db::table('merchants_order mo')
                    ->join('merchants_evaluation mv', 'mv.merchants_order_id = mo.id', 'left')
                    ->where("evalua_level in (4,5)")
                    ->where(array('employee_sal' => $employee_id, 'order_status' => 2))
                    ->where("pay_type != 5 and date_format(checkout_time,'%Y-%m') = '" . date('Y-m',strtotime($check_time_start)) . "'")
                    ->count();*/
                $sell_info=$sell_info_biz;

                break;

            case 6://平台抽成金额

                $where=[
                    ['employee_sal','=',$employee_id],
                    ['type','=',1],
                    ['desposit_status','=',2],
                    ['create_time','>=',$check_time_start],
                    ['create_time','<=',$check_time_end],
                ];

                $sell_info_biz=Db::name('biz_settlement')->where($where)->sum('commission_price');

                $where=[
                    ['employee_sal','=',$employee_id],
                    ['cash_type','<>',3],
                    ['cash_type','<>',4],
                    ['order_status','=',2],
                    ['create_time','>=',$check_time_start],
                    ['create_time','<=',$check_time_end],
                ];

                $sell_info_merchants=Db::name('merchants_order')->where($where)->sum('commission_price');
                //dump($sell_info_biz);exit;
                $sell_info=priceFormat($sell_info_biz+$sell_info_merchants);

                break;
        }
        $employee_check_relate_info['status_num']=$sell_info;
        $employee_check_relate_info['applyMember']=$applyMember;
        //完成状态
        if ($sell_info>=$todo_num)
        {
            $employee_check_relate_info['status']=1;

            $employee_check_relate_info['status_title']='完成';
        }

        return $employee_check_relate_info;
    }
    /**
     * 数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月24日16:32:38
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DataSave($params)
    {


        $params['employee_id']=explode(',',$params['employee_id']);


        Db::startTrans();
        foreach ($params['employee_id'] as $k=>$v) {
            if (!empty($v))
            {
                $data=[
                    'title'=>$params['title'],
                    'check_time'=>$params['year'].'-'.$params['month'].'-01',
                    'employee_station_id'=>$params['employee_station_id'],
                    'employee_department_id'=>$params['employee_department_id'],
                    'employee_department_secid'=>$params['employee_department_secid'],
                    'employee_id'=>$v
                ];
                $employee_check_id=Db::name('employee_check')->insertGetId($data);

                if (!$employee_check_id)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>22001,'msg'=>'创建员工考核信息失败','status'=>false,'debug'=>false]);
                }

                //数据组件
                $data= self::HandleInserData($params,$employee_check_id);


                $employee_check_relate_id=Db::name('employee_check_relate')->insertAll($data);

                if (!$employee_check_relate_id)
                {
                    Db::rollback();
                    throw new \BaseException(['code'=>403 ,'errorCode'=>22002,'msg'=>'创建关联考核信息失败','status'=>false,'debug'=>false]);
                }
            }
        }
        Db::commit();
       // dump(123);exit;
        return DataReturn('ok',0);
    }

    /**
     * 组建插入数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月24日16:32:38
     * @param    [array]          $params [输入参数]
     * @param    [int]          $employee_check_id [employee_check表 的id]
     */
    public static function HandleInserData($params,$employee_check_id){

        //考核类型
        $type=[1,2,3,4,5,6];
        $data=[];
        //dump($params);exit;
        //type level related_type,

        foreach ($type as $tv)
        {
            if (isset($params['level' . $tv])) {

                foreach ($params['level' . $tv] as $levelk=>$levelv)
                {

                    $data_v=[
                        'type'=>$tv,
                        'level'=>$levelv,
                        'todo_num'=>$params['todo_num' . $tv.$levelv],
                        'reward_price'=>$params['reward_price' . $tv.$levelv],
                        'undo_punish_price'=>$params['undo_punish_price' . $tv.$levelv],
                        'undo_punish'=>$params['undo_punish' . $tv.$levelv],
                        'undo_percentage'=>$params['undo_percentage' . $tv.$levelv],
                        'employee_check_id'=>$employee_check_id,
                    ];

                    $data[GetNumberCode()]=$data_v;
                }
            }

        }

        /*
        foreach ($type as $tv)
        {
            if (isset($params['level' . $tv])) {

                foreach ($params['level' . $tv] as $levelk=>$levelv)
                {

                    $data_v=[
                        'type'=>$tv,
                        'level'=>$levelv,
                        'todo_num'=>$params['todo_num' . $tv][$levelk],
                        'reward_price'=>$params['reward_price' . $tv][$levelk],
                        'undo_punish_price'=>$params['undo_punish_price' . $tv][$levelk],
                        'undo_punish'=>$params['undo_punish_price' . $tv][$levelk]>0?1:0,
                        'undo_percentage'=>$params['undo_percentage' . $tv][$levelk],
                        'employee_check_id'=>$employee_check_id,
                    ];

                    $data[GetNumberCode()]=$data_v;
                }
            }

        }
        */


        //类型-等级-关联考核类型-未完成是否给奖励
        $checkrelatecheck_str_arr=explode(',',$params['checkrelatecheck_str']);

        foreach ($data as &$v)
        {
            foreach ($checkrelatecheck_str_arr as $csav)
            {
                if (!empty($csav))
                {
                    $split_csav=explode('-',$csav);
                    $temp_type_level=$split_csav[0].'-'.$split_csav[1];

                    if ($v['type'].'-'.$v['level']==$temp_type_level)
                    {
                        $v['relate_check_str']=isset($v['relate_check_str'])?$v['relate_check_str']:'';
                        $v['relate_check_str']=$v['relate_check_str'].','.$split_csav[2].'-'.$split_csav[3];
                        $v['relate_check_str']=substr($v['relate_check_str'],0,1)==','?substr($v['relate_check_str'],1):substr($v['relate_check_str'],0);
                    }

                }
            }

            if (!isset($v['relate_check_str']))
            {
                $v['relate_check_str']='';
            }
        }


        return $data;
    }

    /**
     * 获取员工数据列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function EmployeeDataList($param){
        $where=EmployeeService::EmployeeListWhere($param);

        $data_params = array(
            'page'         => false,
            'where'       => $where,
            'table'       =>'employee_sal',
            'order'       =>'employee_create desc',
            'field'       =>'id,employee_name'

        );
        $data=EmployeeService::EmployeeDataList($data_params);
        return $data;
    }

    /**
     * 员工数据拼接成html
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function EmployeeDataHtml($param){

        $data=self::EmployeeDataList($param);

        $id_arr=explode(',',$param['param']['id_str']);
        //dump($id_arr);exit;
        $table_html='
                        <div class="card-body">        
                            <div class="form-group">
                                <input type="text" id="search_title"  class="form-control" onkeyup="searchRelevanceStore()" placeholder="请输入员工名称" autocomplete="off">
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                         <div class="row" style="width: 100%;float: left;margin-bottom: 1rem">
                            <div class="col-sm-6">
                                <button class="btn btn-info" data-mark="checkall" onclick="checkAll(this,\'check-item\')" style="">全选</button>
                                <button class="btn btn-info" onclick="relevanceOption(\'check-item\')" style="">批量关联</button>
                            </div>
                        </div>
                        <table class="table table-bordered mb-0">
                        <thead>
                        <tr>
                        <th><input type="checkbox" class="checkall" onclick="checkAll(this,\'check-item\')"></th>
                        <th width="80%">名称</th> 
                        <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="storebody">';

        foreach ($data as $v)
        {
            $checked='';
            if (in_array($v['id'],$id_arr))
            {
                $checked='checked' ;

            }
            //dump($checked);
            $table_html.='<tr class="lump'.$v['id'].'">
                         <td><input '.$checked.' type="checkbox" value="'.$v['id'].'" class="check-item"></td>
                        <td>'.$v['employee_name'].'</td>
                        <td><button class="btn btn-primary connect_detail" data-title="'.$v['employee_name'].'"  data-id="'.$v['id'].'" onclick=connect_detail_callback(this)  >关联</button></td>
                        </tr>';
        }
        $table_html.='
                        </tbody>
                    </table>';

        return ['status'=>true,'data'=>$table_html,'datalist'=>$data];
        //DataReturn('ok',0,$table_html);
    }

    /**
     * 员工数据拼接成html
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function RelatedEmployeeDataHtml($param){

        $data=self::EmployeeDataList($param);
        $id_arr=explode(',',$param['param']['id_str']);
        //dump($id_arr);exit;
        $table_html='
                        <table class="table table-bordered mb-0">
                        <thead>
                        <tr>
                        <th width="80%">名称</th> 
                        <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="storebody">';

        foreach ($data as $v)
        {
            $checked='';
            if (in_array($v['id'],$id_arr))
            {
                $checked='checked' ;

            }
            //dump($checked);
            $table_html.='<tr class="lump'.$v['id'].'">
                        <td>'.$v['employee_name'].'</td>
                        <td><button class="btn layui-btn-danger  connect_detail" data-title="'.$v['employee_name'].'"  data-id="'.$v['id'].'" onclick=disconnect_detail_callback(this)  >删除</button></td>
                        </tr>';
        }
        $table_html.='
                        </tbody>
                    </table>';

        return ['status'=>true,'data'=>$table_html,'datalist'=>$data];
        //DataReturn('ok',0,$table_html);
    }


}
