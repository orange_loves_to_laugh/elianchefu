<?php


namespace app\service;

use Redis\Redis;
use think\Db;

/**
 * 门店提现申请服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月3日15:08:02
 */
class BizDepositService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月3日15:08:02
     * @param    [array]          $params [输入参数]
     */
    public static function ListWhere($params = [])
    {
        $where = [];


        //手机号
        if(!empty($params['phone']))
        {
            $where[] =['phone', 'like', '%'.$params['phone'].'%'];
        }
        //时间段
        if(!empty($params['start_time']))
        {
            $where[] =['create_time', '>=',  $params['start_time'] ];
        }
        if(!empty($params['end_time']))
        {
            $where[] =['create_time', '<=',  $params['end_time'] ];
        }

        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['member_id', '=',  $params['member_id'] ];
        }

        return $where;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日11:40:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DataDealWith($data){

        if(!empty($data))
        {
            foreach($data as &$v)
            {
                //提现人状态
//                if(isset($v['launch_cate']))
//                {
//                    $v['type_title']='合作店';
//                    if (2==$v['type'])
//                    {
//                        $v['type_title']='加盟店';
//                    }
//                }
                if (1==$v['source_type'])
                {
                    $v['type_title']='合作店';
                }
                if (2==$v['source_type'])
                {
                    $v['type_title']='合伙人';
                }
                if (3==$v['source_type'])
                {
                    $v['type_title']='商户';
                }
                if(isset($v['launch_cate'])){
                    $v['launch_cate_title'] = BaseService::StatusHtml($v['launch_cate'],lang('launch_cate'),false);
                }


                //提现操作状态
                if(isset($v['deposit_status']))
                {

                    $v['deposit_status_title']=BaseService::StatusHtml($v['deposit_status'],lang('deposit_status'));
                }
                //金额
                if(isset($v['price']))
                {

                    $v['price']=priceFormat( $v['price']);
                }
                //收款账号
                if(isset($v['account_name'])&&isset($v['account_code']))
                {
                    $v['total_account']=$v['account_code'];
                }
                //门店所属地区
                if(isset($v['area'])&&isset($v['city'])&&isset($v['province']))
                {
                    $v['total_area']=$v['province'].$v['city'].$v['area'];
                }
                #查询提现人状态
                if($v['launch_cate'] == 1){
                    $info = Db::table('biz')->where("biz_status = 1 and biz_type = 1")->find();
                }else if($v['launch_cate'] == 2){
                    $info = Db::table('biz')->where("biz_status = 1 and biz_type = 2")->find();

                }else if($v['launch_cate'] == 3 or $v['launch_cate'] == 7){
                    $info = Db::table('biz')->where("biz_status = 1 and biz_type = 3")->find();

                }else if($v['launch_cate'] == 4){
                    $info = Db::table('merchants')->where("status = 1")->find();

                }else if($v['launch_cate'] == 5 or $v['launch_cate'] == 8){
                    $info = Db::table('merchants')->where("status = 1")->find();

                }
                if(!empty($info)){
                    $v['type_status'] = '营业中';

                }else{
                    $v['type_status'] = '关闭';

                }

                //dump($v['province']);exit;
            }

        }

        return $data;
    }

    /**
     * 提现申请保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ApplySave($params = [])
    {

        // 编辑
        Db::startTrans();
        $params['audit_time'] = date("Y-m-d H:i:s");

        if(Db::name('log_deposit')->where(['id'=>intval($params['id'])])->update($params))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>80001,'msg'=>'提现状态保存失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }

    /**
     * 提现结果保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DepositResultSave($params){
     //deposit_cooperate：成功或失败结果

        Db::startTrans();
        $params['account_time'] = date("Y-m-d H:i:s");

        $re=Db::name('log_deposit')->where('id='.$params['id'])->update($params);
        if (!$re)
        {
           Db::rollback();
           throw new \BaseException(['code'=>403 ,'errorCode'=>80002,'msg'=>'申请结果修改失败','status'=>false,'debug'=>false]);
        }

        //门店余额：成功--》减少或失败--》增多
        $info=Db::name('log_deposit')->where('id='.$params['id'])->field('biz_id,price')->find();



//        $action_sql='setDec';
        if ($params['deposit_status']==4)
        {
            #失败
            $action_sql='setInc';

            if($params['source_type'] ==3 ){
                if($params['launch_cate'] == 5){
                    #商户  提现 失败
                    Db::name('merchants')->where('id='.$info['biz_id'])->$action_sql('balance',$info['price']);
                }
                if($params['launch_cate'] == 8){
                    #商户下架结算提现 失败
                    Db::name('merchants')->where('id='.$info['biz_id'])->update(array('status'=>1));
                }

                $_redis = new Redis();
                $_redis->hDel("merchants",'merchantsInfo'.$info['biz_id']);


            }else{
                #门店
                Db::name('biz')->where('id='.$info['biz_id'])->$action_sql('account_balance',$info['price']);
                if($params['launch_cate'] == 7){
                    #合作店下架结算提现 失败
                    Db::name('biz')->where('id='.$info['biz_id'])->update(array('biz_status'=>1));
                }
            }
        }else{
            #成功

            //source_type =3  是商户launch_cate = 4 保证金退回
            if($params['source_type'] ==3 ){
                if($params['launch_cate'] == 4){
                    #商户保证金
                    #修改商户状态
                    $a = Db::table('merchants')->where("id = '".$info['biz_id']."'")->update(array('level'=>4));
                    $b = Db::table('merchants')->where('id='.$info['biz_id'])->setDec('bond_price',$info['price']);

                }


                $_redis = new Redis();
                $_redis->hDel("merchants",'merchantsInfo'.$info['biz_id']);
            }
            if($params['launch_cate'] == 7){
                #合作店下架结算提现 成功
                $bizInfo = Db::table('biz')
                    ->field('account_balance,security_price')
                    ->where(array('id' => $info['biz_id']))
                    ->find();
                Db::table('aaa')->insert(array(
                    'info'=>'余额=>'.$bizInfo['account_balance'].'押金=>'.$bizInfo['security_price'],
                    'type'=>'合作店下架结算'.$info['biz_id']
                ));
                Db::name('biz')->where('id='.$info['biz_id'])->update(array('account_balance'=>0,'security_price'=>0));
            }
            if($params['launch_cate'] == 8){
                #商户下架结算提现 成功
                $bizInfo = Db::table('merchants')
                    ->field('balance,bond_price')
                    ->where(array('id' => $info['biz_id']))
                    ->find();
                Db::table('aaa')->insert(array(
                    'info'=>'余额=>'.$bizInfo['balance'].'保证金=>'.$bizInfo['bond_price'],
                    'type'=>'商家下架结算'.$info['biz_id']
                ));
                Db::name('merchants')->where('id='.$info['biz_id'])->update(array('balance'=>0,'bond_price'=>0));
            }
        }



//        if (!$re)
//        {
//            Db::rollback();
//            throw new \BaseException(['code'=>403 ,'errorCode'=>80003,'msg'=>'门店账户余额修改失败','status'=>false,'debug'=>false]);
//        }
        Db::commit();
        return DataReturn('ok',0);
    }

    /**
     * 提现申请删除
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DepositDel($params){
        //deposit_cooperate：
        Db::startTrans();
        $info=Db::name('log_deposit')->where('id='.$params['id'])->field('biz_id,price')->find();


        //log_deposit:
        $re=Db::name('log_deposit')->where('id='.$params['id'])->delete();
        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>80004,'msg'=>'日志删除失败','status'=>false,'debug'=>false]);
        }

        //门店余额：


        $re=Db::name('biz')->where('id='.$info['biz_id'])->setInc('account_balance',$info['price']);

        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>80003,'msg'=>'门店账户余额修改失败','status'=>false,'debug'=>false]);
        }
        dump($info);exit;
        Db::commit();
        return DataReturn('ok',0);
    }
}
