<?php


namespace app\service;

use think\Db;
use think\facade\Session;

/**
 * 用户卡卷服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月18日16:12:55
 */
class MemberVoucherService
{
    /**
     * 获取列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日09:56:02
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DataList($params)
    {

        $where = empty($params['where']) ? [['mv.id','>',0]] : $params['where'];
        $field = empty($params['field']) ? '*' : $params['field'];
        $wherestr = empty($params['wherestr']) ? '' : $params['wherestr'];

        // 获取列表
        $data = Db::name('member_voucher mv')
            ->where($where)->where($wherestr)->field($field)->order('start_time desc');

            //->where($where)->field($field)->order('create desc');

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

            $data=$data->paginate(10, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];



        return self::DataDealWith($data);


    }

    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日16:12:55
     * @param    [array]          $params [输入参数]
     */
    public static function MemberVoucherListWhere($params = [])
    {
        $where = [];

        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['mv.member_id', '=',  $params['member_id'] ];
        }
        //时间段
        if(!empty($params['param']['start_time']))
        {
            $where[] =['start_time', '>=',  $params["param"]['start_time'] ];
        }
        if(!empty($params['param']['end_time']))
        {
            $where[] =['start_time', '<=',  $params["param"]['end_time'] ];
        }
        return $where;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function DataDealWith($data){
        if(!empty($data))
        {

            foreach($data as &$v)
            {

                // 卡卷来源
                if(isset($v['voucher_source']))
                {
                    $v['voucher_source_title'] = BaseService::StatusHtml($v['voucher_source'],lang('voucher_source'),false);
                }
                $v['card_type_title'] = '服务卡券';


            }
        }

        return $data;
    }
    /**
     * 获取总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $where [查询条件]
     */
    public static function DataTotal($where,$wherestr=''){
       return (int) Db::name('member_voucher')
            ->alias('mv')
            ->leftJoin(['card_voucher'=>'cv'],'mv.voucher_id=cv.id')
            ->where($where)->where($wherestr)->count();
    }
    /**
     * 获取下属类型列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [int]          $card_type_id [卡卷类型]
     */
    public static function VoucherTypeData($card_type_id)
    {
        $data_list=Db::name('service_class')->where('is_del=2')->field('service_class_title class_title,id')->select();
        if ($card_type_id==3) {//商品卡卷
            $data_list=Db::name('pro_class')->where('class_id=0')->field('class_title,id')->select();
        }
        return DataReturn('ok',0,$data_list);
    }
    /**
     * 获取服务或商品列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [int]          $card_type_id [卡卷类型]
     */
    public static function VoucherServiceData($card_type_id,$cate_id){

        $data_list=Db::name('service')->where('is_del=2 and service_class_id='.$cate_id)->field('service_title title,id')->select();
        foreach ($data_list as &$v)
        {
           $re= Db::name('service_car_mediscount')
                ->where([['service_id','=',$v['id']],['biz_id','=',0],['service_type','=',2],['car_level_id','=',lang('car_level')['s']['id']]])
                ->value('discount');

          $v['title']=$v['title'].'-'.$re;
        }
        if ($card_type_id==3) {//商品
            $data_list=Db::name('biz_pro')->where('biz_pro_status = 1 and biz_pro_class_id='.$cate_id)->field('concat(biz_pro_title ,"-",biz_pro_price) title,id')->select();
        }
        //dump($data_list);exit;
        return DataReturn('ok',0,$data_list);
    }
    /**
     * 用户卡卷保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月19日10:45:42
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveMemberVoucherData($params){


        $p = [
            [
                'checked_type'      => 'min',
                'checked_data'      => '1',
                'key_name'          => 'card_time',
                'error_msg'         => '有效期必须大于0',
                'error_code'         => 13001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'card_type_id',
                'error_msg'         => '卡卷类型不能为空',
                'error_code'         => 13002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'server_id',
                'error_msg'         => '卡卷面值不能为空',
                'error_code'         => 13003,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }

        $count=$params['count'];
        $member_id=$params['member_id'];
        unset($params['count']);
        unset($params['member_id']);
        // 添加:card_voucher
        Db::startTrans();
        $id = Db::name('card_voucher')->insertGetId($params);
        if(!$id)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>13004,'msg'=>'添加卡卷失败','status'=>false,'debug'=>false]);
        }
        $member_voucher_arr=[];
        for($i=1;$i<=$count;$i++)
        {
            $member_voucher_arr[$i]=[
                'member_id'=>$member_id,
                'voucher_id'=>$id,
                'create'=>date('Y-m-d H:i:s',strtotime('+'.$params['card_time'].'day')),
                'status'=>1,
                'voucher_source'=>12,


            ];
        }
        //dump($member_voucher_arr);exit;
        $id = Db::name('member_voucher')->insertAll($member_voucher_arr);

        if(!$id)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>13005,'msg'=>'添加用户卡卷失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }
    /*
     * @content：赠送卡券
     * */
    static function memberVoucherSession($params){
        $data = $params;
        $card_num = $params['card_num'];
        unset($data['card_num']);
        #计算一下 过期时间  = 有效期 + 当前时间
        $time = date('Y-m-d H:i:s');
        $data['create'] =  date('Y-m-d H:i:s',strtotime($time . '+'.$params['card_time'].' day'));
        $data['server_id'] = $params['pro_service_id'];
        $data['card_title']= $data['card_title'].'服务券';
        $data['voucher_source'] = 12;
        $data['status'] = 1;
        $data['voucher_type'] = 4;
        $data['member_id'] = Session('meberIdVoucher');
        unset($data['pro_service_id'],$data['prize_type']);
        #先存卡券表
        $info = array();
        $info['card_title']= $data['card_title'];
        $info['card_time']= $data['card_time'];
        $info['card_type_id']= 1;
        $info['card_price']= $data['card_price'];
        $info['server_id']= $data['server_id'];

        $voucherId = Db::table('card_voucher')->insertGetId($info);
        $data['voucher_id'] = $voucherId;
        #再存用户卡券表
        if($card_num>1){
            $_array=array();
            for($i=0;$i<$card_num;$i++){
                array_push($_array,$data);
            }
            Db::table('member_voucher')->insertAll($_array);
        }else{
            Db::table('member_voucher')->insert($data);
        }

        return array('status'=>true);


    }
}