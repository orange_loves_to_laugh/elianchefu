<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/2
 * Time: 15:48
 */

namespace app\service;

/**
 * 活动管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
namespace app\service;
use think\Db;
use think\db\Expression;
use think\facade\Session;
class ExchangeService
{
    //兑换英文 为：exchange
    /**
     * [integralIndex 获取积分兑换管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function integralIndex($params){
        #where条件  写成活的
        $where = empty($params['where']) ? ['ie.status'=>'1'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #积分兑换管理列表信息
        $data = Db::table('integral_exchange ie')->field('ie.*')
            ->join('integral_exchange_prize iep','iep.integral_exchange_id = ie.id','left')
            ->where($where)->order('sort_number desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);

            $data=$data->toArray()['data'];

        } else {
            $data = $data->select();
        }

        $time = date("Y-m-d H:i:s");
        if(!empty($data)){
            foreach($data as $k=>$v){
                if($time < $v['start_time'] and $time<$v['end_time']){
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['exchange_status'] = '待上架';

                }else if($time >= $v['start_time'] and $time<=$v['end_time']){
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['exchange_status'] = '已上架';

                }else if($time > $v['start_time'] and $time>$v['end_time']){
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['exchange_status'] = '已下架';

                }

                $exchange_detail = Db::table('integral_exchange_prize')->field("if(voucher_type=1,'商品卡券','服务卡券') voucher_type,exchange_type")->where(array('integral_exchange_id'=>$v['id']))->select();

                if(!empty($exchange_detail)){
                    foreach($exchange_detail as $ek=>$ev){
                        if($ev['exchange_type'] == 1){
                            $data[$k]['title'] ='余额';
                        }else if($ev['exchange_type'] == 2){
                            $data[$k]['title'] = $ev['voucher_type'];
                        }else if($ev['exchange_type'] == 3){
                            $data[$k]['title'] = '红包';
                        }
                    }
                    /*    if($ev['exchange_type'] == 1){
                            $exchange_detail[$ek]['balance'] ='余额';
                            #余额
                        }else if($ev['exchange_type'] == 2){
                            $exchange_detail[$ek]['voucher'.$ev['voucher_type']] =$ev['voucher_type'];

                        }else{
                            #红包
                            $exchange_detail[$ek]['packet'] ='红包';

                        }
                        unset($exchange_detail[$ek]['voucher_type'],$exchange_detail[$ek]['exchange_type']);


                    }
                    #二维变一维数组
                    $c=array();
                    array_map(function($v) use (&$c){
                        $c[array_keys($v)[0]] = array_values($v)[0];
                    },$exchange_detail);


                    $data[$k]['title'] = implode(',',$c);*/
                }

            }
        }
        return $data;



    }
    /**
     * 总数
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function IntegralIndexCount(){
        return (int) Db::name('integral_exchange')->where('status = 1')->count();
    }
    /**
     * [integralSave 积分兑换添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    function integralSave($params){

        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'exchange_title',
                'error_msg'         => '商品名称不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'exchange_integral',
                'error_msg'         => '所需积分不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'ground_num',
                'error_msg'         => '上架数量不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'label',
                'error_msg'         => '标签不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '上架时间不能为空',
                'error_code'         => 30003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'end_time',
                'error_msg'         => '下架时间不能为空',
                'error_code'         => 30004,
            ],




        ];
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'imgurl',
                'error_msg' => '请上传缩略图图',
                'error_code' => 30001,
            ];


        }
        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }

//        $data = self::HandleData($params);
        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        } if (isset($data['oldimgurl'])) {
            unset($data['oldimgurl']);
        }
        // 添加/编辑
        if (empty($params['id'])) {
            #添加
            $data['surplus_num'] = $data['ground_num'];
            unset($data['oldground_num']);
            #添加处理 积分兑换表
            $exchangeId = Db::table("integral_exchange")->insertGetId($data);
            if($exchangeId){
                #获取缓存
                $taskPrizeInfo = Session('integralExchangeInfo');
                if(!empty($taskPrizeInfo)){
                    # 删除原来的
                    $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->value('id');
                    if(!empty($spoil_id)){
                        Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                    }
                    #获取镜像  spoil_id：卡券或红包id
                    foreach ($taskPrizeInfo as $k=>$v){
                        if($v['exchange_indate'] == '--'){
                            $taskPrizeInfo[$k]['exchange_indate'] = 0;
                        }
                        $taskPrizeInfo[$k]['integral_exchange_id']= $exchangeId;
                        $taskPrizeInfo[$k]['spoil_id']= $taskPrizeInfo[$k]['pro_service_id'];
                        if($v['exchange_type'] == 2){
                            if($v['voucher_type'] == 1){
                                #卡券表中card_type_id 商品 3
                                $card_type_id = 3;

                            }else if($v['voucher_type'] == 2){
                                #卡券表中card_type_id 服务 1
                                $card_type_id = 1;


                            }
                            #类型为卡券时 上 card_voucher  查下卡券id 存上
                            $voucher_id  = Db::table('card_voucher')->where(array('card_type_id'=>$card_type_id,'server_id'=>$taskPrizeInfo[$k]['pro_service_id']))->value('id');
                            $taskPrizeInfo[$k]['spoil_id']= $voucher_id;

                        }
                        if(isset($v['pro_service_id']))
                            unset($taskPrizeInfo[$k]['pro_service_id']);
                        if($v['exchange_type'] == 3){
                            #存 redpacket 红包
                            $voucherId = Db::table('redpacket')->insertGetId(array(
                                'price'=>$v['price'],
                                'packet_title'=>$v['exchange_title'],
                                'type'=>3,

                                'create_time'=>date("Y-m-d H:i:s"),
                            ));
                            if($voucherId){
                                #task_prize 表 插入红包id
                                $taskPrizeInfo[$k]['spoil_id']= $voucherId;

                                foreach($v['redpacket_detail'] as $rk=>$rv){
                                    if(isset($rv['is_type'])){
                                        $giving_id = Db::table("merchants_voucher")->insertGetId(array("title"=>$rv['title'],"price"=>$rv['price'],"min_consume"=>$rv['min_consume'],
                                            "num"=>1,"validity_day"=>$rv['validity_day'],"is_type"=>$rv['is_type'],"cate_pid"=>$rv['cate_pid'],"voucher_start"=>$rv['voucher_start']));

                                        unset($v['redpacket_detail'][$rk]['title'],$v['redpacket_detail'][$rk]['price'],$v['redpacket_detail'][$rk]['min_consume'],$v['redpacket_detail'][$rk]['num'],$v['redpacket_detail'][$rk]['validity_day'],
                                            $v['redpacket_detail'][$rk]['is_type'],$v['redpacket_detail'][$rk]['cate_pid'],$v['redpacket_detail'][$rk]['active_types']
                                        );
                                        $v['redpacket_detail'][$rk]['voucher_id'] = $giving_id;
                                    }
                                    $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                    $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                                    ksort($v['redpacket_detail'][$rk]);
                                }
                                Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                            }
                        }
                        if(in_array($v['exchange_type'],array(7, 8, 9, 10))){
                            # 删除原来的
                            $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->find();
                            if(!empty($spoil_id)){
                                Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                            }
                            $arr = array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                                "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']);
                            if ($v['is_type'] == 2) {
                                $arr['second_cate_pid'] = $v['second_cate_pid'];
                            }
                            $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
                            unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'], $taskPrizeInfo[$k]['days_id'], $taskPrizeInfo[$k]['prize_type'], $taskPrizeInfo[$k]['card_time'],
                                $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'], $taskPrizeInfo[$k]['active_types'], $taskPrizeInfo[$k]['second_cate_pid']
                            );
                            $taskPrizeInfo[$k]['spoil_id']= $giving_id;
                        }
                        if (in_array($v['exchange_type'],array( 11, 12, 13, 14, 15, 16))) {
                            # 删除原来的
                            $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->find();
                            if(!empty($spoil_id)){
                                Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                            }
                            # 添加汽车服务类型卡券  11-服务通用 12-商品通用 13-全部通用 14-服务分类券 15-商品分类券 16-全部分类券
                            $arr = array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                                "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'],
                                "voucher_start" => $v['voucher_start'], 'platform_ratio' => $v['platform_ratio']);
                            if ($v['is_type'] == 14 or $v['is_type'] == 15 or $v['is_type'] == 16) {
                                $arr['cate_pid'] = $v['cate_pid'];
                                $arr['second_cate_pid'] = $v['second_cate_pid'];
                            }
                            $voucher_id = Db::table("merchants_voucher")->insertGetId($arr);
                            unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'], $taskPrizeInfo[$k]['days_id'],
                                $taskPrizeInfo[$k]['prize_type'], $taskPrizeInfo[$k]['card_time'],
                                $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'], $taskPrizeInfo[$k]['active_types'], $taskPrizeInfo[$k]['second_cate_pid'], $taskPrizeInfo[$k]['platform_ratio']
                            );
                            $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                        }
                        unset($taskPrizeInfo[$k]['redpacket_detail']);
                        unset($taskPrizeInfo[$k]['surplus_num']);


                    }

                    #积分兑换列表
                    Db::table('integral_exchange_prize')->insertAll($taskPrizeInfo);
                }



            }
        } else {
            #修改  传一个未修改之前的上架数量（旧的上架数量 5）  剩余数量（？） = 新值（10）- 旧的上架数量（5） = 5
            #查询 未修改之前的剩余数量
            $surplus_num = Db::table('integral_exchange')->where(array('id' => $params['id']))->value('surplus_num');
            $data['surplus_num'] =  $data['ground_num'] - $data['oldground_num']+$surplus_num;
            unset($data['oldground_num']);
            Db::table("integral_exchange")->where(array('id' => $params['id']))->update($data);

            #获取缓存
            $taskPrizeInfo = Session('integralExchangeInfo');
//            dump($params);
//            dump($taskPrizeInfo);
//            die;
            if(!empty($taskPrizeInfo)){
                # 删除原来的
                $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->value('id');
                if(!empty($spoil_id)){
                    Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                }
                #获取镜像  spoil_id：卡券或红包id
                foreach ($taskPrizeInfo as $k=>$v){
                    if($v['exchange_indate'] == '--'){
                        $taskPrizeInfo[$k]['exchange_indate'] = 0;
                    }
                    $taskPrizeInfo[$k]['integral_exchange_id']= $params['id'];
                    $taskPrizeInfo[$k]['spoil_id']= $taskPrizeInfo[$k]['pro_service_id'];
                    if($v['exchange_type'] == 2){
                        if($v['voucher_type'] == 1){
                            #卡券表中card_type_id 商品 3
                            $card_type_id = 3;

                        }else if($v['voucher_type'] == 2){
                            #卡券表中card_type_id 服务 1
                            $card_type_id = 1;


                        }
                        #类型为卡券时 上 card_voucher  查下卡券id 存上
                        $voucher_id  = Db::table('card_voucher')->where(array('card_type_id'=>$card_type_id,'server_id'=>$taskPrizeInfo[$k]['pro_service_id']))->value('id');
                        $taskPrizeInfo[$k]['spoil_id']= $voucher_id;

                    }
                    if(isset($v['pro_service_id']))
                        unset($taskPrizeInfo[$k]['pro_service_id']);
                    if($v['exchange_type'] == 3){
                        #存 redpacket 红包
                        $voucherId = Db::table('redpacket')->insertGetId(array(
                            'price'=>$v['price'],
                            'packet_title'=>$v['exchange_title'],
                            'type'=>3,
                            'create_time'=>date("Y-m-d H:i:s"),
                        ));
                        if($voucherId){
                            #task_prize 表 插入红包id
                            $taskPrizeInfo[$k]['spoil_id']= $voucherId;
                            foreach($v['redpacket_detail'] as $rk=>$rv){
                                $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                            }

                            Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                        }
                    }
                    if(in_array($v['exchange_type'],array(7, 8, 9, 10))){
                        # 删除原来的
                        $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->find();
                        if(!empty($spoil_id)){
                            Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                        }
                        $arr = array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                            "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']);
                        if ($v['is_type'] == 2) {
                            $arr['second_cate_pid'] = $v['second_cate_pid'];
                        }
                        $giving_id = Db::table("merchants_voucher")->insertGetId($arr);
                        unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'], $taskPrizeInfo[$k]['days_id'], $taskPrizeInfo[$k]['prize_type'], $taskPrizeInfo[$k]['card_time'],
                            $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'], $taskPrizeInfo[$k]['active_types'], $taskPrizeInfo[$k]['second_cate_pid']
                        );
                        $taskPrizeInfo[$k]['spoil_id']= $giving_id;
                    }
                     if (in_array($v['exchange_type'],array( 11, 12, 13, 14, 15, 16))) {
                         # 删除原来的
                         $spoil_id = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$params['id']))->find();
                         if(!empty($spoil_id)){
                             Db::table('integral_exchange_prize')->where(array('id'=>$spoil_id))->delete();
                         }
                        # 添加汽车服务类型卡券  11-服务通用 12-商品通用 13-全部通用 14-服务分类券 15-商品分类券 16-全部分类券
                        $arr = array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                            "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'],
                            "voucher_start" => $v['voucher_start'], 'platform_ratio' => $v['platform_ratio']);
                        if ($v['is_type'] == 14 or $v['is_type'] == 15 or $v['is_type'] == 16) {
                            $arr['cate_pid'] = $v['cate_pid'];
                            $arr['second_cate_pid'] = $v['second_cate_pid'];
                        }
                        $voucher_id = Db::table("merchants_voucher")->insertGetId($arr);
                        unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'], $taskPrizeInfo[$k]['days_id'],
                            $taskPrizeInfo[$k]['prize_type'], $taskPrizeInfo[$k]['card_time'],
                            $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'], $taskPrizeInfo[$k]['active_types'], $taskPrizeInfo[$k]['second_cate_pid'], $taskPrizeInfo[$k]['platform_ratio']
                        );
                         $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                    }
                    unset($taskPrizeInfo[$k]['redpacket_detail']);
                    unset($taskPrizeInfo[$k]['surplus_num']);

                }
//                dump($taskPrizeInfo);die;
                #积分兑换列表
                Db::table('integral_exchange_prize')->insertAll($taskPrizeInfo);
            }



        }
        #添加操作的时候删除活动缓存id  还有修改保存时
        Session::delete('exchangeId');
        #删除添加 商品 服务 会员 赠送 余额  缓存
        Session::delete('integralExchangeInfo');
        Session::delete('VoucherInfo');
        if(isset($params['banner'])) {
            $bannerArr = explode(',', $params['banner']);
        }

        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'integral_exchange'.$params['id'];
        #删除单图
        ResourceService::delCacheItem($params['imgurl']);
        #删除多图(数组形式)
        ResourceService::delCacheItem($bannerArr);
        return json(DataReturn('保存成功', 0));

    }
    /**
     * 保存前详情图数据处理
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params)
    {

        if (isset($params["detail"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'integral_exchange';
            $params['ueditor_content'] = $params['detail'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params['thumb_img'])) {
            unset($params['thumb_img']);
        }

        return $params;
    }


    /**
     *  taskSession 服务  商品  会员 积分 余额 存session
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    function integralSession($params,$taskId){
        #类型 积分  余额  还是卡券
        $type = $params['exchange_type'];
        if($type == 'pro'){
            #商品
            #名称
            $info['exchange_title']=$params['exchange_title'];
            $info['pro_service_id']=$params['pro_service_id'];
            #奖励积分 价值
            $info['price']=$params['detail_price'];
            #奖励类型
            $info['exchange_type']=2;
            $info['voucher_type']=1;

            #奖品数量
            $info['exchange_num']=$params['exchange_num'];

            #有效期
            $info['exchange_indate']=$params['exchange_indate'];


        }else  if($type == 'service'){
            #服务
            #名称
            $info['exchange_title']=$params['exchange_title'];
            $info['pro_service_id']=$params['pro_service_id'];
            #奖励积分 价值
            $info['price']=$params['detail_price'];
            #奖励类型
            $info['exchange_type']=2;
            $info['voucher_type']=2;
            #奖品数量
            $info['exchange_num']=$params['exchange_num'];

            #有效期
            $info['exchange_indate']=$params['exchange_indate'];
        }else if($type == 'packet'){
            #红包
            $info['exchange_title']=$params['packet_title'];
            #奖励积分 价值
            $info['price']=$params['price'];
            #奖励类型 5 服务
            $info['exchange_type']=3;
            $info['voucher_type']=0;

            #奖品数量
            $info['exchange_num']=1;
            #上架数量
            #有效期
            $info['exchange_indate']='365';

            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            $voucher = session('VoucherInfo');
            if(!empty($voucher)){
                #存详情字段  创建时 填到redpacket_detail  红包详情表
                $info['redpacket_detail'] = $voucher;
            }

        }else if($type == 'balance'){
            #余额 $id 是传过来的余额
            #名称
            $info['exchange_title']='奖励余额';
            #奖励积分 价值
            $info['price']=$params['price'];
            #奖励类型 3商品
            $info['exchange_type']=1;
            $info['voucher_type']=0;

            #奖品数量
            $info['exchange_num']=$params['price'];

            #有效期 默认
            $info['exchange_indate']='--';

        }else if($type == 'merchantVoucher' or $type == 'lifeVoucher' or $type == "propertyVoucher" or $type == "youpaiVoucher"){
            $info = $params;
            $info['exchange_type'] = $params['is_type'] == 2 ? 7 : ($params['is_type'] == 3 ? 9 : ($params['is_type'] == 4 ? 10 : 8));
        }

        ###########存缓存################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        #积分兑换只可兑换一种类型，不可多类型存在B、积分兑换只可兑换一种类型，不可多类型存在

        Session::set('integralExchangeInfo', array($info));
        $_sessionActiveTypeInfo = Session('integralExchangeInfo');
        $_str='';
      /*  if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        if(!empty($_sessionActiveTypeInfo)){
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' .$type. $info['pro_service_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['exchange_num'] = intval($_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']]['exchange_num']) + intval($info['exchange_num']);
            } else {

                $_sessionActiveTypeInfo['S' . $type.$info['pro_service_id']] = $info;
            }
        }else{
            $_sessionActiveTypeInfo['S' .$type. $info['pro_service_id']] = $info;

        }

        Session::set('integralExchangeInfo', $_sessionActiveTypeInfo);
        $_str = '';
        # 判断是否存在$taskId
        if(!empty($taskId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($taskId);

            $_str= self::getTaskPrizeInfo($pro, 'DB',$type);

        }*/
        /*getActiveArrInfo*/

        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }
    /**
     * @param $type  1 余额 2卡券 3红包
     *
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    function getIntegralTypeList(){
        #获取缓存活动id 只有修改时候有
        $activeId = Session('exchangeId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上
        $_str = '';

        $_sessionActiveTypeInfo = Session('integralExchangeInfo');
        #这种情况是 修改完成 之后 只能显示一种 类型
        if(!empty($_sessionActiveTypeInfo)){
            $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        }else{
            # 判断是否存在$activeId
            if(!empty($activeId)){
                #查询 商品 服务会员  统一放在数组里  返回
                $pro = self::getTaskContent($activeId);


                $_str= self::getTaskPrizeInfo($pro, 'DB');

            }else{
                $_sessionActiveTypeInfo = array();
                $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
            }
        }
        #积分兑换只可兑换一种类型，不可多类型存在

  /*      $_sessionActiveTypeInfo = Session('integralExchangeInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在$activeId
        if(!empty($activeId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($activeId);


            $_str= self::getTaskPrizeInfo($pro, 'DB');

        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');*/
        return $_str;

    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    function getTaskPrizeInfo($arr,$mark,$demo=0){

        $_str = '';
        if($demo)
        dump($arr);
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
              if($v['exchange_type'] == 1){
                    $_active_type_title = "余额";
                    $intbal = $v['price'].'余额';


                }else if($v['exchange_type'] == 2) {
                  if($v['voucher_type'] == 1){
                      $_active_type_title= "商品卡券";
                      $intbal = sprintf("%.2f",  $v['price']);
                  }else{
                      $_active_type_title= "服务卡券";
                      $intbal = sprintf("%.2f",  $v['price']);
                  }


                }else if($v['exchange_type'] == 3) {
                    $_active_type_title= "红包";
                    $intbal =sprintf("%.2f",  $v['price']);

                } else if (in_array($v['exchange_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                  $_active_type_title = givingTypeTitle($v['exchange_type']);
                  $intbal = priceFormat($v['price']);
                  $name = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], "merchantVoucher", false);
                  $title['title'] = $name['title'];
                  $validity_day = $name['validity_day'];
                  $min_consume = priceFormat($name['min_consume']);
                  $voucher_start = $name['voucher_start'] == 1 ? '是' : '否';
                  $platform_ratio = ($name['platform_ratio'] * 100) . '%';
                  # 查询对应的服务分类
                  if(!empty($name['cate_pid'])) {
                      $first = getServiceClassTitle($name['cate_pid']);
                      if (!empty($first)) {
                          $_active_type_title .= '-' . $first;
                      }
                  }
                  if(!empty($name['second_cate_pid'])) {
                      $second = getServiceClassTitle($name['second_cate_pid']);
                      if (!empty($second)) {
                          $_active_type_title .= '-' . $second;
                      }
                  }
              }


                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['exchange_title'] . "</td>
                            <td >$intbal</td>
                            <td>$_active_type_title</td>
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['exchange_num'] . "','" .$mark . "','3','" . $v['exchange_type']. "')\">" . $v['exchange_num'] . "</td>
                       
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['exchange_indate'] . "','" .$mark . "','5','" . $v['exchange_type'] . "')\">" . $v['exchange_indate'] . "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['exchange_type'] . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }
    /**
     * @param $taskId 奖励id
     * @content 积分兑换奖励详情
     */
    function getTaskContent($taskId){
        $task_prize = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$taskId))->select();
        foreach ($task_prize as $k=>$v) {
            if($v['exchange_type'] == 2){
                if($v['voucher_type'] == 1){
                    #商品 名称
                    $task_prize[$k]['exchange_title'] =  ActiveService::getCardVoucherProServiceTitle($v['spoil_id'],'pro');

                }else{
                    #服务 名称
                    $task_prize[$k]['exchange_title'] =  ActiveService::getCardVoucherProServiceTitle($v['spoil_id'],'service');
                }
            }else if($v['exchange_type'] == 4){
                #红包 名称
                $title= Db::table('redpacket')->field('packet_title')->where(array('id'=>$v['spoil_id']))->find();
                $task_prize[$k]['exchange_title'] = $title['packet_title'];

            }
        }
        return $task_prize;

    }
    /***********************抵用卡券显示***************************/
    function integralVoucherSession($params){
        #类型 积分  余额  还是卡券
        $type = $params['voucher_type'];
        $info = $params;


        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('VoucherInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        if(!empty($_sessionActiveTypeInfo)){
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' .$type. $info['voucher_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' .$type. $info['voucher_id']]['voucher_number'] = intval($_sessionActiveTypeInfo['S' .$type. $info['voucher_id']]['voucher_number']) + intval($info['voucher_number']);
            } else {

                $_sessionActiveTypeInfo['S' . $type.$info['voucher_id']] = $info;
            }
        }else{
            $_sessionActiveTypeInfo['S' .$type. $info['voucher_id']] = $info;

        }

        Session::set('VoucherInfo', $_sessionActiveTypeInfo);
        $_str = '';
        $packetId = Session('packetId');
        if(!empty($packetId)){
            $packet = PacketService::getPacketContent($packetId);
            $_str .= self::getVoucherArrInfo($packet, 'DB');
        }

        $_str .= self::getVoucherArrInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    function getVoucherArrInfo($arr,$mark){

        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                if($v['voucher_type'] == 2) {
                    $_title = '商品通用抵用券';
                } else if ($v['voucher_type'] == 4) {
                    $_title = "单独商品抵用券";
                } else if($v['voucher_type'] == 3){
                    $_title = "服务通用抵用券";


                }else if($v['voucher_type'] == 5) {
                    $_title= "单独服务抵用券";

                }else if($v['voucher_type'] == 6) {
                    $_title= "商品服务通用券";

                }else if($v['voucher_type'] == 1) {
                    $_title= "现金低值通用券";
                    $intbal = sprintf("%.2f",  $v['intbal']);

                }
                if($v['voucher_start'] == 1){
                    $voucher_start = '是';

                }else{
                    $voucher_start = '否';
                }


                $_str .= "<tr id='voucherDetailTr" . $v['id'] . "'>
                            <td >" . $v['voucher_title'] . "</td>
                            <td >$_title</td>
                            <td >" . sprintf("%.2f",  $v['voucher_price']) . "</td>
                            <td >" . sprintf("%.2f",  $v['money_off']) . "</td>
                            <td onclick=\"voucherWrite('" . $k . "','" . $v['voucher_over'] . "','5','" . $mark . "','" .  $v['id'] . "')\">" . $v['voucher_over']. "</td>
                            <td  onclick=\"voucherWrite('" . $k . "','" . $v['voucher_number'] . "','6','" . $mark . "','" .  $v['id'] . "')\">" . $v['voucher_number']. "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"voucherDelInfo('" . $k . "','" . $mark . "','" . $v['voucher_type'] . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    //查询 积分兑换表详情
    function getIntegralContent($exchangeId){
        $redpacket_detail = Db::table('integral_exchange_prize')->where(array('integral_exchange_id'=>$exchangeId))->select();

        return $redpacket_detail;
    }

}