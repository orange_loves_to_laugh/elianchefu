<?php


namespace app\service;

use Redis\Redis;
use think\Db;

/**
 * 俱乐部服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月9日10:01:25
 */
class ClubService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function ClubListWhere($params = [])
    {
        $where = [];

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['club_title', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        return $where;
    }
    /**
     * 获取俱乐部列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:01:25
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function ClubList($params)
    {

        $data=BaseService::DataList($params);
        return self::ClubDataDealWith($data);

    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:01:25
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function ClubDataDealWith($data){
        if(!empty($data))
        {
            $re=[];
            foreach($data as &$v)
            {


                // 内容
//                if(isset($v['club_content']))
//                {
//                    $v['club_content'] = ResourceService::ContentStaticReplace($v['club_content'], 'get');
//                }

//                // 图片
//                if(isset($v['club_picurl']))
//                {
//                    if(!empty($v['club_picurl']))
//                    {
//
//                        $v['club_picurl_show'] = ResourceService::AttachmentPathViewHandle($v['club_picurl']);
//
//                    }
//                }





            }
            $re['data']=$data;

        }
        return $data;
    }


    /**
     * 俱乐部保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ClubSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'club_title',
                'error_msg'         => '标题不能为空',
                'error_code'         => 40002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'club_author',
                'error_msg'         => '作者不能为空',
                'error_code'         => 40003,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'club_picurl',
                'error_msg'         => '缩略图不能为空',
                'error_code'         => 40004,
            ],

        ];
        /*
        # 添加是判断标题是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'news',
                'key_name'          => 'club_title',
                'error_msg'         => '标题不能重复',
                'error_code'         => 40001,
            ];
        }
        */
        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }

        $data=self::HandleData($params);


        // 添加/编辑
        if(empty($params['id']))
        {
            $id = Db::name('club')->insertGetId($data);
            # 发送消息提醒
            $IGeTuiService = new IGeTuiService();
            $IGeTuiService->MessageNotification(0,12,['title'=>$params['club_title']],true,false);
        } else {

            if(Db::name('club')->where(['id'=>intval($params['id'])])->update($data))
            {
                $id = $params['id'];
            }

        }
        if ($id>0) {
            return DataReturn('保存成功', 0);
        }

    }

    /**
     * 保存前数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params){


        //删除无用图片:单图
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'club'.$params['id'];
       // dump(session(ResourceService::$session_suffix));exit;
        ResourceService::delCacheItem($params['club_picurl']);

        if (isset($params["club_content"]))
        {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table']='club';
            $params['ueditor_content']= $params['club_content'];
            $re=ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params['thumb_img']))
        {
            unset($params['thumb_img']);
        }
        if (isset($params['old_imgurl']))
        {
            unset($params['old_imgurl']);
        }
       return $params;
    }

    static function changeNumService($params)
    {
        Db::table("club")->where(array("id"=>$params['id']))->update(array($params['mark']=>$params['val']));
        return array("status"=>true);
    }
}