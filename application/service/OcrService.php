<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/8/26
 * Time: 13:24
 */

namespace app\service;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use AlibabaCloud\SDK\ViapiUtils\ViapiUtils;

class OcrService
{
    function OcrIdCard($imgUrl)
    {
        if (!empty($imgUrl)) {
            AlibabaCloud::accessKeyClient('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW')
                ->regionId('cn-shanghai')
                ->asDefaultClient();
            try {
                $result = AlibabaCloud::rpc()
                    ->product('ocr')
                    // ->scheme('https') // https | http
                    ->version('2019-12-30')
                    ->action('RecognizeIdentityCard')
                    ->method('POST')
                    ->host('ocr.cn-shanghai.aliyuncs.com')
                    ->options([
                        'query' => [
                            'RegionId' => "cn-shanghai",
                            'ImageURL' => ViapiUtils::upload('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW', imgUrl($imgUrl)),
                            'Side' => "face",
                        ],
                    ])
                    ->request();
                $res = $result->toArray();
                return array('status' => true, 'sex' => $res['Data']['FrontResult']['Gender'],
                    'name' => $res['Data']['FrontResult']['Name'],
                    'IDNumber' => $res['Data']['FrontResult']['IDNumber']);
//                return $result->toArray();
            } catch (ClientException $e) {
                return array('status' => false, 'msg' => '请上传身份证正面照片');
//                return $e->getErrorMessage() . PHP_EOL;
            } catch (ServerException $e) {
                return array('status' => false, 'msg' => '请上传身份证正面照片');
//                return $e->getErrorMessage() . PHP_EOL;
            }
        } else {
            return array('status' => false, 'msg' => '请上传身份证照片');
        }
    }

    function OcrBankCard($imgUrl)
    {
        if (!empty($imgUrl)) {
            AlibabaCloud::accessKeyClient('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW')
                ->regionId('cn-shanghai')
                ->asDefaultClient();
            try {
                $result = AlibabaCloud::rpc()
                    ->product('ocr')
                    // ->scheme('https') // https | http
                    ->version('2019-12-30')
                    ->action('RecognizeBankCard')
                    ->method('POST')
                    ->host('ocr.cn-shanghai.aliyuncs.com')
                    ->options([
                        'query' => [
                            'RegionId' => "cn-shanghai",
                            'ImageURL' => ViapiUtils::upload('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW', imgUrl($imgUrl)),
                            'Side' => "face",
                        ],
                    ])
                    ->request();
                $res = $result->toArray();
                return array('status' => true, 'CardNumber' => $res['Data']['CardNumber']);
//                return $result->toArray();
            } catch (ClientException $e) {
                return array('status' => false, 'msg' => '请上传银行卡正面照片');
//                return $e->getErrorMessage() . PHP_EOL;
            } catch (ServerException $e) {
                return array('status' => false, 'msg' => '请上传银行卡正面照片');
//                return $e->getErrorMessage() . PHP_EOL;
            }
        } else {
            return array('status' => false, 'msg' => '请上传银行卡图片');
        }
    }

    function OcrVehicleLicense($imgUrl)
    {
        if (!empty($imgUrl)) {
            AlibabaCloud::accessKeyClient('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW')
                ->regionId('cn-shanghai')
                ->asDefaultClient();
            try {
                $result = AlibabaCloud::rpc()
                    ->product('ocr')
                    // ->scheme('https') // https | http
                    ->version('2019-12-30')
                    ->action('RecognizeDrivingLicense')
                    ->method('POST')
                    ->host('ocr.cn-shanghai.aliyuncs.com')
                    ->options([
                        'query' => [
                            'RegionId' => "cn-shanghai",
                            'ImageURL' => ViapiUtils::upload('LTAI4G9Q8mU7xavjS1zL1y4q', 'FQxWmiftABjugBx2S3zFtiH6yrZUDW', imgUrl($imgUrl)),
                            'Side' => "face",
                        ],
                    ])
                    ->request();
                $res = $result->toArray();
                return array('status' => true,
                    'carInfo' => array(
                        'address' => $res['Data']['FaceResult']['Address'],//住址
                        'engineNumber' => $res['Data']['FaceResult']['EngineNumber'],//发动机号码
                        'issueDate' => $res['Data']['FaceResult']['IssueDate'],//发证日期
                        'model' => $res['Data']['FaceResult']['Model'],//品牌型号 大众汽车牌SVW6666DFD
                        'owner' => $res['Data']['FaceResult']['Owner'],//所有人
                        'licensePlateNumber' => $res['Data']['FaceResult']['PlateNumber'],//车号牌号码
                        'registrationDate' => $res['Data']['FaceResult']['RegisterDate'],//注册日期
                        'useNature' => $res['Data']['FaceResult']['UseCharacter'],//使用性质 非营运
                        'vehicleType' => $res['Data']['FaceResult']['VehicleType'],//车辆类型 小型普通客车
                        'vinCode' => $res['Data']['FaceResult']['Vin']//车辆识别代号
                    )
                );
//                return $result->toArray();
            } catch (ClientException $e) {
                return array('status' => false, 'msg' => '请上传行驶证正面照片');
//                return $e->getErrorCode() . PHP_EOL.$e->getErrorMessage();
            } catch (ServerException $e) {
                return array('status' => false, 'msg' => '请上传行驶证正面照片');
//                return $e->getErrorCode() . PHP_EOL .$e->getErrorMessage();
            }
        } else {
            return array('status' => false, 'msg' => '请上传行驶证图片');
        }
    }
}
