<?php


namespace app\service;

use think\Db;

/**
 * 薪资管理服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年12月22日11:42:35
 */
class SalaryService
{
    /**
     * 薪资列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月22日11:42:35
     * @param    [array]          $params [输入参数]
     */
    public static function SalaryListWhere($params = [])
    {

        $where = [];

        //年份&&月份筛选
        if (!empty($params['param']['month']) || !empty($params['param']['year'])) {
            $params['param']['month'] = strlen($params['param']['month']) < 2 ? '0' . $params['param']['month'] : $params['param']['month'];
            $start = $params['param']['year'] . '-' . $params['param']['month'] . '-01';
            $end = date('Y-m-d', strtotime($start . '+1month -1day'));

            $where[] = ['salary_time', '>=', $start];
            $where[] = ['salary_time', '<=', $end];
        }
        //默认时间
        if (empty($params['param'])) {
            $time = Db::name('employee_salary_sal')->where('id > 0')->order('salary_time desc')->value('salary_time');

            if (!empty($time)) {
                $start = date('Y-m-01', strtotime($time));
                $end = date('Y-m-d', strtotime($start . '+1month -1day'));

                $where[] = ['salary_time', '>=', $start];
                $where[] = ['salary_time', '<=', $end];
            }
        }

        return $where;
    }

    /**
     * 获取最近的查询或创建薪资数据的时间
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月22日13:11:58
     * @desc    description
     */
    public static function GetRecentTime()
    {
        //当前月 下个月 上个月
        $time = Db::name('employee_salary_sal')->where('id > 0')->order('salary_time desc')
            ->value('salary_time');
        $time_now = date('Y-m-01', time());
        //$time='2020-11-11';
        if (empty($time))//工资表为空
        {
            $time_pre = date('Y-m-01', strtotime($time_now . '-1 month'));
            $time = $time_now;

        } else {//工资表有数据
            //2020-11-11
            $time_pre = date('Y-m-01', strtotime($time . '+1month'));

        }

        return ['time_pre' => $time_pre, 'time_now' => $time_now, 'time_default' => $time];
    }

    /**
     * 获取数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月22日13:11:58
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function SalaryDataList($params)
    {
        $data = BaseService::DataList($params);
        if (!empty($data)) {
            foreach ($data as &$v) {
                //员工姓名
                if (isset($v['employee_id']) && $v['employee_id'] > 0) {

                    $v['employee_name'] = Db::name('employee_sal')->where('id=' . $v['employee_id'])->value('employee_name');
                }

                //所属岗位
                $v['station_title'] = EmployeeService::StationDataHandle($v['station_id']);


                //员工状态
                if (isset($v['employee_status'])) {
                    $v['employee_status_title'] = BaseService::StatusHtml($v['employee_status'], [1 => '试用期', 2 => '正式员工', 0 => '离职'], false);
                }
            }
        }
        return $data;
    }

    /**
     * 计算薪资数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月22日13:11:58
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function CalcSalaryData($searchTime='')
    {

        if(!empty($searchTime)){
            $time_pre = $searchTime;
        }else{
            $time = self::GetRecentTime();
            $time_pre = $time['time_pre'];
        }

        $where = "date_format(employee_create,'%Y-%m') <='".date('Y-m',strtotime($time_pre))."'
        and if(employee_status=0,(date_format(quit_time,'%Y-%m-%d')>'".date('Y-m-01',strtotime($time_pre))."'),1)";
        $employee_data = Db::name('employee_sal')->where($where)->order('id desc')->select();

        $employee_data = EmployeeService::EmployeeDataDealWith($employee_data);



        $data = [];

        foreach ($employee_data as $k => $v) {
            //应出勤天数
            $v['fullattendance_days'] = date('d', strtotime($time_pre . "+1month -1day")) - $v['employee_hugh'];
            # 查询当月是否存在绩效考核
            $assessment = Db::table('employee_check')->where(array('employee_id' => $v['id']))
                ->where("date_format(check_time,'%Y-%m') = '" . date('Y-m', strtotime($time_pre)) . "'")
                ->find();
            if (!empty($assessment)) {
                # 绩效考核-> 计算提成
                $deductInfo = self::employeeDeduct($v['id'], true, date('Y-m', strtotime($time_pre)), $assessment['id']);
            } else {
                # 没有考核 , 正常计算提成
                $deductInfo = self::employeeDeduct($v['id'], false, date('Y-m', strtotime($time_pre)));
            }
            //提成
            $v['percentage'] = priceFormat($deductInfo['deduct']);
            //performance_salary 绩效工资
            $v['performance_salary'] = priceFormat($deductInfo['meritPay']);
            //system_punish  系统处罚
            $v['system_punish'] = priceFormat($deductInfo['meritPunish']);
            //创建薪资时间
            $v['salary_time'] = $time_pre;

            $start_day = strtotime($time_pre . '+1day');

            $end_day = strtotime($time_pre . '+1month -2day');

            if (strtotime($v['employee_create']) >= $start_day && strtotime($v['employee_create']) <= $end_day) {
                $v['in_salary_time'] = 1;
            } else {
                $v['in_salary_time'] = 2;
            }

            //满勤薪资
            if ($v['employee_status'] == 2) {
                $v['fullattendance_salary'] = $v['regular_fullattendance_salary'];
                $v['base_salary'] = $v['regular_base_salary'];
                $v['station_salary'] = $v['regular_station_salary'];
            }

            //离职状态
            if ($v['employee_status'] < 1) {
                if (strtotime($v['quit_time']) < strtotime($v['salary_time'])) {
                    unset($v);
                    continue;
                }

            }
            $data[$k] = $v;

        }

        return $data;

    }

    /**
     * @param $employeeId
     * @param $assessment
     * @param $time
     * @param int $checkId
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 员工提成
     */
    public static function employeeDeduct($employeeId, $assessment, $time, $checkId = 0)
    {
        // $assessment  true=>考核  false=>没有考核
        # 是否完成 办卡额考核->办卡额额外奖励 2%
        $cardDeductStatus = true;
        # 是否完成 平台抽成金额考核-> 平台抽成金额 额外奖励 2%
        $commissionDeductStatus = true;
        # 绩效工资
        $meritPay = 0;
        # 绩效处罚(系统处罚)
        $meritPunish = 0;
        # 本身提成
        $selfDeduct = Db::table('deduct')
            ->where('employee_id = ' . -$employeeId . ' and date_format(`create`,"%Y-%m") = "' . $time . '"')
            ->sum('deduct');
        # 商家推广成为优质商家的金额的5%->改成10%
        $highQuality = Db::table('merchants_order mero')
            ->join('merchants_orderdetail merod', 'mero.order_number = merod.order_number', 'left')
            ->where("merchants_level in (1,2)")
            ->where(array('employee_sal' => $employeeId, 'order_status' => 2, 'pay_type' => 5))
            ->where("date_format(mero.create_time,'%Y-%m') = '" . $time . "'")->sum('actual_price');
        $highQualityDeduct = $highQuality * 0.1;
        if ($assessment) {
            //  考核
            # 查询考核详情
            $checkList = Db::table('employee_check_relate')->where(array('employee_check_id' => $checkId))->select();
            if (!empty($checkList)) {
                // type =>  关联类型：1销售额 2办卡额 3办卡数量 4 到店率5 好评率 6平台抽成金额
                foreach ($checkList as $k => $v) {
                    # 考核结果
                    $checkResult = array();
                    if ($v['type'] == 1) {
                        # 销售额考核
                        # 查询关联门店.商户的销售额(实际结算金额)
                        $settlementBiz = Db::table('biz_settlement')
                            ->where(array('employee_sal' => $employeeId, 'desposit_status' => 2))
                            ->where("date_format(create_time,'%Y-%m') = '" . $time . "'")
                            ->sum('price');
                       /* # 商户销售额
                        $settlementMerchants = Db::table('merchants_income')->where(array('employee_sal' => $employeeId))
                            ->where("date_format(checkout_time,'%Y-%m') = '" . $time . "'")
                            ->sum('price');*/
                        $settlementTotal = $settlementBiz;
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($settlementTotal, $v);
                    } elseif ($v['type'] == 2) {
                        # 办卡额考核
                        # 查询关联门店/商户  办卡金额
                        $applyMemberPrice = Db::table('log_handlecard')->where(array('employee_sal' => $employeeId))
                            ->where("date_format(member_create,'%Y-%m') = '" . $time . "'")
                            ->sum('level_price');
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($applyMemberPrice, $v);
                    } elseif ($v['type'] == 3) {
                        # 办卡数量
                        # 查询关联门店/商户  办卡金额
                        $applyMemberPrice = Db::table('log_handlecard')->where(array('employee_sal' => $employeeId))
                            ->where("date_format(member_create,'%Y-%m') = '" . $time . "'")
                            ->count();
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($applyMemberPrice, $v);
                    } elseif ($v['type'] == 4) {
                        # 到店率
                        # 查询关联门店的到店率
                        $bizOrder = Db::table('orders')->where(array('employee_sal' => $employeeId, 'order_status' => 5))
                            ->where("date_format(order_over,'%Y-%m') = '" . $time . "'")
                            ->count();
                        /*# 查询关联商户的到店率
                        $merchantsOrder = Db::table('merchants_order')->where(array('employee_sal' => $employeeId, 'order_status' => 2))
                            ->where("pay_type != 5 and date_format(checkout_time,'%Y-%m') = '" . $time . "'")->count();*/
                        $orderTotal = $bizOrder;
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($orderTotal, $v);
                    } elseif ($v['type'] == 5) {
                        # 好评率
                        #查询门店订单好评率 (好评数量)
                        $bizPraise = Db::table('orders o')
                            ->join('evaluation_score es', 'o.id=es.order_id', 'left')
                            ->where(array('employee_sal' => $employeeId, 'order_status' => 5, 'score' => 5))
                            ->where("date_format(order_over,'%Y-%m') = '" . $time . "'")
                            ->group('orderserver_id')
                            ->count();
                       /* # 关联商户订单好评率  (好评数量)
                        $merchantsPraise = Db::table('merchants_order mo')
                            ->join('merchants_evaluation mv', 'mv.merchants_order_id = mo.id', 'left')
                            ->where("evalua_level in (4,5)")
                            ->where(array('employee_sal' => $employeeId, 'order_status' => 2))
                            ->where("pay_type != 5 and date_format(checkout_time,'%Y-%m') = '" . $time . "'")
                            ->count();*/
                        $praiseTotal = $bizPraise;
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($praiseTotal, $v);
                    } elseif ($v['type'] == 6) {
                        # 平台抽成金额
                        #查询门店订单抽成金额
                        $bizcommission = Db::table('biz_settlement')
                            ->where(array('employee_sal' => $employeeId, 'desposit_status' => 2,'type'=>1))
                            ->where("date_format(create_time,'%Y-%m') = '" . $time . "'")
                            ->sum('commission_price');
                        # 查询商户订单抽成金额
                        $merchantscommission = Db::table('merchants_order')
                            ->where(array('employee_sal' => $employeeId, 'order_status' => 2))
                            ->where("cash_type != 3 and cash_type != 4 and date_format(checkout_time,'%Y-%m') = '" . $time . "'")
                            ->sum('commission_price');
                        $commissionTotal = $bizcommission + $merchantscommission;
                        # 考核结果
                        $checkResult[$v['type']] = self::checkResult($commissionTotal, $v);
                    }
                    # 考核结果处理(计算提成/奖励/处罚)
                    foreach ($checkResult as $rk => $rv) {
                        if ($rv['complete'] == 0) {
                            # 未完成
                            if ($rv['is_punish'] == 1) {
                                # 处罚
                                $meritPunish += $rv['punish'];
                            }
                            if ($rv['is_reward'] == 2) {
                                # 没完成不给提成
                                if ($rk == 2) {
                                    $cardDeductStatus = false;
                                }
                                if ($rk == 6) {
                                    $commissionDeductStatus = false;
                                }
                            }
                        } elseif ($rv['complete'] == 1) {
                            # 完成  奖励
                            $meritPay += $rv['reward'];
                        } else {
                            # 关联考核
                            if (!empty($rv['checkRelate'])) {
                                # 判断关联是否有没完成的
                                $relation = true;
                                foreach ($rv['checkRelate'] as $ck => $cv) {
                                    if ($checkResult[$cv]['complete'] == 0) {
                                        # 未完成
                                        $relation = false;
                                        break;
                                    }
                                }
                                if ($relation) {
                                    # 全部完成,奖励
                                    $meritPay += $rv['reward'];
                                } else {
                                    # 未完成,罚款
                                    if ($rv['is_punish'] == 1) {
                                        # 处罚
                                        $meritPunish += $rv['punish'];
                                    }
                                    if ($rv['is_reward'] == 2) {
                                        # 没完成不给提成
                                        if ($rk == 2) {
                                            $cardDeductStatus = false;
                                        }
                                        if ($rk == 6) {
                                            $commissionDeductStatus = false;
                                        }
                                    }
                                }
                            } else {
                                # 关联考核信息为空,给奖励
                                $meritPay += $rv['reward'];
                            }
                        }
                    }
                }
            }
        }
        # 总提成(本身+ 商家推广5% + 关联办理会员2% + 关联抽成 2%->(2021-05-18 13:07 改成 10%))
        $deduct = $selfDeduct + $highQualityDeduct;
        if ($cardDeductStatus) {
            # 关联的门店/商户  办理会员金额的 2% (关联考核)
            $applyMember = Db::table('log_handlecard')->where(array('employee_sal' => $employeeId))
                ->where('date_format(member_create,"%Y-%m") = "' . $time . '"')
                ->sum('level_price');
            $applyMemberDeduct = $applyMember * 0.02;
            $deduct += $applyMemberDeduct;
        }
        if ($commissionDeductStatus) {
            # 抽成金额的2% (关联考核)
            $relationBizCut = Db::table('biz_settlement')
                ->where('type = 1 and date_format(create_time,"%Y-%m") = "' . $time . '"
             and employee_sal = ' . $employeeId)
                ->sum('commission_price');
            $relationBizCutDeduct = $relationBizCut * 0.1;
            $deduct += $relationBizCutDeduct;
        }
        return array('deduct' => $deduct, 'meritPay' => $meritPay, 'meritPunish' => $meritPunish);
    }

    private static function checkResult($total, $v)
    {
        # 关联考核信息
        $checkRelate = '';
        # 判断是否完成
        if ($total >= $v['todo_num']) {
            # 完成了,判断是否有关联考核
            if ($v['relate_check_ids'] == 1) {
                # 查询关联的考核内容
                $checkRelate = Db::table('employee_check_relate_check')
                    ->where(array('employee_check_relate_id' => $v['id']))->column('related_type');
                $complete = 2;
            } else {
                # 没有关联 , 奖励
                $complete = 1;
            }
        } else {
            # 没完成,罚款
            $complete = 0;
        }
        return array(
            'id'=>$v['id'],
            'total' => $total,//完成总数
            'complete' => $complete,//完成状态  0未完成  1完成   2须关联其他考核信息
            'checkRelate' => $checkRelate,//关联考核信息
            'reward' => $v['reward_price'],//奖励金额
            'punish' => $v['undo_punish_price'],//处罚金额
            'is_punish' => $v['undo_punish'],//1 是  2否
            'is_reward' => $v['undo_percentage'],//未完成是否给提成
        );
    }

    /**
     * 薪资数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date     2020年12月16日11:47:57
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DataSave($params = [])
    {


        // 添加/编辑
        if (empty($params['id'])) {
            $data = [];

            $id_arr = Db::name('employee_sal')->where([['employee_create', '<', date('Y-m-01', time())]])->column('id');;
            foreach ($id_arr as $k => $v) {
                if(array_key_exists('department_id_' . $v,$params)) {
                    $arr = [
                        'employee_id' => $v,
                        'department_id' => $params['department_id_' . $v],
                        'station_id' => $params['employee_station_id_' . $v],
                        'employee_status' => $params['employee_status_' . $v],
                        'salary_time' => date('Y-m-d', strtotime($params['salary_time_' . $v])),
                        'base_salary' => $params['base_salary_' . $v],
                        'station_salary' => $params['station_salary_' . $v],
                        'fullattendance_days' => $params['fullattendance_days_' . $v],
                        'attendance_days' => $params['attendance_days_' . $v],
                        'fullattendance_salary' => $params['fullattendance_salary_' . $v],
                        'action_salary' => $params['action_salary_' . $v],
                        'performance_salary' => $params['performance_salary_' . $v],
                        'percentage' => $params['percentage_' . $v],
                        'bonus' => $params['bonus_' . $v],
                        'system_punish' => $params['system_punish_' . $v],
                        'company_punish' => $params['company_punish_' . $v],
                        'actually_salary' => $params['actually_salary_' . $v],
                    ];
                    $data[$k] = $arr;
                }
            }

            //
            Db::startTrans();


            $re = Db::name('employee_salary_sal')->insertAll($data);
            //dump($data);exit;
            if (!$re) {
                Db::rollback();
                throw new \BaseException(['code' => 403, 'errorCode' => 21001, 'msg' => '创建工资表失败', 'status' => false, 'debug' => false]);
            }
            //dump($data);exit;
            Db::commit();
        } else {

            $re = Db::name('employee_salary_sal')->where(['id' => intval($params['id'])])->update($params);
            if (!$re) {
                throw new \BaseException(['code' => 403, 'errorCode' => 21002, 'msg' => '保存工资信息失败', 'status' => false, 'debug' => false]);
            }
        }

        return DataReturn('保存成功', 0);

    }
}
