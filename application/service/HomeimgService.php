<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 10:28
 */

namespace app\service;
/**
 * 首页图片展示服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Request;
use think\facade\Session;
class HomeimgService
{
    /**
     * [ApplayIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    public static function imgIndex($params){
        #where条件  写成活的
        $where = empty($params['where']) ? ['type'>'0'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('home_imgurl')->where($where)->order('end_time desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        #处理方法
        return self::InforDataDealWith($data,$params);
    }
    /*
     * @content ：处理方法
     * */
    static function InforDataDealWith($data,$params){
        $time = date("Y-m-d H:i:s");
        $time = strtotime($time);
        if(!empty($data)){
            foreach($data as $k=>$v){
                if($time < strtotime($v['start_time']) and $time< strtotime($v['end_time'])){
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['active_status'] = '待上架';

                }else if($time >= strtotime($v['start_time']) and $time<= strtotime($v['end_time'])){
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['active_status'] = '已上架';

                }else if($time > strtotime($v['start_time']) and $time>strtotime($v['end_time'])){
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['active_status'] = '已结束';

                }

            }
        }
        return $data;
    }
    /**
     * 总数
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function imgIndexCount(){
        return (int) Db::name('home_imgurl')->count();
    }
    /**
     * [Save 首页 图片添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function Save($params){
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '上架时间不能为空',
                'error_code'         => 30003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'end_time',
                'error_msg'         => '下架时间不能为空',
                'error_code'         => 30004,
            ],




        ];
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'imgurl',
                'error_msg' => '请上传缩略图图',
                'error_code' => 30001,
            ];


        }
        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        #处理百度编辑器删除多余图片问题（上传了但是没有点击保存 或者删除了 又重新上传）
        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        }
        if (isset($data['old_imgurl'])) {
            unset($data['old_imgurl']);
        }
        # 查询数据库中是否有值  有的情况：当前类型图片 上架的 只有一个  并且 上架的开始时间 结束时间和 数据库中的 上架 下架 不能有交集 要同时大于数据库中的 结束时间 或同时 小于 数据库中的 上架时间   没有的情况
        $start_time = date_create($params['start_time']);
        $end_time = date_create($params['end_time']);
        $start_time =date_format($start_time,"Y/m/d H:i:s");
        $start_time = strtotime($start_time);
        $end_time =date_format($end_time,"Y/m/d H:i:s");
        $end_time = strtotime($end_time);
        $type = $params['type'];
        $id = $params['id'];
        $_where =  '';
        if (!empty($id)){
            $_where .= " id != $id";

        }

        $data_active = Db::table('home_imgurl')->where(" type = $type")->where($_where)->select();

        if(!empty($data_active)){
            #创建的开始时间 和结束时间 都不能和 原有数据的开始时间和结束时间有交集
            foreach ($data_active as &$v){
                if(($start_time >= strtotime($v['end_time']) and $end_time >strtotime($v['end_time']))or ($start_time < strtotime($v['start_time']) and $end_time<= strtotime($v['start_time']) )){
                    #符合证明新上传的时间和数据库中没有交集

                }else{
                    #如果有一种不符合 就会走这
                    $code = "保存的时间和已有的时间相重复!,10001";
                    $error_arr = explode(',',$code );
                    return json(DataReturn($error_arr[0], $error_arr[1]));
                }

            }
        }


        // 添加/编辑
        if (empty($params['id'])) {

            #添加处理
            $activeId = Db::table("home_imgurl")->insertGetId($data);

        } else {
            #修改
            Db::table("home_imgurl")->where(array('id' => $params['id']))->update($data);

        }
        #删除前端缓存
        $_redis = new Redis();
        if($type == 6){
            #注册弹窗
            $_redis->hDel("pop","noregister");

        }

        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'home_imgurl'.$params['id'];
        #删除单图
        ResourceService::delCacheItem($params['imgurl']);


        return json(DataReturn('保存成功', 0));


    }
}