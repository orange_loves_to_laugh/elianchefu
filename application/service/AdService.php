<?php


namespace app\service;

use think\Db;

/**
 * 管理员服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 */
class AdService
{
    /**
     * 管理员登录
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年12月1日11:26:02
     * @param    [array]          $params [输入参数]
     */
    public static function Login($params = [])
    {
//        $re=Db::name('admin')->where('id=1')
//            ->update([
//                'login_salt'=>'492881',
//                'username'=>'Elianadmin',
//                'login_pwd'=>'55b05f760aa3014b377ef892dafb5945'
//            ]);
//        dump($re);exit;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '用户名不能为空',
                'error_code'         => 16001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'password',
                'error_msg'         => '密码不能为空',
                'error_code'         => 16002,
            ],
            [
                'checked_type'      => 'fun',
                'key_name'          => 'username',
                'checked_data'      => 'CheckUserName',
                'error_msg'         => '用户名格式 5~18 个字符（可以是字母数字下划线）',
                'error_code'         => 16003,
            ],
            [
                'checked_type'      => 'fun',
                'key_name'          => 'password',
                'checked_data'      => 'CheckLoginPwd',
                'error_msg'         => '密码格式 6~18 个字符',
                'error_code'         => 16004,
            ],
        ];
        $ret = ParamsChecked($params, $p);
        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

        }

        // 获取管理员
        $admin = Db::name('admin')->field('id,username,login_pwd,login_salt,mobile,login_total,role_id')
            ->where(['username'=>$params['username']])->find();

        if(empty($admin))
        {

            throw new \BaseException(['code'=>403 ,'errorCode'=>16005,'msg'=>'管理员不存在','status'=>false,'debug'=>false]);
        }

        // 密码校验
        $login_pwd = LoginPwdEncryption($params['password'], $admin['login_salt']);

        //c24cc6212f88663db13f763a486b136c


        if($login_pwd != $admin['login_pwd'])
        {

            throw new \BaseException(['code'=>403 ,'errorCode'=>16006,'msg'=>'密码错误','status'=>false,'debug'=>false]);
        }

        // 校验成功
        // session存储
        session('admin', $admin);

        // 返回数据,更新数据库
        if(session('admin') != null)
        {
            $login_salt = GetNumberCode(6);
            $data = array(
                'login_salt'    =>  $login_salt,
                'login_pwd'     =>  LoginPwdEncryption($params['password'], $login_salt),
                'login_total'   =>  $admin['login_total']+1,
                'login_time'    =>  TIMESTAMP,
            );
            if(Db::name('admin')->where(['id'=>$admin['id']])->update($data))
            {
                // 清空权限缓存数据
                //cache(config('cache_admin_left_menu_key').$admin['id'], null);
               // cache(config('cache_admin_power_key').$admin['id'], null);

                return DataReturn('登录成功',0);
            }
        }

        // 失败
        session('admin', null);
        throw new \BaseException(['code'=>403 ,'errorCode'=>16007,'msg'=>'登录失败，请稍后再试！','status'=>false,'debug'=>false]);

    }
}