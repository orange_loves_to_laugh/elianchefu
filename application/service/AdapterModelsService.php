<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/28
 * Time: 13:43
 */

namespace app\service;


use think\Db;

class AdapterModelsService
{
    /**
     * @param null $search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取所有vin的车辆品牌
     */
    function getsVinLogo($search=null)
    {
        $where=" id >0 ";
        if (!empty($search)) {
            $where .= " and logo_title like '%".$search."%'";
        }
        $list=Db::table("vin_carlogo")->where($where)->order("initial")->select();
        return array("data"=>$list);
    }

    /**
     * @param $logo_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取品牌下的车系列
     */
    function getsVinSort($logo_id)
    {
        $order="CONVERT(sort_title USING gbk)";
        $list=Db::table("vin_carsort")->where(array("car_logo"=>$logo_id))->orderRaw($order)->select();
        return array("data"=>$list);
    }

    /**
     * @param $sort_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取系列下的款式
     */
    function getsVinGroup($sort_id)
    {
        $order=" yeartype,CONVERT(grouptitle USING gbk)";
        $list=Db::table("vin_group")->where(array("sort_id"=>$sort_id))->orderRaw($order)->select();
        return array("data"=>$list);
    }

    /**
     * @param $oe_number
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查看适配车型列表
     */
    function checkAdapterModels($oe_number)
    {
        $list=Db::table("oe_adapt oa")
            ->field("if(oa.car_logo=0,'全部品牌',vl.logo_title) logo_title,if(oa.car_sort=0,'全部系列',vs.sort_title) sort_title,
            if(oa.car_group=0,'全部型号',vg.grouptitle) group_title")
            ->join("vin_carlogo vl","oa.car_logo=vl.id",'left')
            ->join("vin_carsort vs","oa.car_sort=vs.id","left")
            ->join("vin_group vg","oa.car_group=vg.id","left")
            ->where(array("oe_number"=>$oe_number))
            ->order("oa.car_logo,oa.car_sort,oa.car_group")
            ->select();
        return array("data"=>$list);
    }

}