<?php


namespace app\service;

/**
 * 采购服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月6日16:16:31
 */
class PurchaseService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @param    [array]          $params [输入参数]
     */
    public static function PurchaseListWhere($params = [])
    {
        $where = [];

        //时间
        if(!empty($params['param']['keywords']))
        {
            $where[] =['club_title', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        return $where;
    }
}