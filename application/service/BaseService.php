<?php


namespace app\service;


use think\Db;

/**
 * 基础服务层
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年7月22日10:57:35
 */
class BaseService
{
    /**
     * 获取数据
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DataList($params = [])
    {
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'id desc';
        $group = isset($params['group']) ? $params['group'] : 'id';
        $field = empty($params['field']) ? '*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;
        $wheres = empty($params["wheres"])? NULL : $params["wheres"];
        if(!empty($params['whereTime'])){
            $data = Db::name($params['table'])->where($where)->where($wheres)->whereTime($params['create_time'],$params['whereTime'])->field($field)->order($order)->group($group);

        }else{

            $data = Db::name($params['table'])->where($where)->where($wheres)->field($field)->order($order)->group($group);

        }

        // 获取列表

        if (!empty($params['group']))
        {
            $data = $data->group($params['group']);
        }

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }

        return $data;

    }

    /**
     * 总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function DataTotal($table,$where){


        return (int) Db::name($table)->where($where)->count();
    }



    /**
     * [删除数据].
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日17:02:36
     */
    public static function DelInfo($params)
    {

        //判断是否是等级分类
        $id = $params['id'];
        $id = explode(',', $id);
        $where_del = [['id' ,'in', $id]];

        Db::startTrans();
        if($params['soft'])
        {

            $re= Db::name($params['table'])->where($where_del)->update($params['soft_arr']);

        }else{
            $re= Db::name($params['table'])->where($where_del)->delete();
        }
        if (!$re)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>$params['errorcode'],'msg'=>$params['msg'],'status'=>false,'debug'=>false]);
        }
        Db::commit();
        return true;
    }
    /**
     * [状态html判断].
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年7月21日17:02:36
     * @param    [int]          $field [需要对比的数据]
     * @param    [array]          $field_map [查找数据的范围]
     * @param    [bool]          $is_html [是否需要渲染为html button]
     */
    public static function StatusHtml($field,$field_map,$is_html=true){
        $buttons=lang('button_list');
        $re=null;
        foreach ($field_map as $k=>$v)
        {
            if ($field==$k)
            {
                if ($is_html) {
                    $str=$k-1;
                    if ($k>count($buttons))
                    {
                        $str=$k%7;
                    }
                    $re='<button type="button" class="btn '.$buttons[$str].'">'.$v.'</button>';
                }else{
                    $re =$v;
                }

            }

        }

        return $re;

    }
    /*
     * 轮播图 去除逗号
     */
    public static function HandleImg($active_picurl){
        $active_picurl=substr($active_picurl,0,1)==','?substr($active_picurl,1):$active_picurl;

        $bannerArr=[];
        if (!empty($active_picurl))
        {
            $bannerArr = explode(',',$active_picurl);
        }


        return $bannerArr;
    }

}
