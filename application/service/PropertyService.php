<?php


namespace app\service;


use Redis\Redis;
use think\Db;

class PropertyService
{
    /**
     * @param $params
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查看物业绑定房屋信息
     */
    static function checkHouse($params)
    {
        $list = array();
        # 查询数据
        if(!empty($params['id'])){
            $list = Db::table("property_house ph")
                ->field("ph.id,pb.build_title,pu.title unit_title,ph.level,ph.house_code,ph.area,pb.property_price,ph.member_title,
                convert((ph.area*pb.property_price),decimal(10,2)) count_price,'DB' mark,date(ph.price_endtime) price_endtime")
                ->leftJoin("property_unit pu","ph.unit_id = pu.id")
                ->leftJoin("property_building pb","pb.id = ph.building_id")
                ->where(array("ph.neighbourhood_id"=>$params['id'],"ph.del_status"=>2))
                ->select();
        }
        $newHouse = Session("building");
        if(!empty($newHouse)){
            foreach($newHouse as $k=>$v){
                if(!empty($v['unity_array'])){
                    foreach($v['unity_array'] as $uk=>$uv) {
                        if(!empty($uv['house_array'])){
                            foreach($uv['house_array'] as $hk=>$hv){
                                array_push($list,array("build_title"=>$v['build_title'],"unit_title"=>$uv['title'],"level"=>$hv['level'],
                                    "house_code"=>$hv['house_code'],
                                    "area"=>$hv['area'],"property_price"=>$v['property_price'],"count_price"=>$v['property_price']*floatval($hv['area']),
                                    "id"=>$hk,"mark"=>"S","price_endtime"=>$hv['price_endtime'],"member_title"=>$hv['member_title']));
                            }
                        }

                    }
                }
            }
        }
        return $list;
    }

    /**
     * @param $params
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 修改到期时间
     */
    static function  confirmUpdateTime($params){
        if($params['mark']=='S'){
            $build = Session("building");
            $build[$params['build_title']]['unity_array'][$params['unit_title']]['house_array'][$params['id']]['price_endtime'] = $params['val'];
            Session("building",$build);
        }else{
            Db::table("property_house")->where(array("id"=>$params['id']))->update(array("price_endtime"=>$params['val']));
        }
        return array("status"=>true);
    }
    /**
     * @param $params
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 修改到期时间
     */
    static function  confirmUpdateMember($params){
        if($params['mark']=='S'){
            $build = Session("building");
            $build[$params['build_title']]['unity_array'][$params['unit_title']]['house_array'][$params['id']]['member_title'] = $params['val'];
            Session("building",$build);
        }else{
            Db::table("property_house")->where(array("id"=>$params['id']))->update(array("member_title"=>$params['val']));
        }
        return array("status"=>true);
    }

    /**
     * @param $params
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除房产
     */
    static function deleteHouse($params){
        if($params['mark']=='S'){
            $build = Session("building");
            unset($build[$params['build_title']]['unity_array'][$params['unit_title']]['house_array'][$params['id']]);
            Session("building",$build);
        }else{
            Db::table("property_house")->where(array("id"=>$params['id']))->update(array("del_status"=>1));
        }
        return array("status"=>true);
    }

    static function SaveData($params)
    {
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'nh_title',
                'error_msg'         => '名称不能为空',
                'error_code'         => 12001,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '联系人不能为空',
                'error_code'         => 12002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'telphone',
                'error_msg'         => '联系电话不能为空',
                'error_code'         => 12003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'nh_lon',
                'error_msg'         => '经度不能为空',
                'error_code'         => 12004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'nh_lat',
                'error_msg'         => '纬度不能为空',
                'error_code'         => 12004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'address',
                'error_msg'         => '地址不能为空',
                'error_code'         => 12004,
            ],
            [
                'checked_type'      => 'max',
                'checked_data'      =>1,
                'key_name'          => 'draw_prop',
                'error_msg'         => '抽成比例必须小于1',
                'error_code'         => 12009,
            ],

        ];

        $ret = ParamsChecked($params, $p);


        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        # 营业执照 单图处理
        self::HandleCache('business_license',$params);
        # 详情图多图处理
        if(isset($params['nh_detailimg'])){
            $params['nh_detailimgs'] = $params['nh_detailimg'];
            #*号拼接成字符串
            $params['nh_detailimg'] = implode('*',explode(',',$params['nh_detailimg']));
        }
        # 园区数据处理
        $field = self::neighbourhoodField();
        $data =array();
        foreach ($params as $k=>$v){
            if(in_array($k,$field)){
                $data[$k]=$v;
            }
        }
        if(empty($params['id'])){
            # 添加判断手机号不能重复
            $is_repeat = Db::table("property_neighbourhood")->field("id")->where(array("telphone"=>$params['telphone'],"status"=>1,"del_status"=>2))->find();
            if(!empty($is_repeat)){
                $_ret = "联系人手机号不能重复!,12001";
                $error_arr=explode(',',$_ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
            }
            $data['create_time'] = date("Y-m-d H:i:s");
            # 添加小区
            $id = Db::table("property_neighbourhood")->insertGetId($data);
            # 添加商户
            $merchants_id=Db::table("merchants")->insertGetId(array("contacts"=>$params['username'],"contacts_phone"=>$params['telphone'],
                "is_type"=>3,"nh_id"=>$id,"title"=>$params['nh_title'],"thumb"=>explode("*",$params['nh_detailimg'])[0],"backgroud_color"=>$params['bg_color'],
                "ad_color"=>$params['text_color'],"ad_words"=>$params['massage_text'],"commission"=>$params['draw_prop'],"giving_score"=>$params['give_integral'],
                "address"=>$params['address'],"license_imgurl"=>$params['business_license'],"tel"=>$params['custom_tel']));
            Db::table("property_neighbourhood")->where(array("id"=>$id))->update(array("merchants_id"=>$merchants_id));
        }else{
            # 修改判断手机号不重复
            $is_repeat = Db::table("property_neighbourhood")->field("id")->where(array("telphone"=>$params['telphone'],"status"=>1,"del_status"=>2))
                ->where("id != {$params['id']}")->find();
            if(!empty($is_repeat)){
                $_ret = "联系人手机号不能重复!,12001";
                $error_arr=explode(',',$_ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
            }
            Db::table("property_neighbourhood")->where(array("id"=>$params['id']))->update($data);
            $merchants_id = Db::table("property_neighbourhood")->where(array("id"=>$params['id']))->value("merchants_id");
            $id = $params['id'];
            Db::table("merchants")->where(array("nh_id"=>$id))
                ->update(array("contacts"=>$params['username'],"contacts_phone"=>$params['telphone'],"title"=>$params['nh_title'],
                    "thumb"=>explode("*",$params['nh_detailimg'])[0],"backgroud_color"=>$params['bg_color'],
                    "ad_color"=>$params['text_color'],"ad_words"=>$params['massage_text'],"commission"=>$params['draw_prop'],
                    "giving_score"=>$params['give_integral'],"address"=>$params['address'],"license_imgurl"=>$params['business_license'],"tel"=>$params['custom_tel']));
            if($merchants_id>0){
                $_redis = new Redis();
                $merchantsInfo =$_redis->hDel("merchants",'merchantsInfo'.$merchants_id);
            }


        }
        # 添加关联员工
        if(!empty($params['assign_employee'])){
            $assign = DB::table("assign_staff")->where(array("biz_id"=>$id,"type"=>3))->find();
            if(empty($assign)){
                # 添加关联员工
                Db::table('assign_staff')->insertGetId(array(
                    'biz_id'=>$id,
                    'employee_id'=>$params['assign_employee'],
                    'type'=>3,
                ));
            }else{
                Db::table('assign_staff')->where(array("biz_id"=>$id,"type"=>3))->update(array('employee_id'=>$params['assign_employee']));
            }
        }else{
            # 删除联系人
            Db::table("assign_staff")->where(array('biz_id'=>$id,"type"=>3))->delete();
        }
        # 添加小区详情
        self::saveHouse($id);
        #删除图片
        if(isset($params['business_license'])){
            ResourceService::delCacheItem($params['business_license']);
        }
        if(isset($params['nh_detailimgs'])){
            $params['nh_detailimgs']=BaseService::HandleImg($params['nh_detailimgs']);
            ResourceService::delCacheItem($params['nh_detailimgs']);
        }
        return DataReturn('保存成功', 0);
    }

    /**
     * 缓存资源处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [string]          $name [资源名称]
     * @param    [array]          $params [输入参数]
     */
    public static function HandleCache($type,$params){
        if (!empty($params[$type]))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'property'.$params['id'];
            ResourceService::delCacheItem($params[$type]);
        }
    }
    static function neighbourhoodField(){
        return [
            "nh_title","nb_detail","province","city","district","address","username","telphone","nh_lon","nh_lat","property_title","assign_employee","nh_detailimg",
            "business_license","bg_color","text_color","massage_text","give_integral","status","draw_prop","charge_cycle","property_balance","custom_tel"
            ];
    }

    /**
     * @param $id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 保存房屋
     */
    static function saveHouse($id)
    {
        $info = Session("building");
        if(!empty($info)){
            foreach($info as $k=>$v){
                # 查询楼号是否有
                $build_repeat = Db::table("property_building")->field("id")->where(array("neighbourhood_id"=>$id,"build_title"=>$v['build_title']))->find();
                if(!empty($build_repeat)){
                   Db::table("property_building")->where(array("id"=>$build_repeat['id']))->update(array("build_cate"=>$v['build_cate'],"property_price"=>$v['property_price']));
                    $buildId = $build_repeat['id'];
                }else{
                    $buildId = Db::table("property_building")->insertGetId(array("neighbourhood_id"=>$id,"build_title"=>$v['build_title'],"build_cate"=>$v['build_cate'],"property_price"=>$v['property_price']));
                }
                # 遍历单元
                if(!empty($v['unity_array'])){
                    foreach($v['unity_array'] as $uk=>$uv){
                        # 查询单元是否重复
                        $unity_repeat = Db::table("property_unit")->field("id")->where(array("neighbourhood_id"=>$id,"building_id"=>$buildId,"title"=>$uv['title']))->find();
                        if(empty($unity_repeat)){
                           $unit_id =  Db::table("property_unit")->insertGetId(array("neighbourhood_id"=>$id,"building_id"=>$buildId,"title"=>$uv['title'],"create_time"=>date("Y-m-d H:i:s")));
                        }else{
                            $unit_id = $unity_repeat['id'];
                        }
                        # 遍历房屋
                        if(!empty($uv['house_array'])){
                            $house_array =array();
                            foreach($uv['house_array'] as $hk=>$hv){
                                # 查询房间是否重复
                                $house_repeat = Db::table("property_house")->field("id")->where(array("neighbourhood_id"=>$id,"building_id"=>$buildId,"unit_id"=>$unit_id,"level"=>$hv['level'],
                                    "house_code"=>$hv["house_code"]))->find();
                                if(empty($house_repeat)){
                                   array_push($house_array,array("neighbourhood_id"=>$id,"building_id"=>$buildId,"unit_id"=>$unit_id,"level"=>$hv['level'],
                                       "house_code"=>$hv["house_code"],"area"=>$hv['area'],"price_endtime"=>empty($hv['price_endtime']) ? NULL : $hv['price_endtime'],"member_title"=>$hv['member_title']));
                                }
                            }
                            if(!empty($house_array)){
                                Db::table("property_house")->insertAll($house_array);
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    function dataList($params)
    {
        $where=self::DataListWhere($params);
        if(!empty($params['param']['keywords'])){
            $wheres="(nh_title like '%".$params['param']['keywords']."%' or telphone like '%".$params['param']['keywords']."%')";
        }
        $data_params = array(
            'page'         => true,
            'number'         => 10,
            'table'     =>'property_neighbourhood',
            'where'     =>$where,
            'wheres'    =>$wheres,
            'order'     =>'id desc',
            'field'     =>"*,(select count(id) from property_house where neighbourhood_id = property_neighbourhood.id and del_status = 2) house_num,
                             (select count(id) from property_house where neighbourhood_id = property_neighbourhood.id and del_status = 2 and member_id>0) recommend_num,
                           (select  coalesce(count(id),0) from channel where channel=8 and biz_id  = property_neighbourhood.id and action_type = 2) handle_member"
        );
        $data = BaseService::DataList($data_params);
        $data=self::DataDealWith($data);
        #查询下 商户所关联的分
        $total = BaseService::DataTotal('property_neighbourhood',$where);
        return ['code' => 0, 'msg' => '', 'count' => $total, 'data' => $data];
    }

    static function DataListWhere($params)
    {
        $where = [["id",">",0],["del_status","=",2]];
        if(!empty($params['param']['province'])){
            $where[]=["province",'=',$params['param']['province']];
        }
        if(!empty($params['param']['city'])){
            $where[]=["city",'=',$params['param']['city']];
        }
        if(!empty($params['param']['area'])){
            $where[]=["district",'=',$params['param']['area']];
        }
        if(!empty($params['param']['employee_sal'])){
            $where[]=["assign_employee",'=',$params['param']['employee_sal']];
        }
        return $where;
    }

    static function DataDealWith($data)
    {
        if(!empty($data)){
            foreach($data as $k=>$v){
                 $data[$k]['area_detail'] = $v['provice'].$v['city'].$v['district'];
                 # 查询关联员工
                 $data[$k]['relate_employee_title']='未关联员工';
                if(isset($v['assign_employee'])&&$v['assign_employee']>0)
                {
                    $data[$k]['relate_employee_title']=Db::name('employee_sal')->where('id='.$v['assign_employee'])->value('employee_name');
                }
                $data[$k]['property_balance'] = Db::table("merchants")->field("balance")->where(array("id"=>$v['merchants_id']))->find()['balance'];
            }
        }
        return $data;
    }

    static function ChangePropertyNum($data)
    {
        Db::table("property_neighbourhood")->where(array("id"=>$data['id']))->update(array($data['mark']=>$data['val']));
        return array("status"=>true);
    }

    /**
     * 商户收款记录
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PropertyCollectionsList($params){
        $data=BaseService::DataList($params);
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                #根据订单号查询 订单所有信息  类型 8 9  则自定义
                if($v['pay_type'] >8){
                    $v['member_name'] = "系统奖励";
                    $v['member_phone'] = "--";
                    $v['total_price'] = priceFormat($v['price']);
                    $v['actual_price'] = priceFormat($v['price']);
                    $v['voucher_price'] = priceFormat(0);
                    $v['commission_price'] = priceFormat(0);
                    $v['checkout_price'] = priceFormat($v['price']);
                    $v['cash_type_title'] = "平台奖励";
                    if($v['pay_type'] >9){
                        $v['cash_type_title'] = "推荐会员";

                    }
                    if($v['pay_type'] == 8){
                        $v['pay_type_title'] = "平台奖励";
                    }else if($v['pay_type'] == 9){
                        $v['pay_type_title'] = "任务获得";
                    }else if($v['pay_type'] == 10){
                        $v['pay_type_title'] = "会员推荐收入";
                    }else if($v['pay_type'] == 11){
                        $v['pay_type_title'] = "会员充值提成";
                    }else{
                        $v['pay_type_title'] = "会员升级提成";
                    }
                }else{
                    if(!empty($v['order_number'])){
                        $order = Db::table('merchants_order')->where(array('order_number'=>$v['order_number']))->find();
                        // ，付款人名称，手机号，
                        $v['member_name']=Db::name('member')->where('id='.$v['member_id'])->value('member_name');
                        if(empty($v['member_name'])){
                            $v['member_name']=Db::name('member')->where('id='.$v['member_id'])->value('nickname');
                            if(empty($v['member_name'])){
                                $v['member_name'] = '--';
                            }
                        }
                        //手机号
                        $v['member_phone'] = $order['member_phone'];
                        if (empty($order['member_phone']))
                        {
                            $v['member_phone']=Db::name('member')->where('id='.$v['member_id'])->value('member_phone');
                            if(empty($v['member_phone'])){
                                $v['member_phone'] = '--';
                            }
                        }

                        //平台抽成 结算金额
                        if (isset($order['commission_price']))
                        {
                            $v['commission_price']=priceFormat($order['commission_price']);
                            $v['checkout_price']=priceFormat($order['actual_price']-$order['commission_price']);
                        }
                        //需支付
                        if (isset($order['total_price']))
                        {
                            $v['total_price']=priceFormat($order['total_price']);
                        }
                        //实际支付
                        if (isset($order['actual_price']))
                        {
                            $v['actual_price']=priceFormat($order['actual_price']);
                        }
                        //卡卷低值
                        $v['voucher_price']=priceFormat(0);
                        if ($order['actual_price']!=$order['total_price'])
                        {

                            $v['voucher_price'] =priceFormat(Db::name('merchants_voucher_order')->where([['order_id','=',$order['id']]])->sum('price')) ;
                        }

                        //付款方式
                        if(isset($order['cash_type']))
                        {
                            $v['cash_type_title']=BaseService::StatusHtml($order['cash_type'],lang('cash_type'),false);
                        }
                        //支付类型
                        if(isset($order['pay_type']))
                        {

                            $v['pay_type_title']=BaseService::StatusHtml($order['pay_type'],lang('merchants_pay_type'),false);
                        }
                    }


                }

            }
        }
        return $data;
    }

    static function PropertyCardlogList($params)
    {
        $data=BaseService::DataList($params);
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                $where = "account_status = 1 and id = '".$v['member_id']."'";
                #查询会员信息
                $data_params = array(
                    'page'         => true,
                    'number'         => 10,
                    'table'     =>'member',
                    'where'     => $where,
                    'field'     =>'*'
                );
                $_res= BaseService::DataList($data_params);
                $v['member_name'] = $_res[0]['member_name'];
                if(empty($v['member_name'])){
                    $v['member_name'] = $_res[0]['nickname'];

                }
                $v['member_phone'] = $_res[0]['member_phone'];
                $v['member_id'] = $_res[0]['id'];
                $v['card_level'] = Db::table('member_level')->where(array('id'=>$v['level_id']))->value('level_title');
                $v['member_balance'] = Db::table("member_mapping")->where(array("member_id"=>$v['member_id']))->value("convert(member_balance,decimal(10,2))");

            }
        }
        return $data;
    }

    public static function MerchantsStaffList($params){
        $data=BaseService::DataList($params);
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                // 员工权限
                $v['staff_power_title']='无权限';
                if (!empty($v['staff_power']))
                {
                    #拆分成数组
                    $staff_power = explode(',',$v['staff_power']);
                    if(!empty($staff_power)){
                        foreach ($staff_power as $sk=>$sv){
                            $staff_power[$sk] = BaseService::StatusHtml($sv,lang('merchants_staff_power'),false);
                        }

                    }
                }
                $v['staff_power_title'] = implode(',',$staff_power);
            }
        }
        return $data;
    }

    /**
     * 商户评论列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function PropertyCommentListWhere($params = [])
    {
        $where = [];

        //时间段
        if(!empty($params['param']['create_time']))
        {
            $time_start=date('Y-m-d 00:00:00',strtotime($params["param"]['create_time']));
            $time_end=date('Y-m-d 23:59:59',strtotime($params["param"]['create_time']));
            $where[] =['me.create_time', '>',  $time_start ];
            $where[] =['me.create_time', '<',  $time_end];
        }
        //dump($params);exit;
        //merchants_id
        if(!empty($params['merchants_id']))
        {
            $where[] =['mo.merchants_id', '=',  $params['merchants_id'] ];
        }
        //付款方式
        if(!empty($params['param']['cash_type']))
        {
            $where[] =['mo.cash_type', '=',  $params['param']['cash_type'] ];
        }

        //评论等级
        if(!empty($params['param']['evalua_level']))
        {
            //好评
            if ($params['param']['evalua_level']==1)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '>',  3 ];
            }
            //中评
            if ($params['param']['evalua_level']==2)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '=',  3 ];
            }
            //查评
            if ($params['param']['evalua_level']==3)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '<',  3 ];
            }
            //未评论
            if ($params['param']['evalua_level']==4)
            {
                $where[] =['me.default_mark', '=', 1 ];
                $where[] =['me.evalua_level', '=',  $params['param']['income_type'] ];
            }
        }

        return $where;
    }
    /**
     * 商户评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function PropertyCommentDataList($params = [])
    {
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'me.create_time desc';
        $field = empty($params['field']) ? 'me.*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;
        $data = Db::name('merchants_evaluation')->alias('me')->leftJoin(['merchants_order'=>'mo'],'me.merchants_order_id=mo.id')->where($where)->where(array('me.default_mark'=>2))->field($field)->order($order);
        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }
        return self::PropertyCommentDealWith($data);
    }
    /**
     * 商户数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     */
    public static function  PropertyCommentDealWith($data){
        if(!empty($data))
        {

            foreach($data as &$v)
            {

                //付款方式
                if(isset($v['cash_type']))
                {

                    $v['cash_type_title']=BaseService::StatusHtml($v['cash_type'],lang('cash_type'),false);
                }
                //evalua_level_title 评论状态
                if(isset($v['default_mark']))
                {
                    if ($v['default_mark']==1) {
                        $v['evalua_level_title']='用户未评论';
                    }
                }

                //evalua_level_title 评论状态
                if(isset($v['evalua_level']))
                {
                    if ($v['evalua_level']==3)
                    {
                        $v['evalua_level_title']='中评';
                    }
                    if ($v['evalua_level']>3)
                    {
                        $v['evalua_level_title']='好评';
                    }

                    if ($v['evalua_level']<3)
                    {
                        $v['evalua_level_title']='差评';
                    }

                }


            }


        }
        return $data;
    }

    /**
     * 商户评论总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function PropertyCommentDataTotal($where){
        return (int)  Db::name('merchants_evaluation')->alias('me')
            ->leftJoin(['merchants_order'=>'mo'],'me.merchants_order_id=mo.id')
            ->where($where)->count();
    }

    static function FinancialChartData()
    {
        # 业主数据统计
        # 已关联房产业主数量
        $already_relation = Db::table("property_memberhouse")->distinct(true)->where("del_status=2")->count("member_id");
        # 今日增加数量
        $today_time = date("Y-m-d");
        $t_bind = Db::table("property_memberhouse")->where("del_status=2 and date(create_time) = '".$today_time."'")->count();
        # 昨日增加数量
        $yes_time = date("Y-m-d",strtotime("-1 day"));
        $y_bind = Db::table("property_memberhouse")->where("del_status=2 and date(create_time) = '".$yes_time."'")->count();
        # 本月增加数量
        $m_time = date("Y-m");
        $m_bind = Db::table("property_memberhouse")->where(" del_status=2 and date_format(create_time,'%Y-%m') = '".$m_time."'")->count();
        # 上月增加数量
        $l_time = date("Y-m",strtotime("-1 month"));
        $l_bind = Db::table("property_memberhouse")->where("member_id>0 and del_status=2 and date_format(create_time,'%Y-%m') = '".$l_time."'")->count();
        # 累计缴费金额
        $accumulated_pay = Db::table("property_payfee")->where(" id > 0")->sum("total_price");
        # 今日缴费
        $t_pay = Db::table("property_payfee")->where(" id > 0 and date(create_time) = '".$today_time."'")->sum("total_price");
        # 昨日缴费
        $y_pay = Db::table("property_payfee")->where(" id > 0 and date(create_time) = '".$yes_time."'")->sum("total_price");
        # 本月缴费
        $m_pay = Db::table("property_payfee")->where(" id > 0 and date_format(create_time,'%Y-%m') = '".$m_time."'")->sum("total_price");
        # 上月缴费
        $l_pay = Db::table("property_payfee")->where(" id > 0 and date_format(create_time,'%Y-%m') = '".$l_time."'")->sum("total_price");
        return array("already_relation"=>$already_relation,"t_bind"=>$t_bind,"y_bind"=>$y_bind,"m_bind"=>$m_bind,"l_bind"=>$l_bind,
                     "accumulated_pay"=>getsPriceFormat($accumulated_pay),"t_pay"=>getsPriceFormat($t_pay),"y_pay"=>getsPriceFormat($y_pay),
                        "m_pay"=>getsPriceFormat($m_pay),"l_pay"=>getsPriceFormat($l_pay));
    }

    static function BindChartData($params)
    {
        $where = "ph.id > 0";
        if(!empty($params['province'])){
            $where .= " and pn.province  = '".$params['province']."'";
        }
        if(!empty($params['city'])){
            $where .= "and pn.city  = '".$params['city']."'";
        }
        if(!empty($params['area'])){
            $where .= "and pn.district  = '".$params['area']."'";
        }

        $list = Db::table("property_memberhouse ph")
            ->field("pn.nh_title title,count(ph.id) value")
            ->leftJoin("property_neighbourhood pn","pn.id = ph.nh_id")
            ->where("ph.member_id > 0 and ph.del_status=2")->where($where)->group("ph.nh_id")->order("value desc")->select();
        $title_array=array();
        if(!empty($list)){
            $title_array = array_column($list,'title');
        }
        return array("data"=>array_values($list),"title"=>array_values($title_array));
    }

    static function PayFeeChartData($params)
    {
        $where = "pp.id > 0 ";
        if(!empty($params['province'])){
            $where .= " and pn.province  = '".$params['province']."'";
        }
        if(!empty($params['city'])){
            $where .= "and pn.city  = '".$params['city']."'";
        }
        if(!empty($params['area'])){
            $where .= "and pn.district  = '".$params['area']."'";
        }
        $list = Db::table("property_payfee pp")
            ->field("pn.nh_title title,convert(sum(pp.total_price),decimal(10,2)) value")
            ->leftJoin("property_neighbourhood pn","pn.id = pp.nh_id")
            ->where($where)
            ->group("pp.nh_id")
            ->order("value desc")
            ->select();
        $title_array=array();
        if(!empty($list)){
            $title_array = array_column($list,'title');
        }
        return array("data"=>array_values($list),"title"=>array_values($title_array));
    }

    static function CommissionChartData($params)
    {
        $where = " bi.id > 0";
        if(!empty($params['province'])){
            $where .= " and pn.province  = '".$params['province']."'";
        }
        if(!empty($params['city'])){
            $where .= "and pn.city  = '".$params['city']."'";
        }
        if(!empty($params['area'])){
            $where .= "and pn.district  = '".$params['area']."'";
        }
        $date = date("Y-m-d",strtotime("-30 days"));
        $date_where = " and date(bi.pay_create)>='".$date."'";
        if(!empty($params['start_time'])){
            $date_where = " and date(bi.pay_create)>='".$params['start_time']."'";
        }
        if(!empty($params['end_time'])){
            $date_where = " and date(bi.pay_create)<='".$params['end_time']."'";
        }

        $list = Db::table("biz_income bi")
            ->field("date(bi.pay_create) title,convert(sum(bi.biz_income),decimal(10,2)) value")
            ->Join("merchants m","m.id = bi.biz_id")
            ->Join("property_neighbourhood pn","pn.id = m.nh_id")
            ->where(" bi.id > 0 and bi.income_type = 18 {$date_where}")
            ->where($where)
            ->group("date(bi.pay_create)")
            ->order("title asc")
            ->select();
        $title_array=array();
        if(!empty($list)){
            $title_array = array_column($list,'title');
        }
        return array("data"=>array_values($list),"title"=>array_values($title_array));
    }

    static function SubsidyChartData($params)
    {
        $where = " s.id > 0 ";
        if(!empty($params['province'])){
            $where .= " and pn.province  = '".$params['province']."'";
        }
        if(!empty($params['city'])){
            $where .= "and pn.city  = '".$params['city']."'";
        }
        if(!empty($params['area'])){
            $where .= "and pn.district  = '".$params['area']."'";
        }
        $date = date("Y-m-d",strtotime("-30 days"));
        $date_where = " and date(s.subsidy_create)>='".$date."'";
        if(!empty($params['start_time'])){
            $date_where = " and date(s.subsidy_create)>='".$params['start_time']."'";
        }
        if(!empty($params['end_time'])){
            $date_where = " and date(s.subsidy_create)<='".$params['end_time']."'";
        }

        $list = Db::table("subsidy s")
            ->field("date(s.subsidy_create) title,convert(sum(s.subsidy_number),decimal(10,2)) value")
            ->leftJoin("property_neighbourhood pn","pn.id = s.biz_id")
            ->where(" s.id > 0 and s.subsidy_type = 16 {$date_where}")
            ->where($where)
            ->group("date(s.subsidy_create)")
            ->order("title asc")
            ->select();
        $title_array=array();
        if(!empty($list)){
            $title_array = array_column($list,'title');
        }
        return array("data"=>array_values($list),"title"=>array_values($title_array));
    }

    static function RecommendChartChartData()
    {
        $where = "ch.id > 0 ";
        if(!empty($params['province'])){
            $where .= " and pn.province  = '".$params['province']."'";
        }
        if(!empty($params['city'])){
            $where .= "and pn.city  = '".$params['city']."'";
        }
        if(!empty($params['area'])){
            $where .= "and pn.district  = '".$params['area']."'";
        }
       $list = Db::table("channel ch")
           ->field("pn.nh_title title,count(ch.id) value")
           ->leftJoin("property_neighbourhood pn","pn.id = ch.biz_id")
           ->where("ch.channel=8 and action_type=2")->where($where)->group("ch.biz_id")->order("value desc")->select();
        $title_array=array();
        if(!empty($list)){
            $title_array = array_column($list,'title');
        }
        return array("data"=>array_values($list),"title"=>array_values($title_array));

    }
}