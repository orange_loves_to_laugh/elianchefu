<?php


namespace app\service;

use think\Db;

/**
 * 服务分类服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月9日10:01:25
 */
class ServiceCategory
{
    /**
     * 获取分类
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function ServiceCategory($params = [])
    {
        // 获取分类
        /*
        $where = empty($params['where']) ? [['is_del','=',2]] : $params['where'];

        $page = $params['page'] ? true : false;

        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 10;

        $number=isset($params['number']) ? intval($params['number']) : 10;

        // 获取列表
        $data = Db::name('service_class')->where($where)->field('*')->order('id desc');
        //分页
        $page_html=null;
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);

            $page_html=$data->render();

            $data=$data->toArray()['data'];

        }else{
            $data=$data->limit($m, $n)->select();
        }
        */
        $params['table']='service_class';
        $params['order']='id desc';
        $data=BaseService::DataList($params);
        return self::ServiceCategoryDataDealWith($data);




    }

    /**
     * 根据pid获取分类列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $where [条件]
     */
    public static function ServiceCategoryList($where = [])
    {

        $data = Db::name('service_class')->field('*')->where($where)->order('id asc')->find();
        return $data;
        //return self::GoodsCategoryDataDealWith($data);
    }
    /**
     * 分类数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param   [array]          $data [分类数据]
     */
    public static function ServiceCategoryDataDealWith($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                if($v['service_class_pid'])
                {
                    $where_next['id'] = $v['service_class_pid'];
                    $v['service_class_level'] = self::ServiceCategoryList($where_next);
                    if(!empty($v['service_class_level']))
                    {
                        $v['service_class_level']=$v['service_class_level']['service_class_title'];
                    }
                }else{

                    $v['service_class_level']='最高级';
                }
            }
        }
//        if($params['page_html']){
//            $data['page_html']=$params['page_html'];
//        }


        return DataReturn('处理成功', 0, $data);
    }

    /**
     * 分类保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月23日09:27:10
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ServiceCategorySave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'service_class_title',
                'error_msg'         => '分类名称不能为空',
                'error_code'         => 20002,
            ],

        ];
        # 添加是判断名称是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'service_class',
                'key_name'          => 'service_class_title',
                'error_msg'         => '分类名称不能重复',
                'error_code'         => 0001,
            ];
        }

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);

            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        $data=$params;
        $data['service_outtime'] = sprintf("%.2f",$params['service_outtime']);
        // 添加/编辑
        if(empty($params['id']))
        {

            $data['service_class_create'] = TIMESTAMP;
            $class_id = Db::name('service_class')->insertGetId($data);
        } else {

            $data['service_class_update'] = TIMESTAMP;

            if(Db::name('service_class')->where(['id'=>intval($params['id'])])->update($data))
            {
                $class_id = $params['id'];
            }
        }


        return json(DataReturn('保存成功', 0));
    }
}