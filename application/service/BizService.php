<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/22
 * Time: 14:32
 */

namespace app\service;

use Overtrue\Socialite\Providers\Base;
use think\Db;
use think\facade\Session;

/**
 * 门店服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
class BizService
{
    /**
     * [BizIndex 获取门店列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function BizIndex($params)
    {
        #where条件  写成活的
//        $where = empty($params['where']['biz_type']) ? array('biz_type'=>1) : $params['where'];
        $where = $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
        #不用tp 5 的 分页时 用
        /*        $m = isset($params['m']) ? intval($params['m']) : 0;
                $n = isset($params['n']) ? intval($params['n']) : 10;*/
        #获取门店列表信息
        $data = Db::table('biz')->field("*")->where($where)->order('biz_status desc,biz_create desc');
        //分页
        $page_html = null;
        $data = $data->paginate($number, false, ['query' => request()->param()]);

        $page_html = $data->render();
        $data = $data->toArray()['data'];
        $params['page_html'] = $page_html;
        #处理方法
        return self::InforDataDealWith($data, $params);

    }

    #处理方法
    function InforDataDealWith($data, $params)
    {
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                //带结算金额  'desposit_status'=>1,
                $data[$k]['settlement_price'] = priceFormat(Db::name('biz_settlement')->where(['biz_id' => $v['id'], 'settlement_status' => 1])->sum('price') +
                    Db::table('biz_settlement')
                        ->where(array('biz_id' => $v['id'], 'settlement_status' => 1, 'desposit_status' => 1, 'mark' => 1))->sum('commission_price'));
//                if($data[$k]['settlement_price']>$v['security_price'] and $v['security_price'] != 0){
//                    $data[$k]['settlement_price']=$v['security_price'];
//                }

                //门店地址
                $data[$k]['biz_address'] = $v['province'] . $v['city'] . $v['area'];
                //$v['settlement_price']='0.00';
                // dump($v);exit;
                //门店状态
                if (isset($v['biz_status'])) {
                    switch ($v['biz_status']) {
                        case 0:
                            $data[$k]['biz_status'] = '停用';
                            break;
                        case 1:
                            $data[$k]['biz_status'] = '启用';
                            break;
                        case 2:
                            $data[$k]['biz_status'] = '待审核';
                            break;

                    }
                }
                //商户类型
                if (isset($v['biz_type'])) {
                    switch ($v['biz_type']) {
                        case 1:
                            $data[$k]['biz_typestore'] = '自营店';
                            break;
                        case 2:
                            $data[$k]['biz_typestore'] = '加盟店';
                            break;
                        case 3:
                            $data[$k]['biz_typestore'] = '合作店';
                            break;

                    }
                }
                #把营业时间放到一起
                if (isset($v['biz_opening'])) {
                    $data[$k]['biz_opencreate'] = $v['biz_opening'] . '-' . $v['biz_closing'];
                }
                #工位数量   #门店余额  未结算的

                if (isset($v['id'])) {
                    $data[$k]['station_num'] = Db::table("station")->where(array('biz_id' => $v['id']))->count();
                    $data[$k]['pricecount'] = getsPriceFormat(Db::table("settlement")->where(array('biz_id' => $v['id'], 'status' => 1))->sum('pricecount'));
                }
                #比列变成百分数
                if (isset($v['proportion'])) {
                    $data[$k]['proportion'] = ($v['proportion'] * 100) . "%";

                }

                //关联员工
                $em = Db::name('assign_staff')->alias('a')
                    ->leftJoin(['employee_sal' => 'e'], 'a.employee_id=e.id')
                    ->where('a.type=1 and a.biz_id=' . $v['id'])
                    ->field('e.id,employee_name')
                    ->find();
                $data[$k]['employee_id'] = empty($em) ? '' : $em['id'];
                $data[$k]['employee_name'] = empty($em) ? '' : $em['employee_name'];


            }

        }
        return $data;

    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    public static function BizIndexCount($_where)
    {
        return (int)Db::name('biz')->where($_where)->count();
    }

    /**
     * 总数
     * @param    [array]          $where [条件]
     * @author  门店添加 修改 处理方法
     * @date    2020年7月21日13:29:33
     * @desc    description
     */
    static function BizInsertWrite($params)
    {


        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'biz_title',
                'error_msg' => '门店名称不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'biz_opening',
                'error_msg' => '营业开始时间不能为空',
                'error_code' => 30003,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'biz_closing',
                'error_msg' => '营业结束时间不能为空',
                'error_code' => 30004,
            ],

            [
                'checked_type' => 'empty',
                'key_name' => 'biz_phone',
                'error_msg' => '联系方式不能为空',
                'error_code' => 30005,
            ],
           /* [
                'checked_type' => 'empty',
                'key_name' => 'building_area',
                'error_msg' => '门店面积不能为空',
                'error_code' => 30005,
            ],*/
            [
                'checked_type' => 'empty',
                'key_name' => 'biz_leader',
                'error_msg' => '负责人不能为空',
                'error_code' => 30006,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'recommend_status',
                'error_msg' => '推荐状态不能为空',
                'error_code' => 30006,
            ],


        ];
        if ($params['biz_type'] == 2 or $params['biz_type'] == 3) {

            /*   $p[]=   [
                   'checked_type'      => 'empty',
                   'key_name'          => 'proportion',
                   'error_msg'         => '抽成比列为空',
                   'error_code'         => 30001,
               ];*/
            if ($params['biz_type'] == 2) {
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'franchise_costs',
                    'error_msg' => '加盟费用为空',
                    'error_code' => 30001,
                ];
            }
            if ($params['biz_type'] == 3) {
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'settlement_method',
                    'error_msg' => '请选择门店结算方式',
                    'error_code' => 30001,
                ];
            }

            /* $p[]=   [
                 'checked_type'      => 'empty',
                 'key_name'          => 'security_price',
                 'error_msg'         => '门店抵押金',
                 'error_code'         => 30001,
             ];*/


        }
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'biz_img',
                'error_msg' => '请上传门头照片',
                'error_code' => 30001,
            ];
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'biz_permit',
                'error_msg' => '请上传营业执照',
                'error_code' => 30001,
            ];
           /* $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'biz_head',
                'error_msg' => '请上传门店照片',
                'error_code' => 30001,
            ];*/
            if ($params['biz_type'] == 2) {
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'super_pwd',
                    'error_msg' => '请输入门店管理密码',
                    'error_code' => 30005,
                ];
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'datacenter_pwd',
                    'error_msg' => '请输入数据中心登录密码',
                    'error_code' => 30005,
                ];
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'pwd',
                    'error_msg' => '请输入门店操作密码',
                    'error_code' => 30005,
                ];

                $p[] = [
                    'checked_type' => 'pwdlength',
                    'checked_data' => '6',
                    'key_name' => 'datacenter_pwd',
                    'error_msg' => '数据中心登录密码最少6位',
                    'error_code' => 30005,
                ];
                $p[] = [
                    'checked_type' => 'pwdlength',
                    'checked_data' => '6',
                    'key_name' => 'pwd',
                    'error_msg' => '门店操作密码最少6位',
                    'error_code' => 30005,
                ];
                $p[] = [
                    'checked_type' => 'pwdlength',
                    'checked_data' => '6',
                    'key_name' => 'super_pwd',
                    'error_msg' => '门店管理密码最少6位',
                    'error_code' => 30005,
                ];
                $p[] = [
                    'checked_type' => 'empty',
                    'key_name' => 'biz_address',
                    'error_msg' => '门店详细地址不能为空',
                    'error_code' => 30005,
                ];
            }


        } else {

            #加盟门店时候有
            if ($params['biz_type'] == 2) {
                if (isset($data['datacenter_pwd'])) {
                    $p[] = [
                        'checked_type' => 'pwdlength',
                        'checked_data' => '6',
                        'key_name' => 'datacenter_pwd',
                        'error_msg' => '数据中心登录密码最少6位',
                        'error_code' => 30005,
                    ];

                }
                if (isset($data['super_pwd'])) {
                    $p[] = [
                        'checked_type' => 'pwdlength',
                        'checked_data' => '6',
                        'key_name' => 'super_pwd',
                        'error_msg' => '门店管理密码最少6位',
                        'error_code' => 30005,
                    ];


                }
                if (isset($data['pwd'])) {

                    $p[] = [
                        'checked_type' => 'pwdlength',
                        'checked_data' => '6',
                        'key_name' => 'pwd',
                        'error_msg' => '门店操作密码最少6位',
                        'error_code' => 30005,
                    ];
                }
            }

        }
        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);

            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }

        // 添加/编辑
        if (empty($params['id'])) {

            $data['biz_create'] = TIMESTAMP;
            #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
            $params['id'] = empty($params['id']) ? '' : $params['id'];
            ResourceService::$session_suffix = 'source_upload' . 'biz' . $params['id'];
            #删除单图
            if (isset($params['biz_permit'])) {
                ResourceService::delCacheItem($params['biz_permit']);

            }
            if (isset($params['biz_head'])) {
                $params['biz_head'] = BaseService::HandleImg($params['biz_head']);
                ResourceService::delCacheItem($params['biz_head']);


            }
            if (isset($params['biz_img'])) {
                ResourceService::delCacheItem($params['biz_img']);
            }
            //删除多图

            #添加处理

            $re = Db::name('biz')
                ->where('biz_type=' . $params['biz_type'] . ' and biz_title="' . $params['biz_title'] . '"')->value('id');
            //dump($params);exit;
            if ($re > 0) {
                return json(DataReturn('门店名称已经存在', -1));
            }
            return self::BizDealInfo($params, $data);

        } else {
            #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
            $params['id'] = empty($params['id']) ? '' : $params['id'];
            ResourceService::$session_suffix = 'source_upload' . 'biz' . $params['id'];
            #删除单图
            if (isset($params['biz_permit'])) {
                ResourceService::delCacheItem($params['biz_permit']);

            }
            if (isset($params['biz_head'])) {
                $params['biz_head'] = BaseService::HandleImg($params['biz_head']);
                ResourceService::delCacheItem($params['biz_head']);
            }
            if (isset($params['biz_img'])) {
                ResourceService::delCacheItem($params['biz_img']);


            }

            return self::BizWriteInfo($params, $data);


        }


    }

    /**
     * 添加处理
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function BizDealInfo($params = [], $data)
    {
        if (!empty($data)) {

            #处理密码  md5  加密
            $data['datacenter_pwd'] = md5($data['datacenter_pwd']);//数据中心登录密码
            $data['super_pwd'] = md5($data['super_pwd']);//门店管理密码
            $data['pwd'] = md5($data['pwd']);//门店操作密码
            /*    $stationInfoSession = Session('stationInfoSession');*/


            $relate_employee = $data['relate_employee'];
            unset($data['relate_employee']);
            //dump($data);die;
            $data['extract_selfservice'] = 0.1;
            $data['extract_selfpro'] = 0.1;
            Db::startTrans();
            $bizId = Db::table("biz")->insertGetId($data);


            if ($bizId) {
                #添加关联员工

                if (isset($relate_employee) && $relate_employee > 0) {
                    $re = Db::table('assign_staff')->insert(['biz_id' => $bizId, 'employee_id' => $relate_employee]);
                    if (!$re) {
                        // Db::rollback();
                    }
                }
                #添加工位
                /* */

                $stationInfoSession = Session('stationInfoSession');

                if (!empty($stationInfoSession)) {
                    foreach ($stationInfoSession as $k => $v) {
                        $stationInfoSession[$k]['biz_id'] = $bizId;
                        if (!isset($v['timer'])) {
                            $stationInfoSession[$k]['timer'] = 0;
                        }

                    }

                    Db::table('station')->insertAll($stationInfoSession);
                    #删除工位缓存
                    Session::delete('stationInfoSession');

                }


                #添加商品权限
                $bizproInfoSession = Session('bizproInfoSession');
                if (!empty($bizproInfoSession)) {
                    foreach ($bizproInfoSession as $k => $v) {
                        $bizproInfoSession[$k]['biz_id'] = $bizId;
                        $bizproInfoSession[$k]['goods_price'] = $v['biz_pro_price'];
                        $bizproInfoSession[$k]['goods_online'] = $v['biz_pro_online'];
                        $bizproInfoSession[$k]['goods_purch'] = $v['biz_pro_purch'];
                        $bizproInfoSession[$k]['pro_id'] = $v['id'];
                        unset(
                            $bizproInfoSession[$k]['biz_pro_number'],
                            $bizproInfoSession[$k]['biz_pro_title'],
                            $bizproInfoSession[$k]['biz_pro_class_id'],
                            $bizproInfoSession[$k]['class_title'],
                            $bizproInfoSession[$k]['biz_pro_price'],
                            $bizproInfoSession[$k]['biz_pro_online'],
                            $bizproInfoSession[$k]['biz_pro_purch'],
                            $bizproInfoSession[$k]['service_class_id'],
                            $bizproInfoSession[$k]['id']
                        );
                    }
                    Db::table('biz_goods')->insertAll($bizproInfoSession);
                    #删除商品权限缓存
                    Session::delete('bizproInfoSession');

                }
                #添加服务权限
                $serviceInfoSession = Session('serviceInfoSession');
                if (!empty($serviceInfoSession)) {
                    foreach ($serviceInfoSession as $k => $v) {
                        $serviceInfoSession[$k]['biz_id'] = $bizId;
                        $serviceInfoSession[$k]['service_id'] = $v['id'];
                        //把 结算价格  添加到  service_car_mediscount 表中  settlement_amount
                        //先判断 当前的服务id 和  门店id 是否 有和这个结算价格 (大中 小)   有的话  直接 修改 结算 价格  没有   添加  type 0 大 1  中  2 小  mark :online 线上  offline 线下   $v['carprice']:结算价格数组
                        self::getBizServiceprice($bizId, $v['id'], $v['carprice']);
                        unset(
                            $serviceInfoSession[$k]['service_title'],
                            $serviceInfoSession[$k]['service_class_title'],
                            $bizproInfoSession[$k]['service_class_id'],
                            $serviceInfoSession[$k]['id'],
                            $serviceInfoSession[$k]['carprice'],

                            $serviceInfoSession[$k]['service_class_id']
                        );

                    }
                    Db::table('service_biz')->insertAll($serviceInfoSession);
                    #删除服务权限缓存
                    Session::delete('serviceInfoSession');

                }
                #创建 直营门店时  关联所有服务
                if ($data['biz_type'] == 1) {
                    #$bizId 门店id  biz:证明是门店创建时执行的
                    getRelationServiceBiz('biz', $bizId);
                }
                Db::commit();


            } else {
                //Db::rollback();
            }

            ///dump(123);exit;
            // Db::commit();

            return json(DataReturn('保存成功', 0));
        }

    }

    /**
     * 修改处理
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function BizWriteInfo($params = [], $data)
    {

        if (!empty($data)) {
            Db::startTrans();
            #处理图片 在上传的时候传oldSrc  就处理了 无需再次处理

            unset($data['oldbiz_permit'], $data['oldbiz_head'], $data['oldbiz_img']);
            #处理密码  md5  加密
            if (isset($data['datacenter_pwd'])) {
                $data['datacenter_pwd'] = md5($data['datacenter_pwd']);//数据中心登录密码

            }
            if (isset($data['super_pwd'])) {
                $data['super_pwd'] = md5($data['super_pwd']);//门店管理密码

            }
            if (isset($data['pwd'])) {
                $data['pwd'] = md5($data['pwd']);//门店操作密码

            }
            #添加工位
            if ($params['biz_type'] == 3) {
                $stationInfoSession = Session('stationInfoSession');
                if (!empty($stationInfoSession)) {
                    $station_arr = Db::table('station')->where('biz_id=' . $params['id'])->select();
                    if (count($stationInfoSession) > count($station_arr)) {
                        //加工位
                        foreach ($stationInfoSession as $k => $v) {
                            $stationInfoSession[$k]['biz_id'] = $data['id'];
                            foreach ($station_arr as $vv) {
                                if ($vv['station_title'] == $v['station_title']) {
                                    unset($stationInfoSession[$k]);
                                }
                            }

                        }

                        Db::table('station')->insertAll($stationInfoSession);
                    } else {
                        //减工位
                        $del_arr = [];
                        foreach ($stationInfoSession as $k => $v) {
                            $stationInfoSession[$k]['biz_id'] = $data['id'];
                            foreach ($station_arr as $kk => $vv) {
                                if ($vv['station_title'] == $v['station_title']) {
                                    unset($station_arr[$kk]);
                                }
                            }
                        }
                        foreach ($station_arr as $kk => $vv) {
                            $del_arr[$kk] = $vv['id'];
                        }

                        Db::name('station')->where([['id', 'in', $del_arr]])->delete();
                    }
                }
            } else {

                /**/
                $stationInfoSession = Session('stationInfoSession');
                if (!empty($stationInfoSession)) {
                    foreach ($stationInfoSession as $k => $v) {
                        if (!isset($v['timer'])) {
                            $stationInfoSession[$k]['timer'] = 0;
                        }
                        $stationInfoSession[$k]['biz_id'] = $data['id'];


                    }
                    Db::table('station')->insertAll($stationInfoSession);

                    #删除工位缓存
                    Session::delete('stationInfoSession');

                }
            }


            #添加商品权限
            $bizproInfoSession = Session('bizproInfoSession');

            if (!empty($bizproInfoSession)) {
                foreach ($bizproInfoSession as $k => $v) {
                    $bizproInfoSession[$k]['biz_id'] = $data['id'];
                    $bizproInfoSession[$k]['goods_price'] = $v['biz_pro_price'];
                    $bizproInfoSession[$k]['goods_online'] = $v['biz_pro_online'];
                    $bizproInfoSession[$k]['goods_purch'] = $v['biz_pro_purch'];
                    $bizproInfoSession[$k]['pro_id'] = $v['id'];
                    unset(
                        $bizproInfoSession[$k]['biz_pro_number'],
                        $bizproInfoSession[$k]['biz_pro_title'],
                        $bizproInfoSession[$k]['biz_pro_class_id'],
                        $bizproInfoSession[$k]['class_title'],
                        $bizproInfoSession[$k]['biz_pro_price'],
                        $bizproInfoSession[$k]['biz_pro_online'],
                        $bizproInfoSession[$k]['biz_pro_purch'],
                        $bizproInfoSession[$k]['id']
                    );
                }

                Db::table('biz_goods')->insertAll($bizproInfoSession);
                #删除商品权限缓存  门店id
                Session::delete('bizproInfoSession');
                Session::delete('bizId');

            }
            #添加服务权限
            $serviceInfoSession = Session('serviceInfoSession');

            /*    dump($serviceInfoSession);
                die;*/
            if (!empty($serviceInfoSession)) {
                foreach ($serviceInfoSession as $k => $v) {
                    #门点id
                    $serviceInfoSession[$k]['biz_id'] = $data['id'];
                    #服务id
                    $serviceInfoSession[$k]['service_id'] = $v['id'];
                    //把 结算价格  添加到  service_car_mediscount 表中  settlement_amount
                    //先判断 当前的服务id 和  门店id 是否 有和这个结算价格 (大中 小)   有的话  直接 修改 结算 价格  没有   添加  type 0 大 1  中  2 小  mark :online 线上  offline 线下   $v['carprice']:结算价格数组
                    //门店添加  大 中 小 车 价格 查下 这个服务的 大中小  价格 存里
                    self::getBizServiceprice($data['id'], $v['id'], $v['carprice']);

                    unset(
                        $serviceInfoSession[$k]['service_title'],
                        $serviceInfoSession[$k]['service_class_title'],
                        $serviceInfoSession[$k]['id'],
                        $serviceInfoSession[$k]['carprice'],
                        $serviceInfoSession[$k]['service_class_id']
                    );
                }
                Db::table('service_biz')->insertAll($serviceInfoSession);

                #删除服务权限缓存
                Session::delete('serviceInfoSession');

            }

            $relate_employee = $data['relate_employee'];
            unset($data['relate_employee']);
            $data['extract_selfservice'] = 0.1;
            $data['extract_selfpro'] = 0.1;
            # 判断是否是审核状态变上架
            $agentInfo=Db::name('biz')->field("biz_status,agent_id")->where(['id' => intval($data['id'])])->find();
            if(!empty($agentInfo) and $agentInfo['biz_status']==2 and $data['biz_status']==1 and !empty($agentInfo['agent_id'])){
                AgentTask::bizRecommendTask($agentInfo['agent_id']);
            }
            if (Db::name('biz')->where(['id' => intval($data['id'])])->update($data)) {
                $id = $data['id'];
            }
            if (isset($relate_employee) && $relate_employee > 0) {
                $assign_staff = Db::name('assign_staff')->where('type=1 and biz_id=' . $data['id'])->find();
                // Db::name('assign_staff')->where('type=1 and employee_id=106')->delete();
                if (empty($assign_staff)) {
                    Db::name('assign_staff')->insertGetId(['type' => 1, 'biz_id' => $data['id'], 'employee_id' => $relate_employee]);
                } else {
                    Db::name('assign_staff')->where('type=1 and biz_id=' . $data['id'])->update(['employee_id' => $relate_employee]);
                }

            }
            Db::commit();
            return json(DataReturn('保存成功', 0));
        }


    }

    /*
     * @content  获取 结算价格 是否有
     * @params type 0 大 1  中  2 小  mark :online 线上  offline 线下   $v['carprice']:结算价格数组 biz_id :门店id  service_id  服务id
     * */
    static function getBizServiceprice($biz_id, $service_id, $carprice)
    {

        $_where = "biz_id = $biz_id and service_id = $service_id ";
        #线上结算价格
        $dcarprice_online = self::getservice_car_mediscount($_where, 1, 3);
        #执行 添加  修改 结算价格 $carprice[0]:大型车价格 $carprice[1] 中型车价格 $carprice[2] 小型车价格
        $data = self::getservice_car_mediscount_save($dcarprice_online, $carprice[0], $_where, 1, 3, $biz_id, $service_id);

        $zcarprice_online = self::getservice_car_mediscount($_where, 1, 2);
        $data = self::getservice_car_mediscount_save($zcarprice_online, $carprice[1], $_where, 1, 2, $biz_id, $service_id);

        $xcarprice_online = self::getservice_car_mediscount($_where, 1, 1);
        $data = self::getservice_car_mediscount_save($xcarprice_online, $carprice[2], $_where, 1, 1, $biz_id, $service_id);

        #线下结算价格
        $dcarprice_offline = self::getservice_car_mediscount($_where, 2, 3);
        $data = self::getservice_car_mediscount_save($dcarprice_offline, $carprice[0], $_where, 2, 3, $biz_id, $service_id);

        $zcarprice_offline = self::getservice_car_mediscount($_where, 2, 2);
        $data = self::getservice_car_mediscount_save($zcarprice_offline, $carprice[1], $_where, 2, 2, $biz_id, $service_id);

        $xcarprice_offline = self::getservice_car_mediscount($_where, 2, 1);
        $data = self::getservice_car_mediscount_save($xcarprice_offline, $carprice[2], $_where, 2, 1, $biz_id, $service_id);

        return true;

    }

    /*查询是否有当前门店id和服务id  的 结算价格*/
    static function getservice_car_mediscount($_where, $service_type, $car_level_id)
    {

        $data = Db::table('service_car_mediscount')->where($_where)->where(array('service_type' => $service_type, 'car_level_id' => $car_level_id))->value('id');
        return $data;
    }

    /*修改 结算 价格  或 新创建 结算价格*/
    static function getservice_car_mediscount_save($data, $carprice, $_where, $service_type, $car_level_id, $biz_id, $service_id)
    {
        #查下大中  下 的 服务 原价格 （discount）
        $discount = Db::table('service_car_mediscount')->where("biz_id = 0 and service_id = $service_id ")->where(array('service_type' => $service_type, 'car_level_id' => $car_level_id))->value('discount');
        if (!empty($data)) {
            #有  直接 修改  当前的 结算价格         直接修改 服务价格
            $res = Db::table('service_car_mediscount')->where($_where)->where(array('service_type' => $service_type, 'car_level_id' => $car_level_id))->update(array('settlement_amount' => $carprice, 'discount' => $discount));

        } else {
            #没有  新添加一个
            $res = Db::table('service_car_mediscount')->insertGetId(array(
                'service_id' => $service_id,
                'car_level_id' => $car_level_id,
                'service_type' => $service_type,
                'biz_id' => $biz_id,
                'settlement_amount' => $carprice,
                'discount' => $discount,
            ));
        }

        return $res;
    }

    /**
     * 查看记录处理
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    $type ：记录类型
     * @param    $biz_id ：门店id
     * @param    $search ：搜索条件
     */
    function BizDealRecord($type, $biz_id, $search, $listRows)
    {
        $_where = "ls.biz_id = $biz_id";
        if ($type == 1) {//服务记录
            #服务项目 名称
            if (!empty($search['searchTitle'])) {
                $_where .= " and ls.log_service_source like '%" . $search['searchTitle'] . "%'";
            }
            #筛选时间
            if (!empty($search['searchTime_max'])) {
                $_where .= "and ls.log_service_create >= '" . $search['searchTime_max'] . "'";
            }
            if (!empty($search['searchTime_min'])) {
                $_where .= " and ls.log_service_create <= '" . $search['searchTime_min'] . "'";
            }


            #服务记录
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = BizService::BizRecordIndex($data_params, $biz_id, $type);


        } else if ($type == 2) {//收益记录
            #服务项目 名称
            if (!empty($search['searchTitle'])) {
                $_where .= " and b.biz_title like '%" . $search['searchTitle'] . "%'";
            }
            #收益记录
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = BizService::BizRecordIndex($data_params, $biz_id, $type);

        } else if ($type == 3) {//提现记录
            #服务项目 名称
            if (!empty($search['searchTitle'])) {
                $_where .= " and b.biz_title like '%" . $search['searchTitle'] . "%'";
            }
            #提现记录
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = BizService::BizRecordIndex($data_params, $biz_id, $type);

        } else {//评论记录
            #服务项目 名称
            if (!empty($search['searchTitle'])) {
                $_where .= " and b.biz_title like '%" . $search['searchTitle'] . "%'";
            }
            #评论记录
            // 获取列表
            $data_params = array(
                'page' => true, //是否分页
                'number' => $listRows, //分页个数
                'where' => $_where,//查找条件
            );
            $data = BizService::BizRecordIndex($data_params, $biz_id, $type);

        }
        return $data;

    }

    /**
     * [BizRecordIndex 服务记录]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function BizRecordIndex($params, $biz_id, $type)
    {
        #where条件  写成活的
        $where = empty($params['where']) ? array('ls.biz_id' => $biz_id) : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
        if ($type == 1) {
            #获取服务记录列表信息
            $data = Db::table('log_service ls')
                ->field("ls.*,b.biz_title,b.biz_phone,b.biz_leader")
                ->join('biz b', 'b.id = ls.biz_id', 'left')
                ->where($where)->order('ls.id desc');
        } else if ($type == 2) {
            #获取收益记录列表信息
            $data = Db::table('biz_income ls')
                ->field("ls.*,b.biz_title,b.biz_phone,b.biz_leader")
                ->join('biz b', 'b.id = ls.biz_id', 'left')
                ->where($where)->order('ls.id desc');
        } else if ($type == 3) {
            #获取提现记录列表信息
            $data = Db::table('log_deposit ls')
                ->field("ls.*,b.biz_title,b.biz_phone,b.biz_leader")
                ->join('biz b', 'b.id = ls.biz_id', 'left')
                ->where($where)->order('ls.id desc');
        } else {
            #获取评论记录列表信息
            $data = Db::table('evaluation_score ls')
                ->field("ls.*,b.biz_title,b.biz_phone,b.biz_leader,e.employee_name")
                ->join('biz b', 'b.id = ls.biz_id', 'left')
                ->join('employee_sal e', 'e.id = ls.employee_id', 'left')
                ->where($where)->order('ls.id desc');
        }

        //分页
        $page_html = null;
        $data = $data->paginate($number, false, ['query' => request()->param()]);

        $page_html = $data->render();
        $data = $data->toArray();


        $params['page_html'] = $page_html;
        #处理方法

        $re = self::RecordDataDealWith($data, $type);

        return $re;

    }

    #记录处理方法
    function RecordDataDealWith($data, $type)
    {
        if (!empty($data['data'])) {
            if ($type == 1) {
                #服务记录

                foreach ($data['data'] as $k => $v) {
                    //支付方式
                    if (isset($v['pay_type'])) {

                        $data['data'][$k]['pay_type'] = BaseService::StatusHtml($v['pay_type'], lang('cash_type'), false);
                    }
                    #处理价格
                    if (isset($v['log_service_price'])) {
                        $data['data'][$k]['log_service_price'] = getsPriceFormat($v['log_service_price']);
                    }
                }


            } else if ($type == 2) {
                #收益记录
                foreach ($data['data'] as $k => $v) {
                    //收入来源
                    if (isset($v['income_type'])) {
                        switch ($v['income_type']) {
                            case 1:
                                $data[$k]['income_type'] = '服务收入';
                                break;
                            case 2:
                                $data[$k]['income_type'] = '办理会员';
                                break;
                            case 3:
                                $data[$k]['income_type'] = '商品销售';
                                break;
                            case 4:
                                $data[$k]['income_type'] = '活动收入';
                                break;
                            case 5:
                                $data[$k]['income_type'] = '会员充值';
                                break;
                            case 5:
                                $data[$k]['income_type'] = '会员升级';
                                break;

                        }
                    }
                    if (isset($v['pay_type'])) {
                        $data['data'][$k]['pay_type'] = BaseService::StatusHtml($v['pay_type'], lang('cash_type'), false);

                    }
                    #处理价格
                    if (isset($v['biz_income'])) {
                        $data['data'][$k]['biz_income'] = getsPriceFormat($v['biz_income']);
                    }
                }

            } else if ($type == 3) {
                #提现记录
                foreach ($data['data'] as $k => $v) {
                    //到账方式
                    if (isset($v['deposit_type'])) {
                        switch ($v['deposit_type']) {
                            case 1:
                                $data['data'][$k]['deposit_type'] = '微信';
                                break;
                            case 2:
                                $data['data'][$k]['deposit_type'] = '支付宝';
                                break;


                        }
                    }

                }
            } else {
                #评论记录
                foreach ($data['data'] as $k => $v) {
                    //评论等级
                    if (isset($v['evalua_level'])) {
                        switch ($v['evalua_level']) {
                            case 0:
                                $data['data'][$k]['evalua_level'] = '无星';
                                break;
                            case 1:
                                $data['data'][$k]['evalua_level'] = '1星';
                                break;
                            case 2:
                                $data['data'][$k]['evalua_level'] = '2星';
                                break;
                            case 3:
                                $data['data'][$k]['evalua_level'] = '3星';
                                break;
                            case 4:
                                $data['data'][$k]['evalua_level'] = '4星';
                                break;
                            case 5:
                                $data['data'][$k]['evalua_level'] = '5星';
                                break;


                        }
                    }
                    //评论状态
                    if (isset($v['default_mark'])) {
                        switch ($v['default_mark']) {
                            case 1:
                                $data['data'][$k]['default_mark'] = '默认';
                                break;
                            case 2:
                                $data['data'][$k]['default_mark'] = '用户评论';
                                break;


                        }
                    }
                    //类型
                    if (isset($v['type'])) {
                        switch ($v['type']) {
                            case 1:
                                $data['data'][$k]['type'] = '服务';
                                break;
                            case 2:
                                $data['data'][$k]['type'] = '商品';
                                break;


                        }
                    }
                    #处理价格
                    if (isset($v['biz_income'])) {
                        $data['data'][$k]['biz_income'] = getsPriceFormat($v['biz_income']);
                    }
                }
            }

        }
        return $data;

    }


    /************************商品权限start***************************************/
    /**
     * [BizProMould 商品权限模板]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function BizProMould($list, $biz_id, $countBizpro, $search_title = '', $select_value = '')
    {
        $str = '';
        if (!empty($list) and is_array($list)) {
            /*
             *   <div class=\"form-groupinput-group-lg\" >
                    <select id=\"select_biztype\"   class=\"select2\" name=\"bizType\" lay-filter=\"selectInfo\">
                        <option  value=\"\"  >商品类型-筛选</option>
                        <option  value=\"1\" {if condition='$select_value == 1'}cheacked{/if} >自营店</option>
                        <option  value=\"2\"  {if condition='$select_value == 2'}cheacked{/if}>加盟店 </option>
                        <option  value=\"3\"  {if condition='$select_value == 3'}cheacked{/if}>合作店</option>

                    </select>
                    <i class=\"form-group__bar\"></i>
                </div>*/
            $str = " 
              <input type='hidden' name='activeType' id='activeType' value='pro'>
              <div class=\"row\" style=\"width: 100 %;float: left\">
                                                                                <div class=\"col - sm - 6\">
                                                                                    <div class=\"form - group\">
                                                                                        <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchServiceName()\" placeholder=\"请输入商品名称，回车键搜索\" autocomplete=\"off\">
                                                                                        <i class=\"form - group__bar\"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <br>
              
                                                                    
             <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                      
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            <button class=\"btn btn-info\" onclick=\"relevanceOption('check-item')\" style=\"\">批量关联</button>
                                            <button class=\"btn btn-info\" onclick=\"relateOption('$biz_id','pro','" . $search_title . "','" . $select_value . "')\" style=\"\">已关联:{$countBizpro}</button>
                                        </div>
                                    </div>
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                                                <th>商品名称</th>
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

            foreach ($list as $k => $v) {
                $str .= "
                    <tr class='service" . $v['id'] . "'>
                        <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ";
                if (!empty($_default_array) and in_array($v['id'], $_default_array)) {
                    $str .= 'checked';
                }
                /*
                 * x销售价格                        <td>" . getsPriceformat($v['biz_pro_online']) . "</td>
*/
                $str .= "></td>
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$v['class_title']}</td>
                        <td>" . getsPriceformat($v['biz_pro_purch']) . "</td>
                      
                        <td> <button class=\"btn btn-info\" onclick=\"aloneOption('{$v['id']}','pro','" . $search_title . "','" . $select_value . "')\" style=\"\">关联</button></td>
                    </tr>";
            }
            $str .= "
                </tbody>
            </table>
        </div>
            </div>";
        }


        return $str;

    }

    /**
     * [getBizproArrInfo 商品权限  已关联列表页面整理]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getBizproArrInfo($arr, $mark, $_bizId)
    {
        $str = '';

        if (!empty($_bizId)) {
            if (!empty($arr) and is_array($arr)) {
                /*
                       *   <div class="row" style="width: 100%;float: left">
                                                          <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <input type="text" id="search_title"  class="form-control" onkeyup="searchRelevanceStore()" placeholder="请输入门店名称，回车键搜索" autocomplete="off">
                                                                  <i class="form-group__bar"></i>
                                                              </div>
                                                          </div>
                                                      </div>
                       * */
                $str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\"><h3  style='justify-content: center;height:10%;align-items: center'>已关联商品</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>商品名称</th>
                                               <!-- <th>商品编号</th>-->
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                             <!--   <th>销售价格</th>-->
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach ($arr as $k => $v) {
                    /*   销售价格 <td onclick=\"bizproWrite('" . $k . "','" . $v['id'] . "','" . $v['biz_pro_online'] . "','" .$mark . "','4','" . $v['biz_id'] . "')\">" . getsPriceformat($v['biz_pro_online']) . "</td>*/
                    $str .= "
                    <tr class='lump" . $v['id'] . "'>
                    
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$v['class_title']}</td>
                        <td>" . getsPriceformat($v['biz_pro_purch']) . "</td>
                    
                        <td> <button class=\"layui-btn layui-btn-info layui-btn-xs\" onclick=\"bizProWriteList('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >修改</button>
                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizProDel('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >删除</button>
                        </td>

                    </tr>";
                }
                $str .= "
                </tbody>
            </table>
        </div>
            </div>";
            }

        } else {
            if (!empty($arr) and is_array($arr)) {
                /*
                       *   <div class="row" style="width: 100%;float: left">
                                                          <div class="col-sm-6">
                                                              <div class="form-group">
                                                                  <input type="text" id="search_title"  class="form-control" onkeyup="searchRelevanceStore()" placeholder="请输入门店名称，回车键搜索" autocomplete="off">
                                                                  <i class="form-group__bar"></i>
                                                              </div>
                                                          </div>
                                                      </div>
                       * */
                $str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\"><h3>暂未关联商品(保存之后变成已关联)</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>商品名称</th>
                                               <!-- <th>商品编号</th>-->
                                                <th>商品分类</th>
                                                <th>供货价格</th>
                                             <!--   <th>销售价格</th>-->
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach ($arr as $k => $v) {
                    /*           商品编号                <td>{$v['biz_pro_number']}</td>
                     销售价格 <td onclick=\"bizproWrite('" . $k . "','" . $v['id'] . "','" . $v['biz_pro_online'] . "','" .$mark . "','4','" . $v['biz_id'] . "')\">" . getsPriceformat($v['biz_pro_online']) . "</td>*/
                    $str .= "
                    <tr class='lump" . $v['id'] . "'>
                    
                        <td>{$v['biz_pro_title']}</td>
                        <td>{$v['class_title']}</td>
                        <td>" . getsPriceformat($v['biz_pro_purch']) . "</td>
                   
                         <td> <button class=\"layui-btn layui-btn-info layui-btn-xs\" onclick=\"bizProWriteList('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >修改</button>
                         <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"bizProDel('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >删除</button>
                         </td>

                    </tr>";
                }
                $str .= "
                </tbody>
            </table>
        </div>
            </div>";
            }

        }

        return $str;

    }

    /**
     * [BizGoodsInfo 商品权限 门店里的商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function BizGoodsInfo($bizId)
    {
        $info = Db::table("biz_goods")->field('pro_id')->where(array('biz_id' => $bizId))->select();
        return $info;

    }
    /************************商品权限end***************************************/

    /** ***********************关联门店start*************************************************/
    static function RelationBizMould($list, $biz_id, $countService, $search_title = '', $select_value = '', $arr = array())
    {
        $str = '';
        if (!empty($list) and is_array($list)) {

            $str = " 
              <div class=\"row\" style=\"width: 50 %;float: left\">
                            <input type='hidden' name='activeType' id='activeType' value='service'>
                <div class=\"col-sm-12\">
                    <div class=\"form-group\">
                        <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchServiceName()\" placeholder=\"请输入服务名称，回车键搜索\" autocomplete=\"off\">
                        <i class=\"form-group__bar\"></i>
                    </div>
                </div>
            </div>
               
                <br>
            <div class=\"card-body\">
                <div class=\"col-sm-12\">
              
            <div class=\"row\" style=\"width: 100%;float: left\">
                <div class=\"col-sm-6\">
                    <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                    <button class=\"btn btn-info\" onclick=\"serviceOption('check-item')\" style=\"\">批量关联</button>
                    <button class=\"btn btn-info\" onclick=\"ServiceRelation('" . $biz_id . "','service','" . $search_title . "','" . $select_value . "')\" style=\"\">已关联:{$countService}</button>
                </div>
            </div>
            <table class=\"table table-hover mb-0\">
                <thead>
                    <tr>
                        <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                        <th>门店名称</th>
                        <th>联系方式</th>
                        <th>门店类型</th>
                        <th>所在区域</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody id='storebody'>";
            foreach ($list as $k => $v) {
                $str .= "
                    <tr class='service" . $v['id'] . "'>
                        <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ";
                # 门店类型
                if ($v['biz_type'] == 2) {
                    $bizType = '加盟店';
                } else {
                    if ($v['recommend_status'] == 1) {
                        $bizType = 'A类门店';
                    } else {
                        $bizType = 'B类门店';
                    }
                }
                $str .= "></td>
                        <td>{$v['biz_title']}</td>
                        <td>{$v['biz_phone']}</td>
                        <td>{$bizType}</td>
                        <td>{$v['province']}-{$v['city']}-{$v['area']}</td>
                        <td> <button class=\"btn btn-info\" onclick=\"aloneServiceOption('{$v['id']}','service','" . $search_title . "','" . $select_value . "')\" style=\"\">关联</button></td>
                    </tr>";
            }
            $str .= "
                </tbody>
            </table>
        </div>
            </div>";
        }


        return $str;

    }
    /** ***********************关联门店end*************************************************/
    /************************服务权限start***************************************/
    /**
     * [ServiceMould 服务权限模板]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function ServiceMould($list, $biz_id, $countService, $search_title = '', $select_value = '', $arr = array())
    {
        $str = '';
        /* <div class=\"form-group\" >
                    <select id=\"select_biztype\"   class=\"select2\" name=\"bizType\" lay-filter=\"selectInfo\">
                        <option  value=\"\"  >服务类型-筛选</option>
                        <option  value=\"1\" {if condition='$select_value == 1'}cheacked{/if} >自营店</option>
                        <option  value=\"2\"  {if condition='$select_value == 2'}cheacked{/if}>加盟店 </option>
                        <option  value=\"3\"  {if condition='$select_value == 3'}cheacked{/if}>合作店</option>

                    </select>
                    <i class=\"form-group__bar\"></i>
                </div>
         * */
        if (!empty($list) and is_array($list)) {

            $str = " 
              <div class=\"row\" style=\"width: 50 %;float: left\">
                            <input type='hidden' name='activeType' id='activeType' value='service'>

                                                                                <div class=\"col-sm-12\">
                                                                                    <div class=\"form-group\">
                                                                                        <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchServiceName()\" placeholder=\"请输入服务名称，回车键搜索\" autocomplete=\"off\">
                                                                                        <i class=\"form-group__bar\"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
               
                <br>
 <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                      
                                    <div class=\"row\" style=\"width: 100%;float: left\">
                                        <div class=\"col-sm-6\">
                                            <button class=\"btn btn-info\" data-mark=\"checkall\" onclick=\"checkAll(this,'check-item')\" style=\"\">全选</button>
                                            <button class=\"btn btn-info\" onclick=\"serviceOption('check-item')\" style=\"\">批量关联</button>
                                            <button class=\"btn btn-info\" onclick=\"ServiceRelation('" . $biz_id . "','service','" . $search_title . "','" . $select_value . "')\" style=\"\">已关联:{$countService}</button>
                                        </div>
                                    </div>
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th><input type=\"checkbox\" class=\"checkall\" onclick=\"checkAll(this,'check-item')\"></th>
                                                <th>服务名称</th>
                                                <th>服务分类</th>
                                                <th>服务价格</th>
                                                <th>服务时长</th>
                                                <th>是否预约</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
            foreach ($list as $k => $v) {
                $str .= "
                    <tr class='service" . $v['id'] . "'>
                        <td><input type=\"checkbox\" value=\"{$v['id']}\" class=\"check-item\" ";

                if ($v['appoint'] == 1) {
                    $appoint = '是';

                } else {
                    $appoint = '是';

                }
                //service  是service 查询的是服务本身的价格
                $str .= "></td>
                        <td>{$v['service_title']}</td>
                        <td>{$v['service_class_title']}</td>
                        <td><button class=\"btn btn-info\" onclick=\"checkServiceCarPrice('{$v['id']}','service')\" >查看</button></td>
                        <td>{$v['service_time']}</td>
                        <td>$appoint</td>
                        <td> <button class=\"btn btn-info\" onclick=\"aloneServiceOption('{$v['id']}','service','" . $search_title . "','" . $select_value . "')\" style=\"\">关联</button></td>
                    </tr>";
            }
            $str .= "
                </tbody>
            </table>
        </div>
            </div>";
        }


        return $str;

    }

    /**
     * [ServiceGoodsInfo 商品权限 门店里的商品]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function ServiceGoodsInfo($bizId)
    {
        $info = Db::table("service_biz")->field('service_id')->where(array('biz_id' => $bizId))->select();
        return $info;

    }

    /**
     * [getServiceArrInfo 服务权限  已关联列表页面整理]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getServiceArrInfo($arr, $mark, $_bizId, $search_title = '')
    {
        $str = '';

        if (!empty($_bizId)) {
            if (!empty($arr) and is_array($arr)) {

                $str = " <div class=\"card-body\">
                                        <div class=\"col-sm-12\"><h3  style='justify-content: center;height:10%;align-items: center'>已关联服务</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>服务名称</th>
                                                <th>服务分类</th>
                                                <th>服务价格</th>
                                                <th>结算价格</th>
                                                <th>是否预约</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach ($arr as $k => $v) {
                    if ($v['appoint'] == 1) {
                        $appoint = '是';

                    } else {
                        $appoint = '是';

                    }
                    $str .= "
                    <tr class='lump" . $v['id'] . "'>
                        <td>{$v['service_title']}</td>
                        <td>{$v['service_class_title']}</td>
                        <td><button class=\"btn btn-info\" onclick=\"checkServiceCarPrice('{$v['id']}')\" >查看</button></td>
                        <td><button class=\"btn btn-info\" onclick=\"getCheckCarPrice('" . $k . "','" . $v['id'] . "','" . getsPriceformat($v['carprice'][0]) . "','" . getsPriceformat($v['carprice'][1]) . "','" . getsPriceformat($v['carprice'][2]) . "','" . $mark . "','carprice','" . $v['biz_id'] . "')\" >查看</button></td>
                       
                        <td onclick=\"serviceStatus('" . $k . "','" . $v['id'] . "','" . $v['appoint'] . "','" . $mark . "','5','" . $v['biz_id'] . "')\">$appoint</td>
                       
                        <td> 
                        
                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"serviceDel('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >删除</button>
                        </td>

                    </tr>";
                }
                /*
                 * <button class=\"layui-btn layui-btn-info layui-btn-xs\" onclick=\"serviceWriteList('$k','{$v['id']}','" .$mark . "','" .$v['biz_id'] . "')\" >修改</button>
                 * */
                $str .= "
                </tbody>
            </table>
        </div>
            </div>";
            }

        } else {
            if (!empty($arr) and is_array($arr)) {

                $str = " 
                <div class=\"card-body\">
                                        <div class=\"col-sm-12\"><h3>暂未关联服务(保存之后变成已关联)</h3>
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>服务名称</th>
                                                <th>服务分类</th>
                                                <th>服务价格</th>
                                                <th>结算价格</th>
                                                <th>是否预约</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";

                foreach ($arr as $k => $v) {
                    if ($v['appoint'] == 1) {
                        $appoint = '是';

                    } else {
                        $appoint = '是';

                    }
                    $str .= "
                    <tr class='lump" . $v['id'] . "'>
                    
                        <td>{$v['service_title']}</td>
                        <td>{$v['service_class_title']}</td>
                        <td><button class=\"btn btn-info\" onclick=\"checkServiceCarPrice('{$v['id']}','service')\" >查看</button></td>
                        <td><button class=\"btn btn-info\" onclick=\"getCheckCarPrice('" . $k . "','" . $v['id'] . "','" . getsPriceformat($v['carprice'][0]) . "','" . getsPriceformat($v['carprice'][1]) . "','" . getsPriceformat($v['carprice'][2]) . "','" . $mark . "','carprice','" . $v['biz_id'] . "')\" >查看</button></td>
                       
                        <td onclick=\"serviceStatus('" . $k . "','" . $v['id'] . "','" . $v['appoint'] . "','" . $mark . "','5','" . $v['biz_id'] . "')\">$appoint</td>
                        <td> 
                       
                        <button class=\"layui-btn layui-btn-danger layui-btn-xs\" onclick=\"serviceDel('$k','{$v['id']}','" . $mark . "','" . $v['biz_id'] . "')\" >删除</button>
                        </td>

                    </tr>";
                    /*
                     *  <button class=\"layui-btn layui-btn-info layui-btn-xs\" onclick=\"serviceWriteList('$k','{$v['id']}','" .$mark . "','" .$v['biz_id'] . "')\" >修改</button>
                     * */
                }
                $str .= "
                </tbody>
            </table>
        </div>
            </div>";
            }

        }

        return $str;

    }

    /**
     * [getServicebizWrite 服务权限  修改页面 服务价格查询]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function getServicebizWrite($where)
    {
        $service_car = Db::table("service_car_mediscount scm")
            ->field("scm.id scm_id,scm.discount,cl.level_title")
            ->where($where)
            ->join("car_level cl", 'cl.id = scm.car_level_id')
            ->select();
        return $service_car;
    }

    function getServiceWrite($_where, $mark)
    {
        if ($mark == 'bizpro') {
            #商品权限
            $service = Db::table('biz_goods bg')
                ->field('bg.biz_id,bg.pro_id id,bg.goods_price,bg.goods_online biz_pro_online,bg.goods_purch biz_pro_purch,bg.settlement_min,bp.biz_pro_title,bp.biz_pro_number,bp.biz_pro_class_id')
                ->join('biz_pro bp', 'bp.id= bg.pro_id', 'left')
                ->where($_where)->find();


        } else {
            #服务权限
            $service = DB::table('service_biz sb')
                ->field("s.service_title,s.service_class_id,sb.settlement_min,sb.appoint")
                ->where($_where)
                ->join('service s', 's.id = sb.service_id')
                ->find();
        }

        return $service;
    }

    /**
     * [getServicePrice 服务权限 查看服务价格]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getServicePrice($id, $service_type, $mark = '', $bizId = 0)
    {
        $biz_id = session('bizId');
        // 9  405
        if (empty($biz_id)) {
            #门店修改操作
            #查询服务本身的价格  service_car_mediscount   门店添加操作
            $biz_id = 0;
        }
        if ($mark == 'service') {
            #查询服务本身的价格  service_car_mediscount   门店添加操作
            $biz_id = 0;
        }
        if (!empty($bizId)) {
            $biz_id = $bizId;
        }
        $data = Db::table('service_car_mediscount scm')
            ->field('scm.*,cl.level_title')
            ->join('car_level cl', 'cl.id = scm.car_level_id', 'left')
            ->where(array('service_id' => $id, 'biz_id' => $biz_id, 'service_type' => $service_type))
            ->select();
        return $data;

    }
    /************************服务权限end***************************************/
    /*合作门店的所有记录*/
    /**
     * [BizLogIndex 获取合作门店的所有的点击获取记录]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function BizLogIndex($params)
    {
        #where条件  写成活的
        $where = $params['where'];
        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
        #获取表名
        $table = $params['table'];
        #获取 需要查询的参数
        $field = $params['field'];
        $order = empty($params['order']) ? array('id' => 'desc') : $params['order'];

        #获取门店列表信息
        $data = Db::table($table)->field($field)->where($where)->order($order);
        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);

            $data = $data->toArray();
        } else {
            $data = $data->select();

        }


        return $data;

        #处理方法
//        return self::InforDataDealWith($data,$params);

    }

    /*
     * @content 获取工位信息
     * */
    static function getStation($biz_id)
    {
        $station = Db::table("station s")->field('*')
//            ->join('service_class sc','sc.id = s.service_class_id')
            ->where(array('biz_id' => $biz_id))->select();
        if (!empty($station)) {
            foreach ($station as $k => $v) {
                $station[$k]['service_deduct'] = ($v['service_deduct'] * 100);
                $station[$k]['biz_deduct'] = ($v['biz_deduct'] * 100);
//                $station[$k]['recommond_royalty'] = ($v['recommond_royalty']*100).'%';
//                $station[$k]['recommond_custom_pro'] = ($v['recommond_custom_pro']*100).'%';
//                $station[$k]['recommond_custom_service'] = ($v['recommond_custom_service']*100).'%';
                $station[$k]['custom_pro_deduct'] = ($v['custom_pro_deduct'] * 100);
                $station[$k]['custom_service_deduct'] = ($v['custom_service_deduct'] * 100);
//                $station[$k]['recommond_pro_royalty'] = ($v['recommond_pro_royalty']*100).'%';
            }

        }

        return $station;

    }

    /*
     * @content 获取结算价格
     * @params $arr 数组
     * @content $_bizId 门店id
     * @content $mark  线上  1  线下 2   默认线上
     * */
    static function getCarPrice($arr, $_bizId, $service_type = 1)
    {
        if (!empty($arr)) {
            foreach ($arr as $k => $v) {
                $arr[$k]['carprice'] = Db::table('service_car_mediscount')->where(array('service_id' => $v['id'], 'biz_id' => $_bizId, 'service_type' => $service_type))->order('car_level_id desc')->column('settlement_amount');

            }
            return $arr;
        }

    }

    /*
     * @content  清除 结算价格 为0
     * */

    static function cleanPriceCar($serviceId, $bizId)
    {
        $_where = "biz_id = $bizId and service_id = $serviceId ";
        $data = Db::table('service_car_mediscount')->where($_where)->update(array('settlement_amount' => 0));
        return true;

    }


    public static function BizIncomeData_old($params)
    {
        $info = $params["searchParams"];

        $bizId = $params["bizId"];

        $_where = [];

        $_where[] = ['biz_id', '=', $bizId];

        #开始时间
        if (!empty($info['start_time'])) {
            $start_time = date_create($info['start_time']);

            $start_time = date_format($start_time, "Y-m-d H:i:s");

            $_where[] = ['create', '>=', $start_time];
        }

        #结束时间
        if (!empty($info['end_time'])) {
            $end_time = date_create($info['end_time']);

            $end_time = date_format($end_time, "Y-m-d H:i:s");

            $_where[] = ['create', '<=', $end_time];
        }

        //收入类型
        if (!empty($info['income_type'])) {

            $_where[] = ['type', '=', $info['income_type']];
        } else {
            $_where[] = ['type', '>=', 3];
        }


        // 获取列表
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => $_where,
            'table' => 'deduct',
            'field' => '*',
            'order' => 'create desc',
        );

        $data = self::BizLogIndex($data_params);


        foreach ($data['data'] as &$v) {

            $v['member_phone'] = '未获取用户信息';

            $v['member_level_title'] = '未获取用户手机号';

            $v['member_name'] = '未获取用户名称';

            $v['total_remark'] = '';


            //支付方式
            $v['pay_type'] = BaseService::StatusHtml($v['pay_type'], lang('cash_type'), false);

            //实际收入
            $v['biz_income_actual'] = $v['biz_income'];

            //收入类型
            $v['type_title'] = BaseService::StatusHtml($v['type'], [3 => '办理会员', 4 => '会员升级', 5 => '会员充值'], false);


            //办理会员
            if ($v['type'] == 3) {
                $v['biz_income_actual'] = $v['biz_income'] * 0.03;

                if (strlen($v['order_number']) == 11) {
                    $v['member_id'] = Db::name('orders')->where('order_number=' . $v['order_number'])->value('member_id');
                }

                if (strlen($v['order_number']) == 1) {
                    $v['member_id'] = $v['order_server_id'];

                    $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone'] . '办理<i class="zmdi zmdi-card zmdi-hc-fw"></i>' . BaseService::StatusHtml($v['order_number'], lang('member_level'), false);;

                }

//                if (strlen($v['order_server_id'])==1)
//                {
//                    $v['member_id']=$v['order_number'];
//
//                    $v['total_remark']=$v['member_name'].'<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>'.$v['member_phone'].'办理<i class="zmdi zmdi-card zmdi-hc-fw"></i>'.BaseService::StatusHtml($v['order_server_id'] ,lang('member_level'),false);;
//
//                }


                self::SelectMemberInfo($v);


            }

            //升级会员
            if ($v['type'] == 4) {
                $v['order_number'] = str_replace(['level', 'up'], ',', $v['order_number']);

                $order_arr = explode(',', $v['order_number']);

                if (count($order_arr) == 3) {
                    $v['member_id'] = $order_arr[0];

                    self::SelectMemberInfo($v);

                    $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone'] . '升级到<i class="zmdi zmdi-card zmdi-hc-fw"></i>' . BaseService::StatusHtml($order_arr[2], lang('member_level'), false);
                }

                $v['biz_income_actual'] = $v['biz_income'] * 0.01;

            }
            //会员充值:张三 13054789654 会员充值25元
            if ($v['type'] == 5) {
                if (strlen($v['order_number']) == 11) {
                    $v['member_id'] = Db::name('orders')->alias('o')
                        //->leftJoin(['member'=>'m'],'m.id=o.member_id')
                        ->where('o.order_number=' . $v['order_number'])
                        ->value('member_id');
                    self::SelectMemberInfo($v);

                    $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone'] . '充值￥' . priceFormat($v['deduct'] * 100);

                }

                if (strlen($v['order_server_id']) < 11 && intval($v['order_server_id']) == $v['order_server_id']) {
                    $v['member_id'] = $v['order_server_id'];

                    self::SelectMemberInfo($v);

                    $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone'] . '充值￥' . priceFormat($v['deduct'] * 100);

                }


            }


        }


        $total_info = Db::table('deduct')->where($_where)->sum('deduct');

        $data['total_price'] = priceFormat($total_info);

        return $data;
    }

    /**
     * 门店收益
     * @param    [array]          $params [输入参数]
     * @version  0.0.1
     * @datetime 2020年9月28日14:13:59
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     */
    public static function BizIncomeData($params)
    {

        $info = $params["searchParams"];

        $bizId = $params["bizId"];

        $_where = [];


        $_where[] = ['member_id', 'in', Db::table('channel')->where("biz_id = $bizId and action_type = 2")
            ->column('member_id')];

        #开始时间
        if (!empty($info['start_time'])) {
            $start_time = date_create($info['start_time']);

            $start_time = date_format($start_time, "Y-m-d H:i:s");

            $_where[] = ['consump_time', '>=', $start_time];
        }

        #结束时间
        if (!empty($info['end_time'])) {
            $end_time = date_create($info['end_time']);

            $end_time = date_format($end_time, "Y-m-d H:i:s");

            $_where[] = ['consump_time', '<=', $end_time];
        }

        //收入类型
        if (!empty($info['income_type'])) {

            $_where[] = ['income_type', '=', $info['income_type']];
        } else {
            $_where[] = ['income_type', 'in', [2, 6, 7]];
        }
        // 获取列表
        $data_params = array(
            'page' => true,
            'number' => 10,
            'where' => $_where,
            'table' => 'log_consump',
            'field' => '*',
            'order' => 'consump_time desc',
        );

        $data = self::BizLogIndex($data_params);

        foreach ($data['data'] as &$v) {

            $v['member_phone'] = '未获取用户信息';

            $v['member_level_title'] = '未获取用户手机号';

            $v['member_name'] = '未获取用户名称';

            $v['total_remark'] = '';

            //时间
            $v['create'] = $v['consump_time'];

            //支付方式
            $v['pay_type'] = BaseService::StatusHtml($v['consump_pay_type'], lang('cash_type'), false);

            //收入类型
            $v['type_title'] = BaseService::StatusHtml($v['income_type'], lang('member_fee_aim'), false);

            self::SelectMemberInfo($v);

            //办理会员
            if ($v['income_type'] == 2) {
                // $v['deduct_commission']=priceFormat($v['consump_price']*0.06);
                $v['deduct_commission'] = Db::table("deduct")->where(array("refer_mark" => 3, "order_server_id" => $v['member_id'], "biz_id" => $bizId))
                    ->where("date(`create`)='" . date("Y-m-d", strtotime($v['consump_time'])) . "'")
                    ->order("id desc")
                    ->limit(1)
                    ->value("deduct");

                $levels = Db::name('member_level')->where([['level_practical', '<=', $v['consump_price']]])
                    ->order('level_practical desc')->find();
                $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone']
                    . '办理<i class="zmdi zmdi-card zmdi-hc-fw"></i>' . $levels['level_title'];

            }

            //升级会员
            if ($v['income_type'] == 6) {
                $v['deduct_commission'] = priceFormat($v['consump_price'] * 0.01);


                $upgrade = Db::name('upgrade')->where([['price', '<=', $v['consump_price']]])
                    ->order('price desc')->find();

                $level_title = Db::name('member_level')->where([['id', '=', $upgrade['up_level']]])->value('level_title');

                $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone']
                    . '升级到<i class="zmdi zmdi-card zmdi-hc-fw"></i>' . $level_title;
            }
            //会员充值:张三 13054789654 会员充值25元
            if ($v['income_type'] == 7) {
                $v['deduct_commission'] = priceFormat($v['consump_price'] * 0.01);

                $v['total_remark'] = $v['member_name'] . '<i class="zmdi zmdi-smartphone-iphone zmdi-hc-fw"></i>' . $v['member_phone']
                    . '充值￥' . priceFormat($v['consump_price']);

            }

            //实际收入
            $v['deduct'] = priceFormat($v['consump_price']);

        }


        $total_info = Db::table('log_consump')->where($_where)->select();

        $data['total_price'] = 0.00;

        foreach ($total_info as $tv) {
            if ($v['income_type'] == 7) {
                $data['total_price'] += $tv['consump_price'] * 0.01;
            }

            if ($v['income_type'] == 6) {
                $data['total_price'] += $tv['consump_price'] * 0.01;
            }

            //办理会员
            if ($v['income_type'] == 2) {
                $data['total_price'] += $tv['consump_price'] * 0.03;
            }
        }

        $data['total_price'] = priceFormat($data['total_price']);
        $taskPrice = Db::table('cooperative_task_list')
            ->where(array('status' => 3, 'biz_id' => $bizId))
            ->sum('reward_balance');
        $data['task_price'] = priceFormat($taskPrice);

        return $data;
    }

    /**
     * 获取用户信息
     * @param   [array]          $where [输入参数]
     * @version 1.0.0
     * @datetime    2020年12月31日11:56:09
     * @desc    description
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     */
    public static function SelectMemberInfo(&$v)
    {
        if ($v['member_id'] > 0) {

            $member = Db::name('member')->alias('m')
                ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=m.id')
                ->where('m.id=' . $v['member_id'])
                ->field('m.member_phone,mm.member_level_id')
                ->find();

            $v['member_phone'] = $member['member_phone'];

            $v['member_name'] = MemberService::SelectMemberName(['id' => $v['member_id']]);

            if ($member['member_level_id'] > 0) {

                $v['member_level_title'] = BaseService::StatusHtml($member['member_level_id'], lang('member_level'), false);

            } else {

                $v['member_level_title'] = '普通用户';
            }
        }

        return $v;
    }


}
