<?php


namespace app\service;

/**
 * 费用管理服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月19日15:32:54
 */
class DisburseSrervice
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月19日15:32:54
     * @param    [array]          $params [输入参数]
     */
    public static function DisburseListWhere($params = [])
    {
        $where = [];

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['phone', 'like', '%'.$params["param"]['phone'].'%'];
        }
        //提现类型
        if(isset($params['launch_cate'])&&intval($params['launch_cate'])>0)
        {
            $where[] = ['launch_cate', '=', $params['launch_cate']];
        }
        return $where;
    }
}