<?php


namespace app\service;

use think\console\command\make\Model;
use think\Db;

/**
 * 提现管理服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月19日15:32:54
 */
class FinanceService
{
    /**
     * 提现列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月19日15:32:54
     * @param    [array]          $params [输入参数]
     */
    public static function DepositListWhere($params = [])
    {
        $where = [];
        $where[] = ['member_id','neq',25];
//        dump($params);exit;
        //手机号
        if(!empty($params['param']['phone']))
        {
            $where[] =['phone', 'like', '%'.$params["param"]['phone'].'%'];
        }
        //关键字:手机 姓名
        if(!empty($params['param']['keywords']))
        {
            $where[] =['phone|username', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //提现时间
        if(!empty($params['param']['create_time']))
        {
            $time=strtotime($params["param"]['create_time']);
            $where[] =['create_time', '>', date('Y-m-d 00:00:00',$time)];
            $where[] =['create_time', '<', date('Y-m-d 23:59:59',$time)];

        }
        if(!empty($params['create_time']))
        {

            $where[] =['create_time', '>', date('Y-m-d 00:00:00',time())];
            $where[] =['create_time', '<', date('Y-m-d 23:59:59',time())];

        }
        //提现金额
        if(!empty($params['param']['price']))
        {
            $where[] =['price', '=', $params["param"]['price']];
        }
        //提现类型
        if(isset($params['launch_cate'])&&intval($params['launch_cate'])>0)
        {
            $where[] = ['launch_cate', '=', $params['launch_cate']];
        }
        if(isset($params["param"]['total_cate'])&&intval($params["param"]['total_cate'])>0)
        {
            switch ($params["param"]['total_cate']){
                case 1:
                    $where[] = ['source_type', '=', 2];
                    break;
                case 2:
                    $where[] = ['source_type', '=', 1];
                    $where[] = ['launch_cate', '=', 3];
                    break;
                case 3:
                    $where[] = ['source_type', '=', 1];
                    $where[] = ['launch_cate', '=', 2];
                    break;
                case 4:
                    // 联盟商家提现
                    $where[] = ['source_type', '=', 3];
                    $where[] = ['launch_cate', '=', 5];
                    break;
                default:
                    $where[] = ['launch_cate', '=', $params['launch_cate']];
                    break;
            }
        }
       // dump($where);exit;
        //提现状态
        if (isset($params['deposit_status']) && intval($params['deposit_status']) > 0) {
            if($params['deposit_status']==4){
                $where[] = ['deposit_status', ['=', 4], ['=', 5], 'or'];
            }else{
                $where[] = ['deposit_status', '=', $params['deposit_status']];
            }
        }

        if (isset($params['param']['deposit_status']) && intval($params['param']['deposit_status']) > 0) {
            if ($params['param']['deposit_status'] == 2) {
                $where[] = ['deposit_status', ['=', 1], ['=', 2], 'or'];
            }elseif ($params['param']['deposit_status']==4){
                $where[] = ['deposit_status', ['=', 4], ['=', 5], 'or'];
            } else {
                $where[] = ['deposit_status', '=', $params['param']['deposit_status']];
            }
        }

        return $where;
    }
    /**
     * 获取提现列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function DepositList($params)
    {

        $data=BaseService::DataList($params);
        return self::DepositDataDealWith($data);

    }
    /**
     * 获取提现总金额
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $where [查询条件]
     */
    public static function DepositTotalPrice($where){
        $price = Db::name('log_deposit')->where($where)->sum('price');

        $price = '￥'.priceFormat($price);
        return $price;
    }
    /**
     * 获取不同类型的待提现总金额
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $where [查询条件]
     */
    public static function TypeUnDepositTotalPrice(){


//        $where=[
//            ['deposit_status','=',2]
//        ];
//        $total_price=self::DepositTotalPrice($where);

        //合作端
//        $where=[
//            ['deposit_status','=',2]
//            ,['source_type','=',1]
//        ];
//        $cooperate_price=self::DepositTotalPrice($where);
        $cooperate_price=Db::name('biz')->where('biz_status=1 and biz_type=3 and id not in(408,3,5)')->sum('account_balance');
        $cooperate_price=priceFormat($cooperate_price);
        //合伙人
//        $where=[
//            ['deposit_status','=',2]
//            ,['source_type','=',2]
//        ];
//        $partner_price=self::DepositTotalPrice($where);
        $partner_price=Db::name('member_partner')->where('status=1 and member_id != 25 and member_id != 9405')->sum('partner_balance');
        $partner_price=priceFormat($partner_price);
        //商家端
//        $where=[
//            ['deposit_status','=',2]
//            ,['source_type','=',3]
//        ];
//        $merchants_price=self::DepositTotalPrice($where);
        $merchants_price=Db::name('merchants')->where('status=1 and id>2')->sum('balance');
        $merchants_price=priceFormat($merchants_price);
         //总数，
        $total_price=$merchants_price+$partner_price+$cooperate_price;
        $total_price=priceFormat($total_price);
        return ['total_price'=>$total_price,'cooperate_price'=>$cooperate_price,'partner_price'=>$partner_price,'merchants_price'=>$merchants_price];
    }

    /**
     * 获取不同类型的待提现总金额
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $where [查询条件]
     */
    public static function TypeUnDepositList(){

        //合作店
        $biz_sql = Db::name('biz')->field('account_balance price,biz_title title,2 type,id')
            ->where('biz_status=1 and biz_type=3 and account_balance >0 and (id not in(408,3,5) )')
            ->buildSql();

        //合伙人
        $member_partner_sql = Db::name('member_partner')->alias('mp')
            ->field('mp.partner_balance price,m.nickname title,1 type,member_id id')
            ->leftJoin(['member'=>'m'],'m.id=mp.member_id')
            ->where('mp.status=1 and mp.partner_balance >0 and mp.member_id !=25 and mp.member_id !=9405')
            ->buildSql();

        //商家端
        $c = Db::name('merchants')->field('balance price,title,3 type, id')
            ->where('status=1 and balance >0  and (id >2)')
            ->union([$biz_sql, $member_partner_sql])
            ->buildSql();

        $list = Db::table($c . ' a')->order('price')->paginate(10, false, ['query' => request()->param()]);;

        $list=$list->toArray();
        foreach ($list['data'] as &$v)
        {
            $v['type_title']=BaseService::StatusHtml($v['type'],[1=>'合伙人',2=>'合作端',3=>'联盟商家'],false);
            $url_arr=[
                1=>'/admin/Member/detail?id='.$v['id'],
                2=>'/admin/biz/Bizwrite?biz_type=3&id='.$v['id'],
                3=>'/admin/JzMerchants/SaveData?id='.$v['id'],
            ];
            $v['url']=BaseService::StatusHtml($v['type'],$url_arr,false);
        }

         //总数，

         return   $list;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function DepositDataDealWith($data)
    {
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                # 支付方式
                if(isset($v['deposit_type']))
                {

                    $v['deposit_type_title']=BaseService::StatusHtml($v['deposit_type'],lang('cash_type'),false);
                }
                # 提现状态
                if(isset($v['deposit_status']))
                {
//                    if ($v['deposit_status'] ==1)
//                    {
//                        $v['deposit_status_title']='成功';
//                    }elseif ($v['deposit_status'] ==2){
//                        $v['deposit_status_title']='失败';
//                    }else{
//                        $v['deposit_status_title']='等待提现';
//                    }
                    $v['deposit_status_title']=BaseService::StatusHtml($v['deposit_status'],lang('deposit_status'),false);

                }
                # 提现金额
                if(isset($v['price']))
                {
                    $v['price']=priceFormat($v['price']) ;
                }

                #提现途径
                if(isset($v['source_type']))
                {
                    switch ($v['source_type'])
                    {
                        case 1:
                            $v['launch_cate_title']='合作门店';
                            break;

                        case 2:
                            $v['launch_cate_title']='合伙人';
                            break;

                        case 3:
                            $v['launch_cate_title']='商户提现';
                            break;

//                        default:
//                            $v['launch_cate_title']=BaseService::StatusHtml($v['launch_cate'],lang('launch_cate'),false);
//                            break;
                    }
                }
                $id_det = $v['biz_id'];
                if($v['source_type']==1){
                    if($v['launch_cate']==2){
                        $id_type = 3;
                    }else{
                        $id_type = 2;
                    }
                }elseif($v['source_type']==2){
                    $id_det = $v['member_id'];
                    $id_type = 1;
                }elseif($v['source_type']==3){
                    $id_type = 4;
                }
                $info = self::depositIdDetail($id_type,$id_det);
                $v["url"]=$info['url'];

            }
        }
        return $data;
    }
    /**
     * 费用列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月19日15:32:54
     * @param    [array]          $params [输入参数]
     */
    public static function ExpensesListWhere($params = [])
    {
        $where = [['id','>',0]];
//        dump($params);exit;
        //名称
        if(!empty($params['title']))
        {
            $where[] =['title', 'like', '%'.$params['title'].'%'];
        }
        //状态
        if(isset($params['category'])&&intval($params['category'])>0)
        {
            $where[] = ['category', '=', $params['category']];
        }
        //类型
        if(isset($params['type_id'])&&intval($params['type_id'])>0)
        {
            $where[] = ['type_id', '=', $params['type_id']];
        }

        return $where;
    }
    /**
     * 获取费用列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $params [输入参数]
     */
    public static function ExpensesList($params){
        $data=BaseService::DataList($params);

        return self::ExpensesDataDealWith($data);
    }

    /**
     * 费用保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月21日16:45:39
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ExpensesSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '标题不能为空',
                'error_code'         => 80002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '经手人不能为空',
                'error_code'         => 80003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'price',
                'error_msg'         => '金额不能为空',
                'error_code'         => 80004,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'explain',
                'error_msg'         => '渠道不能为空',
                'error_code'         => 80005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'type_id',
                'error_msg'         => '类型不能为空',
                'error_code'         => 80006,
            ],

        ];
        /*
        # 添加是判断标题是否重复
        if(empty($params['id'])){
            $p[]=   [
                'checked_type'      => 'unique',
                'checked_data'      => 'news',
                'key_name'          => 'news_title',
                'error_msg'         => '消息标题不能重复',
                'error_code'         => 30001,
            ];
        }
        */
        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=$params;

        //dump($data);exit;
        // 添加/编辑
        if(empty($params['id']))
        {
            $re=Db::name('expenses')->insertGetId($data);
            # 如果是收入费用 增加收入记录
            if($data['category']==1){
                $income_data = array("biz_id"=>0,"biz_income"=>$data['price'],"pay_type"=>$data['cash_type'],"income_type"=>17,"pid"=>$re,"order_number"=>"");
                addBizIncome($income_data);
            }
        } else {
            $data['update_time']=TIMESTAMP;
            $re=Db::name('expenses')->where(['id'=>intval($params['id'])])->update($data);

        }
        if (!$re)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>80007,'msg'=>'保存费用失败','status'=>false,'debug'=>false]);
        }

        return DataReturn('保存成功', 0);

    }
    /**
     * 费用数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年10月19日15:32:54
     * @desc    description
     * @param   [array]          $data [处理的数据]
     */
    public static function ExpensesDataDealWith($data){
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                # 费用状态
                if(isset($v['category']))
                {
                    $v['category_title']=($v['category'] ==1) ? '收入' : '支出';
                }
                # 类型
                if(isset($v['type_id']))
                {
                    $v['type_title']=Db::name('costtype')->where('id='.$v['type_id'])->value('type_title');
                }
                #提现途径
                if(isset($v['price']))
                {
                    $v['price']=priceFormat($v['price']);
                }
            }
        }
        return $data;
    }

    /**
     * 余额使用信息
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月5日16:58:32
     * @desc    description
     * @param   [array]          $params [参数]
     */
    public static function BizTrafficData($params){
        /*
        $where = self::BizTrafficWhere($params);
        $data=Db::name('biz_traffic')->alias('bt')
            ->leftJoin(['biz'=>'b'],'b.id=bt.biz_id')
            ->group('biz_id')
            ->where($where)
            ->field('b.biz_title,biz_address,biz_type,count(member_id) member_total,sum(amount) account,biz_id')
            ->paginate(10, false, ['query' => request()->param()]);
        $data=$data->toArray()['data'];
        foreach ($data as &$v)
        {
            $v['biz_type_title']='自营';
            if ($v['biz_type']==2)
            {
                $v['biz_type_title']='加盟';
            }
            if ($v['biz_type']==3)
            {
                $v['biz_type_title']='合作';
            }
            $v['account']=number_format($v['account'],2);
        }
        $total=Db::name('biz_traffic')->alias('bt')->leftJoin(['biz'=>'b'],'b.id=bt.biz_id')
            ->group('biz_id')
            ->where($where)
            ->count();
        */


        if ($params['time']=='today')
        {
            $start_time=date('Y-m-d 00:00:00');

            $end_time=date('Y-m-d 23:59:59',time());
        }

        if ($params['time']=='yesterday')
        {
            $start_time=date('Y-m-d 00:00:00', strtotime('-1 day'));

            $end_time=date('Y-m-d 23:59:59', strtotime('-1 day'));
        }

        $where = [
            ['consump_time', '>=',  $start_time],
            ['consump_time', '<=', $end_time],
            ['log_consump_type', '=',1],
            ['consump_pay_type','=',5]
        ];

        if (isset($params["param"]['keywords']))
        {
            $where[] =['m.member_name|m.member_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }

        if (isset($params['biz_id']))
        {
            $where[] =['lc.biz_id', '=', $params['biz_id']];
        }

        $a = Db::name('log_consump')->alias('lc')
            ->leftJoin(['member'=>'m'],'m.id=lc.member_id')
            ->field('member_id,consump_price,biz_id,consump_type')
            ->where($where)->buildSql();

        $where = [
            ['create_time', '>=',  $start_time],
            ['create_time', '<=', $end_time],
            ['cash_type', '=', 5],
        ];

        if (isset($params["param"]['keywords']))
        {
            $where[] =['m.member_name|m.member_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        if (isset($params['biz_id']))
        {
            $where[] =['mo.merchants_id', '=', $params['biz_id']];
        }
        $c = Db::name('merchants_order')->alias('mo')
            ->leftJoin(['member'=>'m'],'m.id=mo.member_id')
            ->field('member_id,actual_price consump_price,merchants_id biz_id,mo.member_phone consump_type')
            ->where($where)
            ->union([$a])->buildSql();

        $data = Db::table($c . ' a')->paginate(10, false, ['query' => request()->param()]);

        $data=$data->toArray();

        foreach ($data['data'] as &$v)
        {
            $v['member_name']=MemberService::SelectMemberName(['id'=>$v['member_id']]);

            $v['member_phone']=Db::name('member')->where(['id'=>$v['member_id']])->value('member_phone');

            if (strlen($v['consump_type'])>1) {
                $biz_info=Db::name('merchants')->where(['id'=>$v['biz_id']])->find();

                $v['biz_title']=$biz_info['title'];

                $v['biz_address']=$biz_info['province'].$biz_info['city'].$biz_info['area'].$biz_info['address'];
            }else{
                $biz_info=Db::name('biz')->where(['id'=>$v['biz_id']])->find();

                $v['biz_title']=$biz_info['biz_title'];

                $v['biz_type']=$biz_info['biz_type'];

                $v['biz_address']=$biz_info['biz_address'];
            }

            $v['account']=priceFormat($v["consump_price"]);
        }

        //,biz_address,biz_type,count(id) member_total,sum(amount)
        return DataReturn('ok',0,$data);
    }

    /**
     * 余额使用门店
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月5日16:58:32
     * @desc    description
     * @param   [array]          $params [参数]
     */
    public static function BizTrafficGroupData($params){
        /*
        $where = self::BizTrafficWhere($params);
        $data=Db::name('biz_traffic')->alias('bt')
            ->leftJoin(['biz'=>'b'],'b.id=bt.biz_id')
            ->group('biz_id')
            ->where($where)
            ->field('b.biz_title,biz_address,biz_type,count(member_id) member_total,sum(amount) account,biz_id')
            ->paginate(10, false, ['query' => request()->param()]);
        $data=$data->toArray()['data'];
        foreach ($data as &$v)
        {
            $v['biz_type_title']='自营';
            if ($v['biz_type']==2)
            {
                $v['biz_type_title']='加盟';
            }
            if ($v['biz_type']==3)
            {
                $v['biz_type_title']='合作';
            }
            $v['account']=number_format($v['account'],2);
        }
        $total=Db::name('biz_traffic')->alias('bt')->leftJoin(['biz'=>'b'],'b.id=bt.biz_id')
            ->group('biz_id')
            ->where($where)
            ->count();
        */


        if ($params['time']=='today')
        {
            $start_time=date('Y-m-d 00:00:00');
            $end_time=date('Y-m-d 23:59:59',time());
        }

        if ($params['time']=='yesterday')
        {
            $start_time=date('Y-m-d 00:00:00', strtotime('-1 day'));
            $end_time=date('Y-m-d 23:59:59', strtotime('-1 day'));
        }

        $where = [
            ['lc.consump_time', '>=',  $start_time],
            ['lc.consump_time', '<=', $end_time],
            ['lc.log_consump_type', '=',1],
            ['lc.consump_pay_type','=',5],
            ['lc.income_type','<>',20],
            ['lc.income_type','<>',21],
        ];


        $a = Db::name('log_consump')->alias('lc')
            ->leftJoin(['biz'=>'b'],'b.id=lc.biz_id')
            ->field('sum(consump_price) total_price,b.biz_title,lc.biz_id,b.biz_type,b.biz_phone,b.biz_img')
            ->group('lc.biz_id')
            ->where($where)->buildSql();

//            ->buildSql();
        $where = [
            ['mo.create_time', '>=',  $start_time],
            ['mo.create_time', '<=', $end_time],
            ['mo.cash_type', '=', 5],
        ];

        $c = Db::name('merchants_order')->alias('mo')
            ->leftJoin(['merchants'=>'m'],'m.id=mo.merchants_id')
            ->field('sum(mo.actual_price) total_price,title biz_title,mo.merchants_id biz_id,m.video biz_type,m.tel biz_phone,m.video biz_img')
            ->where($where)
            ->group('mo.merchants_id')
            ->union([$a])->buildSql();

        $data = Db::table($c . ' a')->paginate(10, false, ['query' => request()->param()]);

        $data=$data->toArray();


        foreach ($data['data'] as &$v)
        {

            if (strlen($v['biz_img'])>1) {//门店

               $v['biz_type_title']=BaseService::StatusHtml($v['biz_type'],lang('biz_type'),false);

               //详情连接
                $v['url']='/admin/biz/Bizwrite?id='.$v['biz_id'].'&biz_type='.$v['biz_type'];

            }else{//商户
                $v['biz_type_title']='商户';

                //详情连接
                $v['url']='/admin/JzMerchants/SaveData?status=1&id='.$v['biz_id'];
            }

            $v['total_price']=priceFormat($v["total_price"]);
        }

        //,biz_address,biz_type,count(id) member_total,sum(amount)
        return $data;
    }

    /**
     * 余额使用信息
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月5日16:58:32
     * @desc    description
     * @param   [array]          $params [参数]
     */
    private static function BizTrafficWhere($params){
        $where = [];
        //昨天时间
        $time_start=  date('Y-m-d 00:00:00', strtotime('-1 day'));
        $time_end=  date('Y-m-d 23:59:59', strtotime('-1 day'));
        //名称
        if(!empty($params['param']['keywords']))
        {
            $where[] =['b.biz_title', 'like', '%'.$params['param']['keywords'].'%'];
        }

        if($params['time']=='today')
        {
            $time_start=  date('Y-m-d 00:00:00');
            $time_end=  date('Y-m-d 23:59:59',time());
        }
        $where[] =['bt.create_time', '>=', $time_start];
        $where[] =['bt.create_time', '<=', $time_end];
        $where[] =['bt.pay_type', '=', 5];
        return $where;
    }

    /**
     * @param $params
     * @return array
     * @context 提现成功数据统计
     */
    static function depositStatic($params)
    {
        $where = "id > 0 and member_id != 25 ";
        $search = $params['param']['switchStatic'];
        if(!empty($params['param']['switchStatic'])){
            if($search==1){
                $where.=" and date(account_time)='".date("Y-m-d")."'";
            }elseif($search==2){
                $where.=" and date(account_time)='".date("Y-m-d",strtotime("-1 day"))."'";
            }elseif($search==3){
                $where.=" and date_format(account_time,'%Y-%m')='".date("Y-m")."'";
            }elseif($search==4){
                $where.=" and date_format(account_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
            }else{
                $start_time = $params['param']['start_time'];
                if(!empty($start_time)){
                    $where.=" and date(account_time)>='".$start_time."'";
                }
                $end_time = $params['param']['end_time'];
                if(!empty($start_time)){
                    $where.=" and date(account_time)<='".$end_time."'";
                }
            }
        }
        # 成功总金额
        $all = Db::table("log_deposit")->where(array("deposit_status"=>3))->where($where)->sum("price");
        # 推广员总金额
        $partner = Db::table("log_deposit")->where(array("deposit_status"=>3,"source_type"=>2))->where($where)->sum("price");
        # 合作店
        $biz = Db::table("log_deposit")->where(array("deposit_status"=>3,"source_type"=>1,"launch_cate"=>3))->where($where)->sum("price");
        # 商家
        $merchant = Db::table("log_deposit")->where(array("deposit_status"=>3,"source_type"=>3,"launch_cate"=>5))->where($where)->sum("price");

        return array("all"=>$all,"partner"=>$partner,"biz"=>$biz,"merchant"=>$merchant);
    }

    static function depositStaticDetail($param)
    {
        if($param['type'] == 2){
            $where = "ld.id >0 and ld.member_id != 25";
            if(!empty($param['search_type'])){
                if($param['search_type']==1){
                    $where .= " and ld.source_type=2 ";
                    if(!empty($param['search'])){
                        $info = Db::table("member")->field("id")->where("member_phone like '%".$param['search']."%' or member_name like '%".$param['search']."%'")->select();
                        if(!empty($info)){
                            $id_con = implode(',',array_column($info,'id'));
                            $where.=" and ld.member_id in (".$id_con.")";
                        }
                    }

                }elseif($param['search_type']==2){
                    $where .= " and ld.source_type=1 and ld.launch_cate=3 ";
                    $info = Db::table("biz")->field("id")->where("biz_type =3 and biz_status=1 and (biz_title like '%".$param['search']."%' or biz_phone like '%".$param['search']."%')")->select();
                    if(!empty($info)){
                        $id_con = implode(',',array_column($info,'id'));
                        $where.=" and ld.biz_id in (".$id_con.")";
                    }

                }elseif($param['search_type']==3){
                    $where .= " and ld.source_type=1 and ld.launch_cate=2 ";
                    $info = Db::table("biz")->field("id")->where("biz_type =2 and biz_status=1 and (biz_title like '%".$param['search']."%' or biz_phone like '%".$param['search']."%')")->select();
                    if(!empty($info)){
                        $id_con = implode(',',array_column($info,'id'));
                        $where.=" and ld.biz_id in (".$id_con.")";
                    }
                }elseif($param['search_type']==4){
                    $where .=" and ld.source_type=3 and ld.launch_cate=5 ";
                    $info = Db::table("merchants")->field("id")->where("status=1 and (title like '%".$param['search']."%' or contacts_phone like '%".$param['search']."%')")->select();
                    if(!empty($info)){
                        $id_con = implode(',',array_column($info,'id'));
                        $where.=" and ld.biz_id in (".$id_con.")";
                    }
                }
            }
            if(!empty($param['switchStatic'])){
                $search = $param['switchStatic'];
                if($search==1){
                    $where.=" and date(ld.account_time)='".date("Y-m-d")."'";
                }elseif($search==2){
                    $where.=" and date(ld.account_time)='".date("Y-m-d",strtotime("-1 day"))."'";
                }elseif($search==3){
                    $where.=" and date_format(ld.account_time,'%Y-%m')='".date("Y-m")."'";
                }elseif($search==4){
                    $where.=" and date_format(ld.account_time,'%Y-%m')='".date("Y-m",strtotime("-1 month"))."'";
                }else{
                    $start_time = $param['start_time'];
                    if(!empty($start_time)){
                        $where.=" and date(ld.account_time)>='".$start_time."'";
                    }
                    $end_time = $param['end_time'];
                    if(!empty($start_time)){
                        $where.=" and date(ld.account_time)<='".$end_time."'";
                    }
                }
            }
            # 提现成功的
            $list = Db::table("log_deposit ld")
                ->field("ld.*,convert(coalesce(price,0),decimal(10,2)) price,(select convert(coalesce(sum(price),0),decimal(10,2)) price from
                 log_deposit ldp where ldp.biz_id = ld.biz_id and ldp.source_type = ld.source_type
                  and ldp.launch_cate = ld.launch_cate and ldp.member_id = ld.member_id and ldp.deposit_status=3) account_suc_price")
                ->where(array("ld.deposit_status"=>3))->where($where)->order("account_time desc")->paginate(10, false, ['query' => request()->param()])
                ->toArray();
            if(!empty($list['data'])){
                foreach($list['data'] as $k=>$v){
                    $id_det = $v['biz_id'];
                    if($v['source_type']==1){
                        if($v['launch_cate']==2){
                            $id_type = 3;
                        }else{
                            $id_type = 2;
                        }
                    }elseif($v['source_type']==2){
                        $id_det = $v['member_id'];
                        $id_type = 1;
                    }elseif($v['source_type']==3){
                        $id_type = 4;
                    }
                    $info = self::depositIdDetail($id_type,$id_det);
                    $list['data'][$k]["title"] = $info['title'];
                    $list['data'][$k]["type_title"] = $info['type_title'];
                    $list['data'][$k]["url"] = $info['url'];
                }
            }
            return $list;
        }else{
            # 待提现的 查余额
            $where = null;
            if($param['search_type']==1){
                if(!empty($param['search'])){
                    $where.="m.member_phone like '%".$param['search']."%' or m.member_name like '%".$param['search']."%' and m.id != 25";
                }
                $list = Db::table("member_partner mp,member m")->field("mp.member_id,convert(mp.partner_balance,decimal(10,2)) price,m.member_name,m.nickname,
                    (select convert(coalesce(sum(price),0),decimal(10,2)) from log_deposit ldp
                     where ldp.source_type = 2 and ldp.member_id = mp.member_id and ldp.deposit_status=3) account_suc_price")
                    ->where("m.id = mp.member_id and mp.partner_balance > 0 and mp.status=1")
                    ->where($where)
                    ->paginate(10, false, ['query' => request()->param()])
                    ->toArray();
                if (!empty($list['data'])){
                    foreach($list['data'] as $k=>$v){
                        $list['data'][$k]["title"] = empty($v['member_name']) ? $v['nickname'] : $v['member_name'];;
                        $list['data'][$k]["type_title"] ="推广员";
                        $list['data'][$k]["url"]='/admin/Member/detail?id='.$v['member_id'];
                    }
                }
            }elseif($param['search_type']==2){
                if(!empty($param['search'])){
                    $where.="b.biz_title like '%".$param['search']."%' or b.biz_phone like '%".$param['search']."%'";
                }
                $list = Db::table("biz b")->field("b.id,b.biz_title,convert(b.account_balance,decimal(10,2)) price,(select convert(coalesce(sum(price),0),decimal(10,2)) from log_deposit ldp
                     where ldp.source_type = 1 and (ldp.launch_cate=3 or ldp.launch_cate=2)  and ldp.biz_id = b.id and ldp.deposit_status=3) account_suc_price")
                    ->where("b.biz_type=3 and b.biz_status=1 and b.account_balance >0")
                    ->where($where)
                    ->paginate(10, false, ['query' => request()->param()])
                    ->toArray();
                if (!empty($list['data'])){
                    foreach($list['data'] as $k=>$v){
                        $list['data'][$k]["title"] =$v['biz_title'];;
                        $list['data'][$k]["type_title"] ="合作店";
                        $list['data'][$k]["url"] ='/admin/biz/Bizwrite?biz_type=3&id='.$v['id'];
                    }
                }
            }elseif($param['search_type']==3){
                if(!empty($param['search'])){
                    $where.="b.biz_title like '%".$param['search']."%' or b.biz_phone like '%".$param['search']."%'";
                }
                $list = Db::table("biz b")->field("b.id,b.biz_title,convert(b.account_balance,decimal(10,2)) price,(select convert(coalesce(sum(price),0),decimal(10,2)) from log_deposit ldp
                     where ldp.source_type = 1 and ldp.launch_cate=2 and ldp.biz_id = b.id and ldp.deposit_status=3) account_suc_price")
                    ->where("b.biz_type=3 and b.biz_status=1 and b.account_balance >0")
                    ->where($where)
                    ->paginate(10, false, ['query' => request()->param()])
                    ->toArray();
                if (!empty($list['data'])){
                    foreach($list['data'] as $k=>$v){
                        $list['data'][$k]["title"] =$v['biz_title'];
                        $list['data'][$k]["type_title"] ="加盟店";
                        $list['data'][$k]["url"] ='/admin/biz/Bizwrite?biz_type=2&id='.$v['id'];
                    }
                }
            }elseif($param['search_type']==4){
                if(!empty($param['search'])){
                    $where.="m.title like '%".$param['search']."%' or m.contacts_phone like '%".$param['search']."%'";
                }
                $list = Db::table("merchants m")->field("m.title,m.id,m.is_type,convert(m.balance,decimal(10,2)) price,(select convert(coalesce(sum(price),0),decimal(10,2)) from log_deposit ldp
                     where ldp.source_type = 3 and ldp.launch_cate=5 and ldp.biz_id = m.id and ldp.deposit_status=3) account_suc_price")
                    ->where("m.balance >0")
                    ->where($where)
                    ->paginate(10, false, ['query' => request()->param()])
                    ->toArray();
                if (!empty($list['data'])){
                    foreach($list['data'] as $k=>$v){
                        $list['data'][$k]["title"] =$v['title'];;
                        $list['data'][$k]["type_title"] ="联盟商家";
                        $list['data'][$k]["url"] ='/admin/JzMerchants/SaveData?savemark=1&status=1&is_type='.$v['is_type'].'&id='.$v['id'];
                    }
                }
            }
        }
        return $list;

    }

    static function depositIdDetail($type,$id){
        switch ($type){
            case 1:
                # 推广员
                $info = Db::table("member")->field("member_name,nickname")->where(array("id"=>$id))->find();
                $title = empty($info['member_name']) ? $info['nickname'] : $info['member_name'];
                $type_title = "推广员";
                $url = '/admin/Member/detail?savemark=2&id='.$id;
                break;

            case 2:
                # 合作店
                $info = Db::table("biz")->field("biz_title")->where(array("id"=>$id))->find();
                $title = $info['biz_title'];
                $type_title = "合作店";
                $url ='/admin/biz/Bizwrite?savemark=2&biz_type=3&id='.$id;
                break;
            case 3:
                 # 加盟店
                $info = Db::table("biz")->field("biz_title")->where(array("id"=>$id))->find();
                $title = $info['biz_title'];
                $type_title = "加盟店";
                $url ='/admin/biz/Bizwrite?savemark=2&biz_type=2&id='.$id;
                break;
            case 4:
                # 商家
                $info = Db::table("merchants")->field("title,is_type")->where(array("id"=>$id))->find();
                $title = $info['title'];
                $type_title = "联盟商家";
                if(empty($info['is_type'])){
                    $info['is_type']=2;
                }
                $url = '/admin/JzMerchants/SaveData?savemark=2&status=1&is_type='.$info['is_type'].'&id='.$id;
                break;
            default:
                $title = "未知名称";
                $type_title = "未知类型";
                break;
        }
        return array("title"=>$title,"type_title"=>$type_title,"url"=>$url);
    }

}
