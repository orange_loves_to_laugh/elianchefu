<?php


namespace app\service;


use think\Db;

class RecommendAwardService
{
    static function  DataDealWith($data)
    {
        if(!empty($data)){
            $time = date("Y-m-d H:i:s");
            foreach($data as &$v){
                if($v['status']==2){
                    $v['open_status']="已下架";
                }else{
                    if($v['start_time']>$time){
                        $v['open_status']="待上架";
                    }elseif($v['end_time']<=$time){
                        $v['open_status']="已下架";
                    }else{
                        $v['open_status']="已开始";
                    }
                }
            }
        }
        return $data;
    }
}