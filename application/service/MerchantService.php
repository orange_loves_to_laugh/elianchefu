<?php


namespace app\service;

use AlibabaCloud\SDK\OSS\OSS\GetBucketTagsResponse\tagging\tagSet\tag;
use AlibabaCloud\SDK\Viapiutils\V20200401\Models\GetOssStsTokenResponse\data;
use app\businessapi\ApiService\MerchantsService;
use app\businessapi\ApiService\OrderService;
use app\businessapi\ApiService\RedpacketService;
use app\businessapi\controller\Redpacket;
use Redis\Redis;
use think\Db;
use think\facade\Session;

/**
 * 商户服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年11月12日10:05:31
 */
class MerchantService
{
    /**
     * 分类数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     */
    public static function CateDataDealWith($data){
        if(!empty($data))
        {
             //分类名称 顺数  级别 上级分类 分类类型
            foreach($data as &$v)
            {
                $v['pid_title']='--';
                if ($v['pid']>0)
                {
                    $v['pid_title']=Db::name('merchants_cate')->where('id='.$v['pid'])->value('title');
                }
                $v['level_title']='一级分类';
                if ($v['level']>1)
                {
                    $v['level_title']='二级分类';
                }

                if ($v['type']>0)
                {
                    $v['type_title']=BaseService::StatusHtml($v['type'],lang('merchants_cate_type'),false);
                }

            }


        }
        return $data;
    }

    /**
     * 分类保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function CateSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '分类名称不能为空',
                'error_code'         => 11001,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=$params;
        unset($data['thumb_img']);
        unset($data['old_imgurl']);
        //删除无用图片:单图
        if (!empty($params['icon']))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'merchants_cate'.$params['id'];
            ResourceService::delCacheItem($params['icon']);
        }

        // 添加/编辑
        Db::startTrans();
        if(empty($params['id']))
        {

            $id = Db::name('merchants_cate')->insertGetId($data);
        } else {


            if(Db::name('merchants_cate')->where(['id'=>intval($params['id'])])->update($data))
            {
                $id = $params['id'];
            }
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>11002,'msg'=>'保存分类失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        #删除 前端 分类缓存
        $_redis = new Redis();
        $_redis->hDel('merchants_cate2','all');
        $_redis->hDel('merchants_cate1','all');
        #删除商户端 获取分类名称 缓存
        /*$_redis = new Redis();
        $_redis->hDel("merchants",'merchantsInfo');*/

        return DataReturn('保存成功', 0);
    }
    /**
     * 删除分类
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DelCate($params)
    {
        //分类是否被商户占用
      /*  $id=Db::name('merchants')->where('id='.$params['id'])->value('id');
        if ($id>0)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>11004,'msg'=>'分类正在被占用','status'=>false,'debug'=>false]);
        }*/
        //分类是否被当做父分类
       /* $id=Db::name('merchants_cate')->where('pid='.$params['id'])->value('id');
        if ($id>0)
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>11004,'msg'=>'分类正在被占用','status'=>false,'debug'=>false]);
        }*/
        $params['table']='merchants_cate';
        $params['soft']=true;
        $params['soft_arr']=['status'=>2];
        $params['errorcode']=11003;
        $params['msg']='删除失败';

        BaseService::DelInfo($params);
        #删除 前端 分类缓存
        $_redis = new Redis();
        $_redis->hDel('merchants_cate2','all');
        $_redis->hDel('merchants_cate1','all');

        return DataReturn('ok',0);
    }

    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月16日11:01:09
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsListWhere($params = [])
    {
        $where = [['id','>',0],['is_type','<>',3]];




        //商户状态
        if(!empty($params['status']))
        {
            if($params['status'] == 1){
                $where[] =['status', '<>', 3];

            }else{
                $where[] =['status', '=',  $params['status']];

            }
//            $where[] =['status', '=', 1];
        }

        //关键字
        if(!empty($params['param']['keywords']))
        {
            $where[] =['title|contacts_phone', 'like', '%'.$params["param"]['keywords'].'%'];
        }
        //等级
        if(!empty($params['param']['level']))
        {
            $where[] =['level', '=',  $params["param"]['level']];
        }
        //类型
        if(!empty($params['param']['is_type']))
        {
            $where[] =['is_type', '=',  $params["param"]['is_type']];
        }
        //省
        if(!empty($params['param']['province']))
        {
            $where[] =['province', '=',  $params["param"]['province']];
        }
        //市
        if(!empty($params['param']['city']))
        {
            $where[] =['city', '=',  $params["param"]['city']];
        }
        //区
        if(!empty($params['param']['area']))
        {
            $where[] =['area', '=',  $params["param"]['area']];
        }
        //用户端展示类型
        if(!empty($params['param']['is_type']))
        {
            $where[] =['is_type', '=',  $params["param"]['is_type']];
        }
        // 是否已设置营业时间
        if(!empty($params['param']['setUpTime'])){
            if($params['param']['setUpTime']==1){
                $where[] = ['start_time','neq','not null'];
                $where[] = ['end_time','neq','not null'];
            }else{
                $where[] = ['start_time','null'];
                $where[] = ['end_time','null'];
            }
        }
        # 关联员工
        if(!empty($params['param']['employee_sal'])){
            $where[] = ['relate_employee','=',$params['param']['employee_sal']];
        }
        return $where;
    }


    /**
     * 商户数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     */
    public static function DataDealWith($data){
        if(!empty($data))
        {
            //dump($data);exit;
            foreach($data as &$v)
            {
                #根据商户id  查出 分类名称 数组 拼接显示
                $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id'=>$v['id']))->field('cate_pid,cate_id,id')->select();
                //分类
                $v['cate_title']='--';
                if ($cate_relation[0]['cate_pid']>0)
                {
                    if(isset($cate_relation[0]['cate_pid'])){
                        $v['cate_ptitle_0']=Db::name('merchants_cate')->where('id='.$cate_relation[0]['cate_pid'])->value('title');

                    }

                    if(isset($cate_relation[1]['cate_pid'])){
                        $v['cate_ptitle_1']='/'.Db::name('merchants_cate')->where('id='.$cate_relation[1]['cate_pid'])->value('title');

                    }
                    if(isset($cate_relation[2]['cate_pid'])){
                        $v['cate_ptitle_2']='/'.Db::name('merchants_cate')->where('id='.$cate_relation[2]['cate_pid'])->value('title');

                    }
                    if ($cate_relation[0]['cate_id']>0){
                        if(isset($cate_relation[0]['cate_id'])){
                            $v['cate_ptitle_0'].='-'.Db::name('merchants_cate')->where('id='.$cate_relation[0]['cate_id'])->value('title');

                        }

                        if(isset($cate_relation[1]['cate_id'])){
                            $v['cate_ptitle_1'].='-'.Db::name('merchants_cate')->where('id='.$cate_relation[1]['cate_id'])->value('title');


                        }
                        if(isset($cate_relation[2]['cate_id'])){
                            $v['cate_ptitle_2'].='-'.Db::name('merchants_cate')->where('id='.$cate_relation[2]['cate_id'])->value('title');

                        }

                    }




                    $v['cate_title'] =$v['cate_ptitle_0'].$v['cate_ptitle_1'].$v['cate_ptitle_2'];
                }
                //地区
                if (isset($v['province'])&&isset($v['city'])&&isset($v['area'])&&isset($v['address']))
                {
                    $v['total_area']=$v['province'].$v['city'].$v['area'].$v['address'];
                }

                //优质商家 支持余额付款 商家等级 商家状态
                //状态
                if(isset($v['status']))
                {
                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('open_close_status'),false);
                }
                //优质商家
                if(isset($v['is_good']))
                {

                    $v['good_title']=BaseService::StatusHtml($v['is_good'],lang('yes_no_status'),false);
                }
                //余额付款
                if(isset($v['balance_pay']))
                {

                    $v['balance_pay_title']=BaseService::StatusHtml($v['balance_pay'],lang('yes_no_status'),false);
                }
                //商家等级
                if(isset($v['level']))
                {
                    if ($v['level']<1) {
                        $v['level_title']='普通商家';
                    }else{
                        $v['level_title']=BaseService::StatusHtml($v['level'],[1=>'优质商家',2=>'推荐商家',3=>'诚信商家',4=>'普通商家'],false);
                    }
                    //是否是诚信商家
                    if ($v['level']<4) {
                        $v['is_title_status'] = '是';
                    }else{
                        $v['is_title_status'] = '否';

                    }

                }
                //商户类型
                if(isset($v['is_type'])) {
                    if ($v['is_type'] == 1) {
                        $v['is_type_title'] = '本地生活';

                    } else {
                        $v['is_type_title'] = '联盟商家';

                    }
                }




              //  dump($v);exit;
               // $v['level_title']='低级';
                //关联员工
                $v['relate_employee_title']='未关联员工';
                if(isset($v['relate_employee'])&&$v['relate_employee']>0)
                {

                    $v['relate_employee_title']=Db::name('employee_sal')->where('id='.$v['relate_employee'])->value('employee_name');
                }
                //百分比
                if(isset($v['commission'])){
                    $v['commission_bi'] = ($v['commission'] *100).'%';
                }

                //营业时间

                if(isset($v['start_time'])&&$v['end_time']>0)
                {

                    $v['work_time']=$v['start_time'].'--'.$v['end_time'];
                }
                //办理会员数量
                $v['handle_member']=Db::name('channel')->where('channel=6 and action_type=2 and pid='.$v['id'])->count();
                #剩余几天
                $time = date("Y-m-d");
                $expire_time=date_create($v['expire_time']);
                $v['expire_day'] = ((strtotime(date_format($expire_time,"Y-m-d")) - strtotime($time))/86400).'天';

                // 提现 到店 收入记录
                // 是否已经展示状态
                if(empty($v['start_time']) and empty($v['end_time'])){
                    $v['setTimeUp'] = 2;
                }else{
                    $v['setTimeUp'] = 1;
                }
                # 关联代理
                if($v['agent_id']>0){
                  $v['agent_name'] = Db::connect('ealspell')->table("community_agent")->where(array("id"=>$v['agent_id']))->value("name");
                }else{
                    $v['agent_name']='';
                }
                # 是否赠送积分
                if($v['giving_score']==1){
                    $v['giving_score_title'] = "是";
                }else{
                    $v['giving_score_title'] = "否";
                }
            }


        }
        return $data;
    }
    /**
     * 缓存资源处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [string]          $name [资源名称]
     * @param    [array]          $params [输入参数]
     */
    public static function HandleCache($type,$params){
        if (!empty($params[$type]))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'merchants'.$params['id'];
            ResourceService::delCacheItem($params[$type]);
        }

    }
    /**
     * 商户保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveMerchants($params = [])
    {


//        dump($params);exit;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '名称不能为空',
                'error_code'         => 12001,
            ],
          /*  [
                'checked_type'      => 'empty',
                'key_name'          => 'cate_pid',
                'error_msg'         => '一级分类不能为空',
                'error_code'         => 12002,
            ],*/
          /*  [
                'checked_type'      => 'empty',
                'key_name'          => 'cate_pid',
                'error_msg'         => '二级分类不能为空',
                'error_code'         => 12003,
            ],*/
            [
                'checked_type'      => 'empty',
                'key_name'          => 'contacts',
                'error_msg'         => '联系电话不能为空',
                'error_code'         => 12004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'thumb',
                'error_msg'         => '图片不能为空',
                'error_code'         => 12005,
            ],
             [
                 'checked_type'      => 'empty',
                 'key_name'          => 'license_imgurl',
                 'error_msg'         => '营业执照不能为空',
                 'error_code'         => 12005,
             ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'video',
//                'error_msg'         => '视频不能为空',
//                'error_code'         => 12006,
//            ],
//            [
//                'checked_type'      => 'empty',
//                'key_name'          => 'level',
//                'error_msg'         => '等级不能为空',
//                'error_code'         => 12007,
//            ],
            [
                'checked_type'      => 'min',
                'checked_data'      =>0,
                'key_name'          => 'commission',

                'error_msg'         => '抽成比例必须大于0',
                'error_code'         => 12008,
            ],
            [
                'checked_type'      => 'max',
                'checked_data'      =>1,
                'key_name'          => 'commission',

                'error_msg'         => '抽成比例必须小于1',
                'error_code'         => 12009,
            ],

        ];

        $ret = ParamsChecked($params, $p);


        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        #判断关键词 不能超过3个
        $keyWords = explode('，',$params['keywords']);

        if(!empty($keyWords) and count($keyWords)>3){
            $_ret = "最多可设置3个标签!,12001";
            $error_arr=explode(',',$_ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

        }

        $data=$params;
        unset($data['thumb_img']);
        unset($data['old_imgurl']);//
        unset($data['old_imgurl1']);
        unset($data['old_imgurl_collections_bar']);
        unset($data['old_license_imgurl']);
        //dump($params);exit;
        self::HandleCache('thumb',$params);
        self::HandleCache('license_imgurl',$params);
        self::HandleCache('collections_bar',$params);
        self::HandleCache('video',$params);

        /*
        //删除无用图片:单图
        if (!empty($params['thumb']))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'merchants'.$params['id'];
            ResourceService::delCacheItem($params['thumb']);
        }
        //删除无用图片:付款码
        if (!empty($params['collections_bar']))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'merchants'.$params['id'];
            ResourceService::delCacheItem($params['collections_bar']);
        }

        //删除无用视频
        if (!empty($params['video']))
        {
            $params['id']=empty($params['id'])?'':$params['id'];
            ResourceService::$session_suffix='source_upload'.'merchants'.$params['id'];
            ResourceService::delCacheItem($params['video']);
        }
        */
        unset($data['cate_pid'],$data['cate_id']);
        #把 详情图数组 变成*号拆分数组
        if(isset($params['remarks'])){
            $remarks = explode(',',$params['remarks']);
            #*号拼接成字符串
            $data['remarks'] = implode('*',$remarks);
        }
        if(empty($data['start_time'])){
            unset($data['start_time']);

        }
        if(empty($data['end_time'])){
            unset($data['end_time']);

        }

        # 上传短视频
        $open_toutiao_expiration_start = $data['open_toutiao_expiration_start'];
        $open_toutiao_expiration = $data['open_toutiao_expiration'];
        $open_expiration_time = $data['open_expiration_time'];
        unset( $data['open_toutiao_expiration_start'],$data['open_toutiao_expiration']);
        if(empty($open_expiration_time)){
            unset($data['open_expiration_time']);
        }
        # 开启拍视频
        $take_video_expiration_start = $data['take_video_expiration_start'];
        $take_video_expiration= $data['take_video_expiration'];
        $take_expiration_time = $data['take_expiration_time'];
        unset( $data['take_video_expiration_start'],$data['take_video_expiration']);
        if(empty($take_expiration_time)){
            unset($data['take_expiration_time']);
        }
        # 开启霸屏
        $bully_screen_expiration_start = $data['bully_screen_expiration_start'];
        $bully_screen_expiration= $data['bully_screen_expiration'];
        $bully_expiration_time = $data['bully_expiration_time'];
        unset( $data['bully_screen_expiration_start'],$data['bully_screen_expiration']);
        if(empty($bully_expiration_time)){
            unset($data['bully_expiration_time']);
        }
        // 添加/编辑
        Db::startTrans();
        if(empty($params['id']))
        {
            #判断手机号不能 重复
           $merchantsId =  Db::table('merchants')->where(array('contacts_phone'=>$params['contacts_phone'],'status'=>1))->value('id');
           if(!empty($merchantsId)){
               $_ret = "联系人手机号不能重复!,12001";
               $error_arr=explode(',',$_ret);
               throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

           }
            $merchantsTitle =  Db::table('merchants')->where(array('title'=>$params['title'],'status'=>1))->value('title');
            if(!empty($merchantsTitle)){
                $_ret = "商户名称不能重复!,12001";
                $error_arr=explode(',',$_ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

            }



            $id = Db::name('merchants')->insertGetId($data);
            if($id){
                if(!empty($data['relate_employee'])){
                    #关联员工表
                    Db::table('assign_staff')->insertGetId(array(
                        'biz_id'=>$id,
                        'employee_id'=>$data['relate_employee'],
                        'type'=>2,
                    ));
                }

                #创建分类关系
                if($params['is_type'] == 1){
                    #本地生活
                    #存分类
                    if(!empty($params['cate_pid'])){
                        foreach($params['cate_pid'] as $k=>$v){
                            #存  merchants_cate_relation
                            Db::table('merchants_cate_relation')->insert(array('merchants_id'=>$id,'cate_pid'=>$v,'cate_id'=>0));

                        }
                    }
                }else{
                    #联盟商家
                    $_session = Session('getCateSession');
                    if(!empty($_session)){
                        foreach($_session as $k=>$v){
                            #存  merchants_cate_relation
                            Db::table('merchants_cate_relation')->insert(array('merchants_id'=>$id,'cate_pid'=>$v['cate_pid'],'cate_id'=>$v['cate_id']));

                        }
                    }

                }
            }




        } else {
            #判断手机号不能 重复
            $merchantsId =  Db::table('merchants')->where("contacts_phone = '".$params['contacts_phone']."' and id !='".$params['id']."'and status = 1")->value('id');
            if(!empty($merchantsId)){
                $_ret = "联系人手机号不能重复!,12001";
                $error_arr=explode(',',$_ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

            }
            $merchantsTitle =  Db::table('merchants')->where("title = '".$params['title']."' and id !='".$params['id']."' and status = 1")->value('title');
            if(!empty($merchantsTitle)){
                $_ret = "商户名称不能重复!,12001";
                $error_arr=explode(',',$_ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);

            }
            # 查询是否是审核状态到上架状态
            $shangjia =  Db::table('merchants')->field("status,agent_id")->where("id !='".$params['id']."'")->find();
            if(!empty($shangjia) and $shangjia['status']==3 and $params['status']==1 and $shangjia['agent_id']>0){
                AgentTask::merchantsAddTask($shangjia['agent_id']);
            }
            if (empty( $data['address']))
            {
                unset($data['address']);
            }
            if (empty( $data['lon']))
            {
                unset($data['lon']);
            }
            if (empty( $data['lat']))
            {
                unset($data['lat']);
            }
            $id = $params['id'];
            if(Db::name('merchants')->where(['id'=>intval($params['id'])])->update($data))
            {
                $id = $params['id'];
            }
            #修改关联员工 先查 是否有关联数据  assign_staff
                if(!empty($data['relate_employee'])){

                    $assign_staff = Db::table('assign_staff')->where(array('biz_id'=>$params['id'],'type'=>2))->value('id');
                    if(empty($assign_staff)){
                        #关联员工表
                        Db::table('assign_staff')->insertGetId(array(
                            'biz_id'=>$params['id'],
                            'employee_id'=>$data['relate_employee'],
                            'type'=>2,
                        ));
                    }else{
//                        dump($data['relate_employee']);
                        #修改关联员工
                       $b = Db::table('assign_staff')->where(array('biz_id'=>$params['id'],'type'=>2))->update(array(
                            'employee_id'=>$data['relate_employee']
                        ));
//                       dump($b);die;
                    }

                }else{
                    #删除关联的员工
                    $a = Db::table('assign_staff')->where(array('biz_id'=>$params['id']))->delete();

                }


            #修改关联分类
            if($params['is_type'] == 1){
                #本地生活
                #查询 merchants_cate_relation 的分类  和 传过来的 作对比  不相同 修改
                $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id'=>$params['id']))->field('cate_pid,id')->select();
                if(!empty($cate_relation)){
                    if($cate_relation[0]['cate_pid'] != $params['cate_pid'][0]){
                        Db::table('merchants_cate_relation')->where(array('id'=>$cate_relation[0]['id']))->update(array('cate_pid'=>$params['cate_pid'][0]));
                    }
                    if($cate_relation[1]['cate_pid'] != $params['cate_pid'][1]){
                        Db::table('merchants_cate_relation')->where(array('id'=>$cate_relation[1]['id']))->update(array('cate_pid'=>$params['cate_pid'][1]));
                    }
                    if($cate_relation[2]['cate_pid'] != $params['cate_pid'][2]){
                        Db::table('merchants_cate_relation')->where(array('id'=>$cate_relation[2]['id']))->update(array('cate_pid'=>$params['cate_pid'][2]));
                    }
                }else{
                    #没有 则添加
                    foreach($params['cate_pid'] as $k=>$v){
                        #存  merchants_cate_relation
                        Db::table('merchants_cate_relation')->insert(array('merchants_id'=>$params['id'],'cate_pid'=>$v,'cate_id'=>0));

                    }
                }

            }else{
                #联盟商家
                $_session = Session('getCateSession');
                if(!empty($_session)){
                    foreach($_session as $k=>$v){
                        #存  merchants_cate_relation
                        Db::table('merchants_cate_relation')->insert(array('merchants_id'=>$params['id'],'cate_pid'=>$v['cate_pid'],'cate_id'=>$v['cate_id']));

                    }
                }
            }
        }

        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>12009,'msg'=>'保存商户失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        ResourceService::$session_suffix='source_upload'.'merchants'.$params['id'];
        #删除图片
        if(isset($params['thumb'])){
            ResourceService::delCacheItem($params['thumb']);
        }
        if(isset($params['remarks'])){
            $params['remarks']=BaseService::HandleImg($params['remarks']);
            ResourceService::delCacheItem($params['remarks']);
        }
        if(isset($params['video'])){
            ResourceService::delCacheItem($params['video']);
        }
        # 判断视频功能是否需要加记录
        $log_data= array();
        if($data['open_toutiao']==1){
            # 查询记录是否需要更新
            $log_info = Db::table("log_merchants_expiration")->where(array("merchants_id"=>$id,"type"=>1))->order("id desc")->find();
            if(empty($log_info) or (!empty($log_info) and $log_info['expiration_time']!=$open_expiration_time)){
                array_push($log_data,array_filter(array("type"=>1,"merchants_id"=>$id,"expiration_start"=>$open_toutiao_expiration_start,
                    "duration_time"=>$open_toutiao_expiration,"expiration_time"=>$open_expiration_time)));
            }
        }
        if($data['take_video']==1){
            # 查询记录是否需要更新
            $log_info = Db::table("log_merchants_expiration")->where(array("merchants_id"=>$id,"type"=>2))->order("id desc")->find();
            if(empty($log_info) or (!empty($log_info) and $log_info['expiration_time']!=$take_expiration_time)){
                array_push($log_data,array_filter(array("type"=>2,"merchants_id"=>$id,"expiration_start"=>$take_video_expiration_start,
                    "duration_time"=>$take_video_expiration,"expiration_time"=>$take_expiration_time)));
            }
        }
        if($data['bully_screen']==1){
            # 查询记录是否需要更新
            $log_info = Db::table("log_merchants_expiration")->where(array("merchants_id"=>$id,"type"=>3))->order("id desc")->find();
            if(empty($log_info) or (!empty($log_info) and $log_info['expiration_time']!=$bully_expiration_time)){
                array_push($log_data,array_filter(array("type"=>3,"merchants_id"=>$id,"expiration_start"=>$bully_screen_expiration_start,
                    "duration_time"=>$bully_screen_expiration,"expiration_time"=>$bully_expiration_time)));
            }
        }
        if(!empty($log_data)){
            Db::table("log_merchants_expiration")->insertAll($log_data);
        }
        $_redis = new Redis();
        $_redis->hDel("merchants",'merchantsInfo');

        return DataReturn('保存成功', 0);
    }

    /**
     * 商户收入记录
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsIncomeListWhere($params)
    {
        $where = [];


        //商户id
        if(!empty($params['merchants_id']))
        {
            $where[] =['merchants_id', '=',  $params['merchants_id']];
        }

        //商户id
        if(!empty($params['member_id']))
        {
            $where[] =['member_id', '=',  $params['member_id']];
        }

        return $where;
    }
    /**
     * 商户收入记录
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日10:59:51
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsIncomeList($params)
    {
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as $k=>$v)
            {
                //支付金额
                $data[$k]['price'] = Db::table('merchants_order')->where(array('order_number'=>$v['order_number']))->value('actual_price');
                //时间 金额 支付方式
                if ($v['merchants_id']>0&&$v['merchants_id']>0)
                {
                    $data[$k]['merchants_title']=Db::name('merchants')->where('id='.$v['merchants_id'])->value('title');
                }


                //支付方式
                if(isset($v['pay_type']))
                {

                    $data[$k]['pay_type_title']=BaseService::StatusHtml($v['pay_type'],lang('pay_type'),false);
                }
                //用户类型 用户状态 用户
                if ($v['member_id']>0)
                {
                    $data[$k]['nickname']=Db::name('member')->where('id='.$v['member_id'])->value('member_name');
                    $data[$k]['phone']=Db::name('member')->where('id='.$v['member_id'])->value('member_phone');
                    if(empty($data[$k]['nickname'])){
                        $data[$k]['nickname']=Db::name('member')->where('id='.$v['member_id'])->value('nickname');

                    }
                    $info=Db::name('member_mapping')->alias('mp')
                        ->leftJoin(['member_level'=>'ml'],'mp.member_level_id=ml.id')
                        ->where('member_id='.$v['member_id'])
                        ->field('level_title,mp.member_level_id')
                        ->find();
                    $data[$k]=array_merge($info,$data[$k]);
                    $data[$k]['member_status_title']='非会员';
                    if ($info['member_level_id']>0)
                    {
                        $data[$k]['member_status_title']='会员';
                    }
                }else{
                    unset($data[$k]);

                }
            }
        }
        return $data;
    }
    /**
     * 商户弹窗修改
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function SaveMerchantsPopup($params = [])
    {
        // 编辑
       // dump($params);exit;
        $data=json_encode($params);
        Db::startTrans();


        if(Db::name('sysset')->where(['tagname'=>'merchants_popup'])->update(['desc'=>$data]))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>12010,'msg'=>'弹窗修改失败','status'=>false,'debug'=>false]);
        }
        Db::commit();

        return DataReturn('保存成功', 0);
    }
    /**
     * 商户收款记录
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsCollectionsList($params){
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                #根据订单号查询 订单所有信息  类型 8 9  则自定义
                if($v['pay_type'] >8){
                    $v['member_name'] = "系统奖励";
                    $v['member_phone'] = "--";
                    $v['total_price'] = priceFormat($v['price']);
                    $v['actual_price'] = priceFormat($v['price']);
                    $v['voucher_price'] = priceFormat(0);
                    $v['commission_price'] = priceFormat(0);
                    $v['checkout_price'] = priceFormat($v['price']);

                    $v['cash_type_title'] = "平台奖励";
                    if($v['pay_type'] >9){
                        $v['cash_type_title'] = "推荐会员";

                    }

                    if($v['pay_type'] == 8){
                        $v['pay_type_title'] = "平台奖励";
                    }else if($v['pay_type'] == 9){
                        $v['pay_type_title'] = "任务获得";
                    }else if($v['pay_type'] == 10){
                        $v['pay_type_title'] = "会员推荐收入";
                    }else if($v['pay_type'] == 11){
                        $v['pay_type_title'] = "会员充值提成";
                    }else{
                        $v['pay_type_title'] = "会员升级提成";
                    }
                }else{
                    if(!empty($v['order_number'])){
                        $order = Db::table('merchants_order')->where(array('order_number'=>$v['order_number']))->find();


                        // ，付款人名称，手机号，
                        $v['member_name']=Db::name('member')->where('id='.$v['member_id'])->value('member_name');
                        if(empty($v['member_name'])){
                            $v['member_name']=Db::name('member')->where('id='.$v['member_id'])->value('nickname');
                            if(empty($v['member_name'])){
                                $v['member_name'] = '--';
                            }
                        }
                        //手机号
                        $v['member_phone'] = $order['member_phone'];
                        if (empty($order['member_phone']))
                        {
                            $v['member_phone']=Db::name('member')->where('id='.$v['member_id'])->value('member_phone');
                            if(empty($v['member_phone'])){
                                $v['member_phone'] = '--';
                            }
                        }

                        //平台抽成 结算金额
                        if (isset($order['commission_price']))
                        {
                            $v['commission_price']=priceFormat($order['commission_price']);
                            $v['checkout_price']=priceFormat($order['actual_price']-$order['commission_price']);
                        }
                        //需支付
                        if (isset($order['total_price']))
                        {
                            $v['total_price']=priceFormat($order['total_price']);
                        }
                        //实际支付
                        if (isset($order['actual_price']))
                        {
                            $v['actual_price']=priceFormat($order['actual_price']);
                        }
                        //卡卷低值
                        $v['voucher_price']=priceFormat(0);
                        if ($order['actual_price']!=$order['total_price'])
                        {

                            $v['voucher_price'] =priceFormat(Db::name('merchants_voucher_order')->where([['order_id','=',$order['id']]])->sum('price')) ;
                        }

                        //付款方式
                        if(isset($order['cash_type']))
                        {
                            $v['cash_type_title']=BaseService::StatusHtml($order['cash_type'],lang('cash_type'),false);
                        }
                        //支付类型
                        if(isset($order['pay_type']))
                        {

                            $v['pay_type_title']=BaseService::StatusHtml($order['pay_type'],lang('merchants_pay_type'),false);
                        }
                    }


                }

            }
        }
        return $data;
    }

    /**
     * 商户优惠券
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsVoucherList($params){
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                // 有效期
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['work_time']=$v['start_time'].'-'.$v['end_time'];
                }

                // 状态
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['status']=3;
                    if ($v['start_time']>TIMESTAMP)
                    {
                      $v['status']=1;
                    }

                    if ($v['end_time']< TIMESTAMP)
                    {
                        $v['status']=2;
                    }
                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('sell_status'),false);
                }
            }
        }
        return $data;
    }
    /**
     * 商户推荐会员记录
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsCardlogList($params){
        $data=BaseService::DataList($params);
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                $where = "account_status = 1 and id = '".$v['member_id']."'";
                #查询会员信息
                $data_params = array(
                    'page'         => true,
                    'number'         => 10,
                    'table'     =>'member',
                    'where'     => $where,
                    'field'     =>'*'
                );
                $_res= BaseService::DataList($data_params);
                $v['member_name'] = $_res[0]['member_name'];
                if(empty($v['member_name'])){
                    $v['member_name'] = $_res[0]['nickname'];

                }
                $v['member_phone'] = $_res[0]['member_phone'];
                $v['member_id'] = $_res[0]['id'];
                #会员等级名称
            /*    $v['current_level'] = Db::table('member_level')->where(array('id'=>$v['current_level']))->value('level_title');
                if($v['current_level'] == 0){
                    $v['current_level'] = '普通会员';
                }*/
                $v['card_level'] = Db::table('member_level')->where(array('id'=>$v['level_id']))->value('level_title');
                $v['member_balance'] = Db::table("member_mapping")->where(array("member_id"=>$v['member_id']))->value("convert(member_balance,decimal(10,2))");

            }
        }
        return $data;
    }
    /**
     * 商户推荐商品
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsProductList($params){
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                // 有效期
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['work_time']=$v['start_time'].'-'.$v['end_time'];
                }

                // 状态
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['status']=3;
                    if ($v['start_time']>TIMESTAMP)
                    {
                        $v['status']=1;
                    }

                    if ($v['end_time']< TIMESTAMP)
                    {
                        $v['status']=2;
                    }
                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('sell_status'),false);
                }
            }
        }
        return $data;
    }

    /**
     * 商户推荐商品
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsServiceList($params){
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                // 有效期
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['work_time']=$v['start_time'].'-'.$v['end_time'];
                }

                // 状态
                if (isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['status']=3;
                    if ($v['start_time']>TIMESTAMP)
                    {
                        $v['status']=1;
                    }

                    if ($v['end_time']< TIMESTAMP)
                    {
                        $v['status']=2;
                    }
                    $v['status_title']=BaseService::StatusHtml($v['status'],lang('sell_status'),false);
                }
            }
        }
        return $data;
    }

    /**
     * 商户员工列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月27日10:37:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */

    public static function MerchantsStaffList($params){
        $data=BaseService::DataList($params);
        //dump($data);exit;
        if(!empty($data))
        {
            foreach($data as &$v)
            {
                // 员工权限
                $v['staff_power_title']='无权限';
                if (!empty($v['staff_power']))
                {
                    #拆分成数组
                    $staff_power = explode(',',$v['staff_power']);
                    if(!empty($staff_power)){
                        foreach ($staff_power as $sk=>$sv){
                            $staff_power[$sk] = BaseService::StatusHtml($sv,lang('merchants_staff_power'),false);

                        }

                    }



                }


                $v['staff_power_title'] = implode(',',$staff_power);
            }
        }
        return $data;
    }

    /**
     * 商户评论列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsCommentListWhere($params = [])
    {


        $where = [];

        //时间段
        if(!empty($params['param']['create_time']))
        {
            $time_start=date('Y-m-d 00:00:00',strtotime($params["param"]['create_time']));
            $time_end=date('Y-m-d 23:59:59',strtotime($params["param"]['create_time']));
            $where[] =['me.create_time', '>',  $time_start ];
            $where[] =['me.create_time', '<',  $time_end];
        }
        //dump($params);exit;
        //merchants_id
        if(!empty($params['merchants_id']))
        {
            $where[] =['mo.merchants_id', '=',  $params['merchants_id'] ];
        }
        //付款方式
        if(!empty($params['param']['cash_type']))
        {
            $where[] =['mo.cash_type', '=',  $params['param']['cash_type'] ];
        }

        //评论等级
        if(!empty($params['param']['evalua_level']))
        {
            //好评
            if ($params['param']['evalua_level']==1)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '>',  3 ];
            }
            //中评
            if ($params['param']['evalua_level']==2)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '=',  3 ];
            }
            //查评
            if ($params['param']['evalua_level']==3)
            {
                $where[] =['me.default_mark', '=', 2 ];
                $where[] =['me.evalua_level', '<',  3 ];
            }
            //未评论
            if ($params['param']['evalua_level']==4)
            {
                $where[] =['me.default_mark', '=', 1 ];
                $where[] =['me.evalua_level', '=',  $params['param']['income_type'] ];
            }
        }

        return $where;
    }
    /**
     * 商户评论列表
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsCommentDataList($params = [])
    {
        $where = empty($params['where']) ? [['id','>',0]] : $params['where'];
        $page = $params['page'] ? true : false;
        $number = isset($params['number']) ? intval($params['number']) : 10;
        $order = isset($params['order']) ? $params['order'] : 'me.create_time desc';

        $field = empty($params['field']) ? 'me.*' : $params['field'];
        $m = isset($params['m']) ? intval($params['m']) : 0;
        $n = isset($params['n']) ? intval($params['n']) : 0;

        $data = Db::name('merchants_evaluation')->alias('me')->leftJoin(['merchants_order'=>'mo'],'me.merchants_order_id=mo.id')->where($where)->where(array('me.default_mark'=>2))->field($field)->order($order);

        //分页
        if($page)
        {
            $data=$data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        }else{
            if($n){
                $data=$data->limit($m, $n)->select();
            }else{
                $data=$data->select();

            }
        }
        return self::MerchantsCommentDealWith($data);
    }

    /**
     * 商户数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月12日10:06:12
     * @param    [array]          $data [输入参数]
     */
    public static function  MerchantsCommentDealWith($data){
        if(!empty($data))
        {

            foreach($data as &$v)
            {

                //付款方式
                if(isset($v['cash_type']))
                {

                    $v['cash_type_title']=BaseService::StatusHtml($v['cash_type'],lang('cash_type'),false);
                }
                //evalua_level_title 评论状态
                if(isset($v['default_mark']))
                {
                    if ($v['default_mark']==1) {
                        $v['evalua_level_title']='用户未评论';
                    }
                }

                //evalua_level_title 评论状态
                if(isset($v['evalua_level']))
                {
                    if ($v['evalua_level']==3)
                    {
                        $v['evalua_level_title']='中评';
                    }
                    if ($v['evalua_level']>3)
                    {
                        $v['evalua_level_title']='好评';
                    }

                    if ($v['evalua_level']<3)
                    {
                        $v['evalua_level_title']='差评';
                    }

                }


            }


        }
        return $data;
    }
    /**
     * 商户评论总数
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2021年1月5日09:50:44
     * @param    [array]          $params [输入参数]
     */
    public static function MerchantsCommentDataTotal($where){
       return (int)  Db::name('merchants_evaluation')->alias('me')
            ->leftJoin(['merchants_order'=>'mo'],'me.merchants_order_id=mo.id')
            ->where($where)->count();
    }

    /**
     * 获取商户登录信息
     * @version 1.0.0
     * @date    2020-12-21
     * @desc    description
     */
    public static function LoginMerchantInfo($params)
    {



        // 用户数据处理
        $user = null;

        if(!empty($params['token']))
        {

            $user = cache($params['token']);

            if($user !== null && isset($user['id']))
            {
                return $user;
            }

            // 数据库校验
            return self::AppMerchantInfoHandle(null, 'token', $params['token']);
        }

        return $user;
    }
    /**
     * app用户信息
     * @version 1.0.0
     * @date    2020-12-21
     * @desc    description
     * @param   [int]             $member_id          [指定用户id]
     * @param   [string]          $where_field      [字段名称]
     * @param   [string]          $where_value      [字段值]
     * @param   [array]           $user             [用户信息]
     */
    static function AppMerchantInfoHandle($member_id = null, $where_field = null, $where_value = null, $member = [])
    {
        // 获取用户信息

        $field = '*';
        if(!empty($member_id))
        {
            $merchants = self::MerchantInfo('id', $member_id, $field);

        } elseif(!empty($where_field) && !empty($where_value) && empty($member))
        {
            $merchants = self::MerchantInfo($where_field, $where_value, $field);
        }

        if(!empty($merchants))
        {
            // 用户信息处理
            // token生成并存储缓存
            if(!empty($merchants['contacts_phone']))
            {
                $merchants['token'] = config('bussinessapi.cache_merchant_info').$merchants['contacts_phone'];
                //md5(md5($member['id'].time()).rand(100, 1000000));
                //cache(config('leebridge.cache_user_info').$member['token'], $member);
                cache($merchants['token'], $merchants);


            } else {
                $merchants['token'] = '';
            }


            // 用户登录纪录处理
//            self::VerifyLogMember($merchants['id'], true);
        }

        return $merchants['token'];
    }
    /**
     * 根据字段获取用户信息
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2019-01-25
     * @desc    description
     * @param   [string]          $where_field      [字段名称]
     * @param   [string]          $where_value      [字段值]
     * @param   [string]          $field            [指定字段]
     */
    public static function MerchantInfo($where_field, $where_value, $field = '*',$mark ='')
    {
        if(empty($where_field) || empty($where_value))
        {
            return '';
        }
        if($mark = 'Login'){
            return Db::name('merchants')->where([$where_field=>$where_value])->where(array('status'=>1))->field($field)->find();

        }else{
            return Db::name('merchants')->where([$where_field=>$where_value])->field($field)->find();

        }

    }
    /*
     * @contetn 任务处理详情
     * @return  array
     * */
    static function DataDealWithTask($params){
        if(!empty($params)){
            foreach ($params as $k=>$v) {

                #是否循环
                if(isset($v['is_loop'])){
                    if($v['is_loop'] == 1){
                        $params[$k]['is_loop_title'] = '是';
                    }else{
                        $params[$k]['is_loop_title'] = '否';

                    }
                }
                #任务状态
                $time = date("Y-m-d H:i:s");
                if ($time < $v['start_time'] and $time < $v['end_time']) {
                    #当前时间 小于 开始时间 和结束时间
                    $params[$k]['task_status_title'] = '待上架';

                } else if ($time >= $v['start_time'] and $time <= $v['end_time']) {
                    #当前时间 大于 等于开始 下雨等于 结束
                    $params[$k]['task_status_title'] = '进行中';


                } else if ($time > $v['start_time'] and $time > $v['end_time']) {
                    #当前时间 大于 开始时间 和结束时间
                    $params[$k]['task_status_title'] = '已结束';
                }

                #任务类型
                if(isset($v['task_type'])){

                    $params[$k]['task_type_title'] = BaseService::StatusHtml($v['task_type'],lang('task_type'),false);;

                }
            }

            return $params;
        }

    }
    /*
     * @contetn 红包处理详情
     * @return  array
     * */
    static function DataDealWithReadpacket($params){
        $time = date("Y-m-d H:i:s");
        if(!empty($params)){
            foreach ($params as $k=>$v) {

                #红包状态  待发放 发放中 已结束
                if(isset($v['start_time'])){
                    if($time < $v['start_time'] and $time<$v['end_time']){
                        #当前时间 小于 开始时间 和结束时间
                        $params[$k]['packet_status'] = '待发放';

                    }else if($time >= $v['start_time'] and $time<=$v['end_time']){
                        #当前时间 大于 等于开始 下雨等于 结束
                        $params[$k]['packet_status'] = '发放中';

                    }else if($time > $v['start_time'] and $time>$v['end_time']){
                        #当前时间 大于 开始时间 和结束时间
                        $params[$k]['packet_status'] = '已结束';

                    }
                }

                #总金额比列 百分比
                if(isset($v['price_bili'])){
                    $params[$k]['bili'] = $v['price_bili']*'100'.'%';

                }
                #剩余天数 结束时间 - 当前时间
                $startdate = date("Y-m-d");
                $date=date_create($v['end_time']);
                $enddate  =date_format($date,"Y-m-d");

                $params[$k]['day']=round((strtotime($enddate)-strtotime($startdate))/3600/24) ;

            }
            return $params;
        }

    }

    /*
     * @content 红包创建 修改
     * @return array
     * */
    static function SaveMerchantsRedpacket($params){
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '红包名称不能为空',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '请填写红包开始时间',
                'error_code'         => 30002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'end_time',
                'error_msg'         => '请填写红包结束时间',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'price_bili',
                'error_msg'         => '请填写投放比例',
                'error_code'         => 30002,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'num',
                'error_msg'         => '请填写投放数量',
                'error_code'         => 30002,
            ],



        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }
        #把时间转换成需要的格式 时间戳
        $start_time = date_create($params['start_time']);
        $end_time = date_create($params['end_time']);
        $start_time =date_format($start_time,"Y/m/d H:i:s");
        $end_time =date_format($end_time,"Y/m/d H:i:s");
        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        $id = $params['id'];


        $_where =  '';
        if (!empty($id)){
            $_where .= " id != $id";

        }
        $data_active = Db::table('merchants_redpacket')->where(" status = 1")->where($_where)->select();

        if(!empty($data_active)){
            #创建的开始时间 和结束时间 都不能和 原有数据的开始时间和结束时间有交集
            foreach ($data_active as &$v){
                if(($start_time >= strtotime($v['end_time']) and $end_time >strtotime($v['end_time']))or ($start_time < strtotime($v['start_time']) and $end_time<= strtotime($v['start_time']) )){
                    #符合证明新上传的时间和数据库中没有交集

                }else{
                    #如果有一种不符合 就会走这
                    $code = "保存的时间和已有的时间相重复!,10001";
                    $error_arr = explode(',',$code );
                    return json(DataReturn($error_arr[0], $error_arr[1]));
                }

            }
        }
        #判断 结束时间 大于开始时间
        if( $data['end_time']<=$data['start_time']){

            $ret = '红包结束时间应大于红包开始时间'.','.'1000';
            $error_arr = explode(',', $ret);


            return DataReturn($error_arr[0], $error_arr[1]);

        }
        if(empty($params['id'])){
            #添加
            #存红包

            Db::table('merchants_redpacket')->insert($data);
            return DataReturn('保存成功', 0);



        }else{
            #修改
            Db::table('merchants_redpacket')->update($data);
            return DataReturn('保存成功', 0);

        }
    }

    /*
    * @content:查询二级分类
    * @params: $service_class_id 服务id   $second_classs_id: 二级分类id
    * */

    static function getMerchantsCateClass($data){

        if(!empty($data)){
            $str ='<option value="">请选择二级分类</option>';
            if(!empty($data)){

                foreach($data as $k=>$v){

                    $str.=" <option value=\"{$v['id']}\"";
                    if(!empty($second_classs_id) and $second_classs_id==$v['id']){
                        $str.=" selected";
                    }
                    $str.=" >".$v['title']."</option>";
                }

            }
            return $str;

        }else{
            $str ='';
            return $str;
        }

    }
    /*
     * @content 处理分类id  组成 html
     * @return array
     * @params cate_id 二级分类id
     * @params cate_pid 一级分类id
     * @params merchants_id 商户id
     * @params is_type   1 本地生活 2 联盟商家
     * */
    static function  getCateSession($params){
        $str='';
        $data = array();
        $_array=array();
        $data['cate_id'] =$params['cate_id'];
        $data['cate_pid'] =$params['cate_pid'];
        #根据分类id 获取名称  一级 - 二级
        $cate_pid_title = Db::table('merchants_cate')->where(array('id'=> $data['cate_pid']))->value('title');
        $cate_id_title = Db::table('merchants_cate')->where(array('id'=> $data['cate_id']))->value('title');
        $data['cate_title'] = $cate_pid_title.'-'.$cate_id_title;


        #查询缓存 是否存在

        $_session=  session('getCateSession');
        if(!empty($_session)){
            array_push($_session,$data);
            Session::set('getCateSession',$_session);

        }else{
            array_push($_array,$data);
            session::set('getCateSession',$_array);

        }

        #是否有商户id   证明 是修改
        if(!empty($params['merchants_id'])){
            #根据商户id 查询分类
            $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id'=>$params['merchants_id']))->field('cate_pid,cate_id,id')->select();

            if ($cate_relation[0]['cate_pid']>0)
            {
                foreach($cate_relation as $k=>$v){
                    $cate_pid_title = Db::table('merchants_cate')->where(array('id'=> $v['cate_pid']))->value('title');
                    $cate_id_title = Db::table('merchants_cate')->where(array('id'=> $v['cate_id']))->value('title');
                    $cate_relation[$k]['cate_title'] = $cate_pid_title.'-'.$cate_id_title;
                }

                $str .= self::getInfoCateHtml($cate_relation,'DB');


            }

        }
        $_session_array=  session('getCateSession');

        $str .= self::getInfoCateHtml($_session_array,'S');
        return $str;

    }
    /*
     * 删除之后走的
     * */
    static function getcateDelSession($params){
        #查询缓存 是否存在
        $str='';


        #是否有商户id   证明 是修改
        if(!empty($params['merchants_id'])){
            #根据商户id 查询分类
            $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id'=>$params['merchants_id']))->field('cate_pid,cate_id,id')->select();

            if ($cate_relation[0]['cate_pid']>0)
            {
                foreach($cate_relation as $k=>$v){
                    $cate_pid_title = Db::table('merchants_cate')->where(array('id'=> $v['cate_pid']))->value('title');
                    $cate_id_title = Db::table('merchants_cate')->where(array('id'=> $v['cate_id']))->value('title');
                    $cate_relation[$k]['cate_title'] = $cate_pid_title.'-'.$cate_id_title;
                }

                $str .= self::getInfoCateHtml($cate_relation,'DB');


            }

        }
        $_session_array=  session('getCateSession');
        if(!empty($_session_array)){
            $str .= self::getInfoCateHtml($_session_array,'S');

        }

        return $str;
    }
    /*
   * @content 拼接分类 名称  组成html
   * */
    static function getInfoCateHtml($data,$mark)
    {
        $str = null;
        if (!empty($data)) {

            foreach ($data as $k=>$v) {
                $str .= "
<div style=\"cursor: pointer;padding:0 10px;height:2rem;border: solid 1px #000000;border-radius: 10px;text-align: center;line-height: 2rem;margin-right:2rem;color: #f2f2f2;\"   onclick=\"cateDel('" . $k . "','" . $mark . "','" . $v['id'] . "')\">{$v['cate_title']}</div>";
            }
        }
        return $str;

    }
    /*
     * @content 把 一级分类id 组成字符串
     * */
    static function getCatePidStr($merchants_id){
        #缓存
        $_sesion = Session('getCateSession');
        $_idstr_arrs = array();
        $_idstr_arrd = array();
        $_idstr = '';
        if(!empty($_sesion)){
            foreach ($_sesion as $sk =>$sv){
               array_push($_idstr_arrs,$sv['cate_id']);

            }

        }
        #数据库
        if(!empty($merchants_id)){
            $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id'=>$merchants_id))->field('cate_pid,cate_id,id')->select();

            if ($cate_relation[0]['cate_id']>0)
            {
                foreach($cate_relation as $k=>$v){
                    array_push($_idstr_arrs,$v['cate_id']);
                }
            }
        }
        $_idstr= implode(',',$_idstr_arrs);
        return $_idstr;

    }
    /*
     * @content  删除任务 创建  或修改
     * @return array
     * @params params 任务信息
     * @params id 任务id
     * */
    static function SaveDataTask($params){
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'task_title',
                'error_msg'         => '任务名称不能为空',
                'error_code'         => 12001,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'task_type',
                'error_msg'         => '请选择任务类型',
                'error_code'         => 12004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'task_describe',
                'error_msg'         => '任务描述不能为空',
                'error_code'         => 12005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'task_num',
                'error_msg'         => '任务数量不能为空',
                'error_code'         => 12005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '请选择任务开始时间',
                'error_code'         => 12005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'end_time',
                'error_msg'         => '请选择任务结束时间',
                'error_code'         => 12005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'valid_time',
                'error_msg'         => '任务周期不能为空',
                'error_code'         => 12005,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'task_price',
                'error_msg'         => '奖励金额不能为空',
                'error_code'         => 12005,
            ],




        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);

            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }

        if(!empty($params['id'])){
            #编辑

            Db::table('merchants_task')->where(array('id'=>$params['id']))->update($params);
        }else{
           /* #判断当前任务类型 在进行中
            $taskTime = Db::table('merchants_task')->where(array('task_type'=>$params['task_type']))
               ->find();
            if(!empty($taskTime)){
                $ret = "当前任务类型已存在，不可重复添加,12001";
                $error_arr=explode(',',$ret);
                throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
            }*/
            #创建
            Db::table('merchants_task')->insertGetId($params);

        }
        return DataReturn('保存成功', 0);


    }
    /*
     * @content 查询红包的数量 利润 金额
     * @return  array()
     * 都是所有商户的
     * */
    static function DataDealWithReadpacketLog($params){
        if(!empty($params)){
            foreach ($params as $k=>$v) {

                $create = date_create($v['create_time']);

                #昨日
                $create_time = date_format($create,"Y-m-d");
                #今日
                $day = date('Y-m-d', strtotime("+1day",  strtotime($create_time)));

                #昨日利润 按日期查询
                $zday_price = OrderService::getOrdercommissionPrice($create_time,'');
                #根据今天时间查询红包的发放比列 (这里有 比例 数量)
                $merchants_redpacket = RedpacketService::getMerchantsRedpacketInfo($day);
                if(empty($merchants_redpacket)){
                    $merchants_redpacket['num'] = 0;
                    $merchants_redpacket['price_bili'] = 1;
                }

                #今日所有商户发放金额
                $params[$k]['amount_money'] =priceFormat(($merchants_redpacket['price_bili'] *  $zday_price));
                #已发放金额
                $params[$k]['already_count_price'] = priceFormat(( Db::table('merchants_redpacket_receive_log')->where("DATE_FORMAT(create_time,'%Y-%m-%d') = '$day'")->sum('price')));
                #剩余发放数量
                $params[$k]['surplus_num'] = intval($merchants_redpacket['num']) - intval($v['already_count_num']);
                if($params[$k]['surplus_num']<0){
                    $params[$k]['surplus_num'] = 0;
                }
                #剩余发放金额
                $params[$k]['surplus_price'] = priceFormat(($params[$k]['amount_money']-  $params[$k]['already_count_price']));
                if($params[$k]['surplus_price'] <0){
                    $params[$k]['surplus_price'] = priceFormat(0);
                }


                #今日利润 按日期查询
                $day_price = OrderService::getOrdercommissionPrice($day,'');

                #昨日利润
                $params[$k]['zday_price'] = priceFormat($zday_price);
                #今日利润
                $params[$k]['day_price'] = priceFormat($day_price);
                #每日发放数量
                $params[$k]['amount_num'] = $merchants_redpacket['num'] ;
                #时间 今天 因为merchants_redpacket_log  这个表的时间 核销订单成功后 创建的 所以 需要加1天  才能算今天领取时间
                $params[$k]['create'] = date('Y-m-d', strtotime("+1day",  strtotime($create_time)));


            }
            return $params;
        }
    }
    /*
     * @content  查询红包详情 根据商户id查
     * */
    static function DataDealWithSaveReadpacket($params,$create){

        if(!empty($params)){
            foreach ($params as $k=>$v) {
                $_where = " id = '".$v['merchants_id']."'";
                // 获取列表
                $data_params = array(
                    'page'         => false, //是否分页
                    'where'     => $_where,//查找条件
                );
                $data = MerchantsService::getMerchantsInfo($data_params);
                $params[$k]['title'] = $data['title'];
                $params[$k]['is_type_title'] = $data['is_type_title'];
                $params[$k]['cate_title'] = $data['cate_title_title'];
                $params[$k]['_address'] = $data['_address'];
                $params[$k]['merchants_id'] = $data['id'];
                #领取时间
                $params[$k]['receive_time'] = Db::table('merchants_redpacket_receive_log')
                    ->where("DATE_FORMAT(create_time,'%Y-%m-%d') = '$create' and merchants_id = '".$v['merchants_id']."'")
                    ->order('id desc')
                    ->value('create_time');
                if(empty($params[$k]['receive_time'])){
                    $params[$k]['receive_time'] = '暂未领取';
                }
                #领取金额
                $params[$k]['receive_price'] = priceFormat((Db::table('merchants_redpacket_receive_log')
                    ->where("DATE_FORMAT(create_time,'%Y-%m-%d') = '$create' and merchants_id = '".$v['merchants_id']."'")
                    ->sum('price')));

                #订单金额
                $params[$k]['order_price']  = Db::table('merchants_order')->where("DATE_FORMAT(checkout_time,'%Y-%m-%d') = '$create' and merchants_id = '".$v['merchants_id']."' and order_status = 2" )->value('actual_price');
                if(empty($params[$k]['order_price'])){
                    $params[$k]['order_price'] = priceFormat(0);
                }





            }
            return $params;
        }
    }

}
