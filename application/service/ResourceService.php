<?php
/**
 * Created by ljg
 * User: ljg
 * Date: 2020/6/12
 * Time: 9:53
 * Content: 资源处理公共方法
 */

namespace app\service;

use phpmailer\BaseException;
use Redis\Redis;
use think\Db;
use think\Exception;
use think\Session;

/**
 * Class ResourceService
 * @package app\service
 * @content 资源服务层
 */
class ResourceService
{
    public static $session_suffix = null;

    /**
     * @param $mark 接收上传类型(是图片还是视频)(img:图片  video:视频)
     * @param string $_oldSrc 接收要删除的图片/视频信息
     * @param string $_field 接收上传的图片字段(banner/imgurl/picurl/video)
     * @return array
     */
    static function uploadFile($params)
    {
        $_field = $params['field'];
        $mark = $params['mark'];
        if (empty($mark)) $mark = 'thumb_img';
        if (empty($params['id'])) $params['id'] = '';
        $_oldSrc = $params['oldSrc'];
        $_suffix = $params['table'] . $params['id'];
        self::$session_suffix = 'source_upload' . $_suffix;

        $oss = new OssService();
        if (!empty($mark)) {
            if ($mark == 'img') {
                # 上传图片

                //$_filename ='20201102/1604287537249LBK0AOCcrd.jpg' ;
                $_filename = self::uploads($_field, $_oldSrc);

                if (!empty($_filename)) {

                    //上传到oss
                    $show_url = $oss->OssUpload($_filename);

                    self::setUploadCache($show_url, $mark);

                    if (strlen($show_url) > 0) {
                        self::delFile(ROOT_PATH . 'upload' . $_filename);
                    }
                    if (strlen($_oldSrc) > 0) {
                        $oss->OssDel(str_replace($oss->successurl, '', $_oldSrc));
                    }

                    return array('status' => true, 'src' => $show_url, 'message' => '图片上传成功');
                } else {
                    return array('status' => false, 'src' => $_filename, 'message' => '图片上传失败');
                }
            } else {
                # 上传视频
                $_filename = self::uploads($_field, $_oldSrc, 'video');
                if (!empty($_filename)) {
//                    self::setUploadCache($_filename,$mark);
                    //上传到oss
                    $show_url = $oss->OssUpload($_filename);
                    self::setUploadCache($show_url, $mark);
                    $oss->OssDel(str_replace($oss->successurl, '', $_oldSrc));
                    //删除本地图片
                    self::delFile(ROOT_PATH . 'upload' . $_filename);
                    return array('status' => true, 'src' => $show_url, 'message' => '视频上传成功');
                } else {
                    return array('status' => false, 'src' => $_filename, 'message' => '视频上传失败');
                }
            }
        } else {
            # 上传标识为空
            return array('status' => false, 'message' => '上传失败缺少判断标识!');
        }
    }


    /**
     * @return bool
     * @context 删除缓存中无用文件方法
     */
    public static function delUploadCache()
    {
        $name = self::$session_suffix;

        try {
            $_cache = Session($name);

            if (!empty($_cache)) {
                foreach ($_cache as $k => $v) {
                    /*
                    if($v['type']=='img'){
                        # 判断文件是否存在
                        $_delSrc = ROOT_PATH . DS . 'upload' . DS . $v['src'];
                    }else{
                        $_delSrc = ROOT_PATH . DS . 'upload' .DS . 'video' . DS . $v['src'];
                    }
                    */

                    self::delFile($v['src']);
                }
                $_cache = null;
                Session($name, $_cache);
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {

        }
    }

    /**
     * @param $src_coll
     * @return array
     * @throws BaseException
     * @throws Exception
     * @context 将文件地址在上传文件缓存中删除，以免删除文件
     */
    public static function delCacheItem($src_coll)
    {
        $name = self::$session_suffix;

        if (!empty($src_coll)) {
            $_exect = array();
            $_cache = Session($name);

            if (is_array($src_coll)) {
                $_exect = $src_coll;
            } else {
                array_push($_exect, $src_coll);
            }

            if (empty($_cache)) {

                return array("status" => true);
            }

            foreach ($_cache as $k => $v) {
                if (in_array($v['src'], $_exect)) {
                    unset($_cache[$k]);
                }
            }

            $_cache = array_values($_cache);

            Session($name, $_cache);
            return array("status" => true);
        }
        /* else{
 //            throw new \BaseException(array("code"=>403,"msg"=>"删除上传文件缓存参数错误","errorcode"=>10001,"status"=>false));
         }*/
    }

    /**
     * @param $_file_name  上传的文件名
     * @return bool
     * @context 将上传图片放入缓存
     */
    public function setUploadCache($_file_name, $mark)
    {
        $_cache = Session(self::$session_suffix);
        if (!empty($_cache)) {
            array_push($_cache, array("src" => $_file_name, "type" => $mark));
        } else {
            $_cache = array(array("src" => $_file_name, "type" => $mark));
        }
        Session(self::$session_suffix, $_cache);
        return true;
    }

    /**
     * @param $imgUrl
     * @param $mark
     * @return string
     * @content 图片路径整理(返回 http/https)
     */
    function imgUrl($imgUrl, $mark = false)
    {
        if (!empty($imgUrl) and !is_null($imgUrl)) {
            $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
            if (!$mark) {
                if (file_exists(ROOT_PATH . "public/upload/" . $imgUrl)) {
                    $imgUrl = str_replace('\\', '/', $imgUrl);
                    return $http_type . $_SERVER['HTTP_HOST'] . "/upload/" . $imgUrl;
                } else {
                    return "";
                }
            } else {
                if (file_exists(ROOT_PATH . "public" . DS . $imgUrl)) {
                    $imgUrl = str_replace('\\', '/', $imgUrl);
                    return $http_type . $_SERVER['HTTP_HOST'] . '/' . $imgUrl;
                } else {
                    return "";
                }
            }
        } else {
            return "";
        }
    }


    /**
     * @access public
     * @param string $value
     * @return mixed|string
     * @content 上传base64格式图片
     */
    static function uploadBase($value = '', $mark = '')
    {
        if (empty($mark)) {
            $up_dir = ROOT_PATH . DS . 'upload/' . date("Ymd") . "/";//存放在当前目录的upload文件夹下
        } else {
            $up_dir = ROOT_PATH . DS . 'upload/' . $mark . '/' . date("Ymd") . "/";//存放在当前目录的upload文件夹下
        }
        $base64_img = trim($value);
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_img, $result)) {
            $type = $result[2];
            if (in_array($type, array('pjpeg', 'jpeg', 'jpg', 'gif', 'bmp', 'png'))) {
                if (is_dir($up_dir) || mkdir($up_dir, 0755, true)) {
                    $fileName = self::getMsectime() . self::getRandomString(10) . '.' . $type;
                    $new_file = $up_dir . $fileName;
                    if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_img)))) {
                        if (empty($mark)) {
                            return date("Ymd") . "/" . $fileName;
                        } else {
                            return $mark . '/' . date("Ymd") . "/" . $fileName;
                        }
                    } else {
                        return array('status' => false, 'msg' => '图片上传失败');
                    }
                } else {
                    return array('status' => false, 'msg' => '系统错误');
                }
            } else {
                return array('status' => false, 'msg' => '不支持该图片类型');
            }
        } else {
            return array('status' => false, 'msg' => '图片格式不正确');
        }
    }

    /**
     * @param $_filesrc
     * @return bool
     * @content 按路径删除文件方法
     */
    public static function delFile($_filesrc)
    {

        $oss = new OssService();
        $src = str_replace('http://ecars.oss-cn-beijing.aliyuncs.com/', '', $_filesrc);
        $src = substr($src, 0, 1) == '/' ? substr($src, 1) : substr($src, 0);
        $oss->OssDel($src);
        return true;
//        if (file_exists($_filesrc)) {
//            @unlink($_filesrc);
//            return true;
//        }else{
//            return false;
//        }
    }


    /**
     * @param $filename
     * @param string $delSrc
     * @param string $_type
     * @param int $size
     * @return mixed|string
     * @context 上传文件执行
     */
    private function uploads($filename, $delSrc = '', $_type = '', $size = 204800000000)
    {
        # 判断是否存在要删除的图片,如果存在进行删除,如果不存在正常上传图片
        if (!empty($delSrc)) {
            # 判断文件是否存在
            $_delSrc = ROOT_PATH . DS . 'upload' . DS . $delSrc;
            self::delFile($_delSrc);
        }
        // 获取表单上传文件
        $file = request()->file($filename);
        $fileName = date('Ymd') . DS . self::getMsectime() . self::getRandomString(10);
        // 移动到框架应用根目录/public/upload/ 目录下
        if ($_type == 'video') {
            $info = $file->validate(['size' => $size])->move(ROOT_PATH . DS . 'upload' . DS . 'video', $fileName);
        } else {
            $info = $file->validate(['size' => $size])->move(ROOT_PATH . DS . 'upload', $fileName);
        }
        if ($info) {
            // 成功上传后 获取上传信息
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            //把反斜杠(\)替换成斜杠(/) 因为在windows下上传路是反斜杠径
            $getSaveName = str_replace("\\", "/", $info->getSaveName());
            if ($_type == 'video') {
                return 'video' . '/' . $getSaveName;
            } else {
                return $getSaveName;
            }
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }
    }

    /**
     * @return float
     * @content 返回当前的毫秒时间戳
     */
    static function getMsectime()
    {
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    /**
     * @param $len  长度
     * @param null $chars 字符集
     * @return string   对应长度的随机数字符串
     * @context 获取随机数
     */
    static function getRandomString($len, $chars = null)
    {
        if (is_null($chars)) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }
        mt_srand(10000000 * (double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars) - 1; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }

    /**
     * @param $str string 文件名称
     * @return string 根据获取后缀名判断文件类型
     */
    function fileFormat($str)
    {
        // 取文件后缀名
        $str = strtolower(pathinfo($str, PATHINFO_EXTENSION));
        // 图片格式
        $image = array('webp', 'jpg', 'png', 'ico', 'bmp', 'gif', 'tif', 'pcx', 'tga', 'bmp', 'pxc', 'tiff', 'jpeg', 'exif', 'fpx', 'svg', 'psd', 'cdr', 'pcd', 'dxf', 'ufo', 'eps', 'ai', 'hdri');
        // 视频格式
        $video = array('mp4', 'avi', '3gp', 'rmvb', 'gif', 'wmv', 'mkv', 'mpg', 'vob', 'mov', 'flv', 'swf', 'mp3', 'ape', 'wma', 'aac', 'mmf', 'amr', 'm4a', 'm4r', 'ogg', 'wav', 'wavpack');
        // 压缩格式
        $zip = array('rar', 'zip', 'tar', 'cab', 'uue', 'jar', 'iso', 'z', '7-zip', 'ace', 'lzh', 'arj', 'gzip', 'bz2', 'tz');
        // 文档格式
        $text = array('exe', 'doc', 'ppt', 'xls', 'wps', 'txt', 'lrc', 'wfs', 'torrent', 'html', 'htm', 'java', 'js', 'css', 'less', 'php', 'pdf', 'pps', 'host', 'box', 'docx', 'word', 'perfect', 'dot', 'dsf', 'efe', 'ini', 'json', 'lnk', 'log', 'msi', 'ost', 'pcs', 'tmp', 'xlsb');
        // 匹配不同的结果
        switch ($str) {
            case in_array($str, $image):
                return 'img';
                break;
            case in_array($str, $video):
                return 'video';
                break;
            case in_array($str, $zip):
                return 'zip';
                break;
            case in_array($str, $text):
                return 'text';
                break;
            default:
                return 'img';
                break;
        }
    }


    /**
     * [ContentStaticReplace 编辑器中内容的静态资源替换]
     * @param    [string]    $content [在这个字符串中查找进行替换]
     * @param    [string]    $type    [操作类型[get读取额你让, add写入内容](编辑/展示传入get,数据写入数据库传入add)]
     * @return   [string]             [正确返回替换后的内容, 则返回原内容]
     * @version  1.0.0
     * @datetime 2020年6月22日13:45:48
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     */
    public static function ContentStaticReplace($content, $type = 'get')
    {
        switch ($type) {
            // 读取内容
            case 'get':
                return str_replace('src="/upload/', 'src="' . __MY_PUBLIC_URL__ . 'upload/', $content);
                break;

            // 内容写入
            case 'add':
                return str_replace(array('src="' . __MY_PUBLIC_URL__ . 'upload/ueditor', 'src="' . __MY_ROOT_PUBLIC__ . 'upload/ueditor'), 'src="/upload/ueditor', $content);
        }
        return $content;
    }


    /**
     * 附件展示地址处理
     * @param    [type]                   $value [description]
     * @version  1.0.0
     * @datetime 2020年6月22日13:45:48
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     */
    public static function AttachmentPathViewHandle($value)
    {
        if (!empty($value)) {
            if (substr($value, 0, 4) != 'http') {
                //return OssService::successurl.$value;
                return config('ecars.attachment_host') . '/upload/' . $value;
            }
            return $value;
        }
        return '';
    }


    /**
     * 附件删除
     * @param    [array]              $params [输入参数]
     * @version  1.0.0
     * @datetime 2019-06-25T23:35:27+0800
     * @author   Devil
     * @blog     http://gong.gg/
     */
    public static function AttachmentDelete($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'id',
                'error_msg' => '操作id有误',
            ]
        ];
        $ret = ParamsChecked($params, $p);
        if ($ret !== true) {
            return DataReturn($ret, -1);
        }

        // 获取数据
        $data = Db::name('Attachment')->find(intval($params['id']));
        if (empty($data)) {
            return DataReturn('数据不存在或已删除', -1);
        }

        // 删除文件
        $path = substr(ROOT_PATH, 0, -1) . $data['url'];
        if (file_exists($path)) {
            if (is_writable($path)) {
                if (DB::name('Attachment')->where(['id' => $data['id']])->delete()) {
                    // 删除附件
                    \base\FileUtil::UnlinkFile($path);

                    $ret = DataReturn('删除成功', 0);
                } else {
                    $ret = DataReturn('删除失败', -100);
                }
            } else {
                $ret = DataReturn('没有删除权限', -1);
            }
        } else {
            if (Db::name('Attachment')->where(['id' => $data['id']])->delete()) {
                $ret = DataReturn('删除成功', 0);
            } else {
                $ret = DataReturn('删除失败', -100);
            }
        }

        // 处理
//        if($ret['code'] == 0)
//        {
//            // 附件删除成功后处理钩子
//            $hook_name = 'plugins_service_attachment_delete_success';
//            Hook::listen($hook_name, [
//                'hook_name'         => $hook_name,
//                'is_backend'        => true,
//                'data'              => $data,
//            ]);
//        }
        return $ret;
    }


    /**
     * 附件添加
     * @param    [array]         $params [输入参数]
     * @version  1.0.0
     * @datetime 2020年10月8日17:53:18
     * @author   Devil
     * @blog     https://blog.csdn.net/juziaixiao
     */
    public static function AttachmentAdd($params = [])
    {


        // 添加到redis
        $redis = new Redis([]);
        //$redis->flushDB();

//        $img_url=$redis->hGet('ueditor',$params['table'].$params['id']).','.$params['url'];
//        $redis->hSet('ueditor',$params['table'],$img_url);
        $suffix = $params['table'] . $params['id'];
        if (isset($params['other'])) {
            $suffix .= $params['other'];
        }
        $redis->sAdd('ueditor' . $suffix, $params['url']);
        return DataReturn('添加成功', 0, $params);


        // 删除本地图片
//        if(!empty($params['path']))
//        {
//            \base\FileUtil::UnlinkFile($params['path']);
//        }
//        return DataReturn('添加失败', -100);
    }


    /**
     * 附件路径处理
     * @param   [string]          $value [附件路径地址]
     * @version 1.0.0
     * @date    2020年10月8日17:53:30
     * @desc    description
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     */
    public static function AttachmentPathHandle($value)
    {
        return empty($value) ? '' : str_replace([__MY_PUBLIC_URL__, __MY_ROOT_PUBLIC__], DS, $value);
    }

    /**
     * [DelRedisOssFile 删除缓存和oss中的无用图片]
     * @param    [array]          $params [输入参数]
     * @version  0.0.1
     * @datetime 2020年10月9日16:23:52
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     */
    public static function DelRedisOssFile($params)
    {

        $redis = new Redis();
        $all_img = $redis->sMembers('ueditor' . $params['table'] . $params['id'], true);
        if (!$all_img) {
            return true;
        }
        $all_img = count($redis->sMembers('ueditor' . $params['table'] . $params['id'], true)) > 0 ? $redis->sMembers('ueditor' . $params['table'] . $params['id'], true) : [];


        //获取当前准备保存的所有图片
        $now_img = empty(get_images_from_html($params["ueditor_content"])) ? [] : get_images_from_html($params["ueditor_content"]);
        foreach ($all_img as $k => $v) {
            $redis->delete('ueditor' . $params['table'] . $params['id'], 'set', $v);
            if (in_array($v, $now_img)) {
                continue;
            } else {
                //删除对象存储
                self::delFile($v);

//                $oss=new OssService();
//                $src=str_replace('http://ecars.oss-cn-beijing.aliyuncs.com/','',$v);
//                $src=substr($src,0,1)=='/' ? substr($src,1) :substr($src,0);
//                $oss->OssDel($src);
            }
        }

        return true;
    }

    /**
     * [DelRedisOssFile 删除oss中的无用图片]
     * @param    [string]          $old_src [原图片src]
     * @param    [string]          $temp_src [现图片src]
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月9日16:23:52
     */
    public static function DelOssFile($old_src, $temp_src)
    {


        //$oss=new OssService();
        $old_src = explode(',', $old_src);
        $temp_src = explode(',', $temp_src);
        foreach ($old_src as $k => $v) {
            if (in_array($v, $temp_src)) {
                unset($old_src[$k]);
            }
        }
        foreach ($old_src as $v) {
            self::delFile($v);
        }
        return true;
    }

    /**
     * @param string $content
     * @return string
     * @context 去除富文本标签
     */
    public static function htmlContentFormat($content = '')
    {

        $data = $content;

        $formatData_01 = htmlspecialchars_decode($data);//把一些预定义的 HTML 实体转换为字符
        $formatData_03 = str_replace("&nbsp;", "", $formatData_01);
        $formatData_02 = strip_tags($formatData_03);//函数剥去字符串中的 HTML、XML 以及 PHP 的标签,获取纯文本内容

        return $formatData_02;

    }


}
