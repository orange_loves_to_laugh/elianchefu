<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/6
 * Time: 13:39
 */

namespace app\service;

/**
 * 活动提示管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

use app\reuse\controller\ResponseJson;
use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Session;
class ActivetipsService
{
    use ResponseJson;
    /**
     * [ActivetipsIndex 获取活动管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function activetipsIndex($params){
        #where条件  写成活的
        $where = empty($params['where']) ? ['status'=>'1'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;
//        $exp = new Expression('field(active_putstatus,2,1,3,4)');

        #应用管理列表信息
        $data = Db::table('active_tips')
            ->where($where)->order('active_end desc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data=$data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        #处理方法
        return self::InforDataDealWith($data,$params);


    }
    /**
     * [ActivetipsSave 活动提示管理添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function     activetipsSave($params){
        if(empty($params['jump_type'])){
            unset($params['jump_type']);
            unset($params['jump_id']);
        }

        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'active_title',
                'error_msg'         => '活动名称不能为空',
                'error_code'         => 30002,
            ],


            [
                'checked_type'      => 'empty',
                'key_name'          => 'active_start',
                'error_msg'         => '上架时间不能为空',
                'error_code'         => 30003,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'active_end',
                'error_msg'         => '下架时间不能为空',
                'error_code'         => 30004,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'active_imgurl',
                'error_msg'         => '请上传弹窗图',
                'error_code'         => 30002,
            ],


        ];
        if (empty($params['id'])) {
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'active_imgurl',
                'error_msg' => '请上传弹窗图',
                'error_code' => 30001,
            ];
        }
        #关联类型 为1   编辑详情 2 页面通知
        if($params['is_type'] == 1){
            $p[] = [
                'checked_type' => 'empty',
                'key_name' => 'content_detail',
                'error_msg' => '请上传详情图',
                'error_code' => 30001,
            ];
        }
        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);
            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }


        if (isset($data['thumb_img'])) {
            unset($data['thumb_img']);
        } if (isset($data['oldactive_imgurl'])) {
            unset($data['oldactive_imgurl']);
        }
        #把时间转换成需要的格式 时间戳
        $start_time = date_create($params['active_start']);
        $end_time = date_create($params['active_end']);
        $start_time =date_format($start_time,"Y/m/d H:i:s");
        $end_time =date_format($end_time,"Y/m/d H:i:s");
        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        $id = $params['id'];


        $_where =  '';
        if (!empty($id)){
            $_where .= " id != $id";

        }
        $data_active = Db::table('active_tips')->where(" status = 1")->where($_where)->select();

        if(!empty($data_active)){
            #创建的开始时间 和结束时间 都不能和 原有数据的开始时间和结束时间有交集
            foreach ($data_active as &$v){
                if(($start_time >= strtotime($v['active_end']) and $end_time >strtotime($v['active_end']))or ($start_time < strtotime($v['active_start']) and $end_time<= strtotime($v['active_start']) )){
                    #符合证明新上传的时间和数据库中没有交集

                }else{
                    #如果有一种不符合 就会走这
                    $code = "保存的时间和已有的时间相重复!,10001";
                    $error_arr = explode(',',$code );
                    return json(DataReturn($error_arr[0], $error_arr[1]));
                }

            }
        }
        #判断 活动类型时 的时间是否 符合 我创建的存在
        if($params['active_type'] == 1){
            #从缓存中获取 活动的开始时间 和结束时间
            $_activeSession = Session('activetipsTypeSession');
            if(!empty($_activeSession)){
                if($_activeSession['active_start']>$start_time or $_activeSession['active_end']>$end_time){
                    $code = "活动时间不在你的保存时间内,10001";
                    $error_arr = explode(',',$code );
                    return json(DataReturn($error_arr[0], $error_arr[1]));
                }
            }
        }
        #关联类型 为1   编辑详情
        if($params['is_type'] == 1){
            #处理百度编辑器删除多余图片问题（上传了但是没有点击保存 或者删除了 又重新上传）
            $data = self::HandleData($params);
        }
        // 添加/编辑
        if (empty($params['id'])) {

            #添加处理
            $activeId = Db::table("active_tips")->insertGetId($data);
            if ($activeId) {
                #是否有缓存 有则修改active_type
                $_session = session('ActivetipsTitle');
                if(!empty($_session)){
                    $active_type = $_session['type'];
                    DB::table('active_tips')->where(array('id'=>$activeId))->update(array('active_type'=>$active_type,'spoil_id'=>$_session['id']));
                }
            }


        } else {

            #修改
            Db::table("active_tips")->where(array('id' => $params['id']))->update($data);
            #是否有缓存 有则修改active_type
            $_session = session('ActivetipsTitle');
            if(!empty($_session)){
                $active_type = $_session['type'];
                DB::table('active_tips')->where(array('id'=>$params['id']))->update(array('active_type'=>$active_type,'spoil_id'=>$_session['id']));
            }

        }
        #前端删除缓存
        $_redis = new Redis();
        $_redis->hDel("pop","active");
        #保存图片之后  删除单图 多图上传 缓存 intergral_exchange:表名
        $params['id']=empty($params['id'])?'':$params['id'];
        ResourceService::$session_suffix='source_upload'.'acive_tips'.$params['id'];
        #删除单图
        ResourceService::delCacheItem($params['active_imgurl']);

        #添加操作的时候删除活动提示管理缓存id  还有修改保存时
        Session::delete('activetipsId');
        Session::delete('ActivetipsTitle');
        return json(DataReturn('保存成功', 0));

    }
    /*
   * @content ：处理方法
   * */
    function InforDataDealWith($data,$params){
        $time = date("Y-m-d H:i:s");
        if(!empty($data)){
            foreach($data as $k=>$v){
                if($time < $v['active_start'] and $time<$v['active_end']){
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['active_status'] = '待上架';

                }else if($time >= $v['active_start'] and $time<=$v['active_end']){
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['active_status'] = '已上架';

                }else if($time > $v['active_start'] and $time>$v['active_end']){
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['active_status'] = '已下架';

                }

            }
        }
        return $data;
    }
    /**
     * 保存前数据处理
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params)
    {

        if (isset($params["content_detail"])) {

            //$params["club_content"]=ResourceService::ContentStaticReplace($params["club_content"],'add');
            //删除无用图片
            $params['table'] = 'active';
            $params['ueditor_content'] = $params['content_detail'];
            $re = ResourceService::DelRedisOssFile($params);
            if ($re) {
                unset($params['table']);
                unset($params['ueditor_content']);
            }

        }
        if (isset($params['thumb_img'])) {
            unset($params['thumb_img']);
        }
        if (isset($params['oldactive_imgurl'])) {
            unset($params['oldactive_imgurl']);
        }
        return $params;
    }
    /**
     * 总数
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @param    [array]          $where [条件]
     */
    public static function activetipsIndexCount(){
        return (int) Db::name('active_tips')->where('status = 1')->count();
    }
    /**
     * 活动类型查询
     * @author   lc
     * @date    2020年7月21日13:29:33
     * @desc    description
     * @params  $type 1"服务详情",2"关联活动",3"办理会员活动","4二手车","5联盟商家"
     */
    static function activetipsType($type,$keywords){
        $params['param']['type'] = $type;
        $params['param']['keywords'] = strim($keywords);
        $data=ApplayService::SearchByCateAndKeywordsGetData($params);
       /* if($type == 1){
            #服务详情



            #分页
        }else if($type == 2){
            #关联活动
            #线上活动 查询活动         获取列表
            $time = date("Y-m-d H:i:s");

            $_where="a.active_putstatus != 4 and '$time'>= a.active_start and '$time'<= a.active_end";
            $active = Db::table('active a')
                ->field("*,(a.active_num - (select count(id) from log_active l where active_id = a.id)) surplus_num,(select ac.content_detail from active_content ac where ac.active_id = a.id) content_detail")
                ->where($_where)->order('a.id desc');
            $data= $active->select();


        }else if($type == 3){
            #办理会员

        }
        else if($type == 4){
            #推荐二手车

        }
        else if($type == 5){
            #联盟商家
            #推荐商户
            $active = Db::table('merchants m')
                ->order('m.id desc');


            $data= $active->select();
        }*/

        $re = self::getActiveArrInfo($data,$type);
        return $re;


    }
    /**
     * [getActiveArrInfo 线上活动模板]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params  $type 1"服务详情",2"关联活动",3"办理会员活动","4二手车","5联盟商家"
     */
    static function getActiveArrInfo($arr,$type,$page='',$search_title=''){
        $str = '';
        if (!empty($arr) and is_array($arr)) {
            if($type == 1){
                #服务详情
                $str=" 
                   <div class=\"row\" style=\"width: 100%;float: left\">
                   <input type='hidden'id='activeType' value='{$type}'>
                                                                    <div class=\"col-sm-6\">
                                                                        <div class=\"form-group\">
                                                                            <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchActiveName()\" placeholder=\"请输入服务名称，回车键搜索\" autocomplete=\"off\">
                                                                            <i class=\"form-group__bar\"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
 
                   <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                               
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>服务名称</th>
                                                <th>服务分类</th>
                                                <th>服务时长</th>
                                                <th>是否预约</th>
                                          
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                foreach($arr as $k=>$v){
                    $service_class = getServiceClassTitle($v['service_class_id']);
                    if($v['appoint'] == 1){
                        $appoint = '是';

                    }else{
                        $appoint = '否';

                    }
                    $str.=" 
                        <td>{$v['title']}</td>
                        <td>$service_class</td>
                        <td>{$v['service_time']}</td>
                        <td>$appoint</td>
                      
                        <td> <button class=\"btn btn-info\" onclick=\"getActiveOption('{$v['id']}','{$type}','{$v['title']}','{$v['active_start']}','{$v['active_end']}')\" style=\"\">关联</button></td>
                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            <div class=\"layui-card-body \">
                    <div class=\"page\">
                        <div>
                            {$page}
                        </div>
                    </div>
                </div>
            </div>";

            }else if($type == 2){
                #关联活动
                $str=" 
                   <div class=\"row\" style=\"width: 100%;float: left\">
                   <input type='hidden'id='activeType' value='{$type}'>
                                                                    <div class=\"col-sm-6\">
                                                                        <div class=\"form-group\">
                                                                            <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchActiveName()\" placeholder=\"请输入活动名称，回车键搜索\" autocomplete=\"off\">
                                                                            <i class=\"form-group__bar\"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
 
                   <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                               
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>活动名称</th>
                                                <th>活动原价</th>
                                                <th>活动价格</th>
                                                <th>上架时间</th>
                                                <th>下架时间</th>
                                          
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                foreach($arr as $k=>$v){

                    $str.=" <td>{$v['title']}</td>
                        <td>" . getsPriceformat($v['active_biz']) . "</td>
                        <td>" . getsPriceformat($v['active_online']) . "</td>
                        <td>{$v['active_start']}</td>
                        <td>{$v['active_end']}</td>
                      
                        <td> <button class=\"btn btn-info\" onclick=\"getActiveOption('{$v['id']}','{$type}','{$v['title']}','{$v['active_start']}','{$v['active_end']}')\" style=\"\">关联</button></td>
                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            <div class=\"layui-card-body \">
                    <div class=\"page\">
                        <div>
                            {$page}
                        </div>
                    </div>
                </div>
            </div>";
            }else if($type == 3){
                #办理会员活动
                $str=" 
                   <div class=\"row\" style=\"width: 100%;float: left\">
                   <input type='hidden'id='activeType' value='{$type}'>
                                                                    <div class=\"col-sm-6\">
                                                                        <div class=\"form-group\">
                                                                            <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchActiveName()\" placeholder=\"请输入会员卡名称，回车键搜索\" autocomplete=\"off\">
                                                                            <i class=\"form-group__bar\"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
 
                   <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                               
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>会员卡名称</th>
                                           
                                          
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                foreach($arr as $k=>$v){

                    $str.=" <td>{$v['title']}</td>
                     
                      
                        <td> <button class=\"btn btn-info\" onclick=\"getActiveOption('{$v['id']}','{$type}','{$v['title']}','{$v['active_start']}','{$v['active_end']}')\" style=\"\">关联</button></td>
                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            <div class=\"layui-card-body \">
                    <div class=\"page\">
                        <div>
                            {$page}
                        </div>
                    </div>
                </div>
            </div>";
            }else if($type == 4){
                #二手车

            }else if($type == 5){
                #联盟商家
                $str=" 
                   <div class=\"row\" style=\"width: 100%;float: left\">
                   <input type='hidden'id='activeType' value='{$type}'>
                                                                    <div class=\"col-sm-6\">
                                                                        <div class=\"form-group\">
                                                                            <input type=\"text\" id=\"search_title\" value='{$search_title}'  class=\"form-control\" onkeyup=\"searchActiveName()\" placeholder=\"请输入活动名称，回车键搜索\" autocomplete=\"off\">
                                                                            <i class=\"form-group__bar\"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
 
                   <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                               
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                                <th>商户名称</th>
                                                <th>商户电话</th>
                                                <th>省</th>
                                                <th>市</th>
                                                <th>区</th>
                                                <th>详细地址</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
                foreach($arr as $k=>$v){

                    $str.=" <td>{$v['title']}</td>
                        <td>{$v['tel']}</td>
                        <td>{$v['province']}</td>
                        <td>{$v['city']}</td>
                        <td>{$v['area']}</td>
                        <td>{$v['address']}</td>
                      
                        <td> <button class=\"btn btn-info\" onclick=\"getActiveOption('{$v['id']}','{$type}','{$v['title']}','','')\" style=\"\">关联</button></td>
                    </tr>";
                }
                $str.="
                </tbody>
            </table>
        </div>
            <div class=\"layui-card-body \">
                    <div class=\"page\">
                        <div>
                            {$page}
                        </div>
                    </div>
                </div>
            </div>";
            }

        }


        return $str;

    }
    /**
     * [getActiveOptionInfo 关联展示样式 活动 推荐商户  推荐而手册]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function getActiveOptionInfo($arr,$type){
        $_str = '';
        if (!empty($arr) and is_array($arr)) {
            $_str .= "<tr>
                            <td >" . $arr['title'] . "</td>
                            <td >$type</td>
                     
                        </tr>";
        }


        return $_str;

    }


}