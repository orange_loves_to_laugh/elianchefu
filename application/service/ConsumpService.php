<?php


namespace app\service;

use think\Db;

/**
 * 用户费用记录服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年11月18日10:29:24
 */
class ConsumpService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年11月18日10:29:24
     * @param    [array]          $params [输入参数]
     */
    public static function  ConsumpListWhere($params = [])
    {
        $where = [];

        //时间段
        if(!empty($params['param']['start_time']))
        {
            $where[] =['consump_time', '>=',  $params["param"]['start_time'] ];
        }
        if(!empty($params['param']['end_time']))
        {
            $where[] =['consump_time', '<=',  $params["param"]['end_time'] ];
        }
        //member_id
        if(!empty($params['member_id']))
        {
            $where[] =['member_id', '=',  $params['member_id'] ];
        }
        //income_type
        if(!empty($params['param']['income_type']))
        {
            $where[] =['income_type', '=',  $params['param']['income_type'] ];
        }

        return $where;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @datetime    2020年11月18日10:29:24
     * @desc    description
     * @param    [array]          $data [处理的数据]
     */
    public static function ConsumpDataDealWith($data){
        if(!empty($data))
        {

            foreach($data as &$v)
            {
                // 收入类型
                if(isset($v['income_type']))
                {
                    $v['income_type_title'] = BaseService::StatusHtml($v['income_type'],lang('member_fee_aim'),false);
                }
                //收益金额
                if(isset($v['consump_price']))
                {
                    $v['consump_price'] = priceFormat($v['consump_price']);
                }
                //充值渠道
                if(isset($v['consump_type']))
                {
                    if($v['consump_type'] == 1){
                        $v['consump_type'] = '线上支付';

                    }else{
                        $v['consump_type'] = '门店支付';
                        #查询门店名称
                        $v['biz_title'] = Db::table('biz')->where(array('id'=>$v['biz_id']))->value('biz_title');
                        if(empty($v['biz_title'])){
                            $v['biz_title'] ='无';
                        }

                    }
                }
                //门店名称
                if(isset($v['biz_id'])){
                    $v['biz_title'] = Db::table('biz')->where(array('id'=>$v['biz_id']))->value('biz_title');
                }
                //支付方式
                if(isset($v['consump_pay_type']))
                {
                    $v['consump_pay_type'] = BaseService::StatusHtml( $v['consump_pay_type'],lang('cash_type'),false);
//                    if($v['consump_pay_type'] == 1){
//                        $v['consump_pay_type'] = '微信';
//
//                    }else if($v['consump_pay_type'] == 2) {
//                        $v['consump_pay_type'] = '支付宝';
//
//                    }else if($v['consump_pay_type'] == 3) {
//                        $v['consump_pay_type'] = '银联';
//
//                    }else if($v['consump_pay_type'] == 4) {
//                        $v['consump_pay_type'] = '现金';
//
//                    }else if($v['consump_pay_type'] == 5) {
//                        $v['consump_pay_type'] = '余额';
//
//                    }else if($v['consump_pay_type'] == 6) {
//                        $v['consump_pay_type'] = '卡券';
//
//                    }else{
//                        $v['consump_pay_type'] = '积分';
//
//                    }
                }


            }
        }

        return $data;
    }
}