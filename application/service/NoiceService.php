<?php


namespace app\service;

use app\admin\controller\Common;
use think\Db;

/**
 * 通知服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月24日13:23:09
 */
class NoiceService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月10日11:58:22
     * @param    [array]          $params [输入参数]
     */
    public static function NoticeListWhere($params = [])
    {

        $where=[['is_del','=',2]];
        //接收人
        if(!empty($params['param']['accepter'])&&intval($params['param']['accepter'])>0)
        {
            $where[] = ['accepter', '=', $params['param']['accepter']];
        }
        //显示状态
        if(!empty($params['param']['show_status'])&&intval($params['param']['show_status'])>0)
        {
            //1=>'未上架', 2=>'已下架',3=>'展示中'
            $time=strtotime(TIMESTAMP);
            switch ($params['param']['show_status'])
            {
                case 1:
                    $where[] = ['start_time', '>', TIMESTAMP];
                    break;
                case 2:
                    $where[] = ['end_time', '<', TIMESTAMP];
                    break;
                default:
                    $where[] = ['start_time', '<', TIMESTAMP];
                    $where[] = ['end_time', '>', TIMESTAMP];
            }

        }
        return $where;

    }

    /**
     * 通知保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function NoticeSave($params = [])
    {
        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'title',
                'error_msg'         => '通知标题不能为空',
                'error_code'         => 50002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'accepter',
                'error_msg'         => '显示端不能为空',
                'error_code'         => 50003,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'start_time',
                'error_msg'         => '起止时间不能为空',
                'error_code'         => 50004,
            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'content',
                'error_msg'         => '内容不能为空',
                'error_code'         => 50005,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=self::HandleData($params);

        // 添加/编辑
        Db::startTrans();
        if(empty($params['id']))
        {

            $id = Db::name('notice')->insertGetId($data);
        } else {


            if(Db::name('notice')->where(['id'=>intval($params['id'])])->update($data))
            {
                $id = $params['id'];
            }
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>4001,'msg'=>'保存通知失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }
    /**
     * 参数处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params = [])
    {
        $data=$params;
        $data['start_time']=explode(' to ',$params['start_time'])[0];
        $data['end_time']=explode(' to ',$params['start_time'])[1];
        //$data['content']= ResourceService::ContentStaticReplace( $data['content'], 'add');

        $params['ueditor_content']='';
        if (isset($params['content'])) {
            $params['ueditor_content'].=$params['content'];
        }

        if (empty($params['ueditor_content'])) {
            return true;
        }

        //删除无用图片
        $params['table']='notice';
        $re=ResourceService::DelRedisOssFile($params);
        if ($re) {
            unset($params['table']);
            unset($params['ueditor_content']);
        }

        return $data;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日11:40:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DataDealWith($data){
        if(!empty($data))
        {
            $re=[];
            foreach($data as &$v)
            {
                //消息接收人 news_accept news_status
                if(isset($v['accepter']))
                {
                    switch ($v['accepter']){
                        case 1:
                            $v['accepter_title']='员工端';
                            break;
                        case 2:
                            $v['accepter_title']='合作端';
                            break;
                        case 3:
                            $v['accepter_title']='直营店';
                            break;
                        case 4:
                            $v['accepter_title']='加盟店';
                            break;
                        case 5:
                            $v['accepter_title']='用户端';
                            break;
                        case 6:
                            $v['accepter_title']='联盟商家';
                            break;
                        case 7:
                            $v['accepter_title']='评估端';
                            break;
                    }
                }

                if(isset($v['start_time'])&&isset($v['end_time']))
                {
                    $v['show_status']='<button type="button" class="btn btn-outline-primary">显示中</button>';

                    if (TIMESTAMP<$v['start_time'])
                    {
                        $v['show_status']='<button type="button" class="btn btn-outline-secondary">待显示</button>';
                    }
                    if (TIMESTAMP>$v['end_time'])
                    {
                        $v['show_status']='<button type="button" class="btn btn-outline-danger">已结束</button>';
                    }
                    $v['show_time']=$v['start_time'].' to '.$v['end_time'];
                }


                // 内容
//                if(isset($v['content']))
//                {
//
//                    $v['content'] = ResourceService::ContentStaticReplace($v['content'], 'get');
//                }

            }
            $re['data']=$data;

        }
//        else{
//            return DataReturn('获取失败',0,$data);
//        }

        return $data;
    }

}