<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/10/22
 * Time: 17:26
 */

namespace app\service;
/**
 * 任务管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */

use app\api\ApiService\SubsidyService;
use app\api\ApiService\VoucherService;
use Redis\Redis;
use think\Db;
use think\db\Expression;
use think\facade\Session;

class TaskService
{

    public static function activetipsIndexCount()
    {
        return (int)Db::name('task')->where('task_status != 3')->count();
    }

    /**
     * [TaskIndex 获取任务管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    function TaskIndex($params)
    {
        #where条件  写成活的
        $where = empty($params['where']) ? ['task_status' != '3'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;

        #应用管理列表信息
        $data = Db::table('task t')
            ->field("t.*,(t.task_ground_num - (select count(id) from member_task lt where lt.task_id = t.id and lt.finish_status=3)) surplus_num")->where($where)
            ->order('t.id desc,t.task_status asc');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);
            $data = $data->toArray()['data'];
        } else {
            $data = $data->select();
        }
        return self::InforDataDealWith($data, $params);
    }

    static function InforDataDealWith($data, $params)
    {
        $time = date("Y-m-d H:i:s");
        if (!empty($data)) {
            $time = date("Y-m-d H:i:s");
            foreach ($data as $k => $v) {
                if ($v['task_status'] == 1) {
                    if ($time < $v['start_time'] and $time < $v['end_time']) {
                        #当前时间 小于 开始时间 和结束时间
                        $data[$k]['taskStatus'] = '待上架';

                    } else if ($time >= $v['start_time'] and $time <= $v['end_time']) {
                        #当前时间 大于 等于开始 下雨等于 结束
                        $data[$k]['taskStatus'] = '进行中';

                    } else if ($time > $v['start_time'] and $time > $v['end_time']) {
                        #当前时间 大于 开始时间 和结束时间
                        $data[$k]['taskStatus'] = '已完成';

                    }
                } else {
                    $data[$k]['taskStatus'] = '已关闭';
                }
            }
        }
        return $data;
    }

    /**
     * [TaskSave 任务添加 修改]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */

    static function TaskSave($params)
    {
        $data = $params;
        // 请求参数
        $p = [
            [
                'checked_type' => 'empty',
                'key_name' => 'task_title',
                'error_msg' => '任务名称不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'task_type',
                'error_msg' => '任务类型不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'task_num',
                'error_msg' => '完成数量不能为空',
                'error_code' => 30002,
            ],
            [
                'checked_type' => 'empty',
                'key_name' => 'reward_type',
                'error_msg' => '奖项类型不能为空',
                'error_code' => 30002,
            ],


        ];

        $ret = ParamsChecked($params, $p);

        if ($ret !== true) {
            $error_arr = explode(',', $ret);


            //throw new BaseException(['code'=>403 ,'errorCode'=>20001,'msg'=>'分类名称不能重复','status'=>false,'data'=>null]);
            return json(DataReturn($error_arr[0], $error_arr[1]));
        }


        // 添加/编辑
        if (empty($params['id'])) {

            #添加处理
            $activeId = Db::table("task")->insertGetId($data);
            if ($activeId) {
                #获取缓存
                $taskPrizeInfo = Session('TaskPrizeInfo');
                if (!empty($taskPrizeInfo)) {
                    #获取镜像  spoil_id：卡券或红包id
                    foreach ($taskPrizeInfo as $k => $v) {
                        $taskPrizeInfo[$k]['task_id'] = $activeId;
                        if (array_key_exists('pro_service_id', $taskPrizeInfo[$k]))
                            $taskPrizeInfo[$k]['spoil_id'] = $taskPrizeInfo[$k]['pro_service_id'];
                        if ($v['prize_type'] == 3) {
                            #商品卡券
                            #类型为卡券时 上 card_voucher  查下卡券id 存上
                            $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 3, 'server_id' => $taskPrizeInfo[$k]['pro_service_id']))->value('id');
                            $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                        }
                        if ($v['prize_type'] == 5) {
                            #服务卡券
                            #类型为卡券时 上 card_voucher  查下卡券id 存上
                            $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 1, 'server_id' => $taskPrizeInfo[$k]['pro_service_id']))->value('id');
                            $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                        }
                        if (isset($v['pro_service_id']))
                            unset($taskPrizeInfo[$k]['pro_service_id']);
                        if ($v['prize_type'] == 4) {
                            #存 redpacket 红包
                            $voucherId = Db::table('redpacket')->insertGetId(array(
                                'price' => $v['intbal'],
                                'packet_title' => $v['prize_title'],

                                'create_time' => date("Y-m-d H:i:s"),
                            ));
                            if ($voucherId) {
                                #task_prize 表 插入红包id
                                $taskPrizeInfo[$k]['spoil_id'] = $voucherId;
                                foreach ($v['redpacket_detail'] as $rk => $rv) {
                                    $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                    $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                                }

                                Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                            }
                        } else if ($v['prize_type'] == 7 or $v['prize_type'] == 8 or $v['prize_type'] == 9) {
                            $giving_id = Db::table("merchants_voucher")->insertGetId(array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                                "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'],"second_cate_pid" => $v['second_cate_pid'], "voucher_start" => $v['voucher_start']));
                            unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['price'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'],
                                $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'],$taskPrizeInfo[$k]['second_cate_pid'], $taskPrizeInfo[$k]['active_types']
                            );
                            $taskPrizeInfo[$k]['spoil_id'] = $giving_id;
                        }
                        unset($taskPrizeInfo[$k]['redpacket_detail']);

                    }
                    #任务奖励 表
                    Db::table('task_prize')->insertAll($taskPrizeInfo);
                }
                #修改 查询出 任务di 对应的 上架数量总和  和剩余数量总和 然后修改
                $task_arr = array();
                $task_arr['task_ground_num'] = Db::table('task_prize')->where(array('task_id' => $activeId))->sum('ground_num');
                $task_arr['task_surplus_num'] = Db::table('task_prize')->where(array('task_id' => $activeId))->sum('surplus_num');
                Db::table('task')->where(array('id' => $activeId))->update($task_arr);


            }
            #添加操作的时候删除活动缓存id  还有修改保存时
            Session::delete('taskId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('TaskPrizeInfo');
            return json(DataReturn('保存成功', 0));


        } else {
            #修改

            Db::table("task")->where(array('id' => $params['id']))->update($data);

            #获取缓存
            $taskPrizeInfo = Session('TaskPrizeInfo');
            if (!empty($taskPrizeInfo)) {
                #获取镜像  spoil_id：卡券或红包id
                foreach ($taskPrizeInfo as $k => $v) {
                    $taskPrizeInfo[$k]['task_id'] = $params['id'];
                    if (isset($v['pro_service_id'])) {
                        $taskPrizeInfo[$k]['spoil_id'] = $taskPrizeInfo[$k]['pro_service_id'];
                        if ($v['prize_type'] == 3) {
                            #商品卡券
                            #类型为卡券时 上 card_voucher  查下卡券id 存上
                            $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 3, 'server_id' => $taskPrizeInfo[$k]['pro_service_id']))->value('id');
                            $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                        }
                        if ($v['prize_type'] == 5) {
                            #服务卡券
                            #类型为卡券时 上 card_voucher  查下卡券id 存上
                            $voucher_id = Db::table('card_voucher')->where(array('card_type_id' => 1, 'server_id' => $taskPrizeInfo[$k]['pro_service_id']))->value('id');
                            $taskPrizeInfo[$k]['spoil_id'] = $voucher_id;
                        }
                        unset($taskPrizeInfo[$k]['pro_service_id']);

                    } else {
                        $taskPrizeInfo[$k]['spoil_id'] = 0;
                    }
                    if ($v['prize_type'] == 4) {
                        #存 redpacket 红包
                        $voucherId = Db::table('redpacket')->insertGetId(array(
                            'price' => $v['intbal'],
                            'packet_title' => $v['prize_title'],

                            'create_time' => date("Y-m-d H:i:s"),
                        ));
                        if ($voucherId) {
                            #task_prize 表 插入红包id
                            $taskPrizeInfo[$k]['spoil_id'] = $voucherId;
                            foreach ($v['redpacket_detail'] as $rk => $rv) {
                                $v['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                                $v['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                            }

                            Db::table('redpacket_detail')->insertAll($v['redpacket_detail']);

                        }
                    } else if ($v['prize_type'] == 7 or $v['prize_type'] == 8 or $v['prize_type'] == 9) {
                        $giving_id = Db::table("merchants_voucher")->insertGetId(array("title" => $v['title'], "price" => $v['price'], "min_consume" => $v['min_consume'],
                            "num" => 1, "validity_day" => $v['validity_day'], "is_type" => $v['is_type'], "cate_pid" => $v['cate_pid'], "voucher_start" => $v['voucher_start']));
                        unset($taskPrizeInfo[$k]['title'], $taskPrizeInfo[$k]['price'], $taskPrizeInfo[$k]['min_consume'], $taskPrizeInfo[$k]['num'], $taskPrizeInfo[$k]['validity_day'],
                            $taskPrizeInfo[$k]['is_type'], $taskPrizeInfo[$k]['voucher_start'], $taskPrizeInfo[$k]['cate_pid'], $taskPrizeInfo[$k]['active_types']
                        );
                        $taskPrizeInfo[$k]['spoil_id'] = $giving_id;
                    }
                    unset($taskPrizeInfo[$k]['redpacket_detail']);
                }
                #任务奖励 表
                Db::table('task_prize')->insertAll($taskPrizeInfo);
            }
            #修改 查询出 任务di 对应的 上架数量总和  和剩余数量总和 然后修改
            $task_arr = array();
            $task_arr['task_ground_num'] = Db::table('task_prize')->where(array('task_id' => $params['id']))->sum('ground_num');
            $task_arr['task_surplus_num'] = Db::table('task_prize')->where(array('task_id' => $params['id']))->sum('surplus_num');
            Db::table('task')->where(array('id' => $params['id']))->update($task_arr);


            #添加操作的时候删除活动缓存id  还有修改保存时
            Session::delete('taskId');
            #删除添加 商品 服务 会员 赠送积分 余额  缓存
            Session::delete('TaskPrizeInfo');
            return json(DataReturn('保存成功', 0));


        }

    }

    /**
     *  taskSession 服务  商品  会员 积分 余额 存session
     * @param    [array]          $params [输入参数]
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     */
    static function taskSession($params, $taskId)
    {
        #类型 积分  余额  还是卡券
        $type = $params['prize_type'];
        if ($type == 'pro') {
            #商品
            #名称
            $info['prize_title'] = $params['prize_title'];
            $info['pro_service_id'] = $params['pro_service_id'];
            #奖励积分 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 3商品
            $info['prize_type'] = 3;
            #奖品数量
            $info['prize_num'] = $params['prize_num'];
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = $params['prize_indate'];
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];


        } else if ($type == 'service') {
            $info['pro_service_id'] = $params['pro_service_id'];

            #服务
            $info['prize_title'] = $params['prize_title'];
            #奖励积分 价值
            $info['intbal'] = $params['detail_price'];
            #奖励类型 5 服务
            $info['prize_type'] = 5;
            #奖品数量
            $info['prize_num'] = $params['prize_num'];
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = $params['prize_indate'];

            #剩余数量
            $info['surplus_num'] = $params['ground_num'];

        } else if ($type == 'packet') {
            #红包
            $info['prize_title'] = $params['packet_title'];
            #奖励积分 价值
            $info['intbal'] = $params['price'];
            #奖励类型
            $info['prize_type'] = 4;
            #奖品数量
            $info['prize_num'] = 1;
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = '365';

            #剩余数量
            $info['surplus_num'] = $params['ground_num'];
            $voucher = session('VoucherInfo');
            if (!empty($voucher)) {
                #存详情字段  创建时 填到redpacket_detail  红包详情表
                $info['redpacket_detail'] = $voucher;
            }

        } else if ($type == 'integral') {
            #积分  $id 是积分
            #名称
            $info['prize_title'] = $params['prize_title'];

            #奖励积分 价值
            $info['intbal'] = $params['intbal'];
            #奖励类型 1 积分
            $info['prize_type'] = 1;
            #奖品数量
            $info['prize_num'] = 1;
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = 365;
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];


        } else if ($type == 'balance') {
            #余额 $id 是传过来的余额
            #名称
            $info['prize_title'] = $params['prize_title'];

            #奖励积分 价值
            $info['intbal'] = $params['intbal'];
            #奖励类型 2 余额
            $info['prize_type'] = 2;
            #奖品数量
            $info['prize_num'] = 1;
            #上架数量
            $info['ground_num'] = $params['ground_num'];
            #有效期
            $info['prize_indate'] = 365;
            #剩余数量
            $info['surplus_num'] = $params['ground_num'];

        } else if ($type == 'merchantVoucher' or $type == 'lifeVoucher' or $type == 'propertyVoucher') {
            if ($params['is_type'] == 1) {
                $params['prize_type'] = 8;
            } elseif ($params['is_type'] == 2) {
                $params['prize_type'] = 7;
            } elseif ($params['is_type'] == 3) {
                $params['prize_type'] = 9;
            }
            $info = $params;
        }

        ###########判断显示活动赠送的 还是活动创建的################
        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有活动id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('TaskPrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        if (!empty($_sessionActiveTypeInfo)) {
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' . $type . $info['pro_service_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' . $type . $info['pro_service_id']]['prize_num'] = intval($_sessionActiveTypeInfo['S' . $type . $info['pro_service_id']]['prize_num']) + intval($info['prize_num']);
            } else {

                $_sessionActiveTypeInfo['S' . $type . $info['pro_service_id']] = $info;
            }
        } else {
            $_sessionActiveTypeInfo['S' . $type . $info['pro_service_id']] = $info;

        }

        Session::set('TaskPrizeInfo', $_sessionActiveTypeInfo);
        $_str = '';
        # 判断是否存在$taskId
        if (!empty($taskId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($taskId);

            $_str = self::getTaskPrizeInfo($pro, 'DB', $type);

        }
        /*getActiveArrInfo*/

        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getTaskPrizeInfo($arr, $mark)
    {

        $_str = '';
        if (!empty($arr) and is_array($arr)) {
            foreach ($arr as $k => $v) {
                if ($v['prize_type'] == 1) {
                    $_active_type_title = "积分";
                    $intbal = $v['intbal'] . '积分';
                } else if ($v['prize_type'] == 2) {
                    $_active_type_title = "余额";
                    $intbal = $v['intbal'] . '余额';


                } else if ($v['prize_type'] == 3) {
                    $_active_type_title = "商品卡券";
                    $intbal = sprintf("%.2f", $v['intbal']);

                } else if ($v['prize_type'] == 4) {
                    $_active_type_title = "红包";
                    $intbal = sprintf("%.2f", $v['intbal']);

                } else if ($v['prize_type'] == 5) {
                    $_active_type_title = "服务卡券";
                    $intbal = sprintf("%.2f", $v['intbal']);

                } else if ($v['prize_type'] == 7) {
                    $_active_type_title = "商家消费券";
                    $intbal = sprintf("%.2f", $v['intbal']);
                } else if ($v['prize_type'] == 8) {
                    $_active_type_title = "有派生活消费券";
                    $intbal = sprintf("%.2f", $v['intbal']);
                }
                $_str .= "<tr id='activeDetailTr" . $v['id'] . "'>
                            <td >" . $v['prize_title'] . "</td>
                            <td >$intbal</td>
                            <td>$_active_type_title</td>
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['prize_num'] . "','" . $mark . "','3','" . $v['prize_type'] . "','')\">" . $v['prize_num'] . "</td>
                             <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['ground_num'] . "','" . $mark . "','4','" . $v['prize_type'] . "','" . $v['surplus_num'] . "')\">" . $v['ground_num'] . "</td>
                            <td onclick=\"taskTypeWrite('" . $v['id'] . "','" . $k . "','" . $v['prize_indate'] . "','" . $mark . "','5','" . $v['prize_type'] . "','')\">" . $v['prize_indate'] . "</td>
                              <td >" . $v['surplus_num'] . "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"delDetailInfo('" . $v['id'] . "','" . $k . "','" . $mark . "','" . $v['prize_type'] . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /**
     * @param $type 1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    static function getTaskTypeList()
    {
        #获取缓存活动id 只有修改时候有
        $activeId = Session('taskId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('TaskPrizeInfo');
        if (empty($_sessionActiveTypeInfo)) {
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在任务id
        if (!empty($activeId)) {
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($activeId);


            $_str = self::getTaskPrizeInfo($pro, 'DB');

        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /**
     * @param $taskId 奖励id
     * @content 任务奖励详情
     */
    function getTaskContent($taskId)
    {
        $task = Db::table('task')->where(array('id' => $taskId))->find();
        if (!empty($task)) {
            if ($task['reward_type'] == 1) {
                #积分
                $task_prize = Db::table('task_prize')->where(array('task_id' => $taskId, 'prize_type' => 1))->select();

            } else if ($task['reward_type'] == 2) {
                #余额
                $task_prize = Db::table('task_prize')->where(array('task_id' => $taskId, 'prize_type' => 2))->select();

            } else if ($task['reward_type'] == 3) {
                #卡券
                $task_prize = Db::table('task_prize')->where("task_id = $taskId and (prize_type = 3 or prize_type = 5 or prize_type = 7 or prize_type = 8)")->select();

            } else if ($task['reward_type'] == 4) {
                #红包
                $task_prize = Db::table('task_prize')->where(array('task_id' => $taskId, 'prize_type' => 4))->select();

            }
            foreach ($task_prize as $k => $v) {
                if ($v['prize_type'] == 1) {
                    #积分
                } else if ($v['prize_type'] == 2) {
                    #余额
                } else if ($v['prize_type'] == 3) {
                    #商品 名称
                    $task_prize[$k]['prize_title'] = ActiveService::getCardVoucherProServiceTitle($v['spoil_id'], 'pro');
                } else if ($v['prize_type'] == 4) {
                    #红包 名称
                    $title = Db::table('redpacket')->field('packet_title')->where(array('id' => $v['spoil_id']))->find();
                    $task_prize[$k]['prize_title'] = $title['packet_title'];

                } else if ($v['prize_type'] == 7 or $v['prize_type'] == 8) {
                    $task_prize[$k]['prize_title'] = ActiveService::getCardVoucherProServiceTitle($v['spoil_id'], 'merchantVoucher');
                } else {
                    #服务 名称
                    $task_prize[$k]['prize_title'] = ActiveService::getCardVoucherProServiceTitle($v['spoil_id'], 'service');
                }

            }

        }
        return $task_prize;

    }


    /***********************抵用卡券显示***************************/
    function voucherSession($params)
    {
        #类型 积分  余额  还是卡券
        $type = $params['voucher_type'];
        $info = $params;
        unset($info['session_tag'],$info['days_id']);
        //dump($info);exit;

        ###########判断显示活动赠送的 还是活动创建的################
        $session_name = isset($params['session_tag']) ? $params['session_tag'] : 'VoucherInfo';
        #获取缓存
        $_sessionActiveTypeInfo = Session($session_name);
        // dump($_sessionActiveTypeInfo);exit;
        if (empty($_sessionActiveTypeInfo)) {
            Session::set($session_name, array($info));
            $_sessionActiveTypeInfo = Session($session_name);
        } else {
            array_push($_sessionActiveTypeInfo, $info);
            Session::set($session_name, $_sessionActiveTypeInfo);
        }

        /*if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        if(!empty($_sessionActiveTypeInfo)){
            # 判断是否添加过相同的商品,存在,直接加上添加的商品数量
            if (array_key_exists('S' .$type. $info['voucher_id'], $_sessionActiveTypeInfo)) {
                $_sessionActiveTypeInfo['S' .$type. $info['voucher_id']]['voucher_number'] = intval($_sessionActiveTypeInfo['S' .$type. $info['voucher_id']]['voucher_number']) + intval($info['voucher_number']);
            } else {

                $_sessionActiveTypeInfo['S' . $type.$info['voucher_id']] = $info;
            }
        }
        else{
            $_sessionActiveTypeInfo['S' .$type. $info['voucher_id']] = $info;

        }*/
        //Session::set($session_name, null);
//        Session::set($session_name, $_sessionActiveTypeInfo);
        $_str = '';
        #packetId 关联红包详情的id
        $packetId = Session('packetId');
        if (!empty($packetId)) {
            $packet = PacketService::getPacketContent($packetId);
            $_str .= self::getVoucherArrInfo($packet, 'DB');
        }

        $_str .= self::getVoucherArrInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }

    /*
      * @content 获取红包详情
      * @params redpacket_title_id 红包名称id  也是 红包详情中的repacket_id
      * @params k  缓存键值
      * @params mark  标识  DB:数据库  S : 缓存
      * */
    static function getVoucherSessionList($params)
    {
        #这个$type 代表红包类型（redpacket_detail表 中） 1 普通红包上传 时 redpacket_id 是 redpacket_title  的id 2 其他红包上传 redpacket_id  是repacket 的id
        $type = 2;
        if (!empty($params['redpacket_title_id'])) {
            #存红包名称的id(redpacket_title 表中的 或者redpacket 表)
            session::set('packetId', $params['redpacket_title_id']);
            #证明是 红包名称
            $type = 1;
        }
        if (isset($params['k'])) {
            #获取红包名称缓存
            $redpacketTitlePrizeInfo = session('redpacketTitlePrizeInfo');
            if (!empty($redpacketTitlePrizeInfo)) {
                #获取红包详情缓存
                $_sessionActiveTypeInfo = $redpacketTitlePrizeInfo[$params['k']]['redpacket_detail'];
                if (empty($_sessionActiveTypeInfo)) {
                    $_sessionActiveTypeInfo = array();
                }

            }

        }
        $_str = '';
        #redpacket_title_id 关联红包详情中 repacket_id
        $packetId = session('packetId');
        if (!empty($packetId)) {
            #获取红包详情的所有信息
            $packet = PacketService::getPacketContent($packetId, $type);
            $_str .= self::getVoucherArrInfo($packet, 'DB');
        }

        $_str .= self::getVoucherArrInfo($_sessionActiveTypeInfo, 'S');
        return $_str;
    }

    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    static function getVoucherArrInfo($arr, $mark)
    {
        $_str = '';
        if (!empty($arr) and is_array($arr)) {

            foreach ($arr as $k => $v) {
                if ($v['voucher_type'] == 1) {
                    $_title = '商品通用抵用券';
                } else if ($v['voucher_type'] == 3) {
                    $_title = "单独商品抵用券";
                } else if ($v['voucher_type'] == 2) {
                    $_title = "服务通用抵用券";


                } else if ($v['voucher_type'] == 4) {
                    $_title = "单独服务抵用券";

                } else if ($v['voucher_type'] == 5) {
                    $_title = "商品服务通用券";

                } else if ($v['voucher_type'] == 6) {
                    $_title = "现金低值通用券";
                    $intbal = sprintf("%.2f", $v['intbal']);

                } else if (in_array($v['voucher_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                    $_title = "商家消费券";
                    $_title =   givingTypeTitle($v['voucher_type']);
                } else if ($v['voucher_type'] == 17) {
                    $_title = "会员抵值券";

                }
                if ($v['voucher_start'] == 1) {
                    $voucher_start = '是';

                } else {
                    $voucher_start = '否';
                }


                $_str .= "<tr id='voucherDetailTr" . $v['id'] . "' class='voucherDetailTrClass" . $mark . $k . "'>
                            <td >" . $v['voucher_title'] . "</td>
                            <td >$_title</td>
                            <td >" . sprintf("%.2f", $v['voucher_price']) . "</td>
                            <td >" . sprintf("%.2f", $v['money_off']) . "</td>
                            <td onclick=\"voucherWrite('" . $k . "','" . $v['voucher_over'] . "','5','" . $mark . "','" . $v['id'] . "')\">" . $v['voucher_over'] . "</td>
                            <td  onclick=\"voucherWrite('" . $k . "','" . $v['voucher_number'] . "','6','" . $mark . "','" . $v['id'] . "')\">" . $v['voucher_number'] . "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"voucherDelInfo('" . $k . "','" . $mark . "','" . $v['voucher_type'] . "','" . $v['id'] . "')\">删除</button>
                            </td>
                        </tr>";
            }
        }
        return $_str;

    }

    /**
     * @param $member_id
     * @return array|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取用户分享任务列表
     */
    static function shareTask($member_id){
        if(!empty($member_id)){
            # 查询用户是否是2021-10-01日之后注册
            $memberInfo=Db::table("member")->field("id")->where(array("id"=>$member_id))->where("register_time >='2021-10-01'")->find();
            if(!empty($memberInfo)){
                a: $list=Db::table("member_sharetask")->where(array("member_id"=>$member_id))->order("num asc")->select();
                if(empty($list)){
                    self::addShareTask($member_id);
                    goto a;
                }
                if(!empty($list)){
                    $break = true; // 全部已完成 已领取状态
                    foreach($list as $k=>$v){
                        if($v['status']==1 or $v['receive_status']==1){
                            $break = false;
                            break;
                        }
                    }
                    if($break){
                        return array("status"=>false);
                    }
                    $length = count($list);
                    $base_length_arr = [0,65,55,40,35,30];
                    $_base_num = 0;
                    $_base_length = $base_length_arr[$length];
                    $money_off = self::moneyOffList();
                    foreach($list as $k=>$v){
                        $plot = intval(($v['num'] - $_base_num)*$_base_length);
                        $list[$k]['plot'] = $plot;
                        $list[$k]['price'] = intval($v['price']);
                        $list[$k]['money_off'] = $money_off[$list[$k]['price']];
                        $_base_num = $v['num'];
                    }
                    return array("status"=>true,"list"=>$list);
                }else{
                    return array("status"=>false);
                }
            }else{
                return array("status"=>false);
            }

        }else{
            return array("status"=>false);
        }
    }

    /**
     * @param $member_id
     * @return bool
     * @context 添加用户分享任务
     */
    static function addShareTask($member_id){
        $list = self::shareTaskColl();
        $_array = array();
        foreach($list as $k=>$v){
            array_push($_array,array("member_id"=>$member_id,"num"=>$v['num'],"price"=>$v['price'],"status"=>1,
                "receive_status"=>1));
        }
        if(!empty($_array)){
            Db::table("member_sharetask")->insertAll($_array);
        }
        return true;

    }

    /**
     * @return \int[][]
     * @context 分享任务
     */
    static function shareTaskColl(){
        return array(
            array("num"=>3,"price"=>30,"money_off"=>68),
            array("num"=>5,"price"=>20,"money_off"=>88),
            array("num"=>10,"price"=>38,"money_off"=>128),
        );
    }
    static function moneyOffList(){
        return array("20"=>68,"30"=>88,"38"=>128);
    }

    static function finishShareTask($member_id){
        # 查询该用户推荐人是否是用户
        $pInfo = Db::table("channel c")
            ->field("c.pid")
            ->where(array("c.member_id"=>$member_id))
            ->where("c.channel=2 or c.channel=5")
            ->find();
        if(!empty($pInfo)){
            # 查询该推荐人一共推荐了多少用户
            $num = Db::table("channel c")
                ->join(array("member"=>'m'),"m.id=c.member_id and date(m.register_time)>='2022-02-23'")
                ->where(array("c.pid"=>$pInfo['pid']))
                ->where("c.channel=2 or c.channel=5")
                ->count("distinct c.member_id");
            # 查询是否有小于这个数的推荐任务
            $list =  Db::table("member_sharetask")->where(array("member_id"=>$pInfo['pid'],"status"=>1))
                ->where("num<={$num}")->select();
            if(!empty($list)){
                foreach($list as $v){
                    Db::table("member_sharetask")->where(array("id"=>$v['id']))->update(array("status"=>2));
                }
            }
        }
        return true;
    }

    /**
     * @param $member_id
     * @param $id
     * @return bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 领取任务奖励
     */
    static function receivePrize($member_id,$id){
        $_redis = new Redis();
        $is_repeat = $_redis->lock("receivePrize".$member_id,2);
        if($is_repeat){
            # 查询是否已经领过了
            $info = Db::table("member_sharetask")->where(array("id"=>$id))->find();
            if($info['receive_status']==1){
                $voucher_arr = self::moneyOffList();
                $price = intval($info['price']);
                # 可领取状态
                $_voucher = array(array("voucher_id" => 0, "member_id" => $member_id, "card_time" =>60,
                    "create" => date("Y-m-d H:i:s", strtotime("+60 days")),
                    "voucher_source" => 19, "paytype" => 0, "voucher_cost" => $price, "card_title" => '商家消费通用券',
                    "voucher_type" => 7, "money_off" =>$voucher_arr[$price], "cate_pid" => 0,
                    "is_type"=>2,
                ));
                $voucherService = new VoucherService();
                $voucherService->addMemberVoucher($member_id, $_voucher);
                Db::table("member_sharetask")->where(array("id"=>$id))->update(array("receive_status"=>2));
            }
        }
        return array("status"=>true);
    }

    /**
     * @param $biz_id
     * @param $type
     * @return bool
     * @context 合作端完成当日任务
     */
    static function finishDayTask($biz_id,$type){
        # 查询是否有今日任务
        $info = Db::table("biz_daytask")->where(array("biz_id"=>$biz_id,"task_type"=>$type,"task_status"=>1))->where("date(create_time)='".date("Y-m-d")."'")->find();
        if(!empty($info)){
            $finishStatus = false;
            if($info['task_plan']+1>=$info['task_amount']){
                $_array =array("task_plan"=>$info['task_plan']+1,"task_status"=>2);
                $finishStatus = true;
            }else{
                $_array =array("task_plan"=>$info['task_plan']+1);
            }
            Db::table('biz_daytask')->where(array("id"=>$info['id']))->update($_array);
            if($info['task_status']==1 and $finishStatus){
                # 领取奖励并添加明日任务
                self::receivePrice($biz_id);
            }
        }
        return true;
    }

    /**
     * @param $biz_id
     * @return bool
     * @context 合作端领取任务奖励
     */
    static function receivePrice($biz_id){
        $price = 13.8;
        Db::table('biz')->where(array('id' => $biz_id))->setInc('account_balance', $price);
        SubsidyService::addSubsidy(array("subsidy_type" => 17, "subsidy_number" => $price,'member_id'=>0, "biz_id" => $biz_id,'pid'=>0));
        return true;
    }


}
