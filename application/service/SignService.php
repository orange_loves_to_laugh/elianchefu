<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/10/28
 * Time: 10:15
 */

namespace app\service;
/**
 * 签到管理服务层
 * @author   lc
 * @version  1.0.0
 * @datetime 2020年9月22日13:23:09
 */
use think\Db;
use think\db\Expression;
use think\facade\Session;

class SignService
{
    /**
     * [TaskIndex 获取任务管理列表信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function SignIndex($params){
        #where条件  写成活的
//        $where = empty($params['where']) ? ['task_status'!='3'] : $params['where'];

        #是否分页
        $page = $params['page'] ? true : false;
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;

        #应用管理列表信息
        $data = Db::table('sign ');

        //分页
        if ($page == true) {
            $data = $data->paginate($number, false, ['query' => request()->param()]);

        } else {
            $data = $data->select();
        }
        #跟奖励类型 查询奖励数量
        foreach($data as $k=>$v){
//            $data[$k]['countNum'] = Db::table('signs')->where(array('days_id'=>$v['id'],'signs_type'=>$v['reward_type']))->count('id');
            $data[$k]['countNum'] ='查看';
        }
        return $data;



    }

    /**
     *  signSession 服务  商品  会员 积分 余额 存session
     * @author  LC
     * @datetime    2020年10月9日10:44:43
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    static function signSession($params){

        #类型 积分  余额  还是卡券
        $type = $params['signs_type'];
        $daysId = $params['days_id'];
        if(empty($daysId))
            $daysId = Session('daysId');
        if($type == 'pro'){
            #商品
            #名称
            $info['signs_title']=$params['signs_title'];
            $info['spoil_id']=$params['pro_service_id'];
            #奖励积分 价值
            $info['intebal_num']=$params['detail_price'];
            #奖励类型 3商品
            $info['signs_type']=3;
            $info['voucher_type']=1;
            #奖品数量
            $info['signs_number']=$params['signs_number'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=$params['prize_indate'];
            #剩余数量
            $info['surplus_num']=$params['ground_num'];


        }else if($type == 'service'){
            $info['spoil_id']=$params['pro_service_id'];

            #服务
            $info['signs_title']=$params['signs_title'];
            #奖励积分 价值
            $info['intebal_num']=$params['detail_price'];
            #奖励类型 5 服务
            $info['signs_type']=3;
            $info['voucher_type']=2;
            #奖品数量
            $info['signs_number']=$params['signs_number'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=$params['prize_indate'];

            #剩余数量
            $info['surplus_num']=$params['ground_num'];

        }else if($type == 'redPacket'){
            #红包
            $info['signs_title']=$params['signs_title'];
            #奖励积分 价值
            $info['intebal_num']=$params['price'];
            #奖励类型 4红包
            $info['signs_type']=4;
            #奖品数量
            $info['signs_number']=1;
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']='365';

            #剩余数量
            $info['surplus_num']=$params['ground_num'];
            $voucher = session('VoucherInfo');
            if(!empty($voucher)){
                #存详情字段  创建时 填到redpacket_detail  红包详情表
                $info['redpacket_detail'] = $voucher;
            }

        }else if($type == 'integral'){
            #积分  $id 是积分
            #名称
            $info['signs_title']='签到奖励积分';

            #奖励积分 价值
            $info['intebal_num']=$params['intebal_num'];
            #奖励类型 1 积分
            $info['signs_type']=1;
            #奖品数量
            $info['signs_number']=$params['signs_number'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=365;
            #剩余数量
            $info['surplus_num']=$params['ground_num'];


        }else if($type == 'balance'){
            #余额 $id 是传过来的余额
            #名称
            $info['signs_title']='签到奖励余额';

            #奖励积分 价值
            $info['intebal_num']=$params['intebal_num'];
            #奖励类型 2 余额
            $info['signs_type']=2;
            #奖品数量
            $info['signs_number']=$params['signs_number'];
            #上架数量
            $info['ground_num']=$params['ground_num'];
            #有效期
            $info['prize_indate']=365;
            #剩余数量
            $info['surplus_num']=$params['ground_num'];

        }else if($type=='merchantVoucher' or $type=='lifeVoucher' or $type=="propertyVoucher"){
            $giving_id = Db::table("merchants_voucher")->insertGetId(array("title"=>$params['title'],"price"=>$params['price'],"min_consume"=>$params['min_consume'],
                "num"=>1,"validity_day"=>$params['validity_day'],"is_type"=>$params['is_type'],"cate_pid"=>$params['cate_pid'],"voucher_start"=>$params['voucher_start']));
            $sign_types=$params['is_type']==2 ? 7 : $params['is_type']==3 ? 9 : 8 ;
            unset($params['title'],$params['price'],$params['min_consume'],$params['num'],$params['validity_day'],
                $params['is_type'],$params['voucher_start'],$params['cate_pid'],$params['active_types']
            );
            $info=$params;
            $info['signs_type']=$sign_types;
            $info['spoil_id']=$giving_id;
        }
        $info['days_id']= $daysId;
        #直接获取所需要的数组 插入到sigs 表中


        if($info['signs_type'] == 4){
            #存 redpacket 红包
            $voucherId = Db::table('redpacket')->insertGetId(array(
                'price'=>$info['intebal_num'],
                'packet_title'=>$info['signs_title'],

                'create_time'=>date("Y-m-d H:i:s"),
            ));
            if($voucherId){
                #task_prize 表 插入红包id
                $info['spoil_id']= $voucherId;
                foreach($info['redpacket_detail'] as $rk=>$rv){
                    if(isset($rv['is_type'])){
                        $giving_id = Db::table("merchants_voucher")->insertGetId(array("title"=>$rv['title'],"price"=>$rv['price'],"min_consume"=>$rv['min_consume'],
                            "num"=>1,"validity_day"=>$rv['validity_day'],"is_type"=>$rv['is_type'],"cate_pid"=>$rv['cate_pid'],"voucher_start"=>$rv['voucher_start']));

                        unset($info['redpacket_detail'][$rk]['title'],$info['redpacket_detail'][$rk]['price'],$info['redpacket_detail'][$rk]['min_consume'],$info['redpacket_detail'][$rk]['num'],$info['redpacket_detail'][$rk]['validity_day'],
                            $info['redpacket_detail'][$rk]['is_type'],$info['redpacket_detail'][$rk]['cate_pid'],$info['redpacket_detail'][$rk]['active_types']
                        );
                        $info['redpacket_detail'][$rk]['voucher_id'] = $giving_id;
                    }
                    $info['redpacket_detail'][$rk]['redpacket_id'] = $voucherId;
                    $info['redpacket_detail'][$rk]['voucher_create'] = date("Y-m-d H:i:s");
                    ksort($info['redpacket_detail'][$rk]);
                }
                Db::table('redpacket_detail')->insertAll($info['redpacket_detail']);

            }
        }
        unset($info['redpacket_detail']);
        Db::table("sign")->where(array('id'=>$info['days_id']))->update(array('reward_type'=>$info['signs_type']));
        #删除 这对应天数之前的奖励
        Db::table("signs")->where(array('days_id'=>$info['days_id']))->delete();
        Db::table('signs')->insert($info);




        return json(DataReturn('保存成功', 0));

    }
    /**
     * @param $daysId 签到天数id
     * @content 签到详情
     */
    #查询signs 信息
    function getSignContent($daysId){
        $sign = Db::table('sign')->where(array('id'=>$daysId))->find();
        if(!empty($sign)){
            #奖项类型 1 积分 2 余额 3 卡券  4 红包
             $signs = Db::table('signs')->where(array('days_id'=>$daysId,'signs_type'=>$sign['reward_type']))->select();

            foreach ($signs as $k=>$v) {
                if($v['signs_type'] == 3){
                    if($v['voucher_type'] == 1){
                        #商品 名称
                        $title = Db::table('biz_pro')->field('biz_pro_title')->where(array('id'=>$v['spoil_id']))->find();
                        $signs[$k]['signs_title'] = $title['biz_pro_title'];
                    }else{
                        #服务 名称
                        $title= Db::table('service')->field('service_title')->where(array('id'=>$v['spoil_id']))->find();
                        $signs[$k]['signs_title'] = $title['service_title'];
                    }

                }else if($v['signs_type'] == 4){
                    #红包 名称
                    $title= Db::table('redpacket')->field('packet_title')->where(array('id'=>$v['spoil_id']))->find();
                    $signs[$k]['signs_title'] = $title['packet_title'];

                }else if($v['signs_type']==7 or $v['signs_type']==8){
                    $title=ActiveService::getCardVoucherProServiceTitle($v['spoil_id'],"merchantVoucher");
                    $signs[$k]['signs_title'] = $title;
                }

            }

        }

        $signs= self::getSignPrizeInfo($signs, 'DB');

        return $signs;

    }
    /**
     * @param $arr
     * @param $mark
     * @param $type  商品 服务 ， 会员
     * @return string
     * @content 商品 服务  积分 卡券   红包 信息整理
     */
    function getSignPrizeInfo($arr,$mark){

        $_str = '';
        if (!empty($arr) and is_array($arr)) {
            $_str=" <div class=\"card-body\">
                                        <div class=\"col-sm-12\">
                                      
                                   
                                    <table class=\"table table-hover mb-0\">
                                        <thead>
                                            <tr>
                                              
                                                <th>奖励名称</th>
                                                <th>奖励类型</th>
                                                <th>价值</th>
                                                <th>奖励数量</th>
                                                <th>剩余数量</th>
                                                <th>有效期</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody id='storebody'>";
            foreach ($arr as $k => $v) {
                if ($v['signs_type'] == 1) {
                    $_active_type_title = '积分';
                    $intbal = $v['intebal_num'].'积分';
                } else if($v['signs_type'] == 2){
                    $_active_type_title = '余额';
                    $intbal = $v['intebal_num'].'余额';

                }else if($v['signs_type'] == 3) {
                    if($v['voucher_type'] == 1){
                        #商品
                        $_active_type_title= "商品卡券";
                        $intbal = sprintf("%.2f",  $v['intebal_num']);
                    }else{
                        #服务
                        $_active_type_title= "服务卡券";
                        $intbal = sprintf("%.2f",  $v['intebal_num']);
                    }


                }else if($v['signs_type'] == 4) {
                    $_active_type_title= "红包";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);

                }else if($v['signs_type'] == 7){
                    $_active_type_title= "商家消费券";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);
                }else if($v['signs_type'] == 8){
                    $_active_type_title= "有派生活消费券";
                    $intbal =sprintf("%.2f",  $v['intebal_num']);
                }




                $_str .= "<tr id='signDetailTr" . $v['id'] . "'>
                            <td >" . $v['signs_title'] . "</td>
                            <td>$_active_type_title</td>
                            <td >$intbal</td>

                            <td onclick=\"signTypeWrite('" . $v['id'] . "','" . $v['days_id'] . "','" . $v['signs_number'] . "','" .$mark . "','4','" . $v['signs_type']. "','" . $v['voucher_type']. "')\">" . $v['signs_number'] . "</td>
                          
                             <td >" . $v['surplus_num'] . "</td>

                            <td onclick=\"signTypeWrite('" . $v['id'] . "','" . $v['days_id'] . "','" . $v['prize_indate'] . "','" .$mark . "','6','" . $v['signs_type'] . "','" . $v['signs_type']. "')\">" . $v['prize_indate'] . "</td>

                            <td>
                            <button class=\"layui-btn\" type=\"button\" onclick=\"DelInfo('" . $v['id'] . "','" . $k . "','" . $mark . "')\">删除</button>
                            </td>
                        </tr>";
            }
            $_str.="
                </tbody>
            </table>
        </div>
            </div>";


        }
        return $_str;

    }
    /**
     * @param $type  1 积分 2 余额 3 商品卡券 4 红包 5 服务卡券
     * @return $activeMark type  :  表示创建  give  表示赠送
     * @return string
     * @content 修改 删除 之后 显示的页面（活动类型  活动赠送）
     */
    function getSignTypeList(){
        #获取缓存活动id 只有修改时候有
        $activeId = Session('taskId');

        #数据准备好  开始看缓存是否存在，不存在存  存在 往后插入 相同 加数量  看是否有任务id  有的话 拼接上

        $_sessionActiveTypeInfo = Session('TaskPrizeInfo');
        if(empty($_sessionActiveTypeInfo)){
            $_sessionActiveTypeInfo = array();
        }
        $_str = '';
        # 判断是否存在$activeId
        if(!empty($activeId)){
            #查询 商品 服务会员  统一放在数组里  返回
            $pro = self::getTaskContent($activeId);


            $_str= self::getTaskPrizeInfo($pro, 'DB');

        }

        #type  :  表示创建  give  表示赠送
        $_str .= self::getTaskPrizeInfo($_sessionActiveTypeInfo, 'S');
        return $_str;

    }




}