<?php


namespace app\service;

use Redis\Redis;
use think\Db;
use think\facade\Request;

/**
 * 门店申请服务层
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  1.0.0
 * @datetime 2020年10月9日10:01:25
 */
class BizApplyService
{
    /**
     * 列表条件
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年10月9日10:01:25
     * @param    [array]          $params [输入参数]
     */
    public static function ApplyListWhere($params = [])
    {
        $where = [];


        //申请类型
        if(!empty($params['biz_type'])&&intval($params['biz_type'])>0)
        {
            $where[] =['biz_type', '=', $params["biz_type"]];
        }

        //手机号

        if(!empty($params['phone']))
        {
            $params['phone'] = strim($params['phone']);
            $where[] =['phone', 'like', '%'.$params['phone'].'%'];
        }

        //读取状态
        if(!empty($params['read_status'])&&intval($params['read_status'])>0)
        {
            $where[] = ['read_status', '=', $params['read_status']];
        }

        //时间段：1昨天 2今天 3历史
        if(!empty($params['time_range'])&&intval($params['time_range'])>0)
        {
            if ($params['time_range']==1)
            {
                $where[] = ['create_time', '>', date('Y-m-d 00:00:00', strtotime('-1 day'))];
                $where[] = ['create_time', '<', date('Y-m-d 23:59:59', strtotime('-1 day'))];
            }
            if ($params['time_range']==2)
            {
                $where[] = ['create_time', '>', date('Y-m-d 00:00:00')];
                $where[] = ['create_time', '<', date('Y-m-d 23:59:59',time())];
            }
            if ($params['time_range']==3)
            {

                $where[] = ['create_time', '<', date('Y-m-d 00:00:00', strtotime('-1 day'))];
            }

        }

        //dump($where);exit;
        return $where;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日11:40:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function DataDealWith($data){

        if(!empty($data))
        {
            foreach($data as &$v)
            {
                //读取状态
                if(isset($v['read_status']))
                {
//                    $v['read_status_title']='<button type="button"  class="btn btn-outline-secondary">未读</button>';
//
//                    if (2==$v['read_status'])
//                    {
//                        $v['read_status_title']='<button type="button"  class="btn btn-outline-primary">已读</button>';
//                    }
                    $v['read_status_title']=BaseService::StatusHtml($v['read_status'],lang('read_status'),false);

                }
                //计划规模
                if(isset($v['plan_area']))
                {
                    $v['plan_area_title']=$v['plan_area'].'m²';
                }
                //申请地区
                $v['address_title']=$v['address'];
                if(isset($v['address'])&&isset($v['area'])&&isset($v['city'])&&isset($v['province']))
                {
                    $v['address_title']=$v['province'].$v['city'].$v['area'].$v['address'];
                    $v['address_area']=$v['province'].$v['city'].$v['area'];
                }
                //industry_cate_id
                if(isset($v['industry_cate_id']))
                {
                    $v['industry_cate_title']=Db::name('industry_cate')->where('id='.$v['industry_cate_id'])->value('title');

                }
                //dump($v['province']);exit;
                if($v['biz_id']>0){
                    $bizInfo = Db::table("biz")->field("agent_id,biz_title,service_title,biz_leader,biz_phone,
                    biz_img,biz_permit")->where(array("id"=>$v['biz_id']))->find();
                    if(!empty($bizInfo)){
                        if($bizInfo['agent_id']>0){
                            $v['relation_agent'] =Db::connect("ealspell")->table("community_agent")->where(array("id"=>$bizInfo['agent_id']))
                                ->value("name");
                        }
                        $v['biz_title'] = $bizInfo['biz_title'];
                        $v['service_title'] = $bizInfo['service_title'];
                        $v['biz_leader'] = $bizInfo['biz_leader'];
                        $v['biz_phone'] = $bizInfo['biz_phone'];
                        $v['biz_img'] = $bizInfo['biz_img'];
                        $v['biz_permit'] = $bizInfo['biz_permit'];
                    }

                }
            }

        }

        return $data;
    }

    /**
     * 数据处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月10日11:40:18
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PartnerDataDealWith($data){

        if(!empty($data))
        {
            //dump($data);exit;
            foreach($data as &$v)
            {
                //读取状态
//                if(isset($v['read_status']))
//                {
//                    $v['read_status_title']='<button type="button"  class="btn btn-outline-secondary">未读</button>';
//
//                    if (2==$v['read_status'])
//                    {
//                        $v['read_status_title']='<button type="button"  class="btn btn-outline-primary">已读</button>';
//                    }
//                    $v['read_status_title']=BaseService::StatusHtml($v['read_status'],lang('read_status'));
//                }

                //姓名 所在区域 会员等级 推荐人
                if(isset($v['member_id']))
                {

                    $v['member_title']=MemberService::SelectMemberName(['id'=>$v['member_id']]);

                    $info=Db::name('member')->where('id='.$v['member_id'])->field('member_phone,member_name,member_area,member_city,member_province,member_address')->find();

                    $v['member_address']=$info['member_province'].$info['member_city'].$info['member_area'];
                    $v['member_level']=Db::name('member_mapping')->alias('mm')
                        ->leftJoin(['member_level'=>'ml'],'mm.member_level_id=ml.id')
                        ->where('mm.member_id='.$v['member_id'])
                        ->field('ml.level_title')
                    ->find()['level_title'];
                   // dump($v);exit;
                }
                $v['recommender_title']='无';

                $v['recommender_phone']='无';

                $channel=Db::name('channel')->where('member_id='.$v['member_id'])->order('id desc')->find();

                if (!empty($channel))
                {
                    $v['channel']=$channel['channel'];

                    $v['pid']=$channel['pid'];

                    if ($v['channel']==1)//门店
                    {
                        $info=Db::name('biz')->where('id='.$v['pid'])->find();

                        $v['recommender_title'] =$info['biz_title'];

                        $v['recommender_phone'] =$info['biz_phone'];
                    }

                    if ($v['channel']==4)//员工
                    {
                        $info=Db::name('employee_sal')->where('id='.$v['pid'])->find();

                        $v['recommender_title'] =$info['employee_name'];

                        $v['recommender_phone'] =$info['employee_phone'];
                    }

                    if ($v['channel']==6)//商户
                    {
                        $info= Db::name('merchants')->where('id='.$v['pid'])->find();

                        $v['recommender_title'] =$info['title'];

                        $v['recommender_phone'] =$info['tel'];
                    }
                    if ($v['channel']==7)//自主办理
                    {
                        $v['recommender_title'] =$v['member_title'];

                        $v['recommender_phone'] =$info['member_phone'];
                    }

                    if (in_array($v['channel'],[2,3,5]))//用户
                    {

                        $v['recommender_title']=MemberService::SelectMemberName(['id'=>$v['pid']]);

                        $v['recommender_phone'] =Db::name('member')->where('id='.$v['pid'])->value('member_phone');
                    }

                }

//                if(isset($v['partner_recommender_id'])&&!empty($v['partner_recommender_id']))
//                {
//
//                    $info=Db::name('member')->where('id='.$v['partner_recommender_id'])->field('member_name,member_phone')->find();
//                    $v['recommender_title']=empty($info['member_name'])?$v['recommender_title']:$info['member_name'];
//                    $v['recommender_phone']=empty($info['member_phone'])?$v['recommender_phone']:$info['member_phone'];
//                }
              //dump($v['province']);exit;
            }

        }

        return $data;
    }

    /**
     * 申请保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月30日12:08:31
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ApplyDataSave($params = []){

        // 请求参数
        $p = [
            [
                'checked_type'      => 'empty',
                'key_name'          => 'username',
                'error_msg'         => '申请人姓名不能为空',
                'error_code'         => 70001,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'phone',
                'error_msg'         => '手机号不能为空',
                'error_code'         => 70002,
            ],

            [
                'checked_type'      => 'empty',
                'key_name'          => 'address',
                'error_msg'         => '申请地区不能为空',
                'error_code'         => 70003,
            ],

//            [
//                'checked_type'      => 'min',
//                'checked_data'      =>0,
//                'key_name'          => 'plan_area',
//                'error_msg'         => '门店计划规模必须大于0',
//                'error_code'         => 70004,
//            ],
            [
                'checked_type'      => 'empty',
                'key_name'          => 'remarks',
                'error_msg'         => '请填写备注',
                'error_code'         => 70005,
            ],

        ];

        $ret = ParamsChecked($params, $p);

        if($ret !== true)
        {
            $error_arr=explode(',',$ret);
            throw new \BaseException(['code'=>403 ,'errorCode'=>$error_arr[1],'msg'=>$error_arr[0],'status'=>false,'debug'=>false]);
        }
        $data=$params;

        // 编辑
        Db::startTrans();

        $data['read_status']=2;

       // dump($data);exit;
        if(Db::name('biz_apply')->where(['id'=>intval($params['id'])])->update($data))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>70006,'msg'=>'保存申请失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }

    /**
     * 招募页面数据保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月30日12:08:19
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PageSave($params){
        $data=self::HandleData($params);


        // 添加/编辑
        Db::startTrans();
        if(Db::name('sysset')->where(['id'=>intval($params['id'])])->update($data))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>70005,'msg'=>'保存模板失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }
    /**
     * 招募页面 初始化
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月30日12:08:19
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function ApplyPage($params){

        $biz_type=$params['biz_type'];
        $where=[
            ['tagname','=','recruit_'.$biz_type]
        ];
        $data=Db::name('sysset')->where($where)->find();

        $params['id'] = empty($params['id']) ? '' : $params['id'];
        ResourceService::$session_suffix = 'source_upload' . 'sysset' . $data['id'];
        ResourceService::delUploadCache();
        $data['desc_arr']=explode(',',$data['desc']);
        $data['biz_type']=$biz_type;

        return DataReturn('ok', 0,$data);
    }

    /**
     * 参数处理
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月28日15:37:22
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function HandleData($params = [])
    {
        $data=[];

        //$data['content']= ResourceService::ContentStaticReplace( $data['content'], 'add');

        $nda_thumb = input('desc');

        ResourceService::$session_suffix='source_upload'.'sysset'.$params['id'];

        ResourceService::delCacheItem(BaseService::HandleImg($nda_thumb));

        $data['desc']=$params['desc'];

        return $data;
    }

    /**
     * 合伙人申请保存
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年10月30日12:08:31
     * @desc    description
     * @param    [array]          $params [输入参数]
     */
    public static function PartnerSave($params = []){

        // 请求参数

        $data=$params;

        // 编辑
        Db::startTrans();

        $data['read_status']=2;

        if(Db::name('member_partner')->where(['id'=>intval($params['id'])])->update($data))
        {
            $id = $params['id'];
        }
        if ($id<0)
        {
            Db::rollback();
            throw new \BaseException(['code'=>403 ,'errorCode'=>90004,'msg'=>'保存申请失败','status'=>false,'debug'=>false]);
        }
        Db::commit();
        return DataReturn('保存成功', 0);
    }
}