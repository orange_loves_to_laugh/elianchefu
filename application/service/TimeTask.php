<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/30
 * Time: 10:09
 */

namespace app\service;


use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\orderService;
use app\api\controller\PayMent;
use Redis\Redis;
use think\Db;

class TimeTask
{
    static function settlementTask($bizId)
    {
        $redis = new Redis();
        /* $status = $redis->get('settlementTask'.$bizId);
         if (!empty($status)) {
             return false;
         }
         $redis->set('settlementTask'.$bizId, 'YES');*/
        $status = $redis->lock('settlementTask' . $bizId);
        if ($status) {
//            $time = date('Y-m-d H:i:s', strtotime('-1 day'));
//            if($bizId==692){
//                #诚信店
//                $time = date('Y-m-d 23:59:59', strtotime('-1 day'));
//            }
            # 查询超过24小时未评论的未处理的未结算数据
            $info = Db::table('biz_settlement bs')
                ->field('bs.id,bs.price,bs.biz_id,commission_price,tobe_settled,bs.order_number,o.is_online')
                ->join('orders o', 'o.order_number = bs.order_number', 'left')
                ->where(array('bs.settlement_status' => '1', 'bs.desposit_status' => '1', 'o.evaluation_status' => '1', 'bs.type' => '1', 'bs.mark' => '1'))
//                ->where("o.order_over <= '" . $time . "'")
                ->order('bs.id asc')
                ->select();
            if (!empty($info)) {
                foreach ($info as $k => $v) {
                    # 查询门店抵押金 , 门店历史结算金额
                    $bizInfo = Db::table('biz')->field('settlement_count,security_price,account_balance')->where(array('id' => $v['biz_id']))->find();
                    # 结算信息
                    $settlementInfo = TimeTask::settlementPriceInfo($v['order_number'], $v['biz_id'], $v['is_online']);
                    # 结算金额
                    $price = $settlementInfo['price'];
                    # 抽成金额
                    $commission_price = $settlementInfo['commission_price'];
                    $tobe_settled = $price + $commission_price;
                    if ($bizInfo['security_price'] == 0) {
                        # 加门店余额
                        Db::table('biz')->where(array('id' => $v['biz_id']))->setInc('account_balance', $price);
                        # 结算改为已处理
                        Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array(
                            'desposit_status' => 2, 'settlement_status' => 2, 'price' => $price, 'commission_price' => $commission_price));
                        # 修改合作店抽成(biz_income)
                        Db::table('biz_income')->where(array('pid' => $v['order_number']))->update(array('biz_income' => $commission_price));
                    } else {
                        # 加上本次结算金额后与抵押金作比较
                        $settlement_count = $bizInfo['settlement_count'] + $price;
                        if ($settlement_count <= $bizInfo['security_price']) {
                            Db::table('biz')->where(array('id' => $v['biz_id']))->setInc('settlement_count', $price);
                            # 结算改为已处理
                            Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array(
                                'desposit_status' => 2, 'price' => $price, 'commission_price' => $commission_price));
                        } else {
                            # 大于抵押金,将超过抵押金的部分进行结算()
                            $exceed = $settlement_count - $bizInfo['security_price'];
                            if ($exceed != $price) {
                                # 修改结算金额
                                Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array('price' => $exceed));
                            }
                            # 加门店余额
                            Db::table('biz')->where(array('id' => $v['biz_id']))->setInc('account_balance', $exceed);
                            Db::table('biz')->where(array('id' => $v['biz_id']))->update(array('settlement_count' => $bizInfo['security_price']));
                            # 结算改为已处理
                            Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array(
                                'desposit_status' => 2, 'settlement_status' => 2, 'price' => $price, 'commission_price' => $commission_price));
                            # 修改合作店抽成(biz_income)
                            Db::table('biz_income')->where(array('pid' => $v['order_number']))->update(array('biz_income' => $commission_price));
                        }
                    }
                    # 应结算金额
                    Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array('tobe_settled' => $tobe_settled));
                }
            }
            # 办理会员/充值/升级
            $balance = Db::table('biz_settlement')
                ->field('id,price,biz_id')
                ->where(array('settlement_status' => 1, 'desposit_status' => 1, 'mark' => 1))
                ->where("type != 1")
                ->select();
            if (!empty($balance)) {
                foreach ($balance as $k => $v) {
                    # 加余额
                    Db::table('biz')->where(array('id' => $v['biz_id']))->setInc('account_balance', $v['price']);
                    # 改状态
                    Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array(
                        'desposit_status' => 2, 'settlement_status' => 2));
                }
            }
//            $redis->delete('settlementTask' . $bizId);
            $redis->unlock('settlementTask' . $bizId);
        }
        return array('status' => true);

    }

    /**
     * @param $orderNumber
     * @param $bizId
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content  计算该订单的结算金额
     */
    static function settlementPriceInfo($orderNumber, $bizId, $is_online)
    {
        # 门店信息
//        $bizCommissionInfo = Db::table('biz')
//            ->where(array('id' => $bizId))
//            ->find();
        # 订单抽成比例  线上订单抽成5% 门店订单抽成 10% (诚信洗车场(id: 692 不抽成))
        if ($is_online == 1) {
            # 线上订单
            $drawRatio = 0;
        } else {
            # 门店订单
            $drawRatio = 0.1;
        }
        if ($bizId == 692) {
            $drawRatio = 0;
        }
        # 商品工单
        $orderBiz = Db::table('order_biz ob')
            ->field('ob.id,ob.pro_price,ob.original_pro_price original_price,price_settlement,final_settlement,2 type,custommark,
            (select score from evaluation_score where orderserver_id = ob.id and type=2 limit 1) score,settlement_type,2 relation_eva')
            ->where(array('order_number' => $orderNumber))
            ->buildSql();
        # 查询该订单下的服务工单
        $temporary = Db::table('order_server os')
            ->field('os.id,os.price,os.original_price,price_settlement,final_settlement,1 type,custommark,
            (select score from evaluation_score where orderserver_id = os.id and type=1 limit 1) score,settlement_type,
            if(custommark=1,(select relation_eva from service where id =os.server_id),2) relation_eva')
            ->where(array('order_number' => $orderNumber))
            ->union([$orderBiz])
            ->buildSql();
        # 联合查询
        $orderDetail = Db::table($temporary . 't')->select();
        $price = 0;
        $withdrawal_amount = 0;
        $withdrawal_amount_count = 0;

        foreach ($orderDetail as $k => $v) {
            $settlement_ratio = config('ecarcoo.comment_rate_settlement')['medium'];
            if ($v['settlement_type'] == 2) {
                # 活动结算金额  不走评论
                $settlement_ratio = 1;
            }
            if ($v['type'] == 1) {
                # 服务工单
                # 计算抽成
                if ($v['custommark'] == 1) {
                    # 系统服务
//                    $withdrawal_amount = $v['price_settlement'] * $bizCommissionInfo['extract_platservice'];
                    $withdrawal_amount = $v['price_settlement'] * $drawRatio;
//                    if ($v['relation_eva'] == 1) {
//                        # 关联评价
//                        $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_platservice'] * $settlement_ratio;
//                    } else {
//                        # 不关联评价
//                        $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_platservice'];
//                    }
                    if (false) {//$bizCommissionInfo['relevant_evaluation']==1
                        # 门店设置里面关联评价
//                        $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_platservice'] * $settlement_ratio;
                        $withdrawal_amount_count += $v['price_settlement'] * $drawRatio * $settlement_ratio;
                    } else {
                        # 门店设置  不 关联评价
//                        $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_platservice'];
                        $withdrawal_amount_count += $v['price_settlement'] * $drawRatio;
                    }
                } else {
                    # 自定义服务
//                    $withdrawal_amount = $v['price_settlement'] * $bizCommissionInfo['extract_selfservice'];
                    $withdrawal_amount = $v['price_settlement'] * $drawRatio;
//                    $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_selfservice'];
                    $withdrawal_amount_count += $v['price_settlement'] * $drawRatio;
                }
                $settlement_order = $v['price_settlement'] - $withdrawal_amount;
                # 结算金额
                if ($v['custommark'] == 1) {
                    # 系统服务,最终结算价格
//                    if ($v['relation_eva'] == 1) {
//                        # 该服务 关联用户评价
//                        $final_settlement = $settlement_order * $settlement_ratio;
//                    } else {
//                        # 不关联
//                        $final_settlement = $settlement_order;
//                    }
                    if (false) {//$bizCommissionInfo['relevant_evaluation']==1
                        # 门店设置里面关联评价
                        $final_settlement = $settlement_order * $settlement_ratio;
                    } else {
                        # 门店设置  不 关联评价
                        $final_settlement = $settlement_order;
                    }
                } else {
                    # 自定义服务
                    $final_settlement = $settlement_order;
                }
                Db::table('order_server')->where(array('id' => $v['id']))->update(array('final_settlement' => $final_settlement));
                $price += $final_settlement;
            } else {
                # 商品工单
                # 计算抽成
                if ($v['custommark'] == 1) {
                    # 系统商品
//                    $withdrawal_amount = $v['price_settlement'] * $bizCommissionInfo['extract_platpro'];
                    $withdrawal_amount = $v['price_settlement'] * $drawRatio;
//                    $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_platpro'] * $settlement_ratio;
                    $withdrawal_amount_count += $v['price_settlement'] * $drawRatio * $settlement_ratio;
                } else {
                    # 自定义商品
//                    $withdrawal_amount = $v['price_settlement'] * $bizCommissionInfo['extract_selfpro'];
                    $withdrawal_amount = $v['price_settlement'] * $drawRatio;
//                    $withdrawal_amount_count += $v['price_settlement'] * $bizCommissionInfo['extract_selfpro'];
                    $withdrawal_amount_count += $v['price_settlement'] * $drawRatio;
                }
                $settlement_order = $v['price_settlement'] - $withdrawal_amount;
                # 结算金额
                if ($v['custommark'] == 1) {
                    # 系统商品,最终结算价格
                    $final_settlement = $settlement_order * $settlement_ratio;
                } else {
                    # 自定义商品
                    $final_settlement = $settlement_order;
                }
                Db::table('order_biz')->where(array('id' => $v['id']))->update(array('final_settlement' => $final_settlement));
                $price += $final_settlement;
            }
        }
        return array('price' => $price, 'commission_price' => $withdrawal_amount_count);
    }

    static function clearLevel()
    {
        $redis = new Redis();
        $is_repeat = $redis->lock("clearLevel_mark", 60);
        if ($is_repeat) {
            $info = Db::table('log_handlecard lh')
                ->field('lh.member_id,lh.member_create,mm.member_level_id')
                ->join('member_mapping mm', 'mm.member_id=lh.member_id', 'left')
                ->where("date(lh.member_create) < '2021-11-18' and lh.level_id = 1")
                ->where("(mm.member_balance < 15 and mm.member_level_id = 1) or (mm.member_level_id = 1 and mm.member_expiration < '" . date('Y-m-d') . "')")
                ->select();
            if (!empty($info)) {
                $member_id = implode(',', array_column($info, 'member_id'));
                if (!empty($member_id)) {
                    Db::table('member_mapping')->where('member_id in (' . $member_id . ')')->update(array('member_level_id' => 0));
                    Db::table('aaa')->insert(array(
                        'info' => json_encode($info),
                        'type' => '会员等级修改为0=>' . date('Y-m-d H:i:s')
                    ));
                }
            }
        }
    }

    static function bonusPoints()
    {
        $redis = new Redis();
        //$redis->hDel('extra_giving_integral',date('Y-m'));
//        $redisMark = $redis->hGet('extra_giving_integral', date('Y-m-d'));
//        if (empty($redisMark)) {
        $time = strtotime(date("Y-m-d 23:59:59"))-time();
        $is_repeat = $redis->lock("extra_giving_integral_mark", $time);
        if ($is_repeat) {
            # 查询在指定日期之后办理的初级体验卡会员 , 额外赠送指定月份的奖励积分
            $info = Db::table('log_handlecard')
                ->field('member_id,member_create')
                ->where("date_format(member_create,'%Y-%m-%d') >= '2021-11-18' and level_id = 1
                     and date_format(member_create,'%Y-%m') <= '" . date("Y-m", strtotime('-1 month')) . "'")
                ->where("date_format(member_create,'%d') = '".date("d")."'")
                ->group('member_id')
                ->select();
            if (!empty($info)) {
                # 查询额外赠送积分以及赠送月份
                $levelInfo = Db::table('member_level')->field('extra_giving_integral,extra_integral_time')->where(array('id' => 1))->find();
                if ($levelInfo['extra_integral_time'] > 0 and $levelInfo['extra_giving_integral'] > 0) {
                    $IntegralService = new IntegralService();
                    foreach ($info as $k => $v) {
                        # 查询该用户最后一次赠送积分的记录
                        $logInfo = Db::table('log_integral')
                            ->field('date_format(integral_time,\'%Y-%m\') integral_time')
                            ->where(array('member_id' => $v['member_id'], 'integral_source' => 33))
                            ->order(array('id' => 'desc'))
                            ->find();
                        # 判断当前月份是和否与办理会员日期相同 , 相同不加积分,下个月在加
                        // and (date('d', strtotime($v['member_create'])) <= date('d'))
                        if ((date('Y-m', strtotime($v['member_create'])) < date('Y-m')) and (empty($logInfo) or (!empty($logInfo) and $logInfo['integral_time'] != date('Y-m')))) {
                            # 判断已经赠送几个月的积分
                            $logNum = Db::table('log_integral')
                                ->where(array('member_id' => $v['member_id'], 'integral_source' => 33))
                                ->count();
                            if ($logNum < $levelInfo['extra_integral_time']) {
                                # 赠送积分,加记录
                                Db::table('aaa')->insert(array(
                                    'info' => $levelInfo['extra_giving_integral'],
                                    'type' => $v['member_id'] . '会员额外赠送积分=>' . date('Y-m-d H:i:s')
                                ));
                                $IntegralService->addMemberIntegral($v['member_id'], 33, $levelInfo['extra_giving_integral'], 1);
                            }
                        }
                    }
                }
            }
//            $redis->hSet('extra_giving_integral', date('Y-m'), 1);
            //$redis->unlock('extra_giving_integral_mark');
        }
//        }
    }

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 预约订单超时提醒
     */
    static function reservationExpired()
    {
        $redis = new Redis();
        $requestStatus = $redis->hGet('reservationExpired', 'requestStatus');
        if (!empty($requestStatus)) {
            return false;
        }
        $redis->hSet('reservationExpired', 'requestStatus', 'Running');
        # 查询超时未到店的预约订单
        $orderInfo = Db::table('orders')
            ->field('order_number,biz_pro_id service_id,member_id,order_appoin_time appoint_time,biz_id')
            ->where(array("order_status" => 1, 'order_type' => 3))
            ->where("order_appoin_time <= '" . date('Y-m-d H:i:s') . "'")
            ->select();
        if (!empty($orderInfo)) {
            # 发送消息
            $geTui = new IGeTuiService();
            foreach ($orderInfo as $k => $v) {
                $sendStatus = $redis->hGet('reservationExpired', $v['order_number']);
                if (empty($sendStatus)) {
                    # 没发送过
                    $geTui->MessageNotification($v['member_id'], 2, $v);
                    $redis->hSet('reservationExpired', $v['order_number'], 'alreadySend');
                }
            }
        }
        $redis->hDel('reservationExpired', 'requestStatus');
    }

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 会员卡过期提醒
     */
    static function vipExpired()
    {
        $redis = new Redis();
        $requestStatus = $redis->hGet('vipExpired', 'requestStatus');
        if (!empty($requestStatus)) {
            return false;
        }
        $redis->hSet('vipExpired', 'requestStatus', 'Running');
        $time = date('Y-m-d');
        # 今天是否发送过会员卡过期提醒
        $todayRemind = $redis->hGet('vipExpired', $time);
        if (empty($todayRemind)) {
            # 查询会员卡过期的用户  ->where('equipment_id != ""')
            $member = Db::table('member_mapping mm')
                ->field('mm.id,member_expiration,member_level_id,member_id,member_name,nickname,level_title,member_type,equipment_id')
                ->join('member m', 'm.id=mm.member_id', 'left')
                ->join('member_level ml', 'ml.id=mm.member_level_id', 'left')
                ->where("member_expiration <= '" . $time . "'")
                ->where('member_level_id = 1 or member_level_id = 2')
                ->select();
            if (!empty($member)) {
                $geTui = new IGeTuiService();
                foreach ($member as $k => $v) {
                    if ($v['member_type'] == 2) {
                        Db::table('member_mapping')->where(array('id' => $v['id']))->update(array('member_type' => 1));
                    }
                    # 发送消息提醒状态
                    $sendStatus = $redis->hGet('vipExpiredMember', $v['member_id']);
                    if (empty($sendStatus)) {
                        # 没发送过
                        $geTui->MessageNotification($v['member_id'], 8, array('expiration' => $v['member_expiration'], 'level_title' => $v['level_title']));
                        $redis->hSet('vipExpiredMember', $v['member_id'], 'alreadySend');
                    }
                }
                //, strtotime(date('Y-m-d 23:59:59')) - time()
                $redis->hSet('vipExpired', $time, 'alreadySend');
            }
        }
        $redis->hDel('vipExpired', 'requestStatus');
    }

    /**
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 卡券过期提醒
     */
    static function couponExpired()
    {
        $redis = new Redis();
        $requestStatus = $redis->hGet('couponExpired', 'requestStatus');
        if (!empty($requestStatus)) {
            return false;
        }
        $redis->hSet('couponExpired', 'requestStatus', 'Running');
        $time = date('Y-m-d');
        # 今天是否发送过卡券过期提醒
        $todayRemind = $redis->hGet('couponExpired', $time);
        if (empty($todayRemind)) {
            # 查询卡券即将过期的用户(小于三天)
            $member = Db::table('member_voucher')->field('member_id,`create` time')
                ->where(array('status' => 1))
                ->where("date(`create`) <= '" . date('Y-m-d', strtotime('+3 days')) . "' and date(`create`) >= '" . $time . "'")
                ->group('member_id')
                ->select();
            if (!empty($member)) {
                $geTui = new IGeTuiService();
                foreach ($member as $k => $v) {
                    # 发送消息提醒状态
                    $sendStatus = $redis->hGet('couponExpiredMember', $v['member_id']);
                    if (empty($sendStatus)) {
                        # 没发送过
                        $geTui->MessageNotification($v['member_id'], 11, array('expiration' => $v['time'], 'title' => '服务卡券'));
                        $redis->hSet('couponExpiredMember', $v['member_id'], 'alreadySend', strtotime(date('Y-m-d 23:59:59', strtotime('+ 3 days'))) - time());
                    }
                }
                $redis->hSet('couponExpired', $time, 'alreadySend', strtotime(date('Y-m-d 23:59:59')) - time());
            }
        }
        $redis->hDel('couponExpired', 'requestStatus');
    }

    function newActive()
    {
        $redis = new Redis();
        $requestStatus = $redis->hGet('newActive', 'requestStatus');
        if (!empty($requestStatus)) {
            return false;
        }
        $redis->hSet('newActive', 'requestStatus', 'Running');
        $time = date('Y-m-d');
        # 今天是否发送过活动上新提醒
        $todayRemind = $redis->hGet('newActive', $time);
        if (empty($todayRemind)) {
            # 查询新上的活动
            $active = Db::table('active')->field('active_title')->where("date(active_start) = '" . $time . "'")->select();
            if (!empty($active)) {
                # 发送消息提醒
                $IGeTuiService = new IGeTuiService();
                foreach ($active as $k => $v) {
                    $IGeTuiService->MessageNotification(0, 7, ['title' => $v['active_title']], true, false);
                }
            }
        }
        $redis->hSet('newActive', $time, 'alreadySend', strtotime(date('Y-m-d 23:59:59')) - time());
        $redis->hDel('newActive', 'requestStatus');
    }


    /**
     * @param string $memberId 玩家ID
     * @param int $taskType 任务类型==>
     * '1'=> '分享好友',
     * '2'=> '推荐好友注册',
     * '3'=> '卡券核销',
     * '4'=> '分享二手车',
     * '5'=> '分享商家',
     * '6'=> '分享文章',
     * '7'=> '查看文章',
     * '8'=> '参加活动',
     * '9'=> '服务评论',
     * '10'=> '积分兑换',
     * '11'=> '预约服务',
     * '12'=> '办理会员',
     * '13'=> '会员充值',
     * '14'=> '会员升级',
     * '15'=> '推荐会员',
     * '16'=> '参与摇奖',
     * '17'=> '连续签到',
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 完成任务
     */
    static function finishTask($memberId, $taskType)
    {
        if (!empty($memberId) and !empty($taskType)) {
            # 查询是否存在未完成的该类型的任务
            $taskInfo = Db::table('member_task mt')
                ->field('mt.id,mt.task_plan,mt.task_total,mt.over_time')
                ->leftJoin('task t', 't.id=mt.task_id')
                ->where(array('t.task_type' => $taskType, 'mt.finish_status' => 1, 'mt.member_id' => $memberId))
                ->select();
            //Db::table('aaa')->insert(array('info'=>json_encode($taskInfo)));
            if (!empty($taskInfo)) {
                $redis = new Redis();
                $getui = new IGeTuiService();
                $getuiState = false;
                foreach ($taskInfo as $k => $v) {
                    if (!empty($v['over_time'])) {
                        if ($v['over_time'] < date('Y-m-d H:i:s')) {
                            # 未完成,修改状态
                            Db::table('member_task')->where(array('id' => $v['id']))->update(array('finish_status' => 4));
                        } else {
                            //Db::table('aaa')->insert(array('info'=>'你大爷','type'=>$v['id']));
                            goto finishContinue;
                        }
                    } else {
                        //Db::table('aaa')->insert(array('info'=>'你二大爷','type'=>$v['id']));
                        finishContinue:
                        //Db::table('aaa')->insert(array('info'=>'你三大爷','type'=>$v['id']));
                        $completeNum = $v['task_plan'] + 1;
                        if ($taskType == 17) {
                            # 连续签到
                            $completeNum = $redis->hGet('memberSignNum', $memberId);
                            if (empty($completeNum)) {
                                $completeNum = 0;
                            }
                        }

                        if ($completeNum >= $v['task_total']) {
                            # 已完成,修改状态
                            $getuiState = true;
                            $_arr = array('task_plan' => $completeNum, 'finish_status' => 2, 'finish_time' => date("Y-m-d H:i:s"));
                        } else {
                            $_arr = array('task_plan' => $completeNum);
                        }
                        //Db::table('aaa')->insert(array('info'=>json_encode($_arr),'type'=>$v['id']));
                        Db::table('member_task')->where(array('id' => $v['id']))->update($_arr);
                    }
                }
                if ($getuiState) {
                    # 发送领取任务奖励通知
                    $getui->pushMessageToSingle('任务提醒', '有可领取的任务奖励,立即领取', $memberId);
                }
                return array('status' => true);
            } else {
                return array('status' => false, 'msg' => '用户任务不存在');
            }
        } else {
            return array('status' => false, 'msg' => '参数存在空值');
        }
    }

    static function payRedis()
    {
        $redis = new Redis();
        $is_repeat = $redis->lock("payRedisTimeTask", 60);
        if (!$is_repeat) {
            return true;
        }
        $info = $redis->hGet('payRedis');
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                # 查询支付订单是否支付成功(默认为false , 当支付查询结果为支付时 , 改为true)
                $paySuccess = false;
                # 订单支付相关信息
                $v = json_decode($v, true);
                $payInfo = $v['info'];
                # 判断支付时间 (时间为3分钟后)
                if (!array_key_exists('pay_time', $v)) {
                    $redis->hDel('payRedis', strval($k));
                    Db::table('aaa')->insert(array(
                        'info' => json_encode($v),
                        'type' => 'payRedis-' . date('Y-m-d H:i:s') . '-' . $k
                    ));
                    continue;
                }
                if ($v['pay_time'] < date('Y-m-d H:i:s', strtotime('- 3 minutes'))) {
                    # 查询订单支付状态
                    if (!empty($v['pay_type'])) {
                        $PayMent = new PayMent();
                        $payRes = $PayMent->orderStatusQuery($k, $v['pay_type']);
                        $payResInfo = json_decode($payRes, true);
                    } else {
                        $payResInfo = array();
                    }
                    if ($v['pay_type'] == 1) {
                        # 微信支付-APP
                        if ($payResInfo['return_code'] == 'SUCCESS' && $payResInfo['trade_state'] == 'SUCCESS') {
                            # 支付成功
                            $paySuccess = true;
                        }
                    }
                    if ($v['pay_type'] == 2) {
                        # 支付宝支付-APP
                        if ($payResInfo['code'] == 10000 && $payResInfo['trade_status'] == 'TRADE_SUCCESS') {
                            # 支付成功
                            $paySuccess = true;
                        }
                    }
                    if ($paySuccess) {
                        # 判断订单类型
                        if ($v['type'] == 'Reservation') {
                            # 预约订单
                            if ($payInfo['pay_type'] == 5 and $payInfo['pay_price'] > 0) {
                                $memberService = new \app\api\ApiService\MemberService();
                                $memberInfo = $memberService->MemberInfoCache($payInfo['member_id']);
                                if ($memberInfo['member_balance'] < $payInfo['pay_price']) {
                                    # 清除支付状态缓存
                                    $redis->hDel('payRedis', strval($k));
//                                    return array("status" => false, "msg" => "账户余额不足，请充值后再试");
                                    continue;
                                }
                                $memberInfo['member_balance'] = getsPriceFormat($memberInfo['member_balance'] - $payInfo['pay_price']);
                                $redis->hSetJson("memberInfo", $payInfo['member_id'], $memberInfo);
                            }
                            $orderService = new orderService();
                            $orderService->makeOrderServer($payInfo);
                        } elseif ($v['type'] == 'memberApply' or $v['type'] == 'memberUpgrade' or $v['type'] == 'memberRecharge') {
                            if ($v['type'] != 'memberRecharge') {
                                # 查询用户信息
                                $memberInfo = Db::table('member_mapping')->field('member_level_id,member_expiration')->where(array('member_id' => $payInfo['member_id']))->find();
                                if ($memberInfo['member_level_id'] == $payInfo['up_level']) {
                                    # 清除支付状态缓存
                                   $redis->hDel('payRedis', strval($k));
                                    continue;
//                                return array("status" => true);
                                }
                            }
                            # 支付成功方法要用到的信息
                            if (!empty($v['pay_info'])) {
                                $payArr = json_decode($v['pay_info'], true);
                                $assign_id = $payArr['assign_id'];
                                $employee_id = $payArr['employee_id'];
                                $shareMemberId = $payArr['shareMemberId'];
                                $formMark = $payArr['formMark'];
                            }
                            # 推荐认/门店/信息
                            $recommend = $redis->hGetJson("relation_member", $payInfo['member_id']);
                            if (empty($assign_id) and empty($employee_id) and empty($shareMemberId)) {
                                if (!empty($recommend)) {
                                    //{"share_member_id":"","assignId":"","employeeId":"113","member_id":1733,"formMark":"employeeSal"}
                                    if (array_key_exists('assignId', $recommend) and !empty($recommend['assignId'])) {
                                        $assign_id = $recommend['assignId'];
                                    }
                                    if (array_key_exists('employeeId', $recommend) and !empty($recommend['employeeId'])) {
                                        $employee_id = $recommend['employeeId'];
                                    }
                                    if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                                        $shareMemberId = $recommend['share_member_id'];
                                    }
                                    if (array_key_exists('formMark', $recommend) and !empty($recommend['formMark'])) {
                                        $formMark = $recommend['formMark'];
                                    }
                                }
                            }
                            if ($v['type'] == 'memberApply') {
                                # 会员办理
                                LevelService::applyMember($payInfo['member_id'], $payInfo['up_level'], $payArr['balance'], $payArr['setmeal_id'], array("consump_price" => $payArr['balance'], "pay_type" => $payInfo['pay_type'], "consump_type" => 1,
                                    "order_number" => $k, "biz_id" => 1, 'employee_id' => $employee_id, 'assign_id' => $assign_id, 'shareMemberId' => $shareMemberId, 'formMark' => $formMark), $v['voucherInfo']);
                            }
                            if ($v['type'] == 'memberUpgrade') {
                                # 会员升级
                                $BalanceService = new BalanceService();
                                $BalanceService->memberUpgrade($payInfo['member_id'], $v['pay_price'], $v['pay_type'], 1, 1, $payArr['upgradeId'], $payArr['up_level'], $k, $assign_id, $employee_id, '', $formMark);
                            }
                            if ($v['type'] == 'memberRecharge') {
                                # 会员充值
                                $BalanceService = new BalanceService();
                                $BalanceService->memberRecharge($payInfo['member_id'], $v['pay_price'], $v['pay_type'], 1, $payArr['giving'], 1, $k, $employee_id, $assign_id, $shareMemberId, $formMark);
                            }
                        } elseif ($v['type'] == 'businessOrder') {
                            $params = json_decode($v['info'], true);
                            \app\businessapi\ApiService\OrderService::getSancodeOrder($params);
                            Db::table('aaa')->insert(array('info' => json_encode($v), 'type' => $k . '付款给商家-自动执行' . date('Y-m-d H:i:s')));
                        }
                    }
                    # 清除支付状态缓存
                    $redis->hDel('payRedis', strval($k));
                }
            }
        }
//        $redis->unlock("payRedisTimeTask");
    }

    /**
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 抖音数据
     */
    static function addDouData()
    {
        $redis = new Redis();
        # 当前登陆人数
        $nowNum = $redis->get('loginNum');
        if (empty($nowNum)) {
            $nowNum = 0;
        }
        $nowNum += 1;
        # 查询现有开通功能的商家
        $merchants = Db::table('merchants')
            ->field('id')
            ->where(array('take_video' => 1, 'status' => 1, 'is_test' => 2))
            ->select();
        # 将id 合并到一个数组中
        $idInfo = array_column($merchants, 'id');
        if (!empty($idInfo)) {
            # 要增加数据的商家 , 判断数量是否大于5
            if (count($idInfo) > 5) {
                # 随机取值(所有商家总数的5%-30%)
                $num = ceil(rand(count($idInfo) * 0.05, count($idInfo) * 0.3));
                $idArr_k = array_rand($idInfo, $num);//返回的是键
                # 取值
                $idArr = array();
                foreach ($idArr_k as $idK => $idV) {
                    array_push($idArr, $idInfo[$idV]);
                }
            } else {
                $idArr = $idInfo;
            }
            foreach ($idArr as $k => $v) {
                # 查询今天是否有数据
                $douInfo = Db::table('merchants_dou')->where(array('m_id' => $v))->where("date(add_time) = '" . date('Y-m-d') . "'")->find();
                # 计算要增加的  查看数量
                if (!empty($douInfo)) {
                    # 原有基础上增加1%-5%
                    $play_count = ceil($douInfo['play_count'] * rand(10, 30) / 100);
                } else {
                    # 首次增加多少啊啊?
                    $play_count = rand(50, 100);
                }
                # 点赞量 (查看量的1%-30%随机)
                $like_count = ceil($play_count * rand(1, 30) / 100);
                # 转发量 (点赞量的 1%-15%随机)
                $share_count = ceil($like_count * rand(1, 15) / 100);
                # 分享人数(转发量的 90%-100%随机)
                $share_people = ceil($share_count * rand(90, 100) / 100);
                # 评论量 (点赞量的 1%-10%)
                $comment_count = ceil($play_count * rand(1, 15) / 100);
                if (!empty($douInfo)) {
                    # 不为空,修改
                    Db::table('merchants_dou')->where(array('id' => $douInfo['id']))->update(array(
                        'share_people' => $share_people + $douInfo['share_people'],
                        'share_count' => $share_count + $douInfo['share_count'],
                        'play_count' => $play_count + $douInfo['play_count'],
                        'like_count' => $like_count + $douInfo['like_count'],
                        'comment_count' => $comment_count + $douInfo['comment_count']
                    ));
                } else {
                    # 添加数据
                    Db::table('merchants_dou')->insert(array(
                        'm_id' => $v,
                        'add_time' => date('Y-m-d H:i:s'),
                        'share_people' => $share_people,
                        'share_count' => $share_count,
                        'play_count' => $play_count,
                        'like_count' => $like_count,
                        'comment_count' => $comment_count
                    ));
                }
            }
        }
        if ($nowNum >= 3) {
            # 三个人
            $redis->set('loginNum', 1);
            /* # ①加分享人数(根据平台用户登录数据，每进入3名用户，为现有开通功能的商家中5家商家随机增加一个分享人数)
             if (!empty($merchants)) {
                 # 判断数量是否大于5
                 if (count($idInfo) > 5) {
                     # 随机取值
                     $idArr = array_rand($idInfo, 5);//返回的是键
                     # 取值
                     $idArr = array($idInfo[$idArr[0]], $idInfo[$idArr[1]], $idInfo[$idArr[2]], $idInfo[$idArr[3]], $idInfo[$idArr[4]]);
                 } else {
                     $idArr = $idInfo;
                 }
                 if (!empty($idArr)) {
                     # (第二次以上加数据时,额外加1)
                     $douNum = Db::table('merchants_dou')->where("date(add_time) = '" . date('Y-m-d') . "'")->find();
                     foreach ($idArr as $k => $v) {
                         # 查询今天是否有数据
                         $douInfo = Db::table('merchants_dou')->where(array('m_id' => $v))->where("date(add_time) = '" . date('Y-m-d') . "'")->find();
                         # 分享人数
                         $share_people = 1;
                         # 分享次数随机加1 或2
                         $share_count = 1;
                         if (!empty($douNum)) {
                             # (第二次以上加数据时,额外加1)
                             $share_people += 1;//分享人数
                             $share_count += 1;//分享次数
                         }
                         # 播放量 (分享次数 * 10-20)
                         $play_count = $share_count * rand(10, 30);
                         # 点赞量 (播放量 的 5%-30%)
                         $like_count = ceil($play_count * rand(5, 30) / 100);
                         # 评论量 (点赞量的 3%-10%)
                         $comment_count = ceil($play_count * rand(3, 10) / 100);
                         if (!empty($douInfo)) {
                             # 不为空,修改
                             Db::table('merchants_dou')->where(array('id' => $douInfo['id']))->update(array(
                                 'share_people' => $share_people + $douInfo['share_people'],
                                 'share_count' => $share_count + $douInfo['share_count'],
                                 'play_count' => $play_count + $douInfo['play_count'],
                                 'like_count' => $like_count + $douInfo['like_count'],
                                 'comment_count' => $comment_count + $douInfo['comment_count']
                             ));
                         } else {
                             # 添加数据
                             Db::table('merchants_dou')->insert(array(
                                 'm_id' => $v,
                                 'add_time' => date('Y-m-d H:i:s'),
                                 'share_people' => $share_people,
                                 'share_count' => $share_count,
                                 'play_count' => $play_count,
                                 'like_count' => $like_count,
                                 'comment_count' => $comment_count
                             ));
                         }
                     }
                 }
             }*/
            # 商家随机加点赞查看数量
            $m = Db::table('merchants')
                ->field('id')
                ->where(array('status' => 1, 'is_test' => 2))
                ->select();
            # 将id 合并到一个数组中
            $iInfo = array_column($m, 'id');
            $iArr = array_rand($iInfo, ceil(count($iInfo) * 0.3));// 取出30%的数据
            # 30%的数据的 id
            $iArrReal = array();
            if (!empty($iArr)) {
                foreach ($iArr as $k => $v) {
                    array_push($iArrReal, $iInfo[$v]);
                }
            }
            if (!empty($iArrReal)) {
                $iInfoReal = implode(',', $iArrReal);
                # 点赞数量
                $mLike = rand(1, 2);
                # thumbup_num=>点赞数量 forward_num=>转发量 look_num=>查看数量
                Db::query("update merchants set thumbup_num=thumbup_num+1,forward_num=forward_num+1,look_num=look_num+{$mLike}
                 where id in ({$iInfoReal})");
            }
            $redis->set('loginNum', 0);
        } else {
            $redis->set('loginNum', $nowNum);
        }

        #  开通功能的商家 随机增加 5%-30%播放量
//        if (!empty($idInfo)) {
//            $idArr_play = array_rand($idInfo, ceil(count($idInfo) * 0.3));// 取出30%的数据
//            # 30%的数据的 id
//            $idArr_playReal = array();
//            if (!empty($idArr_play)) {
//                foreach ($idArr_play as $k => $v) {
//                    array_push($idArr_playReal, $idInfo[$v]);
//                }
//            }
//            if (!empty($idArr_playReal)) {
//                $idInfo_play = implode(',', $idArr_playReal);
//                # 5%-30% 随机数
//                $rand_play = rand(3, 7) / 100;
////                $rand_play = rand(1, 5);
////                Db::table('aaa')->insert(array('info'=>$idInfo_play,'type'=>$rand_play));
//                # 修改数据
////                Db::table('merchants_dou')
////                    ->where(" m_id in (" . $idInfo_play . ") and date(add_time) = '" . date('Y-m-d') . "'")
////                    ->setInc('play_count', $rand_play);
//                Db::query("update merchants_dou set play_count = CEILING(play_count * " . $rand_play . ") + play_count
//                where m_id in (" . $idInfo_play . ") and date(add_time) = '" . date('Y-m-d') . "'");
//            }
//        }
        # 每次用户进入 部分文章随机增加 查看/点赞/转发
        $club = Db::table('club')
            ->field('id')
            ->select();
        if (!empty($club)) {
            # 将id 合并到一个数组中
            $clubInfo = array_column($club, 'id');
            $clubArr = array_rand($clubInfo, ceil(count($clubInfo) * 0.3));// 取出30%的数据
            # 30%的数据的 id
            $clubArrReal = array();
            if (!empty($clubArr)) {
                foreach ($clubArr as $k => $v) {
                    array_push($clubArrReal, $clubInfo[$v]);
                }
            }
            if (!empty($clubArrReal)) {
                $clubInfoReal = implode(',', $clubArrReal);
                $clubNum = rand(1, 2);
                Db::query("update club set club_check=club_check+{$clubNum},club_like=club_like+{$clubNum},club_transmit=club_transmit+{$clubNum}
                 where id in ({$clubInfoReal})");
            }
        }
    }


    static function addDouDataNew()
    {
        # 查询现有开通功能的商家
        $merchants = Db::table('merchants')
            ->field('id')
            ->where(array('take_video' => 1, 'status' => 1, 'is_test' => 2))
            ->select();
        # 将id 合并到一个数组中
        $idInfo = array_column($merchants, 'id');
        if (!empty($idInfo)) {
            # 要增加数据的商家 , 判断数量是否大于5
            if (count($idInfo) > 5) {
                # 随机取值(所有商家总数的5%-30%)
                $num = ceil(rand(count($idInfo) * 0.05, count($idInfo) * 0.3));
                $idArr_k = array_rand($idInfo, $num);//返回的是键
                # 取值
                $idArr = array();
                foreach ($idArr_k as $idK => $idV) {
                    array_push($idArr, $idInfo[$idV]);
                }
            } else {
                $idArr = $idInfo;
            }
            foreach ($idArr as $k => $v) {
                # 查询今天是否有数据
                $douInfo = Db::table('merchants_dou')->where(array('m_id' => $v))->where("date(add_time) = '" . date('Y-m-d') . "'")->find();
                # 计算要增加的  查看数量
                if (!empty($douInfo)) {
                    # 原有基础上增加1%-5%
                    $play_count = ceil($douInfo['play_count'] * rand(1, 15) / 100);
                } else {
                    # 首次增加多少啊啊?
                    $play_count = 10;
                }
                # 点赞量 (查看量的1%-30%随机)
                $like_count = ceil($play_count * rand(1, 30) / 100);
                # 转发量 (点赞量的 1%-15%随机)
                $share_count = ceil($like_count * rand(1, 15) / 100);
                # 分享人数(转发量的 90%-100%随机)
                $share_people = ceil($share_count * rand(90, 100) / 100);
                # 评论量 (点赞量的 1%-10%)
                $comment_count = ceil($play_count * rand(1, 15) / 100);
                if (!empty($douInfo)) {
                    # 不为空,修改
                    Db::table('merchants_dou')->where(array('id' => $douInfo['id']))->update(array(
                        'share_people' => $share_people + $douInfo['share_people'],
                        'share_count' => $share_count + $douInfo['share_count'],
                        'play_count' => $play_count + $douInfo['play_count'],
                        'like_count' => $like_count + $douInfo['like_count'],
                        'comment_count' => $comment_count + $douInfo['comment_count']
                    ));
                } else {
                    # 添加数据
                    Db::table('merchants_dou')->insert(array(
                        'm_id' => $v,
                        'add_time' => date('Y-m-d H:i:s'),
                        'share_people' => $share_people,
                        'share_count' => $share_count,
                        'play_count' => $play_count,
                        'like_count' => $like_count,
                        'comment_count' => $comment_count
                    ));
                }
            }
        }
    }

    static function smsSend()
    {
        $redis = new Redis();
        $is_repeat = $redis->lock("smsSend_backstage" , 60);
        if (!$is_repeat) {
            return true;
        }
        $SmsCode = new SmsCode();
        # 发短信--4、注册后未下载APP(·新用户通过抖音、公众号注册成功·24小时内未下载APP)
        $memberInfo_app = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('mm.member_code is not null and mm.member_code != ""')
            ->where('m.register_time <= "' . date('Y-m-d', strtotime('-1 day')) . '"')
            ->where('m.use_app=2')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 4)=0')
            ->select();
        if (!empty($memberInfo_app)) {
            # 发送短信(每次最多发送100条)
            $memberInfo_app_arr = array_chunk(array_column($memberInfo_app, 'member_phone'), 99);
            foreach ($memberInfo_app_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $smsInfo = '提醒您，您在E联车服APP内还有免费的洗车抵用券可使用，应用市场下载“E联车服”，洗车不花钱！';
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_app as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 4));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信---5、有洗车卡券未使用(首次使用商家券核销用户，使用后10天内没有在合作店下单用户，·有合作店可使用的抵用券、)
        $memberInfo_biz = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('(select count(id) from merchants_voucher_order where member_id = m.id) = 1')
            ->where('(select count(id) from member_voucher where member_id=m.id and `create` > "' . date('Y-m-d H:i:s') . '" and status = 1 and voucher_type not in (7,8,9,10,17)) > 0')
            ->where('(select count(id) from orders o where o.member_id = m.id and (date(o.order_create) between  
            (select date(create_time) from merchants_voucher_order where member_id = m.id order by id desc limit 1) and 
            (select date_add(create_time, interval 10 day) from merchants_voucher_order where member_id = m.id order by id desc limit 1))
            ) = 0
            ')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 5)=0')
            ->select();
        if (!empty($memberInfo_biz)) {
            $smsInfo = '提醒您，您在“E联车服”APP内还有免费的洗车抵用券没使用，本地洗车店全部通用。应用市场下载“E联车服”，洗车不花钱！';
            $memberInfo_biz_arr = array_chunk(array_column($memberInfo_biz, 'member_phone'), 99);
            foreach ($memberInfo_biz_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_biz as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 5));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信---6、商家消费券未使用(首次使用合作店核销卡券用户，使用后10天内没有在商家下单或付款用户，·有联盟商家可使用的抵用券、)
        $memberInfo_merchants = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('(select count(id) from member_voucher where member_id = m.id and status = 2 and voucher_type not in (7,8,9,10,17)) = 1')
            ->where('(select count(id) from member_voucher where member_id=m.id and `create` > "' . date('Y-m-d H:i:s') . '" and status = 1 and voucher_type in (7,8,9,10)) > 0')
            ->where('(select count(id) from merchants_order o where o.member_id = m.id and date(o.create_time) between 
            (select date(create_time) from log_cashcoupon where member_id = m.id order by id desc limit 1) and 
            (select date_add(create_time, interval 10 day) from log_cashcoupon where member_id = m.id order by id desc limit 1))=0
            ')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 6)=0')
            ->select();
        if (!empty($memberInfo_merchants)) {
            $smsInfo = '提醒您，您在“E联车服”APP内还有免费的消费抵用券没使用，生活消费全部通用。应用市场下载“E联车服”，洗车不花钱！';
            $memberInfo_merchants_arr = array_chunk(array_column($memberInfo_merchants, 'member_phone'), 99);
            foreach ($memberInfo_merchants_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_merchants as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 6));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信---7、省钱卡即将到期(99会员到期前15天；)
        $memberInfo_vip = Db::table('member m')
            ->field('m.member_phone,mm.member_expiration,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where("mm.member_level_id = 1")
            ->where('mm.member_expiration= "' . date('Y-m-d', strtotime('-15 days')) . '"')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 7)=0')
            ->select();
        if (!empty($memberInfo_vip)) {
            $sql_arr = array();
            foreach ($memberInfo_vip as $k => $v) {
                // ****年**月**日
                $smsInfo = '提醒您，您在“E联车服”的省钱卡权限将于' . $v['member_expiration'] . '到期，APP内升级享终身权限！';
                $SmsCode->Ysms($v['member_phone'], $smsInfo, false);
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 7));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信--8、未购买省钱卡用户(已下载APP用户·合作门店下单2次及以上但未购买省钱卡用户·注册日期在2021年11月1日后·积分剩余大于1000)
        $memberInfo_noVip = Db::table('member m')
            ->field('m.member_phone,mm.member_integral,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where("mm.member_level_id = 0")
            ->where("mm.member_integral > 1000")
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('(select count(id) from  orders where member_id = m.id) >= 2')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 8)=0')
            ->select();
        if (!empty($memberInfo_noVip)) {
            $sql_arr = array();
            foreach ($memberInfo_noVip as $k => $v) {
                $smsInfo = '提醒：您在E联车服APP已有' . $v['member_integral'] . '积分可兑换免费洗车服务，开通省钱卡专享积分兑换特权，洗车不花钱！';
                $SmsCode->Ysms($v['member_phone'], $smsInfo, false);
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 8));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信---12、车险报价短信(·用户注册超过10天·已下载APP·注册时间在2021年11月1日以后)
        $memberInfo_car = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('mm.member_code is not null and mm.member_code != ""')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('date(m.register_time) >= "' . date('Y-m-d', strtotime('-10 days')) . '"')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 12)=0')
            ->select();
        if (!empty($memberInfo_car)) {
            $smsInfo = '用户通知：E联车服APP为您提供车险报价了，最大优惠，最多福利，最好服务，可咨询各合作汽车服务门店报价！应用市场下载“E联车服”，洗车不花钱！';
            $memberInfo_car_arr = array_chunk(array_column($memberInfo_car, 'member_phone'), 99);
            foreach ($memberInfo_car_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_car as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 12));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信----13、卡券到期提醒(·有可使用商家/合作店抵用券·卡券有效期剩余5天·注册时间在2021年11月1日以后)
        $memberInfo_voucher = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('mm.member_code is not null and mm.member_code != ""')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('(select count(id) from member_voucher where member_id = m.id and 
            date(`create`) = "' . date('Y-m-d', strtotime('+5 days')) . '")')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 13)=0')
            ->select();
        if (!empty($memberInfo_voucher)) {
            $smsInfo = '提示您：您在有尚未使用的抵用券即将过期，应用市场下载“E联车服”，洗车不花钱！';
            $memberInfo_voucher_arr = array_chunk(array_column($memberInfo_voucher, 'member_phone'), 99);
            foreach ($memberInfo_voucher_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_voucher as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 13));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        # 发短信-----14、长时间未使用用户(·注册时间在2021年11月1日以后的所有用户·连续15天内未登录APP)
        $memberInfo_login = Db::table('member m')
            ->field('m.member_phone,m.id')
            ->join('member_mapping mm', 'mm.member_id=m.id', 'left')
            ->where('m.member_phone != "" and m.member_phone is not null')
            ->where('mm.member_code is not null and mm.member_code != ""')
            ->where('date(m.register_time) >= "2021-11-01"')
            ->where('(select count(id) from member_login_log where member_id = m.id 
            and date(create_time) >= "' . date('Y-m-d', strtotime('- 15 days')) . '") = 0')
            ->where('(select count(id) from log_sms_backstage where m_id = m.id and send_type = 14)=0')
            ->select();
        if (!empty($memberInfo_login)) {
            $smsInfo = '提示您：轻松享优惠，消费得积分，积分换洗车。应用市场下载“E联车服”，洗车不花钱！';
            $memberInfo_login_arr = array_chunk(array_column($memberInfo_login, 'member_phone'), 99);
            foreach ($memberInfo_login_arr as $k => $v) {
                $telInfo = implode(',', $v);
                $SmsCode->Ysms($telInfo, $smsInfo, false);
            }
            # 添加短信记录
            $sql_arr = array();
            foreach ($memberInfo_login as $k => $v) {
                array_push($sql_arr, array('m_id' => $v['id'], 'm_tel' => $v['member_phone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 14));
            }
            if (!empty($sql_arr))
                Db::table('log_sms_backstage')->insertAll($sql_arr);
        }
        $redis->unlock("smsSend_backstage");
    }
}
