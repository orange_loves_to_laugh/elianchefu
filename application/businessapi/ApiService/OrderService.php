<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/22
 * Time: 17:20
 */

namespace app\businessapi\ApiService;

/*
 * @contetn 订单信息
 * */

use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\MerchantService;
use app\api\ApiService\PropertyService;
use app\api\ApiService\SubsidyService;
use app\businessapi\controller\Index;
use app\businessapi\controller\Message;
use app\businessapi\controller\PayMent;
use app\service\AgentTask;
use app\service\BaseService;
use app\service\EalspellCommissionService;
use app\service\MemberService;
use app\service\SmsCode;
use Redis\Redis;
use think\Db;

class OrderService
{


    static function getMerchantsOrderlist($params)
    {
        #create_time 创建时间 actual_price 实际支付金额  total_price 需支付金额

        if ($params['type'] == 1) {
            #待到店 未核销  order_status = 1
            $_where = "mo.order_status = 1 and pay_type <= 6 and merchants_id= '" . $params['merchants_id'] . "' ";
        } else if ($params['type'] == 2) {
            #历史记录 已核销过的 order_status = 2
            $_where = "mo.order_status = 2 and pay_type=1 and merchants_id= '" . $params['merchants_id'] . "' ";

        } else {
            #到店记录 已核销过的 order_status = 2
            $_where = "mo.order_status = 2 and pay_type < 5 and merchants_id= '" . $params['merchants_id'] . "'";
        }
        #搜索  去重strim
        if (!empty(strim($params['search']))) {
            $_where .= " and (mo.order_number like '%" . $params['search'] . "%' or mo.member_phone like '%" . $params['search'] . "%')";

        }
        #时间筛选
        $start_time = $params['start_time'];
        $end_time = $params['end_time'];
        if (!empty($start_time) and !empty($end_time) and $start_time != '开始时间' and $end_time != '结束时间') {
            #查询 符合的条件
            $_where .= " and DATE_FORMAT(mo.create_time,'%Y-%m-%d')>= '$start_time' and DATE_FORMAT(mo.create_time,'%Y-%m-%d')<='$end_time'";
        }

        $data = Db::table('merchants_order mo')
            ->field('mo.*,mod.product_type,mod.id order_detail_id,mod.product_id,mod.duration,m.member_name,m.nickname,m.member_phone,DATE_FORMAT(mo.checkout_time,\'%Y-%m-%d %H:%i\') checkout_time')
            ->join('member m', 'm.id = mo.member_id', 'left')
            ->join('merchants_orderdetail mod', 'mod.order_id = mo.id', 'left')
            ->where($_where)
            ->order('mo.create_time desc')
            ->select();
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if (empty($v['member_id'])) {
                    $data[$k]['member_phone'] = '暂无手机号';
                    $data[$k]['member_phone'] = '12341230000';
                }
                if(empty($v["member_phone"])){
                    $data[$k]['member_phone'] = '12341230000';
                }
                $data[$k]['order_title'] = '无商品名称';
                if ($v['product_type'] == 4) {
                    $data[$k]['order_title'] = '物业缴费';
                }
                if (isset($v['product_id']) and $v['product_type'] != 4) {
                    $data[$k]['order_title'] = self::getTitle($v['product_type'], $v['product_id']);

                }
                if(empty($v['member_name']) or $v['member_name']==null){
                    $data[$k]['member_name'] = $v['nickname'];
                }
                if ($v['pay_type'] == 6) {
                    # 物业订单查询房屋信息 和缴费时长
                    $data[$k]['duration_title'] = lang("duration")[$v['duration']];
                    $info = PropertyService::getsPayFeeLogOrder($v['order_number']);
                    $data[$k]['house_info'] = $info['build_title'] . $info['unit_title'] . $info['floor_title'] . $info['house_title'] . "号";
                    $data[$k]['area'] = $info['area'];
                }

                $data[$k]['cash_type_title'] = BaseService::StatusHtml($v['cash_type'], lang('cash_type'), false);

            }
        }

        return $data;

    }

    /*
    * @contetn 订单详情
    * */
    static function getMerchantsOrderDetail($order_number, $type)
    {
        if ($type == 1) {
            $data = Db::table('merchants_orderdetail mod')
                ->field('mo.*,
                mod.product_type,mod.id order_detail_id,mod.duration,
                IFNULL(m.member_name,m.nickname) member_name,m.member_phone,
                mod.product_id,mod.order_id,
                m.id member_id,mm.member_level_id,mo.cash_type,
                DATE_FORMAT(mo.checkout_time,\'%Y-%m-%d %H:%s\') checkout_time
                ')
                ->join('merchants_order mo', 'mod.order_id = mo.id', 'left')
                ->join('member m', 'm.id = mo.member_id', 'left')
                ->join('member_mapping mm', 'm.id = mm.member_id', 'left')
                ->where(array('mod.order_number' => $order_number, 'mo.order_status' => 1))
                ->order('mod.create_time desc')
                ->find();
            if (!empty($data)) {
                $data['order_title'] = '无商品名称';
                if ($data['product_type'] != 4) {
                    if (isset($data['product_id'])) {
                        #服务名称
                        $data['order_title'] = self::getTitle($data['product_type'], $data['product_id']);
                        $data['proInfo'] =MerchantsService::getsProDetail($data['product_id']);
                    }
                } else {
                    $data['order_title'] = '物业缴费';
                }
                if ($data['pay_type'] == 6) {
                    # 物业订单查询房屋信息 和缴费时长
                    $data['duration_title'] = lang("duration")[$data['duration']];
                    $info = PropertyService::getsPayFeeLogOrder($data['order_number']);
                    $data['house_info'] = $info['build_title'] . $info['unit_title'] . $info['floor_title'] . $info['house_title'] . "号";
                    $data['area'] = $info['area'];
                }

                #订单类型
                $data['order_type_title'] = BaseService::StatusHtml($data['cash_type'], lang('cash_type'), false);
                #查询卡券低值金额

                $price = self::getMerchantsVoucherPrice($data['member_id'], $data['order_id']);
                if (empty($price['price'])) {
                    $data['merchants_voucher_price'] = priceFormat(0);
                } else {
                    $data['merchants_voucher_price'] = $price['price'];
                }
                #查询会员等级
                $data['member_level_title'] = Db::table('member_level')->where(array('id' => $data['member_level_id']))->value('level_title');
                if (empty($data['member_level_title'])) {
                    $data['member_level_title'] = '未注册用户';
                }


            }
        } else {
            #到店（历史  核销过）
            $data = Index::getMerchantsOrder($order_number);
        }

        return $data;
    }

    /*
   * @contetn 获取订单服务（商品）名称
   * @params $type 1 服务 2 商品
   * @params $id 服务id 或 商品 id
   * */
    static function getTitle($type, $id)
    {
        if ($type == 1) {
            #服务
            $order_title = Db::table('service')->where("service_top =1 and service_status=1 and is_del !=1 and id = $id")->value('service_title');

        } else {
            #商品商户推荐商品表

            $order_title = Db::table('merchants_pro')->where("id = $id")->value('title');


        }
        return $order_title;

    }

    /*
    * @contetn 获取订单卡券低值金额
    * @params $member_id 用户id
    * @params $order_id 订单id
    * */
    static function getMerchantsVoucherPrice($member_id, $order_id)
    {


        $merchants_voucher_price = Db::table('merchants_voucher_order ')->where("order_id= $order_id and member_id= $member_id")->find();
        return $merchants_voucher_price;

    }

    /*
     * @content 搜索会员信息
     * */
    static function getSearchMember($phone)
    {

        $data = Db::table('member m')
            ->field('m.id,m.member_name,m.member_phone,mm.member_level_id,mm.member_balance,m.headimage,nickname')
            ->join('member_mapping mm', 'mm.member_id = m.id', 'left')
            ->where("m.member_phone = $phone  and mm.member_status != 0")->find();
        if (!empty($data)) {
            if(empty($data['member_name'])){
                $data['member_name'] = $data['nickname'];
            }
            $data['member_level_title'] = Db::table('member_level')->where(array('id' => $data['member_level_id']))->value('level_title');
            if(empty($data['member_level_title'])){
                $data['member_level_title'] = '普通用户';
            }
            $data['member_balance'] = priceFormat($data['member_balance']);
        }
        return $data;
    }

    /*
     * @content 核销订单
     * */
    static function getOrderComplete($params)
    {
        $time = date('Y-m-d H:i:s');
        #  判断商户是否为物业
        $is_property = Db::table('merchants_order mo')
            ->field("m.is_type,mod.product_type")
            ->leftJoin("merchants m", "m.id = mo.merchants_id")
            ->leftJoin("merchants_orderdetail mod", "mod.order_id = mo.id")
            ->where(array('mo.order_number' => $params['order_number']))
            ->find();
        if ($is_property['is_type'] == 3 and $is_property['product_type'] == 4) {
            Db::table("property_payfee")->where(array("order_number" => $params['order_number']))->update(array("billing" => 2));
            Db::table("merchants_order")->where(array("order_number" => $params['order_number']))->update(array("order_status" => 2));
            return true;
        }
        $info = Db::table('merchants_order')->where(array('order_number' => $params['order_number'],"order_status"=>2))->find();
        if(!empty($info)){
            return true;
        }
        #核销订单成功  商户余额
        Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->update(array('order_status' => 2, 'checkout_time' => $time));
        #查询订单抽成金额
        $commission_price = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->value('commission_price');
        $total_price = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->value('total_price');
        $actual_price = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->value('actual_price');
        $orderId = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->value('id');


        #根据订单号查询 商户优惠券 是平台 还是商户使用
        $merchants_voucher_order = Db::table('merchants_voucher_order')->where(array('order_id' => $orderId))->value('voucher_type');
        if ($merchants_voucher_order == 1) {
            #平台  商户余额 = 实际支付+抽成
            $balance = $total_price - $commission_price;

        } else {
            #商户 商户月 = 实际支付-抽成
            $balance = $actual_price - $commission_price;


        }
        #商户余额

        Db::table('merchants')->where(array('id' => $params['merchants_id']))->setInc('balance', $balance);
        #查询 商户的员工id
        $employee_sal = Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('relate_employee');
        if (empty($employee_sal)) {
            $employee_sal = 0;
        }
        #加收款记录
        $merchants_order = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->find();
        Db::table('merchants_income')->insert(
            array(
                'merchants_id' => $params['merchants_id'],
                'member_id' => $merchants_order['member_id'],
                'pay_type' => $merchants_order['cash_type'],
                'price' => $balance,
                'order_number' => $params['order_number'],
                'employee_sal' => $employee_sal,//商户关联员工id
            ));
        #加抽成（收入）
        $BizIncome = array(
            'biz_id' => $params['merchants_id'],
            'biz_income' => $balance,
            'pay_type' => $merchants_order['cash_type'],
            'member_id' => $merchants_order['member_id'],
            'income_type' => 13,
            'order_number' => $params['order_number'],
        );
        addBizIncome($BizIncome);
        #根据订单 查询卡券使用记录  是商户卡券  对应使用数数量加1
        $voucher_use = Db::table('merchants_voucher_order')->where(array('order_id' => $merchants_order['id'], 'member_id' => $merchants_order['member_id']))->find();
        if (!empty($voucher_use)) {
            if ($voucher_use['voucher_type'] == 2) {
                #商家优惠券
                Db::table('merchants_voucher')->where(array('id' => $voucher_use['voucher_id']))->setInc('use_num', 1);
            }
        }
        if ($is_property['is_type'] != 3) {
            #预约次数
            TaskService::getTaskPaySucess($params, '6');
            #调下 任务销售额  核销预约订单加销售额(实际支付金额)
//        $params['merchants_id'] = $params['merchants_id'];
//        $params['price'] = $params['price'];
            $params['task_type'] = 3;
            $res = TaskService::getTaskPaySucess($params);

        }
        # 查最后一个推荐办理会员的人是不是合伙人
        self::getRecommendPartner($params);

        #添加默认商户订单评论
        self::merchantsEvaluation($orderId);
        #加红包记录  给商家红包数量
        self::getRedpacketLog($params);
        # 代理提成到账
        EalspellCommissionService::updateMerchantsProCommission($params['order_number']);
        # 代理会员消费到账
        EalspellCommissionService::finishMemberConsumpCommission($params['order_number']);
        # 用户第1，2，3,12 次消费给提成
        EalspellCommissionService::memberConsumpNumFinish($params['order_number'],$merchants_order['member_id']);
        # 首次购买商家套餐
        EalspellCommissionService::memberMerchantsProFinish($params['order_number'],$merchants_order['member_id']);
        #发送语音提醒
        $data['pay_type'] = $merchants_order['cash_type'];
        $data['price'] = $params['price'];
        $message = new Message();
//        dump($data);die;

        $message->send($params['merchants_id'], 1, $data);


        return true;


    }

    /*
     * @content  加红包记录  给商家红包数量
     * */
    static function getRedpacketLog($params)
    {

        #给红包 按数量判断  红包 是否 在首页 显示
        $dtime = date("Y-m-d H:i:s");

//        $commission_price = self::getOrdercommissionPrice($day,$params['merchants_id']);

        $time = date("Y-m-d");
        #查询当天是否有 商户的红包数量 没有 创建一个
        $_where = "merchants_id ='" . $params['merchants_id'] . "'  and DATE_FORMAT(create_time,'%Y-%m-%d') = '$time'";
        $merchants_redpacket_log = Db::table('merchants_redpacket_log')->where($_where)->value('id');
        #查询红包表的红包是否够
        $merchants_redpacket = RedpacketService::getMerchantsRedpacketInfo();

        if (!empty($merchants_redpacket_log)) {
            if ($merchants_redpacket['num'] > $merchants_redpacket['receive_num']) {
                #不等 说明 红包的数量 是有的
                #数量加1
                Db::table('merchants_redpacket_log')->where(array('merchants_id' => $params['merchants_id']))->setInc('num', '1');
                Db::table('merchants_redpacket')->where("'$dtime' > start_time and '$dtime' < end_time")->setInc('receive_num', '1');
            }


        } else {
            Db::table('merchants_redpacket_log')->insert(array(
                'merchants_id' => $params['merchants_id'],
                'num' => 1,
            ));
            #红包数量加1
            Db::table('merchants_redpacket')->where("'$dtime' > start_time and '$dtime' < end_time")->setInc('receive_num', '1');
        }
        return true;
    }

    /*
     * @content 查询商户订单的抽成金额  和 所有商户的抽成金额
     * */
    static function getOrdercommissionPrice($day, $merchants_id)
    {
        $_where = "DATE_FORMAT(checkout_time,'%Y-%m-%d') = '$day' and order_status = 2";
        if (!empty($merchants_id)) {
            $_where .= " and merchants_id = $merchants_id";

        }

        $commission_price = Db::table('merchants_order')->where($_where)->sum('commission_price');
        return $commission_price;

    }

    /*
     * @content 获取 购买天数
     * @param $push_type 1. 连续包月 2 .连续包季 3 . 1个月 4.3个月 5 .半年 6 .一年
     * */
    static function getPushType($push_type)
    {
        #默认 30 天
        $data = '30';
        if ($push_type == 1) {
            #二年365*2
            $data = 730;

        } else if ($push_type == 2) {
            #三年 365*3
            $data = 1095;
        } else if ($push_type == 3) {
            #一个月
            $data = 30;
        } else if ($push_type == 4) {
            #三个月
            $data = 90;
        } else if ($push_type == 5) {
            #半年
            $data = 180;
        } else if ($push_type == 6) {
            #一年
            $data = 365;
        }
        return $data;

    }

    /*
     * @content 商户升级订单 创建
     * @param  total_price  actual_price cash_type  merchants_level
     * */
    static function getUpgradeOrder($params)
    {
        $params['checkout_time'] = self::getPushType($params['push_type']);

        #判断订单号是否存在
        if (empty($params['orderNumber'])) {
            $orderNumber = getOrderNumber();
        } else {
            $orderNumber = $params['orderNumber'];
        }
        #查询 商户的员工id
        $employee_sal = Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('relate_employee');
        if (empty($employee_sal)) {
            $employee_sal = 0;
        }
        $checkout_time = date('Y-m-d H:i:s', strtotime("+" . $params['checkout_time'] . "day"));
        $orderId = Db::table('merchants_order')->insertGetId(array(
            'order_number' => $orderNumber,
            'total_price' => $params['total_price'],//需支付金额
            'actual_price' => $params['actual_price'],//实际支付金额
            'commission_price' => 0, //平台抽成金额  只有用户下单才有  商户的抽成比列*实际支付金额
            'merchants_id' => $params['merchants_id'],
            'member_id' => 0,
            'pay_type' => 5,//5 商家推广 下的订单
            'cash_type' => $params['cash_type'],//支付方式
            'order_status' => 2,//订单状态 默认 已到店状态  其实 主要 看 pay_type 的状态 是否为5
            'checkout_time' => $checkout_time,//这个订单 是升级为推荐商家 或者 是优质商家时 传过来的 购买时长+当前时间
            'employee_sal' => $employee_sal,//商户关联员工id
        ));
        #订单详情
        Db::table("merchants_orderdetail")->insertGetId(array(
            'order_number' => $orderNumber,
            'order_id' => $orderId,
            'product_type' => 3,
            'merchants_level' => $params['merchants_level'], // 商户升级到的 类型 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
        ));
        #给 商家推广费14  商家抽成 13  那部分加到  biz_income里面
        #商家推广费
        $BizIncome = array(
            'biz_id' => $params['merchants_id'],
            'biz_income' => $params['actual_price'],
            'pay_type' => $params['cash_type'],
            'member_id' => 0,
            'income_type' => 14,
            'order_number' => $orderNumber,
        );
        addBizIncome($BizIncome);
        return array('orderId' => $orderId, 'order_number' => $orderNumber);


    }

    /*
     * @content  扫码支付
     * @param order_type 1线上预约，2用户付款-用户端点击扫码按钮进行支付，3扫码-用户扫码支付，4商家扫码-商家扫码收款
     * @param member_id 用户id
     * @param actual_price 实际支付金额
     * @param member_id
     * */
    static function getSancodeOrder($params)
    {
        #判断卡券数组是否存在 存在存 merchants_voucher_order
        $voucherInfo = json_decode($params['voucherInfo'], true);
        #获取商户抽成比列
        $commission = Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('commission');
        # 抽成比例修改为 5% , 2022-01-03 修改不要了 2022-01-12
        //$commission = 0.05;
        if (!empty($commission)) {
            #没有卡券  和是商家优惠券时 实际支付 *抽成
            $commission_price = $commission * $params['actual_price'];
            $balance = $params['actual_price'] - $commission_price;
            //90*0.05 = 5
            #有卡券且是平台优惠券 订单金额 *抽成
            $is_type = 0;
            if (!empty($voucherInfo)) {
                if ($voucherInfo['merchants_id'] == 0) {
                    $is_type = 1;//平台
                } else {
                    $is_type = 2;//商家优惠券

                }
            }
            if ($is_type == 1) {
                //100*0.05
                $commission_price = $commission * $params['total_price'];
                $balance = $params['total_price'] - $commission_price;
            }
            # 扣除商品佣金
            if (!empty($params['product_id'])) {
                $merchantProInfo =  Db::table('merchants_pro')->field("commission")->where(array('id' => $params['product_id']))->find();
                if($merchantProInfo['commission']>0){
                    $balance = $balance - $merchantProInfo['commission'];
                }
            }


        } else {
            $commission_price = 0;
        }

        #判断 是不是商户收款  还是 用户端支付
        if (!empty($params['markType']) and $params['markType'] == 'merchantsApp') {
            $order_status = 2;//已到店 核销成功

        }
        if ($params['order_type'] == 1) {
            $order_status = 1;//待到店
            $consump_type = 1; //线上支付
        } else {
            $order_status = 2;//已到店 核销成功
            $consump_type = 2;//门店支付
        }


        $checkout_time = date("Y-m-d H:i:s");
        #判断订单号是否存在
        if (empty($params['orderNumber'])) {
            $orderNumber = getOrderNumber();
        } else {
            $orderNumber = $params['orderNumber'];
        }
        # 查询是否重复
        $is_repeat = Db::table('merchants_order')->field("id")->where(array('order_number' => $orderNumber))->find();
        if (!empty($is_repeat)) {
            return array('orderId' => $is_repeat['id'], 'order_number' => $orderNumber, 'voucherInfo' => $voucherInfo);
        }
        #参数不存在 默认为0
        if (empty($params['member_id'])) {
            $params['member_id'] = 0;
        }

        #查询用户信息
        if ($params['member_id']) {
            $member = Db::table('member')->where(array('id' => $params['member_id']))->field('member_phone,member_name')->find();
//            $member_level_info = Db::table('member_mapping')->field("member_level_id,member_expiration")->where(array('member_id' => $params['member_id']))->find();
//            if ($member_level_info['member_level_id'] >0  and $member_level_info['member_expiration'] >= date('Y-m-d')) {
//                # 查询该会员是否是代理推荐
//                $channel = Db::table("channel")->where(array("member_id"=>$params['member_id'],"action_type"=>1))->order("id desc")->find();
//                if(!empty($channel)){
//                    if($channel['channel']==10 and $channel['pid']>0){
//                        EalspellCommissionService::addMemberConsump($channel['pid'],$params['actual_price'],$params['member_id'],$orderNumber);
//
//                    }
//                }
//            }
            # 判断是否有车辆服务订单
//            $new_order =  Db::table('orders')->where(array("member_id"=>$params['member_id']))->find();
//            if(empty($new_order)){
//                # 判断商家订单号是不是首次消费
//                $new_merchant_order = Db::table('merchants_order')->where(array("member_id"=>$params['member_id']))
//                    ->where("order_number != '".$orderNumber."'")->find();
//                if(empty($new_merchant_order)){
//                    # 查询该会员是否是代理推荐
//                    $channel = Db::table("channel")->where(array("member_id"=>$params['member_id'],"action_type"=>2))->order("id desc")->find();
//                    if(!empty($channel)){
//                        if($channel['channel']==10 and $channel['pid']>0){
//                            EalspellCommissionService::addRegisterCommission($channel['pid'],$params['member_id']);
//                        }
//                    }
//                }
//            }
            $channel = Db::table("channel")->where(array("member_id"=>$params['member_id'],"channel"=>10,"action_type"=>1))->where("pid > 0")
                ->order("id desc")->find();
            if(!empty($channel)){
                EalspellCommissionService::addMemberConsump($channel['pid'],$params['actual_price'],$params['member_id'],$orderNumber);
                EalspellCommissionService::memberConsumpNum($params['member_id'],$channel['pid'],$orderNumber);
            }



        }
        #查询 商户的员工id
        $employee_sal = Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('relate_employee');
        if (empty($employee_sal)) {
            $employee_sal = 0;
        }
        # 增加微信支付宝收款手续费
        if ($params['cash_type'] == 1 or $params['cash_type'] == 2) {
            SubsidyService::addExpenses(array("type_id" => 2, "price" => $params['actual_price'] * 0.006));
        }
        if ($params['cash_type'] == 3 or $params['cash_type'] == 4) {
            #商户余额  银联 现金  抽成为0
            $commission_price = 0;
        }
        if (!empty($params['merchants_id'])) {
            $agent_id= Db::table('merchants')->where(array('id' => $params['merchants_id']))->value("agent_id");
            if($agent_id>0){
                EalspellCommissionService::merchantsConsump($agent_id,$commission_price,$params['merchants_id'],$orderNumber);

            }
        }
        $orderId = Db::table('merchants_order')->insertGetId(array(
            'order_number' => $orderNumber,
            'total_price' => $params['total_price'],//需支付金额
            'actual_price' => $params['actual_price'],//实际支付金额
            'commission_price' => $commission_price, //平台抽成金额  只有用户下单才有  商户的抽成比列*实际支付金额
            'merchants_id' => $params['merchants_id'],
            'pay_type' => $params['order_type'],//1线上预约，2用户付款-用户端点击扫码按钮进行支付 用户直接 给商家 付款，3扫码-用户扫码支付，4商家扫码-商家扫码收款5 商家推广 下的订单
            'cash_type' => $params['cash_type'],//支付方式
            'member_id' => $params['member_id'],//用户id
            'member_name' => $member['member_name'],//用户姓名
            'member_phone' => $member['member_phone'],//用户手机号
            'order_status' => $order_status,//订单状态 扫码支付 传2
            'checkout_time' => $checkout_time,
            'employee_sal' => $employee_sal,//商户关联员工id
        ));
        #订单详情
        Db::table("merchants_orderdetail")->insertGetId(array(
            'order_number' => $orderNumber,
            'order_id' => $orderId,
            'product_type' => $params['product_type'], //1服务 2商品 3.商户升级
            'product_id' => empty($params['product_id']) ? 0 : $params['product_id'],  //商品id或服务id
        ));

        # 添加消费记录
        Db::table('log_consump')
            ->insert(array(
                'member_id' => $params['member_id'],
                'consump_price' => $params['actual_price'],
                'consump_pay_type' => $params['cash_type'],
                'consump_time' => date("Y-m-d H:i:s"),
                'log_consump_type' => 1,
                'consump_type' => $consump_type,
                'income_type' => 20, // 商户消费
                'biz_id' => $params['merchants_id'],
                'order_number' => $orderNumber
            ));
        #添加足迹记录
        $addLogConsump = new MerchantService();
        $_addLogConsump = array(
            'member_id' => $params['member_id'],
            'merchants_id' => $params['merchants_id'],
            'price' => $params['total_price'],
            'pract_price' => $params['actual_price'],
            'coupon_price' => (intval($params['total_price']) - intval($params['actual_price'])),
            'order_number' => $orderNumber,
            'pay_type' => $params['cash_type'],
        );

        $addLogConsump->addLogConsump($_addLogConsump);
        if(!empty($params['member_id'])){
            # 查询该会员是否是代理推荐
            $agent_id= Db::table('merchants')->where(array('id' => $params['merchants_id']))->value("agent_id");
            if(!empty($agent_id)){
                EalspellCommissionService::merchantSalesAchieved($agent_id,$params['merchants_id']);
                AgentTask::memberConsumpNumTask($agent_id,$params['actual_price']);
                AgentTask::merchantsPayeeTask($agent_id,$params['actual_price']);
            }
        }
        #如果类型是 余额
        if ($params['cash_type'] == 5) {
            #减掉对应会员余额
            $balanceService = new BalanceService();
            $balanceService->operaMemberbalance($params['member_id'], $params['actual_price'], 2, []);
        }
        #如果是商品支付 商品已售数量加1
        if (!empty($params['product_id'])) {
            Db::table('merchants_pro')->where(array('id' => $params['product_id']))->setInc('sold_num', 1);
            if(!empty($params['formMark']) and $params['formMark']=='agent' and !empty($params['assign_id'])){
                # 加代理推荐提成
                $merchantProInfo =  Db::table('merchants_pro')->field("commission")->where(array('id' => $params['product_id']))->find();
                if($merchantProInfo['commission']>0){
                    EalspellCommissionService::addMerchantsPro($params['assign_id'],$merchantProInfo['commission'],$orderNumber,$params['merchants_id']);
                    AgentTask::merchantsProTask($params['assign_id'],$params['actual_price']);
                }
            }
            # 首次购买商家套餐
            $channel = Db::table("channel")->where(array("member_id"=>$params['member_id'],"channel"=>10))->where("pid > 0")
                ->order("id desc")->find();
            if(!empty($channel)){
                EalspellCommissionService::memberMerchantsPro($params['member_id'],$channel['pid'],$orderNumber);
            }
        }
        #消费送积分
        if (!empty($voucherInfo) and !empty($voucherInfo['id'])) {
            $voucherInfo = Db::table('member_voucher')->where(array('id'=>$voucherInfo['id']))->find();
            #卡券低值金额
            if (!empty($voucherInfo['voucher_cost']) and !empty($voucherInfo['voucher_id'])) {


                if ($voucherInfo['merchants_id'] == 0) {
                    $is_type = 1;//平台
                } else {
                    $is_type = 2;//商家优惠券
                    if ($order_status == 2) {
                        #是商家优惠券 给商家优惠券的id 使用数量+1
                        $a = Db::table('merchants_voucher')->where(array('id' => $voucherInfo['id']))->setInc('use_num', 1);
                    }


                }

                #创建优惠券使用表
                $a = Db::table('merchants_voucher_order')->insertGetId(array(
                    'voucher_id' => $voucherInfo['voucher_id'],//为平台 优惠券时是 (现金红包详情id   redpacket_detail )    商户优惠券是merchants_voucher表的id
                    'voucher_type' => $is_type,//优惠券类型：1平台优惠券 2商户优惠券
                    'order_id' => $orderId,//订单id
                    'price' => $voucherInfo['voucher_cost'], //低值金额
                    'member_id' => $voucherInfo['member_id'],//用户id

                ));
                #消掉卡券  把卡券改为核销状态
                Db::table('member_voucher')->where(array('id' => $voucherInfo['id']))->update(array('status' => 2));

            }
        }

        /*  if($params['markType'] == 'merchantsApp'){


        }*/
        #判断是否支持送积分  if (Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('giving_score') == 1) {
        # 判断优质商家送积分  不是优质商家不送
        $merchantsInfo = Db::table('merchants')->field('level,expire_time')->where(array('id' => $params['merchants_id']))->find();
//        if ($merchantsInfo['level']==1 and $merchantsInfo['expire_time'] > date('Y-m-d H:i:s')) {
            #给用户加积分记录
            if (!empty($params['member_id'])) {
                $data['merchants_id'] = $params['merchants_id'];
                if (!empty($params['product_id'])) {
                    $data['biz_pro_id'] = $params['product_id'];
                }
                $integralService = new IntegralService();
                $integralService->addMemberIntegral($params['member_id'], '27', $params['actual_price'], 1, $data);

            }
//        }
        #已核销
        if ($order_status == 2) {
            $params['order_number'] = $orderNumber;
            # 查最后一个推荐办理会员的人是不是合伙人
            self::getRecommendPartner($params);

            #添加默认商户订单评论
            self::merchantsEvaluation($orderId);
            #是已到店订单  加记录
            if ($params['cash_type'] != 3 and $params['cash_type'] != 4) {
                #商户余额  银联 现金  不加 余额
                Db::table('merchants')->where(array('id' => $params['merchants_id']))->setInc('balance', $balance);
            }


            #加收款记录
            Db::table('merchants_income')->insert(
                array(
                    'merchants_id' => $params['merchants_id'],
                    'member_id' => $params['member_id'],
                    'pay_type' => $params['cash_type'],
                    'price' => $balance,
                    'order_number' => $orderNumber,
                    'employee_sal' => $employee_sal,//商户关联员工id
                ));

            #给 商家推广费14  商家抽成 13  那部分加到  biz_income里面
            #商家抽成(收入)
            if ($commission_price > 0) {
                $BizIncome = array(
                    'biz_id' => $params['merchants_id'],
                    'biz_income' => $commission_price,
                    'pay_type' => $params['cash_type'],
                    'member_id' => $params['member_id'],
                    'income_type' => 13,
                    'order_number' => $orderNumber,
                );
                addBizIncome($BizIncome);
            }


            #加红包记录  给商家红包数量
            $a = self::getRedpacketLog($params);
            #发送语音提醒
            $data['pay_type'] = $params['cash_type'];
            $data['price'] = $params['actual_price'];
            $params['price'] = $params['actual_price'];
            $message = new Message();

            $a = $message->send($params['merchants_id'], 1, $data);
            # 任务类型+收款次数
            TaskService::getTaskPaySucess($params, '5');
            #调下 任务销售额  核销预约订单加销售额(实际支付金额)WW
            TaskService::getTaskPaySucess($params, '3');
            # 每日任务完成收款
            TaskService::finishDayTask($params['merchants_id'],3);

        } else {
            #发送语音提醒

            $message = new Message();

            $a = $message->neworder_send($params['merchants_id'], 1);

        }
        $voucherMerchantInfo = json_decode($params['voucherMerchantInfo'], true);
        if(!empty($voucherMerchantInfo) and !empty($voucherMerchantInfo['id'])){
            if ($order_status == 2) {
                #是商家优惠券 给商家优惠券的id 使用数量+1
                $a = Db::table('merchants_voucher')->where(array('id' => $voucherMerchantInfo['id']))->setInc('use_num', 1);
            }
            #创建优惠券使用表
            $a = Db::table('merchants_voucher_order')->insertGetId(array(
                'voucher_id' => $voucherMerchantInfo['voucher_id'],//为平台 优惠券时是 (现金红包详情id   redpacket_detail )    商户优惠券是merchants_voucher表的id
                'voucher_type' => 2,//优惠券类型：1平台优惠券 2商户优惠券
                'order_id' => $orderId,//订单id
                'price' => $voucherMerchantInfo['voucher_cost'], //低值金额
                'member_id' => $voucherMerchantInfo['member_id'],//用户id

            ));
            #消掉卡券  把卡券改为核销状态
            Db::table('member_voucher')->where(array('id' => $voucherMerchantInfo['id']))->update(array('status' => 2));
        }
        if (!empty($params['member_id'])) {
            #发送语音提醒

            $message = new Message();
            $data['pay_type'] = $params['cash_type'];
            $data['price'] = $params['actual_price'];
            $data['member_id'] = $params['member_id'];
            $a = $message->user_send($params['merchants_id'], 1, $data);

            #发送短信提醒
            $SmsCode = new SmsCode();
            if ($params['cash_type'] == 5) {
                $content = $message->getContent($params['merchants_id'], $data);
                $SmsCode->Ysms($member['member_phone'], $content, false);

            }
            #清除用户缓存
            $changeMemberInfo = new \app\api\ApiService\MemberService();
            #更新用户信息
            $changeMemberInfo->changeMemberInfo($params['member_id']);

        }
        $redis = new Redis();
        $redis->hDel('payRedis', strval($orderNumber));
        return array('orderId' => $orderId, 'order_number' => $orderNumber, 'voucherInfo' => $voucherInfo);
    }

    /*
     * @content  商户升级 支付成功
     * @param level 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
     * @param price 保证金  升级价格
     * */
    static function getUpgradePaySuccess($params)
    {
        $merchants = Db::table('merchants')->where(array('id' => $params['merchants_id']))->field('expire_time,level,agent_id')->find();
        #push_type  1. 连续包月 2 .连续包季 3 . 1个月 4.3个月 5 .半年 6 .一年
        $params['expire_time'] = self::getPushType($params['push_type']);
        $data = array();
        if ($params['level'] == 3) {
            #升级为诚信商家
            # 给商户 加保证金  修改商户状态
            $data = array(
                'bond_price' => $params['price'],
                'level' => $params['level'],
            );
            $expire_time = '退回前一直有效';
        } else if ($params['level'] == 2) {
            #升级为推荐商家
            if ($merchants['level'] == 2) {
                #优质商家 延期续费
                #原来的到期时间 + 天数
                $expire_time = date('Y-m-d H:i:s', strtotime("" . $merchants['expire_time'] . "+" . $params['expire_time'] . "day"));
                if(!empty($merchants['agent_id'])){
                    EalspellCommissionService::merchantsUpgrade($merchants['agent_id'],$params['price'],$params['merchants_id']);
                }
            } else {
                $expire_time = date('Y-m-d H:i:s', strtotime("+" . $params['expire_time'] . "day"));
                if(!empty($merchants['agent_id'])){
                    EalspellCommissionService::merchantsCharge($merchants['agent_id'],$params['price'],$params['merchants_id']);
                }
            }
            #
            $data = array(
                'expire_time' => $expire_time,
                'level' => $params['level'],
                'open_toutiao' => 1,
                'open_expiration_time' => $expire_time,
                'push_time'=>$params['expire_time'],
                'push_create_time'=>date("Y-m-d H:i:s")
            );


        } else if ($params['level'] == 1) {
            #升级为优质商家
            if ($merchants['level'] == 2) {
                #原来的到期时间 + 天数
//                $expire_time =date('Y-m-d H:i:s',strtotime("".$merchants['expire_time']."+".$params['expire_time']."day"));
                #改  当前时间  +天数
                # 首次成为优质商家 , 三个月
                if($params['expire_time']==30 || $params['push_type']==3){
                    $params['expire_time'] = 90;
                }
                $expire_time = date('Y-m-d H:i:s', strtotime("+" . $params['expire_time'] . "day"));
                if(!empty($merchants['agent_id'])){
                    EalspellCommissionService::merchantsUpgrade($merchants['agent_id'],$params['price'],$params['merchants_id']);
                    AgentTask::highGradeMerchantsTask($merchants['agent_id']);
                }

            } else {
                if ($merchants['level'] == 1) {
                    #优质商家 延期续费
                    #原来的到期时间 + 天数
                    $expire_time = date('Y-m-d H:i:s', strtotime("" . $merchants['expire_time'] . "+" . $params['expire_time'] . "day"));
                    if(!empty($merchants['agent_id'])){
                        EalspellCommissionService::merchantsUpgrade($merchants['agent_id'],$params['price'],$params['merchants_id']);
                    }
                } else {
                    # 首次成为优质商家 , 三个月
                    if($params['expire_time']==30 || $params['push_type']==3){
                        $params['expire_time'] = 90;
                    }
                    $expire_time = date('Y-m-d H:i:s', strtotime("+" . $params['expire_time'] . "day"));
                    if(!empty($merchants['agent_id'])){
                        EalspellCommissionService::merchantsCharge($merchants['agent_id'],$params['price'],$params['merchants_id']);
                        AgentTask::highGradeMerchantsTask($params['agent_id']);
                    }
                }

            }
            # 成为优质商家 自动开通抖音功能 , 有效期与优质商家的过期时间一致
            $data = array(
                'expire_time' => $expire_time,
                'level' => $params['level'],
                'take_video' => 1,
                'open_toutiao' => 1,
                'open_expiration_time' => $expire_time,
                'take_expiration_time' => $expire_time,
                'push_time'=>$params['expire_time'],
                'push_create_time'=>date("Y-m-d H:i:s")
            );

        }


        Db::table('merchants')->where(array('id' => $params['merchants_id']))->update($data);
        return $expire_time;


    }

    /*
     * @content 商户自动续费记录
     * */
    static function getMerchantsRenew($params)
    {
        Db::table('merchants_renew')->insert($params);
        return true;

    }

    /*
     * 订单默认评论数据
     * */
    static function merchantsEvaluation($orderId)
    {
        if (!empty($orderId)) {
            Db::table('merchants_evaluation')->insert(array(
                'merchants_order_id' => $orderId,
                'evalua_level' => 3,//默认中评

            ));
            return true;
        }

    }

    /*
     * @content 查最后一个推荐办理会员的人是不是合伙人
     * */
    static function getRecommendPartner($params)
    {
        if (empty($params['actual_price'])) {
            $params['actual_price'] = $params['price'];

        }
        #查询订单中的member_id
        $params['member_id'] = Db::table('merchants_order')->where(array('order_number' => $params['order_number']))->value('member_id');
        if (!empty($params['member_id'])) {
            $channel = Db::table('channel')->field('pid,channel,action_type')->where(array('member_id' => $params['member_id']))->order('id desc')->find();
            if (!empty($channel) and $channel['action_type'] == 2 and ($channel['channel'] == 3 or $channel['channel'] == 5) and $channel['pid'] != 0) {
                $pInfo = Db::table('member_mapping')->field('member_type,member_level_id,member_expiration')->where("member_id = " . $channel['pid'])->find();
                if (!empty($pInfo) and $pInfo['member_type'] == 2) {
                    #体验卡 初级体验卡  是否过期  过期改为不是合伙人
                    if (($pInfo['member_level_id'] == 1 or $pInfo['member_level_id'] == 2) and $pInfo['member_expiration'] < date("Y-m-d")) {
                        Db::table('member_mapping')->where("member_id = " . $channel['pid'])->update(array('member_type' => 1));

                    } else {
                        # 合伙人加收益,查询比例
                        $partnerSet = Db::table('sysset')->field('desc')->where(array('tagname' => 'partner_set'))->find();
                        if (!empty($partnerSet)) {
                            $info = json_decode($partnerSet['desc'], true);
                            #consumption_commission 商户比列
                            if (!empty($info) and !empty($info['consumption_commission'])) {
                                $partnerIncome = $params['actual_price'] * $info['consumption_commission'];
                                Db::table('member_partner')->where(array('member_id' => $channel['pid']))->setInc('partner_balance', $partnerIncome);
                                Db::table('member_partner')->where(array('member_id' => $channel['pid']))->setInc('partner_balance_total', $partnerIncome);
                                Db::table('log_consump')->insert(array(
                                    'member_id' => $channel['pid'],
                                    'consump_price' => $partnerIncome,
                                    'consump_pay_type' => 1,
                                    'consump_time' => date('Y-m-d H:i:s'),
                                    'log_consump_type' => 2,
                                    'consump_type' => 1,
                                    'income_type' => 10,

                                    'biz_id' => $params['merchants_id'],
                                    'order_number' => $params['order_number']
                                ));

                            }
                        }
                    }


                }
            }
        }
        return true;

    }

    static function refundTransfer($pay_type, $order_number, $member_id, $pay_price)
    {
        $payMent = new PayMent();
        if ($pay_type == 1) {
            # 微信退款
            $data = [
                'out_trade_no' => $order_number,
                'out_refund_no' => time(),
                'total_fee' => floatval($pay_price) * 100, //
                'refund_fee' => floatval($pay_price) * 100,
                'refund_desc' => '商家退款',
            ];
            $status = $payMent->wxRefund($data);
        } elseif ($pay_type == 2) {
            # 支付宝退款
            $data = [
                'out_trade_no' => $order_number,
                'refund_amount' => $pay_price, // $v['pay_price']
            ];
            $status = $payMent->aliRefund($data);
        } else {
            # 余额退款
            $data = [
                "member_id" => $member_id,
                "pay_price" => $pay_price,
                "order_number" => $order_number
            ];
            $status = self::balanceRefund($data);
        }
        return $status;
    }

    static function balanceRefund($data){
        # 增加余额
        Db::table("member_mapping")->where(array("member_id" =>$data['member_id']))->setInc("member_balance", $data['pay_price']);
        return true;
    }


}

