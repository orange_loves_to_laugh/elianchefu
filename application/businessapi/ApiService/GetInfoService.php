<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/29
 * Time: 14:46
 */

namespace app\businessapi\ApiService;


use app\service\MerchantService;
use think\Db;

class GetInfoService
{
    /**
     * [getMerchantsInfo 获取商户协议 关于 平台]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getMerchantsInfo($mark){
        $data = Db::table('merchants_application')->where(array('merchants_type'=>1,'mark'=>$mark))->value('content');
        return $data;
    }
    /*
     * @content 上传
     * */
    static function receiveStreamFile($receiveFile){
        $streamData = isset($GLOBALS['HTTP_RAW_POST_DATA'])? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
        if(empty($streamData)){
            $streamData = file_get_contents('php://input');
        }
        if($streamData!=''){
            $ret = file_put_contents($receiveFile, $streamData, true);
        }else{
            $ret = false;
        }
        return $ret;
    }
    static function binary_to_file($file){
         $content = $GLOBALS['HTTP_RAW_POST_DATA'];// 需要php.ini设置
        if(empty($content)){
            $content = file_get_contents('php://input');// 不需要php.ini设置，内存压力小
        }
        $ret = file_put_contents($file, $content, true);
        return $ret;
    }



}