<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/28
 * Time: 14:41
 */

namespace app\businessapi\ApiService;


use app\businessapi\controller\Common;
use app\service\BaseService;
use Redis\Redis;
use think\Db;

class MineService extends Common
{
    /**
     * 提现信息 创建
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getMineDepositCreate($params,$merchants_id){
        /*
         *  提现金额 price
         * 提现渠道  1 微信 2 支付宝
         * 收款人姓名 account_name
         * 收款人账号 account_code
         * */
        if(!empty($params)){
            $info = array();

            #商户id 求出省市区  地址
            $params['biz_id'] = $merchants_id;

            #没缓存
            $_where = "status = 1 and id = $merchants_id";
            // 获取列表
            $data_params = array(
                'page'         => false, //是否分页
                'where'     => $_where,//查找条件
            );
            $merchants= MerchantsService::getMerchantsInfo($data_params);



            if(!empty($merchants)){
                $params['province']= $merchants['province'];
                $params['city']= $merchants['city'];
                $params['area']= $merchants['area'];
                $params['address']= $merchants['address'];
                $params['phone']= $merchants['contacts_phone'];
                $params['username']= $merchants['title'];
            }

            if($params['mark'] == 'bondPrice'){
                #商户保证金退回
                $params['source_type'] = 3;//商户来源
                $params['launch_cate'] = 4;//商户提现
                //审核时间
                $info['audit_time'] =  '等待退回';
            }else{
                #商户提现
                $params['source_type'] = 3;//商户来源
                $params['launch_cate'] = 5;//保证金退回
                //审核时间
                $info['audit_time'] =  '等待提现';
                #提现成功 减掉对应商户的余额
                Db::table('merchants')->where(array('id'=>$merchants_id))->setDec('balance',$params['price']);
            }
            unset($params['mark']);

            #添加 商户 提现记录
            $depositId = Db::table('log_deposit')->insertGetId($params);
            if($depositId){
                #修改支付宝账号
                Db::table('merchants')->where(array('id'=>$merchants_id))->update(array('account_code'=>$params['account_code']));
                #删除缓存
                $_redis = new Redis();
                $_redis->hDel("merchants",'merchantsInfo'.$merchants_id);



                #到账时间为空  默认 到账时间时间 =  提现时间+1天
                $info['create_time'] = date('Y-m-d H:i');

                //到账时间
                $info['account_time'] = '等待到账';
                //默认等待审核
                $info['deposit_status'] = 1;

                return $info;

            }

        }
    }
    /**
     * 提现信息 处理
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getMineDepositDetail($arr){
        if(!empty($arr)){
            foreach($arr as $k=>$v){
                if(isset($v['price'])){
                    $arr[$k]['price'] = priceFormat($v['price']);

                }
                if(isset($v['deposit_type'])){
                    if($v['deposit_type'] == 1){
                        $arr[$k]['deposit_type_title'] = '提现至微信';

                    }else{
                        $arr[$k]['deposit_type_title'] = '提现至支付宝';

                    }
                }
                if(isset($v['deposit_status'])){
                    $arr[$k]['deposit_status_title'] = BaseService::StatusHtml($v['deposit_status'],lang('deposit_status'),false);
                }
                if(empty($v['account_time'])){
                    $arr[$k]['account_time'] = '等待到账';
                }
                if(empty($v['audit_time'])){
                    $arr[$k]['audit_time'] = '等待审核';
                }
            }
            return $arr;
        }

    }
    /**
     * 获取当前 商户 的  商户等级 购买记录
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
     */
    static function getMerchantsOrderLevel($type,$merchants_id){
        $checkout_time = Db::table("merchants_order mo")
            ->field('mo.id,mo.checkout_time')
            ->join('merchants_orderdetail md','md.order_id = mo.id','left')
            ->where("mo.order_status = 3 and mo.merchants_id =$merchants_id and mo.pay_type = 5 and md.merchants_level = $type")
            ->find();
        return $checkout_time;
    }
    /**
     * 商家等级 信息
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
     */
    static function getMineExtension($type,$merchants_id){
        #查询当前商户 是否 支付  没支付 返回 没支付 状态  价格   到期时间 checkout_time pay_status:1 支付状态  2 没支付过

//        $checkout_time = self::getMerchantsOrderLevel($type,$merchants_id);
        $expire_time = Db::table('merchants')->where(array('id'=>$merchants_id))->value("DATE_FORMAT(expire_time,'%Y-%m-%d %H:%i') ");

        $arr = self::getMerchantsLevel($type);

        if($type == 1){
            #优质商家

            if(!empty($expire_time)){

                #查询当前等级 是否是推荐级别 2 推荐
                $level =  Db::table('merchants')->where("id = $merchants_id")->value('level');
                if($level == 2){
                    #推荐  支付过 显示价格差   优质- 诚信价格
                    $sincerity = self::getMerchantsLevel(2);
                    #前提 优质商家价格 > 诚信商家价格
                    $arr['continuity_price'] = $arr['continuity_price']-$sincerity['continuity_price'];
                    $arr['continuity_quarter_price'] = $arr['continuity_quarter_price']-$sincerity['continuity_quarter_price'];

//                    $arr['montn_price'] = ($arr['montn_price']-$sincerity['montn_price'])/30;
//                    $arr['quarter_price'] =($arr['quarter_price']-$sincerity['quarter_price'])/90;
//                    $arr['half_price'] = ($arr['half_price']-$sincerity['half_price'])/180;
//                    $arr['year_price'] =(   $arr['year_price']-$sincerity['year_price'])/365;
                    #计算下当前结束时间 剩余 多少天  到期时间-当天时间 = 剩余天数
                    $time = date("Y-m-d");
                    $date=date_create($expire_time);
                    $expire_time_day =  date_format($date,"Y-m-d");
//                    $surplus_day = (int)(($expire_time_day-$time)/(24*3600));
                    $surplus_day = round((strtotime($expire_time) - strtotime($time))/3600/24);
                    #分别计算下 推荐商家 的每天金额
                    #知道商户的最后一次购买的是哪个月的 类型  0 诚信是商家时 默认 为0 1 包月 2 包季 3 一个 月  4三个月 5 半年 6. 一年
                    $type= Db::table('merchants_renew')->where("merchants_id = $merchants_id")->order('id desc')->value('type');

                    #在根据这个月的 每天金额 * 剩余天数 = 购买这个月的剩余钱数（列如购买3个月  这个剩余钱数就是  3个月的剩余钱数）
                    #每个月的金额
                    $month_price = $arr['montn_price'];
                    #3个月的金额
                    $quarter_price = $arr['quarter_price'];
                    $half_price = $arr['half_price'];
                    $year_price = $arr['year_price'];
                    if($surplus_day > 0){
                        if($type == 3){
                            #1个月 剩余钱数
                            $surplus_price = $sincerity['montn_price']/30*$surplus_day;



                        }else if($type == 4){
                            #3个月
                            $surplus_price = $sincerity['montn_price']/90*$surplus_day;

                        }else if($type == 5){
                            #半年
                            $surplus_price = $sincerity['quarter_price']/180*$surplus_day;

                        }else if($type == 6){
                            #1年 365天
                            $surplus_price = $sincerity['year_price']/365*$surplus_day;

                        }
                    }else if($surplus_day == 0){
                        #剩余价格为0
                        $surplus_price = 0;

                    }
                    #返回前端 的 差价 = 当前优质商家的价格 - 剩余钱数   数据库更改 当前时间+ 升级的天数

                    $arr['montn_price'] = priceFormat(($month_price-$surplus_price)) ;

                    $arr['quarter_price'] =priceFormat(($quarter_price-$surplus_price));
                    $arr['half_price'] = priceFormat(($half_price-$surplus_price));
                    $arr['year_price'] =priceFormat(($year_price-$surplus_price));
                    #知道差价  判断差价是否大于 优质商家的价格  大于 返回 status = 0  默认 1  NO:提示 无法升级请选择  其他套餐

                    #差价返回小于等于 0  提示 无法升级请选择  其他套餐
                    if($arr['montn_price'] <=0){
                        $arr['montn_price_status'] = 0;
                        $arr['montn_price'] = 0;
                    }
                    if($arr['quarter_price'] <=0){
                        $arr['quarter_price_status'] = 0;
                        $arr['quarter_price'] = 0;
                    }
                    if($arr['half_price'] <=0){
                        $arr['half_price_status'] = 0;
                        $arr['half_price'] = 0;
                    }
                    if($arr['year_price'] <=0){
                        $arr['year_price_status'] = 0;
                        $arr['year_price'] = 0;
                    }




                }else{
                    $arr['expire_time'] = $expire_time;
                    #计算剩余多少天
                    $arr['day']=round((strtotime($expire_time) - time())/3600/24);
                }



            }else{
                #没有支付过  看是否推荐过推荐商家  没推荐 正常价格  推荐 显示建价格差
                #查询当前等级 是否是推荐级别 2 推荐
                $level =  Db::table('merchants')->where("id = $merchants_id")->value('level');
                if($level == 2){
                    #推荐  支付过 显示价格差   优质- 诚信价格
                    $sincerity = self::getMerchantsLevel(2);
                    #前提 优质商家价格 > 诚信商家价格
                    $arr['continuity_price'] = priceFormat(($arr['continuity_price']-$sincerity['continuity_price']));
                    $arr['continuity_quarter_price'] = priceFormat(($arr['continuity_quarter_price']-$sincerity['continuity_quarter_price']));
                    $arr['montn_price'] = priceFormat(($arr['montn_price']-$sincerity['montn_price']));
                    $arr['quarter_price'] = priceFormat(($arr['quarter_price']-$sincerity['quarter_price']));
                    $arr['half_price'] = priceFormat(($arr['half_price']-$sincerity['half_price']));
                    $arr['year_price'] = priceFormat(($arr['year_price']-$sincerity['year_price']));


                }
                #推荐  没支付过 显示正常价格

                $arr['pay_status'] = 2;


            }
        }else if($type == 2){
            #2推荐商家

            if(!empty($expire_time)){
                $arr['expire_time'] = $expire_time;


            }
        }else if($type == 3){
            #3星级商家(诚信)

            if(!empty($expire_time)){
                $arr['expire_time'] = '永久';
            }
        }
            #4普通商家
        return $arr;

    }
    /**
     * 商家推广订单确认
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params merchants_level : 商户等级  商家推广的商品名称  1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
     * @params checkout_time 到期时间
     * @params cash_type  支付方式  付款方式 1=>'微信',2=>'支付宝',3=>'银联',4=>'现金',5=>'余额',6=>'卡券',7=>'积分'
     * @params pay_type  支付类型  推广时 为 5
     * @params total_price 支付 价格
     * @params actual_price 实际支付 价格
     * @params merchants_id 商户id
     * @params order_number 订单编号
     * @params order_status 订单状态   1待到店订单 (未核销)  2 已到店订单(核销过) 3 推广订单
     */
    static function getMineOrderConfirm($params){
        $data = $params;

        $data['checkout_time'] = $params['checkout_time'];
        $data['total_price'] = $params['actual_price'];
        $data['actual_price'] = $params['actual_price'];
        $data['cash_type'] = $params['cash_type'];
        $data['merchants_id'] = $params['merchants_id'];
        $data['order_number'] = getOrderNumber();;
        $data['order_status'] = $params['order_status'];
        $order_id = Db::table('merchants_order')->insertGetId($data);
        if($order_id){
            $order_detail['merchants_level'] = $params['merchants_level'];
            $order_detail['order_number'] = $data['order_number'];
            $order_detail['order_id'] = $order_id;
            $orderDetail = Db::table('merchants_orderdetail')->insert($order_detail);
            return   array('order_id'=>$order_id,'order_number'=>$data['order_number']);
        }


    }

    /**
     * 保证金 退回记录
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params describe  退回原因
     * @params pay_type  支付方式 1 微信 2 支付宝
     * @params price  保证金
     */
    static function  getMineBondLog($params){
        $data = $params;
        unset($data['contacts'],$data['account_code']);
        if(!empty($params)){
            $resId = Db::table('merchants_bond_log')->insertGetId($params);
            if($resId){
                Db::table('merchants')->where("id = '".$params['merchants_id']."'")->setDec('bond_price',$params['price']);
                Db::table('merchants')->where("id = '".$params['merchants_id']."'")->update(array('level'=>4,'contacts'=>$params['contacts'],'account_code'=>$params['account_code']));
                #退还到余额
//                Db::table('merchants')->where("id = '".$params['merchants_id']."'")->setInc('balance',$params['price']);
            }
            $_where = "id = $resId";
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_bond_log',
                'field' => '*',
                'order' => 'id desc'
            );
            $dataList = BaseService::DataList($data_params)[0];
            if(!empty($dataList)){
                #到账时间为空  默认 到账时间时间 =  提现时间+1天
                $dataList['audit_time'] =  $dataList['create_time'];
                if(empty( $dataList['account_time'])){
                    $dataList['account_time'] = date('Y-m-d H:i:s', strtotime("+24 hours",  strtotime($dataList['create_time'])));

                }
            }
            return $dataList;


        }

    }
    /**
     * 获取商户级别信息
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
     */
    static function getMerchantsLevel($type){
        if($type == 1){
            #优质商家
            $arr['privilege'] = '用户端APP内首页优先展示;\n用户端列表内第1、2位置优先展示;\n用户端列表内底部优先展示;
            \n用户端APP优质商家标签样式展示;\n优先推荐套餐显示与用户端首页;\n优先推荐与其他商家详情页面底部展示;
            \n开通套餐上架管理权限，可发布商家套餐;\n开通抖音拍摄功能，商家版拍摄抖音，获得矩阵流量;\n开通抖音矩阵功能，用户通过商家详情页拍摄抖音送积分;
            \n开通发布宣传视频权限，提升门店曝光;
            \n开通抖音小程序页面。';
            #连续包月  改成2 年
            $arr['continuity_title'] = '二年';
            $arr['continuity_price'] = '2888.00';
            #划线价格
            $arr['old_continuity_price'] = '4588.00';
            $arr['continuity_price_describe'] = '到期后自动停止';


            #连续包季  改成 3 年
            $arr['continuity_quarter_title'] = '三年';
            $arr['continuity_quarter_price'] = '3888.00';
            #划线价格
            $arr['old_continuity_quarter_price'] = '5588.00';
            $arr['continuity_quarter_price_describe'] = '到期后自动停止';


            #一个月
            $arr['montn_title'] = '一个月';
            $arr['montn_price'] = '128.00';
            $arr['old_montn_price'] = '299.00';


            #3个月
            $arr['quarter_title'] = '三个月';
            $arr['quarter_price'] = '298.00';
            $arr['old_quarter_price'] = '897.00';


            #半年
            $arr['half_title'] = '半年';
            $arr['half_price'] = '498.00';
            $arr['old_half_price'] = '1794.00';


            #一年
            $arr['year_title'] = '一年';
            $arr['year_price'] = '898.00';
            $arr['old_year_price'] = '3588.00';
            $arr['describe'] = '到期后自动停止';

        }else if($type == 2){
            #2推荐商家
            $arr['privilege'] = '用户端列表内第3位置优先展示;
                                \n用户端列表内第8、9位置优先展示;
                                \n详情页面内增加“消费担保”提示;
                                \n用户端APP诚信商家标签样式展示;
                                \n开通套餐上架管理权限，可发布商家套餐;
                                \n开通抖音拍摄功能，商家版拍摄抖音，获得矩阵流量;
                                \n开通发布宣传视频权限，提升门店曝光;
                                \n开通抖音小程序页面';
            #连续包月  改成2 年
            $arr['continuity_title'] = '二年';
            $arr['continuity_price'] = '2888.00';
            #划线价格
            $arr['old_continuity_price'] = '4588.00';
            $arr['continuity_price_describe'] = '到期后自动停止';
            #连续包季  改成 3 年
            $arr['continuity_quarter_title'] = '三年';
            $arr['continuity_quarter_price'] = '3888.00';
            #划线价格
            $arr['old_continuity_quarter_price'] = '5588.00';
            $arr['continuity_quarter_price_describe'] = '到期后自动停止';


            #一个月
            $arr['montn_title'] = '一个月';
            $arr['montn_price'] = '68.00';
            $arr['old_montn_price'] = '99.00';

            #3个月
            $arr['quarter_title'] = '三个月';
            $arr['quarter_price'] = '198.00';
            $arr['old_quarter_price'] = '297.00';

            #半年
            $arr['half_title'] = '半年';
            $arr['half_price'] = '388.00';
            $arr['old_half_price'] = '688.00';

            #一年
            $arr['year_title'] = '一年';
            $arr['year_price'] = '594.00';
            $arr['old_year_price'] = '1288.00';
            $arr['describe'] = '到期后自动停止';

        }else if($type == 3){
            #3星级商家(诚信)
            $arr['privilege'] = '成为诚信商家，用户更信任;
                                \n用户端APP诚信商家标签样式展示;
                                \n详情页面内增加“消费担保”提示;
                                \n开通套餐上架管理权限，可发布商家套餐;
                                \n开通发布宣传视频权限，提升门店曝光;
                                \n可升级为推荐商家或优质商家进行推广曝光。';

            #保证金额
            $arr['price'] = '200.00';
            $arr['describe'] = '这是诚信商家描述';

        }else if($type == 4){
            #4普通商家
            $arr['privilege'] = '免费入驻，无需任何费用;\n用户端APP内基于用户当前位置就近展示;\n用户端APP无商家推荐等;\n平台不针对普通商家进行推荐及推广;\n商家除平台推广外所有功能正常使用。';
        }
        return $arr;
    }

    /************** 员工  信息***************/
    /*
     * @content 员工  创建 修改
     * @params id 员工id
     * @params staff_name  姓名
     * @params staff_phone   手机号
     * @params staff_station  岗位名称
     * @params staff_power 员工权限 数组
     * @params merchants_id 商户id
     * */
    static function getMineStaffSave($params){
        if(isset($params['staff_power'])){
            foreach($params['staff_power'] as $k=>$v){
                if(empty($v)){
                    unset($params['staff_power'][$k]);
                }
            }
        }
        $params['staff_power'] = implode(',',$params['staff_power']);


        if(!empty($params['id'])){
            #判断手机号是否重复
            if( Db::table('merchants_staff')->where("staff_phone = '".$params['staff_phone']."' and  id != '".$params['id']."'")->value('id')){
                return false;
            }

            #修改
            Db::table('merchants_staff')->where(array('id'=>$params['id']))->update($params);
        }else{
            #判断手机号是否重复
            if( Db::table('merchants_staff')->where("staff_phone = '".$params['staff_phone']."'")->value('id')){
                return false;
            }
            #创建
            Db::table('merchants_staff')->insert($params);
        }
        return true;

    }
    /*
     * @content 获取商户密码
     * @params $merchants_id 商户id
     * */
    static function getMerchantsPassword($merchants_id){
        $data = Db::table('merchants_password')->where(array('merchants_id'=>$merchants_id))->find();
        return $data;

    }
    /*
     * @content 商户密码修改
     * @params $merchants_id 商户id
     * */
    static function getMinePasswordWrite($params){
        $data = Db::table('merchants_password')->where(array('merchants_id'=>$params['merchants_id']))->find();
        if(!empty($data)){
            Db::table('merchants_password')->where(array('merchants_id'=>$params['merchants_id']))->update($params);
        }else{
            Db::table('merchants_password')->where(array('merchants_id'=>$params['merchants_id']))->insert($params);
        }

        return true;

    }
    /*
     * @content :商户协议等
     * @param  mark 1合作 协议 2  关与平台  3 首页 收款码 图片 4 . 首页推荐会员图片 5  商户管理 门店活动 图图片 6 .商户管理 门店 优惠券 图 7 . 我的页 中间  推荐E联车服会员赚收益图 8 .客服电话
     * @params type = 1 商户
     * */
    static function getMerchants_application($mark){
        if($mark == 'about'){
            $content = Db::table('merchants_application')->where("type = 1 and mark = 2 or mark = 9 or mark = 8")->field('content,mark')->select();
            if(!empty($content)){
                foreach ($content as $k=>$v){
                    if($v['mark'] == 2){
                        $content[$k]['title'] ='公司简介';

                    }else if($v['mark'] == 8){
                        $content[$k]['title'] ='公司优势';
                    }else{
                        $content[$k]['title'] ='联系电话';
                    }

                }
            }
        }else{
            $content = Db::table('merchants_application')->where(array('mark'=>$mark,'type'=>1))->value('content');
        }
//        dump($content);die;
        return $content;


    }
    /*
    * @content 获取商户密码
    * @params $merchants_id 商户id
    * */
    static function handleMerchantsIncome($params){
        if(!empty($params)){
            foreach ($params as &$v){
                //支付方式
                $v['pay_type'] = BaseService::StatusHtml($v['method'],lang('pay_type'),false).'支付';
                $v['order_price'] = priceFormat($v['order_price']);
                //升级后会员名称
                $card = explode(',',$v['order_number']);
                $level_title = '用户';
                if(!empty($card[1])){
                    $level_title = Db::table('member_level')->where(array('id'=>$card[1]))->value('level_title');
                    if($card[1] == 1 or $card[1] == 2){
                        $level_title = $level_title;
                    }

                }else{
                    if(!empty($card[0])){
                        $level_title = Db::table('member_level')->where(array('id'=>$card[0]))->value('level_title');
                        if($card[0] == 1 or $card[0] == 2){
                            $level_title = $level_title;
                        }

                    }
                }
                if($v['type'] == 2){

                    $card_title = '办理';
                    $v['level_title'] = $card_title.$level_title;
                }else if($v['type'] == 4){
                    $card_title = '用户充值';

                }else if($v['type'] == 3){
                    $card_title = '升级';
                    $v['level_title'] = $card_title.$level_title;

                }



            }
            return $params;
        }else{
            return '';
        }

    }
}
