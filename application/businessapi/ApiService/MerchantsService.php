<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/24
 * Time: 15:59
 */

namespace app\businessapi\ApiService;

/*
 * @contetn 商户管理基层方法  前端
 * */


use app\businessapi\controller\Getimg;
use app\businessapi\controller\Message;
use app\businessapi\controller\PayMent;
use app\service\BaseService;
use app\service\OssService;
use app\service\ResourceService;
use app\service\SmsCode;
use Redis\Redis;
use think\Db;

class MerchantsService
{
    /**
     * [getMerchantsInfo 获取商户信息]
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getMerchantsInfo($params)
    {
        #where条件  写成活的
        $where = $params['where'];
        #分页条数
        $number = isset($params['number']) ? intval($params['number']) : 10;

        #获取门店列表信息
        $data = Db::table('merchants')
            ->field("*,DATE_FORMAT(expire_time,'%Y-%m-%d') expire_time")
            ->where($where)
            ->order('create_time desc')->find();

        return self::InforDataDealWith($data, $params);

    }

    #处理方法
    static function InforDataDealWith($data, $params)
    {
        if (!empty($data)) {

            #根据商户id  查出 分类名称 数组 拼接显示
            $cate_relation = Db::table('merchants_cate_relation')->where(array('merchants_id' => $data['id']))->field('cate_pid,cate_id,id')->select();
            //分类
            $data['cate_title_title'] = '--';
            $cate_ptitle_0 = '';
            $cate_ptitle_1 = '';
            $cate_ptitle_2 = '';
            $cate_ptitle_cate1 = '';
            $cate_ptitle_cate2 = '';
            $cate_ptitle_cate3 = '';
            if ($cate_relation[0]['cate_pid'] > 0) {
                if (isset($cate_relation[0]['cate_pid'])) {
                    $cate_ptitle_0 = Db::name('merchants_cate')->where('id=' . $cate_relation[0]['cate_pid'])->value('title');

                }
                if (isset($cate_relation[1]['cate_pid'])) {
                    $cate_ptitle_1 = Db::name('merchants_cate')->where('id=' . $cate_relation[1]['cate_pid'])->value('title');

                }
                if (isset($cate_relation[2]['cate_pid'])) {
                    $cate_ptitle_2 = Db::name('merchants_cate')->where('id=' . $cate_relation[2]['cate_pid'])->value('title');

                }
                if ($cate_relation[0]['cate_id'] > 0) {
                    if (isset($cate_relation[0]['cate_id'])) {
                        $cate_ptitle_0 .= '-' . Db::name('merchants_cate')->where('id=' . $cate_relation[0]['cate_id'])->value('title');
                        $cate_ptitle_cate1 = Db::name('merchants_cate')->where('id=' . $cate_relation[0]['cate_id'])->value('title');

                    }
                    if (isset($cate_relation[1]['cate_id'])) {
                        $cate_ptitle_1 .= '-' . Db::name('merchants_cate')->where('id=' . $cate_relation[1]['cate_id'])->value('title');
                        $cate_ptitle_cate2 = '-' . Db::name('merchants_cate')->where('id=' . $cate_relation[1]['cate_id'])->value('title');


                    }
                    if (isset($cate_relation[2]['cate_id'])) {
                        $cate_ptitle_2 .= '-' . Db::name('merchants_cate')->where('id=' . $cate_relation[2]['cate_id'])->value('title');
                        $cate_ptitle_cate3 = '-' . Db::name('merchants_cate')->where('id=' . $cate_relation[2]['cate_id'])->value('title');

                    }

                }


                #一级分类 与二级分类 名称 拼接
                $data['cate_title_title'] = $cate_ptitle_0 . '/' . $cate_ptitle_1 . '/' . $cate_ptitle_2;
                #前端展示的分类名称拼接
                $data['cate_ptitle_cate'] = $cate_ptitle_cate1 . $cate_ptitle_cate2 . $cate_ptitle_cate3;
            }


            #商户分类 名称  最高级  子集

            #标签 label
            $data['keywords_name'] = array();
            if (!empty($data['keywords'])) {
                $data['keywords'] = explode('，', $data['keywords']);
                $keywords_name = array();
                $keywords = $data['keywords'];
                if (!empty($keywords)) {
                    foreach ($keywords as $k => $v) {
                        $keywords_name[$k]['id'] = $k;
                        $keywords_name[$k]['name'] = $v;

                    }
                    $data['keywords_name'] = $keywords_name;
                }
            }


            #获取商户 二级分类名称  最多3个
            $cate = Db::table("merchants_cate_relation mcr")
                ->field("(select mc.title from merchants_cate mc where mcr.cate_id = mc.id and mc.status = 1) cate_title")
                ->where(array('merchants_id' => $data['id']))->select();
            $data['cate_title'] = $cate;


            #商家类型
            if (isset($data['is_type'])) {
                if ($data['is_type'] == 1) {
                    $data['is_type_title'] = '本地生活';
                } else {
                    $data['is_type_title'] = '联盟商家';

                }
            }
            $data['merchants_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
            #抽成比列
            if (isset($data['commission'])) {
                $data['commission'] = ($data['commission'] * 100) . '%';
            }
            //商户类型
            #把营业时间放到一起
            if (isset($data['start_time'])) {

                $start_time = date_create($data['start_time']);
                $end_time = date_create($data['end_time']);
                $data['start_time'] = date_format($start_time, " H:i");
                $data['end_time'] = date_format($end_time, " H:i");
                $data['opencreate'] = date_format($start_time, " H:i") . '-' . date_format($end_time, " H:i");
            }
            #商户详情
            if (!empty($data['remarks'])) {
                $data['remarks'] = explode('*', $data['remarks']);
            }
            #获取 商户等级
            if (isset($data['level'])) {
                $data['level_title'] = BaseService::StatusHtml($data['level'], lang('merchants_level'), false);

            }
            #获取颜色
            if (isset($data['ad_color'])) {
                $data['ad_color'] = getColorCase($data['ad_color']);
            }
            if (isset($data['backgroud_color'])) {
                $data['backgroud_color'] = getColorCase($data['backgroud_color']);
            }
            #查询是否到期
            $expire_time = Db::table('merchants')->where(array('id' => $data['id']))->value("DATE_FORMAT(expire_time,'%Y-%m-%d %H:%i') ");
            $time = date("Y-m-d H:i");
            if ($expire_time <= $time) {
                if ($data['level'] != 4) {
                    #修改 商户状态 为诚信商家  清商户缓存
                    Db::table('merchants')->where(array('id' => $data['id']))->update(array('level' => 3));
                }

            }
            if ($data['level'] == 3) {
                $data['expire_time'] = '退回前一直有效';
            }
            #地址
            if (isset($data['province']) and isset($data['city']) and isset($data['area']) and isset($data['address'])) {
                $data['_address'] = $data['province'] . $data['city'] . $data['area'] . $data['address'];
            }
        }
        return $data;

    }

    /*
     * @content ：创建  修改 商户优惠券
     * @params : 优惠券名称    title
     * @params : 面值金额      price
     * @params :  门槛金额     min_consume
     * @params : 有效期天数    validity_day
     * @params : 开始时间      start_time
     * @params : 结束时间      end_time
     * @params : 发放数量      num
     * @params : 重复领取      is_repeat   1 是 2 否
     * @params : $this->merchants_id     商户id
     * */
    static function getSaveMerchantsVoucher($params, $merchants_id)
    {
        if (empty($params['id'])) {
            #添加
            if (!empty($params)) {
                $params['merchants_id'] = $merchants_id;
                $params['surplus_num'] = $params['num'];
                $data = Db::table('merchants_voucher')->insertGetId($params);
                return array('status' => true);

            }
        } else {
            #修改
            if (!empty($params)) {
                #计算下  num 是 增加  了还是 减少了  同时  剩余数量  也  增加 和 减少
                $oldnum = Db::table('merchants_voucher')->where(array('id' => $params['id']))->field('num,surplus_num')->find();
                $add_num = intval($params['num']) - intval($oldnum['num']);
                $params['surplus_num'] = intval($oldnum['surplus_num']) + $add_num;


                $data = Db::table('merchants_voucher')->where(array('id' => $params['id']))->update($params);
                return array('status' => true);

            }
        }

    }

    /*
   * @content  商户管理 门店优惠券 删除
   * @return  array
   * */
    static function getMerchantsVoucherDel($params)
    {
        Db::table('merchants_voucher')->where(array('id' => $params['id']))->update(array('status' => 2));
        return true;
    }

    /*
     * @content  商户管理 门店优惠券 信息处理
     * @return  array
     * */
    static function DataDetail($data, $time = '')
    {


        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if ($time < $v['start_time'] and $time < $v['end_time']) {
                    #当前时间 小于 开始时间 和结束时间
                    $data[$k]['active_status'] = '待上架';

                } else if ($time >= $v['start_time'] and $time <= $v['end_time']) {
                    #当前时间 大于 等于开始 下雨等于 结束
                    $data[$k]['active_status'] = '发放中';

                } else if ($time > $v['start_time'] and $time > $v['end_time']) {
                    #当前时间 大于 开始时间 和结束时间
                    $data[$k]['active_status'] = '已结束';

                }
                if ($v['surplus_num'] == 0) {
                    $data[$k]['active_status'] = '已结束';
                }
            }
        }
        return $data;
    }

    /*
    * @content  商户管理 修改信息处理
    * @return  array
    * */
    static function getMerchantsSave($params, $merchants_id)
    {

        #判断是否有新上传的门头照片--base64格式  上传  有则修改
        /*
         * thumb  新上传的门头照片
         * oldthumb 旧门头图片
         * end_time 营业结束时间
         * start_time 营业开始时间
         * temporary_close  临时打烊时间 小时
         * contacts_phone  联系手机号
         * tel  门店电话
         * keywords  标签
         * ad_words  后台广告语   前端门店活动编辑  在前端展示的轮播信息
         * ad_color  广告语颜色   前端门店活动编辑底色
         * is_voice  语音通知提醒 1 开  2 关
         * */
        $oss = new OssService();
        if (!empty($params['thumb'])) {
            #旧门头图片  $params['oldthumb'];
            $oldthumb = $params['oldthumb'];
            unset($params['oldthumb']);
            $thumb_img = $params['thumb'][0];
            if (empty($merchants_id)) {
                #创建时 前端传的是 字符串
                $thumb_img = $params['thumb'];
            }

            #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
            $imgUrl = ResourceService::uploadBase($thumb_img);
            // $imgUrl原图文件名  压缩文件
            $image = (new Getimg())->getThumb($imgUrl, '500', '500');
            #返回的图片路径上传到 oss 服务器
            $thumb = $oss->OssUpload($image);

            $params['thumb'] = $thumb;
            # 删除 上传的门头图片
            @unlink(ROOT_PATH . 'upload/' . $imgUrl);
            @unlink(ROOT_PATH . 'upload/' . $image);

        }
        #商户详情
        if (isset($params['remarks'])) {

            #* 分割 前端传的详情图
            $remarks = $params['remarks'];
            if (!empty($merchants_id)) {
                $_where = "status = 1 and id = $merchants_id";
                // 获取列表
                $data_params = array(
                    'page' => false, //是否分页
                    'where' => $_where,//查找条件
                );
                $data = self::getMerchantsInfo($data_params);
                #数据库中的 商户详情图
                $merchants_remarks = $data['remarks'];

                #删除 前端删除的图片（删除服务器上的图片）  前端跟后台做对比
                if (empty($merchants_remarks)) {
                    $merchants_remarks = array();
                }


                foreach ($merchants_remarks as $k => $v) {
                    if (!in_array($v, $remarks)) {
                        #看数据库中的 地址 是否在 前端地址中 不在删除 oss 中的图片
                        $oss->OssDel($v);
                    }
                }
                #整理前端的图片地址 存到数据库
                foreach ($remarks as $rk => $rv) {
                    if (!in_array($rv, $merchants_remarks)) {
                        #看 前端的地址 是跟 数据库中的地址相同  不相同 看是否有base64 存在 则上传

                        $base = explode(',', $rv);
//                    if($base[0] =='data:image/jpeg;base64'){
                        #把地址存到服务器
                        #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
                        $imgUrl = ResourceService::uploadBase($rv);
                        // $imgUrl原图文件名  压缩文件
                        $image = (new Getimg())->getThumb($imgUrl, '500', '1000');

                        #返回的图片路径上传到 oss 服务器
                        $remarks[$rk] = $oss->OssUpload($image);

                        # 删除 上传的缩略图图片
                        @unlink(ROOT_PATH . 'upload' . $imgUrl);
                        @unlink(ROOT_PATH . 'upload' . $image);

//                    }

                    }
                }
            } else {
                #整理前端的图片地址 存到数据库
                foreach ($remarks as $rk => $rv) {
                    #看 前端的地址 是跟 数据库中的地址相同  不相同 看是否有base64 存在 则上传

                    $base = explode(',', $rv);
                    if ($base[0] == 'data:image/jpeg;base64' or $base[0] == 'data:image/png;base64') {
                        #把地址存到服务器
                        #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
                        $imgUrl = ResourceService::uploadBase($rv);
                        // $imgUrl原图文件名  压缩文件
                        $image = (new Getimg())->getThumb($imgUrl, '500', '1000');

                        #返回的图片路径上传到 oss 服务器
                        $remarks[$rk] = $oss->OssUpload($image);

                        # 删除 上传的缩略图图片
                        @unlink(ROOT_PATH . 'upload' . $imgUrl);
                        @unlink(ROOT_PATH . 'upload' . $image);

                    }

                }
            }


            #*号拼接
            $params['remarks'] = implode('*', $remarks);

        }

        #获取前端颜色
        #广告语颜色

        if (isset($params['ad_color'])) {
            $params['ad_color'] = self::getColor($params['ad_color']);
        } else {
            unset($params['ad_color']);
        }
        #底图颜色
        if (isset($params['backgroud_color'])) {
            $params['backgroud_color'] = self::getColor($params['backgroud_color']);
        } else {
            unset($params['backgroud_color']);
        }
        if (empty($params['ad_words'])) {
            unset($params['ad_words']);
        }
        #获取设备id
        if (!empty($params['equipment_id'])) {
            $equipment_id = Db::table('merchants')->where(array('id' => $merchants_id))->value('equipment_id');
            if (!empty($equipment_id)) {
                unset($params['equipment_id']);

            }
        } else {
            unset($params['equipment_id']);

        }
        #经纬度 根据地址获取
        if (!empty($merchants_id)) {
            $str = array();
            if (!empty($params['keywords_name'])) {
                foreach ($params['keywords_name'] as $k => $v) {
                    $str[$k] = $v['name'];

                }
                $params['keywords'] = implode('，', $str);

            }
            unset($params['keywords_name']);
            #修改
            $data = Db::table('merchants')->where("id=$merchants_id")->update($params);
            # 查询是否为物业
            $info = Db::table('merchants')->field("contacts_phone,tel,nh_id")->where("id=$merchants_id")->find();
            if ($info['nh_id'] > 0) {
                Db::table('property_neighbourhood')->where(array("id" => $info['nh_id']))->update(array("telphone" => $info['contacts_phone'], "custom_tel" => $info['tel']));
            }
        } else {
            #创建
            #获取营业执照图 license_imgurl
            if (!empty($params['license_imgurl'])) {


                #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
                $imgUrl = ResourceService::uploadBase($params['license_imgurl']);
                // $imgUrl原图文件名  压缩文件
                $image = (new Getimg())->getThumb($imgUrl, '500', '500');
                #返回的图片路径上传到 oss 服务器
                $thumb = $oss->OssUpload($image);

                $params['license_imgurl'] = $thumb;
                # 删除 上传的门头图片
                @unlink(ROOT_PATH . 'upload/' . $imgUrl);
                @unlink(ROOT_PATH . 'upload/' . $image);

            }
            #把获取到 省市区 变成经纬度存上
            if (!empty($params['province'] and !empty($params['city']) and !empty($params['area']) and !empty($params['address']))) {
                $address = $params['province'] . $params['city'] . $params['area'] . $params['address'];
                $result_local = getLonLat($address, '');    //获取地区编码
                $location = $result_local['geocodes'][0]['location'];
                $location_ay = explode(",", $location);
                $params['lon'] = $location_ay[0];                //经度
                $params['lat'] = $location_ay[1];                //纬度


            }
            if (!empty($params['cate_id'])) {
                unset($params['cate_id']);

            }

            $data = Db::table('merchants')->insertGetId($params);
            #获取商户分类 数组
            if (isset($params['cate_id'])) {
                $data = 1;
                foreach ($params['cate_id'] as $v) {
                    $cate = explode('-', $v);
                    $arr['cate_pid'] = $cate[0];//一级分类id
                    $arr['cate_id'] = $cate[1];// 二级分了id
                    $arr['merchants_id'] = $data;

                    Db::table('merchants_cate_relation')->insert($arr);
                }
            }


        }


        if (!empty($oldthumb)) {
            #删除 原来的门头照片
            $oss->OssDel($oldthumb);
        }

        return $data;

    }

    /*
    * @content  商户管理 推荐管理 创建修改 信息处理
    * @return  array
    * */
    static function getRecommendProSave($params, $merchants_id)
    {


        /*
         * 缩略图 imgurl
         * 商品名称  title
         * 商品价格 price
         * 商品原价  original_price
         * 上架数量 num
         * 开始时间 start_time
         * 结束时间 end_time
         * 商品描述 详情 detailimage
         * merchants_id
         * */
        $params['merchants_id'] = $merchants_id;
        $oss = new OssService();


        if (!empty($params['imgurl'][0])) {
            #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
            $imgUrl = ResourceService::uploadBase($params['imgurl'][0]);
            // $imgUrl原图文件名  压缩文件
            $image = (new Getimg())->getThumb($imgUrl, '500', '500');

            $thumb = '';
            if (!empty($imgUrl)) {
                #返回的图片路径上传到 oss 服务器
                $thumb = $oss->OssUpload($image);
            }


            $params['imgurl'] = $thumb;
            # 删除 上传的缩略图图片
            @unlink(ROOT_PATH . 'upload/' . $imgUrl);
            @unlink(ROOT_PATH . 'upload/' . $image);
        }

        #商品描述详情  前端怎么传呢? 后台怎么接收
        if (!empty($params['detailimage'])) {

            $isddetail = self::getIsDetail($params['detailimage']);

            #为空  说明上传的时候没图
            if (!empty($isddetail)) {
                $params['detailimage'] = self::getUrl($params['detailimage']);


            }
        }

        if (empty($params['id'])) {
            #添加
            $a = Db::table('merchants_pro')->strict(false)->insertGetId($params);


        } else {
            #修改
            Db::table('merchants_pro')->where(array('id' => $params['id']))->update($params);
            $oldimgurl = $params['oldimgurl'];
            if (!empty($oldimgurl)) {
                #删除 原来的门头照片
                $oss->OssDel($oldimgurl);
            }


        }
        return true;

    }

    static function getRecommendProSaves($params, $merchants_id)
    {

        $params['merchants_id'] = $merchants_id;
        $oss = new OssService();
        # banner 处理
        if (!empty($params['banner'])) {
            foreach ($params['banner'] as &$v) {
                $local_url = ResourceService::uploadBase($v);
                if (!empty($local_url) and !is_array($local_url)) {
                    $v = $oss->OssUpload($local_url);
                    @unlink(ROOT_PATH . 'upload/' . $local_url);
                }
            }
            # 缩略图取banner图第一个
            $params['imgurl'] = $params['banner'][0];
            $params['banner'] = implode(',', $params['banner']);
        }
        #商品描述详情  前端怎么传呢? 后台怎么接收
        if (!empty($params['detailimage'])) {
            foreach ($params['detailimage'] as &$v) {
                $local_url = ResourceService::uploadBase($v);
                if (!empty($local_url) and !is_array($local_url)) {
                    $v = $oss->OssUpload($local_url);
                    @unlink(ROOT_PATH . 'upload/' . $local_url);
                }
            }
            $params['detailimage'] = implode(',', $params['detailimage']);
        }
        if (!empty($params['tips'])) {
            $params['tips'] = implode(',', $params['tips']);
        }
        if (!empty($params['describes'])) {
            $params['describes'] = json_encode($params['describes']);
        }
        if (!empty($params['notice'])) {
            $params['notice'] = json_encode($params['notice']);
        }
        //$params['commission'] = floatval($params['comm']);
        unset($params['end_time1'], $params['surplus_num']);
        if (empty($params['id'])) {
            #添加验证相同商家名称是否重复
            $vali = Db::table("merchants_pro")->where(array("merchants_id" => $merchants_id, "title" => $params['title'], "status" => 1))->find();
            if (!empty($vali)) {
                return array("status" => false, "msg" => "该商品已存在");
            }
            $a = Db::table('merchants_pro')->insertGetId($params);
            # 发短信---9、商家优惠套餐上架短信(·用户与商家套餐商家所在相同行政区域·用户与联盟商家同一代理关联关系·联盟商家上架新增套餐)
            # 查询商家信息
//            $merchantsInfo = Db::table('merchants')->where(array('id' => $merchants_id))->find();
//            $memberInfo = Db::table('member m')
//                ->field('m.member_phone')
//                ->where('m.member_province = "' . $merchantsInfo['province'] . '" and m.member_city = "' . $merchantsInfo['city'] . '"')
//                ->select();
//            if (!empty($memberInfo)) {
//                $SmsCode = new SmsCode();
//                $smsInfo = '提醒：（' . $merchantsInfo['title'] . '）上架新优惠啦~（' . $params['title'] . '）原价：' . $params['original_price'] . '，特价：' . $params['price'] . '，E联车服使用抵用券下单更优惠！';
//                foreach ($memberInfo as $k => $v) {
//                    $SmsCode->Ysms($v['member_phone'], $smsInfo, false);
//                }
//            }
        } else {
            #修改
            $vali = Db::table("merchants_pro")->where("id != {$params['id']}")->where(array("merchants_id" => $merchants_id, "title" => $params['title'], "status" => 1))->find();
            if (!empty($vali)) {
                return array("status" => false, "msg" => "该商品已存在");
            }
            # 判断下架时间是否小于之前的时间
            $info = Db::table("merchants_pro")->where("id = {$params['id']}")->find();
            if ($params['end_time'] < $info['end_time']) {
                return array("status" => false, "msg" => "下架时间不可小于当前时间");
            }
            Db::table('merchants_pro')->where(array('id' => $params['id']))->update($params);
        }
        return array("status" => true, "msg" => "保存成功");

    }

    /*
      * @content  商户管理 推荐管理信息查询
      * @return  array
      * */
    static function getRecommendPro($params, $merchantsId)
    {
        /*
         * status  状态筛选 0 全部 1 待上架 2 销售中 3 已结束 4 已售罄
         *
         * */

        $time = date("Y-m-d H:i:s");
        $_where = "status!=2 and merchants_id = $merchantsId";
        if (!empty($params['id'])) {
            $_where .= " and id = '" . $params['id'] . "'";
        }
        if (!empty($params['status'])) {
            if ($params['status'] == 1) {
                #待上架
                $_where .= " and '$time' < start_time and '$time' < end_time and status=1";

            } else if ($params['status'] == 2) {
                #销售中
                $_where .= " and '$time' >= start_time and '$time' <= end_time and status=1 and num>sold_num";
            } else if ($params['status'] == 3) {
                #已结束
                $_where .= " and (('$time' > start_time and '$time' > end_time) or status=3)";
            } else if ($params['status'] == 4) {
                $_where .= " and status=1 and (num-sold_num)<=0";
            }
        }
        $_order = "recommend_status asc,create_time desc";
        #排序  asc 正序   desc  倒序(从大到小)
        if (!empty($params['mark'])) {
            $mark = $params['mark'];
            #销量排序 sort_num
            if (!empty($params['sort_num'])) {
                $_order = "sold_num $mark";

            }
            #价格排序 sort_price
            if (!empty($params['sort_price'])) {
                $_order = "price $mark";
            }
        }

        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_pro',
            'field' => 'id,title,price,original_price,num,start_time,end_time,detailimage,sold_num,imgurl,(num - sold_num) surplus_num,tips,describes,notice,banner,commission,status',
            'order' => $_order
        );
        $data = BaseService::DataList($data_params);
        if (!empty($data)) {
            foreach ($data as &$v) {
                if ($v['status'] == 1) {
                    if ($time < $v['start_time'] and $time < $v['end_time']) {
                        $v['status_title'] = '待上架';
                        $v['status_type'] = 1;

                    } else if ($time >= $v['start_time'] and $time <= $v['end_time']) {
                        $v['status_title'] = '展示中';
                        $v['status_type'] = 2;

                    } else if ($time > $v['start_time'] and $time > $v['end_time']) {
                        $v['status_title'] = '已下架';
                        $v['status_type'] = 3;

                    }
                } else if ($v['status'] == 3) {
                    $v['status_title'] = '已下架';
                    $v['status_type'] = 3;
                }

                #判断商品剩余数量 等于0 小于0  显示已下架
                if ($v['surplus_num'] <= 0) {
                    $v['status_title'] = '已售罄';
                    $v['status_type'] = 4;
                }
                $v['detailimage'] = explode(',', $v['detailimage']);
                $v['banner'] = explode(',', $v['banner']);
                if (!empty($v['tips'])) {
                    $v['tips'] = explode(',', $v['tips']);
                } else {
                    $v['tips'] = [];
                }
                $v['end_time'] = date("Y-m-d", strtotime("+1 day", strtotime($v['end_time'])));
                $v['describes'] = json_decode($v['describes'], true);
                $v['notice'] = json_decode($v['notice'], true);
                $v['discount'] =strval(round($v['price'] / $v['original_price'],2) * 10) ;
                $v['end_time1'] = array("d" => '00', "h" => "00", "m" => '00', "s" => "00");
            }
        }
        /*
         * 名称 :title
         * 缩略图 :imgurl
         * 价格 :original_price
         * 上架数量 :num
         * 名称 :title
         * 已售数量 :sold_num
         * 剩余数量 :surplus_num
         * 推荐商品id :surplus_num

         * */
        return $data;


    }

    /*
      * @content  商户管理 推荐管理详情  修改推荐商品 走这个
      * @return  array
      * */
    static function getRecommendProDetail($id)
    {

        $_where = "status= 1  and id = $id";

        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_pro',
            'field' => 'id,title,price,original_price,num,start_time,end_time,detailimage,sold_num,imgurl,(num - sold_num) surplus_num',
            'order' => 'create_time desc'
        );
        $data = BaseService::DataList($data_params)[0];
        $data['detail_context'] = mb_substr(strip_tags(preg_replace('/<img[^>]+>/i', '', $data['detailimage'])), 0, 30, "utf-8");
        /*
         * 名称 :title
         * 缩略图 :imgurl
         * 价格 :original_price
         * 上架数量 :num
         * 名称 :title
         * 已售数量 :sold_num
         * 剩余数量 :surplus_num
         * 推荐商品id :surplus_num
         * 投放时间 :start_time
         * 投放结束时间 :end_time

         * */
        return $data;


    }

    /*
      * @content  判断上传的详情是否 有图片
      * @return  array
      * */
    static function getIsDetail($detailimage)
    {
        preg_match_all('/<img[^>]*?src=".*?([^"]*?)"[^>]*?>/i', $detailimage, $match);
        return $match[0];

    }

    /*
      * @content  商户管理 推荐管理详情  获取前端传过来的 富文本图片 地址
      * @return  array
      * */
    static function getUrl($detailimage)
    {
        preg_match_all('/<img[^>]*?src=".*?base64,([^"]*?)"[^>]*?>/i', $detailimage, $match);
        $img = is_array($match[1]) ? implode(',', $match[1]) : $match[1];
        $arr = explode(',', $img);
        #$arr[0] 的意思是  替换完事  $arr[0]   是空的

        if (!empty($arr[0])) {
            #oss服务器上传
            $oss = new OssService();
            $baseurl = "data:image/jpeg;base64," . $arr[0];
            #接收base64 图片 存到文件夹中 在上传到oss 服务器上  在删除本地存的图片
            $imgUrl = ResourceService::uploadBase($baseurl);
            // $imgUrl原图文件名  压缩文件
            $image = (new Getimg())->getThumb($imgUrl, '500', '500');
            #返回的图片路径上传到 oss 服务器
            $ossurl = $oss->OssUpload($image);
            # 删除 本地上传的图片
            @unlink(ROOT_PATH . 'upload/' . $imgUrl);
            @unlink(ROOT_PATH . 'upload/' . $image);
            #替换   把返回的路径 把原来的标签替换
            $result = substr_replace($detailimage, $ossurl, strpos($detailimage, $baseurl), strlen($baseurl));
            #替换之后  走回调
            return self::getUrl($result);
        } else {
            return $detailimage;
        }

    }

    /*
     * @content 获取前端颜色 跟前端颜色 的数组是一样的
     * */
    static function getColor($key)
    {
        $arr = array(
            '0' => "#FFFFFF",
            '1' => "#999999",
            '2' => "#333333",
            '3' => "#E7373B",
            '4' => "#F6BC58",
            '5' => "#F3F015",
            '6' => "#8CC247",
            '7' => "#129295",
            '8' => "#3091F2",
            '9' => "#3DDEFB",
            '10' => "#EE85BF",
            '11' => "#9155D3",
            '12' => "#0A127C",
            '13' => "#6D1313",
        );
        return $arr[$key];

    }

    /*
     * @content 发送商户到期时间提醒 15  5 当天
     * */
    static function Expirtiondetail($params)
    {
        $message = new Message();
        $time = date("Y-m-d");
        if (!empty($params)) {
            foreach ($params as $k => $v) {
//商家等级
                if (isset($v['level'])) {
                    if ($v['level'] < 1) {
                        $level_title = '普通商家';
                    } else {
                        $level_title = BaseService::StatusHtml($v['level'], [1 => '优质商家', 2 => '推荐商家', 3 => '诚信商家', 4 => '普通商家'], false);
                    }


                }
                $data['level_title'] = $level_title;

                #判断当前时间  == 过期时间 发送
                if ($time == $v['expire_time']) {
                    $data['day'] = 1;
                    $message->expir_send($v['id'], 1, $data);

                }
                #判断 当前时间+5 天 == 过期时间 发送
                if (date('Y-m-d', strtotime(" + 5 day")) == $v['expire_time']) {
                    $data['day'] = 5;

                    $message->expir_send($v['id'], 1, $data);

                }
                #判断当前时间+15天  == 过期时间 发送
                if (date('Y-m-d', strtotime(" + 15 day")) == $v['expire_time']) {
                    $data['day'] = 15;

                    $message->expir_send($v['id'], 1, $data);

                }


            }
            return true;
        }

    }

    /*
     * @content 推荐管理商品删除
     * @params  $params['id'] 商品id
     * */
    static function getRecommendProDel($params)
    {
        if (!empty($params['id'])) {
            $_where = "id = '" . $params['id'] . "' and merchants_id = '" . $params['merchants_id'] . "'";
            $a = Db::table('merchants_pro')->where($_where)->update(array('status' => 2));
        }
        return true;

    }

    static function changeStatus($params)
    {
        $id = $params['id'];
        if (empty($id)) {
            return array("status" => false);
        }
        $mark = $params['mark'];
        $_arr = array();
        if ($mark == 'down') {
            # 修改下架状态
            $_arr = array("status" => 3);
            $msg = "套餐已下架";
        } elseif ($mark == 'up') {
            # 修改下架状态
            $info = Db::table("merchants_pro")->where(array("id" => $id))->find();
            if ($info['end_time'] <= date("Y-m-d")) {
                return array("status" => false, "msg" => "请先修改套餐下架时间");
            }
            if (($info['num'] - $info['sold_num']) <= 0) {
                return array("status" => false, "msg" => "请先修改套餐数量");
            }
            $_arr = array("status" => 1);
            $msg = "套餐已上架";
        } elseif ($mark == 'delete') {
            $info = Db::table("merchants_pro")->where(array("id" => $id))->find();

            if ($info['status'] != 1 or ($info['status'] == 1 and ($info['end_time'] < date("Y-m-d")) or $info['num'] <= $info['sold_num'] or $info['start_time']>date("Y-m-d"))) {
                $_arr = array("status" => 2);
                $msg = "套餐已删除";
            } else {
                if ($info['status'] == 1 and $info['start_time'] <= date("Y-m-d") and $info['end_time'] > date("Y-m-d")) {
                    return array("status" => false, "msg" => "请先下架该套餐");
                }
            }


        } else {
            $info = Db::table("merchants_pro")->where(array("id" => $id))->find();
            if ($info['status'] != 1 or ($info['end_time'] < date("Y-m-d")) or $info['num'] <= $info['sold_num']) {
                return array("status" => false, "msg" => "请先上架该套餐");
            }
            Db::table("merchants_pro")->where(array("merchants_id" => $info['merchants_id']))->where("id!=$id")->update(array("recommend_status" => 2));
            $_arr = array("recommend_status" => 1);
            $msg = "套餐已推荐";
        }
        Db::table("merchants_pro")->where(array("id" => $id))->update($_arr);
        return array("status" => true, "msg" => $msg);
    }

    static function getsProDetail($id)
    {
        $info = Db::table("merchants_pro")->where(array("id" => $id))->find();
        if (!empty($info)) {
            $info['price'] = getsPriceFormat($info['price']);
            $info['original_price'] = getsPriceFormat($info['original_price']);
            if (!empty($info['tips'])) {
                $info['tips'] = explode(',', $info['tips']);
            } else {
                $info['tips'] = [];
            }
            if (!empty($info['detailimage'])) {
                $info['detailimage'] = explode(',', $info['detailimage']);
            } else {
                $info['detailimage'] = [];
            }
            if (!empty($info['banner'])) {
                $info['banner'] = explode(',', $info['banner']);
            } else {
                $info['banner'] = [];
            }
            $info['end_time'] = date("Y-m-d", strtotime("+1 day", strtotime($info['end_time'])));
            $info['describes'] = json_decode($info['describes'], true);
            $info['notice'] = json_decode($info['notice'], true);
            $info['discount'] = getsPriceFormat($info['price'] / $info['original_price']) * 10;
            $info['end_time1'] = array("d" => '00', "h" => "00", "m" => '00', "s" => "00");
            $info['surplus_num'] = $info['num'] - $info['sold_num'];
        }
        return $info;
    }

    /**
     * @param $merchants_id
     * @param $price
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商家余额提现至微信钱包
     */
    static function wxDeposit($merchants_id,$price){
        $_redis = new Redis();
        $noRepeat = $_redis->lock("merchantsDeposit".$merchants_id,3);
        if(!$noRepeat){
            return array("status"=>false,"msg"=>"操作过于频繁，请稍后再试");
        }
        # 查询是否有openid
        $merchantsInfo = Db::table("merchants")->field("wxopenid,balance")->where(array("id"=>$merchants_id))->find();
        if(!empty($merchantsInfo['wxopenid'])){
            # 判断余额是否足够
            if($merchantsInfo['balance']>=$price){
                # 添加提现记录
                $array = array("biz_id"=>$merchants_id,"create_time"=>date("Y-m-d H:i:s"),
                    "price"=>$price,"member_id"=>0,"launch_cate"=>5,"deposit_status"=>2,"deposit_type"=>1,"source_type"=>3,
                    );
                $depositId = Db::table('log_deposit')->insertGetId($array);
                # 减掉余额
                Db::table('merchants')->where(array('id'=>$merchants_id))->setDec('balance',$price);
                # 开始提现
                $payStatus = self::startDeposit($merchantsInfo['wxopenid'],$price);
                if($payStatus['status']){
                    # 提现成功
                    self::depositSuccess($depositId);
                    Db::table("merchants")->where(array("id"=>$merchants_id))->update(array('wxopenid'=>''));
                    return array("status" => true, "return_code" => 1,'deposit_status'=>3,'audit_time'=>'等待提现','account_time'=>'等待到账','create_time'=>date('Y-m-d H:i:s'));
                }else {
                    if ($payStatus['code'] == 404) {
                        # 微信系统故障
                        return array("status" => false, "msg" => "微信提现系统故障,请稍后再试");
                    } else {
                        # 提现失败进入等待
                        self::depositSuccess($depositId);
                        Db::table("merchants")->where(array("id"=>$merchants_id))->update(array('wxopenid'=>''));
                        return array("status" => true, "return_code" => 2, "msg" => "提现成功,1-3个工作日内转账到微信余额，敬请关注",'deposit_status'=>3,'audit_time'=>'等待提现','account_time'=>'已到账','create_time'=>date('Y-m-d H:i:s'));
                    }
                }
            }else{
                return array("status"=>false,"msg"=>"余额不足");
            }
        }else{
            return array("status"=>false,"msg"=>"请先微信授权后再次进行提现");
        }
    }

    /**\
     * @param $openid
     * @param $price
     * @param string $order_number
     * @return array
     * @throws \think\Exceptio
     * @context 商家开始提现
     */
    static function startDeposit($openid,$price,$order_number='')
    {
        if(empty($order_number)){
            $order_number = getOrderNumber();
        }
        # 进行提现
        $payment = new PayMent();
        $res = $payment->transfers([
            'partner_trade_no' => $order_number,              //商户订单号
            'openid' => $openid,                        //收款人的openid---oU1MG1nnH71SGTUTAY8Q7KK0POKY
            'check_name' => 'NO_CHECK',            //NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名
            //'re_user_name' => $payee_name,              //check_name为 FORCE_CHECK 校验实名的时候必须提交
            'amount' => floatval($price)*100,                       //企业付款金额，单位为分
            'desc' => 'E联车服商家提现',                  //付款说明
        ]);
        if ($res['status'] and $res['returnCode'] == 'SUCCESS') {
            # 提现成功,修改提现记录状态
            return array("status"=>true,"order_number"=>$order_number,'res'=>$res);
        } else {
            if($res['returnCode'] == 'SYSTEMERROR'){
                return array("status"=>false,"code"=>404,'res'=>$res);
            }else{
                return array("status"=>false,"code"=>101,"order_number"=>$order_number,'res'=>$res);
            }
        }
    }

    /**
     * @param $depositId
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 商家提现成功
     */
    static function depositSuccess($depositId){
         Db::table('log_deposit')->where(array("id"=>$depositId))
             ->update(array("deposit_status"=>3,"account_time"=>date("Y-m-d H:i:s")));
         return array("status"=>true);
    }

    /**
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商家提现执行队列
     */
    static function merchantsDepositInit(){
        # 每2小时初始化一次
        $redis=new Redis();
        $noRepeat = $redis->lock("merchantsDepositInit",7200);
        if(!$noRepeat){
            return false;
        }
        $list = Db::table("log_deposit ld")
            ->field("ld.id,ld.biz_id,m.wxopenid,ld.price")
            ->join("merchants m","m.id = ld.biz_id")
            ->where(array("ld.launch_cate"=>5,"ld.deposit_status"=>2,"ld.deposit_type"=>1,"ld.source_type"=>3))
            ->select();
        if(!empty($list)){
            $ret = true;
            foreach($list as $k=>$v){
                if(!empty($v['wxopenid'])){
                    $res=self::startDeposit($v['wxopenid'],$v['price']);
                    if(!$res['status']){
                        if($res['code']==404){
                            continue;
                        }else {
                            $ret = false;
                            break;
                        }
                    }else{
                        # 提现成功
                        self::depositSuccess($v['id']);
                    }
                }else{
                    continue;
                }
            }
            return $ret;
        }else{
            return false;
        }
    }

}