<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/20
 * Time: 15:52
 */

namespace app\businessapi\ApiService;


use app\api\ApiService\SubsidyService;
use app\businessapi\controller\Common;
use Redis\Redis;
use think\Db;

class TaskService extends Common
{
    /**
     * 任务信息处理
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getTaskDetail($param,$merchants_id){
        if(!empty($param)){
            $time = date("Y-m-d H:i:s");
            foreach ($param as $k=>$v){
                $_where = array('merchants_task_id'=>$v['id'],'merchants_id'=>$merchants_id);

                #已完成数量
                $param[$k]['finsh_num'] =  Db::table('merchants_task_log')->where($_where)->order('id desc')->value('num');
                if(empty($param[$k]['finsh_num'])){
                    $param[$k]['finsh_num']  = 0;
                }
                #先判断的已完成的数量是否符合 这个任务的条件 数量  符合 修改这个商户的的任务领取状态 改为 3
                if($v['task_num'] <=  $param[$k]['finsh_num']){
                    #根据任务id 查询 领取状态
                    $_status = Db::table('merchants_task_log')->where($_where)->order('id desc')->value('status');
                    #符合领取奖励条件
                    if($_status == 1){
                        Db::table('merchants_task_log')->where($_where)->order('id desc')->update(array('status'=>3,'completion_time'=>date("Y-m-d H:i:s")));

                    }
                }

                #根据任务id 查询 领取状态
                $status = Db::table('merchants_task_log')->where($_where)->order('id desc')->value('status');
                $create_time = Db::table('merchants_task_log')->where($_where)->order('id desc')->value('create_time');
                $is_receive = Db::table('merchants_task_log')->where($_where)->order('id desc')->value('is_receive');

                if(empty($status)){
                    #为空证明 这个商户 还没有 领取过任务
                    $param[$k]['receive_status_ttile'] = '领取任务';
                    $param[$k]['receive_status'] = 0;
                }else{

                    #不为空 根据 状态 做判断
                    if($status == 1){
                        #进行中
                        $param[$k]['receive_status_ttile'] = '进行中';
                        $param[$k]['receive_status'] = 1;

                        #当前的是时间 是否大于 领取任务的时间+天数  是的话显示 直接显示已完成
                        if($time >= date("Y-m-d H:i:s", strtotime("+" . $v['valid_time'] . " days", strtotime($create_time)))){

                            #修改状态为任务失败类型
                            $a = Db::table('merchants_task_log')->where($_where)->update(array('status'=>2));

                            #任务失败 未完成
                            $param[$k]['receive_status_ttile'] = '未完成';
                            $param[$k]['receive_status'] = 2;


                        }

                    }else  if($status == 2){
                        #任务失败
                        $param[$k]['receive_status_ttile'] = '未完成';
                        $param[$k]['receive_status'] = 2;

                        if($is_receive == 1){
                            #点击重新领取了 但是 任务到期  或者 任务不是重复领取状态  时  is_receive = 1  返回receive_status=4 前端隐藏
                            if($v['is_loop'] == 1){
                                $param[$k]['receive_status_ttile'] = '领取任务';
                                $param[$k]['receive_status'] = 0;
                            }else{
                                #前端隐藏
                                $param[$k]['receive_status'] = 4;
                            }
                        }

                    }else  if($status == 3){
                        #领取奖励
                        $param[$k]['receive_status_ttile'] = '领取奖励';
                        $param[$k]['receive_status'] = 3;

                    }else  if($status == 4){
                        #已完成  判断是否是循环 是:显示领取任务 否 显示已完成
                        if($v['is_loop'] == 1){
                            $param[$k]['receive_status_ttile'] = '领取任务';
                            $param[$k]['receive_status'] = 0;
                        }else{
                            $param[$k]['receive_status_ttile'] = '已完成';
                            $param[$k]['receive_status'] = 4;
                        }


                    }


                }

            }
            return $param;
        }

    }
    /*
     * @content 领取任务 添加领取任务记录 merchants_task_log
     * @ retrun  array
     * */
    static function getReceiveTask($task_id,$merchants_id){
        $data = Db::table('merchants_task_log')->insertGetId(array(
            'merchants_task_id'=>$task_id,
            'merchants_id'=>$merchants_id,
            'num'=>0,
        ));
        return $data;


    }
    /*
     * @content 任务  领取奖励
     * @param id 任务id
     * @param price 奖励金额
     * */
    static function getTaskPrize($params,$merchants_id){
        #根据任务id 查出 领取金额   商户余额 加上 领取金额 ,  判断 当前 任务 是否 循环  是 :领取成功之后 在插入 一条数据
        #加商户余额
        $a = Db::table('merchants')->where(array('id'=>$merchants_id))->setInc('balance',$params['price']);
        #加收款记录 9.红包获得  merchants_income
        Db::table('merchants_income')->insert(
            array(
                'merchants_id'=>$merchants_id,
                'member_id'=>9000, //是9000 显示为系统奖励
                'pay_type'=>9,
                'price'=>$params['price'],
            ));
        #修改订单状态 已完成 4
        $_where = array('merchants_task_id'=>$params['id'],'merchants_id'=>$merchants_id);
        $data = Db::table('merchants_task_log')->where($_where)->order('id desc')->update(array('status'=>4,'reward_time'=>date("Y-m-d H:i:s")));
        # 增加补贴记录,14.商家完成任务奖励
        SubsidyService::addSubsidy(array("subsidy_type"=>14,"subsidy_number"=>$params['price'],"member_id"=>0,'biz_id'=>$merchants_id,'pid'=>$params['id']));
        if($data){
            $_redis = new Redis();
            $_redis->hDel("merchants",'merchantsInfo'.$merchants_id);
            return true;

        }

    }
    /*
     * @content  支付成功 任务接口
     * @param task_type  1.办理会员数量   2.办理会员金额   3.销售额   4.好评数量   5.收款次数   6.预约次数   7.查看人数   8.点赞人数   9.成为诚信商家   10成为优质商家  11 成为推荐商家   12.热门等级   13.上架优惠券   14.增加员工
     * */
    static  function getTaskPaySucess($params,$task_type=''){
        $num = 1;

        if(!empty($params['num'])){
            $num = $params['num'];

        }
        if(empty($params['task_type'])){
            $params['task_type'] = $task_type;
        }
        #跟据类型 获取任务id   领取状态为 进行中 修改 数量+1
        #查询正在进行的任务
        $time = date('Y-m-d H:i:s');
        $_where = "'$time' >= start_time and '$time'<= end_time";
        $taskId = Db::table("merchants_task")->where(array('task_type'=>$params['task_type']))->where($_where)->field('id,task_num')->select();
        if(!empty($taskId)){
            foreach ($taskId as &$v){
                #任务记录
                $task =  Db::table('merchants_task_log')->where(array('merchants_task_id'=>$v['id'],'merchants_id'=>$params['merchants_id']))->order('id desc')->find();

                if($task['status'] == 1){
                    #判断任务记录的数量 是否分任务的数量相等 相等 不往下执行
                    if($task['num'] < $v['task_num']){
                        if($params['task_type'] == 2 or $params['task_type'] == 3){
                            #办理会员金额  销售额
                            $a =  Db::table('merchants_task_log')->where(array('merchants_task_id'=>$v['id'],'merchants_id'=>$params['merchants_id']))->setInc('num',$params['price']);
                        }else if($params['task_type'] == 12){
                            #热门等级
                            #查询商户的星级是多少
                            $data_params = array(
                                'where' => array('id'=>$params['merchants_id']),
                                'table' => 'merchants',
                                'value' => 'start'
                            );
                            $start = getTableValue($data_params);


                            Db::table('merchants_task_log')->where(array('id'=>$task['id']))->update(array('num'=>$start));
                        }else{
                            #进行中 数量+1
                            Db::table('merchants_task_log')->where(array('id'=>$task['id']))->setInc('num',$num);
                        }
                    }


                }
            }



            return true;

        }

    }

    /**
     * @param $merchants_id
     * @return array|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取商家每日任务
     */
    static function getsMerchantsDayTask($merchants_id){
        # 查询商家信息
        $merchants = Db::table('merchants')->where(array('id' => $merchants_id))->field('expire_time,level,push_time,push_create_time')->find();
        if(!empty($merchants) and $merchants['level']==1 and $merchants['push_time']>30
            and date("Y-m-d",strtotime($merchants['expire_time']))>=date("Y-m-d")){
            # 查询是否有今日任务
            $taskInfo = Db::table('merchants_daytask')->where(array("merchants_id"=>$merchants_id))
                ->where("date(create_time)='".date("Y-m-d")."'")->find();
            if(empty($taskInfo)){
                $taskInfo = self::addDayTask($merchants_id,$merchants['push_create_time']);
            }
            return array("status"=>true,"info"=>$taskInfo,"taskPrice"=>self::dayTaskPrice());
        }else{
            return array("status"=>false);
        }
    }

    /**
     * @param $merchants_id
     * @param $create_time
     * @return array
     * @context 添加每日任务
     */
    static function addDayTask($merchants_id,$create_time){
        $task_coll = self::taskCollen();
        # 商家办理前15天只需要登录
        if(date("Y-m-d",strtotime($create_time))>=date("Y-m-d",strtotime('-15 days'))){
            $task = $task_coll[0];
        }else {
            $rand = rand(1,2);
            $task = $task_coll[$rand];
        }
        $_array =array("merchants_id"=>$merchants_id,"task_type"=>$task['type'],"task_plan"=>0,"task_amount"=>$task['num'],
            "task_status"=>1,"create_time"=>date("Y-m-d H:i:s"),"task_title"=>$task['title']);
        $id = Db::table('merchants_daytask')->insertGetId($_array);
        $_array['id'] = $id;
        return $_array;
    }

    /**
     * @param $merchants_id
     * @param $type
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成每日任务
     */
    static function finishDayTask($merchants_id,$type){
        $info = Db::table('merchants_daytask')->where(array("merchants_id"=>$merchants_id,"task_type"=>$type,"task_status"=>1))
            ->where("date(create_time)='".date("Y-m-d")."'")->find();
        if(!empty($info)){
            if($info['task_plan']+1>=$info['task_amount']){
                $_array =array("task_plan"=>$info['task_plan']+1,"task_status"=>2);
            }else{
                $_array =array("task_plan"=>$info['task_plan']+1);
            }
            Db::table('merchants_daytask')->where(array("id"=>$info['id']))->update($_array);
        }
        return true;
    }

    /**
     * @return array[]
     * @context 商家每日任务类型
     */
    static function taskCollen(){
        return array(
            array("title"=>"每日登录E联车服联盟APP","num"=>1,"type"=>1),
            array("title"=>"分享抖音短视频","num"=>3,"type"=>2),
            array("title"=>"商家每日完成收款","num"=>1,"type"=>3),
        );
    }

    /**
     * @return float
     * @context 每日任务奖励
     */
    static function dayTaskPrice(){
        return 3.6 ;
    }

    /**
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成每日任务发放奖励
     */
    static function merchantsDayTaskCommission(){
        # 每日0点结算
        $date = date("Y-m-d",strtotime('-1 day'));
        $today_date = date("Y-m-d H:i:s");
        $price = self::dayTaskPrice();
        $_redis = new Redis();
        $_date_mark = $_redis->hGet("merchantsDayTask",$date);
        if(empty($_date_mark)){
            $is_repeat = $_redis->lock("merchantsDayTaskLock",60);
            if($is_repeat){
                # 查询是否已经结算过
                $is_already = Db::table('merchants_daytask')->where(array("task_status"=>3))
                    ->where("date(create_time)='".$date."'")->find();
                if(!empty($is_already)){
                    return false;
                }
                $list = Db::table('merchants_daytask')->where(array("task_status"=>2))
                    ->where("date(create_time)='".$date."'")->select();
                if(!empty($list)){
                    foreach ($list as $k=>$v){
                        # 商家加余额
                        Db::table("merchants")->where(array("id"=>$v['merchants_id']))->setInc("balance",$price);
                        # 状态改变
                        Db::table('merchants_daytask')->where(array("id"=>$v['id']))->update(array("task_status"=>3));
                    }
                }
            }
            $_redis->hSet("agentTask",$date,1,86400);
        }
        return true;
    }

}