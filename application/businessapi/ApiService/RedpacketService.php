<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/2/3
 * Time: 18:16
 */

namespace app\businessapi\ApiService;


use app\api\ApiService\SubsidyService;
use Redis\Redis;
use think\Db;

class RedpacketService
{
    /**
     * @content 红包领取
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     * @params price 红包金额
     */
    static function getRedpacket($params){
        $data = Db::table('merchants_redpacket_receive_log')->insert(array(
            'merchants_id'=>$params['merchants_id'],
            'price'=>$params['price'],
        ));
        #红包的已被领取数量数量加1

        $dtime = date("Y-m-d H:i:s");
//        $merchants_redpacket = Db::table('merchants_redpacket')->where("'$dtime' > start_time and '$dtime' < end_time")->setInc('receive_num','1');

        #领取数量加1
        $time = date("Y-m-d");
        #前一天
        $day = date('Y-m-d', strtotime("-1day",  strtotime($time)));

        if($time == "2021-04-07"){
            #时间相等  当天 app 第一次使用
            $day = $time;
        }

        $_where = "merchants_id ='".$params['merchants_id']."'  and DATE_FORMAT(create_time,'%Y-%m-%d') = '$day'";
        $data = Db::table('merchants_redpacket_log')->where($_where)->setInc('receive_num','1');
        if($data){
            #删除红包缓存数组 receive_num - 1
            $merchants_redpacket = RedpacketService::getMerchantsRedpacketInfo();
            $_redis = new Redis();
            $redpacket_arr = $_redis->hGetJson('Redpacket','merchans_redpacket_arr'.$params['merchants_id']);
            if($time == "2021-04-07"){
                #时间相等  存缓存不带商户 500  app 第一次使用
                $redpacket_arr = $_redis->hGetJson('Redpacket','merchans_redpacket_arr');
            }
            if(!empty($redpacket_arr)){
                $receive_num = $merchants_redpacket['receive_num'] - 1;
                unset($redpacket_arr[$receive_num]);
            }
        }
        #红包领取成功 加余额
        Db::table('merchants')->where(array('id'=>$params['merchants_id']))->setInc('balance',$params['price']);
        #查询红包个数
        $num = self::getRedpacketNumInfo($params);
        #加收款记录 5 .任务获得  merchants_income
        Db::table('merchants_income')->insert(
            array(
                'merchants_id'=>$params['merchants_id'],
                'member_id'=>9000,//默认9000  这个是李闯的用户  pay_type 为8 9 是用不着差memberid  是9000 显示为系统奖励
                'pay_type'=>8,
                'price'=>$params['price'],
            ));
        # 增加补贴记录,15.商家红包奖励
        SubsidyService::addSubsidy(array("subsidy_type"=>15,"subsidy_number"=>$params['price'],"member_id"=>0,'biz_id'=>$params['merchants_id']));
        return $num;
    }
    /*
     * @获取红包个数 前一天
     * */
    static function getRedpacketNumInfo($params){
        $time = date("Y-m-d");

        #前一天
        $day = date('Y-m-d', strtotime("-1day",  strtotime($time)));
        if($time == "2021-04-07"){
            #时间相等  app 第一次 当天的 商户下单的数量  200单红包金额  先到先得
            $day = $time;
        }
        $_where = "merchants_id ='".$params['merchants_id']."'  and DATE_FORMAT(create_time,'%Y-%m-%d') = '$day'";
        $merchants_redpacket = Db::table('merchants_redpacket_log')->where($_where)->find();


        if(!empty($merchants_redpacket)){
            $num = intval($merchants_redpacket['num']) - intval($merchants_redpacket['receive_num']);

        }else{
            $num = 0;
        }
        #获取红包信息
        $redpacket = self::getMerchantsRedpacketInfo();
        if($redpacket['num']<= $redpacket['receive_num']){
            $num = 0;
        }
        return $num;
    }

    /**
     * @content 获取商户的红包数量
     * @author   lc
     * @version  1.0.0
     * @datetime 2020年9月22日13:23:09
     */
    static function getRedpacketNum($params){
        $time = date("Y-m-d");

        #查询当前商户红包记录的剩余个数
        $data['num'] = self::getRedpacketNumInfo($params);




        #获取价格 每个红包价格随机 生成红包数组 存redis 之后每个商户获得红包  把 第一个红包数组删除(根据商户红包表 的receive_num - 1  就是数组的下标)
        /*
         * @param $_price 红包总金额
         * @param $_num 红包数量
         * @param $_min 每个人最少 0.01
         * */
        $merchants_redpacket = RedpacketService::getMerchantsRedpacketInfo();

        $day = date('Y-m-d', strtotime("-1day",  strtotime($time)));
        #商户抽成金额

        $commission_price = OrderService::getOrdercommissionPrice($day,$params['merchants_id']);
        #发放金额
        $_price =$merchants_redpacket['price_bili'] *  $commission_price;
        if($time == "2021-04-07"){
            #时间相等  商户抽成金额设置成 500  app 第一次使用
            $_price = 500;
        }
        $_num =$merchants_redpacket['num'];
        $_min =0.01;
        #先看是否有redis 红包 数组
        $_redis = new Redis();
        $redpacket_arr = $_redis->hGetJson('Redpacket','merchans_redpacket_arr'.$params['merchants_id']);
        if($time == "2021-04-07"){
            #时间相等  存缓存不带商户 500  app 第一次使用
            $redpacket_arr = $_redis->hGetJson('Redpacket','merchans_redpacket_arr');
        }

        if(empty($redpacket_arr)){
            #双层判断 红包记录 的数量  和是否有缓存
            if($data['num'] != 0){
                #说明当前的商户有领取红包数量
                #说明 前一天 有订单金额
                $redpacket_arr = self::getRedpacketArr($_price,$_num,$_min);
                if(!empty($redpacket_arr)){
                    $todayStart= date('Y-m-d 00:00:00', time()); //2020-10-01 00:00:00
                    $_redis->hSetJson('Redpacket','merchans_redpacket_arr'.$params['merchants_id'],$redpacket_arr,$todayStart);
                    if($time == "2021-04-07"){
                        #时间相等  存缓存不带商户 500  app 第一次使用
                        $_redis->hSetJson('Redpacket','merchans_redpacket_arr',$redpacket_arr,$todayStart);
                    }
                }

            }
        }

        #当天
        $day = $time;

        $_where = "merchants_id ='".$params['merchants_id']."'  and DATE_FORMAT(create_time,'%Y-%m-%d') = '$day'";
        if($time == "2021-04-07"){
            #时间相等  当天 app 第一次使用

            $_where = " DATE_FORMAT(create_time,'%Y-%m-%d') = '$day'";
        }
        #时间相等  这个 时间 查询录  今天的 所有的 红包领取记 app 第一次使用

        #查询 是第几个红包  （查 红包领取记录  空则是从0 开始   merchants_redpacket_receive_log  ）

        $receive_num = Db::table('merchants_redpacket_receive_log')->where($_where)->count();

        $_repacket_price = getsPriceFormat($redpacket_arr[$receive_num]['money']);
        #红包价格
        $data['price'] = $_repacket_price;
        $data['title'] = $merchants_redpacket['title'];


        return $data;
    }
    /*
     * @contnet 赠送红包金额数组 存redis
     * */
    static function getRedpacketArr($_price,$_num,$_min){
        if(empty($_price)){
            $_price = 0;
        }
        if(empty($_num)){
            $_num = 0;
        }

        for ($i=1;$i<$_num;$i++)
        {
             $safe_total=($_price-($_num-$i)*$_min)/($_num-$i);//随机安全上限
             if($safe_total < $_min){
                 $safe_total = $_min;
             }
             $money=mt_rand($_min*100,$safe_total*100)/100;
             $_price=$_price-$money;
             //红包数据
             $readPack[]= [
                'money'=>$money,
                'balance'=>$_price,
             ];
         }
         //最后一个红包，不用随机
         $readPack[] = [
            'money'=>$_price,
            'balance'=>0,
         ];
        //返回结果
         return $readPack;
    }

    /*
     * @content 获取红包信息
     * @params $dtime 传过来的时间
     * */
    static function getMerchantsRedpacketInfo($dtime=''){
        if(empty($dtime)){
            $dtime = date("Y-m-d");
        }
        $merchants_redpacket = Db::table('merchants_redpacket')->where("'$dtime' > DATE_FORMAT(start_time,'%Y-%m-%d')  and '$dtime' < DATE_FORMAT(end_time,'%Y-%m-%d') and status = 1")->find();
        return $merchants_redpacket;
    }

}