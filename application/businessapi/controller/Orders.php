<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/23
 * Time: 14:46
 */

namespace app\businessapi\controller;


use app\api\ApiService\MemberPayCode;
use app\api\ApiService\VoucherService;
use app\businessapi\ApiService\MineService;
use app\businessapi\ApiService\OrderService;
use Redis\Redis;
use think\Db;

class Orders extends Common
{
    /*
     * @content ：订单确认页  判断订单 卡券是否 可使用
     * @return :array
     * */
    function getVoucherStatus()
    {
        #用户id
        $member_id = input("post.member_id");
        # 收款金额
        $price = input("post.price");
        #商家端 传2  本地生活端 传1
        $type = input("post.type");
        $member_voucher = array();
        # 卡券一天一家只能用一次，(用户端   商家版    一个用户一天也不可重复使用平台赠送的卡券,商户类型得卡券不限制)
        $logInfo = Db::table('merchants_voucher_order')
            ->where(array('member_id' => $member_id, 'voucher_type' => 1))
            ->where("(select merchants_id from merchants_order where id = order_id) = " . $this->merchants_id)
            ->where("date(create_time) = '" . date('Y-m-d') . "'")
            ->find();
//        if(!empty($logInfo)){
//            return array('status'=>true,'voucher_status'=>2,'member_voucher'=>array());
//        }
        if (!empty($member_id)) {
            $where = null;
            if (!empty($logInfo)) {
                $where .= " voucher_source in (3,11,15) ";
            }
            if (!empty($this->merchants['cate_coll'])) {
                $where .= "cate_pid in (" . $this->merchants['cate_coll'] . ") or cate_pid is null or cate_pid = 0";
            }
            $member_voucher = Db::table('member_voucher')
                ->field('*,date(`create`) expiration_time')
                ->where("`create` > '" . date('Y-m-d H:i:s') . "'")
                ->where($where)
                ->where("status = 1 and member_id = $member_id and voucher_type = 7 and (merchants_id = 0 or merchants_id = $this->merchants_id)")
                ->select();
            $count_num = 0;
            if (!empty($member_voucher)) {
                $VoucherService = new VoucherService();
                foreach ($member_voucher as $k => $v) {

                    #判断 收款金额是否大于 门槛金额  大于 则有满减金额
                    if ($price >= $v['money_off']) {
                        $count_num = $count_num + 1;
                        $Desc = $VoucherService->voucherTypeDesc($v);
                        $member_voucher[$k]['TypeTitle'] = $Desc['TypeTitle'];
                        $member_voucher[$k]['voucherDesc'] = $Desc['voucherDesc'];
                        $member_voucher[$k]['voucher_source_title'] = $Desc['voucherSource'];
                    } else {
                        unset($member_voucher[$k]);
                    }

                    /*  else if($v['voucher_source'] == 15){
                          #商户优惠券 merchants_id(商户id)  看商户是哪个端
                          $merchants = Db::table('merchants m')
                              ->where("m.id = '".$v['merchants_id']."' and is_type = $type")
                              ->join('merchants_voucher mv','mv.merchants_id = m.id','left')
                              ->select();

                          $merchants = Db::table('merchants m')
                              ->where("m.id = '".$v['merchants_id']."' and is_type = $type")
                              ->value('id');
                          if(!empty($merchants)){
                              #查询卡券门槛金额
                              $min_consume = Db::table('merchants_voucher')->where(array('id'=>$member_voucher['voucher_id']))->value('min_consume');
                              #判断 收款金额是否大于 门槛金额  大于 则有满减金额
                              if($price > $min_consume){
                                  $count_num = $count_num+1;
                              }

                          }


                      }*/
                }

            }

            #默认没有可用抵用全券
            $status = 2;
            if ($count_num > 0) {
                #1是有
                $status = 1;
            }
        } else {
            $status = 2;
        }


        return array('status' => true, 'voucher_status' => $status, 'member_voucher' => $member_voucher);


    }

    /*
    * @content  订单确认页  商家端核销卡券  获取核销卡券金额
    * @return  array
    * */
    function getVoucherPrice()
    {
        #用户卡券id
        $memberVoucherId = input("post.member_voucher_id");
        #收款金额
        $price = input("post.price");


        #用户id
        #商家端 默认类型 是 2  本地生活 是 1
//        $type = Db::table('merchants')->where(array('id'=>$this->merchants_id))->value('is_type');
        if (!empty($memberVoucherId)) {
            #查询核销卡券金额
            $member_voucher = Db::table('member_voucher')->where("status = 1 and id = $memberVoucherId  and voucher_type = 7 and (merchants_id = 0 or merchants_id = $this->merchants_id) ")->find();

            if (!empty($member_voucher)) {
                if ($price >= $member_voucher['money_off']) {
                    $data = $member_voucher;


                } else {
                    $data['voucher_cost'] = 0;
                }
                /*if($member_voucher['voucher_source'] == 4){
                    #现金红包
                    if($price >= $member_voucher['money_off'] ){
                        $data = $member_voucher;

                    }else{
                        $data['voucher_cost'] = 0;
                    }



                }else{


                }*/

                /* else  if($member_voucher['voucher_source'] == 15){

                     #is_type 1 本地生活  2 联盟商家  merchans_id  0  为平台优惠

                     #商家优惠券 merchants_id(商户id)
                     if(!empty($member_voucher['merchants_id'])){
                         $merchants = Db::table('merchants m')
                             ->where("m.id = '".$member_voucher['merchants_id']."' and is_type = $type")
                             ->value('id');
                         if(!empty($merchants)){
                             if($price >= (Db::table('merchants_voucher')->where(array('id'=>$member_voucher['voucher_id']))->value('min_consume')) ){
                                 $data['price'] = Db::table('merchants_voucher')->where(array('id'=>$member_voucher['voucher_id']))->value('price');

                             }else{
                                 $data['price'] = 0;
                             }
                             $data['voucher_id'] = $member_voucher['voucher_id'];
                         }
                     }

                 }*/
                #返回数据
                /*
                 * price ：低值金额
                 * voucher_id  卡券id
                 * */
                if ($data['voucher_cost'] == 0) {
                    #提示 收款金额 小于 卡券金额
                    return array('status' => false, 'msg' => '收款金额小于卡券最低使用金额,请核对!');

                } else {
                    return array('status' => true, 'data' => $data);

                }
            } else {
                return array('status' => false, 'msg' => '无核销卡券!');

            }


        }

    }

    /*
    * @content  订单确认页  我的页 商家推广订单  点击 立即支付
    * @return array('order_id'=>$order_id,'order_number'=>$data['order_number'])
    * */
    function getMineOrderConfirm()
    {
        $params = input("post.");
        $params['merchants_id'] = $this->merchants_id;
        $data = MineService::getMineOrderConfirm($params);

        return array('status' => true, 'data' => $data);

    }

    /*
   * @content  订单确认页 核销订单  状态改为 已到店 加余额 给红包
   * @return array
   * */
    function getOrderComplete()
    {
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $data = OrderService::getOrderComplete($params);


        return array('status' => true, 'data' => $data);
    }


    /**
     * @return array
     * @context 微信支付 APP
     */
    function wxPayMent()
    {
        $pay_price = floatval(input("post.pay_price")) * 100;
        if ($this->merchants_id == 1 or $this->merchants_id == 2) {
            $pay_price = 1;
        }
        $order = [
            'out_trade_no' => input("post.order_number"),
            'total_fee' => $pay_price, // **单位：分**
            'body' => input("post.body"),
            //'openid' => $this->MemberInfo['member_openid'],
        ];
        $PayMent = new PayMent();
        $res = $PayMent->wxPayAPP($order)->send();
        return array("status" => true, "data" => json_decode($res, true));
    }

    /**
     * @return array
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     * @context 订单状态查询
     */
    function wxPayStatusQuery()
    {
        $order_number = input("post.order_number");
        $PayMent = new PayMent();
        $res = $PayMent->orderStatusQuery($order_number, 1);
        return array("status" => true, "result" => $res);
    }

    function aliPayMent()
    {
        $pay_price = floatval(input("post.pay_price"));
        if ($this->merchants_id == 1 or $this->merchants_id == 2) {
            $pay_price = 0.01;

        }
        $order = [
            'out_trade_no' => input("post.order_number"),
            'total_amount' => $pay_price,
            'subject' => input("post.body"),
        ];
        $PayMent = new PayMent();
        $res = $PayMent->aliPayAPP($order)->send();
        return array("status" => true, "data" => json_decode($res, true));
    }

    function aliPayStatusQuery()
    {
        $order_number = input("post.order_number");
        $PayMent = new PayMent();
        $res = $PayMent->orderStatusQuery($order_number, 2);
//        dump($res);die;
        return array("status" => true, "result" => $res);
    }


    /*
     * @content 商户升级商家  下的订单
     * */
    function getUpgradeOrder()
    {
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $res = OrderService::getUpgradeOrder($params);
        return array("status" => true, "data" => $res);
    }

    /*
     * @content 生成订单号
     * */
    function getOrderNumber()
    {
        $orderNumber = getOrderNumber();
        return array("status" => true, "orderNumber" => $orderNumber);
    }

    /*
     * @content 用户扫码支付 下的订单
     * */
    function getSancodeOrder()
    {

        $params = $this->data_post;
        if (empty($params['merchants_id'])) {
            $params['merchants_id'] = $this->merchants_id;
        }
        Db::table('aaa')->insert(array('info' => json_encode($params), 'type' => '付款给商家-getSancodeOrder' . date('Y-m-d H:i:s')));
        #判断支付类型
        #是余额是判断 余额够不够
        if ($params['cash_type'] == 5) {
            $balance_pay = Db::table('merchants')->where(array('id' => $params['merchants_id']))->value('balance_pay');
            if ($balance_pay == 1) {
                #支持余额付款
                #查询用户id 余额是否够用
                $balance = Db::table('member_mapping')->where(array('member_id' => $params['member_id']))->value('member_balance');
                #余额< 实际支付
                if ($balance < $params['actual_price']) {
                    return array("status" => false, "msg" => '账户余额不足！');

                } else {
                    $res = OrderService::getSancodeOrder($params);
                    return array("status" => true, "data" => $res);
                }

            } else {
                #不支持余额付款
                return array("status" => false, "msg" => '暂不支持余额付款！');

            }


        } else {
            if ($params['cash_type'] == 3 or $params['cash_type'] == 4) {
                $_redis = new Redis();

                #现金或银联支付  存个缓存 15 秒   如果缓存还在 提示 频繁操作
                $actual_price = $_redis->hGet("cash_type", 'cash_type_time' . $this->merchants_id);
                if (empty($actual_price)) {
                    $_redis->hSet("cash_type", 'cash_type_time' . $this->merchants_id, $params['actual_price'], 15);
                    $res = OrderService::getSancodeOrder($params);
                    return array("status" => true, "data" => $res);
                } else {
                    return array("status" => false, "msg" => '请不要频繁操作！');

                }


            } else {
                $res = OrderService::getSancodeOrder($params);
                return array("status" => true, "data" => $res);
            }


        }

    }

    /*
     * @content 商户升级 支付成功
     * @return $expire_time 到期时间
     * */
    function getUpgradePaySuccess()
    {
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $res = OrderService::getUpgradePaySuccess($params);

        $_redis = new Redis();
        $_redis->hDel("merchants", 'merchantsInfo' . $this->merchants_id);
        return array("status" => true, "expire_time" => $res);
    }

    /*
   * @content 商户续费包月 还是 包季记录
   * @return array()
   * */
    function getMerchantsRenew()
    {
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $res = OrderService::getMerchantsRenew($params);

        $_redis = new Redis();
        $_redis->hDel("merchants", 'merchantsInfo' . $this->merchants_id);
        return array("status" => true, "expire_time" => $res);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @context 微信刷卡支付
     */
    function wxPosPayment()
    {
        $order_number = getOrderNumber();
        $pay_price = input("post.pay_price");
        $auth_code = input("post.auth_code");
        $data = array(
            'out_trade_no' => $order_number,
            'body' => '商家收款',
            'total_fee' => $pay_price * 100,
            'auth_code' => $auth_code,
        );
        $payMent = new PayMent();
        $res = $payMent->wxPosPay($data);
        if ($res['return_code'] == "SUCCESS" and $res['result_code'] == 'SUCCESS') {
            return array("status" => true, "return_code" => 1, "order_number" => $order_number);
        } else {
            if ($res['result_code'] == 'USERPAYING') {
                # 用户支付中
                return array("status" => true, "return_code" => 2, "msg" => $res['return_msg'], "order_number" => $order_number);
            }
            return array("status" => false, "msg" => $res['return_msg']);
        }

    }

    /**
     * @return array
     * @throws \think\Exception
     * @context 支付宝支付
     */
    function aliPosPayment()
    {
        $order_number = getOrderNumber();
        $pay_price = input("post.pay_price");
        $auth_code = input("post.auth_code");
        $member_id = input("post.member_id");
        if ($member_id == 13574) {
            $pay_price = 0.01;
        }
        if ($this->merchants_id == 2) {
            $pay_price = 0.01;
        }
        $data = array(
            'out_trade_no' => $order_number,
            'subject' => '商家收款',
            'total_amount' => $pay_price,
            'auth_code' => $auth_code,
        );
        $payMent = new PayMent();
        $res = $payMent->aliPosPay($data);
        if ($res['code'] == '10000') {
            return array("status" => true, "return_code" => 1, "order_number" => $order_number);
        } else {
            return array("status" => false, "msg" => $res['msg']);
        }

    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 余额扫码支付
     */
    function balancePosPayment()
    {
        $order_number = getOrderNumber();
        $pay_price = input("post.pay_price");
        $auth_code = input("post.auth_code");
        $member_id = input("post.member_id");
        # 验证用户码是否正确
        $memberPayCode = new MemberPayCode();
        $vali = $memberPayCode->payCodeVerify($member_id, $auth_code);
        if ($vali['status']) {
            # 查询余额是否满足
            $memberInfo = Db::table("member_mapping")->field("member_balance")->where(array("member_id" => $member_id))->find();
            if ($memberInfo['member_balance'] < $pay_price) {

                return array("status" => false, "msg" => "余额不足");
            }
            return array("status" => true, "msg" => "验证通过", "order_number" => $order_number);
        } else {
            return array("status" => false, "msg" => $vali['msg']);
        }
    }

}