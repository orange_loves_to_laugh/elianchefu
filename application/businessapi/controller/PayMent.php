<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/30
 * Time: 16:01
 */

namespace app\businessapi\controller;


use think\App;
use think\Controller;
use think\Exception;
use think\Log;
use Yansongda\Pay\Pay;

class PayMent extends Controller
{
    function __construct(App $app = null)
    {
        parent::__construct($app);
        header('Access-Control-Allow-Origin:*');  //支持全域名访问，不安全，部署后需要固定限制为客户端网址
        header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); //支持的http 动作
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Authorization , Access-Control-Request-Headers,token,applymark');  //响应头 请按照自己需求添加。
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }
    }
    # 微信提现config
    private $wxDepositConfig = [
        'appid' => 'wx10efe710da3327ca', // APP APPID
        'app_id' => 'wx10efe710da3327ca', // 公众号 APPID
        'miniapp_id' => 'wx10efe710da3327ca', // 小程序 APPID
        'mch_id' => '1573261001',
        'key' => 'youpilive13236999488youpilive488',
        'notify_url' => 'http://coo.elianchefu.com/index/Payment/payBackWx',//支付回调地址
        'cert_client' => ROOT_PATH . '../extend/cert/apiclient_cert.pem', // optional，退款等情况时用到
        'cert_key' => ROOT_PATH . '../extend/cert/apiclient_key.pem',// optional，退款等情况时用到
        'log' => [ // optional
            'file' => 'runtime/payLogs//wechat.log',
            'level' => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        // 'mode' => 'dev',
    ];

    protected $wx_config = [
        'appid' => 'wx10efe710da3327ca', // APP APPID
        'app_id' => 'wx281132202d144816', // 公众号 APPID
        //'miniapp_id' => 'wxb3fxxxxxxxxxxx', // 小程序 APPID
        'mch_id' => '1573261001',
        'key' => 'youpilive13236999488youpilive488',
//        'notify_url' => 'https://elian.cn1.utools.club/businessapi/PayMent/notify',
        'notify_url' => 'http://back.ecarfu.com/businessapi/PayMent/notify',
        'cert_client' => ROOT_PATH . '../extend/cert/apiclient_cert.pem', // optional，退款等情况时用到
        'cert_key' => ROOT_PATH . '../extend/cert/apiclient_key.pem',// optional，退款等情况时用到
        'log' => [ // optional
            'file' => 'runtime/logs/wechat.log',
            'level' => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode' => 'dev', // optional, dev/hk;当为 `hk` 时，为香港 gateway。
    ];

    public function wxPayAPP($data)
    {
        $pay = Pay::wechat($this->wx_config)->app($data);
        return $pay;
    }

    public function wxPayMp($data)
    {
        $pay = Pay::wechat($this->wx_config)->mp($data);
        return $pay;
    }

    public function wxRefund($data)
    {
        $pay = Pay::wechat($this->wx_config)->refund($data);
        return $pay;
    }

    public function notify()
    {
        $pay = Pay::wechat($this->wx_config);
        try {
            $data = $pay->verify(); // 是的，验签就这么简单！
        } catch (\Exception $e) {
            // $e->getMessage();
        }
        return $pay->success()->send();// laravel 框架中请直接 `return $pay->success()`
    }

    /**
     * @param $out_trade_no
     * @return \Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     * @context 查询订单状态
     */
    public function orderStatusQuery($out_trade_no, $type = 1)
    {
        if ($type == 1) {
            $result = Pay::wechat($this->wx_config)->find(array("out_trade_no" => $out_trade_no, "type" => "app"));
            return $result;

        } else {
            try{
                $result = Pay::alipay($this->ali_config)->find(array("out_trade_no" => $out_trade_no, "type" => "app"));

                return $result;
            }catch(\Exception $e){
                $a= $e->raw;
                //交易不存在正常轮循
                if($a['alipay_trade_query_response']['sub_code']=='ACQ.TRADE_NOT_EXIST'){
                    return array("code"=>"10000","trade_status"=>"ACQ.TRADE_NOT_EXIST");
                }
                return array("return_code"=>"FAIL");
            }
        }
    }

    protected $ali_config = [
        'app_id' => '2021002130698167',
        'notify_url' => 'http://back.ecarfu.com/businessapi/PayMent/ali_notify.php',
        'return_url' => 'http://back.ecarfu.com/businessapi/PayMent/ali_return.php',
        'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAojSPnrmQl1GIIvNrRalo6QsnAYU3Wff3grKnHMoi0xZ6ftKMROB6tUY91AVa85CHld7MkrwZHlvDSjUrtzIfBnevpOSCUKIVgSEf4dO3WMbSlLDU7Pu5xz9gCM7WA6tbtvmszgwmk6piZDenSZcx08e4t1QIeBP5YxGjHM4lk8hPFgdSsP1frY7zVGw+fZxKl+QOcW6MtX89NqQOVe84IgSej4cWhTuSwBdKA5SoHLW2vuauvTwAF+qpETkUO4MzzPqYPTUe05Ap6NDbtVhctFerdbRudjfnYZe7Bgj4ukSz0Jc7eJ/BH/6jTZXYMQW7Ck7DwVShapp4pJoaLag11wIDAQAB',
        // 加密方式： **RSA2**
        'private_key' => 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCLCD7Fieupq1wQgYnbW/Wt4ZHXz04BnWetghamBnSUC0sxWbxnJBWKEPQ5Qaf0bWwDLQ9oObo2JJVftF29uKYwsSP6QVuefTN2KL4U9kXBLzTW79UKDg9CGUn+YlgxmhOfNq7b8jqUCTCl3zwKL1ZxP9KbNzzdqI9FBtX7ky+oqsorYauY3LDwQcT3Gso0rDtz8WF0xkX0rtxCq2XHts1Q0bgVC0eUV6oWP+cR9jlHocqwd7dwIaD6GFYkbPQPFrzFA+BJqmQtdjDjUvTgUl/RxLVmE/VD29Z/MHTtkysjg++aVwAC9aQbpepIEBgpolRNNNRBEb08EvMvlyHOe5DLAgMBAAECggEAfvAcP8dY/ySRfhCHiVxAx203904y7o6EZoprm1Ixhil5kalYtuLfN5ubH9uuOSDeiVfOxV5PgbeNGkBXhizcr4BMVOTvfZjaL3urcQAZV/R9zfBTFkcX70WfpT6tPzxpxefrZq2WhiqN3CQhcTsfNW8MHuMkE00e4esH3BXaYaeVMusH7KY7kiKgrIk2ybMr2/uhjVAiClmTOMpK7kldtdpMjE8jiPqo09uNyp4FwU2q2NobnE5EFiyNJ26IY0A+BlcgoqTmJELNHcJigebhQRQDf8oCojcPTw3XuyJA3DHxIXxfhSnrgqmHGGCd3V1WvoOdtC5HBLDbUY+bH08aQQKBgQDAo+hdlk09iX4WAJ5QWWh5N+x6trJdQH1zWdiwNpI+XLD3jQUEW9Cokjo9A5puvr9yd/iYC+sau6WQmwxSEiviy8pMYJUFN2NsFdCdeeQttJd8FTlj1dFkCoRcWVOBxhxrvTSOki2jTfgFV5pHYuSK2zp5K+jYsc44U09TyY5SowKBgQC4wpmNKPQlNWH4hR5joXs3e3X6+9Dcfcp//EHp4VoswpE+3mW410vnzPDdSNGh+o3a2puu19cqpqHahI2FqFcN5tt1TddDTQowZIDRMf0qQlMTjBx2Kst671qFfCCZx/mSmdaKa2so6jP0F/n0uSO7MLs9kBXvOSscblTKez1TuQKBgBrGbWScoG2+L4U1XsvCMZAvorHHtPDbJtSil0ievSLn1T0DH8BkVpMnh+q1FoDwCEILxMS1W18i17dKpaK+ndTvCFYnUm8gLLR4L7/DkmB76lY3yPtEW88w2wbO75lpBMhAyw5J6Qf6rU9oxqvHZgaaw/zVio6b4BLaGXuG1z5LAoGAS9xzKdef70ss79rQsH3UVuKVfbk5d98ab1SHR92kpe45rwOn5q5X11R5rZJBqO10o2DRPYgYK8YBfPSiGzgUjnu0rkSI13Rhc59AmSSaW2iiLcuAwDAwHkj69QYTaLEIU15hDbKvbDxFmrrhgc8m0axfpRZtOy1Q+zp+DClv89kCgYEAnrLYZS7el1oEENWxcb9Nt3Bvl9ozEEcF+WrTl3634nLdR3V08SXmb2o4f1LN+GFafHOlpNohpLZsyAfBZOarXOwgVSF9Ty2+xqe7JbJ8+BDiqjLrH3LQ+XdZF5g254x5/7nIpIzdraJucpnVjaGdFE0Usla8vYZSexiRC3qkpXU=',
        //'app_cert_path'=>ROOT_PATH.'../extend/cert/appCertPublicKey_2021002114641063.crt', // 应用证书
        //'alipay_root_cert_path'=>ROOT_PATH.'../extend/cert/alipayRootCert.crt',// 支付宝根证书
        //'alipay_cert_path'=>ROOT_PATH.'../extend/cert/alipayCertPublicKey_RSA2.crt', // 支付宝公钥证书
        'log' => [ // optional
            'file' => 'runtime/logs/alipay.log',
            'level' => 'debug', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode' => 'dev', // optional,设置此参数，将进入沙箱模式
    ];

    public function aliPayAPP($order)
    {
        $alipay = Pay::alipay($this->ali_config)->app($order);
        return $alipay;
    }

    public function aliRefund($order)
    {
        $alipay = Pay::alipay($this->ali_config)->refund($order);
        return $alipay;
    }

    public function ali_return()
    {
        $data = Pay::alipay($this->ali_config)->verify(); // 是的，验签就这么简单！

        // 订单号：$data->out_trade_no
        // 支付宝交易号：$data->trade_no
        // 订单总金额：$data->total_amount
    }

    public function ali_notify()
    {
        $alipay = Pay::alipay($this->ali_config);

        try {
            $data = $alipay->verify(); // 是的，验签就这么简单！

            // 请自行对 trade_status 进行判断及其它逻辑进行判断，在支付宝的业务通知中，只有交易通知状态为 TRADE_SUCCESS 或 TRADE_FINISHED 时，支付宝才会认定为买家付款成功。
            // 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号；
            // 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额）；
            // 3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）；
            // 4、验证app_id是否为该商户本身。
            // 5、其它业务逻辑情况

            \Yansongda\Pay\Log::debug('Alipay notify', $data->all());
        } catch (\Exception $e) {
            // $e->getMessage();
        }

        return $alipay->success()->send();// laravel 框架中请直接 `return $alipay->success()`
    }

    /**
     * @param $order
     * @return array
     * @content 企业转账
     */
    function transfers($order)
    {
        $result = Pay::wechat($this->wxDepositConfig)->transfer($order);
        if ($result->return_code == 'SUCCESS' and $result->result_code == 'SUCCESS') {
            return array('status' => true, 'code' => 'SUCCESS', 'msg' => '转账成功','res'=>$result);
        } else {
            return array('status' => true, 'code' => 'FAIL', 'msg' => config('errCode.' . $result->return_code),'res'=>$result);
        }
    }

    /**
     * @param $data
     * @return \Yansongda\Supports\Collection
     * @context 微信刷卡支付
     */
    function wxPosPay($data)
    {

        try{
            $pay = Pay::wechat($this->wx_config)->pos($data);
            return $pay;
        }catch(\Exception $e){
            $a= $e->raw;
            if($a['err_code']=='USERPAYING'){
                return array("result_code"=>"USERPAYING");
            }
            return array("return_code"=>"FAIL");
        }

    }

    /**
     * @param $order
     * @return \Yansongda\Supports\Collection
     * @context 支付宝刷卡支付
     */
    function aliPosPay($order)
    {
        $alipay = Pay::alipay($this->ali_config)->pos($order);
        return $alipay;
    }
}
