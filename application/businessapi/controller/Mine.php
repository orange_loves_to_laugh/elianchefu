<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/28
 * Time: 14:39
 */

namespace app\businessapi\controller;
/*
 * @content 我的页
 * */

use app\businessapi\ApiService\MerchantsService;
use app\businessapi\ApiService\MineService;
use app\service\BaseService;
use Redis\Redis;
use think\Db;

class Mine extends Common
{
    /*
      * @content 我的页  提现
      * @return  array 提现id
      * */
    function getMineDepositCreate(){
        $params = input("post.");
        #100 的整数呗 this.num%100 != 0
        if($params['price']%100 != 0){
            return array('status'=>false,'msg'=>'提现金额必须是100的整数倍');
        }
        if($params['deposit_type']==1){
            # 微信提现
//            if($this->merchants_id==61){
//                $params['price'] = 1;
//            }
           return MerchantsService::wxDeposit($this->merchants_id,$params['price']);
        }else{
            if(!empty($params['account_code'])){
                if($params['mark'] == 'bondPrice'){
                    #商户保证金退回
                    # 查询是否存在审核的信息
                    $log = Db::table('log_deposit')->where(array('biz_id'=>$this->merchants_id,'source_type'=>3,'launch_cate'=>4,'deposit_status'=>1))->find();
                    if(!empty($log)){
                        return array('status'=>false,'msg'=>'正在审核,请勿重复提交申请');
                    }
                }else{
                    # 查询商户信息
                    $info = Db::table('merchants')->where(array('id'=>$this->merchants_id))->field('area')->value('area');
                    if($info=='鲅鱼圈区'){
                        return array('status' => false, 'msg' => '平台升级中暂时无法提现！');
                    }
                }
                $data = MineService::getMineDepositCreate($params,$this->merchants_id);
                #删除商户缓存
                $_redis = new Redis();
                $_redis->hDel("merchants",'merchantsInfo'.$this->merchants_id);
                return array('status'=>true,'data'=>$data);
            }else{
                return array('status'=>false,'msg'=>'请填写收款账号');

            }
        }

    }


    /*
     * @content 我的页  提现列表
     * @return  array
     * */
    function getMineDepositInfo(){

        $_where = "source_type = 3 and biz_id = $this->merchants_id";
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'log_deposit',
            'field' => "*",
            'order' => 'id desc'
        );
        $dataList = BaseService::DataList($data_params);
        $data = MineService::getMineDepositDetail($dataList);
        return array('status'=>true,'data'=>$data);
    }
    /*
     * @content 我的页  提现详情
     * @return  array
     * */
    function getMineDepositDetail(){
        /*
         * 提现状态  deposit_status  1等待审核 2提现中   3成功 4失败 5.审核驳回 \\n\\n
         * 提现名称  deposit_status_title
         * 提现金额 price
         * 提现方式  deposit_type 1 微信 2 支付宝
         * 发起提现时间 create_time
         * 提现审核时间 create_time
         * 提现成功时间 account_time
         *
         * */
        $depositId = input("post.depositId");
        if(!empty($depositId)){
            $_where = "id = $depositId";
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'log_deposit',
                'field' => '*',
                'order' => 'id desc'
            );
            $dataList = BaseService::DataList($data_params);
            $data = MineService::getMineDepositDetail($dataList)[0];

            return array('status'=>true,'data'=>$data);

        }


    }

    /*
   * @content 我的页  商家推广 页（点击商家等级 触发接口）
   * @return  array
   * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
   * */
        function getMineExtension(){
        $type = input("post.type");
        $data = MineService::getMineExtension($type,$this->merchants_id);
        return array('status'=>true,'data'=>$data);

    }
    /*
     * @contetn  请求商户类型  是否到期 到期直接修改类型
     * */
    function getMerchatsExpiretime(){
        #查询是否到期
        $expire_time = Db::table('merchants')->where(array('id'=>$this->merchants_id))->value("DATE_FORMAT(expire_time,'%Y-%m-%d %H:%i') ");
        $level = Db::table('merchants')->where(array('id'=>$this->merchants_id))->value("level ");
        $time = date("Y-m-d H:i");
        if(strtotime($expire_time) <= time()){
            #修改 商户状态 为诚信商家  清商户缓存
            if($level != 4){
                Db::table('merchants')->where(array('id'=>$this->merchants_id))->update(array('level'=>3));

            }

        }
        $_redis = new Redis();
        $_redis->hDel("merchants",'merchantsInfo'.$this->merchants_id);
        return array('status'=>true);


    }
    /*
    * @content 保证金 退回记录
    * @return  array
    * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
    * */
    function getMineBondLog(){
        $params = input("post.");
        $params['merchants_id'] = $this->merchants_id;
        $data = MineService::getMineBondLog($params);
        #删除商户缓存
        $_redis = new Redis();
        $_redis->hDel("merchants",'merchantsInfo'.$this->merchants_id);

        return array('status'=>true,'data'=>$data);
    }
    /*
   * @content 商家推广页 点击提交
   * @return  array
   * @params $type  商户等级 1优质商家、2推荐商家、3星级商家(诚信)、4普通商家
   * */
    function getMineJudgment(){
        $type = input("post.type");
        if($type == 2){
            $data = MineService::getMerchantsOrderLevel($type,$this->merchants_id);
            if(!empty($data)){
                return array('status'=>true);

            }else{
                return array('status'=>false,'msg'=>'请先缴纳诚信商家保证金');

            }

        }else{
            return array('status'=>true);

        }

    }
    /*********************** 我的页  员工管理 start *********************************/
    /*
     * @content 员工列表
     * @return  array
     * */
    function getMineStaffInfo(){

        $_where = "merchants_id = $this->merchants_id";
        $id = input("post.id");
        if(!empty($id)){
            $_where .= " and id =$id";
        }
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_staff',
            'field' => '*',
            'order' => 'id desc'
        );
        $data = BaseService::DataList($data_params);
        if(!empty($data)){
            foreach($data as $k=>$v){
                #拆分权限
                if(!empty($v['staff_power'])){
                    $data[$k]['staff_power'] = explode(',',$v['staff_power']);

                /*    if(!empty($data['staff_power'])){
                        foreach ($data['staff_power'] as $k=>$v){
                            $data['staff_power'][$k]['value'] = $v;

                        }
                    }*/
                }
            }
        }
        return array('status'=>true,'data'=>$data);

    }
    /*
     * @content 员工 创建 修改
     * @return  array
     * @params  id 员工id
     * */
    function getMineStaffSave(){
        $params = input("post.");
        $params['merchants_id'] = $this->merchants_id;
        if(Db::table('merchants')->where(array('status'=>1,'contacts_phone'=>$params['staff_phone']))->value('id')){
            return array('status'=>false,'msg'=>'手机号重复！请换手机号');

        }
        $data = MineService::getMineStaffSave($params);
        if($data){
            return array('status'=>true,'data'=>$data);

        }else{
            return array('status'=>false,'msg'=>'手机号重复！请换手机号');

        }

    }
    /*
    * @content 员工 删除
    * @return  array
    * @params  id 员工id
    * */
    function getMineStaffDel(){
        $params = input("post.");
        $data = Db::table('merchants_staff')->where(array('id'=>$params['id']))->delete();

        return array('status'=>true,'data'=>$data);

    }
    /*********************** 我的页  员工管理 end *********************************/
    /*
   * @content 账号设置 注销门店
   * @return  array
   * */
    function getMineMerchantsDel(){
        Db::table('merchants')->where(array('id'=>$this->merchants_id))->update(array('status'=>2));
        return array('status'=>true);

    }
    /*
     * @content 修改密码
     * @return  array
     * @params  oldpassword  原密码
     * @params  newpassword     新密码
     * @params  confirmpassword 确认密码
     * */
    function getMinePasswordWrite(){
        $params = input("post.");
        $pass = MineService::getMerchantsPassword($this->merchants_id);


        if($pass['login_mdpassword'] == md5($params['oldpassword'])){
            if($params['newpassword'] == $params['confirmpassword']){
                $data_params['login_password'] = $params['newpassword'];
                $data_params['login_mdpassword'] = md5($params['newpassword']);
                $data_params['merchants_id'] = $this->merchants_id;
                $data = MineService::getMinePasswordWrite($data_params);
                if($data){
                    return array('status'=>true);

                }
            }else{
                return array('status'=>false,'msg'=>'新密码与确认密码不相同!');

            }

        }else{
            return array('status'=>false,'msg'=>'原密码不正确!');
        }

    }
    /*
     * @content 修改提现密码
     * @return  array
     * @params  oldpassword_deposit  原提现密码
     * @params  newpassword_deposit     新提现密码
     * @params  confirmpassword_deposit 确认提现密码
     * */
    function getDepositPasswordWrite(){
        $params = input("post.");
        $pass = MineService::getMerchantsPassword($this->merchants_id);
        #查一下是否有密码
        if(!empty($pass)){
            #判断原密码  是否正确
            if($pass['deposit_mdpassword'] == md5($params['oldpassword_deposit'])){
                if($params['newpassword_deposit'] == $params['confirmpassword_deposit']){

                    $data_params['deposit_password'] = $params['newpassword_deposit'];
                    $data_params['deposit_mdpassword'] = md5($params['newpassword_deposit']);
                    $data_params['merchants_id'] = $this->merchants_id;
                    $data = MineService::getMinePasswordWrite($data_params);
                    if($data){
                        return array('status'=>true);

                    }
                }else{
                    return array('status'=>false,'msg'=>'新提现密码与确认提现密码不相同!');

                }

            }else{
                return array('status'=>false,'msg'=>'原提现密码不正确!');
            }
        }else{
            #设置密码
            $data = Db::table('merchants_password')->insert(array(
                'merchants_id'=>$this->merchants_id,
                'deposit_password'=>$params['newpassword_deposit'],
                'deposit_mdpassword'=>md5($params['newpassword_deposit']),
            ));
            if($data){
                return array('status'=>true);

            }

        }


    }
    /*
    * @content 忘记密码
    * @return  array
    * @params  手机号  phone
    * @params  验证码  code
    * @params  newpassword     新密码
    * @params  confirmpassword 确认密码
    * @params  mark  deposit  提现  login 登录密码
    * */
    function getForgetPassword(){
        $params = input('post.');
        $mark = input("post.mark");
        #缓存获取验证码
        $redis = new Redis();
        # 发送短信验证码
        $code = $redis->hGet('smsCode', strval($params['phone']));
        if(!empty($params['code'])){
            if($params['code'] ==$code ){
                if($params['newpassword'] == $params['confirmpassword']){
                    if($mark== 'deposit'){
                        #提现密码修改
                        $data_params['deposit_password'] = $params['newpassword'];
                        $data_params['deposit_mdpassword'] = md5($params['newpassword']);
                        $data_params['merchants_id'] = $this->merchants_id;
                        $data = MineService::getMinePasswordWrite($data_params);
                    }else{
                        #登录密码
                        $data_params['login_password'] = $params['newpassword'];
                        $data_params['login_mdpassword'] = md5($params['newpassword']);
                        $data_params['merchants_id'] = $this->merchants_id;
                        $data = MineService::getMinePasswordWrite($data_params);
                    }
                    if($data){
                        return array('status'=>true);

                    }
                }else{
                    if($mark== 'deposit'){
                        return array('status'=>false,'msg'=>'新提现密码与确认提现密码不相同!');

                    }else{
                        return array('status'=>false,'msg'=>'新密码与确认密码不相同!');

                    }
                }

            }else{
                return array('status'=>false,'msg'=>'验证码不正确!');

            }
        }else{
            return array('status'=>false,'msg'=>'验证码不能为空!');

        }



    }
    /*
   * @content 商户协议 等
   * @return  array
   * @params  1合作 协议 2  关与平台  3 首页 收款码 图片 4 . 首页推荐会员图片 5  商户管理 门店活动 图图片 6 .商户管理 门店 优惠券 图 7 . 我的页 中间  推荐E联车服会员赚收益图 8 .客服电话

   * */
    function getMerchants_application(){
        $type = input("post.type");
        if(!empty($type)){
            $data = MineService::getMerchants_application($type);
            #删除商户缓存
            $_redis = new Redis();
            $_redis->hDel("merchants",'merchantsInfo'.$this->merchants_id);
            return array('status'=>true,'data'=>$data);
        }

    }
    /*
     * @content  获取提现密码
     * */
    function getDepositpwd(){
        $deposit_password = Db::table('merchants_password')->where(array('merchants_id'=>$this->merchants_id))->value('deposit_password');
        if(!empty($deposit_password)){
            return array('status'=>true,'data'=>$deposit_password);


        }else{
            return array('status'=>true,'data'=>'depositfalse');

        }


    }
    /*
     * @content  我的页 推荐会员 分享 信息
     * */

    function mineAccessInfo()
    {
        return array("status" => true, 'recommendVip' => config('share.recommendVip'));
    }

    /*
     * @content 查询是否有提现密码
     * */
    function getDeposit(){
        $data = Db::table('merchants_password')->where(array('merchants_id'=>$this->merchants_id))->value('deposit_password');
        if(!empty($data)){
            return array('status'=>true);
        }else{
            return array('status'=>false,'msg'=>'暂无提现密码,无法修改!');

        }

    }
    /*
    * @content 存提现密码
    * */
    function getSaveDeposit(){
        $params = input();
            $res= Db::table('merchants_password')->insert(array(
                'deposit_password'=>$params['deposit_password'],
                'deposit_mdpassword'=>md5($params['deposit_password']),
                'merchants_id'=>$this->merchants_id
            ));
            if($res){
                return array('status'=>true);
            }else{
                return array('status'=>false,'msg'=>'设置失败,请重新设置!');

            }



    }


    /*
    * @content 商户推荐会员列表
    * @return  array
    * */
    function merchants_recommended_member(){
        #会员推荐人数 （办理会员的人数 log_handlecard  用户分组）
        $data['member_count'] = Db::table('log_handlecard')->where(array('log_resource'=>3,'biz_id'=>$this->merchants_id))->group('member_id')->count('id');
        if(empty($data['member_count'])){
            $data['member_count'] = 0;
        }

        #推荐总收益(办理 充值 升级记录  查 merchants_income)
        $data['count_profit'] = priceFormat(Db::table('biz_settlement')->where(array('biz_id'=>$this->merchants_id))->where("type > 1 and mark = 2 and settlement_status = 2")->sum('tobe_settled'));
        if(empty($data['count_profit'])){
            $data['count_profit'] = priceFormat(0);
        }
        //   ->leftJoin(['member_mapping' => 'mm'], 'mm.member_id=li.member_id')
        #用户详情(merchants_income 用户分组)
        $data['member_info'] = Db::table('biz_settlement bs')
            ->join('member m','m.id = bs.member_id','left')
            ->join('member_mapping mm','mm.member_id = bs.member_id','left')
            ->field('IFNULL(m.member_name,m.nickname) member_name,CONVERT(mm.member_balance,DECIMAL(10,2)) member_balance,bs.*,m.headimage,IFNULL((select level_title from member_level ml where ml.id = mm.member_level_id),"普通用户") level_title')
            ->where(array('bs.biz_id'=>$this->merchants_id))
            ->where("bs.type =2 and bs.mark = 2 and bs.settlement_status = 2")
            ->group('member_id')
            ->select();
        return array('status'=>true,'data'=>$data);
    }
    /*
   * @content 会员用户信息详情
   * @return  array
   * */
    function member_detail(){
        $member_id = input("post.id");
        if(!empty($member_id)){
            $member = Db::table('member_mapping mm')
                ->leftJoin(['member'=>'m'],'mm.member_id  = m.id')
                ->field('mm.member_level_id,mm.member_integral,mm.member_balance,
                IFNULL(m.member_name,m.nickname) member_name,m.member_phone,m.headimage,
                DATE_FORMAT(m.register_time,"%Y-%m-%d %H:%i") register_time,
                mm.member_expiration,m.headimage,IFNULL((select level_title from member_level ml where ml.id = mm.member_level_id),"普通用户") member_level_title')
                ->where("mm.member_id = $member_id")
                ->find();
            if(!empty($member)){
                $member['member_balance'] = priceFormat($member['member_balance']);
                #获取最新的办理会员时间
                $handling_time = Db::table('log_consump')->where(array('member_id'=>$member_id,'income_type'=>2))->field('consump_time,id,member_id')->order('id desc')->find();
                $member['register_time'] = $handling_time['consump_time'];
            }
            return array('status'=>true,'data'=>$member);


        }

    }

    /*
   * @content 会员用户充值记录
   * @return  array
   * */
    function member_recharge(){
        $member_id = input("post.id");
        if(!empty($member_id)){
            $_where = "member_id = $member_id and biz_id = $this->merchants_id and type>1 and mark = 2";

            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'biz_settlement',
                'field' => '*,DATE_FORMAT(create_time,"%Y-%m-%d %H:%i") create_time',
                'order' => 'create_time desc'
            );
            $datalist = BaseService::DataList($data_params);
            $data = MineService::handleMerchantsIncome($datalist);
            return array('status'=>true,'data'=>$data);

        }
    }

}
