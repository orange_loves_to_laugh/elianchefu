<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/23
 * Time: 17:24
 */

namespace app\businessapi\controller;

/*
 * @content :商户管理
 * */

use app\businessapi\ApiService\MerchantsService;
use app\service\BaseService;
use Protocol\Curl;
use Redis\Redis;
use think\Db;

class Merchants extends Common
{

    function delVideoInfo()
    {
        $url = input('post.url');
        Db::table('merchants_video')->where(array('video_url' => $url))->delete();
        return array('status' => true);
    }

    function getVideos()
    {
        $num = 5;
        $numMsg = '视频数量不得超过5条！';
        $info = Db::table('merchants_video')->where(array('m_id' => $this->merchants_id))->column('video_url');
        return array('status' => true, 'info' => $info, 'num' => $num, 'numMsg' => $numMsg);
    }
    function getVideoList()
    {
        $num = 5;
        $numMsg = '视频数量不得超过5条！';
        $info = Db::table('merchants_video')->where(array('m_id' => $this->merchants_id))->select();
        return array('status' => true, 'info' => $info, 'num' => $num, 'numMsg' => $numMsg);
    }

    function addVideos()
    {
        $info = input('post.info');
        $_arr = array();
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                $v = str_replace(array('"', '\\'), "", $v);
                array_push($_arr, array('m_id' => $this->merchants_id, 'video_url' => $v));
            }
        }
        if (!empty($_arr)) {
            Db::table('merchants_video')->insertAll($_arr);
        }
        return array('status' => true);
    }

    function getTopics()
    {
        # 历史话题
        $info = Db::table('merchants_topic')->field('topics title,status')
            ->where(array('m_id' => $this->merchants_id, 'status' => 0))
            ->select();
        # 正在使用的话题
        $tipsArr = Db::table('merchants_topic')
            ->where(array('m_id' => $this->merchants_id, 'status' => 1))
            ->column('topics');
        # 话题数量
        $topicsNum = 10;
        $tipsNum_show = 3;
        # 视频
        $video_info = Db::table('merchants_video')->where(array('m_id' => $this->merchants_id))->limit(3)->column('video_url');
        # 奖励设置
        $_where = "merchants_id = $this->merchants_id and status = 1 and dou_mark=1";
        $time = date("Y-m-d H:i:s");
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_voucher',
            'field' => 'id,title,price,min_consume,start_time,end_time,validity_day,num,use_num,surplus_num',
        );
        $voucher = BaseService::DataList($data_params);
        $voucher = MerchantsService::DataDetail($voucher, $time);
        return array('status' => true, 'data' => $info, 'tipsArr' => $tipsArr,
            'topicsNum' => $topicsNum, 'tipsNum_show' => $tipsNum_show,
            'video_info' => $video_info, 'voucher_info' => $voucher
        );
    }

    function setTopics()
    {
        $info = input('post.info');
        $tipsArr = Db::table('merchants_topic')->field('id,topics')
            ->where(array('m_id' => $this->merchants_id, 'status' => 1))
            ->select();
        $delArr = array();
        $info_new = array();
        if (!empty($tipsArr)) {
            foreach ($tipsArr as $k => $v) {
                if (!in_array($v['topics'], $info)) {
                    array_push($delArr, $v['id']);
                } else {
                    array_push($info_new, $v['topics']);
                }
            }
        }
        $updateArr = array();
        $tipsArrs = Db::table('merchants_topic')->field('id,topics')
            ->where(array('m_id' => $this->merchants_id, 'status' => 0))
            ->select();
        if (!empty($tipsArrs)) {
            foreach ($tipsArrs as $k => $v) {
                if (in_array($v['topics'], $info)) {
                    array_push($updateArr, $v['id']);
                    array_push($info_new, $v['topics']);
                }
            }
        }
        if (!empty($delArr)) {
            Db::table('merchants_topic')->where("id in (" . implode(',', $delArr) . ")")->update(array('status' => 0));
        }
        if (!empty($updateArr)) {
            Db::table('merchants_topic')->where("id in (" . implode(',', $updateArr) . ")")->update(array('status' => 1));
        }
            $_arr = array_diff($info, $info_new);
            if (!empty($_arr)) {
                $_arrAdd = array();
                foreach ($_arr as $k => $v) {
                    array_push($_arrAdd, array('m_id' => $this->merchants_id, 'topics' => $v));
                }
                if (!empty($_arrAdd)) {
                    Db::table('merchants_topic')->insertAll($_arrAdd);
                }
            }
        return array('status' => true);
    }

    function getCopywriting()
    {
        $info = Db::table('merchants_copywriting')->field('id,context discribe')->where(array('m_id' => $this->merchants_id))->select();
        return array('status' => true, 'info' => $info);
    }

    function setCopywriting()
    {
        $add_info = input('post.add_info');
        $del_info = input('post.del_info');
        if (!empty($del_info)) {
            $delId = implode(',', $del_info);
            Db::table('merchants_copywriting')->where("id in (" . $delId . ")")->delete();
        }
        if (!empty($add_info)) {
            $addArr = array();
            foreach ($add_info as $k => $v) {
                if ($v['id'] == 0) {
                    array_push($addArr, array('context' => $v['discribe'], 'm_id' => $this->merchants_id));
                } else {
                    Db::table('merchants_copywriting')->where(array('id' => $v['id']))->update(array('context' => $v['discribe']));
                }
            }
            if (!empty($addArr)) {
                Db::table('merchants_copywriting')->insertAll($addArr);
            }
        }
        return array('status' => true);
    }

    function share_id()
    {
        $share_id = '';
        $client_token_url = 'https://open.douyin.com/oauth/client_token/';
        $order = [
            'client_key' => 'awc69tynmozedlyf',
            'client_secret' => 'fc215af541ed53f39327cdde571d8b8b',
            'grant_type' => 'client_credential'
        ];
        $curl = new Curl();
        $res = $curl->post($client_token_url, $order, 'form');
        $resData = json_decode($res, true);
        if (!empty($resData)) {
            if ($resData['data']['error_code'] == 0) {
                $token = $resData['data']['access_token'];
                $url = 'https://open.douyin.com/share-id/?access_token=' . $token . '&need_callback=true';
                $re = $curl->get($url);
                $reData = json_decode($re, true);
                if ($reData['data']['error_code'] == 0) {
                    $share_id = $reData['data']['share_id'];
                }
            }
        }
        return array("status" => true, 'share_id' => $share_id);;
    }

    /*
    * @content  商户管理 获取商户信息
    * @return  array
    * */
    function getMerchantsInfo()
    {
        $_redis = new Redis();
        $_redis->hDel("merchants", 'merchantsInfo' . $this->merchants_id);
        $merchantsInfo = $_redis->hGet("merchants", 'merchantsInfo' . $this->merchants_id);
        if (!empty($merchantsInfo)) {
            #有缓存
            $data = json_decode($merchantsInfo, true);


        } else {
            #没缓存
            $_where = "status = 1 and id = $this->merchants_id";
            // 获取列表
            $data_params = array(
                'page' => false, //是否分页
                'where' => $_where,//查找条件
            );
            $data = MerchantsService::getMerchantsInfo($data_params);

            $_redis->hSet("merchants", 'merchantsInfo' . $this->merchants_id, json_encode($data));


        }
        # 判断上传视频的权限
        if ($data['open_toutiao'] == 1) {
            # 开启拍视频功能,判断时间
            if ($data['open_expiration_time'] <= date('Y-m-d H:i:s')) {
                # 过期了
                $data['open_toutiao'] = 2;
                Db::table('merchants')->where(array('id' => $data['id']))->update(array('open_toutiao' => 2));
            }
        }
        $imgInfo = array(
            'icon' => imgUrl('static/img/toutiao_bus.gif', true).'?1',
            'video_btn_bg' => imgUrl('static/img/video_btn_bg.png', true),
            'video_popup' => imgUrl('static/img/video_popup.png', true),
        );
        $topic = array_filter(explode('，', $data['douyin_topic']));
        if (empty($topic)) {
            $topic = ['E联车服'];
        }
        $title = $data['title'];
        # 判断商家注册时间决定提现方式
        if(date('Y-m-d',strtotime($data['create_time'])) < '2022-04-30'){
            $data['deposit_pay_type'] = 2;
        }else{
            $data['deposit_pay_type'] = 1;
        }
        return array('status' => true, 'data' => $data, 'imgInfo' => $imgInfo, 'topic' => $topic, 'title' => $title);

    }

    function getVideoInfo()
    {
        # 查询视频列表
//        $arr = array(
//            array(
//                'videoUrl'=>'http://vjs.zencdn.net/v/oceans.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-09-03 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-09-02 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'https://media.w3.org/2010/05/sintel/trailer.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-09-01 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'https://v-cdn.zjol.com.cn/276984.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-08-03 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'https://v-cdn.zjol.com.cn/276986.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-07-03 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'https://v-cdn.zjol.com.cn/276986.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-06-03 09:30',
//                'playStatus'=>true
//            ),
//            array(
//                'videoUrl'=>'https://v-cdn.zjol.com.cn/276986.mp4',
//                'like'=>10,
//                'look'=>50,
//                'share'=>30,
//                'time'=>'2021-09-05 09:30',
//                'playStatus'=>true
//            ),
//        );
        $_redis = new Redis();
        $merchantsInfo = $_redis->hGet("merchants", 'merchantsInfo' . $this->merchants_id);
        if (!empty($merchantsInfo)) {
            #有缓存
            $data = json_decode($merchantsInfo, true);


        } else {
            #没缓存
            $_where = "status = 1 and id = $this->merchants_id";
            // 获取列表
            $data_params = array(
                'page' => false, //是否分页
                'where' => $_where,//查找条件
            );
            $data = MerchantsService::getMerchantsInfo($data_params);

            $_redis->hSet("merchants", 'merchantsInfo' . $this->merchants_id, json_encode($data));


        }
        $arr = array();
        $topic = array_filter(explode('，', $data['douyin_topic']));
        if (empty($topic)) {
            $topic = ['E联车服'];
        }
        $title = Db::table('merchants')->where(array('id' => $this->merchants_id))->value('title');
        return array('status' => true, 'videoInfo' => $arr, 'info' => array_chunk($arr, 6, true), 'topic' => $topic, 'title' => $title);
    }

    function addVideoIntegral()
    {
        $merchant_id = input('post.id');
        $member_id = input('post.member_id');
        $share_id = input('post.share_id');
        Db::table('share_video')
            ->insert(array(
                'member_id' => $member_id,
                'm_id' => $merchant_id,
                'share_id' => $share_id,
                'add_time' => date('Y-m-d H:i:s'),
                'integral_num' => 0
            ));
    }

    /*
      * @content ：创建 修改 商户优惠券
      * @params : 优惠券名称    title
      * @params : 面值金额      price
      * @params :  门槛金额     min_consume
      * @params : 有效期天数    validity_day
      * @params : 开始时间      start_time
      * @params : 结束时间      end_time
      * @params : 发放数量      num
      * @params : 重复领取      is_repeat   1 是 2 否
      * @params : $this->merchants_id     商户id
      * @params : voucher_id   是否有卡券id 有 的话  是修改  没有 是创建
      * */
    function getSaveMerchantsVoucher()
    {
        #优惠券 数据 一维数组
        $params = input("post.");
        if(array_key_exists('dou_mark',$params)){
            if($params['dou_mark']==1){
                $params['start_time'] = date('Y-m-d H:i:s');
                $params['end_time'] = date('Y-m-d H:i:s',strtotime('+10 years'));
            }
        }
        $data = MerchantsService::getSaveMerchantsVoucher($params, $this->merchants_id);
        return array('status' => true);

    }

    /*
     * @content  商户管理 门店优惠券列表
     * @return  array
     * */
    function getMerchantsVoucher()
    {
        #时间筛选  0 全部 1 待上架 2 发放中 3  已结束
        $active_status = input("post.active_status");
        $_where = "merchants_id = $this->merchants_id and status = 1 and dou_mark=2";
        $time = date("Y-m-d H:i:s");
        if (!empty($active_status)) {
            if ($active_status == 1) {
                #待上架
                $_where .= " and '$time' < start_time and '$time' < end_time";

            } else if ($active_status == 2) {
                #发放中
                $_where .= " and '$time' >= start_time and '$time' <= end_time";
            } else if ($active_status == 3) {
                #已结束
                $_where .= " and '$time' > start_time and '$time' > end_time";
            }
        }
        $params = input("post.");
        $_order = "create_time desc";
        #排序  asc 正序   desc  倒序(从大到小)
        $mark = $params['mark'];
        #领取人数排序 sort_num
        if (!empty($params['sort_num'])) {
            $_order = "use_num $mark";

        }
        #金额排序 sort_price
        if (!empty($params['sort_price'])) {
            $_order = "price $mark";
        }
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_voucher',
            'field' => 'id,title,price,min_consume,start_time,end_time,validity_day,num,use_num,surplus_num',
            'order' => $_order
        );
        $data = BaseService::DataList($data_params);
        $data = MerchantsService::DataDetail($data, $time);
        return array('status' => true, 'data' => $data);
    }

    /*
     * @content  商户管理 门店优惠券 获取编辑信息
     * @return  array
     * */
    function getMerchantsVoucherEditList()
    {
        $voucherId = input("post.id");
        $_where = "id = $voucherId and status = 1";
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_voucher',
            'field' => '*',
            'order' => 'id desc'
        );
        $data = BaseService::DataList($data_params)[0];
        /*
          * @params : 优惠券名称    title
          * @params : 面值金额      price
          * @params :  门槛金额     min_consume
          * @params : 有效期天数    validity_day
          * @params : 开始时间      start_time
          * @params : 结束时间      end_time
          * @params : 发放数量      num
          * @params : 重复领取      is_repeat   1 是 2 否
          * @params : $this->merchants_id     商户id
         * */
        return array('status' => true, 'data' => $data);

    }

    /*
     * @content  商户管理 门店优惠券  删除
     * @return  array
     * */
    function getMerchantsVoucherDel()
    {
        $params = input("post.");
        if (!empty($params)) {
            $data = MerchantsService::getMerchantsVoucherDel($params);

        }
        return array('status' => true);
    }

    /*
     * @content  商户管理 修改商户信息  上传门头照片   修改营业时间   创建还没写
     * @return  array
     * */
    function getMerchantsSave()
    {
        #商户修改
        $params = input("post.");
        $mark = $params['mark'];
        if ($mark == 'phone') {
            $params['contacts_phone'] = $params['phone'];

        } else if ($mark == 'tel') {
            $params['tel'] = $params['phone'];

        }
        unset($params['phone'], $params['mark']);


        $data = MerchantsService::getMerchantsSave($params, $this->merchants_id);

        $_redis = new Redis();
        $_redis->hDel("merchants", 'merchantsInfo' . $this->merchants_id);
        return array('status' => true);

    }

    /*
    * @content  商户管理 推荐管理 创建  修改
    * @return  array
    * */
    function getRecommendProSave()
    {
        $params = input("post.");


        $data = MerchantsService::getRecommendProSave($params, $this->merchants_id);
        return array('status' => true, 'data' => $data);

    }

    function getRecommendProSaves()
    {
        $param = input("post.");
        $params = $param['data'];
        $params['id'] = $param['id'];
        $data = MerchantsService::getRecommendProSaves($params, $this->merchants_id);
        return $data;
    }

    /*
    * @content  商户管理 推荐管理列表
    * @return  array
    * */
    function getRecommendPro()
    {
        $params = input("post.");
        $data = MerchantsService::getRecommendPro($params, $this->merchants_id);
        return array('status' => true, 'data' => $data);

    }

    /*
    * @content  商户管理 推荐管理详情
    * @return  array
    * */
    function getRecommendProDetail()
    {
        $id = input("post.id");
        $data = MerchantsService::getRecommendProDetail($id);
        return array('status' => true, 'data' => $data);

    }

    /*
   * @content  商户管理 推荐管理商品删除
   * @return  array
   * */
    function getRecommendProDel()
    {
        $params = input("post.");
        $params['merchants_id'] = $this->merchants_id;


        $data = MerchantsService::getRecommendProDel($params);
        return array('status' => true);

    }

    function changeProStatus()
    {
        $params = input("post.");
        return MerchantsService::changeStatus($params);
    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 更新用户openid
     */
    function updateMerchantsOpenid(){
        $openid = input("post.openid");
        if(!empty($openid)){
            Db::table("merchants")->where(array("id"=>$this->merchants_id))->update(array("wxopenid"=>$openid));
        }
        return array("status"=>true);
    }


}
