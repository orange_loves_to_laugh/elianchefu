<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/2/25
 * Time: 13:36
 */

namespace app\businessapi\controller;


use app\api\ApiService\BalanceService;
use app\api\ApiService\LevelService;
use app\businessapi\ApiService\GetInfoService;
use app\service\IGeTuiService;
use Protocol\Curl;
use Redis\Redis;
use think\Db;
use think\Session;

class Message
{
    private $APPKEY = '';
    private $AccessKeySecret = '';

    function __construct()
    {
        $this->APPKEY = config('merchantConfig.aliappkey');
        $this->AccessKeySecret = config('merchantConfig.aliaccess');
    }

    /**
     * @param $merchants_id
     * @param int $type
     * @param $data
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 发送语音消息
     */
    function send($merchants_id,$type=1,$data)
    {

        if($type==1){
            # 判断商家是否开启语音提醒
            $info = Db::table("merchants")->field("is_voice")->where(array("id"=>$merchants_id))->find();
            if($info['is_voice']==1){
                if($data['pay_type']==1){
                    $pay_type = '微信';
                }elseif($data['pay_type']==2){
                    $pay_type = '支付宝';
                }else{
                    $pay_type = '用户余额';
                }
                # 发送收款成功语音提醒
                $IG = new IGeTuiService(2);

//                $text = "E联智慧生活,".$pay_type."收款".$data['price']."元,请查收!";
                $text = "E联智慧生活,门店收款".$data['price']."元,请查收!";
//                $sendText = "E联智慧生活 ".$pay_type."收款".$data['price']."元 请查收";
                $sendText = "E联智慧生活 门店收款".$data['price']."元 请查收";
//                $sendText = "请注意查收";
                $return_arry = $this->readMessage($sendText,$text);

                $res = $IG->MessageNotification($merchants_id,15,array("title"=>"收款成功","text"=>$text,'rules_url'=>$return_arry['rules_url']));
;           return $res;
            }

        }
        return true;
    }

    /**
     * @return array
     * @context 请求语音合成并返回
     * @param $sendText  有停顿的
     * @param $stext 没有停顿的
     * @param $mark 类型
     */

    function readMessage($sendText,$stext,$mark=''){

//        $msg = input("post.msg");
        $text = $this->TTS($sendText);
        #随机字符串
        $round  = md5(uniqid(md5(microtime(true)),true));
        #生成文件
        $myfile = fopen(ROOT_PATH.'/'.'shareimg/'.$round."file.mp3", "w");
        #生成文件 存到 缓存  下次调用 用来删除文件
        $src = ROOT_PATH.'/'.'shareimg/'.$round."file.mp3";
        $_redis = new Redis();
        if($mark == 'expire'){
            $_redis->hSet("voiceFile",'expireVoice',$src);
        }else{
            $_redis->hSet("voiceFile",'voice',$src);

        }


        fwrite($myfile,$text);
        fclose($myfile);
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $rules = $http_type . $_SERVER['HTTP_HOST'].'/shareimg/'.$round."file.mp3";
        $array['text'] = $stext;
        $array['sendText'] = $sendText;
        #音频地址
        $array['rules_url'] = $rules;


        return $array;


    }
    /*
     * @conten  发送商户到期语音提醒
     * */
    function expir_send($merchants_id,$type=1,$data)
    {
        if($type==1){
            # 判断商家是否开启语音提醒
            $info = Db::table("merchants")->field("is_voice")->where(array("id"=>$merchants_id))->find();
            if($info['is_voice']==1){

                # 发送收款成功语音提醒
                $IG = new IGeTuiService(2);
                #您的推荐商家等级   还有10天后到期
               /* if($data['day'] == 1){
                    $text = "您的".$data['level_title']."即将到期!";
                    $sendText = "您的".$data['level_title']." 即将到期";

                }else{*/
                    $text = "您的".$data['level_title']."等级还有".$data['day']."天后到期!";
                    $sendText = "您的".$data['level_title']."等级还有".$data['day']." 天后到期";

//                }

                $return_arry = $this->readMessage($sendText,$text,'expire');
                $IG->MessageNotification($merchants_id,15,array("title"=>"等级到期提醒","text"=>$text,'rules_url'=>$return_arry['rules_url']));
            }
        }
        return true;
    }
    /*
    * @conten 新的预约订单 支付成功之后
    * */
    function neworder_send($merchants_id,$type=1)
    {
        if($type==1){
            # 判断商家是否开启语音提醒
            $info = Db::table("merchants")->field("is_voice")->where(array("id"=>$merchants_id))->find();
            if($info['is_voice']==1){

                # 发送收款成功语音提醒
                $IG = new IGeTuiService(2);
                #您的推荐商家等级   还有10天后到期
                /* if($data['day'] == 1){
                     $text = "您的".$data['level_title']."即将到期!";
                     $sendText = "您的".$data['level_title']." 即将到期";

                 }else{*/
                $text = "E联商家版提醒您,有新订单了,请接单!";
//                $sendText = "E联商家版提醒您 有新订单了 请接单!";

//                }

//                $return_arry = $this->readMessage($sendText,$text,'expire');
                $IG->MessageNotification($merchants_id,15,array("title"=>"预约订单提醒","text"=>$text),true,false);
            }
        }
        return true;
    }


    /**
     * @param $text
     * @return mixed
     * @context 阿里云语音合成(云盟账户)
     */
    function TTS($text){
        $appkey = $this->APPKEY;
        $token = $this->getAliAccessToken();
        $text = urlencode($text);
        $url = "https://nls-gateway.cn-shanghai.aliyuncs.com/stream/v1/tts?appkey={$appkey}&token={$token}&text={$text}&format=wav&sample_rate=16000&volume=100&speech_rate=50";
        $curl = new Curl();
        $res=$curl->get($url);
        return $res;
    }

    /**
     * @return bool|mixed|string|null
     * @context 获取阿里云token
     */
    function getAliAccessToken()
    {
        $redis = new Redis();
        $token=$redis->hGet("aliToken","Token");
        if(!empty($token)){
            return $token;
        }
        $data=array(
            "AccessKeyId"=>"LTAI4G9Q8mU7xavjS1zL1y4q",
            "Action"=>"CreateToken",
            "Version"=>"2019-02-28",
            "Format"=>"JSON",
            "RegionId"=>"cn-shanghai",
            "Timestamp"=>gmdate("Y-m-d\TH:i:s\Z"),
            "SignatureMethod"=>"HMAC-SHA1",
            "SignatureVersion"=>"1.0",
            "SignatureNonce"=>$this->getsSignatureNonce(),
        );
        $data['Signature']=$this->MakeSign($data);
        $curl = new Curl();
        $res = $curl->post("http://nls-meta.cn-shanghai.aliyuncs.com/",$data,'appcode');
        $res = json_decode($res,true);
        if(empty($res['ErrMsg'])){
            # 计算过期时间
            $time = $res['Token']['ExpireTime']-time();

            $redis->hSet("aliToken","Token",$res['Token']['Id'],$time);
            return $res['Token']['Id'];
        }else{
            return null;
        }
    }
    /**
     * @return string
     * @context 生成uuid 不重复字符串
     */
    function getsSignatureNonce(){
        $point=[8,4,4,4,12];
        $str = md5(microtime());
        $newStr = array();
        $p=0;
        foreach($point as $k=>$v){
            array_push($newStr,substr($str,$p,$v));
            $p+=$v;
        }
        return implode('-',$newStr);
    }

    /**
     * @access public
     * @param $data array 传过来的所有数据
     * @return string
     * context 生成签名
     */
    function MakeSign($data)
    {
        $accessKeySecret=$this->AccessKeySecret;
        # 初始化返回结果
        $_receipt = null;
        if (is_array($data) and !empty($data)) {
            # 对数组进行字典序排序
            ksort($data);
            #将参数格式化成url参数
            $buff = null;
            foreach ($data as $k => $v) {
                $buff .= urlencode($k). "=" . urlencode($v). "&";
            }
            $buff = trim($buff, "&");
            //将HTTP请求的方法（GET）、URL编码的URL路径（/）、
            //第1步获取的规范化请求字符串使用与（&）符号连接成待签名字符串：HTTPMethod + "&" + percentEncode("/") + "&" + percentEncode(queryString)。
            $_receipt = "POST"."&".urlencode('/')."&".urlencode($buff);
            $_receipt =urlencode(base64_encode(hash_hmac("sha1",$_receipt, $accessKeySecret.'&', true)));
        }
        return $_receipt;
    }


    /**
     * @param $merchants_id
     * @param int $type
     * @param $data
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 发送语音消息 给用户端用户发送消息
     */
    function user_send($merchants_id,$type=1,$data)
    {
        #商户名称
        $merchants_title = Db::table('merchants')->where(array('id'=>$merchants_id))->value('title');
        $balance = Db::table('member_mapping')->where(array('member_id'=>$data['member_id']))->value('member_balance');

        if($type==1){

                if($data['pay_type']==1){
                    $pay_type = '微信';
                }elseif($data['pay_type']==2){
                    $pay_type = '支付宝';
                }else{
                    $pay_type = '用户余额';
                }
                # 发商户支付成功语音提醒  给用户发送
                $IG = new IGeTuiService(1);

//                $text = $pay_type."收款".$data['price']."元";
//                $text = "E联智慧生活,".$pay_type."收款".$data['price']."元,请查收!";
                $text = "【E联车服】亲爱的用户:您给".$merchants_title."商户,".$pay_type."支付了".$data['price']."元,用户余额剩余".priceFormat($balance)."元!";
//                $sendText = "E联智慧生活 ".$pay_type."收款".$data['price']."元 请查收";
//                $sendText = "请注意查收";
//                $return_arry = $this->readMessage($sendText,$text);
                //'rules_url'=>$return_arry['rules_url']

                $res = $IG->MessageNotification($data['member_id'],16,array("title"=>"商户支付成功","text"=>$text));
                return $res;
        }

        return true;
    }
    /*
     * @content 请求返回的短信内容
     * */
    function getContent($merchants_id,$data){
        #商户名称
        $merchants_title = Db::table('merchants')->where(array('id'=>$merchants_id))->value('title');
        $balance = Db::table('member_mapping')->where(array('member_id'=>$data['member_id']))->value('member_balance');

        if($data['pay_type']==1){
            $pay_type = '微信';
        }elseif($data['pay_type']==2){
            $pay_type = '支付宝';
        }else{
            $pay_type = '用户余额';
        }


        $content="您给".$merchants_title."商户,".$pay_type."支付了".priceFormat($data['price'])."元,用户余额剩余".priceFormat($balance)."元!";

        return $content;
    }



}
