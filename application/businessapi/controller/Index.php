<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/21
 * Time: 11:41
 */

namespace app\businessapi\controller;

use app\businessapi\ApiService\OrderService;
use app\service\BaseService;
use app\service\EalspellCommissionService;
use app\service\MerchantService;
use Redis\Redis;
use think\Controller;
use think\App;
use think\Db;

class Index extends Common
{
    function __construct(App $app = null)
    {
        parent::__construct($app);

    }

    function index()
    {
        return $this->fetch("/index/index");
    }

    /*
     * @content ：查询商家端首页
     * @return :array
     * */
    function homeInfo()
    {
        $time = date(' H:i:s');
        #获取token
//        $token = MerchantService::AppMerchantInfoHandle(1);

        #调用查询方法
        #首先查询   商户信息
        $where_bussiness = " id = $this->merchants_id";
        $data_params_business = array(
            'page' => false,
            'number' => 10,
            'where' => $where_bussiness,
            'table' => 'merchants',
            'field' => '*',
            'order' => 'id desc'
        );

        $info = BaseService::DataList($data_params_business)[0];
        if (!empty($info)) {

            #查询打烊状态
            if ($info['temporary_close'] == 0) {
                #判断营业开始时间 是否大于 结束时间 eg:9.00  > 2.00  意味着 第2天2点之前都算营业  当前时间大于 结束  小于开始
                if (strtotime($info['start_time']) > strtotime($info['end_time'])) {
                    #第二天
                    if (strtotime($time) > strtotime($info['end_time']) and strtotime($time) < strtotime($info['start_time'])) {
                        $info['start_status_title'] = '打烊';

                    } else {
                        $info['start_status_title'] = '营业中';

                    }

                } else {
                    #正常 24 小时内  第一天
                    #没有临时打烊  当前时间 大于等于 营业开始时间  小于 营业结束时间  营业状态为营业中 反之 打烊
                    if (strtotime($time) >= strtotime($info['start_time']) and strtotime($time) < strtotime($info['end_time'])) {
                        $info['start_status_title'] = '营业中';
                    } else if (strtotime('00:00') == strtotime($info['start_time']) and strtotime('00:00') == strtotime($info['end_time'])) {
                        $info['start_status_title'] = '营业中';

                    } else {
                        $info['start_status_title'] = '打烊';

                    }
                }


            } else {
                #有临时打烊  当前时间 大于等于 打烊时间  小于打烊时间+时长 营业状态为营业中 反之 打烊
                $temporary_close = $info['temporary_close'];
                #打烊的结束时间
                $end_time = date('H:i:s', strtotime("+$temporary_close hours", strtotime($info['close_time'])));

                if ($time >= $end_time and $time < $info['end_time']) {
                    $info['start_status_title'] = '营业中';
                } else {
                    $info['start_status_title'] = '打烊';

                }

            }
        }
        #查询订单管理个数 (待到店)
        $where_merchantsOrder = " order_status = 1 and merchants_id = $this->merchants_id";
        $data_params_merchantsOrder = array(
            'page' => false,
            'number' => 10,
            'where' => $where_merchantsOrder,
            'table' => 'merchants_order',
            'field' => '*',
            'order' => 'id desc'
        );
        $info['merchantsOrder'] = count(BaseService::DataList($data_params_merchantsOrder));
        #查询消息管理个数
        if (!empty($info['create_time'])) {
            $register_time = $info['create_time'];

        } else {
            $register_time = date("Y-m-d H:i:s");
        }
        $info['merchantsNews'] = Db::table("news")->where("is_del=2 and  news_accept=4 and news_sendtime>='" . $register_time . "'
         and id not in (select news_id from news_accepter where accepter_id = {$this->merchants_id} and accepter_type=4)")->count();

        #查询系统通知
        $time = date("Y-m-d H:i:s");
        $where_merchantsNotice = "accepter= 6 and '$time'>= start_time and '$time' <end_time";
        $data_params_merchantsNotice = array(
            'page' => false,
            'number' => 10,
            'where' => $where_merchantsNotice,
            'table' => 'notice',
            'field' => 'id,content   ',
            'order' => 'id desc'
        );
        $res = BaseService::DataList($data_params_merchantsNotice);
        if(!empty($res)) {
            $merchantsNotice = $res[0];
            $info['merchantsNotice'] = $merchantsNotice['content'];
        }
        return array('status' => true, 'data' => $info);


    }

    /*
     * @content ：查询商家端首页 商家 收款信息
     * @return :array
     * */
    function homeOrderInfo()
    {
        #查询 今日 本周   本月的 商家订单的收款金额
        $_where = "merchants_id = $this->merchants_id";

        $mark = input("post.mark");//1 今日 2 本周 3 本月 5 上月
        $type = input("post.type");// type  = dataCenter  为数据中心

        if ($mark == 1) {
            #今日
            $info = self::getTodayInfo($_where, 'd');

        } else if ($mark == 2) {
            #本周
            $info = self::getTodayInfo($_where, 'w');

        } else if ($mark == 3) {
            #本月
            $info = self::getTodayInfo($_where, 'm');

        } else if ($mark == 5) {
            #上月
            $info = self::getTodayInfo($_where, 'last month');

        } else if ($mark == 4) {
            #时间筛选
            $today_start = input("post.start_time");
            $today_end = input("post.end_time");

            #查询 符合的条件
            if ($type == 'dataCenter') {
                #数据中心 时间筛选 订单的 结算时间  merchants_income 的 checkout_time
                $_where .= " and DATE_FORMAT(checkout_time,'%Y-%m')>= '$today_start' and DATE_FORMAT(checkout_time,'%Y-%m')<='$today_end'";
            } else {
                $_where .= " and DATE_FORMAT(checkout_time,'%Y-%m-%d')>= '$today_start' and DATE_FORMAT(checkout_time,'%Y-%m-%d')<='$today_end'";
            }


            $info = self::getTimeInfo($_where);

        } else {
            if ($type == 'dataCenter') {

                #默认查询本月
                $info = self::getTodayInfo($_where, 'm');

            } else {
                #默认查询今天
                $info = self::getTodayInfo($_where, 'd');
            }

        }

        return array('status' => true, 'data' => $info);


    }

    /*
     * @content 1 今日 2 本周 3 本月
     * @return  array
     * */
    static function getTodayInfo($_where, $day, $type = '')
    {
        if ($type == 'getLogCollection') {
            #点击收款记录查看 merchants_income 表 支付方式：1微信 2支付宝 3平台余额
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'whereTime' => $day,
                'table' => 'merchants_income',
                'field' => "merchants_id,DATE_FORMAT(checkout_time,'%Y-%m-%d %H:%i') create_time,member_id,pay_type,
                 CASE pay_type WHEN 1 THEN '微信支付'WHEN 2 THEN '支付宝支付'WHEN 3 THEN '银联支付'WHEN 4 THEN '现金支付' WHEN 5 THEN '余额支付' WHEN 6 THEN '卡券支付' WHEN 7 THEN '积分支付' WHEN 8 THEN '平台奖励' WHEN 9 THEN '任务奖励'WHEN 10 THEN '会员推荐收入'WHEN 11 THEN '会员充值提成' ELSE '会员升级提成' END pay_type_title,
                price,order_number",
                'order' => 'id desc',
                'create_time' => 'checkout_time'
            );
            #交易笔数 count_strok  收款金额 cout_price
            $data = BaseService::DataList($data_params);


        } else {
            #merchants_income 表

            #收款金额 cout_price
            $data['cout_price'] = priceFormat((Db::table('merchants_income')->where($_where)->where("pay_type=5 or pay_type=1 or pay_type=2  ")->whereTime('checkout_time', $day)->sum('price')));

            #交易笔数
            $data['count_strok'] = Db::table('merchants_order')->where($_where)->where("order_status = 2 and pay_type != 5")->whereTime('checkout_time', $day)->count('id');
            #付款顾客
            $data['payment_user'] = Db::table('merchants_order')->where($_where)->where("order_status = 2 and pay_type != 5")->whereTime('checkout_time', $day)->group('member_id')->count('member_id');
            #余额
            $data['payment_type1'] = getsPriceFormat(Db::table('merchants_income')->where("pay_type=5 and $_where")->whereTime('checkout_time', $day)->sum('price'));
            #微信
            $data['payment_type2'] = getsPriceFormat(Db::table('merchants_income')->where("pay_type=1 and $_where")->whereTime('checkout_time', $day)->sum('price'));
            #支付宝
            $data['payment_type3'] = getsPriceFormat(Db::table('merchants_income')->where("pay_type=2 and $_where")->whereTime('checkout_time', $day)->sum('price'));
        }

        return $data;
    }

    /*
     * @content 时间筛选
     * @return  array
     * @return  type=getLogCollection   收款记录页面   核销订单 成功之后加 merchants_income
     * */
    static function getTimeInfo($_where, $type = '')
    {
        if ($type == 'getLogCollection') {
            #点击收款记录查看 merchants_income 表 支付方式：1微信 2支付宝 3平台余额
            $data_params = array(
                'page' => false,
                'number' => 10,
                'where' => $_where,
                'table' => 'merchants_income',
                'field' => 'merchants_id, DATE_FORMAT(checkout_time,\'%Y-%m-%d %H:%i\') create_time,member_id,pay_type,
                    CASE pay_type WHEN 1 THEN \'微信支付\'WHEN 2 THEN \'支付宝支付\'WHEN 3 THEN \'银联支付\'WHEN 4 THEN \'现金支付\' WHEN 5 THEN \'余额支付\' WHEN 6 THEN \'卡券支付\' WHEN 7 THEN \'积分支付\' WHEN 8 THEN \'平台奖励\'WHEN 9 THEN \'任务奖励\'WHEN 10 THEN \'用户推荐收入\'WHEN 11 THEN \'用户充值提成\' ELSE \'用户升级提成\' END pay_type_title,
                    price,order_number',
                'order' => 'id desc',
            );
            #交易笔数 count_strok  收款金额 cout_price
            $data = BaseService::DataList($data_params);
        } else {
            #merchants_income 表

            #收款金额 cout_price
            $data['cout_price'] = priceFormat((Db::table('merchants_income')->where($_where)->where("pay_type=5 or pay_type=1 or pay_type=2  ")->sum('price')));

            #交易笔数 查询结算时间的
            $data['count_strok'] = Db::table('merchants_order')->where($_where)->where("order_status = 2 and pay_type != 5")->count('id');
            #付款顾客  查询结算时间的
            $data['payment_user'] = Db::table('merchants_order')->where($_where)->where("order_status = 2 and pay_type != 5")->group('member_id')->count('member_id');
            #余额
            $data['payment_type1'] = Db::table('merchants_income')->where("pay_type=5 and $_where")->sum('price');
            #微信
            $data['payment_type2'] = Db::table('merchants_income')->where("pay_type=1 and $_where")->sum('price');
            #支付宝
            $data['payment_type3'] = Db::table('merchants_income')->where("pay_type=2 and $_where")->sum('price');

        }
        return $data;
    }

    /*
     * @content ：首页收款记录
     * @return :array
     * */
    function getLogCollection()
    {
        #查询 今日 本周   本月的 商家订单的收款金额
        $_where = "merchants_id=$this->merchants_id";

        $mark = input("post.mark");//1 今日 2 本周 3 本月
        if ($mark == 1) {
            #今日
            $today_start = input("post.start_time");
            $today_end = input("post.end_time");
            if ($today_start == '开始时间' and $today_end == '结束时间') {
                $info = self::getTodayInfo($_where, 'd', 'getLogCollection');
            } else {
                $_where .= " and DATE_FORMAT(checkout_time,'%Y-%m-%d')>= '$today_start' and DATE_FORMAT(checkout_time,'%Y-%m-%d')<= '$today_end'";
                $info = self::getTimeInfo($_where, 'getLogCollection');
            }

        } else if ($mark == 2) {
            #本周
            $info = self::getTodayInfo($_where, 'w', 'getLogCollection');

        } else if ($mark == 3) {
            #本月
            $info = self::getTodayInfo($_where, 'm', 'getLogCollection');

        } else if ($mark == 5) {
            #上月
            $info = self::getTodayInfo($_where, 'last month');

        } else if ($mark == 4) {
            #时间筛选
            $today_start = input("post.start_time");
            $today_end = input("post.end_time");

            $_where .= " and DATE_FORMAT(checkout_time,'%Y-%m-%d')>= '$today_start' and DATE_FORMAT(checkout_time,'%Y-%m-%d')<= '$today_end'";
            $info = self::getTimeInfo($_where, 'getLogCollection');

        } else {
            #今日
            $info = self::getTodayInfo($_where, 'd', 'getLogCollection');
        }


        return array('status' => true, 'data' => $info);
    }


    /*
    * @content ：查询商户订单信息
    * @return :array
    * */
    static function getMerchantsOrder($order_number)
    {
        # 付款金额:total_price,   卡券抵用金额  merchants_voucher_price    实际支付:actual_price 平台抽成:commission_price  实际到账: actual_account 付款时间:checkout_time  会员手机号:member_phone    评论状态 evalua_level  34 中评 3 以下 差评 5 好评       评语 evalua_comment
        $member_id = Db::table('merchants_order')->where(array('order_number' => $order_number))->value('member_id');

        if (!empty($member_id)) {
            $data = Db::table('merchants_order mo')
                ->field('mo.total_price,actual_price,mo.commission_price,
            DATE_FORMAT(mo.checkout_time,\'%Y-%m-%d %H:%s\') checkout_time,
            IFNULL(m.member_name,m.nickname) member_name,m.member_phone,me.evalua_level,me.evalua_comment,
            mo.id order_id,mo.member_id,mm.member_balance,mo.cash_type')
                ->join('merchants_evaluation me', 'me.merchants_order_id = mo.id', 'left')
                ->join('member m', 'm.id = mo.member_id')
                ->join('member_mapping mm', 'm.id = mm.member_id')
                ->where("mo.order_number = {$order_number} ")
                ->find();
        } else {
            $data = Db::table('merchants_order mo')
                ->field('mo.total_price,actual_price,mo.commission_price,
            DATE_FORMAT(mo.checkout_time,\'%Y-%m-%d %H:%s\') checkout_time,
            me.evalua_level,me.evalua_comment,mo.cash_type,
            mo.id order_id,mo.member_id')
                ->join('merchants_evaluation me', 'me.merchants_order_id = mo.id', 'left')
                ->where("mo.order_number = {$order_number} and mo.order_status = 2")
                ->find();
        }
        if (!empty($data)) {

            if (empty($data['member_id'])) {
                $data['member_phone'] = '暂无手机号';
            }
            if ($data['evalua_level'] == 3 or $data['evalua_level'] == 4) {
                $data ['evalua_level_title'] = '中评';
            } else if ($data['evalua_level'] == 5) {
                $data ['evalua_level_title'] = '好评';
            } else {
                $data ['evalua_level_title'] = '差评';

            }
            #订单类型
            $data['order_type_title'] = BaseService::StatusHtml($data['cash_type'], lang('cash_type'), false);
            #查询卡券低值金额
            $price = OrderService::getMerchantsVoucherPrice($data['member_id'], $data['order_id']);
            if (empty($price['price'])) {
                $data['merchants_voucher_price'] = 0;
                #判断是否有卡券抵用  无  到账金额 = 实际到账
                $data['arrival_account'] = priceFormat($data['total_price'] - $data['commission_price']);
                $data['actual_account'] = priceFormat($data['total_price'] - $data['commission_price']);
                $data['platform_title'] = '';//卡券补贴来源
            } else {
                #判断 是商家的卡券  还是平台的卡券
                if ($price['voucher_type'] == 1) {
                    #平台 实际支付+ 卡券金额 - 抽成金额 =到账金额 = 实际到账+卡券
                    $data['arrival_account'] = priceFormat(($data['total_price']));
                    $data['actual_account'] = priceFormat(($data['total_price'] - $data['commission_price']));
                    $data['platform_title'] = '平台补贴';
                } else {
                    #商家  实际支付 - 减抽成 = 到账金额 = 实际到账
                    $data['arrival_account'] = priceFormat($data['actual_price']);
                    $data['actual_account'] = priceFormat($data['actual_price'] - $data['commission_price']);
                    $data['platform_title'] = '商家优惠券';

                }

                $data['merchants_voucher_price'] = $price['price'];
            }

            #查询到账金额

        }
        return $data;
    }

    /*
  * @content ：查询商户订单 的 待到店的订单列表
  * @return :array
  * */
    function getMerchantsOrderList()
    {
        #查询 merchants_order  order_status = 1; 订单列表 传过来有类型  type 1 待到店 2 历史记录  手机号  订单号搜索  search  开始时间 start_time   结束时间 end_time   订单号：order_number
        $params = input("post.");
        $params['merchants_id'] = $this->merchants_id;
        $data = OrderService::getMerchantsOrderlist($params);
        return array('status' => true, 'data' => $data);


    }

    /*
      * @content ：查询商户订单 的 详情(首页 核销确认订单页 信息)
      * @return :array
      * */
    function getMerchantsOrderDetail()
    {
        #查询
        $order_number = input("post.order_number");
        #type 1 待到店  2 历史记录 （到店）
        $type = input("post.type");
        $data = OrderService::getMerchantsOrderDetail($order_number, $type);
        if (!empty($data)) {
            return array('status' => true, 'data' => $data);

        } else {
            return array('status' => false, 'msg' => '暂无订单信息!');

        }


    }

    /*
    * @content ：首页会员搜索
    * @return :array
    * */
    function getSearchMember()
    {
        $phone = input("post.phone");
        if (!empty($phone)) {
            $data = OrderService::getSearchMember($phone);
            if (!empty($data)) {
                return array('status' => true, 'data' => $data);

            } else {
                return array('status' => false, 'msg' => '暂无用户信息！');

            }

        } else {
            return array('status' => false, 'msg' => '手机号为空！');
        }

    }

    /*
      * @content ：首页消息列表查询 联盟商家消息
      * @return :array
      * */
    function getHomeNewsList()
    {
        # 查询未读消息  $this->merchants
        $register_time = $this->merchants['create_time'];
        # 用户未读消息数量
        $unread = Db::table("news n")
            ->field("n.id,n.news_title,n.news_context,n.news_author,date_format(n.news_sendtime,'%Y-%m-%d %H:%i') news_sendtime,n.news_thumb,1 as reader_status,1 as addstatus")
            ->where("is_del=2 and  news_accept=4 and news_sendtime>='" . $register_time . "'
        and id not in (select news_id from news_accepter where accepter_id = {$this->merchants_id} and accepter_type=4)")
            ->order("n.news_sendtime desc")
            ->select();
        # 查询已读消息
        $read = Db::table("news_accepter na,news n")
            ->field("n.id,n.news_title,n.news_author,date_format(n.news_sendtime,'%Y-%m-%d %H:%i') news_sendtime,n.news_thumb,na.reader_status,2 as addstatus")
            ->where(array("na.accepter_id" => $this->merchants_id, "na.accepter_type" => 4, "na.is_del" => 2))
            ->where("na.news_id=n.id")
            ->order("na.id desc")
            ->select();
        # 合并消息
        $newsList = array_merge($unread, $read);


        foreach ($newsList as $k => $v) {
            #去掉img 标签
            $newsList[$k]['news_context'] = preg_replace('/<img[^>]+>/i', '', $v['news_context']);

        }
        return array("status" => true, "data" => $newsList);
    }


    /*
      * @content ：消息详情查询 联盟商家消息详情
      * @return :array
      * */
    function news_detail()
    {
        $id = input("post.id");
        $addstatus = input("post.addstatus");
        if ($addstatus == 1) {
            # 查询这个消息是否添加过
            $is_repet = Db::table("news_accepter")->where(array("news_id" => $id, "accepter_type" => 4, "accepter_id" => $this->merchants_id))->find();
            if (empty($is_repet)) {
                # 未添加状态 先添加到已读消息
                Db::table("news_accepter")->insert(array("news_id" => $id, "accepter_type" => 4, "accepter_id" => $this->merchants_id, "reader_status" => 2));
            }
        }
        # 查询消息详情
        $info = Db::table("news")->field("*,date_format(news_sendtime,'%Y-%m-%d %H:%i') news_sendtime")->where(array("id" => $id))->find();
        if (!empty($info)) {
            $info['news_type_title'] = '系统消息';
        }
        return array("status" => true, "data" => $info);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 消息删除
     */
    function news_del()
    {
        $id = input("post.id");
        $addstatus = input("post.addstatus");

        if ($addstatus == 1) {
            # 查询这个消息是否添加过
            $is_repet = Db::table("news_accepter")->where(array("news_id" => $id))->find();
            if (empty($is_repet)) {
                # 未添加状态 先添加到已读消息
                Db::table("news_accepter")->insert(array("news_id" => $id, "accepter_type" => 4, "accepter_id" => $this->merchants_id, "reader_status" => 2, "is_del" => 1));
                return array("status" => true);
            }
        }
        Db::table("news_accepter")->where(array("news_id" => $id, "accepter_type" => 4, "accepter_id" => $this->merchants_id))->update(array("is_del" => 1));
        return array("status" => true);
    }


    function getDouYinInfo()
    {
        #查询 今日 本周   本月的 抖音数据
        $_where = "m_id = $this->merchants_id";

        $mark = input("post.mark");//today 今日 week 本周 month 本月 search 搜索
        $type = input("post.type");// type  = dataCenter  为数据中心

        if ($mark == 'today') {
            $time_where = "to_days(add_time) = to_days(now())";
            #今日
            /*$info = array(
                'share_people' => 0,//总分享人数
                'share_count' => 0,//总分享次数
                'play_count' => 0,//总播放量
                'like_count' => 0,//总点赞量
                'comment_count' => 0,//总评论量
            );*/
        } else if ($mark == 'week') {
            #本周
            $time_where = "YEARWEEK(date_format(add_time,'%Y-%m-%d')) = YEARWEEK(now())";

        } else if ($mark == 'month') {
            #本月
            $time_where = "DATE_FORMAT(add_time,'%Y%m') = DATE_FORMAT(CURDATE(),'%Y%m')";

        } else if ($mark == 'search') {
            #时间筛选
            $today_start = input("post.start_time");
            $today_end = input("post.end_time");
            $time_where = "DATE_FORMAT(add_time,'%Y%m') >= '" . $today_start . "' and DATE_FORMAT(add_time,'%Y%m') <= '" . $today_end . "'";

        } else {
            #默认查询今天
            $time_where = "to_days(add_time) = to_days(now())";

        }
        # 假数据
        $info = Db::table('merchants_dou')
            ->field("IFNULL(sum(share_people),0) share_people,
            IFNULL(sum(share_count),0) share_count,
            IFNULL(sum(play_count),0) play_count,
            IFNULL(sum(like_count),0) like_count,
            IFNULL(sum(comment_count),0) comment_count
            ")
            ->where($_where)
            ->where($time_where)
            ->find();
        # 真实数据
        $shareInfo = Db::table('share_video')
            ->field("IFNULL(count(distinct member_id),0) share_people,
            IFNULL(count(member_id),0) share_count")
            ->where($_where)
            ->where($time_where)
            ->find();
        if (!empty($shareInfo)) {
            $info['share_people'] += $shareInfo['share_people'];
            $info['share_count'] += $shareInfo['share_count'];
        }
        return array('status' => true, 'data' => $info);
    }

    function getDouChartsInfo()
    {
        $redis = new Redis();
        # 当前登陆人数
        $nowNum = $redis->get('loginNumMerchants' . $this->merchants_id);
        if (empty($nowNum)) {
            $nowNum = 0;
        }
        $nowNum += 1;
        if ($nowNum >= 3) {
            # 随机加数据
            $rand_play = rand(1, 10);
            # 修改数据
            Db::table('merchants_dou')
                ->where(" m_id = " . $this->merchants_id . " and date(add_time) = '" . date('Y-m-d') . "'")
                ->setInc('play_count', $rand_play);
            $redis->set('loginNumMerchants' . $this->merchants_id, 0);
        } else {
            $redis->set('loginNumMerchants' . $this->merchants_id, $nowNum);
        }
        $categories = array(
            date('d', strtotime('-6 days')) . '日',
            date('d', strtotime('-5 days')) . '日',
            date('d', strtotime('-4 days')) . '日',
            date('d', strtotime('-3 days')) . '日',
            date('d', strtotime('-2 days')) . '日',
            date('d', strtotime('-1 days')) . '日',
            date('d') . '日',
        );
        $info = Db::query("select a.click_date,ifnull(share_people,0) share_people,ifnull(share_count,0) share_count,
ifnull(play_count,0) play_count,ifnull(like_count,0) like_count,ifnull(comment_count,0) comment_count
from (
    SELECT date_sub(curdate(), interval 6 day) as click_date
    union all
    SELECT date_sub(curdate(), interval 5 day) as click_date
    union all
    SELECT date_sub(curdate(), interval 4 day) as click_date
    union all
    SELECT date_sub(curdate(), interval 3 day) as click_date
    union all
    SELECT date_sub(curdate(), interval 2 day) as click_date
    union all
    SELECT date_sub(curdate(), interval 1 day) as click_date
    union all
    SELECT curdate() as click_date
) a left join (
  select date(add_time) as addtime, sum(share_people) share_people,sum(share_count) share_count,
  sum(play_count) play_count,sum(like_count) like_count,sum(comment_count) comment_count 
  from merchants_dou where m_id = " . $this->merchants_id . "
  group by date(add_time) 
) b on a.click_date = b.addtime");
        if (!empty($info)) {
            $playInfo = array_column($info, 'play_count');
            $likeInfo = array_column($info, 'like_count');
            $commentInfo = array_column($info, 'comment_count');
        } else {
            $playInfo = [0, 0, 0, 0, 0, 0, 0];
            $likeInfo = [0, 0, 0, 0, 0, 0, 0];
            $commentInfo = [0, 0, 0, 0, 0, 0, 0];
        }
        $series = array(
            array(
                'name' => '播放',
                'data' => $playInfo
            ),
            array(
                'name' => '点赞',
                'data' => $likeInfo
            ),
            array(
                'name' => '评论',
                'data' => $commentInfo
            ),
        );
        $data = array('categories' => $categories, 'series' => $series);
        return array('status' => true, 'data' => $data);
    }

    function orderRefund(){
        # 查询订单信息
        $order_number = input("post.order_number");
        if(!empty($order_number)){
            $orderInfo = Db::table("merchants_order")->where(array("order_number"=>$order_number))->find();
            if(!empty($orderInfo) and $orderInfo['order_status']==1){
                $res = OrderService::refundTransfer($orderInfo['cash_type'],$order_number,$orderInfo['member_id'],$orderInfo['actual_price']);
                if(!$res){
                    return array("status"=>false,"msg"=>"退款失败");
                }else{
                    # 将订单改为已退款
                    Db::table("merchants_order")->where(array("order_number"=>$order_number))->update(array("order_status"=>3));
                    EalspellCommissionService::refundDeduct($order_number);
                    return array("status"=>true,"msg"=>"退款成功");
                }

            }else{
                return array("status"=>false,"msg"=>"该订单不可退款");
            }
        }else{
            return array("status"=>false,"msg"=>"订单号错误");
        }

    }


}
