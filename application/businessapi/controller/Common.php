<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/8/2 0002
 * Time: 11:39
 */

namespace app\businessapi\controller;


use app\businessapi\ApiService\MerchantsService;
use app\businessapi\ApiService\TaskService;
use app\service\MemberService;
use app\service\MerchantService;
//use phpmailer\BaseException;
use think\Controller;
use think\facade\Request;
use think\route\Resource;

/**
 * 接口公共控制器
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年7月22日10:57:35
 */
class Common extends Controller
{
    // 用户id
    protected $merchants_id='';

    // 用户id
    protected $merchants=[];

    // 输入参数 post
    public $data_post;

    // 输入参数 get
    protected $data_get;

    // 输入参数 request
    protected $data_request;

    /**
     * 构造方法
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     */
    public function __construct()
    {
        // 输入参数
        $this->data_post = input('post.');
        $this->data_get = input('get.');
        $this->data_request = input();
        parent::__construct();
        # 结算定时任务
        GetIndex::settlementTask();
        // 公共数据初始化
        if(!empty(Request::header('token')))
        {
            $this->CommonInit();
        }
        TaskService::merchantsDayTaskCommission();
        MerchantsService::merchantsDepositInit();
    }

    /**
     * [CommonInit 公共数据初始化]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     */
    private function CommonInit()
    {
        if(empty(cache(Request::header('token'))))
        {
            return true;
            throw new BaseException(['code'=>403 ,'errorCode'=>1015,'msg'=>'请授权登录','status'=>false,'debug'=>false ]);
        }

        // 商户数据
        $this->merchants = MerchantService::LoginMerchantInfo(['token'=>Request::header('token')]);
        $this->merchants_id = cache(Request::header('token'))['id'];
        if($this->merchants_id){
            //TaskService::finishDayTask($this->merchants_id,1);
        }
    }

    /**
     * [Islogin 登录校验]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     */
    public function Islogin()
    {

        if(!isset($this->merchants['tel']))
        {
            throw new \BaseException(['code'=>403 ,'errorCode'=>1015,'msg'=>'请用手机号登录','status'=>false,'debug'=>false ]);

        }
    }


    /**
     * [_empty 空方法操作]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version  0.0.1
     * @datetime 2020年7月22日10:57:35
     * @param    [string]      $name [方法名称]
     */
    protected function _empty($name)
    {
        exit(json_encode(DataReturn($name.' 非法访问', -1000)));
    }


}