<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/2/4
 * Time: 21:35
 */

namespace app\businessapi\controller;


use app\businessapi\ApiService\MerchantsService;
use app\service\BaseService;
use Redis\Redis;
use think\Controller;
use think\Db;
use think\facade\Request;

class GetIndex extends Controller
{
    /*
     * @content  商户管理  创建商户信息
     * @return  array
     * */
    function getMerchantsSave(){
        #商户修改
        $params = input("post.");
        if(!empty($params['cate_id'])){
            if(count($params['cate_id'])>3){
                return array('status'=>false,'msg'=>'行业类型不得超过3个!');

            }
        }


        $data = MerchantsService::getMerchantsSave($params,'');

        return array('status'=>true);

    }
    /**
     * @return array
     * @context 验证用户支付密码
     */
    function validatePassWord()
    {
        $password = md5(input("post.password"));
        $member_paypass = Db::table('member_mapping')->where(array('member_id'=>input("post.member_id")))->value('member_paypass');
        if ($password == $member_paypass) {
            return array("status" => true);
        } else {
            return array("status" => false, "msg" => "密码错误");
        }
    }
    /**
     * @return array
     * @context 请求 一级分类信息
     */
    function getCateClass(){
        $_where = 'status=1 and type = 2 and pid = 0';
        $title = input("post.title");
        if(!empty($title)){
            $_where = " and  title like '%".$title."%'";



        }
        $data_params = array(
            'page'         => true,
            'number'         => 10,
            'where'          =>$_where,
            'table'     =>'merchants_cate',
            'order'     =>'sort_num desc'

        );
        $data = BaseService::DataList($data_params);
        return array('status'=>true,'data'=>$data);
    }
    /**
     * @return array
     * @context 请求 二级分类信息
     */
    function getCateClassid(){
        $pid = input("post.pid");
        $_where = "status=1 and type = 2 and pid = $pid";

        $data_params = array(
            'page'         => true,
            'number'         => 10,
            'where'          =>$_where,
            'table'     =>'merchants_cate',
            'order'     =>'sort_num desc'

        );
        $data = BaseService::DataList($data_params);
        return array('status'=>true,'data'=>$data);

    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 检查更新
     */
    function CheckUpdates()
    {
        # 检查更新
        $appid = input("post.appid") ?? '__UNI__F77E396';
        $version = input("post.version") ?? '1.0.0'; //客户端版本号
        $type = input('post.system') ?? 'android';
        $rsp = array("status" => true, "up_status" => false); //默认返回值，不需要升级
        if (isset($appid) && isset($version)) {
            # 查询最新的版本信息
            $info = Db::table('merchants_version_control')
                ->where(array('appid' => $appid))
                ->order(array('version' => 'desc'))
                ->find();
            if($type=='ios'){
                $info['version'] = $info['ios_version'];
            }
            if (!empty($info) and intval(str_replace('.', '', $version)) < intval(str_replace('.', '', $info['version']))) { //校验appid
                $rsp["up_status"] = true;
            }
            if($info['version']=='3.0.9') {
                $m_id = cache(Request::header('token'))['id'];
                # 查询注册日期
                $m_register = Db::table('merchants')->where(array('id'=>$m_id))->value('create_time');
                if(date('Y-m-d',strtotime($m_register)) < '2022-04-15'){
                    $rsp["up_status"] = false;
                }
            }
            $rsp['version'] = $info['version'];
            $rsp["note"] = $info['note']; //release notes
            $rsp["url"] = $info['url']; //应用升级包下载地址
            $rsp["ios_url"] = $info['ios_url']; //应用升级包下载地址
            $rsp['force_update'] = $info['force_update'];
        }
        return $rsp;
    }
    /*
     * @content 判断 商户到期时间为 15 天  5 天  当天  时 发送到期时间提醒
     * @return  array（）
     * */
    function getMerchantsExpirtion(){
//        return true;
        $time = date("Y-m-d");
        $datetime = date('Y-m-d',strtotime(" + 15 day"));
        $_where = "is_type = 2 and status = 1 and level<3 and '$time'<= DATE_FORMAT(expire_time,'%Y-%m-%d') and '$datetime' >= DATE_FORMAT(expire_time,'%Y-%m-%d') ";
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants',
            'field' => '*,DATE_FORMAT(expire_time,\'%Y-%m-%d\') expire_time',
            'order' => 'id asc',
        );
        $dataList = BaseService::DataList($data_params);
        $data = MerchantsService::Expirtiondetail($dataList);
        return array('status'=>true);
    }
    /*
    * @content # 办理会员/充值/升级   24小时后自动进入余额
    * */
    static function settlementTask(){
        // 创建redislock对象
        $oRedisLock = new Redis();
        // 定义锁标识
        $key = 'settlementTaskMerchants';

        // 获取锁
        $is_lock = $oRedisLock->lock($key, 20);

        if($is_lock){
            # 办理会员/充值/升级   24小时后自动进入余额
            $balance = Db::table('biz_settlement')
                ->field('id,price,biz_id')
                ->where(array('settlement_status' => 1, 'desposit_status' => 1))
//                ->where("create_time <= '" . date('Y-m-d H:i:s', strtotime('-1 day')) . "'")
                ->where("type != 1 and mark= 2")
                ->select();
            if (!empty($balance)) {
                foreach ($balance as $k => $v) {
                    # 改状态
                    Db::table('biz_settlement')->where(array('id' => $v['id']))->update(array(
                        'desposit_status' => 2, 'settlement_status' => 2));
                    # 加余额
                    Db::table('merchants')->where(array('id' => $v['biz_id']))->setInc('balance', $v['price']);

                }
                sleep(5);
                $oRedisLock->unlock($key);
            }


            // 获取锁失败
        }else{

            return false;

        }



    }
    /*
     * @请求 删除合成语音文件
     * */
    function getDelVoice(){
        $mark = input("post.mark");
        $_redis = new Redis();
        if($mark == 'expire'){
            #到期时间  提醒 语音 删除
            $src = $_redis->hGet("voiceFile",'expireVoice');

        }else{
            #收款  提醒 语音 删除
            $src = $_redis->hGet("voiceFile",'voice');

        }
        delFile($src);
        return (array('status'=>true,'src'=>$src));

    }

    /**
     * 跳转下载地址.
     * @author   juzi
     * @blog     https://blog.csdn.net/juziaixiao
     * @version  1.0.0
     * @datetime 2020年7月10日09:35:10
     */
    public function DownLoadApk(){
        $edition = Db::table('merchants_version_control')->field('url,ios_url,id')->order("id desc")->find();
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
            #苹果跳转
            $url = $edition['ios_url'];

//            echo 'systerm is IOS';
        }else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')){
            $url = $edition['url'];

//            echo 'systerm is Android';
        }else{
            $url = $edition['url'];

//            echo 'systerm is other';
        }



//        http://back.ecarfu.com/static/merchantsapk/ios/E联商家版.ipa
        if(!empty($url)){
            $this->assign('url',$url);

            #加下载记录
//            $res = Db::table("merchants_log_download")->insert(array("create_time"=>date("Y-m-d H:i:s"),"edition_id"=>$id));

//            if($res){
                #跳转到下载地址
                return $this->fetch('/index/index');
//            }
        }
    }
}