<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/2/3
 * Time: 18:13
 */

namespace app\businessapi\controller;


use app\businessapi\ApiService\RedpacketService;
use Redis\Redis;

class Redpacket extends Common
{
    /*
    * @content ：红包领取
    * @return :array
    * */
    function getRedpacket(){
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $_redis = new Redis();

        #红包领取  存个缓存 5 秒   如果缓存还在 提示 频繁操作
        $actual_price =$_redis->hGet("Redpacket",'getRedpacket'.$this->merchants_id);
        if(empty($actual_price)){
            $_redis->hSet("Redpacket",'getRedpacket'.$this->merchants_id,$params['price'],5);
            $data = RedpacketService::getRedpacket($params);

            return array('status'=>true,'data'=>$data);

        }else{
            return array("status" => false, "msg" => '请不要频繁操作！');

        }
    }
    /*
    * @content ：获取商户的红包数量 生成红包数组 存redis
    * @return :array
    * */
    function getRedpacketNum(){
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $data = RedpacketService::getRedpacketNum($params);
        return array('status'=>true,'data'=>$data);
    }




}