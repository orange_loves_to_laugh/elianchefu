<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/20
 * Time: 15:36
 */

namespace app\businessapi\controller;

/*
 * @content 任务
 * */

use app\businessapi\ApiService\TaskService;
use app\service\BaseService;
use Redis\Redis;
use think\Db;
class Task extends Common
{
    /*
     * @content : 任务列表
     * @return : array
     * */
    function getTaskList(){
        #提前查询等级是多少
        $params['merchants_id'] = $this->merchants_id;
        TaskService::getTaskPaySucess($params,12);
        #查询任务  根据任务记录表状态 判断

        $time = date("Y-m-d H:i:s");
        $_where = "'$time' >= start_time and '$time'<= end_time";
        $data_params = array(
            'page' => false,
            'number' => 10,
            'where' => $_where,
            'table' => 'merchants_task',
            'field' => '*',
            'order' => 'id asc'
        );
        $dataList = BaseService::DataList($data_params);
        $data = TaskService::getTaskDetail($dataList,$this->merchants_id);

        return array('status'=>true,'data'=>$data);



    }
    /*
   * @content : 领取任务
   * @return : array
   * */
    function  getReceiveTask(){
        $task_id = input("post.id");

        $time = date("Y-m-d H:i:s");
        /******* 雙重判断  点击重复**********/
        #判断 当前任务id  status = 1  是否存在
        $_task_status = Db::table('merchants_task_log')->where(array('merchants_task_id'=>$task_id,'merchants_id'=>$this->merchants_id,'status'=>1))->field('id,status')->find();
        if(!empty($_task_status)){
            return array('status'=>false,'msg'=>"已经领取过了!");
        }
        $_redis = new Redis();
        #领取奖励  存个缓存 5 秒   如果缓存还在 提示 频繁操作
        $actual_price =$_redis->hGet("Task",'getReceiveTask'.$this->merchants_id);
        if(!empty($actual_price)){
            return array("status" => false, "msg" => '已经领取过了！');

        }
        $_redis->hSet("Task",'getReceiveTask'.$this->merchants_id,1,2);

        /******* 雙重判断  点击重复**********/

        #判斷当前任务 是否存在
        $_where = "'$time' <= end_time and '$time' >= start_time and id = $task_id";
        $_taskId = Db::table('merchants_task')->where($_where)->field('id,is_loop')->find();
        if(!empty($_taskId)){
            $where = array('merchants_task_id'=>$task_id,'merchants_id'=>$this->merchants_id);
            #查询 任务记录表
            $_task_log = Db::table('merchants_task_log')->where($where)->field('id,status')->order("id desc")->find();
            if(!empty($_task_log)){


                #证明领取过 判断当前是任务是否可以重复领取
                if($_taskId['is_loop'] == 1){

                    #可以重复领取(循环)
                    $data = TaskService::getReceiveTask($task_id,$this->merchants_id);
                    return array('status'=>true);
                }else{
                    #是否有已完成的数据
                    $_status = Db::table('merchants_task_log')->where(array('merchants_task_id'=>$task_id,'merchants_id'=>$this->merchants_id,'status'=>4))->field('id,status')->find();
                    if(!empty($_status)){
                        #is_receive 修改为 1  时 前端不展示

                        Db::table('merchants_task_log')->where(array('id'=>$_task_log['id']))->update(array('is_receive'=>1));
                        return array('status'=>false,'msg'=>"该任务不可重复领取");

                    }
                    $data = TaskService::getReceiveTask($task_id,$this->merchants_id);
                    return array('status'=>true);


                }
            }else{
                $data = TaskService::getReceiveTask($task_id,$this->merchants_id);
                return array('status'=>true);

            }


        }else{
            return array('status'=>false,'msg'=>"当前任务已过期!");

        }


    }

    /*
     * @content : 领取奖励
     * @return : array
     * */
    function getTaskPrize(){
        $params = $this->data_post;
        $_redis = new Redis();
        $time = date("Y-m-d");

        #领取奖励  存个缓存 5 秒   如果缓存还在 提示 频繁操作
        $actual_price =$_redis->hGet("Task",'getTaskPrize'.$this->merchants_id);
        if(empty($actual_price)){
            $_redis->hSet("Task",'getTaskPrize'.$this->merchants_id,1,2);
            #判断当前商户 当天 只能领取一次奖励

            $_where = "merchants_id = $this->merchants_id and DATE_FORMAT(reward_time,'%Y-%m-%d') = '$time' and status = 4";
            $lq_price = Db::table('merchants_task_log')->where($_where)->find();
            if(!empty($lq_price)){
                return array("status" => false, "msg" => "奖励每天只可领取一次");

            }
            #任务id   id
            $data = TaskService::getTaskPrize($params,$this->merchants_id);
            return array('status'=>true);

        }else{
            return array("status" => false, "msg" => '已经领取过了！');

        }

    }

    /*
     * @content  支付成功 任务接口
     * */
    function getTaskPaySucess(){
        $params = $this->data_post;
        $params['merchants_id'] = $this->merchants_id;
        $res = TaskService::getTaskPaySucess($params);


        return array("status" => true, "data" => $res);
    }

    /**
     * @return array|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取商家每日任务
     */
    function getsDayTask(){
        $merchants_id = $this->merchants_id;
        //return array("status"=>false,"msg"=>null);
        return TaskService::getsMerchantsDayTask($merchants_id);
    }

}