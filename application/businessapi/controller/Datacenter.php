<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/21
 * Time: 15:33
 */

namespace app\businessapi\controller;


use app\service\MerchantService;
use Redis\Redis;
use think\Db;

class Datacenter extends Common
{
    /*
     * @content : 数据中心列表  经营数据分析
     * @return : array
     * */
    function dataCenterInfo()
    {
        #查询 今日 本周   本月的 商家订单的收款金额
        $_where = "merchants_id = $this->merchants_id";

        $mark = input("post.mark");//1 本月 2 上月 4 本周
        $type = input("post.type");// type  = dataCenter  为数据中心

        if ($mark == 1) {
            #本月

            $info = self::getTodayInfo($_where, 'm',$mark);

        } else if ($mark == 2) {
            #上月
            $info = self::getTodayInfo($_where, 'last month',$mark);

        }else if ($mark == 4) {
            #本周
            $info = self::getTodayInfo($_where, 'w',$mark);
        }else if ($mark == 3) {
            #时间筛选
            $today_start = input("post.start_time");
            $today_end = input("post.end_time");

            #查询 符合的条件
            if($type == 'dataCenter'){
                #数据中心 时间筛选
                $_where .= " and DATE_FORMAT(checkout_time,'%Y-%m')>= '$today_start' and DATE_FORMAT(checkout_time,'%Y-%m')<='$today_end'";
            }
            #开始时间的上月
            $begin_time =date("Y-m",strtotime("last month",strtotime($today_start)));


            $info = self::getTodayInfo($_where,$begin_time,$mark);

        }else{
            if($type == 'dataCenter'){

                #默认查询本月
                $info = self::getTodayInfo($_where, 'm',$mark);

            }

        }


        return array('status' => true, 'data' => $info);


    }
    /*
     * @content  删除 商户缓存数据  数据中心实时更新
     * */
    function getDelRedis(){
        $_redis = new Redis();
        $_redis->hDel("merchants",'merchantsInfo'.$this->merchants_id);
        $where = "mo.merchants_id = $this->merchants_id and default_mark = 2";
        $data = Db::name('merchants_evaluation')->alias('me')->leftJoin(['merchants_order'=>'mo'],'me.merchants_order_id=mo.id')->where($where)->sum('me.evalua_level');


        return array('status'=>true,'data'=>$data);
    }
    /**
     * [CommentList 评论列表]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2020年11月12日09:49:38
     */
    public function getCommentList(){
        //接收评论id
        $where = "mo.merchants_id = $this->merchants_id and me.default_mark = 2";
        $id =input("post.id");
        if(!empty($id)){
            $where .=" and me.id = $id";
        }



        $data_params = array(
            'page'         => true,
            'number'         => 10,

            'where'     => $where,
            'field'     =>'(DATE_FORMAT(me.create_time,\'%Y-%m-%d %H:%s\')) create_time,me.evalua_level,me.evalua_comment,mo.actual_price,mo.cash_type'
        );
        $data= MerchantService::MerchantsCommentDataList($data_params);


//            $total_num = MerchantService::MerchantsCommentDataTotal($where);


        return array('status'=>true,'data'=>$data);

    }
    /*
     * @content 时间筛选 经营数据分析
    * @param $mark 1 本月 2 上月 3  时间筛选
    * @return  array
    * */
    static function getTodayInfo($_where, $day, $mark= '')
    {
        $data = [];
        if ($mark == 3) {
            #时间筛选
            #到店人数
            $data['count_member'] = Db::table('merchants_income')->where($_where)->group('member_id')->count("member_id");
            if(empty($data['count_member'])){
                $data['count_member'] = 0;
            }
            #人均到店次数 一共次数 / 人数
            $count_num = Db::table('merchants_income')->where($_where)->count("id");
            #$data['count_member'] 不能为0
            if(!empty($data['count_member'])){
                $data['store_equal_num'] = $count_num / $data['count_member'];
                $data['store_equal_num'] = strval(sprintf("%.0f", $data['store_equal_num']));

            }else{
                $data['store_equal_num'] = 0;
            }


            #最多到店次数
//            $count_member = Db::table('merchants_income m')->where($_where)
//                ->group('member_id')
//                ->field("(select count(id) from merchants_income mi where mi.member_id = m.member_id ) store_num")->order('store_num desc')->select();
            $count_member =Db::table("merchants_income m")->field("count(id) store_num")->where($_where)->group("member_id")->order('store_num desc')->select();

            $data['most_store_num'] = $count_member[0]['store_num'];
            if(empty($count_member)){
                $data['most_store_num'] = 0;
            }
            #新增到店次数 时间筛选（eg ： 1 到 5 月）人数  - 开始时间的上月 人数
            $data['new_store_num'] =   $data['count_member']-(Db::table('merchants_income')->where("DATE_FORMAT(checkout_time,'%Y-%m') = '2021-01'")->group('member_id')->count("member_id"));

            if( $data['new_store_num'] < 0){
                $data['new_store_num'] = 0;
            }


            #人均消费金额
            $count_price= Db::table('merchants_income')->where($_where)->sum("price");
            #$data['count_member'] 不能为0
            if(!empty($data['count_member'])){
                $data['equal_price'] = priceFormat(($count_price / $data['count_member']));

            }else{
                $data['equal_price'] = priceFormat(0);
            }


        } else {
            #merchants_income 表
            /*************到店人数*****************/
            $data['count_member'] = Db::table('merchants_income')->where($_where)->whereTime('checkout_time', $day)->group('member_id')->count("member_id");

            /****************人均到店次数   一共次数 / 人数***************/
            $count_num = Db::table('merchants_income')->where($_where)->whereTime('checkout_time', $day)->count("id");
            #$data['count_member'] 不能为0
            if(!empty($data['count_member'])){
                $data['store_equal_num'] = $count_num / $data['count_member'];
                $data['store_equal_num'] = strval(sprintf("%.0f", $data['store_equal_num']));


            }else{
                $data['store_equal_num'] = 0;
            }

            /*******************最多到店次数********************/
            $count_member =Db::table("merchants_income m")->field("count(id) store_num")->where($_where)->whereTime('checkout_time', $day)->group("member_id")->order('store_num desc')->select();
            $data['most_store_num'] = $count_member[0]['store_num'];
            if(empty($count_member)){
                $data['most_store_num'] = 0;
            }
            /*********************新增到店次数 本月人数  - 上月人数******************/
            if($mark == 1){
                #本月
                $data['new_store_num']  =   $data['count_member']-(Db::table('merchants_income')->where($_where)->whereTime('checkout_time', 'last month')->group('member_id')->count("member_id"));
            }else{
                #上上月
                $begin_time =  date("Y-m",mktime(0, 0 , 0,date("m")-2,1,date("Y")));

             /*   $end_time =  date("Y-m-d H:i:s",mktime(23,59,59,date("m")-1 ,0,date("Y")));*/
                $data['new_store_num'] = $data['count_member'] - (Db::table('merchants_income')->where(" DATE_FORMAT(checkout_time,'%Y-%m') = $begin_time")->group('member_id')->count("member_id"));


            }
            if( $data['new_store_num'] < 0){
                $data['new_store_num'] = 0;
            }
            /******************人均消费金额*******************/
            $count_price= Db::table('merchants_income')->where($_where)->whereTime('checkout_time', $day)->sum("price");
            #$data['count_member'] 不能为0
            if(!empty($data['count_member'])){
                $data['equal_price'] = priceFormat(($count_price / $data['count_member']));

            }else{
                $data['equal_price'] = priceFormat(0);
            }
        }

        return $data;
    }
}