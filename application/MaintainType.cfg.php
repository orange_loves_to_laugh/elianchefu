<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/4/9
 * Time: 10:09
 * context: 保养类型
 */
return array(
    array("title"=>"更换润滑油","price"=>"0.00","mileage"=>"5000","suggest_time"=>"180"),
    array("title"=>"更换防冻液","price"=>"0.00","mileage"=>"30000","suggest_time"=>"730"),
    array("title"=>"更换齿轮油","price"=>"0.00","mileage"=>"60000","suggest_time"=>""),
    array("title"=>"更换驱动桥油","price"=>"0.00","mileage"=>"25000","suggest_time"=>"180"),
    array("title"=>"更换火花塞","price"=>"0.00","mileage"=>"45000","suggest_time"=>""),
    array("title"=>"更换变速箱滤清器","price"=>"0.00","mileage"=>"60000","suggest_time"=>""),
    array("title"=>"刹车油","price"=>"0.00","mileage"=>"40000","suggest_time"=>"730"),
    array("title"=>"转向助力油","price"=>"0.00","mileage"=>"50000","suggest_time"=>"730"),
    array("title"=>"变速箱油","price"=>"0.00","mileage"=>"60000","suggest_time"=>""),
    array("title"=>"清洗节气门","price"=>"0.00","mileage"=>"20000","suggest_time"=>""),
    array("title"=>"清洗喷油嘴","price"=>"0.00","mileage"=>"20000","suggest_time"=>""),
    array("title"=>"清洗三元催化","price"=>"0.00","mileage"=>"10000","suggest_time"=>"180"),
    array("title"=>"清洗进气管道","price"=>"0.00","mileage"=>"30000","suggest_time"=>"365"),
    array("title"=>"清洗空调管道","price"=>"0.00","mileage"=>"20000","suggest_time"=>"365"),
    array("title"=>"机油滤芯","price"=>"0.00","mileage"=>"5000","suggest_time"=>"180"),
    array("title"=>"空气滤芯","price"=>"0.00","mileage"=>"10000","suggest_time"=>"180"),
    array("title"=>"空调滤芯","price"=>"0.00","mileage"=>"10000","suggest_time"=>"180"),
    array("title"=>"汽油滤芯","price"=>"0.00","mileage"=>"20000","suggest_time"=>"365"),
);

