<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/6/12
 * Time: 16:30
 */

/**
 * @param $str
 * @return string
 * @context 给字符串加引号
 */
function change_to_quotes($str)
{
    return sprintf("'%s'", $str);
}

/**
 * @param $v
 * @param $k
 * @param $kname
 * @context 导入表格使用方法 替换关联数组为索引数组
 */
function foo(&$v, $k, $kname)
{
    $v = array_combine($kname[0], array_slice($v, 0));
    $v['excel_type'] = $kname[1][0];
}

/**
 * @param $str
 * @param int $len
 * @return string
 * @content php显示指定长度的字符串，超出长度以省略号(...)填补尾部显示
 */
function cutSubstr($str, $len = 14)
{
    if (mb_strlen($str) > $len) {
        $str = mb_substr($str, 0, $len) . '...';
    }
    return $str;
}

/**
 * @param $array
 * @param $value
 * @return int|string
 * @content 根据值查询数组对应的键(当存在多个相同的值时,只会返回第一个查询到的数据)(多维数组时返回的是第一层数组的键)
 */
function searchKey($array, $value)
{
    foreach ($array as $key => $row) {
        if (!is_array($row)) {
            if ($row == $value) {
                return $key;
            }
        } else {
            $r = searchKey($row, $value);
            if ($r != -1) {
                return array_keys($array, $row)[0];
            }
        }
    }
    return -1;
}

/**
 * @return string
 * @content 序列化
 */
function iserializer($value)
{
    return serialize($value);
}

/**
 * @return array
 * @content 反序列化
 */
function iunserializer($value)
{
    if (empty($value)) {
        return array();
    }

    if (!is_serialized($value)) {
        return $value;
    }
    if (version_compare(PHP_VERSION, '7.0.0', '>=')) {

        $result = unserialize($value, array('allowed_classes' => false));

    } else {
        if (preg_match('/[oc]:[^:]*\d+:/i', $seried)) {
            return array();
        }
        $result = unserialize($value);
    }
    if ($result === false) {
        $temp = preg_replace_callback('!s:(\d+):"(.*?)";!s', function ($matchs) {
            return 's:' . strlen($matchs[2]) . ':"' . $matchs[2] . '";';
        }, $value);
        return unserialize($temp);
    } else {
        return $result;
    }
}

/**
 * @param $data 字符串
 * @param bool $strict
 * @return bool
 * @context 判断是否序列化过
 */
function is_serialized($data, $strict = true)
{
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        if (false === $semicolon && false === $brace)
            return false;
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        case 'a' :
            return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'O' :
            return false;
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}

/**
 * @return string
 * @context 获取操作端ip
 */
function getip()
{
    static $ip = '';
    $ip = $_SERVER['REMOTE_ADDR'];
    if (isset($_SERVER['HTTP_CDN_SRC_IP'])) {
        $ip = $_SERVER['HTTP_CDN_SRC_IP'];
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
        foreach ($matches[0] AS $xip) {
            if (!preg_match('#^(10|172\.16|192\.168)\.#', $xip)) {
                $ip = $xip;
                break;
            }
        }
    }
    if (preg_match('/^([0-9]{1,3}\.){3}[0-9]{1,3}$/', $ip)) {
        return $ip;
    } else {
        return '127.0.0.1';
    }
}

/**
 * [DataReturn 公共返回数据]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2016-12-07T22:03:40+0800
 * @param    [string]       $msg  [提示信息]
 * @param    [int]          $code [状态码]
 * @param    [mixed]        $data [数据]
 * @return   [json]               [json数据]
 */
function DataReturn($msg = '', $code = 0, $data = '')
{
    // 默认情况下，手动调用当前方法
    if (empty($result)) {
        $result = array('msg' => $msg, 'code' => $code, 'data' => $data);
    }

    // 错误情况下，防止提示信息为空
    if ($result['code'] != 0 && empty($result['msg'])) {
        $result['msg'] = '操作失败';
    }
    $result['status'] = $result['code'] == 0 ? true : false;

    return $result;
}

/**
 * 获取当前脚本名称
 * @version 1.0.0
 * @date    2019-06-20
 * @desc    description
 */
function CurrentScriptName()
{
    if (empty($_SERVER['SCRIPT_NAME'])) {
        if (empty($_SERVER['PHP_SELF'])) {
            if (!empty($_SERVER['SCRIPT_FILENAME'])) {

                $name = $_SERVER['SCRIPT_FILENAME'];
            }
        } else {
            $name = $_SERVER['PHP_SELF'];
        }
    } else {
        $name = $_SERVER['SCRIPT_NAME'];
    }

    if (!empty($name)) {
        $loc = strripos($name, '/');
        if ($loc != false) {
            $name = substr($name, $loc + 1);
        }
    }
    return $name;
}


/**
 * [GetNumberCode 随机数生成]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 * @param    [int] $length [生成位数]
 * @return   [int]         [生成的随机数]
 */
function GetNumberCode($length = 6)
{
    $code = '';
    for ($i = 0; $i < intval($length); $i++) $code .= rand(0, 9);
    return $code;
}


/**
 * [CheckIdCard 身份证号校验]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 * @param    [string] $string [用户名]
 * @return   [boolean]        [成功true, 失败false]
 */
function CheckIdCard($string)
{
    return (preg_match('/' . lang('common_regex_id_card') . '/', $string) == 1) ? true : false;
}


/**
 * [CheckUserName 用户名校验]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 * @param    [string] $string [用户名]
 * @return   [boolean]        [成功true, 失败false]
 */
function CheckUserName($string)
{
    return (preg_match('/' . lang('common_regex_username') . '/', $string) == 1) ? true : false;
}

/**
 * [CheckLoginPwd 密码格式校验]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 * @param    [string] $string [登录密码]
 * @return   [boolean]        [正确true, 错误false]
 */
function CheckLoginPwd($string)
{
    return (preg_match('/' . lang('common_regex_pwd') . '/', $string) == 1) ? true : false;
}

/**
 * [LoginPwdEncryption 登录密码加密]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月1日11:26:02
 * @param    [string] $pwd  [需要加密的密码]
 * @param    [string] $salt [配合密码加密的随机数]
 * @return   [string]       [加密后的密码]
 */
function LoginPwdEncryption($pwd, $salt)
{
    return md5($salt . trim($pwd));
    // return md5(trim($pwd));
}

/**
 * [ParamsChecked 参数校验方法]
 * @version  1.0.0
 * @datetime 2020年12月1日11:26:02
 * @param    [array]                   $data   [原始数据]
 * @param    [array]                   $params [校验数据]
 * @return   [boolean|string]                  [成功true, 失败 错误信息]
 */

function ParamsChecked($data, $params)
{
    if (empty($params) || !is_array($data) || !is_array($params)) {
        return '内部调用参数配置有误';
    }


    foreach ($params as $v) {
        if (!isset($v['error_code'])) {
            $v['error_code'] = 00000;
        }
        if (empty($v['key_name']) || empty($v['error_msg'])) {
            return '内部调用参数配置有误';
        }

        // 是否需要验证
        $is_checked = true;

        // 数据或字段存在则验证
        // 1 数据存在则验证
        // 2 字段存在则验证
        if (isset($v['is_checked'])) {
            if ($v['is_checked'] == 1) {
                if (empty($data[$v['key_name']])) {
                    $is_checked = false;
                }
            } else if ($v['is_checked'] == 2) {
                if (!isset($data[$v['key_name']])) {
                    $is_checked = false;
                }
            }
        }

        // 是否需要验证
        if ($is_checked === false) {
            continue;
        }

        // 数据类型,默认字符串类型
        $data_type = empty($v['data_type']) ? 'string' : $v['data_type'];

        // 验证规则，默认isset
        $checked_type = isset($v['checked_type']) ? $v['checked_type'] : 'isset';
        switch ($checked_type) {
            // 是否存在
            case 'isset' :
                if (!isset($data[$v['key_name']])) {


                    return $v['error_msg'] . ',' . $v['error_code'];

                }
                break;

            // 是否为空
            case 'empty' :
                if (empty($data[$v['key_name']])) {

                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 是否存在于验证数组中
            case 'in' :
                if (empty($v['checked_data']) || !is_array($v['checked_data'])) {
                    return '内部调用参数配置有误';
                }
                if (!isset($data[$v['key_name']]) || !in_array($data[$v['key_name']], $v['checked_data'])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 是否为数组
            case 'is_array' :
                if (!isset($data[$v['key_name']]) || !is_array($data[$v['key_name']])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 长度
            case 'length' :
                if (!isset($v['checked_data'])) {
                    return '长度规则值未定义';
                }
                if (!is_string($v['checked_data'])) {
                    return '内部调用参数配置有误';
                }
                if (!isset($data[$v['key_name']])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                if ($data_type == 'array') {
                    $length = count($data[$v['key_name']]);
                } else {
                    $length = mb_strlen($data[$v['key_name']], 'utf-8');
                }
                $rule = explode(',', $v['checked_data']);
                if (count($rule) == 1) {
                    if ($length > intval($rule[0])) {
                        return $v['error_msg'] . ',' . $v['error_code'];
                    }
                } else {
                    if ($length < intval($rule[0]) || $length > intval($rule[1])) {
                        return $v['error_msg'] . ',' . $v['error_code'];
                    }
                }
                break;
            // 密码长度
            case 'pwdlength' :
                if (!isset($v['checked_data'])) {
                    return '长度规则值未定义';
                }
                if (!is_string($v['checked_data'])) {
                    return '内部调用参数配置有误';
                }
                if (!isset($data[$v['key_name']])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                if ($data_type == 'array') {
                    $length = count($data[$v['key_name']]);
                } else {
                    $length = mb_strlen($data[$v['key_name']], 'utf-8');
                }
                $rule = explode(',', $v['checked_data']);
                if (count($rule) == 1) {
                    #密码长度<规定长度 提示
                    if ($length < intval($rule[0])) {
                        return $v['error_msg'] . ',' . $v['error_code'];
                    }
                } else {
                    if ($length < intval($rule[0]) || $length > intval($rule[1])) {
                        return $v['error_msg'] . ',' . $v['error_code'];
                    }
                }
                break;
            // 自定义函数
            case 'fun' :

                if (empty($v['checked_data']) || !function_exists($v['checked_data'])) {
                    return '验证函数为空或函数未定义';
                }
                $fun = $v['checked_data'];
                if (!isset($data[$v['key_name']]) || !$fun($data[$v['key_name']])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 最小
            case 'min' :
                if (!isset($v['checked_data'])) {
                    return '验证最小值未定义';
                }

                if (!isset($data[$v['key_name']]) || $data[$v['key_name']] <= $v['checked_data']) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 最大
            case 'max' :
                if (!isset($v['checked_data'])) {
                    return '验证最大值未定义';
                }

                if (!isset($data[$v['key_name']]) || $data[$v['key_name']] >= $v['checked_data']) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 相等
            case 'eq' :
                if (!isset($v['checked_data'])) {
                    return '验证相等未定义';
                }
                if (!isset($data[$v['key_name']]) || $data[$v['key_name']] == $v['checked_data']) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;

            // 数据库唯一
            case 'unique' :
                if (!isset($v['checked_data'])) {
                    return '验证唯一表参数未定义';
                }
                if (empty($data[$v['key_name']])) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                $temp = \think\Db::name($v['checked_data'])->where([$v['key_name'] => $data[$v['key_name']]])->find();
                // $temp = db($v['checked_data'])->where([$v['key_name']=>$data[$v['key_name']]])->find();
                if (!empty($temp)) {
                    return $v['error_msg'] . ',' . $v['error_code'];
                }
                break;
        }
    }

    return true;
}

/**
 * [CheckMobile 手机号码格式校验]
 * @version  0.0.1
 * @datetime 2020年4月23日15:29:53
 * @param    [int] $mobile [手机号码]
 * @return   [boolean]     [正确true，失败false]
 */
function CheckMobile($mobile)
{

    return (preg_match('/' . lang('common_regex_mobile') . '/', $mobile) == 1) ? true : false;
}


/**
 * @access function
 * @param string $address 地址
 * @param string $city 城市
 * @return array
 * @context 根据地址获取经纬度
 */
function getLonLat($address, $city = null)
{
    $_receipt = null;
    $url = "https://restapi.amap.com/v3/geocode/geo?key=3f5396b83b7b09cfb811027943f8d7c7&address=" . trim($address) . "&city=" . trim($city);
    $_curl = new \Protocol\Curl();
    $_receipt = $_curl->get($url);
    return json_decode($_receipt, true);
}

/**
 * @access public
 * @param $lng 经度
 * @param $lat 纬度
 * @return 解析后详情 为json格式
 * @context 逆地址解析
 */
function getAddressResolution($lng, $lat)
{
    $url = "https://restapi.amap.com/v3/geocode/regeo?key=3f5396b83b7b09cfb811027943f8d7c7&location=$lng,$lat&output=json";
    $_curl = new \Protocol\Curl();
    $result = $_curl->get($url);
    return $result;
}

/**
 * @param $price
 * @param $mark true 显示1位小数  false 显示两位小数
 * @return bool|null|string
 * @context 价格格式化 整数保留1位，2小数末位是0，显示保留1位
 */
function priceFormat($price, $mark = false)
{

    if (!empty($price)) {
        if ($mark) {
            $price = strval(sprintf("%.2f", $price));
            $end = substr($price, -1);
            if ($end == 0) {
                $price = substr($price, 0, (strlen($price) - 1));
            }
        } else {
            $price = sprintf("%.2f", $price);
        }
        return $price;
    } else {

        return '0.00';
    }
}

/**
 * @access public
 * @return null
 * @context  随机生成6位短信验证码
 */
function randCodes($length = 6)
{
    #验证码长度

    #随机验证码
    $num = str_shuffle(rand(pow(10, ($length - 1)), pow(10, $length) - 1));
    return $num;
}

/**
 * Convert a string to an array
 * @param string $str
 * @param number $split_length
 * @return multitype:string
 * @context 字符串无分隔符拆分成数组
 */
function mb_str_splits($str, $split_length = 1, $charset = "UTF-8")
{
    if (func_num_args() == 1) {
        return preg_split('/(?<!^)(?!$)/u', $str);
    }
    if ($split_length < 1) return false;
    $len = mb_strlen($str, $charset);
    $arr = array();
    for ($i = 0; $i < $len; $i += $split_length) {
        $s = mb_substr($str, $i, $split_length, $charset);
        $arr[] = $s;
    }
    return $arr;
}

/**
 * @return float
 * @content 返回当前的毫秒时间戳
 */
function getMsectime()
{
    list($msec, $sec) = explode(' ', microtime());
    $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    return $msectime;
}


/**
 * @param $content
 * @return null
 *
 */
/**
 * [get_images_from_html 从HTML文本中提取所有图片]
 * @version  1.0.0
 * @datetime 2017-12-12T15:26:13+0800
 * @param    [array]                   $content   [富文本内容]
 * @return   [boolean|string]                  [成功返回字符串, 失败 返回null]
 */
function get_images_from_html($content)
{
    $pattern = "/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
    preg_match_all($pattern, htmlspecialchars_decode($content), $match);
    if (!empty($match[1])) {
        return $match[1];
    }
    return null;
}


/**
 * 路径解析指定参数
 * @author  juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年9月30日13:21:27
 * @param   [string]            $key        [指定key]
 * @param   [mixed]             $default    [默认值]
 * @param   [string]            $path       [参数字符串 格式如： a/aa/b/bb/c/cc ]
 */
function PathToParams($key = null, $default = null, $path = '')
{
    $data = $_REQUEST;
    if (empty($path) && isset($_REQUEST['s'])) {
        $path = $_REQUEST['s'];
    }
    if (!empty($path) && !array_key_exists($key, $data)) {
        if (substr($path, 0, 1) == '/') {
            $path = mb_substr($path, 1, mb_strlen($path, 'utf-8') - 1, 'utf-8');
        }
        $position = strrpos($path, '.');
        if ($position !== false) {
            $path = mb_substr($path, 0, $position, 'utf-8');
        }
        $arr = explode('/', $path);


        $index = 0;
        foreach ($arr as $k => $v) {
            if ($index != $k) {
                $data[$arr[$index]] = $v;
                $index = $k;
            }
        }
    }

    if ($key !== null) {
        return array_key_exists($key, $data) ? $data[$key] : $default;
    }
    return $data;

}


/**
 * 获取当前系统所在根路径
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年9月30日13:30:58
 * @desc    description
 */
function GetDocumentRoot()
{
    // 当前所在的文档根目录
    if (!empty($_SERVER['DOCUMENT_ROOT'])) {
        return $_SERVER['DOCUMENT_ROOT'];
    }

    // 处理iis服务器DOCUMENT_ROOT路径为空
    if (!empty($_SERVER['PHP_SELF'])) {
        // 当前执行程序的绝对路径及文件名
        if (!empty($_SERVER['SCRIPT_FILENAME'])) {
            return str_replace('\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0 - strlen($_SERVER['PHP_SELF'])));
        }

        // 当前所在绝对路径
        if (!empty($_SERVER['PATH_TRANSLATED'])) {
            return str_replace('\\', '/', substr(str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']), 0, 0 - strlen($_SERVER['PHP_SELF'])));
        }
    }

    // 服务器root没有获取到默认使用系统root_path
    return (substr(ROOT_PATH, -1) == '/') ? substr(ROOT_PATH, 0, -1) : ROOT_PATH;
}

/**
 * 根据数量输出对应的空格
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年9月30日13:30:58
 * @desc    description
 */
function GetSpaceByCount($count)
{
    $space = '';
    for ($i = 0; $i < $count; $i++) {
        $space .= '&nbsp;';
    }
    return $space;
}

/**
 * [GetEmployeeCheck 查询员工完成考核状态]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月26日15:24:00
 * @param    [int]  $employee_check_relate_id  [employee_check_relate数据表id]
 * @param    [string]  key  [字段名]
 */
function GetEmployeeCheck($employee_check_relate_id, $key = 'null')
{
    $employee_check_relate_info = \think\Db::name('employee_check_relate')
        ->alias('ecr')
        ->leftJoin(['employee_check' => 'ec'], 'ecr.employee_check_id')
        ->field('ecr.*,ec.check_time,ec.employee_id')
        ->where('ecr.id=' . $employee_check_relate_id)->find();
    //1销售额
    $employee_id = $employee_check_relate_info['employee_id'];
    $check_time = $employee_check_relate_info['check_time'];
    $todo_num = $employee_check_relate_info['todo_num'];
    $employee_check_relate_info['status'] = '未完成';
    switch ($employee_check_relate_info['type']) {
        case 1://销售额考核
            $where = [
                ['os.employee_id', 'REGEXP', "($employee_id,)|(,$employee_id)|$employee_id"],
                ['order_create', '>=', $check_time],
                ['order_create', '<=', date('Y-m-d 23:59:00', strtotime($check_time . '+1month -1day'))],
            ];
            $sell_info = \think\Db::name('order_server')
                ->alias('os')
                ->leftJoin(['orders' => 'o'], 'os.order_number=o.order_number')
                ->where($where)
                ->sum('pay_price');
            //完成状态
            if ($sell_info >= $todo_num) {
                $employee_check_relate_info['status'] = '完成';
            }
            //所处阶段

            break;
        case 2://2办卡额 3办卡数量 4 到店率5 好评率
            $employee_check_relate_info['status'] = '完成';
            break;
    }
    if (empty($key)) {
        return $employee_check_relate_info;
    }
    return $employee_check_relate_info[$key];
    // return $employee_check_relate_id;
}

/**
 * [arraySort 查询员工完成考核状态]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月26日15:24:00
 * @param    [arr]  $array  [排序数组]
 * @param    [string]  key  [指定键]
 * @param    [string]  sort  [字段名]
 */
function arraySort($array, $keys, $sort = 'asc')
{
    $newArr = $valArr = array();
    foreach ($array as $key => $value) {
        $valArr[$key] = $value[$keys];
    }
    ($sort == 'asc') ? asort($valArr) : arsort($valArr);
    reset($valArr);
    foreach ($valArr as $key => $value) {
        $newArr[$key] = $array[$key];
    }
    return $newArr;
}

/**
 * [CheckBase64 校验是否为base64编码格式]
 * @author   juzi
 * @blog     https://blog.csdn.net/juziaixiao
 * @version  0.0.1
 * @datetime 2020年12月26日15:24:00
 * @param    [string]  $str  [待校验字符串]
 */
function CheckBase64($str)
{
    if (!str_is_base64($str)) {
        return $str;
    }
    if ($str == base64_encode(base64_decode($str))) {
        return base64_decode($str);
    } else {
        return $str;
    }
}


/**
 * 判断否为UTF-8编码
 *
 * @param $str
 * @return bool
 */
function is_utf8($str)
{
    $len = strlen($str);
    for ($i = 0; $i < $len; $i++) {
        $c = ord($str[$i]);
        if ($c > 128) {
            if (($c > 247)) {
                return false;
            } elseif ($c > 239) {
                $bytes = 4;
            } elseif ($c > 223) {
                $bytes = 3;
            } elseif ($c > 191) {
                $bytes = 2;
            } else {
                return false;
            }
            if (($i + $bytes) > $len) {
                return false;
            }
            while ($bytes > 1) {
                $i++;
                $b = ord($str[$i]);
                if ($b < 128 || $b > 191) {
                    return false;
                }
                $bytes--;
            }
        }
    }
    return true;
}


/**
 * 判断是否用base64进行encode过
 *
 * @param $str
 * @return bool
 */
function str_is_base64($str)
{
    if (is_utf8(base64_decode($str)) && base64_decode($str) != '') {
        return true;
    }
    return false;
}

/**
 * 根据经纬度获取地理位置
 * @author   juzi
 * @blog    https://blog.csdn.net/juziaixiao
 * @version 1.0.0
 * @date    2020年9月3日11:07:51
 * @desc    description
 * @param   [float]         $longitude     [精度]
 * @param   [int]           $latitude  [维度]
 * @param   [string]        $pois
 */
function getAddressComponent($longitude, $latitude, $pois = 1)
{

//    $ip = '122.68.33.168';
//    $ak=Db::name('payapi')->where('id=4')->value('appsecrt');
//    $url = file_get_contents("http://api.map.baidu.com/location/ip?ip=$ip&ak=$ak&coor=bd09ll");
//    $res1 = json_decode($url,true);
//    $res2=getAddressComponent($res1['content']['point']['x'],$res1['content']['point']['y']);
//    dump($res2);exit;
    $ak = \think\Db::name('sysset')->where(['tagname' => 'map_ak'])->value('desc');;

    $param = array(
        'ak' => $ak,
        'location' => implode(',', array($latitude, $longitude)),
        'pois' => $pois,
        'output' => 'json'
    );

    // 请求百度api
    $response = (new \Protocol\Curl())->get('http://api.map.baidu.com/geocoder/v2/', $param);
    //toCurl('http://api.map.baidu.com/geocoder/v2/', $param);

    $result = array();

    if ($response) {
        $result = json_decode($response, true);
    }
    return $result;
}




