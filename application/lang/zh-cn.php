<?php
return array(
    //用户列表
    'excel_member_data' => [

        ['name' => '用户姓名', 'field' => 'member_name'],
        ['name' => '用户手机号', 'field' => 'member_phone'],
        //['name'=>'会员等级','field'=>'member_level_title'],
        //['name'=>'用户余额','field'=>'member_balance'],
        ['name' => '注册时间', 'field' => 'register_time'],
    ],
    //提现审核申请
    'excel_member_list' => [
        ['name' => '用户id', 'field' => 'id'],
        ['name' => '用户姓名', 'field' => 'name'],
        ['name' => '用户手机号', 'field' => 'phone'],
    ],

    //提现审核申请
    'excel_action_deposit_list' => [
        ['name' => '提现人名称', 'field' => 'username'],
        ['name' => '提现人手机号', 'field' => 'phone'],
        ['name' => '提现来源', 'field' => 'type_title'],
        ['name' => '提现类型', 'field' => 'launch_cate_title'],
        ['name' => '提现人状态', 'field' => 'type_status'],
        ['name' => '门店所属地区', 'field' => 'total_area'],
        ['name' => '提现金额', 'field' => 'price'],
        ['name' => '收款人名称', 'field' => 'account_name'],
        ['name' => '收款人账号', 'field' => 'account_code'],
    ],
    //门店详情导出数据
    'excel_action_dispath_ordergoods_list' => [
        ['name' => '门店名称', 'field' => 'biz_title'],
        ['name' => '门店电话', 'field' => 'biz_phone'],
        ['name' => '门店电话', 'field' => 'biz_address'],
        ['name' => '车辆品牌', 'field' => 'brand_title'],
        ['name' => '车辆型号', 'field' => 'brandmodel_title'],
        ['name' => '识别代码', 'field' => 'identification'],
        ['name' => '配件名称', 'field' => 'title'],
        ['name' => '需求数量', 'field' => 'send_num'],
        ['name' => '备注信息', 'field' => 'remarks'],
        ['name' => '进货价格', 'field' => 'purchase_price'],
        ['name' => '建议售价', 'field' => 'selling_price'],
        ['name' => '结算价格', 'field' => 'checkingout_price'],
        ['name' => '状态', 'field' => 'read_status'],
    ],
    //到期商户导出数据
    'excel_action_merchants_expire_list' => [
        ['name' => '商户名称', 'field' => 'title'],
        ['name' => '分类', 'field' => 'cate_title'],
        ['name' => '联系电话', 'field' => 'contacts_phone'],
        ['name' => '地区', 'field' => 'total_area'],
        ['name' => '业务员', 'field' => 'relate_employee_title'],
        ['name' => '到期时间', 'field' => 'expire_time'],
        ['name' => '剩余天数', 'field' => 'expire_day'],
        ['name' => '商家类型', 'field' => 'is_type_title'],
        ['name' => '商家状态', 'field' => 'level_title'],

    ],

    //到期商户导出数据
    'excel_action_merchants_materiel_list' => [
        ['name' => '收货人名称', 'field' => 'name'],
        ['name' => '收货人手机号', 'field' => 'phone'],
        ['name' => '收货人地址', 'field' => 'materiel_address'],
        ['name' => '状态', 'field' => 'is_status_title'],
    ],

    //到期商户导出数据
    'excel_action_creation_list' => [
        ['name' => '门店名称', 'field' => 'bizTitle'],
        ['name' => '门店类型', 'field' => 'bizType'],
        ['name' => '门店区域', 'field' => 'bizArea'],
        ['name' => '门店电话', 'field' => 'bizPhone'],
        ['name' => '门店关联员工', 'field' => 'employeeName'],
        ['name' => '服务/商品', 'field' => 'type'],
        ['name' => '服务/商品-名称', 'field' => 'title'],
        ['name' => '一级分类', 'field' => 'class_first'],
        ['name' => '二级分类', 'field' => 'class_second'],
        ['name' => '关键词', 'field' => 'service_keyborder'],
        ['name' => '轮播声明文字', 'field' => 'service_statement'],
        ['name' => '服务时长', 'field' => 'service_time'],
        ['name' => '门店价格', 'field' => 'biz_price'],
        ['name' => '平台价格', 'field' => 'online_price'],
        ['name' => '是否打折', 'field' => 'is_discount'],
        ['name' => '折扣类型', 'field' => 'discount_type'],
        ['name' => '是否展示给用户', 'field' => 'is_display'],
        ['name' => '关联保养记录', 'field' => 'is_maintain'],
        ['name' => '关联维修记录', 'field' => 'is_repair'],
        ['name' => '关联质保记录', 'field' => 'is_warranty'],
        ['name' => '缩略图', 'field' => 'service_image'],
        ['name' => '主图', 'field' => 'service_picurl'],
        ['name' => '详情图', 'field' => 'service_context'],
        ['name' => '审核类型', 'field' => 'mark'],

    ],
    //商家审核
    'excel_action_merchants_list' => [
        ['name' => '商户名称', 'field' => 'title'],
        ['name' => '商户电话', 'field' => 'tel'],
        ['name' => '商户分类', 'field' => 'cate_title'],
        ['name' => '联系人', 'field' => 'contacts'],
        ['name' => '联系电话', 'field' => 'contacts_phone'],
        ['name' => '地区', 'field' => 'total_area'],
        ['name' => '详细地址', 'field' => 'address'],
        ['name' => '是否赠送积分', 'field' => 'giving_score_title'],
        ['name' => '商家抽成', 'field' => 'commission_bi'],
    ],
    //门店审核
    'excel_action_biz_list' => [
        ['name' => '门店名称', 'field' => 'biz_title'],
        ['name' => '门店电话', 'field' => 'service_tel'],
        ['name' => '联系人', 'field' => 'biz_leader'],
        ['name' => '联系电话', 'field' => 'biz_phone'],
        ['name' => '省', 'field' => 'province'],
        ['name' => '市', 'field' => 'city'],
        ['name' => '区', 'field' => 'area'],
        ['name' => '详细地址', 'field' => 'biz_address'],
        ['name' => '商家抽成', 'field' => 'proportion'],
    ]

    //button样式列表
, 'button_list' => ['btn-outline-primary', 'btn-outline-secondary', 'btn-outline-success', 'btn-outline-danger', 'btn-outline-warning', 'btn-outline-info', 'btn-outline-light', 'btn-outline-juzi1', 'btn-outline-juzi2']
    //是否状态判断
, 'yes_no_status' => [1 => '是', 2 => '否']
    //开启状态判断
, 'open_close_status' => [1 => '开启', 2 => '关闭', 3 => '申请中']
    //费用类型
, 'fee_type' => [1 => '收入', 2 => '支出']
    //支付&提现方式
, 'cash_type' => [
        1 => '微信支付',
        2 => '支付宝支付',
        3 => '银联支付',
        4 => '现金支付',
        5 => '余额支付',
        6 => '卡券核销',
        7 => '积分兑换'
    ]
, 'cash_type_total_info' => [
        ['name' => '微信支付', 'value' => 1, 'en_name' => 'wx'],
        ['name' => '支付宝支付', 'value' => 2, 'en_name' => 'ali'],
        ['name' => '银联支付', 'value' => 3, 'en_name' => 'unionpay'],
        ['name' => '现金支付', 'value' => 4, 'en_name' => 'cash'],
        ['name' => '余额支付', 'value' => 5, 'en_name' => 'balance'],
        ['name' => '卡券核销', 'value' => 6, 'en_name' => 'voucher'],
    ]
    //提现状态
, 'deposit_status' => [1 => '等待审核', 2 => '提现中', 3 => '成功', 4 => '失败', 5 => '驳回']
    //提现类型
, 'launch_cate' => [1 => '直营店', 2 => '加盟店', 3 => '合作店', 4 => '保证金退回', 5 => '商户提现', '7' => '合作店下架结算', '8' => '商家下家结算']
    //成为会员渠道
, 'member_source_type' => [1 => '门店推荐', 2 => '会员推荐', 3 => '推广员推荐', 4 => '员工推荐', 5 => '用户推荐',
        6 => '商家推荐', 7 => '自助办理']
    //用户费用类型
, 'member_fee_aim' => [1 => '服务收入', 2 => '会员办理', 3 => '商品销售', 4 => '活动收入',
        5 => '生活超市', 6 => '升级会员', 7 => '会员充值', 8 => '推荐会员提成',
        9 => '推荐推广员提成', 10 => '会员消费提成', 11 => '推广员消费提成',
        12 => '积分兑换收入', 13 => '申请推广员', 14 => '签到奖励', 15 => '预约退款',
        16 => '任务奖励', 17 => '平台赠送', 18 => '平台返现', 19 => '保险询价', 20 => '商户消费', 21 => '物业缴费',22=>'充值赠送']
    //门店收入类型
, 'biz_income_type' => [1 => '服务收入', 2 => '会员办理', 3 => '商品销售', 4 => '活动收入',
        5 => '生活超市', 6 => '升级会员', 7 => '会员充值', 8 => '推荐会员提成',
        9 => '推荐推广员提成', 10 => '会员消费提成', 11 => '推广员消费提成',
        12 => '合作店抽成', 13 => '商家抽成', 14 => '商家推广费', 15 => '配件收入',
        16 => '耗材收入', 17 => '平台收入', 18 => '物业费抽成']

    //卡卷来源
, 'voucher_source' => [1 => '购买服务', 2 => '活动获得', 3 => '加盟店赠送', 4 => '现金红包', 5 => '线上会员办理', 6 => '线下会员办理', 7 => '线上升级', 8 => '线下升级', 9 => '线上充值', 10 => '线下充值', 11 => '商家线上赠送', -2 => '活动赠送', -1 => '服务赠送', 12 => '平台赠送', 13 => '体验卡激活获得', 14 => '积分兑换', 15 => '联盟商家', 16 => '合伙人', 17 => '签到', 18 => '大转轮', 19 => '任务']
    //卡卷类型
, 'voucher_type' => [1 => '服务卡券', 2 => '现金抵制卡券', 3 => '商品卡券',]
    //车型
, 'car_level' => [1 => '小型车', 2 => '中型车', 3 => '大型车',]

    //读取状态
, 'read_status' => [1 => '未读', 2 => '已读',]
    //发送状态
, 'send_status' => [1 => '已发送', 2 => '待发送',]
    //用户性别
, 'member_sex' => [0 => '女', 1 => '男', 2 => '未知',]
    //用户等级
, 'member_level' => [1 => '120天初级体验卡', 2 => '300天体验', 3 => '银色Vip', 4 => '黄金Vip', 5 => '白金Vip', 6 => '如意Vip', 7 => '贵宾Vip', 8 => '钻石Vip', 9 => '至尊Vip']
    //用户账户状态
, 'account_status' => [1 => '正常', 2 => '停用']
    //合伙人状态判断
, 'member_type' => [1 => '普通人', 2 => '推广员',]
    //商户分类类型
, 'merchants_cate_type' => [2 => '联盟商家', 1 => '服务商家',]
    //商户等级
, 'merchants_level' => [1 => '优质商家', 2 => '推荐商家', 3 => '诚信商家', 4 => '普通商家']
    //用户在商户消费支付方式
, 'merchants_pay_type' => [1 => '线上预约', 2 => '用户付款', 3 => '扫码支付', 4 => '商家扫码']
, 'pay_type' => [1 => '微信', 2 => '支付宝', 3 => '银联', 4 => '现金', 5 => '余额', 6 => '卡券', 7 => '积分', 8 => '平台奖励', 9 => '任务奖励', 10 => '用户推荐收入', 11 => '用户充值提成', 12 => '用户升级提成']
    //商户结算状态
, 'merchants_checkout_status' => [1 => '待结算', 2 => '已结算',]
    //商户订单状态
, 'merchants_order_status' => [1 => '待核销', 2 => '已核销',]
    //上下架状态
, 'sell_status' => [1 => '未上架', 2 => '已下架', 3 => '展示中']
    //推荐合伙人奖励类型
, 'partner_prise_type' => [1 => '卡卷', 2 => '红包']
    //门店类型
, 'biz_type' => [1 => '直营店', 2 => '加盟店', 3 => '合作店',]
    //通知接收端
, 'accepter_type_notice' => [1 => '员工端', 2 => '合作端', 3 => '直营店', 4 => '加盟店', 5 => '用户端', 6 => '联盟商家', 7 => '评估端口']
    // 消息接收方
, 'accepter_type_news' => [1 => '合作门店', 2 => '所有用户', 3 => '所有员工', 4 => '联盟商家',]
    //app类型
, 'app_client' => [1 => '用户端', 2 => '商家端', 3 => '合作端']
    //推荐服务显示位置
, 'recommond_serviced_position' => [1 => '位置1', 2 => '位置2', 3 => '位置3',]
    //评论级别
, 'comment_level' => [1 => '差评', 2 => '差评', 3 => '一般', 4 => '一般', 5 => '好评', 0 => '未评论']
    //分享类型 integral_set 积分呢设置表
, 'integral_type' => [
        'register_integral' => '首次注册',
        'is_app' => '下载 app ',
        'is_realname' => '实名认证',
        'is_public_number' => '关注公众号',
        'share_register_integra' => '分享服务奖励',
        'share_busines_integral' => '分享商家',
        'is_user_register' => '推荐注册',
        'is_share_club' => '分享俱乐部',
        'is_service_comment' => '服务评价',
        'is_forward' => '转发评论',
        'share_car_integral' => '分享二手车',
        'is_share_order' => '分享订单',
    ],
    //图片类型
    'imge_type' => ['rcardimg' => '推荐用户底图', 'fstoreimg' => '加盟店底图', 'cstoreimg' => '合作店底图', 'businessimg' => '商家底图', 'partnerimg' => '推广员底图', 'loading' => '加载页管理', 'proadvert' => '商品详情页广告管理', 'indexpopup' => '首页弹窗管理', 'indexadvert' => '首页广告管理', 'mineadvert' => '我的页广告管理', 'mineadvert_bottom' => '我的页底部广告管理', 'mineadvert_center' => '我的页中间广告管理']
    //应用banner类型
, 'application_banner_type' => [
        'banner' => '首页banner',
        'protocol' => '协议管理',
        'cardbanner' => '会员页banner',
        'carbanner' => '二手车banner',
        'rcardimg' => '推荐会员底图管理',
        'fstoreimg' => '加盟点底图',
        'cstoreimg' => '合作店底图',
        'businessimg' => '商家底图',
        'partnerimg' => '推广员底图',
        'loading' => '加载页管理',
        'proadvert' => '商品详情页广告',
        'indexpopup' => '首页弹窗',
        'indexadvert' => '首页广告',
        'mineadvert_center' => '我的页中间广告',
        'mineadvert_bottom' => '我的页底部广告',
        'aboutmine' => '关于我们',
        'statement' => '声明管理',
        'clubbanner' => '俱乐部banner',
        'locallifebanner' => '本地生活banner',
    ]
,
    //商户任务类型
    'task_type' => [1 => '储值用户数量', 2 => '储值用户金额', 3 => '销售额', 4 => '好评数量', 5 => '收款次数', 6 => '预约次数', 7 => '.查看人数', 8 => '.点赞人数', 9 => '成为诚信商家', 10 => '成为优质商家', 11 => '成为推荐商家', 12 => '热门等级', 13 => '上架优惠券', 14 => '增加员工']
    //图片跳转类型
, 'image_jump_type' => [1 => '服务', 2 => '活动', 3 => '用户', 4 => '二手车', 5 => '联盟商家', 11 => '本地生活', 6 => '招募', 9 => '外部链接', 10 => '积分列表',12=>'办理会员']
, 'image_jump_type_title' => [
        1 => ['table' => 'service', 'title' => '服务详情', 'field' => 'service_title title'],
        2 => ['table' => 'active', 'title' => '活动详情', 'field' => 'active_title title'],
        3 => ['table' => 'member_level', 'title' => '用户体验卡详情', 'field' => 'level_title title'],
        4 => ['table' => 'active', 'title' => '二手车详情', 'field' => 'active_title title'],
        5 => ['table' => 'merchants', 'title' => '联盟商家详情', 'field' => ' title'],
        6 => ['table' => 'recruitment', 'title' => '招募详情', 'field' => 'title'],
        11 => ['table' => 'merchants', 'title' => '本地生活详情', 'field' => 'title'],
    ]
    //员工状态
, 'employee_status' => [1 => '试用期', 2 => '正式员工', 0 => '离职']
    //员工婚姻状况
, 'employee_marriage_status' => [1 => '已婚', 2 => '未婚', 3 => '离异', 4 => '丧偶', 5 => '未选择']
    //员工学历
, 'employee_education' => [1 => '小学', 2 => '初中', 3 => '高中', 4 => '中专', 5 => '大专', 6 => '本科', 7 => '本科以上']
    //员工考核类型
, 'check_type' => [1 => '关联销售额考核', 2 => '关联办卡额考核', 3 => '关联办卡数量考核', 4 => '关联到店率考核', 5 => '关联好评率考核', 6 => '平台抽成金额']

    //用户被呼叫次数
, 'calling_time' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => '6次以上']
    //用户参加游戏状态
, 'play_game_status' => [1 => '已参与', 2 => '进行中', 3 => '已成功', 4 => '已领取', 5 => '未参与',]
    //会员账户余额
, 'member_balance' => [1 => '1-100', 2 => '100-200', 3 => '200-300', 4 => '300-700',
        5 => '500-700', 6 => '700-1000', 7 => '1000-1500', 8 => '1500-2000',
        9 => '2000-3000', 10 => '3000-4000', 11 => '4000-5000', 12 => '5000以上']

    //用户累计消费金额
, 'cumulative_amount' => [1 => '1-50', 2 => '50-100', 3 => '100-300', 4 => '300-500',
        5 => '500-1000', 6 => '1000-2000', 7 => '2000-3000', 8 => '3000以上',]

    //商户员工岗位
, 'merchants_staff_power' => [1 => '提现功能', 2 => '查看订单', 3 => '查看数据', 4 => '修改信息', 5 => '更改时间', 6 => '付费推广', 7 => '发放优惠', 8 => '退回保障', 9 => '密码修改']
    //搜索条件
, 'search_field' => [
//                                            1=>['title'=>'keywords','name'=>'关键词'],
        2 => ['title' => 'register_time', 'name' => '注册时间'],
        3 => ['title' => 'member_create', 'name' => '办理时间'],
        4 => ['title' => 'member_level', 'name' => '用户等级'],
        5 => ['title' => 'member_sex', 'name' => '用户性别'],
        6 => ['title' => 'calling_time', 'name' => '呼叫次数'],
        7 => ['title' => 'experience_card_status', 'name' => '体验卡激活'],
        8 => ['title' => 'member_balance', 'name' => '用户余额'],
        9 => ['title' => 'position', 'name' => '外呼坐席'],
        10 => ['title' => 'cumulative_amount', 'name' => '累计消费金额'],
        11 => ['title' => 'insurance_status', 'name' => '是否购买保险'],
        12 => ['title' => 'play_game_status', 'name' => '是否参加游戏'],
    ]




    // 正则
    // 用户名
, 'common_regex_username' => '^[A-Za-z0-9_]{2,18}$'
    // 用户名
, 'common_regex_pwd' => '^.{6,18}$'
    // 手机号码
, 'common_regex_mobile' => '^1((3|4|5|6|7|8|9){1}\d{1})\d{8}$'
    // 身份证号码
, 'common_regex_id_card' => '^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$',
    "subsidy_type" => [1 => "后台赠送余额", 2 => "会员办理赠送余额", 3 => "升级用户赠送余额", 4 => "用户充值赠送余额", 5 => "参加活动赠送余额", 6 => "签到赠送余额", 7 => "任务赠送余额",
        8 => "摇奖赠送余额", 9 => "积分兑换余额", 10 => "投保赠送余额", 11 => "服务差价补贴", 12 => "卡券核销或抵用", 13 => "合作店完成任务奖励", 14 => "商家完成任务奖励",
        15 => "商家红包奖励", 16 => "合作端提现",17=>"合作店每日任务奖励"],
    "integral_source_type" => [
        0 => "额外赠送", 1 => "首次登陆", 2 => "分享注册", 3 => "线上购买服务", 4 => "评论订单", 5 => "积分兑换", 6 => "线上会员办理", 7 => "关注公众号",
        8 => "分享文章", 9 => "注册赠送", 10 => "线上升级用户", 11 => "线上充值", 12 => "门店购买商品额外赠送", 13 => "活动赠送", 14 => "查看文章",
        15 => "分享服务", 16 => "分享商家", 17 => "分享二手车", 18 => "分享用户", 19 => "推荐会员办理", 20 => "推荐用户充值",
        21 => "推荐用户升级", 22 => "下载APP", 23 => "成为推广员", 24 => "签到奖励", 25 => "商家推广", 26 => "分享联盟商家", 27 => "商家支付", 28 => "服务消费",
        34=>"看视频奖励",
        -12 => "线上购买商品额外赠送", -11 => "线下充值", -10 => "线下升级用户", -6 => "线下会员办理", -3 => "门店购买服务", -2 => "评论分享", -1 => "实名认证",
    ],
    'duration' => [3 => "季度", 6 => "半年", 12 => "年度", 24 => "两年"],
);
