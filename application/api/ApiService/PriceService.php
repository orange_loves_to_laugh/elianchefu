<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/26
 * Time: 13:55
 */

namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class PriceService
{
    /**
     * @param $service_id
     * @return array|bool|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务价格及会员折扣
     */
    function servicePrice($service_id, $biz_id = 0, $mark = true)
    {
        $_redis = new Redis();
        if ($mark)
            $_redis->hDel("servicePrice", $service_id);
        $price = $_redis->hGetJson("servicePrice", $service_id);
        if (empty($price)) {
            $price = array();
            $service_discount = Db::table("service_car_mediscount")->where(array("service_id" => $service_id, "biz_id" => $biz_id))->select();
            foreach ($service_discount as $k => $v) {
                if ($v['service_type'] == 1) {
                    $price['online'][$v['car_level_id']] = $v['discount'];
                } else {
                    $price['offline'][$v['car_level_id']] = $v['discount'];
                }
            }
            # 查询该服务的服务折扣类型
            // 平台服务  余额支付打折  不是余额支付不打折 -  去掉平台价格
            // 合作端上架服务商品  设置折扣(所有会员折扣一样=>0.9)   线上下单打折(不分是不是会员)  门店下单不打折
            $service = Db::table("service")->field("discount_type,biz_pid")->where(array("id" => $service_id))->find();
            # 查询会员折扣
            $member_discount = Db::table("level_discount")->where(array("type" => $service['discount_type']))->select();
            foreach ($member_discount as $k => $v) {
                $price['member_discount'][$v['level_id']] = $v['discount'];
            }
            $price['biz_pid'] = $service['biz_pid'];
            $_redis->hSetJson("servicePrice", $service_id, $price);
        }
        return $price;
    }

    /**
     * @param $service_id
     * @param $car_level
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务会员对应的价格
     */
    function serviceLevelPrice($service_id, $car_level)
    {
        $price = $this->servicePrice($service_id);
        $carLevelPrice = $price['online'][$car_level];
        # 查询所有会员
        $level = Db::table("member_level")->field("id,level_title")->where("id", ">", "0")->order("id asc")->select();
        foreach ($level as $k => $v) {
            $level[$k]['price'] = is_null($price['member_discount'][$v['id']]) ? $carLevelPrice : getsPriceFormat($carLevelPrice * $price['member_discount'][$v['id']]);
        }
        return $level;
    }
}
