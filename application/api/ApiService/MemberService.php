<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 10:10
 */

namespace app\api\ApiService;


use app\service\AgentTask;
use app\service\EalspellCommissionService;
use app\service\TimeTask;
use base\Curl;
use Redis\Redis;
use think\Db;

class MemberService
{
    /**
     * @param $memberid
     * @return array|bool|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取缓存中用户信息
     */
    function MemberInfoCache($memberid)
    {
        $_redis = new Redis();
        $_redis->hDel("memberInfo", $memberid);
        $memberinfo = $_redis->hGetJson("memberInfo", $memberid);
        if (empty($memberinfo)) {
            $memberinfo = $this->MemberInfoDB($memberid);
            $MemberService = new MemberService();
            $memberinfo['token'] = $MemberService->authCode($memberid, "ENCODE");
            if (!empty($memberinfo)) {
                $_redis->hSetJson("memberInfo", $memberid, $memberinfo);
            }
        } else {
            if ($memberinfo["member_level_id"] == 1) {
                # 判断是否过期
                if ($memberinfo['member_expiration'] < date("Y-m-d")) {
                    $memberinfo['expiration_member_level'] = $memberinfo['member_level_id'];
                    $memberinfo['member_level_id'] = 0;
                    $memberinfo['expiration_status'] = 2;
                    $_redis->hSetJson("memberInfo", $memberid, $memberinfo);
                }
            }
        }
        return $memberinfo;
    }

    /**
     * @param $memberid
     * @return array|null|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取数据库中用户信息
     */
    function MemberInfoDB($memberid)
    {
        $info = Db::table("member m,member_mapping mm")
            ->field("m.*,mm.member_level_id,mm.member_code,mm.member_expiration,mm.member_integral,convert(mm.member_balance,decimal(10,2)) member_balance,
            mm.member_paypass,mm.member_status,mm.member_unionid,mm.remind_mark,mm.member_type,mm.register_time,mm.member_create,mm.first_pay")
            ->where("m.id={$memberid} and mm.member_id=m.id")
            ->find();
        if (!empty($info)) {
            $MemberService = new MemberService();
            $info['token'] = $MemberService->authCode($memberid, "ENCODE");

            # 判断什么时间办理的会员,2021-11-18之前办理的会员(已过期) 我的页面 只能升级
//            $logHandle = Db::table('log_handlecard lh')
//                ->field('date(lh.member_create) member_create')
//                ->where("lh.level_id = 1")
//                ->where(array('member_id'=>$memberid))
//                ->order('id desc')
//                ->find();
//            if(!empty($logHandle)){  $logHandle['member_create'] < '2021-11-18' and
                if(($info['member_level_id']==1 or $info['member_level_id']==0) and !empty($info['member_expiration']) and  $info['member_expiration'] < date('Y-m-d')){
                    # 体验卡会员已过期
                    $info['logHandle'] = 1;
                }else{
                    if($info['member_level_id']==0){
                        $info['logHandle'] = 1;
                    }else {
                        $info['logHandle'] = 2;
                    }
                }
//            }else{
//                $info['logHandle'] = 2;
//            }

            # 体验卡过期状态 只有判断体验卡时候用
            $info['expiration_status'] = 1;
            if(empty($info['member_name'])){
                $info['member_name'] = $info['toutiao_name'];
            }
            if(empty($info['nickname'])){
                $info['nickname'] = $info['toutiao_name'];
            }else{
                $info['member_name'] = $info['nickname'];
            }

            if(empty($info['headimage'])){
                $info['headimage'] = $info['toutiao_head'];
            }
            # 判断用户会员等级
            if ($info['member_level_id'] == 1) {
                # 判断是否过期
                if ($info['member_expiration'] < date("Y-m-d")) {
                    $info['expiration_member_level'] = $info['member_level_id'];
                    $info['member_level_id'] = 0;
                    $info['expiration_status'] = 2;

                }
            }else{
                $info['member_expiration'] = '永久有效';
            }
            if ($info['expiration_status'] == 2) {
                $info['level_title'] = LevelService::memberLevelInfo(1);
            } else {
                if ($info['member_level_id'] > 0) {
                    $info['level_title'] = LevelService::memberLevelInfo($info['member_level_id']);
                } else {
                    $info['level_title'] = array("level_title" => "普通用户", "car_limit" => 1, "lineup_car_limit" => 0);
                }
            }
            # 查询领取的红包是哪一个
            $log_redPacket = Db::table("redpacket_log")->field("redpacket_id")->where(array("member_id" => $info['id'], "type" => 1))->order("id desc")->find();
            if (!empty($log_redPacket)) {
                $info['redpacket_id'] = $log_redPacket['redpacket_id'];
            } else {
                $info['redpacket_id'] = 0;
            }
            # 手机号隐藏
            if (!empty($info['member_phone'])) {
                $info['member_phone_hide'] = $this->hidPhone($info['member_phone']);
            }
            # 查询第一辆车
            $firstCar = Db::table('member_car')
                ->field('car_licens')
                ->where(array('member_id' => $info['id']))
                ->order(array('id' => 'asc'))
                ->find();
            if (!empty($firstCar)) {
                $firstCar = $firstCar['car_licens'];
            }
            $info['first_car'] = $firstCar;
            # 合伙人信息
            $partnerInfo = array();
            if ($info['member_type'] == 2) {
                # 查询
                $partnerInfo = Db::table('member_partner')->where(array('member_id' => $memberid))->find();
                if (!empty($partnerInfo)) {
                    $partnerInfo['partner_balance'] = priceFormat($partnerInfo['partner_balance']);
                }
            }
            $info['partnerInfo'] = $partnerInfo;
            # 提现配置
            $info['partner_withdrawal'] = config('ratios.partner_withdrawal');
        }
        return $info;
    }

    /**
     * @param $memberid
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 修改用户信息
     */
    function changeMemberInfo($memberid)
    {
        $_redis = new Redis();
        $memberinfo = $this->MemberInfoDB($memberid);
        if (!empty($memberinfo)) {
            $_redis->hSetJson("memberInfo", $memberid, $memberinfo);
        }
        return true;
    }


    private $_member_field = array("member_name", "member_age", "member_sex", "member_born", "member_phone", "member_idcard",
        "member_address", "member_province", "member_city", "member_area", "member_street",
        "member_lon", "member_lat", "member_openid", "nickname", "headimage", "subscribe", "first_login",
        "card_code", "source_type", "source_id", "use_app", "equipment_id", "equipment_version");
    private $_member_mapping_field = array("member_account", "member_password", "member_openid", "member_code", "member_integral",
        "member_balance", "member_paypass", "member_create", "member_unionid", "register_time", "member_type");

    /**
     * @param $info
     * @return bool
     * @context 新用户注册
     */
    function memberRegister($info, $applymark)
    {
        $_member = array();
        $_member_mapping = array();
        foreach ($info as $k => $v) {
            if (in_array($k, $this->_member_field)) {
                $_member[$k] = $v;
            }
            if (in_array($k, $this->_member_mapping_field)) {
                $_member_mapping[$k] = $v;
            }
            if ($applymark == 3) {
                $_member['toutiao_openid'] = $_member['member_openid'];
                $_member['toutiao_unionid'] = $_member['member_unionid'];
                $_member['toutiao_time'] = date('Y-m-d H:i:s');
                $_member['toutiao_name'] = $_member['nickname'];
                $_member['toutiao_head'] = $_member['headimage'];
                unset($_member['member_openid']);
                unset($_member['member_unionid']);
                unset($_member_mapping['member_unionid']);
            }
        }
        # 判断手机号是否被注册过
        $is_register = Db::table("member")->field("id")->where(array("member_phone" => $_member['member_phone']))->find();
        if (empty($is_register['id'])) {
            # 判断unionid 有没有被注册
            if ($applymark == 3) {
                # 抖音
                $unionWhere = array("toutiao_unionid" => $info['member_unionid']);
            }else{
                $unionWhere = array("member_unionid" => $info['member_unionid']);
            }
            $union_register = Db::table("member_mapping")->field("member_id")->where($unionWhere)->find();
            if (!empty($union_register)) {
                $member_id = $union_register['member_id'];
                Db::table("member")->where(array("id" => $member_id))->update($_member);
                $_member_mapping['member_id'] = $member_id;
                $_member_mapping['register_time'] = date("Y-m-d H:i:s");
                Db::table("member_mapping")->where(array("member_id" => $member_id))->update($_member_mapping);
            } else {
                # 添加用户信息
                $member_id = Db::table("member")->insertGetId($_member);
                # 添加用户关联
                $_member_mapping['member_id'] = $member_id;
                $_member_mapping['register_time'] = date("Y-m-d H:i:s");
                Db::table("member_mapping")->insert($_member_mapping);
            }

        } else {
            $member_id = $is_register['id'];
            Db::table("member")->where(array("id" => $member_id))->update($_member);
            $_member_mapping['member_id'] = $member_id;
            $_member_mapping['register_time'] = date("Y-m-d H:i:s");
            Db::table("member_mapping")->where(array("member_id" => $member_id))->update($_member_mapping);
        }
        # 注册渠道处理
        $this->channel($info, $member_id, 1);
        # 注册加积分
        # 先查询是否添加过注册的积分
        $logIntegral = Db::table('log_integral')->where(array('member_id' => $member_id, 'integral_source' => 9))->find();
        if (empty($logIntegral)) {
            $integral = IntegralService::integralSetItem('register_integral');
            if (!empty($integral)) {
                $integralService = new IntegralService();
                $integralService->addMemberIntegral($member_id, 9, $integral, 1);
            }
        }
        # 下载APP加积分
        $memberInfo = Db::table('member')->field('equipment_id')->where(array('id' => $member_id))->find();
        if (!empty($memberInfo['equipment_id'])) {
            $appIntegral = Db::table('log_integral')->field('id')->where(array('member_id' => $member_id, 'integral_source' => 22))->find();
            if (empty($appIntegral)) {
                $integral = IntegralService::integralSetItem('is_app');
                if (!empty($integral)) {
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($member_id, 22, $integral, 1);
                }
            }
        }
        # 推荐人加积分
        if (!empty($info['shareMemberId'])) {
            $shareIntegral = IntegralService::integralSetItem('is_user_register');
            if (!empty($shareIntegral)) {
                $logIntegral = Db::table('log_integral')->where(array('member_id' => $info['shareMemberId'], 'integral_source' => 2))
                    ->where("date_format(integral_time,'%Y-%m-%d %H:%i') = '" . date("Y-m-d H:i") . "'")->find();
                if (empty($logIntegral)) {
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($info['shareMemberId'], 2, $shareIntegral, 1);
                }
            }
            # 完成任务=>'2'=> '推荐好友注册'
            TimeTask::finishTask($info['shareMemberId'], 2);
        }

        if (!empty($info))
            return $member_id;
    }

    /**
     * @param $info array('employee_id'=>员工id , 'assign_id'=>门店id , 'shareMemberId'=> 用户id)
     * @param int $memberId 用户id
     * @param int $type 类型  1=>注册  2=>办理会员
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户注册/办理会员渠道信息
     */
    function channel($info, $memberId, $type,$channel = 0)
    {

        $pid = 0;
        if (!empty($info['employee_id'])) {
            # 员工推荐
            $channel = 4;
            $pid = $info['employee_id'];
            if (array_key_exists('formMark', $info) and ($info['formMark'] == 'employeeSal' or $info['formMark'] == 'calling')) {
                $pid = -$pid;
            }
        }
        if (!empty($info['assign_id']) and empty($info['employee_id'])) {
            # 门店推荐
            $channel = 1;
            $pid = $info['assign_id'];
        }
        if (!empty($info['shareMemberId'])) {
            # 普通用户
            $channel = 5;
            $pid = $info['shareMemberId'];
            # 查询是不是会员
            $memberLevel = Db::table('member_mapping')->field('member_level_id')->where(array('member_id' => $info['shareMemberId']))->find()['member_level_id'];
            if ($memberLevel > 0) {
                # 会员推荐
                $channel = 2;
            }
            # 查询是不是合伙人
            $partner = Db::table('member_partner')->field('id')->where(array('member_id' => $info['shareMemberId'], 'status' => 1))->find();
            if (!empty($partner)) {
                # 合伙人推荐
                $channel = 3;
            }
        }
        #商户办理会员
        if (!empty($info['formMark']) and $info['formMark'] == 'merchants') {
            #商户id
            $pid = $info['assign_id'];
            $channel = 6;


        }
        #商户员工办理会员 2021年3月4日 商户员工办理也是给商户办理  员工没提成 biz_id  就是商户id
        if (!empty($info['merchants_employee'])) {
            #商户id
            $pid = $info['employee_id'];
            $channel = 7;


        }
        if(!empty($info['formMark']) and $info['formMark']=='agent'){
            $pid = $info['assign_id'];
            $channel = 10;
            AgentTask::teamNewMermber($pid);
            AgentTask::downLoadTask($pid);
        }
        # 添加渠道信息
        Db::table('channel')->insert(array(
            'member_id' => $memberId,
            'pid' => $pid,
            'channel' => $channel,
            'biz_id' => $info['assign_id'] ?? 0,
            'action_type' => $type
        ));
        if($type==2){
            # 办理会员将邀请好友的记录也变成这个人
            Db::table('channel')->where(array("member_id"=>$memberId,"action_type"=>1))
                ->update(array("channel"=>$channel,"pid"=>$pid,"biz_id"=>$info['assign_id'] ?? 0));
        }
    }

    /**
     * @param $mobile
     * @param $code
     * @return bool
     * @context 验证短信验证码
     */
    function valiPhoneCode($mobile, $code)
    {
        $redis = new Redis();
        $valicode = $redis->hGet('smsCode', strval($mobile));
        if ($code == '2004' or $code == $valicode) {
            return true;
        }
        return false;
    }

    /**
     * @param $string
     * @param string $operation
     * @param string $key
     * @param int $expiry
     * @return bool|string
     * @content 生成token(玩家ID  加密/解密)
     */
    function authCode($string, $operation = 'DECODE', $key = '', $expiry = 0)
    {
        // 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙
        $ckey_length = 4;
        // 密匙
        $key = md5($key ? $key : config('ecars.token_prefix'));

        // 密匙a会参与加解密
        $keya = md5(substr($key, 0, 16));
        // 密匙b会用来做数据完整性验证
        $keyb = md5(substr($key, 16, 16));
        // 密匙c用于变化生成的密文
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
        // 参与运算的密匙
        $cryptkey = $keya . md5($keya . $keyc);
        $key_length = strlen($cryptkey);
        // 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，
        //解密时会通过这个密匙验证数据完整性
        // 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
        $string_length = strlen($string);
        $result = '';
        $box = range(0, 255);
        $rndkey = array();
        // 产生密匙簿
        for ($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        // 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上对并不会增加密文的强度
        for ($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        // 核心加解密部分
        for ($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            // 从密匙簿得出密匙进行异或，再转成字符
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if ($operation == 'DECODE') {


            // 验证数据有效性，请看未加密明文的格式
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            // 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因
            // 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码
            return $keyc . str_replace('=', '', base64_encode($result));
        }
    }

    /**
     * @param $phone
     * @param $start
     * @param $length
     * @return mixed
     * @content 手机号****加密显示
     */
    function hidPhone($phone, $start = 4, $length = 4)
    {
        $_start = '*';
        for ($i = 1; $i < $length; $i++) {
            $_start .= '*';
        }
        return substr_replace($phone, $_start, $start, $length);
    }

    /**
     * @param $unionid
     * @return array|bool[]
     * @context 获取挪车码
     */
    function getsNuocheCode($unionid, $member_id)
    {
        if (empty($unionid)) {
            return array("status" => false);
        }
        $_curl = new Curl();
        $result = $_curl->post("https://www.nuochepai.cn/wxchat/EpublicCode/getCodeNum.html", array('unionid' => $unionid));
        $result = json_decode($result, true);
        if (!empty($result['status']) and $result['status'] == true) {
            $code = $result['code'];
            # 修改用户code
            Db::table("member_mapping")->where(array("member_id" => $member_id))->update(array("member_code" => $code));
            $this->confirmNuocheCode($member_id, $code, $unionid);
            return array("status" => true, "code" => $code);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 本地库获取挪车id
     */
    function getsNuocheCodeNew($member_id)
    {
        $codeNum = "";
        $_time = 0;
        while (true) {
            $code_table = rand(0, 9);
            $code = Db::table("codes_" . $code_table)->where("codes_bind=1 and codes_status=0")->orderRaw("rand()")->find();
            if (!empty($code)) {
                Db::table("codes_" . $code_table)->where(array("id" => $code['id']))->update(array("codes_bind" => 0));
                $codeNum = str_pad($code['id'], 5, "0", STR_PAD_LEFT);
                $codeNum = $code_table . $codeNum;
                # 修改用户code
                Db::table("member_mapping")->where(array("member_id" => $member_id))->update(array("member_code" => $codeNum));
                break;
            } else {
                if ($_time > 20) break;
                $_time++;
            }
        }
        return array("status" => true, "code" => $codeNum);
    }

    /**
     * @param $member_id
     * @param $code
     * @param $unionid
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 确认挪车码
     */
    function confirmNuocheCode($member_id, $code, $unionid)
    {
        #查询用户信息
        $member_info = Db::table('member')->where(array('id' => $member_id))->find();
        if (!empty($member_info)) {
            $data = array_filter(array('member_phone' => $member_info['member_phone'],
                'member_name' => $member_info['member_name'],
                'member_age' => $member_info['member_age'],
                'member_sex' => $member_info['member_sex'],
                'member_born_year' => $member_info['member_born_year'],
                'member_born_month' => $member_info['member_born_month'],
                'member_born_day' => $member_info['member_born_day'],
                'member_idcard' => $member_info['member_idcard'],
                'member_address' => $member_info['member_address'],
                'member_province' => $member_info['member_province'],
                'member_city' => $member_info['member_city'],
                'member_area' => $member_info['member_area'],
                'member_street' => $member_info['member_street'],
                'member_lon' => $member_info['member_lon'],
                'member_lat' => $member_info['member_lat'],
                'member_unionid' => $unionid,
                'code' => $code));
        } else {
            $data = array('code' => $code);
        }
        $_curl = new Curl();
        $result = $_curl->post("https://www.nuochepai.cn/wxchat/EpublicCode/changeCodeStatus.html", $data);
        return true;
    }
}

