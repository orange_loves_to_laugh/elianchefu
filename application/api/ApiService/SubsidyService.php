<?php


namespace app\api\ApiService;


use think\Db;

class SubsidyService
{
    static function addSubsidy($data)
    {
        $_array=array("subsidy_type"=>$data['subsidy_type'],"subsidy_create"=>date("Y-m-d H:i:s"),
            "subsidy_number"=>$data['subsidy_number'],"member_id"=>$data["member_id"]);
        if(!empty($data["pid"])){
            # 商户订单id  服务工单id  合作店任务id  商户任务id
            $_array["pid"]=$data["pid"];
        }
        if(!empty($data["order_number"])){
            $_array["order_number"] = $data["order_number"];
        }
        if(!empty($data["biz_id"])){
            $_array["biz_id"] = $data["biz_id"];
        }
        if(!empty($data["order_type"])){
            $_array["order_type"] = $data["order_type"];
        }
        if(!empty($data["voucher_id"])){
            $_array["voucher_id"] = $data["voucher_id"];
        }
        Db::table("subsidy")->insert($_array);
        return true;
    }

    /**
     * @param $data
     * @return bool
     * @context 添加平台的费用支出
     */
   static function addExpenses($data)
    {
        $explain = array(2=>"平台收款手续费",3=>"车险投保提成金额",4=>"推广员收入提成金额",5=>"合作店办理会员提成金额",9=>"商家办理会员提成提成金额",10=>"合作店升级提成金额",
            11=>"商家会员升级提成金额", 12=>"合作店会员充值提成金额",13=>"商家会员充值提成金额",14=>"推荐合伙人");
        $_array = array("category"=>2,"explain"=>$explain[$data["type_id"]],"type_id"=>$data['type_id'],"price"=>$data["price"],"title"=>$explain[$data["type_id"]],
            "create_time"=>date("Y-m-d H:i:s"),"update_time"=>date("Y-m-d H:i:s"));
        if(!empty($data['username'])){
            $_array['username'] = $data['username'];
        }
        Db::table("expenses")->insert($_array);
        return true;
    }

    /**
     * @param $biz_id
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 添加门店推荐会员奖励任务
     */
    static function recommendBizTask($biz_id)
    {
        # 查询总开关
        $switch = Db::table("switch")->where(array("switch_type"=>"recommendaward","switch_status"=>1))->find();
        if(empty($switch)){
            return true;
        }
        # 查询今日活动
        $award = Db::table("recommend_award")->where(array("status"=>1))
            ->where("start_time<='".date("Y-m-d H:i:s")."' and end_time>'".date("Y-m-d H:i:s")."'")
            ->find();
        if(empty($award)){
            return true;
        }
        # 查询该门店当天是否有可提现账户
        $accountInfo = Db::table("recommend_biz")->where(array("biz_id"=>$biz_id,"create_time"=>date("Y-m-d")))->find();
        if(!empty($accountInfo)){
            if($accountInfo['plan']<$accountInfo['amount']){
                Db::table("recommend_biz")->where(array("id"=>$accountInfo['id']))->update(array("plan"=>intval($accountInfo['plan'])+1,
                    "earn_balance"=>floatval($accountInfo['earn_balance'])+floatval($accountInfo['once_earn']),
                    "earn_t"=>floatval($accountInfo['earn_t'])+floatval($accountInfo['once_earn'])));
            }
        }else{
            if(!empty($award)){
                $info = array(
                    "biz_id"=>$biz_id,"create_time"=>date("Y-m-d"),"amount"=>intval($award['award_max']/$award['award_once']),
                    "plan"=>1,"max_earn"=>$award['award_max'],"once_earn"=>$award['award_once'],
                    "earn_balance"=>$award['award_once'],"earn_t"=>$award['award_once'],"title"=>$award['title']
                );
                $id = Db::table("recommend_biz")->insertGetId($info);
            }

        }
        return true;
    }
}