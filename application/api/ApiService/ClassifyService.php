<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/3
 * Time: 16:10
 */

namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class ClassifyService
{
    static private $MARK;
    static private $max_array=array();
    static private $middle_array=array();
    static private $CLASS_PID;
    /**
     * @return array|bool|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务分类
     */
    static function serviceClassAll()
    {
        $_redis=new Redis();
        //$_redis->hDel("service_class","all");
        $_class=$_redis->hGetJson("service_class","all");
        if(empty($_class)){
            $_class=Db::table("service_class")->where(array("is_del"=>2))->order("service_class_pid asc")->select();
            if(!empty($_class)){
                $_redis->hSetJson("service_class","all",$_class);
                return $_class;
            }else{
                return [];
            }
        }else{
            return $_class;
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务多级分类
     */
    static function getsServiceMoreClass()
    {
        $class_all=self::serviceClassAll();
        if(empty($class_all)){
            return [];
        }
        $_return = array();
        foreach($class_all as $k=>$v){
            if($v['service_class_pid']==0){
                $_return[$v['id']]['id']=$v['id'];
                $_return[$v['id']]['name']=$v['service_class_title'];
            }else{
                if(empty($_return[$v['service_class_pid']]['child'])){
                    $_return[$v['service_class_pid']]['child']=array(array("id"=>$v['id'],"name"=>$v['service_class_title']));
                }else{
                    array_push($_return[$v['service_class_pid']]['child'],array("id"=>$v['id'],"name"=>$v['service_class_title']));
                }
            }
        }
        return array_values($_return);
    }

    /**
     * @param null $mark
     * @return array|bool|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取服务分类
     */
    static function getsServiceClass($mark=null,$class_pid=null)
    {
        $class_all=self::serviceClassAll();
        if(empty($class_all)){
            return [];
        }else{
            if(empty($mark)){
                return $class_all;
            }else{
                self::$MARK=$mark;
                self::$CLASS_PID=$class_pid;
                $class=array_filter($class_all,"self::screenClass");
                if(self::$MARK=='second'){
                    if(!empty(self::$middle_array)){
                        $plurs=array_filter(self::$middle_array,"self::screenClass");
                        if(!empty($plurs)){
                            $class=array_merge($class,$plurs);
                        }
                    }
                }
                return array_values($class);
            }
        }
    }

    /**
     * @param $item
     * @return bool
     * @context 服务分类过滤
     */
    static function screenClass($item)
    {
        if(self::$MARK=='max'){
            # 获取服务分类最高级的4个
            if($item['service_class_pid']!=0){
                return false;
            }
        }elseif(self::$MARK=='second'){
            if($item['service_class_pid']==0){
                array_push(self::$max_array,$item['id']);
                return false;
            }else{
                if(in_array($item['service_class_pid'],self::$max_array)){
                   return true;
                }else{
                    array_push(self::$middle_array,$item);
                    return false;
                }
            }
        }elseif(self::$MARK=='second_item'){
            if($item['service_class_pid']==0 or $item['service_class_pid']!=self::$CLASS_PID){
                return false;
            }
        }
        return true;
    }


}