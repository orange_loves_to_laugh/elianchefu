<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10
 * Time: 10:56
 */

namespace app\api\ApiService;


use app\service\CommissionService;
use app\service\EalspellCommissionService;
use app\service\IGeTuiService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class BalanceService
{
    /**
     * @param $member_id
     * @param $price  增加/扣除的 价格
     * @param $mark   增加/扣除 标识
     * @param $data   消费记录数据
     * @return bool
     * @throws \think\Exception
     * @context 操作用户余额
     */
    function operaMemberbalance($member_id, $price, $mark, $data = array())
    {
        if ($mark == 1) {
            # 增加余额
            Db::table("member_mapping")->where(array("member_id" => $member_id))->setInc("member_balance", $price);

        } else {
            # 扣除余额
            Db::table("member_mapping")->where(array("member_id" => $member_id))->setDec("member_balance", $price);

        }
        if (!empty($data)) {
            # 添加消费记录
            $_log = array("member_id" => $member_id, "consump_price" => $data['consump_price'], "consump_pay_type" => $data['pay_type'],
                "consump_time" => date("Y-m-d H:i:s"), "log_consump_type" => $data['log_consump_type'], "consump_type" => $data['consump_type'],
                "income_type" => $data['income_type']);
            if (key_exists('giving', $data)) {
                $_log['giving'] = $data['giving'];
            }
            if (key_exists('order_number', $data)) {
                $_log['order_number'] = $data['order_number'];
            }
            if (key_exists('biz_id', $data)) {
                $_log['biz_id'] = $data['biz_id'];
            }
            Db::table("log_consump")->insert($_log);
        }
        return true;
    }

    /**
     * @param $member_id
     * @param $pay_price 支付金额
     * @param $pay_type 支付方式
     * @param $consump_type 线上/门店支付
     * @param int $giving 赠送金额
     * @param int $biz_id 门店id
     * @param string $order_number 订单号
     * @return bool
     * @throws \think\Exception
     */
    function memberRecharge($member_id, $pay_price, $pay_type, $consump_type, $giving = 0, $biz_id = 1, $order_number = '', $employee_id = '', $assign_id = '', $shareMemberId = '', $formMark = '')
    {
        $_redis = new Redis();
        $MemberService = new MemberService();
        $memberInfo = $MemberService->MemberInfoCache($member_id);
        $memberInfo['member_balance'] = getsPriceFormat($memberInfo['member_balance'] + $pay_price + $giving);
        $this->operaMemberbalance($member_id, ($pay_price), 1, array("consump_price" => $pay_price, "pay_type" => $pay_type, "log_consump_type" => 2,
            "consump_type" => $consump_type, "giving" => $giving, "income_type" => 7, "biz_id" => $biz_id, "order_number" => $order_number));
        $_redis->hSetJson("memberInfo", $member_id, $memberInfo);
        if($giving>0){
            $this->operaMemberbalance($member_id, ($giving), 1, array("consump_price" => $giving, "pay_type" => $pay_type, "log_consump_type" => 2,
                "consump_type" => $consump_type, "giving" => $giving, "income_type" => 22, "biz_id" => $biz_id, "order_number" => $order_number));
            # 增加补贴记录
            SubsidyService::addSubsidy(array("subsidy_type"=>4,"subsidy_number"=>$giving,"member_id"=>$member_id));
        }
        # 增加微信支付宝收款手续费
        if($pay_type==1 or $pay_type==2){
            SubsidyService::addExpenses(array("type_id"=>2,"price"=>$pay_price*0.006));
        }
        # 判断是否有推荐员工或推荐门店
        $nowLevel = $memberInfo['member_level_id'];
        if (!empty($employee_id) or !empty($assign_id)) {
            $commissionService = new CommissionService();
            # 当前等级

            $commissionService->cooperativeDeduct($assign_id, $employee_id, 'Recharge', $pay_price, $member_id, $nowLevel, 0, $order_number, $formMark,$pay_type);
            /* if (!empty($employee_id) and empty($assign_id)) {
                 # 加员工提成 算到平台

             } elseif (!empty($employee_id) and !empty($assign_id)) {
                 # 判断员工是否是指派员工  指派员工将提成2%结算到门店里

             } elseif (empty($employee_id) and !empty($assign_id)) {
                 # 合作店直接升级 只加1%的提成到结算里

             }*/
        }

        # 会员终身绑定 门店 之后 会员每次升级充值均获得1% 的提成奖励
        LevelService::getMerchantsDeduct($assign_id, $employee_id, 'Recharge', $pay_price, $member_id, $nowLevel, 0, $order_number, $formMark,$pay_type);
        # 推荐人加积分
        if (!empty($shareMemberId)) {
            $integral = intval($pay_price * 0.3);
            $integralService = new IntegralService();
            $integralService->addMemberIntegral($shareMemberId, 20, $integral, 1);
        }
        # 完成任务=>13 会员充值
        TimeTask::finishTask($member_id, 13);
        # 给代理提成
        EalspellCommissionService::memberCharge($member_id);
        # 发送消息提醒
        $IGeTuiService = new IGeTuiService();
        $IGeTuiService->MessageNotification($member_id, 6, ['price' => $pay_price, 'giveBalance' => $giving, 'balance' => $memberInfo['member_balance'], 'phone' => $memberInfo['member_phone'], 'integral' => $memberInfo['member_integral']], false);
        return true;
    }

    /**
     * @param $member_id
     * @param $pay_price
     * @param $pay_type
     * @param $consump_type
     * @param int $biz_id
     * @param $upgradeId
     * @param $now_level
     * @param $up_level
     * @param string $order_number
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 升级会员
     */
    function memberUpgrade($member_id, $pay_price, $pay_type, $consump_type, $biz_id = 1, $upgradeId, $up_level, $order_number = '', $assign_id = '', $employee_id = '', $shareMemberId = '', $formMark = '')
    {
        $_redis = new Redis();
        $MemberService = new MemberService();
        $memberInfo = $MemberService->MemberInfoCache($member_id);
        $addPrice = $pay_price;
        $addIntegral = 0;
        $integral_type = $consump_type == 1 ? 10 : -10;
        $voucher_source = $consump_type == 1 ? 7 : 8;
        # 增加微信支付宝收款手续费
        if($pay_type==1 or $pay_type==2){
            SubsidyService::addExpenses(array("type_id"=>2,"price"=>$pay_price*0.006));
        }
        # 获取升级赠送
        $giving = LevelService::upgradeGiving($upgradeId);
        if (!empty($giving)) {
            $VoucherService = new VoucherService();
            foreach ($giving as $k => $v) {
                if ($v['giving_type'] == 1 or $v['giving_type'] == 2) {
                    # 加卡券
                    $_voucher = array();
                    $voucher_type = $v['giving_type'] == 1 ? 3 : 4;
                    for ($i = 0; $i < $v['giving_number']; $i++) {
                        array_push($_voucher, array("voucher_id" => $v['giving_id'], "card_time" => $v['card_time'], "voucher_source" => $voucher_source,
                            "paytype" => $pay_type, "voucher_cost" => $v['intbal'], "card_title" => $v['card_title'], "voucher_type" => $voucher_type));
                    }
                    $VoucherService->addMemberVoucher($member_id, $_voucher);
                } elseif ($v['giving_type'] == 3) {
                    # 加积分
                    $addIntegral += $v['intbal'] * $v['giving_number'];
                } elseif ($v['giving_type'] >=7 and $v['giving_type'] <= 16 ) {
                    $VoucherService->addMerchantVoucher($member_id, $v['giving_number'], $v['giving_id'], 7);
                } else {
                    # 加余额
                    $addPrice += $v['intbal'] * $v['giving_number'];
                    # 增加补贴记录
                    SubsidyService::addSubsidy(array("subsidy_type"=>3,"subsidy_number"=>$v['intbal'] * $v['giving_number'],"member_id"=>$member_id));
                }
            }
        }
        $memberInfo['member_balance'] = getsPriceFormat($memberInfo['member_balance'] + $addPrice);
        $this->operaMemberbalance($member_id, $addPrice, 1, array("consump_price" => $addPrice, "pay_type" => $pay_type, "log_consump_type" => 2,
            "consump_type" => $consump_type, "income_type" => 6, "biz_id" => $biz_id, "order_number" => $order_number));
        if ($addIntegral > 0) {
            $IntegralService = new IntegralService();
            $IntegralService->addMemberIntegral($member_id, $integral_type, $addIntegral, 1, array("biz_id" => $biz_id));
            $memberInfo['member_integral'] += intval($addIntegral);
        }
        # 当前用户等级
        $nowLevel = $memberInfo['member_level_id'];
        # 改变用户会员等级
        $memberInfo['member_level_id'] = $up_level;
        $memberInfo['expiration_status'] = 1;
        Db::table("member_mapping")->where(array("member_id" => $member_id))->update(array("member_level_id" => $up_level));
        # 恢复合伙人状态
        if($up_level>=2 and $memberInfo['member_type']==1){
            $partnerInfo = Db::table('member_partner')->field('id')->where(array('member_id'=>$member_id))->find();
            if(!empty($partnerInfo)){
                Db::table('member_mapping')->where(array('member_id'=>$member_id))->update(array('member_type'=>2));
                $memberInfo['member_type'] = 2;
            }
        }
        $_redis->hSetJson("memberInfo", $member_id, $memberInfo);
        # 判断是否有推荐员工或推荐门店
//        $employee_id = 11;
        if (!empty($employee_id) or !empty($assign_id)) {
            $commissionService = new CommissionService();
            $commissionService->cooperativeDeduct($assign_id, $employee_id, 'upgrade', $pay_price, $member_id, $nowLevel, $up_level, '', $formMark,$pay_type);

            /*if (!empty($employee_id) and empty($assign_id)) {
                # 加员工提成 算到平台

            } elseif (!empty($employee_id) and !empty($assign_id)) {
                # 判断员工是否是指派员工  指派员工将提成2%结算到门店里

            } elseif (empty($employee_id) and !empty($assign_id)) {
                # 合作店直接升级 只加1%的提成到结算里

            }*/
        }
        # 会员终身绑定 门店 之后 会员每次升级充值均获得1% 的提成奖励
        LevelService::getMerchantsDeduct($assign_id, $employee_id, 'upgrade', $pay_price, $member_id, $nowLevel, $up_level, '', $formMark,$pay_type);
        $levelInfo = LevelService::memberLevelInfo($up_level);
        # 推荐升级会员加积分
        if (!empty($shareMemberId)) {
            $integral = intval($pay_price * 0.3);
            $integralService = new IntegralService();
            $integralService->addMemberIntegral($shareMemberId, 21, $integral, 1);
        }
        # 完成任务=>14 会员升级
        TimeTask::finishTask($member_id, 14);
        # 发送消息提醒
        $IGeTuiService = new IGeTuiService();
        $IGeTuiService->MessageNotification($member_id, 5, ['level_title' => $levelInfo['level_title'], 'member_level' => $up_level]);
        return true;
    }

}
