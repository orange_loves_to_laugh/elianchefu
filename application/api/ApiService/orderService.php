<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/28
 * Time: 16:07
 */

namespace app\api\ApiService;


use app\api\controller\Merchants;
use app\api\controller\PayMent;
use app\businessapi\ApiService\TaskService;
use app\businessapi\controller\Task;
use app\service\AgentTask;
use app\service\CommissionService;
use app\service\EalspellCommissionService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class orderService
{
    /**
     * @param $info
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 支付成功后生成订单
     */
    function makeOrderServer($info)
    {
        $pay_type = $info['pay_type'];
        if($pay_type==-1){
            $pay_type = 5;
        }
        # 判断当价格为0的时候 支付方式为卡券核销
        if ($info['pay_price'] <= 0) {
            $info['pay_type'] = 6;
            $info['pay_price'] = 0;
            if ($info['pay_price'] < 0)
                $info['voucher_cost'] = $info['voucher_cost'] + $info['pay_price'];
        }
        # 增加微信支付宝收款手续费
        if ($info['pay_type'] == 1 or $info['pay_type'] == 2) {
            SubsidyService::addExpenses(array("type_id" => 2, "price" => $info['pay_price'] * 0.006));
        }
        # 查询服务原价
        $orderPrice = $info['pay_price'] + $info['voucher_cost'];
        if (!empty($info['order_server'])) {
            $_order_server = array();
            $PriceService = new PriceService();
            $discount = 1;
            $redis = new Redis();
            $recountPriceDiscount = $redis->hGetJson('recountPriceDiscount', 'm' . $info['member_id']);
            if (!empty($recountPriceDiscount)) {
                $discount = $recountPriceDiscount[$pay_type];
            }
            $orderPrice = 0;
            foreach ($info['order_server'] as $k => $v) {
//              "server_id" => $v['server_id']
                $price = $PriceService->servicePrice($v['server_id'], $info['biz_id']);
                Db::table('aaa')->insert(array(
                    'info' => json_encode($price),
                    'type' =>  'makeOrderServer-payRedis-' . date('Y-m-d H:i:s') . '-orderNumber'
                ));
                Db::table('aaa')->insert(array(
                    'info' => json_encode($recountPriceDiscount),
                    'type' =>  'recountPriceDiscount-payRedis-' . date('Y-m-d H:i:s') . '-orderNumber'
                ));
                Db::table('aaa')->insert(array(
                    'info' => json_encode($discount),
                    'type' =>  '$discount-payRedis-' . date('Y-m-d H:i:s') . '-orderNumber'
                ));
                $info['order_server'][$k]['original_price'] = floatval($price['online'][$info['car_level']]) * $discount;
                $orderPrice += $info['order_server'][$k]['original_price'];
            }
        }
        $orders_array = array();
        if ($info['orders_type'] == 3) {
            # 预约订单(线上订单)
            $orders_array = array("biz_pro_id" => $info['server_id'], "member_id" => $info['member_id'], "order_create" => date("Y-m-d H:i:s"), "order_number" => $info['order_number'],
                "order_status" => 1, "order_type" => $info['orders_type'], "pay_create" => date("Y-m-d H:i:s"), "pay_type" => $info['pay_type'], "pay_price" => $info['pay_price'], "biz_type" => 2,
                "order_appoin_time" => $info['appoint_time'], "biz_id" => $info['biz_id'], "member_car_id" => $info['member_car_id'], "car_liences" => $info['car_liences'],
                "car_level" => $info['car_level'], "car_logo" => $info['car_logo'], "car_sort" => $info['car_sort'], "order_price" => $orderPrice, "voucher_price" => $info['voucher_cost'], "wx_app_pay" => $info['wx_app_pay'],
                "is_online" => 1);
        }
        $pay_order_number = $info['order_number'];
//        if (!empty($info['already']) and $info['already'] == 1) {
//            # 已经有预约订单的情况
//            $info['order_number'] = $info['already_order_number'];
//            # 查询订单信息
//            $order_info = Db::table("orders")->where(array("order_number" => $info['already_order_number']))->find();
//            $order_id = $order_info['id'];
//            $pay_price = $order_info['pay_price'] + $info['pay_price'];
//            $order_price = $order_info['order_price'] + $info['pay_price'];
//            $voucher_price = $order_info['voucher_price'] + $info['voucher_cost'];
//            Db::table("orders")->where(array("order_number" => $info['already_order_number']))
//                ->update(array("already" => 1, "pay_price" => $pay_price, "order_price" => $order_price, "voucher_price" => $voucher_price));
//        } else {
        # 查询相同订单
        $orderInfo = Db::table('orders')->field('id')->where(array('order_number' => $info['order_number']))->find();
        if (!empty($orderInfo)) {
            return array("status" => true);
        }
        $order_id = Db::table("orders")->insertGetId($orders_array);
//        }

        //dump($info['order_server']);
        if (!empty($info['order_server'])) {
            $_order_server = array();
            foreach ($info['order_server'] as $k => $v) {
                array_push($_order_server, array("order_number" => $info['order_number'], "server_id" => $v['server_id'], "status" => 1, "price" => $v['price'],
                    "custommark" => $v['custommark'], "price_lock" => $v['price_lock'], "coupon_mark" => $v['coupon_mark'], "coupon_price" => $v['coupon_price'],
                    "original_price" => $v['original_price'], "pay_order_number" => $pay_order_number, "pay_type" => $info['pay_type'], "wx_app_pay" => $info['wx_app_pay'], 'appointment_status' => 2));
            }
            Db::table("order_server")->insertAll($_order_server);
        }
        if ($info['pay_type'] == 5 and $info['pay_price'] > 0) {
            # 余额支付 扣除用户余额
            Db::table("member_mapping")->where(array("member_id" => $info['member_id']))->setDec("member_balance", $info['pay_price']);
        }
        if ($info['voucher_id'] > 0) {
            $orderServerId = Db::table('order_server')->where(array('order_number'=>$info['order_number']))->value('id');
            Db::table("member_voucher")->where(array("id" => $info['voucher_id']))->update(array("status" => 2, "order_id" => $order_id,'order_server_id'=>$orderServerId));
        }
        # 判断是否有积分赠送
        $integral_giving = Db::table("service_giving")->field("giving_number")->where(array("service_id" => $info['server_id'], "giving_type" => 4))->find();
        if (!empty($integral_giving)) {
            $integralService = new IntegralService();
            $integralService->addMemberIntegral($info['member_id'], 3, $integral_giving['giving_number'], 1);
        }
        # 完成服务=>11 预约服务
        TimeTask::finishTask($info['member_id'], 11);
        # 判断是否关联的是代理完成代理任务
        $agentInfo = Db::table("channel")->field("pid")->where(array("member_id" => $info['member_id'], "channel" => 10))->find();
        if (!empty($agentInfo)) {
            AgentTask::memberConsumpNumTask($agentInfo['pid'], $info['pay_price']);
        }

        return array("status" => true);
    }

    /**
     * @param $biz
     * @return null|string
     * @context 商户订单号
     */
    static function getOrderNumber($biz)
    {
        $str = null;
        $biz_mark = "30";
        if ($biz >= 10) {
            $biz_mark .= rand(0, 9) . $biz;
        } else {
            $biz_mark .= rand(0, 9) . $biz;
        }
        # 日期时间
        $date = time();
        $number = Db::table('orders')->where(array("biz_id" => $biz))->where("date(order_create)='" . date("Y-m-d") . "'")->count();
        $number = sprintf("%05d", $number + 1);
        $str = $date . $biz_mark . $number;
        return $str;
    }

    /**
     * @param $member_id
     * @param $status
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 预约订单信息
     */
    static function appointOrders($member_id, $status, $page = 0)
    {
        $where = "o.id>0 ";
        if ($status == 1) {
            # 待到店 and o.order_appoin_time>='" . date("Y-m-d H:i:s", strtotime("-30 minutes")) . "'
            $where .= " and ((o.order_status in (1,2,3) ) or o.order_status=6)";
        } elseif ($status == 2) {
            # 已失效 o.order_appoin_time<='" . date("Y-m-d H:i:s", strtotime("-1 hour")) . "' and
            $where .= " and (o.order_status =4 or ( o.order_status in (1,2,3)))";
        } else {
            # 已到店
            $where .= " and o.order_status in (5)";
        }
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        if ($status < 3) {
            $list = Db::table("orders o")
                ->field("o.order_number,o.order_appoin_time,s.service_title,convert(o.pay_price,decimal(10,2)) order_price,concat(b.city,b.area,b.biz_address) address,
            date_format(o.order_appoin_time,'%Y/%m/%d %H:%i:%s') order_appoin_time,
            (case b.recommend_status when 1 then 'A类店' when 2 then 'B类店' end) biz_types,biz_phone,
            b.biz_title,(case b.biz_type when 1 then '直营店' when 2 then '加盟店' when 3 then '合作店' end) biz_type,
            b.biz_lon,b.biz_lat")
                ->leftJoin("biz b", "o.biz_id = b.id")
                ->leftJoin("service s ", "o.biz_pro_id = s.id")
                ->where(array("o.member_id" => $member_id, "o.order_type" => 3))
                ->where($where)
                ->order("o.order_appoin_time")
                ->limit($page_num, 10)
                ->select();
        } else {
            $list = Db::query("
              select * from (select o.order_number,s.service_title,convert(o.pay_price,decimal(10,2)) order_price,
            date_format(o.order_appoin_time,'%Y/%m/%d %H:%i:%s') order_appoin_time,b.biz_title,
              (case b.recommend_status when 1 then 'A类店' when 2 then 'B类店' end) biz_types,biz_phone,                      
            (case b.biz_type when 1 then '直营店' when 2 then '加盟店' when 3 then '合作店' end) biz_type,b.biz_lon,b.biz_lat,1 checkorder_type
            from orders o left join biz b on o.biz_id = b.id left join service s on o.biz_pro_id = s.id where o.member_id = {$member_id} and o.order_type=3 and {$where}
                union
                select  mo.order_number, mp.title service_title,mo.actual_price as order_price,
                       date_format(mo.create_time,'%Y/%m/%d %H:%i:%s') order_appoin_time,
                       m.title biz_title,'' biz_type,m.tel biz_phone,m.level biz_type,m.lon biz_lon,m.lat biz_lat,2 checkorder_type
            from merchants_order mo left join merchants_orderdetail mods on mods.order_id = mo.id  left join merchants_pro mp on mp.id = mods.product_id 
                left join merchants m  on m.id = mo.merchants_id where mo.member_id = {$member_id} and mo.order_status != 1 and (mo.pay_type=1 or mo.pay_type=6)) as t 
            order by order_appoin_time limit {$page_num},10
            ");
            if (!empty($list)) {
                $Merchant = new Merchants();
                foreach ($list as $k => $v) {
                    if ($v['checkorder_type'] == 2) {
                        $list[$k]['biz_type'] = $Merchant->merchantLevel($v['biz_type']);
                    }
                }
            }
        }

        return $list;
    }

    /**
     * @param $member_id
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 待评论订单
     */
    static function evaluationOrders($member_id)
    {
        $list = Db::table("orders o")
            ->field("o.order_number,date_format(o.order_over,'%Y-%m-%d %H:%i') order_over,o.biz_id,b.biz_title,o.pay_type,
            convert(o.pay_price,decimal(10,2)) pay_price,1 evaluation_order_type")
            ->leftJoin("biz b", "b.id=o.biz_id")
            ->where(array("o.evaluation_status" => 1, "o.member_id" => $member_id))
            ->where("o.order_status=5 or o.order_status=7")
            ->where("o.order_over", ">=", date("Y-m-d H:i:s", strtotime('-1 day')))
            ->where("o.order_type = 2 or o.order_type = 3")
            ->select();
        $merchantsOrderList = Db::table("merchants_evaluation me,merchants_order mo,merchants m")
            ->field("mo.order_number,date_format(me.create_time,'%Y-%m-%d %H:%i') order_over,mo.merchants_id biz_id,m.title biz_title,mo.pay_type,
            convert(mo.actual_price,decimal(10,2)) pay_price,2 evaluation_order_type")
            ->where(array("me.default_mark" => 1, "mo.member_id" => $member_id, "mo.order_status" => 2))
            ->where("m.id = mo.merchants_id and me.merchants_order_id = mo.id and me.create_time >= '" . date("Y-m-d H:i:s", strtotime('-1 day')) . "'")
            ->select();
        $list = array_merge($list, $merchantsOrderList);
        return $list;
    }

    /**
     * @param $order_number
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 待评论订单格式化
     */
    static function formatEvaluationOrders($order_number, $evaluation_order_type)
    {
        if (!empty($evaluation_order_type) and $evaluation_order_type != 1) {
            # 查询商家订单
            return array(array("order_number" => $order_number, "comment_score" => 0));
        } else {
            # 查询服务工单
            $orderServer = Db::table("order_server")->field("order_server.*,1 orderType")->where(array("order_number" => $order_number))->select();
            $orderbiz = Db::table("order_biz")->field("order_biz.*,2 orderType")->where(array("order_number" => $order_number))->select();
            $newArray = array_merge($orderServer, $orderbiz);
            $_mapping_array = array();
            if (!empty($newArray)) {
                foreach ($newArray as $k => $v) {
                    if (empty($v['employee_id'])) {
                        if (key_exists("biz", $_mapping_array)) {
                            if ($v['orderType'] == 1) {
                                array_push($_mapping_array['biz']['service'], $v['id']);
                                array_push($_mapping_array['biz']['service_custommark'], $v['custommark']);
                            } else {
                                array_push($_mapping_array['biz']['pro'], $v['id']);
                                array_push($_mapping_array['biz']['pro_custommark'], $v['custommark']);
                            }
                        } else {
                            $_mapping_array['biz'] = array("service" => array(), "pro" => array(), "orders" => array(), "comment_score" => 0, 'service_custommark' => array(), 'pro_custommark' => array());
                            if ($v['orderType'] == 1) {
                                array_push($_mapping_array['biz']['service'], $v['id']);
                                array_push($_mapping_array['biz']['service_custommark'], $v['custommark']);
                            } else {
                                array_push($_mapping_array['biz']['pro'], $v['id']);
                                array_push($_mapping_array['biz']['pro_custommark'], $v['custommark']);
                            }
                            $_mapping_array['biz']['orders'] = $v;
                        }
                    } else {
                        # 将员工变为数组然后排序后用and 连接
                        $employee_arr = explode(',', $v['employee_id']);
                        sort($employee_arr);
                        $employee_str = implode('and', $employee_arr);
                        if (key_exists($employee_str, $_mapping_array)) {
                            if ($v['orderType'] == 1) {
                                array_push($_mapping_array[$employee_str]['service'], $v['id']);
                                array_push($_mapping_array[$employee_str]['service_custommark'], $v['custommark']);
                            } else {
                                array_push($_mapping_array[$employee_str]['pro'], $v['id']);
                                array_push($_mapping_array[$employee_str]['pro_custommark'], $v['custommark']);
                            }
                        } else {
                            $_mapping_array[$employee_str] = array("service" => array(), "pro" => array(), "orders" => array(), "comment_score" => 0, "employee_arr" => $employee_arr, 'service_custommark' => array(), 'pro_custommark' => array());
                            if ($v['orderType'] == 1) {
                                array_push($_mapping_array[$employee_str]['service'], $v['id']);
                                array_push($_mapping_array[$employee_str]['service_custommark'], $v['custommark']);
                            } else {
                                array_push($_mapping_array[$employee_str]['pro'], $v['id']);
                                array_push($_mapping_array[$employee_str]['pro_custommark'], $v['custommark']);
                            }
                            $_mapping_array[$employee_str]['orders'] = $v;
                        }
                    }

                }
                return $_mapping_array;
            } else {
                return array();
            }
        }
    }

    /**
     * @param $order_number
     * @param $list
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 订单评论提交
     */
    function orderCommentCommit($order_number, $list, $feedback = "")
    {
        # 查询订单信息
        $orderInfo = Db::table("orders")->where(array("order_number" => $order_number))->find();
        $commissionService = new CommissionService();
        # 修改评论得分
        foreach ($list as $k => $v) {
            if ($v['comment_score'] == 1) {
                $v['comment_score'] = 5;
            } elseif ($v['comment_score'] == 2) {
                $v['comment_score'] = 3;
            } else {
                $v['comment_score'] = 0;
            }
            if ($k == 'biz') {
                foreach ($v['service'] as $sk => $sv) {
                    # 查询是否有这条评论
                    $serviceAlready = Db::table("evaluation_score")->field("id")->where(array("order_id" => $orderInfo['id'], "employee_id" => 0, "orderserver_id" => $sv, "type" => 1))->find();
                    if (!empty($serviceAlready)) {
                        Db::table("evaluation_score")->where(array("id" => $serviceAlready['id']))->update(array("score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "default_mark" => 2));
                    } else {
                        Db::table("evaluation_score")->insert(array("order_id" => $orderInfo['id'], "employee_id" => 0, "orderserver_id" => $sv, "type" => 1,
                            "create_time" => date("Y-m-d H:i:s"), "score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "biz_id" => $orderInfo['biz_id'],
                            "reffermark" => 1, "default_mark" => 2));
                    }
                    if ($v['service_custommark'][$sk] == 1) {
                        self::updateCommentRate(1, $sv);
                    }
                    # 完成门店获得服务好评任务
                    if($v['comment_score']==5 and !empty($orderInfo['biz_id'])){
                        \app\service\TaskService::finishDayTask($orderInfo['biz_id'],6);
                    }
                }
                foreach ($v['pro'] as $pk => $pv) {
                    # 查询是否有这条评论
                    $serviceAlready = Db::table("evaluation_score")->field("id")->where(array("order_id" => $orderInfo['id'], "employee_id" => 0, "orderserver_id" => $pv, "type" => 2))->find();
                    if (!empty($serviceAlready)) {
                        Db::table("evaluation_score")->where(array("id" => $serviceAlready['id']))->update(array("score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "default_mark" => 2));
                    } else {
                        Db::table("evaluation_score")->insert(array("order_id" => $orderInfo['id'], "employee_id" => 0, "orderserver_id" => $pv, "type" => 2,
                            "create_time" => date("Y-m-d H:i:s"), "score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "biz_id" => $orderInfo['biz_id'],
                            "reffermark" => 1, "default_mark" => 2));
                    }
                    if ($v['pro_custommark'][$pk] == 1) {
                        self::updateCommentRate(2, $pv);
                    }

                }
            } else {
                foreach ($v['employee_arr'] as $m => $n) {
                    foreach ($v['service'] as $sk => $sv) {
                        # 查询是否有这条评论
                        $serviceAlready = Db::table("evaluation_score")->field("id")->where(array("order_id" => $orderInfo['id'], "employee_id" => $n, "orderserver_id" => $sv, "type" => 1))->find();
                        if (!empty($serviceAlready)) {
                            Db::table("evaluation_score")->where(array("id" => $serviceAlready['id']))->update(array("score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "default_mark" => 2));

                        } else {
                            Db::table("evaluation_score")->insert(array("order_id" => $orderInfo['id'], "employee_id" => $n, "orderserver_id" => $sv, "type" => 1,
                                "create_time" => date("Y-m-d H:i:s"), "score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "biz_id" => $orderInfo['biz_id'],
                                "reffermark" => 1, "default_mark" => 2));
                        }
                        if ($v['service_custommark'][$sk] == 1) {
                            self::updateCommentRate(1, $sv);
                        }
                        # 这里修改服务的提成
                        $commissionService->commission($orderInfo['biz_id'], $sv, 1, $orderInfo['car_level'], 'comm', $n, $v['comment_score']);
                    }

                    foreach ($v['pro'] as $pk => $pv) {
                        # 查询是否有这条评论
                        $serviceAlready = Db::table("evaluation_score")->field("id")->where(array("order_id" => $orderInfo['id'], "employee_id" => $n, "orderserver_id" => $pv, "type" => 2))->find();
                        if (!empty($serviceAlready)) {
                            Db::table("evaluation_score")->where(array("id" => $serviceAlready['id']))->update(array("score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "default_mark" => 2));
                        } else {
                            Db::table("evaluation_score")->insert(array("order_id" => $orderInfo['id'], "employee_id" => $n, "orderserver_id" => $pv, "type" => 2,
                                "create_time" => date("Y-m-d H:i:s"), "score" => $v['comment_score'], "evalua_level" => $v['comment_score'], "biz_id" => $orderInfo['biz_id'],
                                "reffermark" => 1, "default_mark" => 2));
                        }
                        if ($v['pro_custommark'][$pk] == 1) {
                            self::updateCommentRate(2, $pv);
                        }
                        # 这里修改商品的提成
                        $commissionService->commission($orderInfo['biz_id'], $pv, 2, $orderInfo['car_level'], 'comm', $n, $v['comment_score']);
                    }
                }
            }

        }
        # 将订单改为已评论
        Db::table("orders")->where(array("order_number" => $order_number))->update(array("evaluation_status" => 2));
        if (!empty($feedback)) {
            Db::table("feedback")->insert(array("content" => $feedback, "biz_id" => $orderInfo['biz_id'], "status" => 1, "create_time" => date("Y-m-d H:i:s"), "order_number" => $order_number));
        }
        # 门店结算
        $commissionService->bizSettlement($order_number, $orderInfo['pay_type'], $orderInfo['order_price'], $orderInfo['voucher_price'], $orderInfo['biz_id'], 'modify',$orderInfo['is_online']);
        # 评论加积分
        $integral = IntegralService::integralSetItem('is_service_comment');
        $integralService = new IntegralService();
        $integralService->addMemberIntegral($orderInfo['member_id'], 4, $integral, 1);
        # 加订单消费积分
        $order_integral = intval($orderInfo['pay_price']);
        if ($order_integral > 0) {
            $integralService->addMemberIntegral($orderInfo['member_id'], 29, $order_integral, 1);
        }
        return true;
    }

    /**
     * @param $order_number
     * @param $list
     * @param string $feedback
     * @context 商家订单评论
     */
    function orderMerchantsCommentCommit($order_number, $list, $feedback = "")
    {
        $orderInfo = Db::table("merchants_order mo")
            ->field("mo.*,m.is_type")
            ->leftJoin("merchants m", "mo.merchants_id = m.id")
            ->where(array("mo.order_number" => $order_number))
            ->find();
        # 评论得分
        $comment_score = 0;
        if ($list[0]['comment_score'] == 1) {
            $comment_score = 5;
        } elseif ($list[0]['comment_score'] == 2) {
            $comment_score = 3;
        } else {
            $comment_score = 0;
        }
        if ($orderInfo['is_type'] != 3) {
            if ($comment_score == 0 and !empty($orderInfo['merchants_id'])) {
                # 差评扣除商家星级
                $merchantInfo = Db::table("merchants")->field("start")->where(array("id" => $orderInfo['merchants_id']))->find();
                if (!empty($merchantInfo) and $merchantInfo['start'] > 0) {
                    Db::table("merchants")->where(array("id" => $orderInfo['merchants_id']))->setDec('start', 1);
                }
            }
            TaskService::getTaskPaySucess(array("merchants_id" => $orderInfo['merchants_id']), '4');
        }
        # 修改评论
        Db::table("merchants_evaluation")->where(array("merchants_order_id" => $orderInfo['id']))->update(array("evalua_level" => $comment_score,
            "evalua_comment" => $feedback, "default_mark" => 2));

        return true;
    }


    /**
     * @param $server_id
     * @param $biz_id
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务排队数量
     */
    static function serverLineUp($server_id, $biz_id, $start_time, $end_time)
    {
        $orders = Db::table("order_server os,orders o")->field("o.order_type,o.order_appoin_time")->where(array("os.server_id" => $server_id, "os.status" => 1))
            ->where("os.order_number = o.order_number and o.order_status in (1,2,3,6) and o.order_type in (2,3) and o.biz_id = {$biz_id}")
            ->where("(o.order_type =2) 
            or (o.order_type =3 and o.order_appoin_time>='" . $start_time . "' and o.order_appoin_time<='" . $end_time . "')")
            ->order("o.order_appoin_time asc")
            ->select();

        return $orders;
    }

    /**
     * @param $time
     * @param $_occupy
     * @return bool
     * @context 筛选时间数组 找出
     */
    static function screenTimeSection($time, $_occupy)
    {
        foreach ($_occupy as $k => $v) {
            if (($time >= $v[0] and $time < $v[1]) or $time >= end($_occupy)[1]) {
                return false;
                break;
            }
        }
        return true;
    }

    /**
     * @param $type
     * @param $id
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException\
     * @context 修改服务或商品的好评率
     */
    static function updateCommentRate($type, $id)
    {
        if ($type == 1) {
            # 服务好评率
            $count = Db::table("evaluation_score es,order_server os")->where("es.orderserver_id = os.id and os.server_id = {$id} and es.default_mark = 2 and es.type =1")->count("es.id");
            $Good = Db::table("evaluation_score es,order_server os")->where("es.orderserver_id = os.id and os.server_id = {$id} and es.score=5 and es.default_mark = 2 and es.type =1")->count("es.score");
            if ($Good > 0) {
                $rate = getsPriceFormat($Good / $count) * 100;
                Db::table("service")->where(array("id" => $id))->update(array("service_evaluation" => $rate));
            }
        } else {
            # 商品好评率
            $count = Db::table("evaluation_score es,order_biz ob")->where("es.orderserver_id = ob.id and ob.biz_pro_id = {$id} and es.default_mark = 2 and es.type =2")->count("es.id");
            $Good = Db::table("evaluation_score es,order_biz ob")->where("es.orderserver_id = ob.id and ob.biz_pro_id = {$id} and es.score=5 and es.default_mark = 2 and es.type =2")->count("es.score");
            if ($Good > 0) {
                $rate = getsPriceFormat($Good / $count) * 100;
                Db::table("biz_pro")->where(array("id" => $id))->update(array("biz_pro_comment" => $rate));
            }
        }
        return true;
    }

    /**
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 预约超时订单退款
     */
    static function ordersOverTime()
    {
        $redis = new Redis();
//        $is_over = $redis->hGet("overtime", "overtime");
        $is_over = $redis->lock("overtime", 30);
        //$redis->hDel("overtime","overtime");
        //var_dump($is_over);
        if ($is_over) {
//            $redis->hSet("overtime", "overtime", 1);
            # 查询是否有超时订单
            $list = Db::table("orders o")->field("o.id,o.member_id,o.biz_pro_id,o.pay_type,o.pay_price,o.order_number,o.voucher_price,o.already")->where(array("o.order_type" => 3))
                ->where("o.order_appoin_time<='" . date("Y-m-d H:i:s", strtotime("-1 hour")) . "' and o.order_status in (1,2,3)")
                ->select();
            if (!empty($list)) {

                $payMent = new PayMent();
                foreach ($list as $k => $v) {
                    if ($v['already'] == 1) {
                        # 预约合并订单退款
                        $status = true;
                        # 查询服务详情
                        $serverList = Db::table("order_server")->field("id,pay_order_number,pay_type,price")->where(array("order_number" => $v['order_number'], "status" => 1))->select();
                        if (!empty($serverList)) {
                            foreach ($serverList as $sk => $sv) {
                                $res = self::refundTransfer($sv['pay_type'], $sv['pay_order_number'], $v['member_id'], $sv['price']);
                                if (!$res) {
                                    $status = false;
                                } else {
                                    # 将工单改为已退款状态
                                    Db::table("order_server")->where(array("id" => $sv['id']))->update(array("status" => 9));
                                }
                            }
                        }
                        if ($status) {
                            self::refundOrderDesposit($v['order_number'], $v['id'], $v['voucher_price'], $v['member_id'], $v['pay_price']);
                        }

                    } else {
                        $status = self::refundTransfer($v['pay_type'], $v['order_number'], $v['member_id'], $v['pay_price']);
                        if ($status) {
                            self::refundOrderDesposit($v['order_number'], $v['id'], $v['voucher_price'], $v['member_id'], $v['pay_price']);
                        }
                    }
                }
//                $redis->hDel("overtime", "overtime");
            }
//            else {
//                $redis->hDel("overtime", "overtime");
//            }
            $redis->unlock('overtime');
        }
//        $redis->hDel("overtime", "overtime");
    }

    static function refundTransfer($pay_type, $order_number, $member_id, $pay_price)
    {
        $payMent = new PayMent();
        if ($pay_type == 1) {
            # 微信退款
            $data = [
                'out_trade_no' => $order_number,
                'out_refund_no' => time(),
                'total_fee' => floatval($pay_price) * 100, //
                'refund_fee' => floatval($pay_price) * 100 * 0.5,
                'refund_desc' => '预约超时退款',
            ];
            $status = $payMent->wxRefund($data);
        } elseif ($pay_type == 2) {
            # 支付宝退款
            $data = [
                'out_trade_no' => $order_number,
                'refund_amount' => $pay_price * 0.5, // $v['pay_price']
            ];
            $status = $payMent->aliRefund($data);
        } else {
            # 余额退款
            $data = [
                "member_id" => $member_id,
                "pay_price" => $pay_price,
                "order_number" => $order_number
            ];
            $status = self::balanceRefund($data);
        }
        return $status;
    }

    /**
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 退款成功后处理订单
     */
    static function refundOrderDesposit($order_number, $order_id, $voucher_price, $member_id, $pay_price)
    {
        # 将订单改为超时作废
        Db::table("orders")->where(array("order_number" => $order_number))->update(array("order_status" => 4));
        # 判断是否使用卡券
        if ($voucher_price > 0) {
            Db::table("member_voucher")->where(array("order_id" => $order_id))->update(array("status" => 1));
        }
        # 删除这条消费记录
        $consump = Db::table("log_consump")->where(array("order_number" => $order_number))->find();
        if (!empty($consump)) {
            unset($consump['id']);
        } else {
            $consump['order_number'] = $order_number;
            $consump['member_id'] = $member_id;
        }
        $consump['consump_time'] = date("Y-m-d H:i:s");
        $consump['log_consump_type'] = 2;
        $consump['income_type'] = 15;
        $consump['consump_price'] = floatval($pay_price) * 0.5;
        Db::table("log_consump")->insert($consump);
        # 给代理退提成
        EalspellCommissionService::refundDeduct($order_number);
        return true;
    }

    static function balanceRefund($data)
    {
        $balanceService = new BalanceService();
        $balanceService->operaMemberbalance($data['member_id'], $data['pay_price'] * 0.5, 1);
        return true;
    }

    static function serviceInventDec($data, $biz_id)
    {

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                # 查询该服务是否有关联商品
                $relavice = Db::table("log_servicepro")->where(array("service_id" => $v['service_id']))->select();
                if (!empty($relavice)) {
                    foreach ($relavice as $rk => $rv) {
                        # 查询该商户库存是否足够
                        $invent = Db::table("biz_invent")->where(array("biz_id" => $biz_id, "biz_pro_id" => $rv['pro_id']))->find();
                        if (!empty($invent) and $invent['number'] > 0) {
                            # 判断剩余数量是否足够
                            if ($invent['number'] >= $rv['pro_num']) {
                                # 减去对应数量
                                Db::table("biz_invent")->where(array("biz_id" => $biz_id, "biz_pro_id" => $rv['pro_id']))->setDec("number", $rv['pro_num']);
                            } else {
                                # 直接为0
                                Db::table("biz_invent")->where(array("biz_id" => $biz_id, "biz_pro_id" => $rv['pro_id']))->update(array("number" => 0));
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
