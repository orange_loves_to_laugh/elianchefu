<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/20
 * Time: 20:32
 */

namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class PartnerService
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询合伙人设置
     */
    function partnerGive()
    {
        # 合伙人赠送信息
        $redis = new Redis();
        $info = $redis->hGetJson('partner_set', 'info');
//        if (empty($info)) {
            $info = Db::table('sysset')->field('desc')->where(array('tagname' => 'partner_set'))->find();
            if (!empty($info)) {
                $info = json_decode($info['desc'],true);
                if (!empty($info)) {
                    $info['membership_ratio']=0.04;
                    foreach ($info['prise_info'] as $k => $v) {
                        if ($v['type'] == 1 or $v['type'] == 2) {
                            # 商品/服务卡券
                            $info['prise_info'][$k]['detailInfo'] = Db::table('card_voucher')->where(array('id' => $v['id']))->find();
                            $info['prise_info'][$k]['detailInfo']['card_times'] = date('Y-m-d', strtotime('+ ' . $v['card_time'] . ' days'));
                        } else {
                            # 赠送红包
                            $info['prise_info'][$k]['detailInfo'] = Db::table('redpacket_detail')->where(array('id' => $v['id']))->find();
                            $info['prise_info'][$k]['detailInfo']['voucher_overs'] =  date('Y-m-d', strtotime('+ ' . $info['prise_info'][$k]['detailInfo']['voucher_over'] . ' days'));
                        }
                    }
                }
                $redis->hSetJson('partner_set', 'info', $info);
            }
//        }
        return array('status' => true, 'msg' => '查询成功', 'info' => $info);
    }
}
