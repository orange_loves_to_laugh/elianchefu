<?php


namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class MerchantService
{
    /**
     * @param $mark
     * @return array|bool|mixed|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取商户分类
     */
    function merchantCate($mark,$is_type)
    {
        if($mark=='all'){
            return $this->cateAll($is_type);
        }elseif($mark=='more'){
            return $this->cateLevel($is_type);
        }

    }

    /**
     * @return array|bool|mixed|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商户全部分类
     */
    function cateAll($is_type)
    {
        $_redis = new Redis();
        //$_redis->hDel("merchants_cate".$is_type,"all");
        $list = $_redis->hGetJson("merchants_cate".$is_type,"all");
        if(empty($list)){
            $list = Db::table("merchants_cate")->where(array("status"=>1,"type"=>$is_type))->order(array("level"=>"asc","pid"=>'asc',"sort_num"=>'desc'))->select();
            if(!empty($list)){
                $_redis->hSetJson("merchants_cate".$is_type,"all",$list);
            }
        }
        return $list;
    }

    /**
     * @return array
     * @context 商户分类
     */
    function cateLevel($is_type,$pid=0)
    {
        $list = $this->cateAll($is_type);
        if(empty($list)){
            return [];
        }
        $_return = array();
        foreach($list as $k=>$v){
            if($v['level']==1){
                $_return[$v['id']]['id']=$v['id'];
                $_return[$v['id']]['name']=$v['title'];
                $_return[$v['id']]['icon']=$v['icon'];
            }else{
                if(empty($_return[$v['pid']]['child'])){
                    $_return[$v['pid']]['child']=array(array("id"=>0,"name"=>"全部"),array("id"=>$v['id'],"name"=>$v['title']));
                }else{
                    array_push($_return[$v['pid']]['child'],array("id"=>$v['id'],"name"=>$v['title']));
                }
            }
        }
        if($pid != 0){
            $_return = $_return[$pid]['child'];
        }
        return array_values($_return);
    }

    /**
     * @param $is_type
     * @param $cate_id
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取某一高级分类下的子级分类
     */
    function getsCateChild($is_type,$cate_id){
        $list  = Db::table("merchants_cate")->field("id")->where(array("pid"=>$cate_id,"level"=>2,"type"=>$is_type))->select();
        if(empty($list)){
           return array();
        }
        return $list;
    }

    /**
     * @param int $is_type
     * @param int $limit
     * @param bool $randomStatus
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 按等级查询商家
     */
    function LevelMerchant($level,$lat,$lon,$is_type=2,$limit=0,$randomStatus=false,$where=null,$where_toutiao='',$pageNum=0)
    {
        $list =  Db::table("merchants m")
            ->field("distinct m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
           start_time,end_time,temporary_close,close_time")
            ->leftJoin("merchants_cate_relation mcr","mcr.merchants_id = m.id ")
            ->where(array("status"=>1,"is_type"=>$is_type,"level"=>$level))
//            ->where("(
//                (start_time!='00:00' and end_time != '00:00') and
//                (
//                    (UNIX_TIMESTAMP(start_time)>UNIX_TIMESTAMP(end_time) and start_time<='".date("H:i:s")."' and end_time>='".date("H:i:s")."')
//                    or
//                    (UNIX_TIMESTAMP(start_time)<=UNIX_TIMESTAMP(end_time) and
//                        (
//                             (end_time>'".date("H:i:s")."') or (start_time<='".date("H:i:s")."')
//                         )
//                    )
//                )
//            )
//            or
//            (start_time='00:00' and end_time = '00:00')")
//            ->where("temporary_close<=0 or (temporary_close>0 and (close_time>'".date("Y-m-d H:i:s")."'
//            or date_add(close_time,interval temporary_close hour))<'".date("Y-m-d H:i:s")."')")
            ->where("m.id != 1 and m.id != 2 and m.id != 1387 and m.id != 1388")
            ->where("m.start_time is not null and m.end_time is not null")
            ->where($where_toutiao)
            ->where($where);
        if($randomStatus){
            $list=$list->orderRaw('rand()');
        }else{
            $list=$list->order("distance asc");
        }
        if($limit>0){
            $list = $list->limit($pageNum,$limit);
        }
        $list=$list->select();

        return $list;
    }

    /**
     * @param $data
     * @return bool
     * @context 添加足迹记录
     */
    function addLogConsump($data)
    {
        if(!empty($data)){
            Db::table("merchants_logconsump")->insert(array(
                "member_id"=>$data['member_id'],
                "merchants_id"=>$data['merchants_id'],
                "price"=>$data['price'],
                "pract_price"=>$data['pract_price'],
                "coupon_price"=>$data['coupon_price'],
                "order_number"=>$data['order_number'],
                "pay_type"=>$data['pay_type'],
                "create_time"=>date("Y-m-d H:i:s"),
                "status"=>1,
            ));
            return true;
        }else{
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除足迹
     */
    function delLogConsump($id)
    {
        Db::table("merchants_logconsump")->where(array("id"=>$id))->delete();
        return true;
    }

    static function memberMerchantsOrderService($member_id,$status)
    {
        $list = Db::table("merchants_order mo")
            ->field("mo.order_number,mo.actual_price as order_price,mo.create_time,mo.pay_type,m.title,m.tel,
            m.level,m.lon,m.lat,m.address,mod.product_id,mp.title product_title,mod.duration")
            ->join("merchants_orderdetail mod","mod.order_id = mo.id")
            ->leftJoin("merchants_pro mp","mp.id = mod.product_id and mod.product_type!=4")
            ->join("merchants m","m.id = mo.merchants_id")
            ->where(array("mo.member_id"=>$member_id,"mo.order_status"=>$status))
            ->where("mo.pay_type=1 or mo.pay_type=6")
            ->select();
        return $list;
    }

    static function merchantsFullDiscount($merchants_id)
    {
        $list = Db::table("merchants_voucher")
            ->field("ROUND(min_consume) min_consume,ROUND(price) price")
            ->where(array("merchants_id"=>$merchants_id,"status"=>1,'dou_mark'=>2))->where("surplus_num",">",0)
            ->where("start_time<='".date("Y-m-d H:i:s")."' and end_time>='".date("Y-m-d H:i:s")."' and min_consume>0")
            ->order("price asc")->limit(3)->select();
        return $list;
    }

    static function merchantsRecommendPro($merchants_id,$num=0)
    {
        $list = Db::table("merchants_pro")
            ->where(array("merchants_id"=>$merchants_id,"recommend_status"=>1,"status"=>1))
            ->where("start_time<='" . date("Y-m-d") . "' and end_time >='" . date("Y-m-d") . "' and (num - sold_num)>0")
            ->order("id desc");
        if($num>0){
            $list = $list->limit($num);
        }
        $list=$list->select();
        return $list;
    }

}
