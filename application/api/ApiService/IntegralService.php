<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/8
 * Time: 17:55
 */

namespace app\api\ApiService;


use think\Db;

class IntegralService
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查询积分设置
     */
    static function integralSet()
    {
        $list = Db::table("integral_set")->select();
        return array_column($list, null, 'integral_type');
    }

    /**
     * @param $mark
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 返回对应类型的积分设置
     */
    static function integralSetItem($mark)
    {
        $list = self::integralSet();
        if ($mark == 'shareService' or $mark == 'shareVip') {
            $mark = 'share_register_integra';
        }
        return $list[$mark]['integral'];
    }

    /**
     * @param $member_id
     * @param $integral_type
     * @param $integral_num
     * @param $exec_type
     * @param array $data
     * @return bool
     * @throws \think\Exception
     * @context 用户增加或减少积分
     */
    function addMemberIntegral($member_id, $integral_type, $integral_num, $exec_type, $data = array())
    {
        if (!empty($integral_num) and $integral_num > 0) {
            if ($exec_type == 1) {
                # 获得积分
                Db::table("member_mapping")->where(array("member_id" => $member_id))->setInc('member_integral', $integral_num);
            } else {
                # 使用积分
                Db::table("member_mapping")->where(array("member_id" => $member_id))->setDec('member_integral', $integral_num);
            }
            # 增加积分记录
            $_log = array("integral_number" => $integral_num, "integral_source" => $integral_type, "integral_type" => $exec_type, "integral_time" => date("Y-m-d H:i:s"),
                "member_id" => $member_id);
            if (!empty($data)) {
                if (key_exists('biz_id', $data)) {
                    $_log['biz_id'] = $data['biz_id'];
                }
                if (key_exists('biz_pro_id', $data)) {
                    $_log['biz_pro_id'] = $data['biz_pro_id'];
                }
                if (key_exists('source_mark', $data)) {
                    $_log['source_mark'] = $data['source_mark'];
                }
            }
            Db::table("log_integral")->insert($_log);
        }
        return true;
    }

    /**
     * @return array
     * @context 积分规则
     */
    static function integralRule()
    {

        return array(
           "jfhd"=>array(
               "新用户首次注册赠送30积分；",
               "首次成功下载“E联车服APP”赠送60积分；",
               "完成应用内实名认证，赠送50积分；",
               "完成应用内的任务、游戏、签到、摇奖等活动，获得赠送随机积分；",
               "关注“E联车服”微信公众号，赠送20积分；",
               "分享服务或商品至微信，每次随机获得1-3积分；",
               "分享应用内联盟商家海报或链接至微信好友或朋友圈，并大于1次以上打开链接，随机获得赠送1-5积分；",
               "每推荐一名新用户完成注册“E联车服”，获得赠送30积分；",
               "分享“E联车服”应用内的俱乐部文字至微信朋友圈，每次获得随机赠送1-3积分；",
               "办理会员、升级会员或会员储值，根据活动获得积分赠送；",
               "通过连接邀请新用户办理会员，将根据办理会员金额的30%获得积分；",
               "每次服务完成后，对服务质量进行评价，每次评价获得3积分奖励；",
               "评论后转发微信朋友圈，将获得5积分赠送奖励；",
               "分享应用内的二手车至微信朋友圈，每辆车每次获得2积分；",
               "到达应用内指定的联盟商家消费，将获得对应消费金额对应的积分奖励。",
           ),
            "jfsy"=>array(
                "成为E联车服平台会员后，可使用积分在应用内兑换现金余额、服务抵用券、现金满减券、消费满减券；",
                "会员积分可使用于为本地联盟商家增加热度使用，帮助本地商家上热门，每次助力将消耗2积分；",
                "会员积分可使用于“E联车服”平台陆续待上线的其他兑换服务。",
            ),
            "jfsm"=>array(
                "“E联车服”应用内积分于现金无关，无法等值兑换；",
                "平台内积分制针对“E联车服”会员可进行兑换使用，非会员不可使用；",
                "会员积分于会员身份绑定，会员过期或注销等情况下积分不可使用；",
                "积分不可赠与他人或转借；",
                "会员积分不定期清零，平台内积分清零时不予通知用户；",
                "应用内积分如遇自然灾害或不可抗拒因素造成不可用时，“E联车服”不予承担责任；",
                "积分可兑换的商品或相关服务不定期进行更换，具体兑换以当前应用内展示为准；",
                "如遇用户采用非法或以不正当手段获取积分的，平台有权收回所有积分。",
            )
        );
    }

    /**
     * @param int $class_id
     * @param string $priceOrder
     * @param string $canExchange
     * @param string $memberIntegral
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 积分兑换列表
     */
    static function integralExchange($class_id = 0, $priceOrder = "", $canExchange = "", $memberIntegral = "", $id = '',$show_mark=0)
    {
        $order = "ie.sort_number desc";
        if (!empty($priceOrder)) {
            $priceOrder = $priceOrder == 'down' ? "desc" : "asc";
            $order = "ie.exchange_integral {$priceOrder}";
        }
        $where = "";
        if (!empty($class_id)) {
            if($class_id==1){
                # 汽车服务抵用券
                $where .= " and iep.exchange_type in (2,10,11,12,13,14,15,16)";
            }elseif($class_id==2){
                # 联盟商家抵用券
                $where .= " and iep.exchange_type = 7";
            }elseif($class_id==3){
                # 余额
                $where .= " and iep.exchange_type = 1";
            }elseif($class_id==4){
                # 本地生活抵用券
                $where .= " and iep.exchange_type = 8";
            }elseif($class_id==5){
                # 本地生活抵用券
                $where .= " and iep.exchange_type = 9";
            }

        }
        if (!empty($canExchange)) {
            $where .= " and ie.exchange_integral <= {$memberIntegral}";
        }
        if (!empty($id)) {
            $where .= " and ie.id = {$id}";
        }
        if($show_mark==0) {
            $list = Db::table("integral_exchange ie,integral_exchange_prize iep")
                ->field("ie.*,convert(iep.price,decimal(10,2)) price,iep.exchange_type,iep.voucher_type,
                iep.spoil_id,iep.exchange_title prize_title,iep.exchange_num,exchange_indate,ie.limit_num")
                ->where("ie.status=1 and ie.surplus_num>0  and start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "' and iep.integral_exchange_id=ie.id {$where}")
                ->order($order)
                ->select();
        }else{
            $list = Db::table("integral_exchange ie,integral_exchange_prize iep")
                ->field("ie.*,convert(iep.price,decimal(10,2)) price,iep.exchange_type,iep.voucher_type,
                iep.spoil_id,iep.exchange_title prize_title,iep.exchange_num,exchange_indate,ie.limit_num")
                ->where("ie.status=1 and ie.surplus_num>0  and start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "' and iep.integral_exchange_id=ie.id {$where}")
                ->order($order)
                ->limit(4)
                ->select();
        }
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['label'] = explode('，', $v['label']);
                $tips = '';
                if($v['exchange_type'] >= 7 and $v['exchange_type'] <= 16){
                    #类型为卡券时 上 card_voucher  查下卡券id 存上
                    $moneyOff  = Db::table('merchants_voucher')->where(array('id'=>$v['spoil_id']))->value('min_consume');
                    $tips = '单笔消费金额满'.priceFormat($moneyOff).'元可抵用';
                }
                $list[$k]['tips'] = $tips;
            }
        }
        return $list;
    }

}
