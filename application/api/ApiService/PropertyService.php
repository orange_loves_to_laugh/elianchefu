<?php


namespace app\api\ApiService;


use app\businessapi\controller\Message;
use app\service\SmsCode;
use Redis\Redis;
use think\Db;

class PropertyService
{
    static function getsPropertyList($params)
    {
        if(empty($params['lon']) or empty($params['lat']) or empty($params['province']) or empty($params['city'])){
            return array("status" => false, "msg" => "暂无物业信息");
        }
        $where = "pn.id > 0 ";
        $length = 10 ;
        if (empty($params['page'])) {
            $page = 0;
        }
        $page_num = $page * $length;
        if(!empty($params['search'])){
            $search =implode("%",mb_str_splits($params['search'])) ;
            $where.=" and pn.nh_title like '%".$search."%'";
        }
        $list = Db::table("property_neighbourhood pn")
            ->field("distinct pn.id nh_id, pn.*,round(st_distance_sphere(point({$params['lon']},{$params['lat']}),point(pn.nh_lon,pn.nh_lat))/1000,2) AS distance")
            ->where(array("pn.status"=>1,"pn.del_status"=>2))
            ->where("pn.province like '%".$params['province']."%' and pn.city like '%".$params['city']."%'")
            ->where($where)
            ->order("distance asc")
            ->limit($page_num, $length)->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                $list[$k]['thumb'] = explode("*",$v['nh_detailimg'])[0];
                $list[$k]['voucher_list']=MerchantService::merchantsFullDiscount($v['merchants_id']);
            }
            $flag = count($list) == 10 ? true : false;
            return array("status" => true, "list" => $list, "flag" => $flag);
        }else{
            return array("status" => false, "msg" => "暂无商户信息");
        }
    }

    static function getsPropertyDetail($params)
    {
        if(empty($params['id']) or empty($params['lon']) or empty($params['lat'])){
            return array("status"=>false);
        }

        $info = Db::table("property_neighbourhood pn")
            ->field("distinct pn.id nh_id, pn.*,round(st_distance_sphere(point({$params['lon']},{$params['lat']}),point(pn.nh_lon,pn.nh_lat))/1000,2) AS distance,
            m.keywords")
            ->join("merchants m","m.nh_id = pn.id")
            ->where(array("pn.status"=>1,"pn.del_status"=>2))
            ->where("pn.id = {$params['id']}")
            ->find();
        $info['banner'] = explode("*",$info['nh_detailimg']);
        if(!empty($info['keywords'])){
            $info['keywords'] = explode('，',$info['keywords']);
        }else{
            $info['keywords'] = array();
        }
        if(!empty($member_id)){
            # 查询这个用户分享这个商家分享了几次
            $log_share = Db::table("log_share")->where(array("member_id"=>$member_id,"type"=>10,"other_id"=>$params['id']))->count();
            $info['share_num']=$log_share+1;
        }
        return array("status"=>true,"info"=>$info);
    }

    static function getsPropertyBuild($params)
    {
        $list = array();
        $build_array = self::BuildArr($params['nh_id'],1);
        if(empty($build_array)){
            return array("status"=>false);
        }else{
            $list = array_column($build_array,NULL,'id');
            # 查询单元
            $unit_array = self::unitArr($params['nh_id'],1);
            if(empty($unit_array)){
                return array("status"=>false);
            }else{
                foreach($unit_array as $k=>$v){
                    $list[$v['building_id']]['unit_array'][$v['id']] = $v;
                }
                # 查询房屋
                $house_array = self::houseArr($params['nh_id'],1);
                if(!empty($house_array)){
                    foreach($house_array as $k=>$v){
                        if(empty($list[$v['building_id']]['unit_array'][$v['unit_id']]["floor_array"][$v['level']])){
                            $list[$v['building_id']]['unit_array'][$v['unit_id']]["floor_array"][$v['level']] = array("id"=>$v['level'],"title"=>$v['level'],"house_array"=>array($v['id']=>$v));
                        }else{
                            $list[$v['building_id']]['unit_array'][$v['unit_id']]["floor_array"][$v['level']]["house_array"][$v['id']] = $v;
                        }

                    }
                }
            }
        }
        return array("status"=>true,"data"=>$list);
    }

    /**
     * @param $id
     * @param $type
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取楼房
     */
    static function BuildArr($id,$type)
    {
        $list =array();
        if($type==1){
            # 通过园区id获取所有楼号
            $list = Db::table("property_building")->field("id,neighbourhood_id,build_title title,property_price")->where(array("neighbourhood_id"=>$id,"del_status"=>2))->select();
        }
        return $list;
    }

    static function unitArr($id,$type)
    {
        $list = array();
        if($type==1){
            # 通过园区id获取所有单元
            $list = Db::table("property_unit")->field("id,title,building_id")->where(array("neighbourhood_id"=>$id))->select();
        }
        return $list;
    }
    static function houseArr($id,$type)
    {
        $list = array();
        if($type==1){
            # 通过园区id 获取所有房屋
            $list = Db::table("property_house")->field("id,building_id,unit_id,level,house_code title,area,member_title")->where(array("neighbourhood_id"=>$id,"del_status"=>2))->select();
        }
        if(!empty($list)){
            foreach($list as $k=>$v){
                if(!empty($v['member_title'])){
                    $list[$k]['member_name_hide'] =self::substr_cut($v['member_title']);
                }

            }
        }
        return $list;
    }
    /**
     * 只保留字符串首尾字符，隐藏中间用*代替（两个字符时只显示第一个）
     * @param string $user_name 姓名
     * @return string 格式化后的姓名
     */
    static function substr_cut($user_name){
        $strlen     = mb_strlen($user_name, 'utf-8');
        $firstStr     = mb_substr($user_name, 0, 1, 'utf-8');
        $lastStr     = mb_substr($user_name, -1, 1, 'utf-8');
        return $strlen == 2 ? str_repeat('*', mb_strlen($user_name, 'utf-8') - 1). $lastStr : str_repeat("*", $strlen - 1) . $lastStr;
    }

    static function getsPropertyCanUseVoucher($params)
    {
        # 查询用户是否是会员
        $MemberService = new MemberService();
        $memberInfo = $MemberService->MemberInfoCache($params['member_id']);
        if($memberInfo['member_level_id']<=0){
            return array("status"=>false,"msg"=>"非会员不可使用卡券");
        }
        $list=Db::table("member_voucher")
            ->field("*,date(`create`) expiration_time")
            ->where(array("member_id"=>$params['member_id'],"status"=>1,"voucher_type"=>9))
            ->where("money_off<={$params['price']}")
            ->where("(merchants_id>0 and merchants_id = {$params['merchants_id']}) or (merchants_id=0)")
            ->select();
        if(!empty($list)){
            $VoucherService=new VoucherService();
            foreach($list as $k=>$v){
                $Desc = $VoucherService->voucherTypeDesc($v);
                $list[$k]['TypeTitle'] = $Desc['TypeTitle'];
                $list[$k]['voucherDesc'] = $Desc['voucherDesc'];
                $list[$k]['voucher_source_title'] = $Desc['voucherSource'];
            }
        }
        return array("status"=>true,"list"=>$list,"num"=>count($list));
    }

    static function makePropertyOrder($params)
    {
        $_redis = new Redis();
        $is_repeat = $_redis->lock("propertyPay".$params['orderNumber'],60);
        if(!$is_repeat){
            return array("status"=>true);
        }
        # 判断是否重复下单
        $is_repeat =  Db::table('merchants_order')->field("id")->where(array('order_number'=>$params['orderNumber']))->find();
        if(!empty($is_repeat)){
            return array("status"=>true,'orderId'=>$is_repeat['id'],'order_number'=>$params['orderNumber'],'voucherInfo'=>$params['voucherInfo']);
        }
        # 余额支付判断账户余额是否足够
        if($params['cash_type']==5){
            $nowBalance = Db::table("member_mapping")->where(array("member_id"=>$params['member_id']))->value("member_balance");
            if(floatval($nowBalance)<floatval($params['actual_price'])){
                return array("status"=>false,"msg"=>"账户余额不足");
            }
        }
        $voucherInfo = json_decode($params['voucherInfo'],true);
        $houseInfo = json_decode($params['house_info'],true);
        $time = date("Y-m-d H:i:s");
        # 获取物业信息
        $info = Db::table("property_neighbourhood")->where("id = {$params['nh_id']}")->find();
        $commission_price=0;
        if(!empty($info['draw_prop'])){
            $commission = $info['draw_prop'];
        }


        # 增加微信支付宝收款手续费
        if($params['cash_type']==1 or  $params['cash_type']==2){
            SubsidyService::addExpenses(array("type_id"=>2,"price"=>$params['actual_price']*0.006));
        }
        $member = array();
        if(!empty($params['member_id'])){
            $MemberService =new MemberService();
            $member = $MemberService->MemberInfoCache($params['member_id']);
        }
        $is_type  = 1;
        if($params['cash_type']==5){
            # 余额支付是否有卡券
            if(!empty($voucherInfo)){
                # 卡券是物业卡券还是平台卡券
                if(!empty($voucherInfo['merchants_id'])){
                    # 物业卡券 按实际支付金额抽成
                    $commission_price = $params['actual_price']*$commission;
                    $balance = $params['actual_price'] - $commission_price;
                    $is_type =1;//平台
                }else{
                    # 平台卡券按总金额抽成
                    $commission_price = $params['total_price']*$commission;
                    $balance = $params['total_price'] - $commission_price;
                    $is_type =2;//商家优惠券
                    Db::table('merchants_voucher')->where(array('id'=>$voucherInfo['voucher_id']))->setInc('use_num',1);
                }
                #消掉卡券  把卡券改为核销状态
                Db::table('member_voucher')->where(array('id'=>$voucherInfo['id']))->update(array('status'=>2));
            }else{
                $commission_price = $params['actual_price']*$commission;
                $balance = $params['actual_price'] - $commission_price;
            }
        }else{
            # 其他方式支付按实际支付总金额抽成
            $commission_price = $params['actual_price']*$commission;
            $balance = $params['actual_price'] - $commission_price;
        }
        $orderId = Db::table('merchants_order')->insertGetId(array(
            'order_number'=>$params['orderNumber'],
            'total_price'=>$params['total_price'],//需支付金额
            'actual_price'=>$params['actual_price'],//实际支付金额
            'commission_price'=>$commission_price, //平台抽成金额  只有用户下单才有  商户的抽成比列*实际支付金额
            'merchants_id'=>$params['merchants_id'],
            'pay_type'=>$params['order_type'] ,//1线上预约，2用户付款-用户端点击扫码按钮进行支付 用户直接 给商家 付款，3扫码-用户扫码支付，4商家扫码-商家扫码收款5 商家推广 下的订单
            'cash_type'=>$params['cash_type'],//支付方式
            'member_id'=>$params['member_id'],//用户id
            'member_name'=>$member['member_name'],//用户姓名
            'member_phone'=>$member['member_phone'],//用户手机号
            'order_status'=>1,//订单状态 扫码支付 传2
            'checkout_time'=>$time,
            'employee_sal'=>$info['assign_employee'],//商户关联员工id
        ));
        #订单详情
        Db::table("merchants_orderdetail")->insertGetId(array(
            'order_number'=>$params['orderNumber'],
            'order_id'=>$orderId,
            'product_type'=>$params['product_type'], //1服务 2商品 3.商户升级 4.物业订单
            'product_id'=>empty($houseInfo['house_id'])?0:$houseInfo['house_id'],  //商品id或服务id
            'duration' =>$params['duration'],                                       // 缴费时长
        ));
        if($params['cash_type']==5){
            # 余额支付是否有卡券
            if(!empty($voucherInfo)){
                # 增加商家优惠券使用数量 增加卡券使用记录  将用户卡券核销
                $a = Db::table('merchants_voucher_order')->insertGetId(array(
                    'voucher_id'=>$voucherInfo['voucher_id'],//为平台 优惠券时是 (现金红包详情id   redpacket_detail )    商户优惠券是merchants_voucher表的id
                    'voucher_type'=>$is_type,//优惠券类型：1平台优惠券 2商户优惠券
                    'order_id'=> $orderId,//订单id
                    'price'=>$voucherInfo['voucher_cost'], //低值金额
                    'member_id'=>$params['member_id'],//用户id

                ));
            }
        }
        # 增加用户房产
        $member_house = Db::table("property_memberhouse")->where(array("house_id"=>$houseInfo['house_id'],"member_id"=>$params['member_id'],"del_status"=>2))->find();
        if(empty($member_house)){
            Db::table("property_memberhouse")->insert(array("nh_id"=>$info['id'],"nh_title"=>$info['nh_title'],"build_id"=>$houseInfo['build_id'],
                "build_title"=>$houseInfo['build_title'],"unit_id"=>$houseInfo['unit_id'],"unit_title"=>$houseInfo['unit_title'],"floor_id"=>intval($houseInfo['floor_id']),
                "floor_title"=>$houseInfo['floor_title'],"house_id"=>$houseInfo['house_id'],"house_title"=>$houseInfo['house_title'],"area"=>$houseInfo['area'],
                "payfee_time"=>date("Y-m-d H:i:s"),"duration"=>$params['duration'],"member_id"=>$params['member_id'],"create_time"=>date("Y-m-d H:i:s")));
        }else{
            Db::table("property_memberhouse")->where(array("id"=>$member_house['id']))->update(array("payfee_time"=>date("Y-m-d H:i:s"),"duration"=>$params['duration']));
        }
        # 修改房产缴费到期时间
        $house_Information = Db::table("property_house")->where(array("id"=>$houseInfo['house_id']))->find();
        if(!empty($house_Information['price_endtime'])){
            $end_time = date("Y-m-d H:i:s",strtotime("+{$params['duration']} months",strtotime($house_Information['price_endtime'])));
        }else{
            $end_time = date("Y-m-d H:i:s",strtotime("+{$params['duration']} months"));
        }
        Db::table("property_house")->where(array("id"=>$houseInfo['house_id']))->update(array("price_time"=>$time,"price_endtime"=>$end_time));
        # 增加用户缴费记录
        $log_voucher_id = 0;
        $log_voucher_price = 0;
        if($params['cash_type']==5){
            # 余额支付是否有卡券
            if(!empty($voucherInfo)){
                $log_voucher_id = $voucherInfo['id'];
                $log_voucher_price = $voucherInfo['voucher_cost'];
            }
        }
        Db::table("property_payfee")->insert(array("member_id"=>$params['member_id'],"house_id"=>$houseInfo['house_id'],"total_price"=>$params['total_price'],
            "pract_price"=>$params['actual_price'],"voucher_price"=>$log_voucher_price,"voucher_id"=>$log_voucher_id,"nh_id"=>$info['id'],"create_time"=>$time,
            "pay_type"=>$params['cash_type'],"duration"=>$params['duration'],"order_number"=>$params['orderNumber']));
        # 增加用户消费记录
        Db::table('log_consump')
            ->insert(array(
                'member_id' => $params['member_id'],
                'consump_price' => $params['actual_price'],
                'consump_pay_type' => $params['cash_type'],
                'consump_time' => date("Y-m-d H:i:s"),
                'log_consump_type' => 1,
                'consump_type' => 1,
                'income_type' => 21, // 物业缴费
                'biz_id'=>$params['merchants_id'],
                'order_number'=>$params['orderNumber']
            ));
        # 增加商户足迹记录
        $addLogConsump = new MerchantService();
        $_addLogConsump =array(
            'member_id'=>$params['member_id'],
            'merchants_id'=>$params['merchants_id'],
            'price'=>$params['total_price'],
            'pract_price'=>$params['actual_price'],
            'coupon_price'=>(intval($params['total_price']) - intval($params['actual_price'])),
            'order_number'=>$params['orderNumber'],
            'pay_type'=>$params['cash_type'],
        );
        $addLogConsump->addLogConsump($_addLogConsump);
        # 增加商户收款记录 # 增加商户余额
        Db::table('merchants')->where(array('id'=>$params['merchants_id']))->setInc('balance',$balance);
        Db::table('merchants_income')->insert(
            array(
                'merchants_id'=>$params['merchants_id'],
                'member_id'=>$params['member_id'],
                'pay_type'=>$params['cash_type'],
                'price'=>$balance,
                'order_number'=>$params['orderNumber'],
                'employee_sal'=>$info['assign_employee'],//商户关联员工id
            ));
        # 平台物业费抽成收入
        if($commission_price>0){
            $BizIncome = array(
                'biz_id' => $params['merchants_id'],
                'biz_income' => $commission_price,
                'pay_type' => $params['cash_type'],
                'member_id'=>$params['member_id'],
                'income_type'=>18,
                'order_number'=>$params['orderNumber'],
            );
            addBizIncome($BizIncome);
        }
        # 余额支付扣款
        if($params['cash_type'] == 5){
            #减掉对应会员余额
            $balanceService = new BalanceService();
            $balanceService->operaMemberbalance($params['member_id'],$params['actual_price'],2,[]);
        }
        # 用户消费送积分
        if($info['give_integral'] == 1){
            $data['merchants_id'] = $params['merchants_id'];
            $integralService = new IntegralService();
            $integralService->addMemberIntegral($params['member_id'],'30',intval($params['actual_price']),1,$data);
        }
        # 合伙人消费收益
        \app\businessapi\ApiService\OrderService::getRecommendPartner($params);
        # 添加评论订单
        \app\businessapi\ApiService\OrderService::merchantsEvaluation($orderId);
        # 发送语音通知
        # 商户通知
        #发送语音提醒
        $data['pay_type'] = $params['cash_type'];
        $data['price'] = $params['actual_price'];
        $params['price'] = $params['actual_price'];
        $message= new Message();
        $a = $message->send($params['merchants_id'],1,$data);
        # 用户通知
        if(!empty($params['member_id'])){
            $data['pay_type'] = $params['cash_type'];
            $data['price'] = $params['actual_price'];
            $data['member_id'] = $params['member_id'];
            $a = $message->user_send($params['merchants_id'],1,$data);

            #发送短信提醒
            $SmsCode = new SmsCode();
            if($params['cash_type'] == 5){
                $content =  $message->getContent($params['merchants_id'],$data);
                $SmsCode->Ysms($member['member_phone'],$content,false);
            }
            #清除用户缓存
            $changeMemberInfo = new \app\api\ApiService\MemberService();
            #更新用户信息
            $changeMemberInfo->changeMemberInfo($params['member_id']);
        }
        $_redis->unlock("propertyPay".$params['orderNumber']);
        return array("status"=>true,'orderId'=>$orderId,'order_number'=>$params['orderNumber'],'voucherInfo'=>$voucherInfo,
            "paytime"=>date("Y-m-d"),"endtime"=>date("Y-m-d",strtotime($end_time)));
    }

    static function getsMyHouse($params)
    {
        $list = Db::table("property_memberhouse pm")
            ->field("pm.*,convert(pm.area,decimal(10,2)) as area,pb.property_price,(pb.property_price*pm.area) total_price,ph.member_title")
            ->join("property_building pb","pb.id = pm.build_id")
            ->join("property_house ph","ph.id = pm.house_id")
            ->where(array("pm.member_id"=>$params['member_id'],"pm.del_status"=>2))->order("pm.id desc")->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                $list[$k]['payfee_times'] = date("Y-m-d",strtotime($v['payfee_time']));
                $list[$k]['duration_title'] = lang("duration")[$v['duration']];
                if(!empty($v['member_title'])){
                    $list[$k]['member_name_hide'] = self::substr_cut($v['member_title']);
                }else{
                    $list[$k]['member_name_hide'] = "";
                }

                $list[$k]['info'] = Db::table("property_neighbourhood")->where(array("id"=>$v['nh_id']))->find();
            }
        }
        return array("status"=>true,"list"=>$list);
    }

    static function getsPayFeeLog($params)
    {
        $where = null;
        if(!empty($params['house_id'])){
            $where = "pp.house_id = {$params['house_id']}";
        }
        $list = Db::table("property_payfee pp")
            ->field("PP.*,pm.nh_title,pm.build_title,pm.unit_title,pm.floor_title,pm.house_title,pm.area")
            ->leftJoin("property_memberhouse pm","pp.house_id = pm.house_id and pm.member_id = {$params['member_id']}")
            ->where(array("pp.member_id"=>$params['member_id']))
            ->where($where)
            ->order("pp.id desc")
            ->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                $list[$k]['payfee_times'] = date("Y-m-d",strtotime($v['create_time']));
                $list[$k]['duration_title'] = lang("duration")[$v['duration']];
                $list[$k]['pay_type_title'] =getpay_type($v['pay_type']) ;
            }
        }
        return array("status"=>true,"list"=>$list);
    }

    static function removeHouses($params)
    {
        Db::table("property_memberhouse")->where(array("id"=>$params['id']))->update(array("del_status"=>1));
        return array("status"=>true);
    }

    static function getsPayFeeLogOrder($order_number)
    {
        $info = Db::table("property_payfee pp")
            ->field("pm.*,pp.duration")
            ->join("property_memberhouse pm","pm.house_id = pp.house_id")
            ->where(array("order_number"=>$order_number))
            ->find();
        return $info;

    }
}