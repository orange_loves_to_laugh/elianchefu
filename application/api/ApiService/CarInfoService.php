<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/11
 * Time: 14:09
 */

namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class CarInfoService
{
    /**
     * @param int $memberId 用户id
     * @param bool $mark 是否需要加点
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户车辆列表
     */
    static function memberCarInfo($memberId, $mark = false)
    {
        if (empty($memberId)) {
            return array("status" => false, 'msg' => '车辆列表为空');
        }
        # 用户车辆列表
        $list = Db::table("member_car mc")
            ->field("mc.*,cs.id car_sort_id,cs.logo_id car_logo_id,cs.sort_title,cs.level car_level,cl.title logo_title,cle.level_title")
            ->join("car_sort cs", "mc.car_type_id=cs.id", 'left')
            ->join("car_logo cl", "cs.logo_id=cl.id", 'left')
            ->join("car_level cle", "cs.level=cle.id", 'left')
            ->order(array('queue_status' => 'asc'))
            ->where(array("mc.member_id" => $memberId))->where("mc.status!=3")->select();
        # 免排队车辆的id
        $noQueuing = array();
        # 不是免排队车浪数组信息
        $memberCar = array();
        if (!empty($list)) {
            # 车牌号数组
            $carNum = array_column($list, 'car_licens');
            # 免排队车辆的id
            foreach ($list as $k => $v) {
                if ($v['queue_status'] == 1) {
                    array_push($noQueuing, $v['id']);
                }else{
                    $memberCar[$k]=$v;
                }
                if ($mark) {
                    $list[$k]['car_num'] = addAStr($v['car_licens']);
                }
            }
            return array("status" => true, "list" => $list, 'countNum' => count($list), 'noQueuing' => $noQueuing, 'noQueuingNum' => count($noQueuing), 'carNum' => $carNum,'member_car'=>$memberCar);
        } else {
            return array("status" => true,"list"=>array(), 'countNum' => 0, 'noQueuing' => 0, 'noQueuingNum' =>0, 'carNum' => [],'member_car'=>[],'msg' => '车辆列表为空');
        }
    }

    /**
     * @param string $_search
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 获取车辆品牌信息
     */
    static function getCarLogo($_search = '')
    {
        # 接收查询信息
        if (!empty($_search)) {
            # 带搜索条件,直接查库
            $info = Db::table('car_logo cl')->field('cl.id logo_id,cl.title logo_title')
                ->join('car_sort cs', 'cs.logo_id=cl.id')
                ->where("cs.sort_title like '%" . $_search . "%' or cl.title like '%" . $_search . "%'")
                ->group('cl.id')
                ->select();
            $_return = CarInfoService::groupByInitials($info, 'logo_title');
        } else {
            # 查询车辆品牌信息(先从redis中获取,redis中没有,查询数据库)
            $redis = new Redis();
            $_return = $redis->hGetJson('Ecars', 'carLogo');
            if (empty($_return)) {
                # redis中为空,查询数据库
                $info = Db::table('car_logo')->field('id logo_id,title logo_title')->select();
                $_return = CarInfoService::groupByInitials($info, 'logo_title');
                $redis->hSetJson('Ecars', 'carLogo', $_return);
            }
        }
        return array('status' => true, 'data' => $_return);
    }


    /**
     * @param string $logoId
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 获取车辆系列信息
     */
    static function getCarSort($logoId)
    {
        # 接收品牌id
        if (!empty($logoId)) {
            # 先查redis,没有再查数据库
            $redis = new Redis();
            $_return = $redis->hGetJson('Ecars', 'carSort' . $logoId);
            if (empty($_return)) {
                # 查询数据库
                $_return = Db::table('car_sort cs')
                    ->field('cs.id sort_id,sort_title,level car_level,level_title')
                    ->join('car_level cl', 'cl.id=cs.level', 'left')
                    ->where(array('logo_id' => $logoId))
                    ->select();
                $redis->hSetJson('Ecars', 'carSort' . $logoId, $_return);
            }
            return array('status' => true, 'data' => $_return);
        } else {
            return array('status' => false, 'msg' => '品牌id为空');
        }
    }

    /**
     * @param $memberId
     * @param $carId
     * @return array
     * @content 是否可以操作免排队车辆信息修改.删除
     */
    static function operateNoQueue($memberId,$carId)
    {
        # 查询现在有几辆特权的车辆
        $privilegeNum = Db::table('member_car')->where(array('member_id' => $memberId, 'status'=>1,'queue_status' => 1))
            ->where('status != 3')
            ->count('id');
        # 加载修改特权车辆的限制
        if ($privilegeNum <= 2) {
            $limit = config('modifyCar.lineUpCar')[0];
        } elseif ($privilegeNum > 2 and $privilegeNum <= 4) {
            $limit = config('modifyCar.lineUpCar')[1];
        } else {
            $limit = config('modifyCar.lineUpCar')[2];
        }
        # 查询车牌号
        $carNum = Db::table('member_car')->field('car_licens')->where(array('id'=>$carId))->find()['car_licens'];
        # 几天内服务过几次  不可操作 'member_id' => $memberId,
        $serviceNum = Db::table('orders')->where(array('car_liences'=>$carNum))
            ->where("date(order_over) >= '" . date('Y-m-d', strtotime('- ' . $limit['day'] . ' days')) . "'")
            ->count('id');
        if ($serviceNum >= $limit['serviceNum']) {
            return array('status' => false, 'msg' => '系统繁忙,请稍后再试');
        }
        return array('status' => true);
    }

    /**
     * @param $memberId
     * @param $memberLevel
     * @return array
     * @content 是否可以删除车辆
     */
    static function operateDelCar($memberId, $memberLevel)
    {
        if ($memberLevel > 0) {
            # 加载删除车辆限制
            $limit = config('modifyCar.delCar')[$memberLevel];
            # 查询是否满足条件
            $serviceInfo = Db::table('orders')->where(array('member_id' => $memberId))
                ->where("date(order_over) >= '" . date('Y-m-d', strtotime('- ' . $limit['day'] . ' days')) . "'")
                ->group('car_liences')
                ->count('id');
            if ($serviceInfo >= $limit['carNum']) {
                return array('status' => false, 'msg' => '系统繁忙,请稍后再试');
            }
        }
        return array('status' => true);
    }


    /**
     * @param $mark
     * @param $order_number
     * @param $member_level_id
     * @param $member_expiration
     * @param $member_code
     * @param $car_level
     * @return bool
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 重新计算订单价格
     */
    function WriteOrderPrice($mark = false, $order_number, $member_level_id = '', $member_expiration = '', $member_code = '', $car_level = '')
    {
        if ($mark) {
            # 查询所需参数信息
            $orderInfo = Db::table('orders')->field('car_level,member_id,biz_id')->where(array('order_number' => $order_number))->find();
            $car_level = $orderInfo['car_level'];
            $memberInfo = Db::table('member_mapping')->field('member_level_id,member_expiration,member_code')->where(array('member_id' => $orderInfo['member_id']))->find();
            $member_level_id = $memberInfo['member_level_id'];
            $member_expiration = $memberInfo['member_expiration'];
            $member_code = $memberInfo['member_code'];

        }
        # 判断会员状态
        if ($member_level_id > 0) {
            # 是会员,
            if (($member_level_id == 1 or $member_level_id == 2) and $member_expiration < date('Y-m-d')) {
                if (!empty($member_code)) {
                    $level = 'code';
                } else {
                    $level = 'noCode';
                }
            } else {
                $level = 'member';
            }
        } else {
            if (!empty($member_code)) {
                $level = 'code';
            } else {
                $level = 'noCode';
            }
        }
        # 查询订单车牌号、用户id
        $orderinfo = Db::table("orders")->field("car_liences, member_id,biz_id")
            ->where(array("order_number" => $order_number))->find();
        $bizId = $orderinfo['biz_id'];
        if($bizId==1){
            $bizId=0;
        }
        # 查询服务工单,商品工单
        # 服务工单
        $orderServer = Db::table('order_server')
            ->field("id,server_id,price,custommark,price_lock")
            ->where(array('order_number' => $order_number))->select();
        $orderBiz = Db::table('order_biz')
            ->field("id,biz_pro_id,pro_price,custommark,price_lock,bizpro_number")
            ->where(array('order_number' => $order_number))->select();
        # 订单差价
        $diffPrice = 0;
        if (!empty($orderServer)) {
            foreach ($orderServer as $k => $v) {
                if ($v['custommark'] == 1) {
                    # 系统服务
                    # 查询对应车辆等级对应的服务价格
                    if ($level != 'noCode') {
                        # 注册/会员
                        $_discount = Db::table("service_car_mediscount")->field(array("discount"))
                            ->where(array("service_id" => $v['server_id'], 'car_level_id' => $car_level, 'service_type' => 1, 'biz_id' => $bizId))
                            ->find()['discount'];
                    } else {
                        # 未注册不是会员
                        $_discount = Db::table("service_car_mediscount")->field(array("discount"))
                            ->where(array("service_id" => $v['server_id'], 'car_level_id' => $car_level, 'service_type' => 2, 'biz_id' => $bizId))
                            ->find()['discount'];
                    }
                    if (empty($_discount)) {
                        if ($level != 'noCode') {
                            # 注册/会员
                            $_discount = Db::table("service_car_mediscount")
                                ->where(array("service_id" => $v['server_id'], 'service_type' => 1, 'biz_id' => $bizId))
                                ->max('discount');
                        } else {
                            # 未注册不是会员
                            $_discount = Db::table("service_car_mediscount")
                                ->where(array("service_id" => $v['server_id'], 'service_type' => 2, 'biz_id' => $bizId))
                                ->max('discount');
                        }
                    }
                    $_res_discount = sprintf("%.2f", $_discount);
                    Db::table("order_server")->where(array('id' => $v['id']))->update(array("original_price" => $_res_discount));
                    if ($level != 'member') {
                        if (!empty($_res_discount) and !is_null($_res_discount)) {
                            if ($_res_discount != $v['price'] and $v['price_lock'] == 1) {
                                # 计算差价
//                                $diffPrice = $diffPrice + ($v['price'] - $_res_discount);
                                $diffPrice = $diffPrice + $_res_discount;
                                #修改工单价格
                                Db::table("order_server")->where(array('id' => $v['id']))->update(array("price" => $_res_discount));
                            } else {
                                $diffPrice = $diffPrice + $v['price'];
                            }
                        } else {
                            $diffPrice = $diffPrice + $v['price'];
                        }
                    } else {
                        # 查询当前服务是否与当前会员绑定
                        $relevance_service = Db::table('relevance_service rs')
                            ->field('discount,car_impose')
                            ->where(array('rs.service_id' => $v['server_id']))
                            ->where("rs.privilege_id in 
                            (select p.id from privilege p where p.privilege_relevance=1 and find_in_set(p.id,(select level_privilege from level_content lc 
                            where lc.level_id = " . $member_level_id . ")))")
                            ->find();
                        if (!empty($relevance_service)) {
                            if (!empty($orderinfo['member_id'])) {
                                # 查询当前车辆是否在限制内
                                if ($relevance_service['car_impose'] > 0) {
                                    $car_in_impose = Db::query("select id from member_car where member_id = {$orderinfo['member_id']} and  '{$orderinfo['car_liences']}' in 
                                          (select t.car_licens from (select car_licens from member_car 
                                          where member_id = {$orderinfo['member_id']} order by id asc limit {$relevance_service['car_impose']} ) as t )");
                                    if (!empty($car_in_impose)) {
                                        $member_level_discount = $relevance_service;
                                    } else {
                                        goto sysdiscount;
                                    }
                                } else {
                                    $member_level_discount = $relevance_service;
                                }
                            } else {
                                goto sysdiscount;
                            }
                        } else {
                            # 查询会员折扣
                            # 查询当前服务下会员折扣
                            sysdiscount:
                            $member_level_discount = Db::table("service_mediscount")
                                ->field(array("discount"))->where(array("service_id" => $v['server_id'], 'member_level' => $member_level_id))->find();
                        }
                        if (is_null($member_level_discount['discount']) or $member_level_discount['discount'] == 1) {
                            $_res_memberdiscount = 1;
                        } else {
                            $_res_memberdiscount = floatval($member_level_discount['discount']);
                        }
                        if (!empty($_res_discount) and !is_null($_res_memberdiscount)) {
                            $_res_price = sprintf("%.2f", $_res_discount) * $_res_memberdiscount;
                        }
                        if (!is_null($_res_price) and floatval($_res_price) >= 0) {
                            if ($_res_price != $v['price'] and $v['price_lock'] == 1) {
                                # 计算差价
//                                $diffPrice = $diffPrice + ($v['price'] - $_res_discount);
                                $diffPrice = $diffPrice + $_res_price;
                                #修改工单价格
                                Db::table("order_server")->where(array('id' => $v['id']))->update(array("price" => $_res_price));
                            } else {
                                $diffPrice = $diffPrice + $v['price'];
                            }
                        } else {
                            $diffPrice = $diffPrice + $v['price'];
                        }
                    }
                } else {
                    # 查询服务原价
                    $custom_price = Db::table("customserver")->field(array("custom_server_price"))
                        ->where(array("id" => $v['server_id']))->find();
                    # 自定义服务
                    if ($level == 'member') {
                        $price = floatval($v['price']);
//                        $discount_number = getsCustomServerDescount($member_level_id,2);
//                        $discount = $discount_number[$member_level_id];
//                        $discount = getsCustomServerDescount($member_level_id, 2);
                        $discount = Db::table('level_discount')->field('discount')->where(array('level_id'=>$member_level_id,'type'=>2))->find()['discount'];
                        if (floatval($custom_price['custom_server_price']) * $discount != $price and $v['price_lock'] == 1) {
                            # 计算差价
//                            $diffPrice = $diffPrice + ($price - ($custom_price['custom_server_price'] * $discount));
                            $diffPrice = $diffPrice + floatval($custom_price['custom_server_price']) * $discount;
                            Db::table("order_server")->where(array('id' => $v['id']))
                                ->update(array("price" => floatval($custom_price['custom_server_price']) * $discount));
                        } else {
                            $diffPrice = $diffPrice + $v['price'];
                        }
                    } else {
                        Db::table("order_server")->where(array('id' => $v['id']))->update(array("price" => $custom_price['custom_server_price']));
                        $diffPrice = $diffPrice + $custom_price['custom_server_price'];
                    }
                }
            }
        }
        # 商品工单
        if (!empty($orderBiz)) {
            foreach ($orderBiz as $k => $v) {
                if ($v['custommark'] == 2) {
                    # 自定义商品查询原价
                    $price = Db::table("custom_biz")->field("custom_biz_price")->where(array('id' => $v['biz_pro_id']))->find();
                    if ($level == 'member') {
                        if ($v['custommark'] == 2) {
                            # 自定义商品
                            $order_price = floatval($v['pro_price']);
                            # 引入折扣
//                            $descount_list = getsCustomBizDescount();
//                            $discount = $descount_list[$member_level_id];
//                            $discount = getsCustomServerDescount($member_level_id, 1);
                            $discount = Db::table('level_discount')->field('discount')->where(array('level_id'=>$member_level_id,'type'=>1))->find()['discount'];
                            if (floatval(($price['custom_biz_price'] * $discount * $v['bizpro_number'])) != $order_price and $v['price_lock'] == 1) {
                                # 计算差价
//                            $diffPrice = $diffPrice + ($order_price - ($price['custom_biz_price'] * $discount * $v['bizpro_number']));
                                $diffPrice = $diffPrice + floatval($price['custom_biz_price'] * $discount * $v['bizpro_number']);
                                Db::table("order_biz")
                                    ->where(array("id" => $v['id']))
                                    ->update(array("pro_price" => floatval($price['custom_biz_price'] * $discount * $v['bizpro_number'])));
                            } else {
                                $diffPrice = $diffPrice + $v['pro_price'];
                            }
                        }
                    } else {
                        $diffPrice = $diffPrice + $price['custom_biz_price'] * $v['bizpro_number'];
                        Db::table("order_biz")
                            ->where(array("id" => $v['id']))
                            ->update(array("pro_price" => floatval($price['custom_biz_price'] * $v['bizpro_number'])));
                    }
                } else {
                    # 系统商品
                    if ($level == 'member') {
                        # 会员
                        $_shop_discount = Db::table("biz_pro_mediscount")->field("discount")
                            ->where(array("biz_pro_id" => $v['biz_pro_id'], "member_level" => $member_level_id))->find();
                        if (!is_null($_shop_discount['discount']) and $_shop_discount['discount'] != 1) {
                            $_shop_price = Db::table("biz_pro")->field("biz_pro_online")->where(array("id" => $v['biz_pro_id']))->find();
                            if (!is_null($_shop_price['biz_pro_online'])) {
                                $_res_shopdiscount = floatval($_shop_price['biz_pro_online']) * floatval($_shop_discount['discount']) * $v['bizpro_number'];
                                $_res_shopdiscount = sprintf('%.2f', $_res_shopdiscount);
                                if ($_res_shopdiscount != floatval($v['pro_price']) and $v['price_lock'] == 1) {
                                    Db::table("order_biz")->where(array('id' => $v['id']))->update(array("pro_price" => $_res_shopdiscount));
                                }
                            }
                        }
                    } else {
                        # 不是会员,判断注册没注册
                        if ($level == 'code') {
                            $_shop_price = Db::table("biz_pro")->field("biz_pro_online")->where(array("id" => $v['biz_pro_id']))->find();
                            if (!is_null($_shop_price['biz_pro_online'])) {
                                if (floatval($_shop_price['biz_pro_online'] * $v['bizpro_number']) != floatval($v['pro_price']) and $v['price_lock'] == 1) {
                                    Db::table("order_biz")->where(array('id' => $v['id'])
                                    )->update(array("pro_price" => floatval($_shop_price['biz_pro_online'] * $v['bizpro_number'])));
                                }
                            }
                        }
                    }
                }
            }
        }
        # 修改订单总价
        Db::table('orders')->where(array('order_number' => $order_number))->update(array('order_price' => $diffPrice));
//        # 完成任务
//        BizTask::FinishTask(3, $orderinfo['biz_id'], 1);
        return true;
    }

    /*
 * @access public
 * @return bool
 * @context 获取自定义服务会员折扣
 * */
    function getsCustomServerDescount($member_level_id,$type)
    {
        //$type 1商品  2服务
        $level_discount = Db::table('level_discount')->field('discount')->where(array('level_id'=>$member_level_id,'type'=>$type))->find()['discount'];
        return $level_discount;
        /* return array(
             '1' => 1,//初级体验
             '2' => 0.9,//体验卡
             '3' => 0.9,//银卡
             '4' => 0.85,//金卡
             '5' => 0.85,//白金卡
             '6' => 0.8,//如意卡
             '7' => 0.8,//贵宾卡
             '8' => 0.75,//钻石卡
             '9' => 1,//至尊卡
         );*/
    }

    /**
     * 二维数组根据首字母分组排序
     * @param  array $data 二维数组
     * @param  string $targetKey 首字母的键名
     * @return array             根据首字母关联的二维数组
     */
    static function groupByInitials(array $data, $targetKey = 'name')
    {
        $data = array_map(function ($item) use ($targetKey) {
            return array_merge($item, [
                'initials' => CarInfoService::getInitials($item[$targetKey]),
            ]);
        }, $data);
        $data = CarInfoService::sortInitials($data);
        return $data;
    }

    /**
     * 按字母排序
     * @param  array $data
     * @return array
     */
    static function sortInitials(array $data)
    {
        $sortData = [];
        foreach ($data as $key => $value) {
            $sortData[$value['initials']][] = $value;
        }
        ksort($sortData);
        return $sortData;
    }

    /**
     * 获取首字母
     * @param  string $str 汉字字符串
     * @return string 首字母
     */

    static function getInitials($str)
    {
        if (empty($str)) {
            return '#';
        }
        $fchar = ord($str{0});
        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str{0});
        }
        $s1 = iconv('UTF-8', 'GBK', $str);
        $s2 = iconv('GBK', 'UTF-8', $s1);
        $s = $s2 == $str ? $s1 : $str;
        $asc = ord($s{0}) * 256 + ord($s{1}) - 65536;
        if ($asc >= -20319 && $asc <= -20284) {
            return 'A';
        }

        if ($asc >= -20283 && $asc <= -19776) {
            return 'B';
        }

        if ($asc >= -19775 && $asc <= -19219) {
            return 'C';
        }

        if ($asc >= -19218 && $asc <= -18711) {
            return 'D';
        }

        if ($asc >= -18710 && $asc <= -18527) {
            return 'E';
        }

        if ($asc >= -18526 && $asc <= -18240) {
            return 'F';
        }

        if ($asc >= -18239 && $asc <= -17923) {
            return 'G';
        }

        if ($asc >= -17922 && $asc <= -17418) {
            return 'H';
        }

        if ($asc >= -17417 && $asc <= -16475) {
            return 'J';
        }

        if ($asc >= -16474 && $asc <= -16213) {
            return 'K';
        }

        if ($asc >= -16212 && $asc <= -15641) {
            return 'L';
        }

        if ($asc >= -15640 && $asc <= -15166) {
            return 'M';
        }

        if ($asc >= -15165 && $asc <= -14923) {
            return 'N';
        }

        if ($asc >= -14922 && $asc <= -14915) {
            return 'O';
        }

        if ($asc >= -14914 && $asc <= -14631) {
            return 'P';
        }

        if ($asc >= -14630 && $asc <= -14150) {
            return 'Q';
        }

        if ($asc >= -14149 && $asc <= -14091) {
            return 'R';
        }

        if ($asc >= -14090 && $asc <= -13319) {
            return 'S';
        }

        if ($asc >= -13318 && $asc <= -12839) {
            return 'T';
        }

        if ($asc >= -12838 && $asc <= -12557) {
            return 'W';
        }

        if ($asc >= -12556 && $asc <= -11848) {
            return 'X';
        }

        if ($asc >= -11847 && $asc <= -11056) {
            return 'Y';
        }

        if ($asc >= -11055 && $asc <= -10247) {
            return 'Z';
        }

        return null;
    }
}
