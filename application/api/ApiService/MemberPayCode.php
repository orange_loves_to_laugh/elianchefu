<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/23
 * Time: 9:50
 */

namespace app\api\ApiService;


use Redis\Redis;

class MemberPayCode
{
    /**
     * @param $memberId
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 生成用户支付二维码信息
     */
    static function memberPayCode($memberId)
    {
        # 生成用户支付二维码(30s有效)
        if (empty($memberId)) {
            return array('status' => false, 'msg' => '用户信息获取失败');
        }
        $memberService = new MemberService();
        $memberInfo = $memberService->MemberInfoCache($memberId);
        if (empty($memberInfo['member_phone'])) {
            return array('status' => false, 'msg' => '请先注册');
        }
        $code = randCodes();
        $returnCode = $memberService->authCode($memberInfo['member_phone'] . 'mobile' . $code . 'valicode' . time() . 'timestamp', 'ENCODE');
//        $returnCode = $memberInfo['member_phone'] . 'mobile' . $code . 'valicode' . time() . 'timestamp';
        $redis = new Redis();
        $redis->hDel('memberPayCode', $memberId);
        $redis->hSet('memberPayCode', $memberId, $code, 35);

        return array('status' => true, 'qrCode' => $returnCode);
//        return array('status' => true, 'qrCode' => $returnCode);
    }

    /**
     * @param $memberId
     * @param $verifyInfo
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户支付码解析验证
     */
    static function payCodeVerify($memberId, $verifyInfo)
    {
        # 支付二维码验证
        if (empty($memberId) or empty($verifyInfo)) {
            return array('status' => false, 'msg' => '系统错误,参数不全');
        }
        # 解码
        $memberService = new MemberService();
        $payCode = $memberService->authCode($verifyInfo);
        if (empty($payCode)) {
            return array('status' => false, 'msg' => '支付码信息解析失败');
        }
        # 解析手机号
        $phone = explode('mobile', $payCode)[0];
        # 手机号判断
        $memberInfo = $memberService->MemberInfoCache($memberId);
        if ($memberInfo['member_phone'] != $phone) {
            return array('status' => false, 'msg' => '请扫描本人付款码');
        }
        # 解析验证码
        $valiCode = explode('valicode', trim(str_replace($phone . 'mobile', '', $payCode)))[0];
        $redis = new Redis();
        $redisCode = $redis->hGet('memberPayCode', strval($memberId));
        if (empty($redisCode)) {
            return array('status' => false, 'msg' => '付款码已过期,请提示用户刷新');
        }
        if ($redisCode != $valiCode) {
            return array('status' => false, 'msg' => '请扫描本人付款码');
        }
        # 解析时间
        $timestamp = explode('timestamp', trim(str_replace($phone . 'mobile' . $valiCode . 'valicode', '', $payCode)))[0];
        if ((time() - $timestamp) >= 30) {
            return array('status' => false, 'msg' => '付款码已过期,请提示用户刷新');
        }
        return array('status' => true, 'msg' => '验证通过');
    }
}
