<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/9
 * Time: 13:26
 */

namespace app\api\ApiService;


use Redis\Redis;
use think\Db;

class AdvanceService
{
    /**
     * @param $mark
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 根据类型获取广告图片
     */
   static function getsAdvance($mark)
    {
        $_redis=new Redis();
        $_redis->hDel("advance",$mark);
        $advance=$_redis->hGetJson("advance",$mark);
        if(empty($advance)){
            # 查询数据库中数据
            $where = null;
            if($mark!='protocol' and $mark!="privacy" ){
                $where = "application_start<='" . date("Y-m-d H:i:s") . "' and application_end>='" . date("Y-m-d H:i:s") . "'";
            }
            $advance=Db::table("application")->where(array("application_mark"=>$mark))
                ->where($where)->select();
            if(!empty($advance)){
                $_redis->hSetJson("advance",$mark,$advance);
            }
        }
        if(!empty($advance)){
            foreach($advance as $k=>$v){
                if($v['is_jump']==1 and $v['is_type']==2){
                    $jump = self::getsJumpUrl($v['jump_type'], $v['jump_id'],$v['application_uri'],$v['cate_id']);
                    $advance[$k]['url'] = $jump['url'];
                    $advance[$k]['param'] = $jump['param'];
                }
            }
        }


        return array("status"=>true,"info"=>$advance);
    }

    /**
     * @param $jump_type
     * @param $jump_id
     * @context 弹窗或图片跳转地址
     */
   static function getsJumpUrl($jump_type,$jump_id,$jump_uri=NULL,$cate_id=0)
    {
        $param=array();
        switch($jump_type){
            case 1:
                // 服务详情
                $url="index/service_info";
                $param['id']=$jump_id;
                $param['type']=2;
                break;
            case 2:
                // 关联活动
                $url="index/activeDetailed";
                $param['id']=$jump_id;
                break;
            case 3:
                // 办理会员
                $url="index/member_info";
                $param['level']=$jump_id;
                break;
            case 4:
                // 二手车
                if(!empty($jump_id)){
                    $param['id']=$jump_id;
                    $url="market/carMarket";
                }else{
                    $url="market/carMarket";
                }
                break;
            case 5:
                // 联盟商家
                if(!empty($jump_id)){
                    $param['id']=$jump_id;
                    $url="index/shop_info";
                }else{
                    $url="index/union_shop";
                    $param['is_type'] = 2;
                    if($cate_id>0){
                        $param["cate_pid"] =$cate_id;
                        //$param['name'] = Db::table("merchants_cate")->where(array("id"=>$cate_id))->value('title');
                    }
                }
                break;
            case 6:
                // 招募
                $url="index/partner";
                $param['type'] = $jump_id-1;
                break;
            case 9:
                //外部链接
                $url = $jump_uri;
                break;
            case 10:
                //外部链接
                $url="mine/IntegralConvertible";
                break;
            case 11:
                // 本地生活
                if(!empty($jump_id)){
                    $param['id']=$jump_id;
                    $url="index/shop_info";
                }else{
                    $url="index/union_shop";
                    $param['is_type'] = 1;
                    if($cate_id>0){
                        $param["class_id"] =$cate_id;
                        $param['name'] = Db::table("merchants_cate")->where(array("id"=>$cate_id))->value('title');
                    }
                }
                break;
            case 12:
                // 办理会员
                $url = "recommend/reVip";
                break;
            case 13:
                // 便民服务跳转
                $url = "index/local_life_other";
                $param['img_list'] = array(imgUrl('static/img/Client/local_life.png', true));
                $param['img_list'] = array(imgUrl('https://back.elianchefu.com/static/img/recruit/hehuoren.png', true));
                $param['phone'] = '400-6505736';
                $param['phone_tips'] = '立即拨打';
                $param['phone_title'] = '拨打电话';
                $param['title'] = '便民服务wa';
                break;
            default :
                $url="index/partner";
                break;
        }
        return array("url"=>$url,"param"=>$param);
    }

    /**
     * @return array
     * @context 用户端客服电话
     */
    static function customerServerTel()
    {
        $tel=config("share.customerServerTel");
        return $tel;
    }
}