<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/26
 * Time: 18:02
 */

namespace app\api\ApiService;


use think\Db;

class ActiveService
{
    /**
     * @param $memberId
     * @param int $activeId 签到天数/活动id
     * @param int $category 活动类型  1 大转轮 2 任务  3 签到
     * @param bool $quantityJudge 是否需要上架数量判断
     * @param bool $singleJudge 是否需要判断单人中奖次数
     * @param bool $rewardRate 是否需要判断奖品概率
     */
    function giveReward($memberId, $activeId, $category, $quantityJudge = true, $singleJudge = true, $rewardRate = true)
    {
        if ($category == 3) {
            # 领取签到奖励
            $rewardInfo = Db::table('signs')->where(array('days_id' => $activeId))->find();
            if (!empty($rewardInfo) and $rewardInfo['surplus_num'] > 0) {
                # 剩余数量减一
                Db::table('signs')->where(array('days_id' => $activeId))->setDec('surplus_num', 1);
                if ($rewardInfo['signs_type'] == 1) {
                    # 积分
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($memberId, 24, $rewardInfo['intebal_num'], 1);
                } elseif ($rewardInfo['signs_type'] == 2) {
                    # 余额
                    $balanceService = new BalanceService();
                    $balanceService->operaMemberbalance($memberId, $rewardInfo['intebal_num'], 1, array(
                        'consump_price' => $rewardInfo['intebal_num'],
                        'pay_type' => 5,
                        'log_consump_type' => 2,
                        'consump_type' => 1,
                        'income_type' => 14
                    ));
                    # 增加补贴记录,6,签到赠送余额
                    SubsidyService::addSubsidy(array("subsidy_type"=>6,"subsidy_number"=>$rewardInfo['intebal_num'],"member_id"=>$memberId));
                } elseif ($rewardInfo['signs_type'] == 3) {
                    # 卡券
                    $voucherInfo = Db::table('card_voucher')->where(array('server_id' => $rewardInfo['spoil_id']))->order('id asc')->find();
                    $voucherArr = array();
                    for ($i = 0; $i < $rewardInfo['signs_number']; $i++) {
                        array_push($voucherArr, array(
                            'voucher_id' => $rewardInfo['spoil_id'],
                            'member_id' => $memberId,
                            'create' => date('Y-m-d H:i:s', strtotime('+ ' . $rewardInfo['prize_indate'] . ' days')),
                            'status' => 1,
                            'voucher_source' => 17,
                            'voucher_cost' => $voucherInfo['card_price'],
                            'card_title'=>$voucherInfo['card_title'],
                            'card_time' => $rewardInfo['prize_indate'],
                            'server_id' => $voucherInfo['server_id'],
                            'start_time' => date('Y-m-d H:i:s'),
                            'voucher_type' => $rewardInfo['voucher_type'] == 1 ? 3 : 4,
                        ));
                    }
                    if (!empty($voucherArr)) {
                        Db::table('member_voucher')->insertAll($voucherArr);
                    }
                } elseif ($rewardInfo['signs_type'] == 4) {
                    # 红包
                    # 查询红包详情
                    $redpacket = Db::table('redpacket_detail')->where(array('id' => $rewardInfo['spoil_id']))->select();
                    if (!empty($redpacket)) {
                        $memberVoucher = array();
                        for ($i = 0; $i < $rewardInfo['signs_number']; $i++) {
                            foreach ($redpacket as $rk => $rv) {
                                array_push($memberVoucher, array(
                                    'voucher_id' => 0,
                                    'member_id' => $memberId,
                                    'create' => date('Y-m-d H:i:s', strtotime('+ ' . $rv['voucher_over'] . ' days')),
                                    'status' => 1,
                                    'voucher_source' => 17,
                                    'redpacket_id' => $rewardInfo['spoil_id'],
                                    'voucher_cost' => $rv['voucher_price'],
                                    'card_title' => $rv['voucher_title'],
                                    'card_time' => $rv['voucher_over'],
                                    'server_id' => $rv['voucher_id'],
                                    'voucher_type' => $rv['voucher_type'],
                                    'money_off' => $rv['money_off'],
                                    'voucher_start' => $rv['voucher_start']
                                ));
                            }
                        }
                        if (!empty($memberVoucher)) {
                            Db::table('member_voucher')->insertAll($memberVoucher);
                        }
                    }
                }elseif ($rewardInfo['signs_type'] == 7 or $rewardInfo['signs_type'] == 8 or $rewardInfo['signs_type'] == 9 ) {
                    # 7 联盟商家   8本地生活 9 物业
                    $voucherService = new VoucherService();
                    $voucherService->addMerchantVoucher($memberId, $rewardInfo['signs_number'], $rewardInfo['spoil_id'], 17);
                } else {
                    # 其他物品
                    return array('status' => true, 'rewardInfo' => array());
                }
                # 更新用户redis
                $memberService = new MemberService();
                $memberService->changeMemberInfo($memberId);
                return array('status' => true, 'rewardInfo' => $rewardInfo);
            } else {
                return array('status' => true, 'rewardInfo' => array());
            }
        }
    }
}
