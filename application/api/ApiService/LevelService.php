<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/9
 * Time: 17:13
 */

namespace app\api\ApiService;


use app\service\ActiveService;
use app\service\AgentTask;
use app\service\CommissionService;
use app\service\EalspellCommissionService;
use app\service\IGeTuiService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class LevelService
{
    /**
     * @param string $level
     * @return array|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员卡信息
     */
    static function memberLevelInfo($level = '', $nowLevel = '', $order = "id asc")
    {
        $where = "";
        if (!empty($nowLevel)) {
//            if ($nowLevel == 1) {
//                $nowLevel = 2;
//            }
            $where = "id > {$nowLevel}";
        }
        $list = Db::table("member_level")->where($where)->order($order)->select();
        if (!empty($level)) {
            $list[0]['voucherAfter'] = 99;
            return $list[$level - 1];
        } else {
            return $list;
        }
    }

    /**
     * @param $level
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取等级对应的权限
     */
    static function getLevelPrivilege($level)
    {
        $list = Db::table("privilege")
            ->where("find_in_set(id,(select level_privilege from level_content where level_id = {$level}))")
            ->select();
        return $list;
    }

    /**
     * @param $level
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取对应等级的充值金额
     */
    static function getLevelRecharge($level)
    {
        if ($level == 0) {
            $level = 1;
        }
        $list = Db::table("recharge")->where(array("level" => $level))->order("price asc")->select();
        return $list;
    }

    /**
     * @param $level
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员升级信息
     */
    static function upgradeInfo($level, $where = '')
    {
        $list = Db::table("upgrade u")
            ->field("u.id,u.up_level,convert(u.price,decimal(10,2)) price,ml.level_title,convert(ml.level_price,decimal(10,2)) level_price,ml.level_img,ml.level_content,
            (select  coalesce(convert(sum(ug.giving_number*ug.intbal),decimal(10,2)),0) from upgrade_giving ug where ug.upgrade_id = u.id and ug.giving_type=4) giving_num")
            ->leftJoin("member_level ml", "ml.id=u.up_level")
            ->where(array("u.now_level" => $level))
            ->where($where)
            ->order("u.up_level desc")
            ->select();
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_content'] = self::levelContentFormat($v['level_content']);
            }
        }

        return $list;
    }

    /**
     * @param $upgradeId
     * @return array|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 升级赠送
     */
    static function upgradeGiving($upgradeId)
    {
        $list = Db::table("upgrade_giving ug")
            ->field("ug.*,cv.card_title,cv.card_type_id,
            (case ug.giving_type
                    when 1 then (select bp.biz_pro_title from biz_pro bp where bp.id=ug.giving_id) 
                    when 2 then (select s.service_title from service s where s.id=ug.giving_id)
                    when 3 then '积分'
                    when 4 then '余额'
                    when 7 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 8 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 9 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 10 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 11 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 12 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 13 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 14 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 15 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    when 16 then (select mv.title from merchants_voucher mv where mv.id=ug.giving_id)
                    end) card_title")
            ->leftJoin("card_voucher cv", "ug.giving_type in (1,2) and cv.id=ug.giving_id")
            ->where(array("ug.upgrade_id" => $upgradeId))
            ->select();
        return $list;
    }

    /**
     * @param $level
     * @return \think\db\Query
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员套餐
     */
    static function levelPackage($level, $setmeal_id = '')
    {
        $where = "";
        if (!empty($setmeal_id)) {
            $where = " id = {$setmeal_id}";
        }
        $list = Db::table("setmeal")->where("level_id", "=", $level)->where($where)->select();
        if (!empty($list)) {
            foreach ($list as $lk => $lv) {
                $detail = Db::table("level_giving lg")
                    ->field("lg.*,date_add(curdate(), interval 1 year) period_time")
                    ->where("lg.setmeal_id", "=", $lv['id'])->select();
                if (!empty($detail)) {
                    foreach ($detail as $k => $v) {
                        # 消费标准
                        $min_consume = '--';
                        # 有效期
                        $validity_day = '--';
                        # 是否当天使用
                        $voucher_start = '--';
                        # 平台承担比例
                        $platform_ratio = 0;
                        if ($v['giving_type'] == 1) {
                            # 类型
                            $_active_type_title = "商品卡券";
                            # 抵值金额
                            $intbal = sprintf("%.2f", $v['intbal']);
                            # 名称
                            $title = getActiveTitle($v['giving_type'], $v['giving_id']);
                        } else if ($v['giving_type'] == 2) {
                            $_active_type_title = "服务卡券";
                            $intbal = sprintf("%.2f", $v['intbal']);
                            $title = getActiveTitle($v['giving_type'], $v['giving_id']);
                        } else if ($v['giving_type'] == 3) {
                            $_active_type_title = "红包卡券";
                            $intbal = sprintf("%.2f", $v['intbal']);
                            $title = Db::name('redpacket')->where('id=' . $v['giving_id'])->field('packet_title title')->find();
                        } else if (in_array($v['giving_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))) {
                            $_active_type_title = givingTypeTitle($v['giving_type']);
                            $intbal = priceFormat($v['intbal']);
                            $name = ActiveService::getCardVoucherProServiceTitle($v['giving_id'], "merchantVoucher", false);
                            $title['title'] = $name['title'];
                            $validity_day = $name['validity_day'];
                            $min_consume = priceFormat($name['min_consume']);
                            $voucher_start = $name['voucher_start'] == 1 ? '是' : '否';
                            $platform_ratio = ($name['platform_ratio'] * 100) . '%';
                            /* # 查询对应的服务分类
                             $first = getServiceClassTitle($name['cate_pid']);
                             if (!empty($first)) {
                                 $_active_type_title .= '-' . $first;
                             }
                             $second = getServiceClassTitle($name['second_cate_pid']);
                             if (!empty($second)) {
                                 $_active_type_title .= '-' . $second;
                             }*/
                        }
                        $detail[$k]['giving_title'] = $title['title'];
                        $detail[$k]['typeTitle'] = $_active_type_title;
                        $detail[$k]['min_consume'] = $min_consume;
                        $detail[$k]['validity_day'] = $validity_day;
                        $detail[$k]['intbal'] = $intbal;
                    }
                    $list[$lk]['giving'] = $detail;
                    $list[$lk]['price'] = array_sum(array_column($detail, 'intbal'));
                }
            }
        }
        return $list;
    }

    static function applyMember($member_id, $level, $balance, $setmeal_id, $data = array(), $voucherInfo = '')
    {
        $_redis = new Redis();
        $is_repeat = $_redis->lock("makemember" . $member_id, 60);
        if (!$is_repeat) {
            return true;
        }
        $_redis->hDel('payRedis', $data['order_number']);
        # 获取办理的会员信
        $levelInfo = self::memberLevelInfo($level);
        $expira = date("Y-m-d", strtotime("+3000 days"));
        if ($level <= 1) {
            $expira = date("Y-m-d", strtotime("+{$levelInfo['level_time']} days"));
        }
        # 判断之前是不是会员 ,之前不是会员删除所有车辆信息
        $memberService = new MemberService();
        $memberInfo = $memberService->MemberInfoCache($member_id);
        if ($memberInfo['member_level_id'] == $level) {
            # 查询办卡记录是不是今天
            $handlecard_info = Db::table("log_handlecard")->field("member_create,level_id")->where(array("member_id" => $member_id))->order("id desc")->find();
            if ($handlecard_info['level_id'] == $level and date("Y-m-d", strtotime($handlecard_info['member_create'])) == date("Y-m-d")) {
                return true;
            }
        }
        if ($memberInfo['member_level_id'] == 0) {
            Db::table('member_car')->where(array('member_id' => $member_id))->update(array('member_id' => 0));
        }
        # 修改用户等级余额 过期时间
        Db::table("member_mapping")->where(array("member_id" => $member_id))->update(array("member_level_id" => $level, "member_expiration" => $expira));
        # 卡券核销
        if (!empty($voucherInfo)) {
            Db::table('member_voucher')->where(array('id' => $voucherInfo['id']))->update(array('status' => 2));
        }
        $BalanceService = new BalanceService();
        if (empty($balance)) {
            $balance = $levelInfo['level_practical'];
        }
        $balance_opera = $balance;
        $balance_opera_commission = $balance;
        # 初级体验卡不加余额
        if ($level == 1) {
            $balance_opera = 0;
        }
        if (!empty($balance)) {
            if (!empty($voucherInfo)) {
                $data['consump_price'] = $data['consump_price'] - $voucherInfo['voucher_cost'];
            }
            $BalanceService->operaMemberbalance($member_id, $balance_opera, 1, array("consump_price" => $data['consump_price'], "pay_type" => $data['pay_type'],
                "log_consump_type" => 2,    // 1消费  2充值
                "consump_type" => $data['consump_type'],// 1线上 或 2 门店支付
                "income_type" => 2, "order_number" => $data['order_number'], "biz_id" => $data['biz_id']
            ));
        }
        # 增加微信支付宝收款手续费
        if ($data['pay_type'] == 1 or $data['pay_type'] == 2) {
            SubsidyService::addExpenses(array("type_id" => 2, "price" => $balance * 0.006));
        }
        # 增加用户对应套餐的卡券
        $giving = self::levelPackage($level, $setmeal_id);
//        if (!empty($giving)) {
//            $giving = $giving[0]['giving'];
//        }
        if (!empty($giving)) {
            $_voucher = array();
            $voucher_source = $data['consump_type'] == 1 ? 5 : 6;
            $VoucherService = new VoucherService();
            foreach ($giving as $gk => $gv) {
                $giving = $gv['giving'];
                foreach ($giving as $k => $v) {
                    if ($v['giving_type'] == 3) {
                        $VoucherService->addRedpacket($member_id, $v['giving_id'], 4, $v['giving_number'], array("voucher_source" => $voucher_source, "paytype" => $data['pay_type']), $v['giving_id']);
                    } elseif ($v['giving_type'] >= 7 and $v['giving_type'] <= 16) {
                        $VoucherService->addMerchantVoucher($member_id, $v['giving_number'], $v['giving_id'], $voucher_source);
                    } else {
                        $card_type_id = $v['giving_type'] == 1 ? 3 : 1;
                        $voucher_type = $v['giving_type'] == 1 ? 5 : 4;
                        # 查询卡券id
                        $voucher = Db::table("card_voucher")->field("id,card_title")->where(array("card_type_id" => $card_type_id, "server_id" => $v['giving_id']))->find();
                        for ($i = 0; $i < $v['giving_number']; $i++) {
                            array_push($_voucher, array("voucher_id" => $voucher['id'], "member_id" => $member_id, "card_time" => 365, "voucher_source" => $voucher_source, "paytype" => $data['pay_type'],
                                "voucher_cost" => $v['intbal'], "card_title" => $voucher['card_title'], "voucher_type" => $voucher_type, "server_id" => $v['giving_id']));
                        }
                    }
                }
            }
            $VoucherService->addMemberVoucher($member_id, $_voucher);
        }
        # 是否有赠送积分
        if (!empty($levelInfo['giving_integral'])) {
            $IntegralService = new IntegralService();
            $IntegralService->addMemberIntegral($member_id, 13, $levelInfo['giving_integral'], 1);
        }
        /* # 是否有额外赠送积分
         if (!empty($levelInfo['extra_giving_integral'])) {
             $IntegralService = new IntegralService();
             $IntegralService->addMemberIntegral($member_id, 6, $levelInfo['extra_giving_integral'], 1);
         }*/
        # 恢复合伙人状态
        if ($level >= 2 and $memberInfo['member_type'] == 1) {
            $partnerInfo = Db::table('member_partner')->field('id')->where(array('member_id' => $member_id))->find();
            if (!empty($partnerInfo)) {
                Db::table('member_mapping')->where(array('member_id' => $member_id))->update(array('member_type' => 2));
            }
        }

        # 员工 、合作店提成
        $commissionService = new CommissionService();
        if ($level == 1) {
            $balance_opera_commission = 99;
        }
        $deduct = $commissionService->cooperativeDeduct($data['assign_id'], $data['employee_id'], 'Membership', $balance_opera_commission, $member_id, $level, '', '', $data['formMark'], $data['pay_type']);
        # 增加办卡记录
        if ($data['formMark'] == 'employeeSal' or $data['formMark'] == 'calling') {
            $employeeId = -$data['employee_id'];
        } else {
            $employeeId = $data['employee_id'];
        }
        # 查询门店关联的员工
        $assignStaff = Db::table('assign_staff')->field('employee_id')->where(array('biz_id' => $data['assign_id'], 'type' => 1))->find();
        if (empty($assignStaff)) {
            $assignStaff = 0;
        } else {
            $assignStaff = $assignStaff['employee_id'];
        }
        //类型  0 自己办的 1 门店推荐 办的 2商户推荐办的

        if ($data['formMark'] == 'merchants' or $data['formMark'] == 'merchants_employee') {
            #merchants  ，merchants_employee 都是商户 给商户 办卡
            $data['consump_type'] = 3;
            # 查询门店关联的员工
            $assignStaff = Db::table('assign_staff')->field('employee_id')->where(array('biz_id' => $data['assign_id'], 'type' => 2))->find();
            if (empty($assignStaff)) {
                $assignStaff = 0;
            } else {
                $assignStaff = $assignStaff['employee_id'];
            }
        }
        if (empty($data['shareMemberId'])) {
            if (!empty($data['assign_id'])) {
                # 增加额外赠送
                self::addLevelRecommend($data['formMark'], $level, $member_id, array("assign_id" => $data['assign_id']));
            }
        }
        if ($data['formMark'] == 'agent') {
            $data['consump_type'] = 4;
            if ($level == 1) {
                EalspellCommissionService::addBandMember($data['assign_id'],$member_id);
            }
            AgentTask::applyMemberTask($data['assign_id']);
        }
        if (!empty($voucherInfo)) {
            $levelInfo['level_practical'] = $levelInfo['level_practical'] - $voucherInfo['voucher_cost'];
        }
        #merchants  ，merchants_employee 都是商户 给商户 办卡
        Db::table("log_handlecard")->insert(array("member_id" => $member_id, "level_id" => $level, "member_create" => date("Y-m-d H:i:s"), "level_price" => $levelInfo['level_practical'],
            "employee_id" => $employeeId, "log_resource" => $data['consump_type'], "active_id" => 0, "backbalance" => 0, "employee_deduct" => $deduct['employee_deduct'], "pay_type" => $data['pay_type'],
            "biz_id" => $data['assign_id'], "source_type" => 0, "source_id" => 0, 'employee_sal' => $assignStaff));
        # 办理会员渠道
        $memberService->channel($data, $member_id, 2);
        $IGeTuiService = new IGeTuiService();
        # 推荐人加30%积分shareMemberId
        if (!empty($data['shareMemberId'])) {
            $integral = intval($data['consump_price'] * 0.3);
            $integralService = new IntegralService();
            $integralService->addMemberIntegral($data['shareMemberId'], 19, $integral, 1);
            # 判断是不是合伙人
            $shareMemberInfo = $memberService->MemberInfoCache($data['shareMemberId']);
            if ($shareMemberInfo['member_type'] == 2) {
                //or $shareMemberInfo['member_level_id'] == 2
                if (($shareMemberInfo['member_level_id'] == 1) and $shareMemberInfo['member_expiration'] < date('Y-m-d')) {
                    # 过期,不是合伙人
                    Db::table('member_mapping')->where(array('member_id' => $data['shareMemberId']))->update(array('member_type' => 1));
                } else {
                    # 增加额外赠送
                    self::addLevelRecommend("shareMemberId", $level, $member_id);
                    # 增加合伙人收益
                    $partnerService = new PartnerService();
                    $partnerSet = $partnerService->partnerGive();
                    $partnerIncome = $partnerSet['info']['membership_ratio'] * $data['consump_price'];
                    Db::table('member_partner')->where(array('member_id' => $data['shareMemberId']))->setInc('partner_balance', $partnerIncome);
                    Db::table('member_partner')->where(array('member_id' => $data['shareMemberId']))->setInc('partner_balance_total', $partnerIncome);
                    # 加收益记录
                    Db::table('log_consump')->insert(array(
                        'member_id' => $data['shareMemberId'],
                        'consump_price' => $partnerIncome,
                        'consump_pay_type' => $data['pay_type'],
                        'consump_time' => date('Y-m-d H:i:s'),
                        'log_consump_type' => 2,
                        'consump_type' => 1,
                        'income_type' => 8,
                        'biz_id' => 0
                    ));
                    $partnerBalance = Db::table('member_partner')->field('partner_balance')->where(array('member_id' => $data['shareMemberId']))->find()['partner_balance'];
                    SubsidyService::addExpenses(array("type_id" => 4, "price" => $partnerIncome));
                    $IGeTuiService->MessageNotification($data['shareMemberId'], 14, array('price' => $partnerIncome, 'title' => '推荐用户', 'balance' => $partnerBalance));
                }
            }
            # 完成任务=>15 推荐会员
            TimeTask::finishTask($data['shareMemberId'], 15);
        }
        $_redis->unlock("makemember" . $member_id);
        # 完成任务=>12 办理会员
        TimeTask::finishTask($member_id, 12);
        # 门店收入
        # 发送消息提醒
        $IGeTuiService->MessageNotification($member_id, 4, ['level_title' => $levelInfo['level_title'], 'level_time' => $levelInfo['level_time'], 'member_level' => $level], false);
        return true;
    }

    /**
     * @param $member_id
     * @param $uplevel
     * @param int $type
     * @param string $nowLevel
     * @return string
     * @context 会员 和升级订单号
     */
    static function makeMemberLevelOrders($member_id, $uplevel, $type = 1, $nowLevel = '')
    {
        if ($type == 1) {
            // 办理会员订单
            $order_number = "a" . $uplevel . "m" . $member_id . "t" . getMsectime();
        } elseif ($type == 3) {
            // 会员充值订单
            $order_number = 'c' . $uplevel . "m" . $member_id . "n" . $nowLevel . "t" . getMsectime();
        } else {
            // 会员升级订单
            $order_number = 'n' . $nowLevel . "m" . $member_id . "u" . $uplevel . "t" . getMsectime();
        }
        return $order_number;
    }

    /**
     * @param $data
     * @return array
     * @context 会员说明格式
     */
    static function levelContentFormat($data)
    {
        if (empty($data)) {
            return $data;
        }
        $_return = array();
        # 首先按中文分号拆分
        $_first = explode('；', $data);
        foreach ($_first as $k => $v) {
            # 按中文冒号拆分
            $_item = explode('：', $v);
            $_item_array = array();
            $_item_array['title'] = $_item[0];
            # 判断内容是否有中文逗号
            if (strpos($_item[1], '，')) {
                $_item_array['content'] = explode('，', $_item[1]);
            } else {
                $_item_array['content'] = array($_item[1]);
            }
            array_push($_return, $_item_array);
        }
        return $_return;
    }

    static function levelProtocol()
    {
        return array(
            "protocol_title" => "E联车服会员办理协议",
            "explain" => "E联车服会员（以下简称本服务）是由辽宁有派网络科技有限公司（以下简称本公司）向广大E联车服用户提供的会员专享服务，本协议由您和本公司签订。",
            "context" => [
                ["title" => "一、声明与承诺", "content" => [
                    "（一）各条款前所列索引关键词仅为帮助您理解该条款表达的主旨之用，不影响或限制本协议条款的含义或解释。为维护您自身权益，建议您仔细阅读各条款具体表述。一旦您成为会员，即代表您同意遵循本协议的所有约定。",
                    "（二）您同意，本公司有权随时对本协议内容进行单方面的变更，并予以公示，无需另行单独通知您，若您在本协议内容公告变更后继续使用会员服务，表示您已充分阅读、理解并接受修改后的协议内容；若您不同意修改后的协议内容，您应停止使用本会员服务。",
                    "（三）您声明，在您同意接受本协议并成为会员时，您具有法律规定的民事权利能力和民事行为能力，若不具备前述条件，您应立即终止注册或停止使用本服务。",

                ]],
                ["title" => "二、会员开通", "content" => [
                    "（一）会员服务开通前，您需要完成E联车服账号的注册或登录。",
                    "（二）您申请开通E联车服会员时，需根据页面提示充值相应的会员服务费用后方可完成开通。",
                ]],
                ["title" => "三、会员服务使用", "content" => [
                    "（一）凭此卡在E联车服消费时可使用余额支付，并享受折扣优惠。",
                    "（二）平台办理会员分为9个等级：初级体验、体验卡、银卡会员、金卡会员、白金卡会员、如意卡会员、贵宾卡会员、钻石卡会员、至尊卡会员。您可根据平台内升级的相关规则来升级会员等级，并享受升级后的会员特权及相关折扣，初级体验只能升级到银卡以上。",
                    "（三）初级体验与体验卡设有时间限制，逾期会员权益失效，其他等级会员永久有效，具体以平台公示为准。",
                    "（四）办理会员即赠送积分。",
                    "（五）初级体验没有积分兑换权限。",
                    "（六）非会员、初级体验和体验卡春节期间不予服务，请您谅解。",
                    "（七）会员卡余额低于15元，且持续超过90天，会员卡将自动作废，取消您的会员资格。请合理确认好您的会员卡余额，若出现上述情况，您将无权向E联车服索要赔偿。",
                    "（八）会员可享受会员专属活动，具体可登录平台查看，以页面公示为准。",

                ]],
                ["title" => "三、退卡事宜", "content" => [
                    "（一）如您需要退卡，平台将按照历史所有消费恢复原价进行计算，并扣除退卡金额的30%作为退卡手续费。",
                    "（二）退卡需到直营门店进行办理。",

                ]],
                ["title" => "四、协议执行", "content" => [
                    "（一）甲乙双方对本协议如有争议，按《中华人民共和国合同法》的有关规定执行。",
                    "（二）如您的行为使E联车服遭受损失（包括自身的直接经济损失、商誉损失及对外支付的赔偿金、和解款、律师费、诉讼费等间接经济损失）或违反相关法律规定，E联车服有权终止您的会员权益，且您应赔偿E联车服及/或其关联公司的上述全部损失。",

                ]],
                ["title" => "四、用户会员使用注意事项", "content" => [
                    "（一）确定开通并支付时，请再次确定好您要充值的会员内容。",
                    "（二）您同意，E联车服有权力通过邮件、短信电话等形式，向您发送相关活动信息等必要信息的权利。",
                    "（三）E联车服会员在本平台操作的各项行为应符合法律法规规定、平台规则规定及E联车服会员协议，当出现违规行为、违规订单时，E联车服有权按照平台内规则对您的账号和订单予以处理，如E联车服会对您进行平台权益和会员权益降级，强制风险验证、冻结或关闭账号、取消订单、暂停或停止提供服务、关闭会员开通资格、关停已开通的会员服务，已绑定的或获取的会员权益被全部/部分取消或暂停使用等操作，不退换已支付的会员服务费用，且不进行任何赔偿或补偿。如给E联车服或相关方造成损失，您需承担全部责任。从公平合理的角度，您应知晓，所有通过该账户实现的操作、产生的全部后果都您自行承担，用户违规行为包含但不限于以下行为：",
                    "1）利用E联车服会员服务进行盈利或非法获利；",
                    "2）任何形式转让或转移E联车服会员服务或E联车服会员权益；",
                    "3）将E联车服会员服务或会员权益借给他人使用、代他人下单；",
                    "4）通过任何不正当手段或以违反城市信用原则的方式开通E联车服会员服务，如恶意绕过正常开通流程；",
                    "5）使用E联车服会员权益获取的商品或服务并非用于个人消费或使用；",
                    "6）用户在使用会员服务时，存在利用秒杀器等工具下单、套取优惠利差、虚假下单交易、提供虚假交易信息等扰乱交易秩序、违反交易规则的行为；",
                    "7）平台判断，您的会员行为不符合公平原则或诚实信用原则的情形。",

                ]],
                ["title" => "五、法律管辖和适用", "content" => [
                    "（一）本协议的订立、执行和解释及争议的解决均应适用在中华人民共和国大陆地区适用之有效法律（但不包括其冲突法规则）。 如发生本协议与适用之法律相抵触时，则这些条款将完全按法律规定重新解释，而其它有效条款继续有效。如缔约方就本协议内容或其执行发生任何争议，双方应尽力友好协商解决；协商不成时，任何一方均可向法院提起诉讼。",

                ]],
                ["title" => "六、无担保和责任限制", "content" => [
                    "（一）您理解本平台将尽最大努力确保本服务及其所涉及的技术及信息安全、有效、准确、可靠，但受限于现有技术及本平台有限的服务能力，对于下述原因导致的合同履行障碍、瑕疵、延后或内容变更等情形，导致您直接或间接损失，您理解E联车服无法承担责任：",
                    "1）因自然灾害、罢工、暴乱、战争、恐怖袭击、政府行为、司法行政命令等不可抗力因素；",
                    "2）因电信部门技术调整或故障、通讯网络故障等公共服务因素；",
                    "3）用户自身设备的软件、系统、硬件和通信线路出现故障，或用户通过非本协议项下约定的方式使用本服务的；",
                    "4) 平台已善意管理，但出现紧急设备/系统的故障/维护、网络信息与数据安全等情况。",
                ]],
                ["title" => "七、其他", "content" => [
                    "（一）如因不可抗力或其它本站无法控制的原因使本站活动服务无法及时提供或无法按本协议进行的，E联车服会合理地尽力协助处理善后事宜。",
                    "（二）用户除遵守该会员协议外，仍应同时遵守《E联车服用户协议》。",

                ]],
                ["title" => "八、第三方SDK", "content" => [
                    "（一）\"com.igexin.sdk\"(个推;个推推送) , 用于用户支付成功时进行通知",
                    "（二） \"com.g.gysdk\"(个推) , 个推相关的SDK",
                    "（三） \"com.getui\"(个推;个推应用统计;个像) , 个推相关的SDK",
                    "（四） \"com.huawei.hms\"(华为;华为推送) , 用于华为手机用户,支付成功时候进行厂商推送",
                    "（五） \"com.huawei.agconnect\"(华为;华为联运应用) , 华为相关SDK",
                    "（六） \"com.amap.api\"(高德地图;高德导航;高德定位;阿里高德地图;高德) , 为方便用户使用导航到店功能,推荐更多优质商户",
                    "（七） \"com.xiaomi.push\"(小米;小米推送) 用于小米手机用户,支付成功时候进行厂商推送",

                ]],
            ],
            "end" => "此版会员协议于2018年12月5日更新发布，自发布日起生效。",
        );
    }


    /**
     * @param $data
     * @return array 2021 03 19
     * @context 会员终身绑定 门店 之后 会员每次升级充值均获得1% 的提成奖励
     */
    static function getMerchantsDeduct($assignBizId = 1, $employeeId, $mark = 'Membership', $price, $memberId, $level_id = 0, $up_level = 0, $orderNumber = '', $formMark, $pay_type)
    {
        # 升级/充值
        $cooperBiz = 0.02;
        if ($mark == 'upgrade') {
            # 升级
            $deductType = 12;
            $settlementType = 3;
            $orderNumber = $level_id . ',' . $up_level;
        } else {
            # 充值
            $deductType = 11;
            $settlementType = 4;
            $orderNumber = 0;
        }
        # 查询是否有绑定的商户
        $bindingInfo = Db::table('log_handlecard')
            ->field("0 employee_id,biz_id")
            ->where("member_id = $memberId and log_resource =3")
            ->order(array('id' => 'desc'))->find();
        if (!empty($bindingInfo)) {
            #查询商户是否存在
            $merchants_status = Db::table('merchants')->where(array('id' => $bindingInfo['biz_id']))->value('status');
            #查询是否关联员工
            $relate_employee = Db::table('merchants')->where(array('id' => $bindingInfo['biz_id']))->value('relate_employee');
            if (!empty($merchants_status) and $merchants_status == 1) {
                $end_settlement = getsPriceFormat(floatval(floatval($price) * $cooperBiz));
                $commission_price = 0;
                # 加结算
                $b = Db::table("biz_settlement")->insert(
                    array("price" => $end_settlement, "commission_price" => $commission_price,
                        "desposit_status" => 1, 'order_price' => $price,
                        'biz_id' => $bindingInfo['biz_id'], 'order_number' => $orderNumber,
                        'create_time' => date('Y-m-d H:i:s'), 'tobe_settled' => $end_settlement,
                        'method' => $pay_type, 'type' => $settlementType, "settlement_status" => 1,
                        'member_id' => $memberId, 'mark' => 2)
                );

                #给商户加收款记录 10会员推荐收入 11 会员充值提成 12 会员升级提成  merchants_income
                Db::table('merchants_income')->insert(
                    array(
                        'merchants_id' => $bindingInfo['biz_id'],
                        'member_id' => $memberId, //是9000 显示为系统奖励
                        'pay_type' => $deductType,
                        'card_pay_type' => $pay_type,
                        'price' => $end_settlement,
                        'level_id' => $orderNumber,
                    ));
//                if (!empty($relate_employee)) {
//                    # 门店加提成  biz_id 为负数 是商户id
//                    $a = Db::table("deduct")->insert(
//                        array(
//                            "order_number" => $orderNumber,
//                            'order_server_id' => $memberId,
//                            'type' => $deductType,
//                            'employee_id' => -$relate_employee,
//                            'deduct' => $end_settlement,
//                            'refer_mark' => 3,
//                            'biz_id' => -$bindingInfo['biz_id'],
//                            'create' => date("Y-m-d H:i:s")
//                        )
//                    );
//                }


            }

        }
        return true;

    }

    static function addLevelRecommend($formark, $level, $member_id, $data = array())
    {
        if ($formark == 'calling') {
            return false;
        }
        $recommend_channel = 0;
        if ($formark == "merchants" or $formark == "merchants_employee") {
            # 商户赠送
            $recommend_channel = 2;
        } else if ($formark == "shareMemberId") {
            # 推广员赠送
            $recommend_channel = 1;
        } else {
            # 合作店赠送查看是否是合作店 （直营店不加）
            if (!empty($data['assign_id'])) {
                $biz_info = Db::table("biz")->where(array("id" => $data['assign_id'], "biz_type" => 3))->find();
                if (!empty($biz_info)) {
                    $recommend_channel = 3;
                }
            }
        }
        if (!empty($recommend_channel) and !empty($level)) {
            # 查询是否有额外赠送
            $present = Db::table("level_recommend")->where(array("level_id" => $level, "recommend_channel" => $recommend_channel))
                ->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")->select();
            if (!empty($present)) {
                $_voucher = array();
                $_integral = 0;
                foreach ($present as $k => $v) {
                    if ($v['num'] > 0) {
                        if ($v['present_type'] == 1) {
                            # 增加卡券
                            for ($i = 0; $i < $v['num']; $i++) {
                                array_push($_voucher, array("voucher_id" => $v['voucher_id'], "member_id" => $member_id, "card_time" => $v['voucher_over'], "voucher_source" => 12, "paytype" => 0,
                                    "voucher_cost" => $v['price'], "card_title" => $v['voucher_title'], "voucher_type" => $v['voucher_type'], "server_id" => $v['service_id']));
                            }
                        } else {
                            $_integral += $v['num'];
                        }
                    }
                }
                if (!empty($_voucher)) {
                    $VoucherService = new VoucherService();
                    $VoucherService->addMemberVoucher($member_id, $_voucher);
                }
                if (!empty($_integral)) {
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($member_id, 0, $_integral, 1);
                }
            }
        }
        return true;
    }

}
