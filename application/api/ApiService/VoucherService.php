<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10
 * Time: 16:08
 */

namespace app\api\ApiService;


use think\Db;

class VoucherService
{
    /**
     * @param $memberid
     * @param array $data
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户卡券列表
     */
    function memberVoucher($memberid,$data=array())
    {
        $where="id >0 ";
        $order="";
        if(key_exists('service',$data)){
            # 查询是否有未完成的预约订单 并查询是否使用了现金抵值券 如使用则本次不可使用
            $appoint_voucher = Db::table("member_voucher mv,orders o")
                ->where(array("mv.member_id"=>$memberid,"o.member_id" =>$memberid, "o.order_type" => 3, "o.order_status" => 1,"mv.status"=>2))
                ->where("o.order_appoin_time>'" . date("Y-m-d H:i:s") . "'")
                ->where("mv.order_id = o.id and o.pay_price>0")
                ->count();
            if($appoint_voucher>0){
                $where.=" and voucher_cost >= {$data['price']}";
            }
            $where.=" and status=1 and voucher_type!=7 and voucher_type!=8 and voucher_type!=9 and voucher_type!=10  and voucher_type!=17 and ((voucher_type!=3 and voucher_type!=1 and voucher_type!=4) or (voucher_type=4 and server_id = {$data['service_id']}))
             and ((voucher_start !=1 and date(start_time)< '".date("Y-m-d")."') or (voucher_start = 1)) and ((money_off=0  or money_off is NULL) or (money_off >0 and money_off <= {$data['price']}))";
        }
        if(key_exists('orderby',$data)){
            $order=$data['orderby'];
        }
        if(key_exists('voucher_type',$data)){
            $where .= " and status = 1 and voucher_type = ".$data['voucher_type'];
        }
        if(key_exists('screen_type',$data)){
            if($data['screen_type']==1){
                $where .= " and voucher_type!=7 and voucher_type!=8 and voucher_type!=9 and voucher_type!=10  and voucher_type!=17 ";
            }elseif($data['screen_type']==3){
                $where .= " and voucher_type = 8";
            }elseif($data['screen_type']==4){
                $where .= " and voucher_type = 9";
            }elseif($data['screen_type']==2){
                $where .= " and voucher_type = 7";
            }
        }
        $list=Db::table("member_voucher")->field("*,date(`create`) expiration_time")->where(array("member_id"=>$memberid))->where($where)->order($order)->select();
        if(!empty($list)){
            foreach($list as $k=>$v){
                if(in_array($v['voucher_type'], array(7, 8, 9, 10, 11, 12, 13, 14, 15, 16))){
                    $list[$k]['cate_pid'] = Db::table("merchants_voucher")->field("cate_pid")->where(array("id"=>$v['voucher_id']))->find()['cate_pid'];
                    $list[$k]['second_cate_pid'] = Db::table("merchants_voucher")->field("second_cate_pid")->where(array("id"=>$v['voucher_id']))->find()['second_cate_pid'];
                    if($v['merchants_id']!=0){
                        $merchantsInfo = Db::table("merchants")->field("title,nh_id")->where(array("id"=>$v['merchants_id']))->find();
                        $list[$k]['merchants_title'] = $merchantsInfo['title'];
                        $list[$k]['nh_id'] = $merchantsInfo['nh_id'];
                    }

                }

            }
        }
        //var_dump(Db::table("member_voucher")->getLastSql());
        return array("list"=>$list,"num"=>count($list));
    }

    function voucherFormat($voucher,$service_id,$mark=true,$carInfo=[]){
        # 查询服务信息
        $serviceInfo = Db::table('service')
            ->field('service_class_id,class_pid,biz_pid,add_type')
            ->where(array('id' => $service_id))
            ->find();
        $nowDate = date("Y-m-d H:i:s");
        foreach ($voucher['list'] as $k => $v) {
            # 是否删除卡券信息
            $is_delete = false;
            # 判断卡券类型
            if ($v['voucher_source'] == 4 or $v['voucher_source'] == 14) {
                $biz_pid_where = false;
            } else {
                # 'biz_pid != 0'
                $biz_pid_where = true;
            }
            $add_type = 0;
            if ($v['create'] < $nowDate) {
                # 已过期
                $is_delete = true;
            }
            if (in_array($v['voucher_type'], array(11, 12, 13, 14, 15, 16))) {
                $cardVoucherInfo = Db::table('merchants_voucher')->where(array('id' => $v['voucher_id']))->find();
                $cardVoucherInfo['card_type_id'] = $v['voucher_type'];
            } else {
                if($v['voucher_type'] != 2) {
                    $cardVoucherInfo = Db::table('card_voucher')->where(array('id' => $v['voucher_id']))->find();
                }else{
                    # 服务通用券
                    $cardVoucherInfo = array('card_type_id'=>2);
                }
            }
            if (empty($cardVoucherInfo)) {
                # 查询结果为空 , 去除该卡券信息
                $is_delete = true;
            }
            if ($cardVoucherInfo['card_type_id'] == 1) {
                # 是否存在该卡券对应的服务
                if ($cardVoucherInfo['server_id'] != $service_id) {
                    # 去除该卡券信息
                    $is_delete = true;
                }
            }elseif ($cardVoucherInfo['card_type_id'] == 2) {
                # 服务通用券
                $is_delete = false;
            } elseif ($cardVoucherInfo['card_type_id'] == 3) {
                # 是否存在该卡券对应的商品
                # 去除该卡券信息
                $is_delete = true;
            } elseif ($cardVoucherInfo['card_type_id'] == 11 or $cardVoucherInfo['card_type_id'] == 12 or $cardVoucherInfo['card_type_id'] == 13) {
                if ($cardVoucherInfo['card_type_id'] == 11) {
                    # 判断该订单下的工单中是否存在该卡券对应的服务(服务通用 门店创建的服务)
                    # "add_type = 2"
                    $add_type = 2;
                } elseif ($cardVoucherInfo['card_type_id'] == 12) {
                    # 判断该订单下的工单中是否存在该卡券对应的服务(商品通用 门店创建的商品)
                    # "add_type = 1"
                    $add_type = 1;
                }
//                    if ($biz_pid_where && $serviceInfo['biz_pid'] == 0) {
//                        # 去除该卡券信息
//                        $is_delete = true;
//                    }
                if ($add_type != 0 && $serviceInfo['add_type'] != $add_type) {
                    # 去除该卡券信息
                    $is_delete = true;
                }
            } elseif ($cardVoucherInfo['card_type_id'] == 14 or $cardVoucherInfo['card_type_id'] == 15 or $cardVoucherInfo['card_type_id'] == 16) {

                # 14 服务分类抵用券 15商品分类抵用券(门店创建的商品) 16全部分类抵用券
                if (empty($cardVoucherInfo['second_cate_pid'])) {
                    # 二级分类为空
                    $classInfo = Db::table('service_class')->field('id')->where('service_class_pid = ' . $cardVoucherInfo['cate_pid'] . ' or id = ' . $cardVoucherInfo['cate_pid'])->select();
                    $classInfo = array_column($classInfo, 'id');
                    if (!in_array($serviceInfo['service_class_id'], $classInfo)) {
                        # 去除该卡券信息
                        $is_delete = true;
                    }
                } else {
                    # 二级分类不为空
                    if ($serviceInfo['service_class_id'] != $cardVoucherInfo['second_cate_pid']) {
                        # 去除该卡券信息
                        $is_delete = true;
                    }
                }

//                    if ($biz_pid_where && $serviceInfo['biz_pid'] == 0) {
//                        # 去除该卡券信息
//                        $is_delete = true;
//                    }
                if ($cardVoucherInfo['card_type_id'] == 15) {
                    # 判断该订单下的工单中是否存在该卡券对应的服务(商品分类 门店创建的商品)
                    # "add_type = 1"
                    $add_type = 1;
                }

                if ($add_type != 0 && $serviceInfo['add_type'] != $add_type) {
                    # 去除该卡券信息
                    $is_delete = true;
                }

                # 判断扫码领的券一个车只能用一次
                if($v['voucher_id']==504 and $mark){

                    # 查询是否用过
                    $isUsed = Db::table("orders o")
                        ->join(array("log_cashcoupon"=>"lc"),"lc.order_number = o.order_number")
                        ->join(array("member_voucher"=>"mv"),"mv.id = lc.voucher_id and mv.voucher_id = 504 and mv.voucher_type=14")
                        ->where("o.car_liences = '" . $carInfo['car_licens'] . "'")
                        ->count("o.id");

                    if($isUsed>0){

                        $is_delete = true;
                    }
                }


            } else {
                # 去除该卡券信息
                $is_delete = true;
            }
            if ($is_delete) {
                # 去除该卡券信息
                unset($voucher['list'][$k]);
                $voucher['num']--;
                continue;
            }
            $Desc = $this->voucherTypeDesc($v);
            $voucher['list'][$k]['TypeTitle'] = $Desc['TypeTitle'];
            $voucher['list'][$k]['voucherDesc'] = $Desc['voucherDesc'];
            $voucher['list'][$k]['voucher_source_title'] = $Desc['voucherSource'];
        }
        return $voucher;
    }

    /**
     * @param $member_id
     * @param $data
     * @return bool
     * @context 用户添加卡券
     */
    function addMemberVoucher($member_id,$data)
    {

        foreach($data as $k=>$v){
            $_voucher=array("voucher_id"=>$v['voucher_id'],"member_id"=>$member_id,"card_time"=>$v['card_time'],
                "create"=>date("Y-m-d H:i:s",strtotime("+{$v['card_time']} days")),
                "voucher_source"=>$v['voucher_source'],"paytype"=>$v['paytype'],"voucher_cost"=>$v['voucher_cost'],"card_title"=>$v['card_title'],
                "voucher_type"=>$v['voucher_type']
                );
            $_log=array("member_id"=>$member_id,"status"=>1,"create_time"=>date("Y-m-d H:i:s"),"source"=>$v['voucher_source']);
            if(!empty($v['payprice'])){
                $_voucher['payprice']=$v['payprice'];
            }
            if(!empty($v['car_type_id'])){
                $_voucher['car_type_id']=$v['car_type_id'];
            }
            if(key_exists('redpacket_id',$v)){
                $_voucher['redpacket_id']=$v['redpacket_id'];
                $_log['type']=1;
            }
            if(key_exists('server_id',$v)){
                $_voucher['server_id']=$v['server_id'];
            }
            if(key_exists('money_off',$v)){
                $_voucher['money_off'] = $v['money_off'];
            }
            if(key_exists('voucher_start',$v)){
                $_voucher['voucher_start']=$v['voucher_start'];
            }
            if(key_exists('merchants_id',$v)){
                $_voucher['merchants_id']=$v['merchants_id'];
            }
            if(key_exists('cate_pid',$v)){
                $_voucher['cate_pid']=$v['cate_pid'];
            }
            if(key_exists('is_type',$v)){
                $_voucher['is_type']=$v['is_type'];
            }
            if(key_exists('t_openid',$v)){
                $_voucher['t_openid']=$v['t_openid'];
            }

            $voucher_id=Db::table("member_voucher")->insertGetId($_voucher);
            $_log['voucher_id']=$voucher_id;
            Db::table("log_voucher")->insert($_log);
        }
        return true;
    }

    /**
     * @param $info
     * @return array
     * @context 卡券类型 和描述
     */
     function voucherTypeDesc($info)
    {
        $voucherSource=$this->voucherChannel($info['voucher_source']);
        $merchats_title="";
        if($info['redpacket_id']== 0  and $info['merchants_id']==0){
            if($info['voucher_type']==3){
                $TypeTitle="商品券";
            }elseif($info['voucher_type']==4){
                $TypeTitle="服务券";
            }elseif($info['voucher_type']==7 or $info['voucher_type']==8){
                $TypeTitle="消费券";
            }elseif($info['voucher_type']==9){
                $TypeTitle="物业券";
            }else{
                $TypeTitle="抵用券";
            }
        }elseif($info['voucher_source']!=15 and $info['redpacket_id']!= 0 and $info['merchants_id']==0){
            $TypeTitle="抵用券";
        }elseif($info['voucher_type']==9){
            $TypeTitle="物业券";
            $merchats_title = $info['merchants_title'];
        }elseif($info['merchants_id']!=0 or $info['voucher_type']==7 or $info['voucher_type']==8){
            $TypeTitle="消费券";
            $merchats_title = $info['merchants_title'];

        }
        $desc=["该卡券只能使用一次，过期后不可使用"];
        # 使用说明
        if(!empty($info['money_off'])){
            array_push($desc,"满".getsPriceFormat($info['money_off'])."可使用");
        }
        if($info['voucher_start']!=1){
            array_push($desc,"获得当天不可使用");
        }
        if(!empty($merchats_title)){
            $voucherSource .= "(".$merchats_title.")";
        }
        return array("TypeTitle"=>$TypeTitle,"voucherDesc"=>$desc,"voucherSource"=>$voucherSource);
    }

    /**
     * @param $source
     * @return string
     * @context 卡券来源
     */
    function voucherChannel($source)
    {
        switch (intval($source)) {
            case 1:
            default:
                return "购买服务";
                break;
            case -1:
                return "服务赠送";
                break;
            case 2:
                return "活动获得";
                break;
            case -2:
                return "活动赠送";
                break;
            case 3:
                return "商家门店赠送";
                break;
            case 4:
                return "现金红包";
                break;
            case 5:
                return "线上会员办理";
                break;
            case 6:
                return "门店会员办理";
                break;
            case 7:
                return "线上升级";
                break;
            case 8:
                return "门店升级";
                break;
            case 9:
                return "线上充值";
                break;
            case 10:
                return "门店充值";
                break;
            case 11:
                return "商家线上赠送";
                break;
            case 12:
                return "平台赠送";
                break;
            case 13:
                return "体验卡激活";
                break;
            case 14:
                return "积分兑换";
                break;
            case 15:
                return "联盟商家";
                break;
            case 19:
                return "任务获得";
                break;
            case 20:
                return "扫码领取";
                break;
        }
    }

    /**
     * @return array
     * @context 卡券规则
     */
    function voucherRule()
    {
        return [
            "syfw"=>["“E联车服”注册用户（下称“用户”）通过“E联车服”产品注册及参与各类市场推广活动所获得的卡券适用本规则。"],
            "sycj"=>array(
                "用户在“E联车服”直营店、加盟店、合作店进行服务之后,结账时出示卡券核销码即可；",
                "用户在“E联车服”平台预约服务或进行相关操作时，根据卡券类型进行抵用；",
                "用户在“E联车服”应用内展示的所有联盟商家消费时，根据卡券类型进行直接抵用现金。",

            ),
            "hdfs"=>array(
                "参与有关活动赠送获得；",
                "会员办理获得；",
                "在“E联车服”平台内购买获得；",
                "领取平台内联盟商家发放获得；",
            ),
            "bksy"=>array(
                "不符合用户所拥有的卡券上所标明的使用条件的，不可使用；",
                "卡券超过使用期限的，不可使用；",
                "平台要求的特殊时间段或特定活动不可使用。",

            ),
            "qtsm"=>array(
                "卡券均不可找零、兑换成提现，一旦用户领取成功，不得转让或者赠与他人使用；",
                "卡券使用期限应当以用户所领取的卡券标明的使用时间为准，过期作废。",
                "一旦卡券被用户成功使用，将从用户“我的卡券”中消失，不再展示。用户可在“我的卡券”中查询当前未使用和已过期的卡券；",
                "用户在获取和使用卡券的过程中，如果出现违规行为（如作弊领取、恶意套现等），E联车服有权取消用户的卡券，并可撤销相关违规交易；",
                "本规则自发布之日起生效。E联车服有权根据市场的实际情况在适用法律所允许的范围之内对卡券的使用规则进行调整和修改；",
                "E联车服有权在适用法律所允许的范围内对卡券的领取及相关活动和使用规则进行解释。",
            ),
        ];
    }

    /**
     * @param $member_id
     * @param $redpacket_id
     * @param $type
     * @param int $num
     * @param array $data
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 增加红包卡券
     */
    function addRedpacket($member_id,$redpacket_id,$type=1,$num=1,$data=array(),$big_redPacket_id=0)
    {
        # 查询红包信息
        $redpacketInfo=Db::table("redpacket_detail")->where(array("redpacket_id"=>$redpacket_id,"type"=>$type))->select();
        $_voucher=array();
        if(!empty($redpacketInfo)){
            foreach($redpacketInfo as $k=>$v){
                $voucher_id = "";
                $is_type=0;
                $cate_pid="";
                if($v['voucher_type']==7){
                    $voucher_id = $v['voucher_id'];
                    $is_type = 2;
                    $cate_pid = Db::table("merchants_voucher")->field("cate_pid")->where(array("id"=>$v['voucher_id']))->find()['cate_pid'];
                }
                if($v['voucher_type']==8){
                    $voucher_id = $v['voucher_id'];
                    $is_type = 1;
                    $cate_pid = Db::table("merchants_voucher")->field("cate_pid")->where(array("id"=>$v['voucher_id']))->find()['cate_pid'];
                }
                if($v['voucher_type']==9){
                    $voucher_id = $v['voucher_id'];
                    $is_type = 3;
                }
                if($v['voucher_type']==10){
                    $voucher_id = $v['voucher_id'];
                    $is_type = 4;
                }
                for($i=0;$i<$num;$i++){
                    for($k=0;$k<$v['voucher_number'];$k++){

                        array_push($_voucher,array("voucher_id"=>$voucher_id,"member_id"=>$member_id,"card_time"=>$v['voucher_over'],
                            "voucher_source"=>$data['voucher_source'],"paytype"=>$data['paytype'],"voucher_cost"=>$v['voucher_price'],
                            "card_title"=>$v['voucher_title'],"voucher_type"=>$v['voucher_type'],"redpacket_id"=>$redpacket_id,"server_id"=>$v['voucher_id'],
                            "money_off"=>$v['money_off'],"voucher_start"=>$v['voucher_start'],"is_type"=>$is_type,"cate_pid"=>$cate_pid));
                    }
                }
            }
            $this->addMemberVoucher($member_id,$_voucher);
            # 增加领取红包记录
            Db::table("redpacket_log")->insert(array("redpacket_id"=>$big_redPacket_id,"member_id"=>$member_id,"create_time"=>date("Y-m-d H:i:s"),
                "type"=>$type,"redpacket_title_id"=>$redpacket_id));
        }
        return true;
    }

    /**
     * @param $redpacket_id
     * @param $type
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取红包的卡券
     */
    function redpacketVoucher($redpacket_id,$type){
        $list=Db::table("redpacket_detail rd")
            ->field("rd.*,date_add(curdate(), interval rd.voucher_over day) period_time")
            ->where(array("redpacket_id"=>$redpacket_id,"type"=>$type))
            ->select();
        return $list;
    }

    /**
     * @param $num
     * @param string $_where
     * @param string $member_id
     * @return array|\PDOStatement|string|\think\Collection|\think\model\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取红包记录
     */
    function redpacketLog($num,$type,$_where='',$member_id=''){
        $where=" rl.id >0 ";
        if(!empty($member_id)){
            $where.=" and rl.member_id = {$member_id} ";
        }
        if(!empty($where)){
            $where.=$_where;
        }
        $list=Db::table("redpacket_log rl")
            ->field("rl.*,m.nickname,rt.redpacket_title small_red_title,rt.price small_price")
            ->leftJoin("redpacket_title rt","rt.id = rl.redpacket_title_id")
            ->leftJoin("member m","m.id = rl.member_id")
            ->where("type","=",$type)
            ->where($where)
            ->order("rl.id desc");
        if(!empty($num)){
            $list=$list->limit($num);
        }
        $list=$list->select();
        return $list;
    }

    /**
     * @param $member_id
     * @param $num
     * @param $voucher_id
     * @param $voucher_source
     * @param int $redpacket_id
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户添加消费券
     */
    function addMerchantVoucher($member_id,$num,$voucher_id,$voucher_source,$redpacket_id=0)
    {
        $voucher = array();
        $merchantInfo = Db::table("merchants_voucher")->where(array("id"=>$voucher_id))->find();
        if(!empty($merchantInfo)){
            $giving_type = 0;
            if($merchantInfo['is_type']==2){
                # 联盟商家
                $giving_type = 7;
            }elseif($merchantInfo['is_type']==3){
                # 物业缴费
                $giving_type = 9;
            }elseif($merchantInfo['is_type']==1){
                # 本地生活
                $giving_type = 8;
            }elseif ($merchantInfo['is_type']==4){
                # 有派精品购
                $giving_type = 10;
            }else{
                $giving_type = $merchantInfo['is_type'];
            }

            for ($i = 0; $i < $num; $i++) {
                array_push($voucher, array(
                    'voucher_id' => $merchantInfo['id'],
                    'member_id' => $member_id,
                    'card_time' => $merchantInfo['validity_day'],
                    'voucher_source'=>$voucher_source,
                    'voucher_cost' => $merchantInfo['price'],
                    'card_title' => $merchantInfo['title'],
                    'voucher_type' =>$giving_type,
                    'payprice' => $merchantInfo['price'],
                    'car_type_id' => 0,
                    "redpacket_id"=>$redpacket_id,
                    'server_id' => 0,
                    'money_off'=>$merchantInfo['min_consume'],
                    "voucher_start"=>$merchantInfo['voucher_start'],
                    "is_type"=>$merchantInfo['is_type'],
                    "cate_pid"=>$merchantInfo['cate_pid'],
                    'paytype'=>0
                ));
            }
            if(!empty($voucher)){
                $this->addMemberVoucher($member_id,$voucher);
            }
        }
        return true;
    }
}
