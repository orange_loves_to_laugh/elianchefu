<?php


namespace app\api\controller;


use app\api\ApiService\MemberService;
use app\service\OcrService;
use app\service\ResourceService;
use Redis\Redis;
use think\Db;

class Inquiry extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询今日 4家 保险公司的活动
     */
    function getActiveInfo()
    {
        # 当前时间
        $nowTime = date('Y-m-d H:i:s');
        # 查询四个保险公司的活动
        $info = Db::table('inquiry_active inac')
            ->field('insa.title,inac.id,inac.safe_id,inac.active_explain,compulsory_rebate,car_rebate,business_rebate')
            ->join('inquiry_safe insa', 'inac.safe_id=insa.id', 'left')
            ->where("active_start <= '" . $nowTime . "' and active_end >= '" . $nowTime . "'")
            ->group('safe_id')
            ->order('inac.id desc')
            ->limit(4)
            ->select();
        if (!empty($info)) {
            # 查询每个活动的赠送
            foreach ($info as $k => $v) {
                $givingInfo = Db::table('inquiry_active_giving')
                    ->field("CASE giving_type 
                    WHEN 1 THEN '商品' 
                    WHEN 2 THEN '服务' 
                    WHEN 3 THEN '会员' 
                    WHEN 4 THEN '积分' 
                    WHEN 5 THEN '余额'  
                    WHEN 7 THEN '联盟商家卡券' 
                    WHEN 8 THEN '本地生活卡券' 
                    WHEN 9 THEN '物业缴费卡券' 
                    WHEN 10 THEN '有派精品购卡券' 
                    WHEN 11 THEN '服务通用卡券' 
                    WHEN 12 THEN '商品通用卡券'
                    WHEN 13 THEN '全部通用卡券'
                    WHEN 14 THEN '服务分类卡券'
                    WHEN 15 THEN '商品分类卡券'
                    WHEN 16 THEN '全部分类卡券'
                    END giving_type_title,
                    giving_type,
                    sum(giving_price) price")
                    ->where(array('active_id' => $v['id']))
                    ->group('giving_type')
                    ->select();
                $info[$k]['givingInfo'] = $givingInfo;
            }
        }
        # 活动通知
        $activeTips = "每个人都有不可告人的秘密,有自己的渴望,欲求,以及难以启齿的需要. 所以,日子要过下去,人就要学会宽容";
        return array('status' => true, 'msg' => '查询成功', 'listInfo' => $info, 'activeTips' => $activeTips);
    }

    # 获取报价
    function getQuote()
    {
        # 手机号--(不是平台注册用户,弹窗提示/禁止提交)
        $phone = input('post.phone');
        if (empty($phone)) {
            return array('status' => false, 'msg' => '请填写被保险人实名制手机号码');
        }
        if (!CheckMobile($phone)) {
            return array('status' => false, 'msg' => '手机号不正确');
        }
        # 通过手机号查询是否注册
        $memberInfo = Db::table('member m')
            ->field('m.id,member_code,member_level_id')
            ->join('member_mapping mm', 'm.id=mm.member_id', 'left')
            ->where(array('member_phone' => $phone))
            ->find();
        if (empty($memberInfo)) {
            return array('status' => false, 'msg' => '该手机号平台未注册,请先注册');
        }
        /*return array(
            'status' => true,
            'msg' => '提交成功',
            'carInfo' => array('car_licens' => '辽AABCDE',
                'car_recogin_code' => 'jkasdgasdf',
                'register_time' => '2021-10-10',//行驶证注册时间
                'grant_register' => '2022-10-10',//行驶证发证日期
                'status' => 2,//认证状态  1未认证 2已认证 3删除
                'enginecode' => '650573613634916704',//发动机号码
                'owner_name' => '筱志',//车主姓名
                'vehicle_img' => 'https://baidu.com/',//行驶证照片地址
                'create_time' => date('Y-m-d H:i:s'),
                'member_id' => $this->MemberId,
                'model' => '品牌型号',//品牌型号
                'vehicleType' => '车辆类型',//车辆类型
            ),
            'memberInfo' => array(
                'name' => '筱志先生',
                'phone' => $phone
            ),
            'insuredAmount' => array(
                'third' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'driver' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'seat' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'scratch' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
            )
        );*/
        # 上传身份证-接口识别
        $idCard_img = input('post.idCard_img');
        $idCard_imgUrl = ResourceService::uploadBase($idCard_img, 'idcard');
        if (is_array($idCard_imgUrl)) {
            return $idCard_imgUrl;
        }
        # 识别成功-该用户直接进行实名制信息更新-(身份证信息也存)
        $ocr = new OcrService();
        $ocrRes = $ocr->OcrIdCard($idCard_imgUrl);
        if ($ocrRes['status']) {
            //$ocrRes['IDNumber'] 身份证号
            //$ocrRes['name'] 姓名
            //$ocrRes['sex'] 性别
            # 性别
            $sex = getSexInfo($ocrRes['sex']);
            # 通过身份证号获取年龄/出生年月日
            $ageInfo = getAge($ocrRes['IDNumber']);
            # 修改用户信息
            Db::table('member')->where(array('id' => $memberInfo['id']))
                ->update(array(
                    'member_name' => $ocrRes['name'],//姓名
                    'member_age' => $ageInfo['age'],//年龄
                    'member_sex' => $sex,//性别
                    'member_born_year' => $ageInfo['year'],
                    'member_born_month' => $ageInfo['month'],
                    'member_born_day' => $ageInfo['day'],
                    'member_born' => $ageInfo['born'],
                    'member_idcard' => $ocrRes['IDNumber'],//身份证号
                    'idcard_url' => imgUrl($idCard_imgUrl)
                ));
            Db::table('member_mapping')->where(array('member_id' => $memberInfo['id']))->update(array('member_status' => 2));
            # 更新redis
            $memberService = new MemberService();
            $memberService->changeMemberInfo($memberInfo['id']);
        } else {
            return $ocrRes;
        }
        # 上传行驶证-接口识别
        $vehicle_img = input('post.vehicle_img');
        $vehicle_imgUrl = ResourceService::uploadBase($vehicle_img, 'vehicle');
        if (is_array($vehicle_imgUrl)) {
            return $vehicle_imgUrl;
        }
        # 识别成功-存储辆信息-(按车牌号进行信息修改)
        $ocrRes_vehicle = $ocr->OcrVehicleLicense($vehicle_imgUrl);
        if ($ocrRes_vehicle['status']) {
            $carInfo = $ocrRes_vehicle['carInfo'];
            $carInfo['car_licens'] = $ocrRes_vehicle['carInfo']['licensePlateNumber'];
            $carInfo['vehicle_img'] = imgUrl($vehicle_imgUrl);
            # 查询该用户是否存在该车辆
            $memberCarInfo = Db::table('member_car')
                ->field('id')
                ->where(array('member_id' => $memberInfo['id'], 'car_licens' => trim($ocrRes_vehicle['carInfo']['licensePlateNumber'])))
                ->find();
            if (!empty($memberCarInfo)) {
                # 存在-修改信息
                Db::table('member_car')
                    ->where(array('id' => $memberCarInfo['id']))
                    ->update(array(
                        'car_recogin_code' => $ocrRes_vehicle['carInfo']['vinCode'],//车辆识别代码
                        'register_time' => $ocrRes_vehicle['carInfo']['registrationDate'],//行驶证注册时间
                        'grant_register' => $ocrRes_vehicle['carInfo']['issueDate'],//行驶证发证日期
                        'status' => 2,//认证状态  1未认证 2已认证 3删除
                        'enginecode' => $ocrRes_vehicle['carInfo']['engineNumber'],//发动机号码
                        'owner_name' => $ocrRes_vehicle['carInfo']['owner'],//车主姓名
                        'vehicle_img' => imgUrl($vehicle_imgUrl)//行驶证照片地址
                    ));
            } else {
                # 不存在-添加车辆信息
                Db::table('member_car')
                    ->insert(array(
                        'car_licens' => $ocrRes_vehicle['carInfo']['licensePlateNumber'],
                        'car_recogin_code' => $ocrRes_vehicle['carInfo']['vinCode'],
                        'register_time' => $ocrRes_vehicle['carInfo']['registrationDate'],//行驶证注册时间
                        'grant_register' => $ocrRes_vehicle['carInfo']['issueDate'],//行驶证发证日期
                        'status' => 2,//认证状态  1未认证 2已认证 3删除
                        'enginecode' => $ocrRes_vehicle['carInfo']['engineNumber'],//发动机号码
                        'owner_name' => $ocrRes_vehicle['carInfo']['owner'],//车主姓名
                        'vehicle_img' => imgUrl($vehicle_imgUrl),//行驶证照片地址
                        'create_time' => date('Y-m-d H:i:s'),
                        'member_id' => $memberInfo['id']
                    ));
            }
        } else {
            return $ocrRes_vehicle;
        }
        # 返回信息--(车牌号/车型/车辆品牌/系别/提交的电话)/(投保险种信息(投保金额(代码写死)))
        # 姓名
        if ($sex == 1) {
            $name = mb_substr($ocrRes['name'], 0, 1) . '先生';
        } else {
            $name = mb_substr($ocrRes['name'], 0, 1) . '女士';
        }
        if ($memberInfo['member_level_id'] > 1) {
            $member_status = 3;
        } else {
            $member_status = 2;
        }

        return array(
            'status' => true,
            'msg' => '提交成功',
            'carInfo' => $carInfo,
            'memberInfo' => array(
                'name' => $name,
                'phone' => $phone,
                'idcard' => $ocrRes['IDNumber'],
                'member_status' => $member_status
            ),
            'insuredAmount' => array(
                'third' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'driver' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'seat' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
                'scratch' => array(
                    array('actual_num' => 10, 'show_num' => '10万'),
                    array('actual_num' => 20, 'show_num' => '20万'),
                    array('actual_num' => 30, 'show_num' => '30万'),
                    array('actual_num' => 40, 'show_num' => '40万'),
                    array('actual_num' => 50, 'show_num' => '50万'),
                    array('actual_num' => 60, 'show_num' => '60万')
                ),
            )
        );
    }

    function quoteSubmit()
    {
        //carInfo: this.carInfo,
        //          memberInfo: this.memberInfo,
        //          sele_index:this.sele_index
        # 车辆信息
        $carInfo = input('post.carInfo');
        # 用户信息
        $memberInfo = input('post.memberInfo');
        # 用户咨询报价信息
        $quote_info = input('post.sele_index');
        if (empty($quote_info)) {
            return array('status' => false, 'msg' => '请补全信息');
        }
        # 添加询价信息
        $res = Db::table('inquiry_history')
            ->insertGetId(array(
                'license_num' => $carInfo['licensePlateNumber'],
                'license_modle' => $carInfo['model'],
                '车辆类型' => $carInfo['vehicleType'],
                'member_id' => $this->MemberId,
                'create_time' => date('Y-m-d H:i:s'),
                'member_status' => $memberInfo['member_status'],//用户状态 1未注册 2注册用户 3平台会员
                'source' => 1,
                'member_name' => $memberInfo['name'],
                'member_phone' => $memberInfo['phone'],
                'member_card' => $memberInfo['idcard'],
                'driv_license' => $carInfo['vehicle_img'],
                'frame_number' => $carInfo['vinCode'],
                'engine_number' => $carInfo['engineNumber'],
                'handle_status' => 1
            ));
        # 添加 咨询险种及投保金额 信息
        Db::table('inquiry_history_user')
            ->insert(array(
                'inquiry_history_id' => $res,
                'third_inquiry' => $quote_info[0],
                'driver_inquiry' => $quote_info[1],
                'seat_inquiry' => $quote_info[2],
                'scratch_inquiry' => $quote_info[3],
            ));
        return array('status' => true, 'msg' => '提交成功,请耐心等待');
    }

    // 询价记录
    function inquiryRecord()
    {
        # 查询该用户提交的历史询价记录
        $listInfo = Db::table('inquiry_history')
            ->field('id,license_num,license_modle,member_phone,member_name,handle_status,handle_read,vehicle_type,
            (case handle_status when 1 then "未报价" when 2 then "已报价" when 3 then "已投保" end) handle_status_show,
            (case handle_read when 1 then "未读" when 2 then "已读" end) handle_read_show
            ')
            ->where(array('member_id' => $this->MemberId))
            ->select();
        if (!empty($listInfo)) {
            # 查询活动赠送
            foreach ($listInfo as $k => $v) {
                # 保险类型(商业险)
                $listInfo[$k]['insurance_type'] = '商业险';
                # 赠送卡券(价值,,元)
                $listInfo[$k]['giving_total'] = '价值500.00元';
            }
        }
        # 查询该用户提交的投保记录记录
        $insureList = Db::table('inquiry_history')
            ->field('id,license_num,license_modle,member_phone,member_name,handle_status,handle_read,vehicle_type,
            (case handle_status when 1 then "未报价" when 2 then "已报价" when 3 then "已投保" end) handle_status_show,
            (case handle_read when 1 then "未读" when 2 then "已读" end) handle_read_show
            ')
            ->where(array('member_id' => $this->MemberId))
            ->where(array('handle_status' => 3, 'audit_status' => 1))
            ->select();
        if (!empty($insureList)) {
            # 查询活动赠送
            foreach ($insureList as $k => $v) {
                # 保险类型(商业险)
                $insureList[$k]['insurance_type'] = '商业险';
                # 赠送卡券(价值,,元)
                $insureList[$k]['giving_total'] = '价值500.00元';
            }
        }
        $handleTips = '等待报价(无法查看详情)';
        return array('status' => true, 'msg' => '查询成功', 'listInfo' => $listInfo, 'insureList' => $insureList, 'handleTips' => $handleTips);
    }

    // 询价记录详情
    function inquiryRecord_detail()
    {
        # 接收询价记录id
        $id = input('post.id');
        if (empty($id)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        # 修改状态
        Db::table('inquiry_history')->where(array('id' => $id))->update(array('handle_read' => 2));
        # 保险公司id
        $safe_id = input('post.safe_id');
        if (!empty($safe_id)) {
            $where = array('if.safe_id' => $safe_id);
        } else {
            $where = array();
        }
        # 查询报价详情
        $detailInfo = Db::table('inquiry_offer iof')
            ->field('iof.id,safe_id,total_price,back_price,give_id,offer_explain,payment_img,title,active_explain,pay_price,give_id')
            ->join('inquiry_safe isa', 'isa.id=iof.safe_id', 'left')
            ->where(array('inquiry_history_id' => $id))
            ->where($where)
            ->select();
        if (!empty($detailInfo)) {
            # 查询活动赠送
            foreach ($detailInfo as $k => $v) {
                # 查询报价险种明细
                $offer_detail = Db::table('inquiry_offer_detail')
                    ->field('insurance_id,insured_price,premium_price,back_price,type,
                    (case type
                     when 1 then "交强险"
                     when 2 then "车船税"
                     when 3 then "车损险"
                     when 4 then "三者险"
                     when 5 then "司机险"
                     when 6 then "座位险"
                     when 7 then "车身划痕险"
                     end) type_title')
                    ->where(array('offer_id' => $v['id']))
                    ->where("type > 2")
                    ->select();
                $detailInfo[$k]['offer_detail'] = $offer_detail;
                # 保险类型(商业险)
                $detailInfo[$k]['insurance_type'] = '商业险';
                # 查询活动赠送
                $active_giving = Db::table('inquiry_active_giving')
                    ->field("CASE giving_type 
                    WHEN 1 THEN '商品' 
                    WHEN 2 THEN '服务' 
                    WHEN 3 THEN '会员' 
                    WHEN 4 THEN '积分' 
                    WHEN 5 THEN '余额'  
                    WHEN 7 THEN '联盟商家卡券' 
                    WHEN 8 THEN '本地生活卡券' 
                    WHEN 9 THEN '物业缴费卡券' 
                    WHEN 10 THEN '有派精品购卡券' 
                    WHEN 11 THEN '服务通用卡券' 
                    WHEN 12 THEN '商品通用卡券'
                    WHEN 13 THEN '全部通用卡券'
                    WHEN 14 THEN '服务分类卡券'
                    WHEN 15 THEN '商品分类卡券'
                    WHEN 16 THEN '全部分类卡券'
                    END giving_type_title,
                    giving_type,
                    sum(giving_price) price")
                    ->where(array('active_id' => $v['id']))
                    ->group('giving_type')
                    ->select();
                $detailInfo[$k]['active_giving'] = $active_giving;
                # 赠送卡券(价值,,元)
                $detailInfo[$k]['giving_total'] = '价值' . array_sum(array_column($active_giving, 'price')) . '元';
            }
        }
        return array('status' => true, 'msg' => '加载成功', 'detailInfo' => $detailInfo);
    }

    function insureNow()
    {
        $id = input('post.id');
        $safe_id = input('post.safe_id');
        if (empty($id) or empty($safe_id)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        $res = Db::table('inquiry_history')
            ->where(array('id' => $id))
            ->update(array('safe_id' => $safe_id));
        return array('status' => true, 'msg' => '加载成功');
    }

    #  通过车牌号查询 是否存在询价
    function cashBack_find()
    {
        # 车牌号
        $car_num = input('post.car_num');
        if (empty($car_num)) {
            return array('status' => false, 'msg' => '请填写正确的车牌号码');
        }
        # 查询 询价历史
        $info = Db::table('inquiry_history')
            ->where(array('license_num' => $car_num, 'handle_status' => 3, 'audit_status' => 0))
            ->find();
        if (empty($info)) {
            # 没有信息
            $cashBack_mark = 2;
            $cashBack_tips = '未投保';
        } else {
            # 有信息
            $cashBack_mark = 1;
            $cashBack_tips = '正常';
        }
        return array('status' => true, 'cashback_mark' => $cashBack_mark, 'cashBack_tips' => $cashBack_tips);
    }

    # 询价返现-提交支付凭证
    function cashBack()
    {
        $redis = new Redis();
        $is_repeat = $redis->lock("cashBack_submit" . $this->MemberId, 60);
        if (!$is_repeat) {
            return true;
        }
        # 车牌号
        $car_num = input('post.car_num');
        # 手机号
        $phone = input('post.phone');
        # 支付金额
        $price = input('post.price');
        # 支付凭证
        $img_info = input('post.img_info');
        $info = Db::table('inquiry_history')
            ->where(array('license_num' => $car_num, 'handle_status' => 3, 'audit_status' => 0))
            ->find();
        if (!empty($info)) {
            Db::table('inquiry_history')->where(array('id' => $info['id']))->update(array('pay_img' => $img_info));
            return array('status' => true, 'msg' => '提交成功');
        } else {
            return array('status' => false, 'msg' => '无投保记录');
        }
    }
}