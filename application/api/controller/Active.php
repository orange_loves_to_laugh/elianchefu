<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/5
 * Time: 15:01
 */

namespace app\api\controller;


use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\MemberService;
use app\api\ApiService\SubsidyService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class Active extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content  查询活动列表
     */
    function listInfo()
    {
//        dump($this->MemberInfo);
        #判断查询类型
        if ($this->MemberInfo['member_level_id'] == 0) {
            # 不是会员
            $usePerson = 'use_person = 2 or use_person = 3';
        } else {
            # 是会员
            $usePerson = 'use_person = 1 or use_person = 3';
        }
        # 查询所有活动
        $currentTime = date('Y-m-d H:i:s');
        $listInfo = Db::table('active')
            ->field('id,active_title title,active_end,active_explain content,active_biz original_price,active_online selling_price,active_imgurl img,active_mold state,use_person,
             case active_mold 
                            when 1 then \'特价商品\'
                            when 2 then \'特价服务\'
                            when 3 then \'会员办理\'  end tag,active_limit')
//            ->where($usePerson)
            ->where("active_start <= '" . $currentTime . "' and active_end > '" . $currentTime . "'")
            ->where("active_num > 0 and active_putstatus = 2")
            ->order(array('id' => 'desc'))
            ->select();
        # 结束时间
        $aTime = array();
        if (!empty($listInfo)) {
            foreach ($listInfo as $k => $v) {
                if ($v['use_person'] == 1) {
                    $listInfo[$k]['usePerson'] = '会员专享';
                } else {
                    $listInfo[$k]['usePerson'] = '';
                }
                array_push($aTime, timediff($currentTime, $v['active_end'], false)['timediff']);
            }
        }
        return array('status' => true, 'msg' => '查询成功', 'info' => $listInfo, 'aTime' => $aTime);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询活动详情
     */
    function activeDetail()
    {
        # 接收活动id
        $activeId = input('post.id');
        if (empty($activeId)) {
            return array('status' => false, 'msg' => '系统错误');
        }
        $info = $this->activeInfo($activeId);
        return array('status' => true, 'msg' => '查询成功', 'info' => $info);
    }

    /**
     * @param $activeId
     * @return array|bool|mixed|null|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 活动详情
     */
    function activeInfo($activeId)
    {
        $redis = new Redis();
        $redis->hDel('activeInfo', $activeId);
        $info = $redis->hGetJson('activeInfo', $activeId);
        if (empty($info)) {
            # 查询活动详情
            $info = Db::table('active')
                ->field('active.id,active_title title,date_format(active_end,"%Y/%m/%d %H:%i:%s") active_end,active_explain content,active_biz original_price,active_online selling_price,active_imgurl img,active_mold state,
             case active_mold 
                            when 1 then \'特价商品\'
                            when 2 then \'特价服务\'
                            when 3 then \'会员办理\'  end tag,content_detail,active_picurl,active_limit,use_person')
                ->join('active_content', 'active.id=active_id', 'left')
                ->where(array('active.id' => $activeId))
                ->find();
            if (!empty($info['content_detail'])) {
                $info['detailImg'] = explode(',', $info['content_detail']);
            }
            if (!empty($info['active_picurl'])) {
                $info['banner'] = explode(',', $info['active_picurl']);
            }
            # 查询活动详情
            $info['detailList'] = Db::table('active_detail')
                ->where(array('active_id' => $activeId))
                ->select();
            $info['service_id'] = $info['detailList'][0]['detail_id'];
            # 查询活动赠送
            $info['activeGiving'] = Db::table('active_giving')
                ->where(array('active_id' => $activeId))
                ->order(array('giving_type' => 'asc'))
                ->select();
            if (!empty($info['activeGiving'])) {
                foreach ($info['activeGiving'] as $k => $v) {
                    if ($v['giving_type'] == 4) {
                        $info['activeGiving'][$k]['type'] = '赠送账户积分';
                    } elseif ($v['giving_type'] == 5) {
                        $info['activeGiving'][$k]['type'] = '赠送账户余额';
                    } else {
                        if ($v['giving_type'] == 1) {
                            $info['activeGiving'][$k]['type'] = '商品券';
                        }
                        if ($v['giving_type'] == 2) {
                            $info['activeGiving'][$k]['type'] = '服务券';
                        }
                        # 查询卡券详情
                        $info['activeGiving'][$k]['cardTitle'] = Db::table('card_voucher')->field('card_title')
                            ->where(array('id' => $v['giving_id']))->find()['card_title'];
                        $info['activeGiving'][$k]['expiration_time'] = date('Y-m-d', strtotime('+ ' . $v['expiration'] . ' days'));
                    }
                }
            }
            # 活动限制
            $info['logNum'] = Db::table('log_active')->where(array('member_id' => $this->MemberId, 'active_id' => $activeId))->count();
            #判断是否可以参加
            $info['canJoin'] = 1;
            if ($info['use_person'] == 1) {
                # 只有会员能参加
                if ($this->MemberInfo['member_level_id'] == 0) {
                    $info['canJoin'] = 0;
                    $info['canJoinMsg'] = '仅限平台储值用户参加';
                }
            }
            if ($info['use_person'] == 2) {
                # 只有非会员能参加
                if ($this->MemberInfo['member_level_id'] != 0) {
                    $info['canJoin'] = 0;
                    $info['canJoinMsg'] = '仅限普通用户参加';
                }
            }
            # 分享跳转地址
            $info['jumpUrl'] = 'http://web.ecarfu.com/pages/index/activeDetailed?id=' . $activeId . '&shareMemberId=' . $this->MemberId;
            # 支付成功分享跳转地址
            $info['paySuccessUrl'] = config('share.paySuccess') . '?shareMemberId=' . $this->MemberId;
            $redis->hSetJson('activeInfo', $activeId, $info);
        }
        return $info;
    }

    function activePay()
    {
        $pay_price = floatval(input("post.pay_price")) * 100;
        $order = [
            'out_trade_no' => input("post.order_number"),
            'total_fee' => '1', // **单位：分**
            'body' => input("post.body"),
            'openid' => input('post.openid'),
        ];
        $PayMent = new PayMent();
        $res = $PayMent->wxPayMp($order);
        return array("status" => true, "data" => json_decode($res, true));
    }

    function activeOrderNumber()
    {
        $id = input('post.id');
        $pay_type = input('post.pay_type');
        if ($pay_type != 5) {
            # 查询活动类型
            $use_person = Db::table('active')->field('use_person')->where(array('id' => $id))->find()['use_person'];
            if ($use_person == 1) {
                # 会员专享
                if ($this->MemberInfo['member_level_id'] == 0) {
                    return array('status' => false, 'msg' => '该活动仅支持余额支付');
                }
            }
        }
        $order_number = date('YmdHis') . time();
        return array('status' => true, 'order_number' => $order_number);
    }

    function activePaySuccess()
    {
        # 接收活动id
        $activeId = input('post.id');
        # 选择的套餐
        $setmeal_id = input('post.setmeal_id');
        # 支付方式(1微信 2支付宝  3银联  4现金 5余额)
        $payType = input('post.payType');
        $order_number = input("post.order_number");
        $employee_id = input("post.employee_id");
        $assign_id = input("post.assign_id");
        $shareMemberId = input("post.shareMemberId");
        # 查询活动详情
        $activeInfo = $this->activeInfo($activeId);
        $currentTime = date('Y-m-d H:i:s');
        if ($payType == 5) {
            # 判断余额是否足够
            if ($this->MemberInfo['member_balance'] < $activeInfo['selling_price']) {
                return array('status' => false, 'msg' => '余额不足');
            }
        }
        if ($activeInfo['state'] != 3) {
            # 添加消费记录
            Db::table('log_consump')
                ->insert(array(
                    'member_id' => $this->MemberId,
                    'consump_price' => $activeInfo['selling_price'],
                    'consump_pay_type' => $payType,
                    'consump_time' => $currentTime,
                    'log_consump_type' => 1,
                    'consump_type' => 1,
                    'income_type' => 4
                ));
        }
        if ($payType == 5) {
            $balanceService = new BalanceService();
            $balanceService->operaMemberbalance($this->MemberId, $activeInfo['selling_price'], 2, []);
        }
        # 增加的用户卡券
        $memberVoucher = array();
        # 活动内容
        if (!empty($activeInfo['detailList'])) {
            foreach ($activeInfo['detailList'] as $k => $v) {
                if ($v['active_type'] == 1 or $v['active_type'] == 2) {
                    # 卡券
                    if ($v['active_type'] == 1) {
                        # 商品
                        $card_type_id = 3;
                        $voucher_type = 3;
                    } else {
                        # 服务
                        $card_type_id = 1;
                        $voucher_type = 4;
                    }
                    $voucherInfo = Db::table('card_voucher')
                        ->where(array('server_id' => $v['detail_id'], 'card_type_id' => $card_type_id))->find();
                    if (!empty($voucherInfo)) {
                        for ($i = 0; $i < $v['detail_number']; $i++) {
                            array_push($memberVoucher, array(
                                'voucher_id' => $voucherInfo['id'],
                                'member_id' => $this->MemberId,
                                'create' => date('Y-m-d H:i:s', strtotime('+ ' . $v['expiration'] . 'days')),
                                'payprice' => $v['detail_price'],
                                'car_type_id' => 0,
                                'voucher_cost' => $voucherInfo['card_price'],
                                'card_title' => $voucherInfo['card_title'],
                                'card_time' => $v['expiration'],
                                'server_id' => $v['detail_id'],
                                'start_time' => $currentTime,
                                'voucher_type' => $voucher_type,
                                'settlement_price' => $v['settlement_price'],
                            ));
                        }
                    }
                } else {

                    # 办理会员
                    LevelService::applyMember($this->MemberId, $v['detail_id'], '', $setmeal_id, array("consump_price" => $activeInfo['selling_price'], "pay_type" => $payType, "consump_type" => 1,
                        "order_number" => $order_number, "biz_id" => 1, 'employee_id' => $employee_id, 'assign_id' => $assign_id, 'shareMemberId' => $shareMemberId));
                }
            }
        }
        # 活动赠送
        if (!empty($activeInfo['activeGiving'])) {
            $integralService = new IntegralService();
            $balanceService = new BalanceService();
            foreach ($activeInfo['activeGiving'] as $k => $v) {
                if ($v['giving_type'] == 1 or $v['giving_type'] == 2) {
                    # 1 商品卡券  2 服务卡券
                    if ($v['giving_type'] == 1) {
                        # 商品
                        $voucher_type = 3;
                    } else {
                        # 服务
                        $voucher_type = 4;
                    }
                    $voucherInfo = Db::table('card_voucher')
                        ->where(array('id' => $v['giving_id']))->find();
                    if (!empty($voucherInfo)) {
                        for ($i = 0; $i < $v['giving_number']; $i++) {
                            array_push($memberVoucher, array(
                                'voucher_id' => $voucherInfo['id'],
                                'member_id' => $this->MemberId,
                                'create' => date('Y-m-d H:i:s', strtotime('+ ' . $v['expiration'] . 'days')),
                                'payprice' => $v['giving_price'],
                                'car_type_id' => 0,
                                'voucher_cost' => $voucherInfo['card_price'],
                                'card_title' => $voucherInfo['card_title'],
                                'card_time' => $v['expiration'],
                                'server_id' => $voucherInfo['server_id'],
                                'start_time' => $currentTime,
                                'voucher_type' => $voucher_type,
                                'money_off' => 0,
                                'voucher_source' => 2,
                                "voucher_start" => 1,
                                "settlement_price" => $v['settlement_price'],
                            ));
                        }
                    }
                } elseif ($v['giving_type'] == 4) {
                    # 积分
                    $integralService->addMemberIntegral($this->MemberId, 13, $v['giving_price'], 1);
                } elseif ($v['giving_type'] == 7 or $v['giving_type'] == 8 or $v['giving_type'] == 9) {
                    # 查询商家卡券
                    $merchantInfo = Db::table("merchants_voucher")->where(array("id" => $v['giving_id']))->find();
                    if (!empty($merchantInfo)) {
                        for ($i = 0; $i < $v['giving_number']; $i++) {
                            array_push($memberVoucher, array(
                                'voucher_id' => $merchantInfo['id'],
                                'member_id' => $this->MemberId,
                                'create' => date('Y-m-d H:i:s', strtotime('+ ' . $v['expiration'] . 'days')),
                                'payprice' => $v['giving_price'],
                                'car_type_id' => 0,
                                'voucher_cost' => $merchantInfo['price'],
                                'card_title' => $merchantInfo['title'],
                                'card_time' => $merchantInfo['validity_day'],
                                'server_id' => 0,
                                'start_time' => $currentTime,
                                'voucher_type' => $v['giving_type'],
                                'money_off' => $merchantInfo['min_consume'],
                                'voucher_source' => 2,
                                "voucher_start" => $merchantInfo['voucher_start'],
                            ));
                        }
                    }
                } else {
                    # 余额
                    $logConsump = array(
                        'consump_price' => $v['giving_price'],
                        'pay_type' => 5,
                        'log_consump_type' => 2,
                        'consump_type' => 1,
                        'income_type' => 4
                    );
                    $balanceService->operaMemberbalance($this->MemberId, $v['giving_price'], 1, $logConsump);
                    # 增加补贴记录,5参加活动赠送余额
                    SubsidyService::addSubsidy(array("subsidy_type"=>5,"subsidy_number"=>$v['giving_price'],"member_id"=>$this->MemberId,'pid'=>$activeId,'order_number'=>$order_number));
                }
            }
        }
        # 添加卡券信息
        if (!empty($memberVoucher)) {
            Db::table('member_voucher')->insertAll($memberVoucher);
        }
        # 活动记录
        Db::table('log_active')->insert(array(
            'member_id' => $this->MemberId,
            'active_id' => $activeId,
            'active_type' => $activeInfo['state'],
            'create_time' => $currentTime
        ));
        # 活动数量减一
        Db::table('active')->where(array('id' => $activeId))->setDec('active_num', 1);
        # 更新用户信息
        $memberService = new MemberService();
        $memberService->changeMemberInfo($this->MemberId);
        # 完成任务=>'8'=> '参加活动'
        TimeTask::finishTask($this->MemberId, 8);
        return array('status' => true);
    }
}
