<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/23
 * Time: 16:44
 */

namespace app\api\controller;


use app\api\ApiService\AdvanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\MemberService;
use app\api\ApiService\orderService;
use app\api\ApiService\PriceService;
use app\api\ApiService\VoucherService;
use app\service\CommissionService;
use app\service\ResourceService;
use Protocol\Curl;
use Redis\Redis;
use think\App;
use think\Controller;
use think\Db;
use think\facade\Request;

class HomeInfo extends Controller
{
    function __construct(App $app = null)
    {
        parent::__construct($app);
        header('Access-Control-Allow-Origin:*');  //支持全域名访问，不安全，部署后需要固定限制为客户端网址
        header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); //支持的http 动作
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Authorization , Access-Control-Request-Headers,token,applymark');  //响应头 请按照自己需求添加。
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }

    }

    function showInfo()
    {
        $mark=input("post.mark");
        $_redis = new Redis();
        # 首页广告管理
        $advance=AdvanceService::getsAdvance("indexadvert");
        if (empty($advance)) {
            $_advance_return = array("status" => false);
        } else {
            $imgurl=$advance['info'][0]['application_image'];
            $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
            if($applymark==3){
                $imgurl=imgUrl('static/img/imgurl_toutiao.png',true);
            }
            $_advance_return = array("status" => true, "imgurl" => $imgurl,"imgurl_toutiao" => imgUrl('static/img/imgurl_toutiao.png',true));
        }
        # 用户端通知
        $notice = Db::table("notice")->where(array("accepter" => 5, "is_del" => 2))
            ->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")
            ->order("id desc")
            ->find();
        if (empty($notice)) {
            $_notice_return = array("status" => false);
        } else {
            $_notice_return = array("status" => true, "context" => ResourceService::htmlContentFormat($notice['content']));
        }
        # 首页banner
        $banner=AdvanceService::getsAdvance("banner");

        if (empty($banner)) {
            $_banner_return = array("status" => false);
        } else {
            if(empty($mark)){
                $_banner_return = array("status" => true, "imgurl" =>$banner['info']);
            }else{
                $_banner_return = array("status" => true, "imgurl" =>$banner['info']);
            }

        }
        $handle = $this->applyMemberPop();
        if (!empty($handle['imgurl'])) {
            $handle = $handle['imgurl'];
        } else {
            $handle = imgUrl('static/img/Client/handle.gif', true).'?1';
        }
        # 首页
        $client = array(
            'handle' => $handle,
            'home' => imgUrl('static/img/Client/home2.png', true),
            'recommend' => imgUrl('static/img/Client/recommend.gif', true).'?1',
            'recruit' => imgUrl('static/img/Client/recruit.png', true),
            'runner'=> imgUrl('static/img/Client/runner.png', true),
            'homeVideo'=>imgUrl('static/img/Client/home_video.gif', true).'?1',
        );
        # 办理会员弹窗
        $applyMemberUrl = imgUrl('static/img/Client/apply_member.png', true);
        # 签到总开关(1 开启  2关闭)
        $switchSign = Db::table('switch')->field('switch_status')->where(array('switch_type' => 'sign'))->find()['switch_status'];
        # 头条办会员弹窗
        $toutiaoMember = config('share.toutiaoMember');
        # 首页抖音判断是否展示吃喝玩乐
        $toutiaoShow = config('share.toutiaoShow');
        return array("status" => true, "notice" => $_notice_return, "applyMemberUrl"=>$applyMemberUrl,"banner" => $_banner_return, "advance" => $_advance_return, 'client' => $client,'switchSign'=>$switchSign,'toutiaoMember'=>$toutiaoMember,'toutiaoShow'=>$toutiaoShow);
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 首页弹窗
     */
    function indexPop()
    {
        return array("status" => false);
        $_redis = new Redis();
        # 首页弹窗(消息管理弹窗通知)
        $homePop = $_redis->hGetJson("pop", "homePop");
        if (empty($homePop)) {
            $homePop = Db::table("apop")->where(array("is_del" => 2))->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")
                ->order("id desc")->find();
            $_redis->hSetJson("pop", "homePop", $homePop);
        }
        if (!empty($homePop)) {
            $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
            if($applymark==3) {
            $homePop['pop_detail'] =str_replace( '<img', '<img width="100%"',$homePop['pop_detail']);
            }
            if($homePop['is_skip']==1 and $homePop['is_type']==2){
                $jump = AdvanceService::getsJumpUrl($homePop['jump_type'], $homePop['jump_id'],$homePop['pop_url']);
                $homePop['url'] = $jump['url'];
                $homePop['param'] = $jump['param'];
            }
            return array("status" => true, "popInfo" => $homePop);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 获取天气
     */
    function getWeather()
    {
        $lat = input("post.lat");
        $lon = input("post.lon");
        $province = input("post.province");
        $city = input("post.city");
        $_redis = new Redis();
        $getting = $_redis->hGet("weather", "getting");
        if (!empty($getting)) {
            sleep(3);
        }
        $id = null;   // 默认没有信息
        # 查询是否有这个城市的天气
        $info = Db::table("todayweather")->where(array("province" => $province, "city" => $city))->find();

        if (!empty($info)) {
            $id = $info['id'];
            if ($info['time'] > date("Y-m-d H:i:s", strtotime("-2 hours"))) {
                $temperature = $info['temp'];// 当前气温
                $weather = $info['weather'];// 当前天气
                $wash_car = $info['wash_index'];// 洗车指数
                $weather_pic = $info['weather_pic'];// 天气图标
                return array("status" => true, "wash_index" => $wash_car, "temp" => $temperature, "weather" => $weather, "weather_pic" => $weather_pic);
            }
        }
        $_redis->hSet("weather", "getting", 1);
        $_curl = new Curl();
        // 阿里云易源天气数据经纬度查询
        $res = $_curl->get("https://ali-weather.showapi.com/gps-to-weather?from=5&lat={$lat}&lng={$lon}&need3HourForcast=0&needAlarm=0&needHourData=0&needIndex=1&needMoreDay=0",
            null,
            array("appcode" => "7c445e9d2ca14b0c8b551dac102017aa"));
        $res = json_decode($res, true);
        if ($res["showapi_res_code"] == 0) {
            $temperature = $res['showapi_res_body']['now']['temperature'];// 当前气温
            $weather = $res['showapi_res_body']['now']['weather'];// 当前天气
            $wash_car = $res['showapi_res_body']['f1']['index']['wash_car']['title'];// 洗车指数
            $weather_pic = $res['showapi_res_body']['now']['weather_pic'];// 天气图标
            $_array = array("province" => $province, "city" => $city, "time" => date("Y-m-d H:i:s"), "wash_index" => $wash_car, "temp" => $temperature,
                "weather" => $weather, "weather_pic" => $weather_pic);
            if (is_null($id)) {
                Db::table("todayweather")->insert($_array);
            } else {
                Db::table("todayweather")->where(array("id" => $id))->update($_array);
            }
            $_redis->hDel("weather", "getting");
        }
        return array("status" => true, "wash_index" => $wash_car, "temp" => $temperature, "weather" => $weather, "weather_pic" => $weather_pic);
    }

    /**
     * @return array
     * @context 根据经纬度获取地址
     */
    function getDetailAddress()
    {
        $lon = input("post.lon");
        $lat = input("post.lat");
        # 获取详细地址
        $res = $this->analysisLonLat($lon, $lat);
        if ($res['status'] == 1 and $res['info'] == 'OK') {
            $_province = $res['regeocode']['addressComponent']['province'];
            $_city = $res['regeocode']['addressComponent']['city'];
            if (empty($_city)) {
                $_city = '';
            }
            $_district = $res['regeocode']['addressComponent']['district'];
            $_township = $res['regeocode']['addressComponent']['township'];
            return array('status' => true, "address" => $_province . $_city . $_district . $_township, "province" => $_province, "city" => $_city);
        } else {
            return array("status" => false, "msg" => "未获取到经纬度");
        }
    }

    /**
     * @param $lon
     * @param $lat
     * @return mixed
     * @context 经纬度解析地址
     */
    function analysisLonLat($lon, $lat)
    {
        $url = "https://restapi.amap.com/v3/geocode/regeo?key=3f5396b83b7b09cfb811027943f8d7c7&location=$lon,$lat&output=json";
        $_curl = new Curl();
        $res = $_curl->get($url);
        return json_decode($res, true);
    }

    function aaa()
    {
        $str = Aes::encrypt("kdjfdjkdjkf1455");

        $str2 = Aes::decrypt($str);

        var_dump($str);
        var_dump("========================");
        var_dump($str2);
    }

    /**
     * @return array
     * @context 获取卡券规则
     */
    function getsVoucherRules()
    {
        $VoucherService = new VoucherService();
        return array("status" => true, "rules" => $VoucherService->voucherRule());
    }

    function demo()
    {
        var_dump(date("Y-m-d", strtotime("-30 days", strtotime("2021-03-04"))));
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取积分设置
     */
    function getsIntegralSetItem()
    {
        $mark = input("post.mark");
        return array("status" => true, "integral" => IntegralService::integralSetItem($mark));
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 未注册用户弹窗
     */
    function noRegisterPop()
    {
        $_redis = new Redis();
        $pop = $_redis->hGetJson("pop", "noregister");
        if (empty($pop)) {
            $pop = Db::table("home_imgurl")->where(array("type" => 6))
                ->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")
                ->find();
            if (!empty($pop)) {
                $time = strtotime($pop['end_time']) - time();
                $_redis->hSetJson("pop", "noregister", $pop, $time);
            }
        }
        if (!empty($pop)) {
            return array("status" => true, "popInfo" => $pop);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @context 获取客服电话
     */
    function getsCustomerTel()
    {
        return array("status" => true, "tel" => AdvanceService::customerServerTel());
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户协议
     */
    function memberAgent()
    {
        $type = input("post.type");
        if (empty($type)) {
            $type = 1;
        }
        if ($type == 1) {
            $protocol = AdvanceService::getsAdvance("protocol");
        } else {
            # 隐私协议
            $protocol = AdvanceService::getsAdvance("privacy");
        }

        return array("status" => true, "protocol" => $protocol['info'][0]['application_context']);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 检查更新
     */
    function CheckUpdates()
    {
        # 检查更新
        $appid = input("post.appid") ?? '__UNI__7BB36DB';
        $version = input("post.version") ?? '1.0.0'; //客户端版本号
        $type = input('post.system') ?? 'android';
        $rsp = array("status" => true, "up_status" => false); //默认返回值，不需要升级
        if (isset($appid) && isset($version)) {
            # 查询最新的版本信息
            $info = Db::table('version_control')
                ->where(array('appid' => $appid))
                ->order(array('version' => 'desc'))
                ->find();
            if ($type == 'ios') {
                $info['version'] = $info['ios_version'];
            }
            if (!empty($info) and intval(str_replace('.', '', $version)) < intval(str_replace('.', '', $info['version']))) { //校验appid
                $rsp["up_status"] = true;
            }
            $rsp['version'] = $info['version'];
            $rsp["note"] = $info['note']; //release notes
            $rsp["url"] = $info['url']; //应用升级包下载地址
            $rsp["ios_url"] = $info['ios_url']; //应用升级包下载地址
            $rsp['force_update'] = $info['force_update'];
        }
        return $rsp;
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 首页推荐服务
     */
    function recommendService()
    {
        $member_id = input("post.member_id");
        # 查询所有推荐服务并查询线上最低价格
        $list = Db::table("service s")
            ->field("s.id,s.service_title,s.service_image,coalesce(scm.discount,0) discount")
            ->leftJoin("service_car_mediscount scm", "scm.service_id=s.id and scm.car_level_id=1 and service_type=1 and biz_id=0")
            ->where(array("s.service_top" => 1, "s.service_status" => 1))
            ->where('s.service_title', 'NOT REGEXP', '^\*\*|\#\#')
            ->order("s.id asc,s.service_click desc,s.service_evaluation desc")
            ->select();
        if (!empty($list)) {
            if (!empty($member_id)) {
                $MemberService = new MemberService();
                $memberInfo = $MemberService->MemberInfoCache($member_id);
            }
            $min_price = 0;
            $max_price = 0;
            $PriceService = new PriceService();
            foreach ($list as $k => $v) {
                $price = $PriceService->servicePrice($v['id']);
                if (!empty($member_id)) {
                    if (!empty($price['online'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']);
                    }
                    if ($memberInfo['member_level_id'] > 0) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']) * max($price['member_discount']);
                    }
                } else {
                    if (!empty($price['offline'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['offline']);
                    }
                }
                if ($min_price == $max_price) {
                    $list[$k]['discount'] = getsPriceFormat($min_price);
                } else {
                    $list[$k]['discount'] = getsPriceFormat($min_price) . "~" . getsPriceFormat($max_price);
                }
            }
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 首页办理会员图
     */
    function applyMemberPop()
    {
        $_redis = new Redis();
        $pop = $_redis->hGetJson("pop", "applyMember");
        if (empty($pop)) {
            $pop = Db::table("home_imgurl")->where(array("type" => 1))
                ->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")
                ->find();
            if (!empty($pop)) {
                $time = strtotime($pop['end_time']) - time();
                $_redis->hSetJson("pop", "applyMember", $pop, $time);
            }
        }
        if (!empty($pop)) {
            return array("status" => true, "popInfo" => $pop);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 我的页广告
     */
    function MineAdvance()
    {
        $top = AdvanceService::getsAdvance("mineadvert_center");
        $under = AdvanceService::getsAdvance("mineadvert_bottom ");
        $recommend =  imgUrl('static/img/Client/gg2.png', true);
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        if($applymark==3){
            $recommend=imgUrl('static/img/Client/gg_douyin.png',true);
        }
        return array("status" => true, "top" => $top['info'][0], "under" => $under['info'][0],"recommend_img"=>$recommend);
    }
    
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 加载页图片
     */
    function loadingAdvance()
    {
        $img = AdvanceService::getsAdvance("loading");
        return array("status" => true, "img" => $img['info'][0]['application_image']);
    }

    /**
     * @return array
     * @content 关于我们
     */
    function AboutUs()
    {
        # 关于我们
        return array('status' => true, 'info' => array('url' => 'http://www.e联车服.中国'));
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取服务对应的 会员价格
     */
    function getsServiceLevelPrice()
    {
        $service_id = input("post.service_id");
        $carLevel = input("post.car_level");
        $price = new PriceService();
        $res = $price->serviceLevelPrice($service_id, $carLevel);
        return array("status" => true, "list" => $res);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员列表
     */
    function memberLevelList()
    {
        $member_id = input("post.member_id");
        $nowLevel = 0;
        if (!empty($member_id)) {
            $MemberService = new MemberService();
            $memberInfo = $MemberService->MemberInfoCache($member_id);
            if($memberInfo['expiration_status']==2){
                $nowLevel = $memberInfo['expiration_member_level'];
            }else{
                $nowLevel = $memberInfo['member_level_id'];
            }

        }
        return array("status" => true, "list" => LevelService::memberLevelInfo('', $nowLevel, "id asc"), "nowLevel" => $nowLevel);
    }


    function changeCommission()
    {
        $order_number = input("post.order_number");
        $score = input("post.score");
        # 查询订单信息
        $orderInfo = Db::table("orders")->where(array("order_number" => $order_number))->find();
        if (!empty($orderInfo)) {
            $commissionService = new CommissionService();
            # 查询服务订单
            $order_server = Db::table("order_server")->where(array("order_number" => $order_number))->select();
            if (!empty($order_server)) {
                foreach ($order_server as $k => $v) {
                    if (!empty($v['employee_id'])) {
                        $employee_con = explode(',', $v['employee_id']);
                        foreach ($employee_con as $m => $n) {
                            $commissionService->commission($orderInfo['biz_id'], $v['id'], 1, $orderInfo['car_level'], 'comm', $n, $score);
                        }
                    }
                }
            }
            # 查询商品
            $order_pro = Db::table("order_biz")->where(array("order_number" => $order_number))->select();
            if (!empty($order_pro)) {
                foreach ($order_pro as $k => $v) {
                    if (!empty($v['employee_id'])) {
                        $employee_con = explode(',', $v['employee_id']);
                        foreach ($employee_con as $m => $n) {
                            $commissionService->commission($orderInfo['biz_id'], $v['id'], 2, $orderInfo['car_level'], 'comm', $n, $score);
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 未注册红包弹窗
     */
    function noRegisterRedPacketPop()
    {
        $_redis = new Redis();
        $redpacket = $_redis->hGetJson("redpacket", "bigRedpacket");
        if (empty($redpacket)) {
            // 查询当前红包是哪个
            $redpacket = Db::table("redpacket")->where("type = 1 and status = 1 and surplus_num > 0 and create_time<='" . date("Y-m-d H:i:s") . "' and end_time >='" . date("Y-m-d H:i:s") . "'")
                ->order("id desc")->find();
            if (!empty($redpacket)) {
                $time = strtotime($redpacket['end_time']) - time();
                $_redis->hSetJson("redpacket", "bigRedpacket", $redpacket, $time);
            }
        }

        if (!empty($redpacket)) {
            $info = Db::table("redpacket_title")->where(array("redpacket_id" => $redpacket['id'], "person" => -2, "status" => 1))->find();
            if (!empty($info)) {
                $redpacket['info'] = $info;
                return array("status" => true, "redpacket" => $redpacket);
            } else {
                return array("status" => false, "msg" => "无对应的红包详情");
            }
        }
    }

    /**
     * @return array
     * @context 本地生活背景图
     */
    function localLifeBg(){
        $img=  imgUrl('static/img/Client/local_bg.gif', true);
        return array("status"=>true,"img"=>$img);
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 扫码领取的卡券
     */
    function scanReceiveVoucher(){
        $member_id = input("post.member_id");
        # 查询该用户是否有扫码领取的券
        if(!empty($member_id)){
            $is_repeat = Db::table("member_voucher")->where(array("voucher_id" => 504,"member_id"=>$member_id,"voucher_source"=>20))->find();
            if(empty($is_repeat)){
                $_voucher = array(array("voucher_id" => 504, 'merchants_id' => 0, "member_id" => $member_id, "card_time" => 10,
                    "create" => date("Y-m-d H:i:s", strtotime("+10 days")),
                    "voucher_source" => 20, "paytype" => 0, "voucher_cost" =>15.3, "card_title" => '洗车优惠券',
                    "voucher_type" => 14, "money_off" => 18,
                ));
                $voucherService = new VoucherService();
                $voucherService->addMemberVoucher($member_id,$_voucher);
                return array("status"=>true);
            }else{
                return array("status"=>false,"msg"=>"您已领取过优惠券，不可重复参与");
            }
        }else{
            return array("status"=>false,"msg"=>"请注册后领取");
        }
    }

}
