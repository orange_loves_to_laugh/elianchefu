<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/11
 * Time: 18:19
 * 用户各种记录
 */

namespace app\api\controller;


use app\api\ApiService\CarInfoService;
use app\api\ApiService\PropertyService;
use think\Db;

class MemberLogInfo extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户车辆保养记录
     */
    function MaintenanceRecords()
    {
        if ($this->MemberId == 1733 or $this->MemberId == 3630) {
//            $this->MemberId = 25;
        }
        $limitRows = 10;
        $pages = input('post.page') ?? 0;
        $limit = $limitRows * $pages;
        # 查询保养记录
        $logMaintain = Db::table('log_maintain lm')->field('maintain_type,now_mileage,maintain_title,
                date_format(maintain_time,"%Y-%m-%d %H:%i") maintain_time,mc.car_licens car_num,(CONCAT_WS("·",cl.title,cs.sort_title)) car_info,
            cle.level_title,cl.title logo_title,cs.sort_title,next_mileage,suggest_time,biz_title,biz_phone')
            ->join('biz b', 'b.id=lm.biz_id', 'left')
            ->join('member_car mc', 'mc.car_licens=lm.car_licens', 'left')
            ->join('car_sort cs', 'cs.id=mc.car_type_id', 'left')
            ->join('car_logo cl', 'cl.id=cs.logo_id', 'left')
            ->join('car_level cle', 'cle.id=cs.level', 'left')
            ->where(array('mc.member_id' => $this->MemberId))
            ->group('maintain_time,now_mileage')
            ->order(array('maintain_time' => 'desc'))
            ->limit($limit, $limitRows)
            ->select();
        if (!empty($logMaintain)) {
            foreach ($logMaintain as $k => $v) {
                if($v['maintain_type']> 0){
                    $logMaintain[$k]['maintain_title'] = getMaintainType($v['maintain_type'])['title'];
                }
                $logMaintain[$k]['car_licens'] = $logMaintain[$k]['car_num'];
                $logMaintain[$k]['car_num'] = addAStr($v['car_num']);
            }
        }
        return array('status' => true, 'data' => $logMaintain);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 保养记录详情
     */
    function logMaintainDetail()
    {
        # 接收车牌号
        $carNum = input('post.car_num');
        $maintain_time = input('post.maintain_time');
        $now_mileage = input('post.now_mileage');
        if (empty($carNum)) {
            return array('status' => false, 'msg' => '车牌号不能为空');
        }
        $info = Db::table('log_maintain lm')
            ->field('maintain_type,maintain_price,now_mileage,next_mileage,remark,maintain_price,maintain_title,
                date(maintain_time) maintain_time,suggest_time')
            ->where(array('car_licens' => $carNum, 'now_mileage' => $now_mileage))
            ->where("date_format(maintain_time,\"%Y-%m-%d %H:%i\") = '" . $maintain_time . "'")
            ->order('lm.id desc')
            ->select();
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                if($v['maintain_type'] > 0) {
                    $info[$k]['maintain_title'] = getMaintainType($v['maintain_type'])['title'];
                }
            }
        }
        return array('status' => true, 'info' => $info);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 质保记录
     */
    function logWarranty()
    {
//        $this->MemberId=1733;
        $logWarranty = Db::table('log_warranty lw')->field('DISTINCT(lw.id),lw.car_num,lw.now_mileage,
                date_format(lw.now_time,"%Y-%m-%d %H:%i") now_time,(CONCAT_WS("·",cl.title,cs.sort_title)) car_info,
            cle.level_title,cl.title logo_title,cs.sort_title,lw.next_mileage,lw.next_time,biz_title,biz_phone,sl.log_validity,sl.log_scope,s.service_title')
            ->join('biz b', 'b.id=lw.biz_id', 'left')
            ->join('service_log sl', 'sl.id=lw.warranty_id', 'left')
            ->join('service s', 'sl.service_id=s.id', 'left')
            ->join('member_car mc', 'mc.car_licens=lw.car_num', 'left')
            ->join('car_sort cs', 'cs.id=mc.car_type_id', 'left')
            ->join('car_logo cl', 'cl.id=cs.logo_id', 'left')
            ->join('car_level cle', 'cle.id=cs.level', 'left')
            ->where(array('lw.member_id' => $this->MemberId))
            ->order(array('lw.id' => 'desc'))
            ->select();
        return array('status' => true, 'info' => $logWarranty);
    }

    function logRepair(){
        $logRepair = Db::table('log_repair lr')->field('lr.id,lr.car_num,lr.now_mil,lr.repair_title,
                date_format(lr.add_time,"%Y-%m-%d %H:%i") add_time,(CONCAT_WS("·",cl.title,cs.sort_title)) car_info,
            cle.level_title,cl.title logo_title,cs.sort_title,biz_title,biz_phone')
            ->join('biz b', 'b.id=lr.b_id', 'left')
            ->join('member_car mc', 'mc.car_licens=lr.car_num', 'left')
            ->join('car_sort cs', 'cs.id=mc.car_type_id', 'left')
            ->join('car_logo cl', 'cl.id=cs.logo_id', 'left')
            ->join('car_level cle', 'cle.id=cs.level', 'left')
            ->where(array('mc.member_id' => $this->MemberId))
            ->group('add_time,now_mil')
            ->order(array('add_time' => 'desc'))
            ->select();
        return array('status' => true, 'info' => $logRepair);
    }
    function logRepair_detail(){
        # 接收车牌号
        $carNum = input('post.car_num');
        $add_time = input('post.add_time');
        $now_mileage = input('post.now_mil');
        $logRepair = Db::table('log_repair lr')->field('lr.id,lr.car_num,lr.now_mil,lr.repair_title,
                date_format(lr.add_time,"%Y-%m-%d %H:%i") add_time')
            ->where(array('car_num' => $carNum, 'now_mil' => $now_mileage))
            ->where("date_format(add_time,\"%Y-%m-%d %H:%i\") = '" . $add_time . "'")
            ->order(array('lr.id' => 'desc'))
            ->select();
        return array('status' => true, 'info' => $logRepair);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 消费充值记录
     */
    function logConsump()
    {
        if ($this->MemberId == 13283 or $this->MemberId == 3630) {
            $this->MemberId = 12831;
        }
        # 查询消费记录/充值记录  (consump_type=> 1 消费  2充值)
        $mark = input('post.log_consump_type') ?? 1;
        $_where = 'log_consump_type =' . $mark;
        /*if ($mark == 1) {
            # 消费
            $_where = 'order_status = 5 ';
        } else {
            # 充值
            $_where = 'log_consump_type =' . $mark;
        }*/
        # 分页
        $limitRows = 10;
        $pages = input('post.page') ?? 0;
        $limit = $limitRows * $pages;
        # 用户绑定车辆车牌号数组
        $carNumList = CarInfoService::memberCarInfo($this->MemberId)['carNum'];
        array_unshift($carNumList, '全部');
        # 车牌号筛选
        $carNum = input('post.car_num');
        if ($mark == 1 and !empty($carNum) and $carNum != '全部') {
            $_where .= ' and car_liences = "' . $carNum . '"';
        }
        # 时间筛选
        $time = input('post.time');
        if (!empty($time)) {
            $_where .= " and date(consump_time) ='" . $time . "'";
            /*if ($mark == 1) {
                # 消费
                $_where .= " and date(order_over) = '" . $time . "'";
            } else {
                # 充值
                $_where .= " and date(consump_time) ='" . $time . "'";
            }*/
        }
        # 支付方式筛选(1 微信 2支付宝 3现金  4银联  5余额 6卡券  7积分)
        $payType = input('post.pay_type');
        if (!empty($payType)) {
            if ($mark == 1) {
                # 消费
                $_where .= " and pay_type = " . $payType;
            } else {
                # 充值
                $_where .= " and consump_pay_type = " . $payType;
            }
        }
        if ($mark == 1) {
            # 消费,查订单
//            $logInfo = Db::table('orders o')
//                ->field("car_liences car_num,date_format(order_over,'%Y-%m-%d %H:%i') time,car_mileages mileages,
//                voucher_price,order_price,pay_price,pay_type,order_number,biz_id,biz_title,biz_phone")
//                ->join('biz b', 'b.id=o.biz_id', 'left')
//                ->where(array('member_id' => $this->MemberId))
//                ->where($_where)
//                ->limit($limit, $limitRows)
//                ->order(array('order_over' => 'desc'))
//                ->select();
            $logInfo = Db::table('log_consump lc')
                ->field('lc.id,consump_price pay_price,log_consump_type,consump_pay_type pay_type,
            date_format(consump_time,"%Y-%m-%d %H:%i") time,
        consump_type,giving,income_type,lc.biz_id,biz_title,biz_phone,
        car_liences car_num,car_mileages mileages,voucher_price,order_price,lc.order_number,
        m.title merchants_title,m.tel,concat(m.province,m.city,m.area,m.address) address,mo.total_price,mo.actual_price,(mo.total_price-mo.actual_price) mo_voucher')
                ->join('orders o', 'lc.order_number=o.order_number', 'left')
                ->join('biz b', 'b.id=lc.biz_id and lc.income_type!=20 and lc.income_type!=21', 'left')
                ->join("merchants m", "m.id = lc.biz_id and (lc.income_type=20 or lc.income_type=21)", "left")
                ->join("merchants_order mo", "(lc.income_type=20 or lc.income_type=21) and lc.order_number = mo.order_number", "left")
                ->where(array('lc.member_id' => $this->MemberId))
                ->where($_where)
                ->limit($limit, $limitRows)
                ->order(array('consump_time' => 'desc'))
                ->select();
        } else {
            $logInfo = Db::table('log_consump lc')
                ->field('lc.id,consump_price,log_consump_type,consump_pay_type pay_type,
            date_format(consump_time,"%Y-%m-%d %H:%i") time,
        consump_type,giving,order_number,income_type,biz_id,biz_title,biz_phone')
                ->join('biz b', 'b.id=lc.biz_id', 'left')
                ->where(array('log_consump_type' => 2))
                ->where(array('member_id' => $this->MemberId))
                ->where($_where)
                ->where('income_type not in (8,9,10)')
                ->limit($limit, $limitRows)
                ->order(array('consump_time' => 'desc'))
                ->select();
        }
        if (!empty($logInfo)) {
            foreach ($logInfo as $k => $v) {
                $logInfo[$k]['voucher_price'] = floatval($v['voucher_price']);
                $logInfo[$k]['order_price'] = floatval($v['order_price']);
                # 支付方式
                $logInfo[$k]['payInfo'] = getpay_type($v['pay_type']);
                # 消费类型
                if (array_key_exists('income_type', $v)) {
                    $logInfo[$k]['consumpInfo'] = lang('member_fee_aim')[$v['income_type']] ?? '服务收入';
                    if ($logInfo[$k]['consumpInfo'] == '活动收入' and $mark == 1) {
                        $logInfo[$k]['consumpInfo'] = '购买活动';
                    }
                }
                if ($mark == 1 and !empty($v['order_number'])) {
                    if ($v['income_type'] == 20) {
                        $logInfo[$k]['biz_title'] = $v['merchants_title'];
                        $logInfo[$k]['biz_phone'] = $v['tel'];
                        $logInfo[$k]['car_num'] = $logInfo[$k]['consumpInfo'];
                        # 查询商户订单详情
                        $logInfo[$k]['detailList'] = Db::table('merchants_orderdetail')
                            ->field('product_type orderType,if(product_type=1,"服务","商品") type,1 num,1 coupon_mark,
                            if(product_id=0,"商户消费",if(product_type=1,(select title from merchants_service where id = product_id),(select title from merchants_pro where id = product_id))) title,
                            if(product_id=0,' . $v['total_price'] . ',if(product_type=1,(select price from merchants_service where id = product_id),(select price from merchants_pro where id = product_id))) price
                            ')
                            ->where(array('order_number' => $v['order_number']))
                            ->select();

                    }elseif($v['income_type'] == 21){
                        # 查询物业订单
                        $info=PropertyService::getsPayFeeLogOrder($v['order_number']);
                        $logInfo[$k]['house_info']=$info['build_title'].$info['unit_title'].$info['floor_title'].$info['house_title']."号";
                        $logInfo[$k]['area'] = $info['area'];
                        $logInfo[$k]['duration_title']=lang("duration")[$info['duration']];
                        $logInfo[$k]['duration']=$info['duration'];
                    } else {
                        # 查询商品工单
                        $orderBiz = Db::table('order_biz ob')
                            ->field("2 orderType,if(" . $v['pay_type'] . "=5,ob.pro_price,ob.original_pro_price) price,
                        ob.bizpro_number num,coupon_mark,
                if(ob.custommark=1,'系统商品','自定义商品') type,
                if(ob.custommark = 1,(select biz_pro_title from biz_pro bp where bp.id = ob.biz_pro_id limit 1),
                (select custom_biz_title from custom_biz cb where cb.id = ob.biz_pro_id limit 1)) title ")
                            ->where(array('order_number' => $v['order_number']))
                            ->buildSql();
                        # 查询服务工单
                        $orderServer = Db::table('order_server os')
                            ->field("1 orderType,if(" . $v['pay_type'] . "=5,os.price,os.original_price) price,1 num,coupon_mark,
                if(os.custommark=1,'系统服务','自定义服务') type,
                if(os.custommark=1,(select service_title from service s where s.id=os.server_id limit 1),
                   (select custom_server_title from customserver cs where cs.id=os.server_id limit 1)) title")
                            ->where(array('order_number' => $v['order_number']))
                            ->union([$orderBiz])
                            ->buildSql();
                        $logInfo[$k]['detailList'] = Db::table($orderServer . 'tem')->select();
                        $logInfo[$k]['order_price'] = array_sum(array_column($logInfo[$k]['detailList'], 'price'));
                    }
                } else {
                    $logInfo[$k]['car_num'] = '购买活动';
                    if (array_key_exists('income_type', $v)) {
                        $logInfo[$k]['car_num'] = lang('member_fee_aim')[$v['income_type']] ?? '服务收入';
                        if ($logInfo[$k]['car_num'] == '活动收入' and $mark == 1) {
                            $logInfo[$k]['car_num'] = '购买活动';
                        }
                    }
                }
            }
        }
        $payTypeInfo = lang('cash_type_total_info');
        array_unshift($payTypeInfo, array('name' => '支付方式', 'value' => 0));
        return array('status' => true, 'info' => $logInfo, 'carNumList' => $carNumList, 'payTypeInfo' => $payTypeInfo);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 用户积分记录
     */
    function logIntegral()
    {
        # 查询积分记录
        # 分页
        $limitRows = 10;
        $pages = input('post.page') ?? 0;
        $limit = $limitRows * $pages;
        # 类型筛选  0->兑换  1->获得
        $type = input('post.integral_type') ?? 0;
        $logInfo = Db::table('log_integral')
            ->field('date_format(integral_time,"%Y-%m-%d %H:%i") time,integral_number num,integral_source,biz_id,biz_pro_id,
            if(' . $type . '=0,(select exchange_title from integral_exchange where id = log_integral.biz_pro_id),"") title')
            ->where(array('member_id' => $this->MemberId, 'integral_type' => $type))
            ->order(array('integral_time' => 'desc'))
            ->limit($limit, $limitRows)
            ->select();
        if (!empty($logInfo)) {
            foreach ($logInfo as $k => $v) {
                $logInfo[$k]['source'] = getIntegralType($v['integral_source']);
            }
        }
        return array('status' => true, 'info' => $logInfo);
    }
}
