<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/15
 * Time: 11:11
 */

namespace app\api\controller;


use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\SubsidyService;
use app\api\ApiService\VoucherService;
use think\App;
use think\Db;
use think\facade\Request;

class MemberTask extends Common
{
    function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->addTask();
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 任务列表
     */
    function taskList()
    {
        if($this->MemberId==1733){
            $this->MemberId=25;
        }
        $list = Db::table("member_task mt")
            ->field("mt.id,mt.task_id,mt.task_plan,mt.finish_status,t.task_title,t.task_type,mt.task_total,t.task_condition,t.task_detail,t.main_task")
            ->join("task t", "t.id = mt.task_id")
            ->where("mt.finish_status<3 and mt.member_id={$this->MemberId}")
            ->order("t.main_task asc,mt.finish_status desc")
            ->select();
        # 主任务
        $mainTask = array();
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                # 查询任务奖励
                $list[$k]['rewardInfo'] = Db::table('task_prize')
                    ->field('')
                    ->where(array('task_id' => $v['task_id']))
                    ->select();
                if ($v['main_task'] == 1) {
                    # 主任务
                    $mainTask = $list[$k];
                    $mainTask['length']=1;
                    unset($list[$k]);
                }
            }
        }
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        if(!config('share.toutiaoShow') and $applymark==3){
            $mainTask=array();
        }
        return array("status" => true, "listInfo" => array_values($list), 'mainTask' => $mainTask);
    }

    function addTask()
    {
        Db::table('member_task')->where('finish_status = 1 and over_time is not null and over_time <= "'.date('Y-m-d H:i:s').'" ')->update(array('finish_status'=>4));
        $list = Db::table("member_task mt")
            ->field("mt.id,mt.task_id,mt.task_plan,mt.finish_status,t.task_title,t.task_type,mt.task_total,t.task_condition,t.task_detail,t.main_task")
            ->join("task t", "t.id = mt.task_id")
            ->where("mt.finish_status<3 and mt.member_id={$this->MemberId}")
            ->order("t.main_task asc,mt.finish_status desc")
            ->select();
        $listCount = count($list);
        if ($listCount < 1000) {
            $listIdInfo = implode(',', array_column($list, 'task_id'));
            # 添加新任务
            $currentTime = date('Y-m-d H:i:s');
            $canAdd_where = 'task_status = 1 and main_task = 2 and start_time<= "' . $currentTime . '" and end_time >= "' . $currentTime . '"';
            if (!empty($listIdInfo)) {
                $canAdd_where .= ' and id not in (' . $listIdInfo . ')';
            }
            # 查询可添加的任务(不包括主任务)
            $canAdd = Db::table('task')
                ->field('id,task_num task_number,loop_cycle,is_loop')
                ->where($canAdd_where)
                ->select();
            # 判断主任务
            if (!in_array(1, array_column($list, 'main_task'))) {
                $mainTask = Db::table('task')
                    ->field('id,task_num task_number,loop_cycle')
                    ->where(array('task_status' => 1, 'main_task' => 1))
                    ->where('start_time<= "' . $currentTime . '" and end_time >= "' . $currentTime . '"')
                    ->find();
                if (!empty($mainTask)) {
                    array_push($canAdd, $mainTask);
                }
            }
            # 添加任务列表
            if (!empty($canAdd)) {
                # 没有任务周期
                $memberTask = array();
                # 存在任务周期
                $memberTask_cycle = array();
                # 查询已完成的任务
                $logTaskId = array();
                $logTask = Db::table('member_task')->field('task_id')->where(array('member_id'=>$this->MemberId))->select();
                if(!empty($logTask)){
                    $logTaskId = array_column($logTask,'task_id');
                }
                foreach ($canAdd as $k => $v) {
                    if($v['is_loop']==2){
                        # 不是循环任务,剔除
                        if(in_array($v['id'],$logTaskId)){
                            continue;
                        }
                    }
                    if ($v['loop_cycle'] == 0) {
                        array_push($memberTask, array(
                            'member_id' => $this->MemberId,
                            'task_id' => $v['id'],
                            'task_plan' => 0,
                            'task_total' => $v['task_number'],
                            'finish_status' => 1,
                            'create_time' => date('Y-m-d H:i:s'),
                        ));
                    } else {
                        array_push($memberTask_cycle, array(
                            'member_id' => $this->MemberId,
                            'task_id' => $v['id'],
                            'task_plan' => 0,
                            'task_total' => $v['task_number'],
                            'finish_status' => 1,
                            'create_time' => date('Y-m-d H:i:s'),
                            'over_time' => date('Y-m-d H:i:s', strtotime('+ ' . $v['loop_cycle'] . ' days')),
                        ));
                    }
                }
                # 执行添加
//                $_add = false;
                if (!empty($memberTask)) {
                    Db::table('member_task')->insertAll($memberTask);
//                    $_add = true;
                }
                if (!empty($memberTask_cycle)) {
                    Db::table('member_task')->insertAll($memberTask_cycle);
//                    $_add = true;
                }

            }
        }
        return array("status" => true, "listInfo" => $list);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 领取任务奖励
     */
    function receiveReward()
    {
        # 用户任务id
        $memberTaskId = input('post.id');
        # 任务id
        $taskId = input('post.task_id');
        if (empty($memberTaskId) or empty($taskId)) {
            return array('status' => false, 'msg' => '系统错误,参数为空');
        }
        # 判断状态
        $_status = Db::table('member_task')->field('finish_status')
            ->where(array('id' => $memberTaskId))->find()['finish_status'];
        if ($_status != 2) {
            return array('status' => false, 'msg' => '系统错误,无法领取奖励');
        }
        # 查询任务对应的奖励
        $_rewardInfo = Db::table('task_prize')
            ->where(array('task_id' => $taskId))
            ->select();
        if (empty($_rewardInfo)) {
            return array('status' => false, 'msg' => '来晚啦,奖品已经被领没了');
        }
        $voucherService = new VoucherService();
        $balanceService = new BalanceService();
        $integralService = new IntegralService();
        $_voucher = array();
        foreach ($_rewardInfo as $k => $v) {
            if ($v['prize_type'] == 1) {
                # 加积分
                $integralService->addMemberIntegral($this->MemberId, 28, $v['intbal'], 1);
            } elseif ($v['prize_type'] == 2) {
                # 加余额
                $balanceService->operaMemberbalance($this->MemberId, $v['intbal'], 1, array("consump_price" => $v['intbal'], "pay_type" => 0,
                    "log_consump_type" => 2,    // 1消费  2充值
                    "consump_type" => 1,// 1线上 或 2 门店支付
                    "income_type" => 16,
                ));
                # 增加补贴记录,7,任务赠送余额
                SubsidyService::addSubsidy(array("subsidy_type"=>7,"subsidy_number"=>$v['intbal'],"member_id"=>$this->MemberId,'pid'=>$memberTaskId));
            } elseif ($v['prize_type'] == 3 or $v['prize_type'] == 5) {
                # 3 商品卡券 5 服务卡券
                $card_type_id = $v['prize_type'] == 3 ? 3 : 1;
                $voucher_type = $v['prize_type'] == 3 ? 3 : 4;
                # 查询卡券id
                $voucher = Db::table("card_voucher")->field("id,card_title,server_id")->where(array("card_type_id" => $card_type_id, "id" => $v['spoil_id']))->find();
                for ($i = 0; $i < $v['prize_num']; $i++) {
                    array_push($_voucher, array("voucher_id" => $voucher['id'], "member_id" => $this->MemberId, "card_time" => $v['prize_indate'], "voucher_source" => 19, "paytype" => 0,
                        "voucher_cost" => $v['intbal'], "card_title" => $voucher['card_title'], "voucher_type" => $voucher_type, "server_id" => $voucher['server_id']));
                }
            } elseif ($v['prize_type'] == 4) {
                # 红包
                $voucherService->addRedpacket($this->MemberId, $v['spoil_id'], 2,$v['prize_num'], array('voucher_source' => 19, 'paytype' => 0));
            } elseif ($v['prize_type'] == 7 or $v['prize_type'] == 8) {
                # 7 联盟商家   8本地生活
                $voucherService->addMerchantVoucher($this->MemberId, $v['prize_num'], $v['spoil_id'], 19);
            }
            # 数量减一
            Db::table('task_prize')->where(array('id' => $v['id']))->setDec('surplus_num');
        }
        if (!empty($_voucher)) {
            $voucherService->addMemberVoucher($this->MemberId, $_voucher);
        }
        # 更改状态
        Db::table('member_task')->where(array('id' => $memberTaskId))->update(array('finish_status' => 3, 'reward_time' => date('Y-m-d H:i:s')));
        return array('status' => true, 'msg' => '奖励领取成功');
    }
}
