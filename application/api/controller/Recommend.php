<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/16
 * Time: 13:38
 */

namespace app\api\controller;


use Redis\Redis;
use think\Controller;

class Recommend extends Controller
{
    /**
     * @context 推荐文案
     * @return array
     */
    function recommendMethod()
    {
        return array("status" => true,
            "income" => [
                'head' => [],
                'body' => [
                    "成为“E联车服”平台省钱卡以上用户",
                    "对方通过分享码或链接办理“E联车服”省钱卡成功；",
                    "您将获得对方购买省钱卡金额的30%积分奖励；",
                    "每推荐一位用户可获得一次性积分奖励；",
                    "成为“E联车服推广员”积分奖励外还可获得额外奖励。",
                ],
                'foot' => [],
            ],
            "recommend" => [
                "点击下方“立即推荐好友”按钮；",
                "选择您喜欢的海报图片，将图片保存至手机相册；",
                "对方通过微信识别海报推荐码注册后购买省钱卡，推荐有效；",
                "直接分享链接至微信好友或微信朋友圈；",
                "对方微信链接注册后购买省钱卡成功，推荐有效。",
            ],
            "hhr_incomes" => [
                'head' => [
                    "申请成为“E联车服”平台推广员后，即可推荐用户注册并使用E联车服平台，与更多好友分享平台优惠及平台补贴，为好友提供日常生活消费保障。",
                    "推荐好友享受优惠时，好友成为平台储值用户可获得更多平台特权及政策，同时推广员将获得如下收益：",
                ],
                'body' => [
                    "对方首次办理会员选择套餐金额对应的4%提成收入；",
                    "享受对方所有应用内生活服务消费的2%提成收入；",
                    "享受对方所有汽车养护类服务的2%提成收入；",
                    "对方由储值用户升级为推广员时，您将获得对应奖励；",
                    "获得对方办理会员金额的30%平台可兑换积分奖励。",
                ],
                'foot' => [
                    "以上奖励收入实时到账，可实时进行提现，秒到微信零钱包。"
                ],
            ],
            "hhr_recommend" => [
                "点击下方“立即推荐好友”按钮；",
                "选择您喜欢的海报图片，将图片保存至手机相册；",
                "对方通过微信识别海报推荐码注册后完成平台储值，推荐有效；",
                "直接分享链接至微信好友或微信朋友圈；",
                "对方微信链接注册后完成会员办理，推荐有效。",
            ],
        );

    }

    /**
     * @return bool[]
     * @context 绑定办理会员关系
     */
    function bindRelationMember()
    {
        $data = input("post.");
        $_redis = new Redis();
        $_time = strtotime(date("Y-m-d 23:59:59")) - time();
        if(empty($data['share_member_id']) and empty($data['assignId']) and empty($data['employeeId'])){
            return array("status" => true);
        }
        $_redis->hSetJson("relation_member", $data['member_id'], $data);
        return array("status" => true);
    }
}
