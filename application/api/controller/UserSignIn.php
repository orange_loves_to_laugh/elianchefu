<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/26
 * Time: 16:26
 */

namespace app\api\controller;


use app\api\ApiService\ActiveService;
use app\service\EalspellCommissionService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class UserSignIn extends Common
{
    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 发起签到
     */
    function signIn()
    {
        $redis = new Redis();
        # 连续签到天数
        $signNum = $redis->hGet('memberSignNum', $this->MemberId);
        if ($signNum == false) {
            $signNum = 0;
        }
        # 今天是否可以签到
        $isSign = $redis->hGet('memberIsSign', $this->MemberId);
        if ($isSign) {
            # 发起签到
            # 签到记录
            $signId = $signNum + 1;
            if ($signId == 8) {
                $signId = 1;
            }
            Db::table('log_signs')->insert(array('sign_id' => $signId, 'member_id' => $this->MemberId, 'log_time' => date("Y-m-d H:i:s"), 'seriase_day_num' => $signId));
            # 更新redis
            $this->MemberInfo['today_sign_status'] = true;
            $redis->hSetJson('memberInfo', $this->MemberId, $this->MemberInfo);
            # 判断今天签到是否给奖励
            $isGive = Db::table('sign')->field('sign_status')->where(array('sign_type' => $signId))->find()['sign_status'];
            # 更新redis
            $this->memberSignList();
            # 完成任务=>连续签到
            TimeTask::finishTask($this->MemberId, 17);
            EalspellCommissionService::memberSign($this->MemberId);
            if ($isGive == 1) {
                # 领取签到奖励
                $activeService = new ActiveService();
                $res = $activeService->giveReward($this->MemberId, $signId, 3, false, false, false);
                return array('status' => true, 'msg' => '签到成功', 'rewardInfo' => $res['rewardInfo']);
            } else {
                return array('status' => true, 'msg' => '签到成功', 'rewardInfo' => array());
            }


        } else {
            # 今天已经签到过了
            return array('status' => false, 'msg' => '今天已经签到了');
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 玩家签到列表
     */
    function signList()
    {
        $this->memberSignList();
        $redis = new Redis();
        # 玩家签到列表
        $signList = $redis->hGetJson('memberSignList', $this->MemberId);
        # 连续签到天数
        $signNum = $redis->hGet('memberSignNum', $this->MemberId);
        /*
        1.新增用户签到功能, 连续签到可获得积分或卡券等神秘大礼。
        2.新增任务功能 , 完成任务可领取更多优惠
        2.新增优质商户推荐,本地生活服务
        3.修复了已知问题,优化了使用体验
         * */
        if ($signNum == false) {
            $signNum = 0;
        }
        # 今天是否可以签到
        $isSign = $redis->hGet('memberIsSign', $this->MemberId);
        if ($isSign == false) {
            $isSign = 0;
        }
        if (empty($signList)) {
            $signInfo = $this->memberSignList();
            $signList = $signInfo['signList'];
            $signNum = $signInfo['num'];
            $isSign = $signInfo['isSign'];
        }
        /*  # 玩家签到->奖品领取记录
          $logSignIn = Db::table('active_rewardlog')
              ->field('create_time,reward_cate type,reward_num,reward_title,unit,voucher_title,(case reward_cate
              when 1 then\'积分\'
              when 2 then \'金币\'
              when 3 then \'卡券\'
              when 4 then \'恢复券\'
              end) cate')
              ->where(array('member_code' => $this->MemberId, 'active_category' => 4))
              ->order('id desc')
              ->limit(5)
              ->select();
          if (!empty($logSignIn)) {
              foreach ($logSignIn as $k => $v) {
                  $logSignIn[$k]['time'] = date('Y-m-d H:i', strtotime($v['create_time']));
                  $logSignIn[$k]['typeTitle'] = getsPrizeCategory($v['type']);
              }
          }*/
        //'logSignIn' => $logSignIn
        # 签到规则
        $signRules = array(
            '1.登录用户每天可签到一次。',
            '2.连续签到3天可获得惊喜礼包。',
            '3.连续签到7天可获得积分或卡券等神秘大礼。'
        );
        # 活动规则
        $activeRules = array(
            '1.登录用户每天可签到一次。',
            '2.连续签到3天可获得惊喜礼包。',
            '3.连续签到7天可获得积分或卡券等神秘大礼。'
        );
        return array('status' => true, 'msg' => '查询成功', 'signList' => $signList, 'signNum' => $signNum, 'isSign' => $isSign, 'signRules' => $signRules, 'activeRules' => $activeRules);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 玩家签到->奖品领取记录
     */
    function rewardLog()
    {
        $listRows = 5;
        $pageMark = input('post.page');
        if (empty($pageMark)) {
            $pageMark = 0;
        }
        $page = $pageMark * $listRows;
        $logSignIn = Db::table('active_rewardlog')
            ->field('*,(case reward_cate 
            when 1 then\'积分\'
            when 2 then \'金币\'
            when 3 then \'卡券\'
            when 4 then \'恢复券\'
            end) cate')
            ->where(array('member_code' => $this->MemberId, 'active_category' => 4))
            ->order('id desc')
            ->limit($page, $listRows)
            ->select();
        return array('status' => true, 'msg' => '查询成功', 'logSignIn' => $logSignIn);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 玩家签到情况
     */
    private function memberSignList()
    {
        $redis = new Redis();
        # 查询签到列表
        $signList = $redis->hGetJson('signList', 'list');
        if (empty($signList)) {
            $signList = Db::table('sign')->field('id,sign_type sign_day,sign_status,reward_type')->order('id asc')->select();
            $redis->hSetJson('signList', 'list', $signList);
        }
        # 查询签到记录
        $logSign = Db::table('log_signs')->where(array('member_id' => $this->MemberId))->order('id desc')->find();
        $last_time = strtotime($logSign['log_time']);//最后一次签到时间
        $current_day = strtotime(date('Y-m-d 23:59:59'));//当天时间的最后1刻
        $timeIs = timediff($last_time, $current_day);//时间差
        # 连续签到天数
        $num = 0;
        # 今天是否可以签到
        $isSign = 0;
        if (!empty($logSign)) {
            # 更新连续签到天数
            $num = $logSign['seriase_day_num'];
            foreach ($signList as $key => $value) {
                //判断是否漏签
                if ($timeIs['day'] > 1) {// 漏签
                    $num = 0;
                    if ($value['id'] == 1) {
                        $signList[$key]['is_sign'] = 1;//仅第一天的可以签 即首次签到
                        $isSign = 1;
                    } else {
                        $signList[$key]['is_sign'] = 2;//不可以签
                    }
                } else {// 未漏签
                    if ($logSign['seriase_day_num'] == 7) {//签满7天后
                        if ($timeIs['day'] == 1) {//今天没签
                            $num = 0;
                            if ($value['id'] == 1) {
                                $is_sign = 1;//仅第一天的可以签 即首次签到
                                $isSign = 1;
                            } else {
                                $is_sign = 2;//不可以签
                            }
                        } elseif ($timeIs['day'] == 0) {//今天签过
                            $is_sign = 3;//已签到过
                        }
                    } else {//未签满7天，如1-6天之间
                        if ($timeIs['day'] == 1) {//今天没签
                            if ($value['id'] == $logSign['seriase_day_num'] + 1) {
                                $is_sign = 1;//可以签
                                $isSign = 1;
                            } elseif ($value['id'] <= $logSign['seriase_day_num']) {
                                $is_sign = 3;//已签到过
                            } else {
                                $is_sign = 2;//不可以签
                            }
                        } elseif ($timeIs['day'] == 0) {//今天签过
                            if ($value['id'] <= $logSign['seriase_day_num']) {
                                $is_sign = 3;//已签到过
                            } else {
                                $is_sign = 2;//不可以签
                            }
                        }
                    }
                    $signList[$key]['is_sign'] = $is_sign;
                }
            }
        } else {//首次签到
            foreach ($signList as $k => $v) {
                if ($v['id'] == 1) {
                    $signList[$k]['is_sign'] = 1;//仅第一天的可以签 即首次签到
                    $isSign = 1;
                } else {
                    $signList[$k]['is_sign'] = 2;//不可以签
                }
            }
        }
        $time = strtotime(date('Y-m-d 23:59:59')) - time();
        # 玩家签到列表
        $redis->hSetJson('memberSignList', $this->MemberId, $signList, $time);
        # 连续签到天数
        $redis->hSet('memberSignNum', $this->MemberId, $num, $time);
        # 今天是否可以签到
        $redis->hSet('memberIsSign', $this->MemberId, $isSign, $time);
        return array('signList' => $signList, 'isSign' => $isSign, 'num' => $num);
    }
}
