<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/26
 * Time: 17:34
 */

namespace app\api\controller;


use app\api\ApiService\CarInfoService;
use Redis\Redis;
use think\Db;

class CarInfo extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取用户车辆信息
     */
    function getMemberCar()
    {
        return CarInfoService::memberCarInfo($this->MemberId, true);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 修改车辆公里数
     */
    function modifyCarMileage()
    {
        # 接收车辆id
        $car_id = input('post.id');
        # 接收修改的车辆公里数
        $mileage = input('post.mileage');
        # 接收车牌号
        $car_num = input('post.car_num');
        if (!empty($car_id) and !empty($mileage) and !empty($car_num)) {
            Db::table('member_car')->where(array('id' => $car_id))->update(array('car_mileage' => $mileage));
            # 将该车牌号对应的未结算的订单里面的 公里数 进行修改
            Db::table('orders')->where(array('car_liences' => $car_num))->where("order_status=1 or order_status = 6")
                ->update(array('car_mileages' => $mileage));
            return array('status' => true, 'msg' => '修改成功');
        } else {
            return array('status' => false, 'msg' => '参数为空');
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 删除车辆信息(修改状态为  已删除=>3)
     */
    function delCar()
    {
        # 接收车辆id
        $car_id = input('post.id');
        # 接收车牌号
        $car_num = input('post.car_num');
        if (!empty($car_id) and !empty($car_num)) {
            # 判断是否可以删除车辆
//            $status = CarInfoService::operateDelCar($this->MemberId, $this->MemberLevel);
//            if (!$status['status']) {
//                return $status;
//            }
            Db::table('member_car')->where(array('id' => $car_id))->update(array('status' => 3));
            return array('status' => true, 'msg' => '删除成功');
        } else {
            return array('status' => false, 'msg' => '系统繁忙');
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 获取车辆品牌信息
     */
    function getCarLogo()
    {
        #接收搜索信息
        $_search = input('post.search');
        return CarInfoService::getCarLogo($_search);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 获取车辆品牌对应的系别信息
     */
    function getCarSort()
    {
        # 接收品牌id
        $logoId = input('post.logo_id');
        return CarInfoService::getCarSort($logoId);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 添加车辆时==>当用户输入完车牌号时  查询该车牌号之前时候存在信息  存在返回展示  不存在用户正常填写
     */
    function carInfo()
    {
        $carNum = input('post.car_num');
        if (empty($carNum)) {
            return array('status' => false);
        }
        # 查询该车辆是否存在信息
        $carInfo = Db::table('member_car mc')
            ->field('owner_name,enginecode,car_mileage,car_type_id,grant_register,register_time,car_engine_number,car_recogin_code,queue_status,
            sort_title,level_title,title logo_title,level car_level,logo_id')
            ->join('car_sort cs', 'cs.id=mc.car_type_id', 'left')
            ->join('car_logo cl', 'cl.id=cs.logo_id', 'left')
            ->join('car_level clv', 'clv.id=cs.level', 'left')
            ->where(array('car_licens' => trim(strtoupper($carNum))))
            ->order(array('mc.id' => 'desc'))
            ->find();
        if (empty($carInfo)) {
            return array('status' => false);
        }
        return array('status' => true, 'carInfo' => $carInfo);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 修改车辆信息
     */
    function modifyCarInfo()
    {
        # 接收车辆信息
        $carInfo = input('post.carInfo');
        # 接收操作标识
        $mark = input('post.mark');
        if (empty($carInfo)) {
            return array('status' => false, 'msg' => '车辆信息为空');
        }
        $_data = array(
            'car_recogin_code' => $carInfo['car_recogin_code'],
            'register_time' => $carInfo['register_time'],
            'grant_register' => $carInfo['grant_register'],
            'car_type_id' => $carInfo['car_type_id'],
            'enginecode' => $carInfo['enginecode'],
            'owner_name' => $carInfo['owner_name']
        );
        # 修改车辆信息
        if ($mark == 'modify') {
            # 原车辆品牌,系列,等级信息
            $oldInfo = input('post.oldInfo');
            # 验空
            if (!empty($carInfo['car_recogin_code']) and !empty($carInfo['register_time'])
                and !empty($carInfo['grant_register']) and !empty($carInfo['car_type_id'])
                and !empty($carInfo['enginecode']) and !empty($carInfo['owner_name'])) {
                # 已认证
                $_data['status'] = 2;
            } else {
                $_data['status'] = 1;
            }
            # 修改车辆信息
            Db::table('member_car')
                ->where(array('id' => $carInfo['id']))
                ->update($_data);
            if ($oldInfo['car_type_id'] != $carInfo['car_type_id']) {
                # 将该车牌号对应的未结算的订单里面的 车辆信息修改
                Db::table('orders')->where(array('car_liences' => $carInfo['car_licens']))->where("order_status=1 or order_status = 6")
                    ->update(
                        array('car_level' => $carInfo['car_level'],
                            'car_logo' => $carInfo['car_logo_id'],
                            'car_sort' => $carInfo['car_sort_id'])
                    );
                # 判断车辆等级是否发生了变化
                if ($oldInfo['car_level'] != $carInfo['car_level']) {
                    $orderNumber = Db::table('orders')->field('order_number')->where(array('car_liences' => $carInfo['car_licens']))->where("order_status=1 or order_status = 6")->find();
                    # 需要重新计算订单价格
                    if(!empty($orderNumber)) {
                        $wrietCar = new CarInfoService();
                        $wrietCar->WriteOrderPrice(true, $orderNumber['order_number']);
                    }
//                    $_redis = new Redis();
//                    $_redis->hSet('modifyOrderPrice', $carInfo['car_licens'], '1');
                }
            }
            return array('status' => true, 'msg' > '修改成功');
        }
        # 添加车辆信息
        if ($mark == 'add') {
            # 查询当前绑定的车辆数量, 判断是否可以继续绑定车辆
            $carNum = Db::table('member_car')->where(array('member_id' => $this->MemberId))->where('status != 3')->count('id');
            if ($carNum >= $this->MemberInfo['level_title']['car_limit'] and $this->MemberLevel > 0) {
                return array('status' => false, 'msg' => '超出可绑定车辆数量限制');
            }
            # 判断这个车牌号是否添加过
            $thisInfo = Db::table('member_car')->field('id,member_id')
                ->where(array('car_licens' => $carInfo['car_licens']))->where('status != 3')->select();
            if (!empty($thisInfo)) {
                if (in_array($this->MemberId, array_column($thisInfo, 'member_id'))) {
                    return array('status' => false, 'msg' => '该车已经添加过了');
                } else {
                    # 清空其他人的车
                    Db::table('member_car')->where(array('car_licens' => $carInfo['car_licens']))->update(array('status' => 3));
//                    return array('status' => false, 'msg' => '该车已被别人绑定');
                }
            }
            # 添加车辆信息
            $_data['member_id'] = $this->MemberId;
            $_data['create_time'] = date('Y-m-d H:i:s');
            $_data['car_licens'] = $carInfo['car_licens'];
            $carId = Db::table('member_car')->insert(array_filter($_data));
            # 将该车牌号对应的未结算的订单里面的 车辆信息修改
            Db::table('orders')->where(array('car_liences' => $carInfo['car_licens']))->where("order_status=1 or order_status = 6")
                ->update(
                    array('car_level' => $carInfo['car_level'],
                        'car_logo' => $carInfo['car_logo_id'],
                        'car_sort' => $carInfo['car_sort_id'])
                );
            $orderNumber = Db::table('orders')->field('order_number')->where(array('car_liences' => $carInfo['car_licens']))->where("order_status=1 or order_status = 6")->select();
            if (!empty($orderNumber)) {
                $carInfoService = new CarInfoService();
                foreach ($orderNumber as $k => $v) {
                    $carInfoService->WriteOrderPrice(true, $v['order_number']);
                }
            }
            $carInfo_return = Db::table("member_car mc")
                ->field("mc.*,cs.id car_sort_id,cs.logo_id car_logo_id,cs.sort_title,cs.level car_level,cl.title logo_title,cle.level_title")
                ->join("car_sort cs", "mc.car_type_id=cs.id", 'left')
                ->join("car_logo cl", "cs.logo_id=cl.id", 'left')
                ->join("car_level cle", "cs.level=cle.id", 'left')
                ->order(array('queue_status' => 'asc'))
                ->where(array("mc.id" => $carId))->find();
            return array('status' => true, 'msg' => '绑定成功','carInfo'=>$carInfo_return);
        }
        return array('status' => false, 'msg' => '系统错误');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 设置免排队车辆
     */
    function modifyPrivilegeCar()
    {
        # 接收要绑定特权的车辆id集合
        $carId = input('post.carId');
        if (empty($carId)) {
            return array('status' => false, 'msg' => '车辆id不能为空');
        }
        # 判断能不能进行修改
        if ($this->MemberInfo['level_title']['lineup_car_limit'] <= 0) {
            return array('status' => false, 'msg' => '当前会员等级不支持绑定免排队特权车辆');
        }
        /*# 是否可以操作免排队
        $status = CarInfoService::operateNoQueue($this->MemberId);
        if (!$status['status']) {
            return $status;
        }*/
        # 查询用户免排队车辆数量
        $noQueuing = Db::table('member_car')->where(array('member_id' => $this->MemberId, 'queue_status' => 1, 'status' => 1))->count();
        if ($this->MemberInfo['level_title']['lineup_car_limit'] < ($noQueuing + 1)) {
            return array('status' => false, 'msg' => '超出可绑定特权车辆数量限制');
        }
        # 绑定
        Db::table('member_car')->where(array('id' => $carId))->update(array('queue_status' => 1));

        return array('status' => true, 'msg' => '设置成功');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @content 删除车辆免排队特权
     */
    function removePrivilege()
    {
        # 接收车辆id
        $carId = input('post.carId');
        if (empty($carId)) {
            return array('status' => false, 'msg' => '系统错误,车辆id为空');
        }
        # 是否可以操作免排队
        $status = CarInfoService::operateNoQueue($this->MemberId, $carId);
        if (!$status['status']) {
            return $status;
        }
        Db::table('member_car')->where(array('id' => $carId))->update(array('queue_status' => 2));
        return array('status' => true, 'msg' => '修改成功');
    }
}
