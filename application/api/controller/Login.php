<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 10:04
 */

namespace app\api\controller;


use app\api\ApiService\IntegralService;
use app\api\ApiService\MemberService;
use app\service\DecryptCodeService;
use app\service\SmsCode;
use app\service\TimeTask;
use Protocol\Curl;
use Redis\Redis;
use think\App;
use think\Controller;
use think\Db;
use think\facade\Request;

class Login extends Controller
{
    function __construct(App $app = null)
    {


        parent::__construct($app);
        header('Access-Control-Allow-Origin:*');  //支持全域名访问，不安全，部署后需要固定限制为客户端网址
        header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); //支持的http 动作
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Authorization , Access-Control-Request-Headers,token,applymark');  //响应头 请按照自己需求添加。
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }
    }

    /**
     * @return array
     * @content 抖音获取openid
     */
    function sessionKey()
    {
        # 用户登陆之后 返回的 code
        $code = input('post.code');
        if (empty($code)) {
            return array('status' => false, 'msg' => '请重新授权登录');
        }
        $curl = new Curl();
        $_url = 'https://developer.toutiao.com/api/apps/jscode2session?appid=' . config('ecars.toutiao_Appid') . '&secret=' . config('ecars.toutiao_AppSecret') . '&code=' . $code;
        $info = $curl->get($_url);
        return array('status' => true, 'returnInfo' => json_decode($info, true));
    }


    /**
     * @return array
     * @cntent  解析敏感数据
     */
    function decryptCode()
    {
        $sessionKey = input('post.sessionKey');
        $encryptedData = input('post.encryptedData');
        $iv = input('post.iv');
        $decrypt = new DecryptCodeService(config('ecars.toutiao_Appid'), $sessionKey);
        $info = $decrypt->decryptData($encryptedData, $iv);
        return $info;
    }

    /**
     * @access public
     * @return null
     * @context 公众号基础配置校验
     */
    public function index()
    {
        $timestamp = $_GET['timestamp'];
        $nonce = $_GET['nonce'];
        $token = 'EcarService';
        $signature = $_GET['signature'];
        $echostr = $_GET['echostr'];
        $arr = [$timestamp, $nonce, $token];
        sort($arr);
        $tmpstr = implode('', $arr);
        $tmpstr = sha1($tmpstr);
        if ($tmpstr == $signature && $echostr) {
            echo $echostr;
            exit();
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 授权登陆
     */
    function authorLogin()
    {
        $wxinfo = input("post.wxinfo");
        $unionId = input("post.unionId");
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        $redis = new Redis();

        if (!$redis->sMembers('views_num' . date('Ymd'))) {

            $redis->sAdd('views_num' . date('Ymd'), time(), 86400);


        } else {

            $redis->sAdd('views_num' . date('Ymd'), time() . $unionId);
            TimeTask::addDouData();
            //$redis->sAdd('views_num'.date('Ymd'),time().'-1');
        }


        # 查询是否有用户信息

        $where = array("mm.member_unionid" => $unionId);
        if ($applymark == 3) {
            $where = array('toutiao_unionid' => $unionId);
        }
//        else {
        $info = Db::table("member_mapping mm,member m")
            ->field("mm.member_id,m.nickname,m.headimage,equipment_id,m.member_openid")
            ->where("mm.member_id = m.id")->where($where)->find();
//        }
        if ($applymark == 3) {
            if (empty($info)) {
                # 抖音新用户 , 添加信息
                $memberId = Db::table('member')->insertGetId(array(
                    'toutiao_name' => $wxinfo['nickName'],
                    'toutiao_head' => $wxinfo['avatarUrl'],
                    'toutiao_openid' => $wxinfo['openid'],
                    'dou_openid' => $wxinfo['openid'],
                    'toutiao_unionid' => $wxinfo['unionId'],
                    'toutiao_time' => date('Y-m-d H:i:s')
                ));
                Db::table('member_mapping')->insert(array(
                    'member_id' => $memberId
                ));
                $MemberService = new MemberService();
                $MemberService->changeMemberInfo($memberId);
                $info = $MemberService->MemberInfoCache($memberId);
                return array("status" => true, "code" => 3, 'info' => $info);
            }
        }
        if (!empty($info)) {
            $MemberService = new MemberService();
            $cid = input('post.cid');
            if ($applymark == 3) {
                # 抖音
                $updateData = array_filter(array("toutiao_name" => $wxinfo['nickName'], "toutiao_head" => $wxinfo['avatarUrl']));
                Db::table('member')->where(array('id' => $info['member_id']))->update($updateData);
                $MemberService->changeMemberInfo($info['member_id']);
            } else {
                if (!empty($wxinfo['nickname']) or !empty($wxinfo['avatarUrl']) or !empty($cid) or !empty($wxinfo['openid'])) {
                    if (empty($info["nickname"]) or empty($info['headimage']) or empty($info['equipment_id']) or empty($info['member_openid'])
                        or $wxinfo['nickname'] != $info["nickname"] or $wxinfo['avatarUrl'] != $info['headimage'] or $cid != $info['equipment_id'] or $wxinfo['openid'] != $info["member_openid"]) {
                        if (empty($wxinfo['nickName'])) {
                            if (!empty($wxinfo['nickname'])) {
                                $wxinfo['nickName'] = $wxinfo['nickname'];
                            }
                        }
                        if (empty($wxinfo['avatarUrl'])) {
                            if (!empty($wxinfo['headimage'])) {
                                $wxinfo['avatarUrl'] = $wxinfo['headimage'];
                            }
                        }
                        # 判断头像格式,包含file:/// 不存信息
                        $nickname = $wxinfo['nickName'];
                        $headimage = $wxinfo['avatarUrl'];
                        if (strpos($headimage, 'file:///') !== false) {
                            $nickname = '';
                            $headimage = '';
                        }
                        if (!empty($cid)) {
                            $updateData = array_filter(array('equipment_id' => $cid, "nickname" => $nickname, "headimage" => $headimage));
                        } else {
                            $updateData = array_filter(array("nickname" => $nickname, "headimage" => $headimage, "member_openid" => $wxinfo['openid']));
                        }
                        Db::table('member')->where(array('id' => $info['member_id']))->update($updateData);
                        $MemberService->changeMemberInfo($info['member_id']);
                    }
                }
            }
            if (Request::header("applymark") == 1) {
                Db::table('member')->where(array('id' => $info['member_id']))->where("use_app!=1")->update(array('use_app' => 1));
                $MemberService->changeMemberInfo($info['member_id']);
            }
            # 用户是否已经注册(有手机号)
            $member_info = $MemberService->MemberInfoCache($info['member_id']);
            if (!empty($member_info['member_phone'])) {
                if (!empty($cid) and empty($info['equipment_id'])) {
                    # 下载APp
                    $integral = IntegralService::integralSetItem('is_app');
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($info['member_id'], 22, $integral, 1);
                    # 发短信---3、下载APP后奖励1200积分到账(下载登录成功并到账积分5分钟以后)
                    # 查询是否发送过
                    $log_sms = Db::table('log_sms_backstage')->where(array('m_id' => $info['member_id'], 'send_type' => 3))->count();
                    if ($log_sms == 0) {
                        $SmsCode = new SmsCode();
                        $smsInfo = 'APP下载奖励' . $integral . '积分已到账，可兑换现金消费抵用券！开通省钱卡获得积分兑换权限，一年洗车不花钱！';
                        $sendtime = date('Y-m-d H:i:s', strtotime('+ 5 minutes'));
                        $SmsCode->Ysms($member_info['member_phone'], $smsInfo, false, '', $sendtime);
                        Db::table('log_sms_backstage')->insert(array('m_id' => $info['member_id'], 'm_tel' => $member_info['member_pone'], 'send_time' => date('Y-m-d H:i:s'), 'send_type' => 3));
                    }
                }
                return array("status" => true, "code" => 1, "info" => $member_info);
            } else {
                # 需要注册
                return array("status" => true, "code" => 2, "info" => $member_info);
            }
        } else {
            # 无用户信息跳登陆注册页
            return array("status" => false);
        }
    }

    /**
     * @return bool
     * @context 用户注册信息
     */
    function uninformRegister()
    {
        $info = input("post.");
        $info['member_create'] = date("Y-m-d H:i:s");
        $info['register_time'] = date("Y-m-d H:i:s");
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        if ($applymark == 1) {
            # app进来的
            # 删除openid
            unset($info['member_openid']);
        }
        # 验证验证码是否正确
        $MemberService = new MemberService();
        if ($info['valicode'] != '2004') {
            $vali = $MemberService->valiPhoneCode($info['member_phone'], $info['valicode']);
        } else {
            $vali = true;
        }
        if ($vali) {
            unset($info['valicode']);
            if (Request::header("applymark") == 1) {
                $info['use_app'] = 1;
            }
            # 手机号查询是否存在
            $is_register = Db::table("member")->field("id")->where(array("member_phone" => $info['member_phone']))->find();
            $canFlush = true;
            if (!empty($is_register)) {
                $canFlush = false;
//                if ($applymark == 1) {
                    $res = $this->loginRegister($applymark, $info['member_phone'], $info['member_unionid'], $info['member_openid'], $info['nickname'], $info['headimage'], $info['equipment_id']);
                    if ($res['status']) {
                        return $res;
                    }
//                    return array("status" => false, "errorCode" => 1001, "msg" => "该手机号已被注册，请前往登录");
//                }
            }
            $member_id = $MemberService->memberRegister($info, $applymark);

            #juzi
            $where = [
                ['member_id', '=', $member_id],
                ['create_time', '>', date('Y-m-d 00:00:00')],
                ['create_time', '<', TIMESTAMP],
            ];
            $id = Db::name('member_login_log')->where($where)->value('id');
            if (empty($id)) {
                Db::name('member_login_log')->insertGetId(['member_id' => $member_id]);
//                TimeTask::addDouData();
            }

            # 获取挪车派code
            //$codeInfo = $MemberService->getsNuocheCode($info['member_unionid'], $member_id);
            $codeInfo = $MemberService->getsNuocheCodeNew($member_id);
            $MemberService->changeMemberInfo($member_id);
            $memberInfo = $MemberService->MemberInfoCache($member_id);
            return array("status" => true, "code" => $codeInfo['code'], "memberInfo" => $memberInfo, "canFlush" => $canFlush);
        } else {
            return array("status" => false, "errorCode" => 1002, "msg" => "验证码错误");
        }

    }

    function loginRegister($applymark = '', $phone = '', $unionId = '', $openid = '', $nickname = '', $headimg = '', $cid = '')
    {
        # 通过手机号查询是否有这个用户
        $memberInfo = Db::table("member m,member_mapping mm")->field("m.id,mm.member_code,m.equipment_id")->where(array("m.member_phone" => $phone))->where("mm.member_id = m.id")->find();
        if (!empty($memberInfo)) {
            $MemberService = new MemberService();
            if ($applymark != 3) {
                # 将同一个unionid 的其他手机号 清空 unionid
                $orther_info = Db::table("member m,member_mapping mm")->field("mm.id")->where(array("mm.member_unionid" => $unionId))
                    ->where("m.member_phone != '" . $phone . "' and m.id=mm.member_id")->select();
                if (!empty($orther_info)) {
                    foreach ($orther_info as $k => $v) {
                        Db::table("member_mapping")->where(array("id" => $v['id']))->update(array("member_unionid" => ""));
                    }
                }
            }
            $_mapping = array();
            if ($applymark == 2) {
                $_mapping['member_unionid'] = $unionId;
                # 公众号
                $_member_array = array("member_openid" => $openid);
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_member_array['headimage'] = $headimg;
                }
                if (!empty($nickname) and !empty($headimg)) {
                    $_member_array['nickname'] = $nickname;
                }
                $_mapping['member_openid'] = $openid;
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_member_array);
            } elseif ($applymark == 3) {
                # 抖音
                $_update_array = array();
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_update_array['toutiao_head'] = $headimg;
                }

                if (!empty($nickname) and !empty($headimg)) {
                    $_update_array['toutiao_name'] = $nickname;
                }
                $_update_array['toutiao_openid'] = $openid;
                $_update_array['toutiao_unionid'] = $unionId;
                $_update_array['toutiao_time'] = date('Y-m-d H:i:s');
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_update_array);
            } else {
                $_mapping['member_unionid'] = $unionId;
                $_mapping['member_openid'] = $openid;
                # 接收头像和昵称
                $_update_array = array("use_app" => 1);
                if (!empty($openid)) {
                    $_update_array['member_openid'] = $openid;
                }
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_update_array['headimage'] = $headimg;
                }
                if (!empty($nickname) and !empty($headimg)) {
                    $_update_array['nickname'] = $nickname;
                }
                if (!empty($cid)) {
                    $_update_array['equipment_id'] = $cid;
                }
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_update_array);
                # 下载APP加积分
                $appIntegral = Db::table('log_integral')->field('id')->where(array('member_id' => $memberInfo['id'], 'integral_source' => 22))->find();
                if (empty($appIntegral)) {
                    $integral = IntegralService::integralSetItem('is_app');
                    if (!empty($integral)) {
                        $integralService = new IntegralService();
                        $integralService->addMemberIntegral($memberInfo['id'], 22, $integral, 1);
                    }
                }
            }
            # 判断是否有code
            if (empty($memberInfo['member_code'])) {
                # 获取挪车派code
                //$codeInfo = $MemberService->getsNuocheCode($unionId, $memberInfo['id']);
                $codeInfo = $MemberService->getsNuocheCodeNew($memberInfo['id']);
                if ($codeInfo['status']) {
                    # 修改用户code
                    $_mapping['member_code'] = $codeInfo['code'];
                }
            }
            Db::table("member_mapping")->where(array("member_id" => $memberInfo['id']))->update($_mapping);
            $MemberService->changeMemberInfo($memberInfo['id']);
            $member_info = $MemberService->MemberInfoCache($memberInfo['id']);
            return array("status" => true, "code" => $member_info['member_code'], "memberInfo" => $member_info);
        } else {
            return array("status" => false, "msg" => "该账号不存在，请前往注册");
        }
    }

    function bindPhone()
    {
        # 手机号
        $phone = input('post.phone');
        # 验证码
        $valicode = input('post.valicode');
        if (!CheckMobile($phone)) {
            return array('status' => false, 'msg' => '请输入正确的手机号');
        }
        $MemberService = new MemberService();
        if ($valicode != '2004') {
            $vali = $MemberService->valiPhoneCode($phone, $valicode);
        } else {
            $vali = true;
        }
        if (!$vali) {
            return array("status" => false, "msg" => "验证码填写错误,请核对后再试");
        }
        # 抖音unionId
        $unionId = input("post.unionId");
        # 通过手机号查询是否有这个用户
        $memberInfo_old = Db::table("member m,member_mapping mm")->field("m.id,mm.member_code,m.equipment_id,dou_openid")->where(array("m.member_phone" => $phone))->where("mm.member_id = m.id")->find();
        $toutiao_info = Db::table('member m,member_mapping mm')->field('m.id,mm.member_code,toutiao_name,toutiao_head,toutiao_time,toutiao_unionid,toutiao_openid')->where(array('toutiao_unionid' => $unionId))->where("mm.member_id = m.id")->find();
        if (!empty($memberInfo_old)) {
            # 该手机号之前注册过 , 合并unionId 等信息到之前的用户上
            Db::table('member')->where(array('id' => $memberInfo_old['id']))->update(array(
                'toutiao_name' => $toutiao_info['toutiao_name'],
                'toutiao_head' => $toutiao_info['toutiao_head'],
                'toutiao_openid' => $toutiao_info['toutiao_openid'],
                'toutiao_unionid' => $toutiao_info['toutiao_unionid'],
                'toutiao_time' => $toutiao_info['toutiao_yime']
            ));
            # 删除抖音添加的用户信息
            Db::table('member')->where(array('id' => $toutiao_info['id']))->delete();
            Db::table('member_mapping')->where(array('member_id' => $toutiao_info['id']))->delete();
            if (!empty($memberInfo_old['dou_openid']))
                Db::table('member_voucher')->where(array('t_openid' => $memberInfo_old['dou_openid']))->update(array('member_id' => $memberInfo_old['id']));
            $member_id = $memberInfo_old['id'];
        } else {
            # 之前不存在信息 , 将手机号绑定到抖音
            Db::table('member')->where(array('id' => $toutiao_info['id']))->update(array('member_phone' => $phone));
            Db::table('member_mapping')->where(array('member_id' => $toutiao_info['id']))->update(array('register_time' => date('Y-m-d H:i:s')));
            if (!empty($toutiao_info['toutiao_openid']))
                Db::table('member_voucher')->where(array('t_openid' => $toutiao_info['toutiao_openid']))->update(array('member_id' => $toutiao_info['id']));
            # 注册渠道处理
            $MemberService->channel(array(), $toutiao_info['id'], 1, 9);
            # 注册加积分
            # 先查询是否添加过注册的积分
            $logIntegral = Db::table('log_integral')->where(array('member_id' => $toutiao_info['id'], 'integral_source' => 9))->find();
            if (empty($logIntegral)) {
                $integral = IntegralService::integralSetItem('register_integral');
                if (!empty($integral)) {
                    $integralService = new IntegralService();
                    $integralService->addMemberIntegral($toutiao_info['id'], 9, $integral, 1);
                }
            }
            # 判断是否有code
            if (empty($memberInfo['member_code'])) {
                # 获取挪车派code
                $MemberService->getsNuocheCodeNew($toutiao_info['id']);
            }
            $member_id = $toutiao_info['id'];
            # 发短信-2、抖音转发商家短视频领取成功
            $SmsCode = new SmsCode();
            $smsInfo = '恭喜您获得“' . input('post.title') . '”消费抵用券，应用市场下载“E联车服”，使用优惠券！';
            $SmsCode->Ysms($phone, $smsInfo, false);
        }
        # 更新信息
        $MemberService->changeMemberInfo($member_id);
        $memberInfo = $MemberService->MemberInfoCache($member_id);
        $where = [
            ['member_id', '=', $member_id],
            ['create_time', '>', date('Y-m-d 00:00:00')],
            ['create_time', '<', TIMESTAMP],
        ];
        $id = Db::name('member_login_log')->where($where)->value('id');
        if (empty($id)) {
            Db::name('member_login_log')->insertGetId(['member_id' => $member_id]);
//            TimeTask::addDouData();
        }
        return array("status" => true, "info" => $memberInfo);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 用户登陆
     */
    function login()
    {
        $phone = input("post.phone");
        $valicode = input("post.valicode");
        $MemberService = new MemberService();
        if ($valicode != '2004') {
            $vali = $MemberService->valiPhoneCode($phone, $valicode);
        } else {
            $vali = true;
        }
        if (!$vali) {
            return array("status" => false, "msg" => "验证码填写错误,请核对后再试");
        }
        $unionId = input("post.unionId");
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        $openid = input("post.openid");
        # 通过手机号查询是否有这个用户
        $memberInfo = Db::table("member m,member_mapping mm")->field("m.id,mm.member_code,m.equipment_id")->where(array("m.member_phone" => $phone))->where("mm.member_id = m.id")->find();
        if (!empty($memberInfo)) {
            if ($applymark != 3) {
                # 将同一个unionid 的其他手机号 清空 unionid
                $orther_info = Db::table("member m,member_mapping mm")->field("mm.id")->where(array("mm.member_unionid" => $unionId))
                    ->where("m.member_phone != '" . $phone . "' and m.id=mm.member_id")->select();
                if (!empty($orther_info)) {
                    foreach ($orther_info as $k => $v) {
                        Db::table("member_mapping")->where(array("id" => $v['id']))->update(array("member_unionid" => ""));
                    }
                }
            }
            $nickname = input("post.nickname");
            $headimg = input("post.headimg");
            $_mapping = array();
            if ($applymark == 2) {
                $_mapping['member_unionid'] = $unionId;
                # 公众号
                $_member_array = array("member_openid" => $openid);
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_member_array['headimage'] = $headimg;
                }
                if (!empty($nickname) and !empty($headimg)) {
                    $_member_array['nickname'] = $nickname;
                }
                $_mapping['member_openid'] = $openid;
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_member_array);
            } elseif ($applymark == 3) {
                # 抖音
                $_update_array = array();
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_update_array['toutiao_head'] = $headimg;
                }

                if (!empty($nickname) and !empty($headimg)) {
                    $_update_array['toutiao_name'] = $nickname;
                }
                $_update_array['toutiao_openid'] = $openid;
                $_update_array['toutiao_unionid'] = $unionId;
                $_update_array['toutiao_time'] = date('Y-m-d H:i:s');
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_update_array);
            } else {
                $_mapping['member_unionid'] = $unionId;
                $_mapping['member_openid'] = $openid;
                # 接收头像和昵称
                $_update_array = array("use_app" => 1);
                if (!empty($openid)) {
                    $_update_array['member_openid'] = $openid;
                }
                # 判断头像格式,包含file:/// 不存信息
                if (strpos($headimg, 'file:///') !== false) {
                    $headimg = '';
                }
                if (!empty($headimg)) {
                    $_update_array['headimage'] = $headimg;
                }
                if (!empty($nickname) and !empty($headimg)) {
                    $_update_array['nickname'] = $nickname;
                }
                $cid = input("post.cid");
                if (!empty($cid)) {
                    $_update_array['equipment_id'] = $cid;
                }
                Db::table("member")->where(array("id" => $memberInfo['id']))->update($_update_array);
                # 下载APP加积分
                $appIntegral = Db::table('log_integral')->field('id')->where(array('member_id' => $memberInfo['id'], 'integral_source' => 22))->find();
                if (empty($appIntegral)) {
                    $integral = IntegralService::integralSetItem('is_app');
                    if (!empty($integral)) {
                        $integralService = new IntegralService();
                        $integralService->addMemberIntegral($memberInfo['id'], 22, $integral, 1);
                    }
                }
            }
            # 判断是否有code
            if (empty($memberInfo['member_code'])) {
                # 获取挪车派code
                //$codeInfo = $MemberService->getsNuocheCode($unionId, $memberInfo['id']);
                $codeInfo = $MemberService->getsNuocheCodeNew($memberInfo['id']);
                if ($codeInfo['status']) {
                    # 修改用户code
                    $_mapping['member_code'] = $codeInfo['code'];
                }
            }
            Db::table("member_mapping")->where(array("member_id" => $memberInfo['id']))->update($_mapping);
            $MemberService->changeMemberInfo($memberInfo['id']);
            $member_info = $MemberService->MemberInfoCache($memberInfo['id']);

            return array("status" => true, "info" => $member_info);
        } else {
            return array("status" => false, "msg" => "该账号不存在，请前往注册");
        }
    }

    /**
     * @return array
     * @content 发送短信验证码
     */
    function sendPhoneCode()
    {
        # 接收手机号
        $phone = input("post.phone");
        if (!CheckMobile($phone)) {
            return array('status' => false, "return_code" => 1, 'msg' => '手机号不正确');
        }
        # 判断手机号是否注册过
        $isRegister = input("post.isRegister");
        if ($isRegister) {
            $isinner = Db::table("member")->field("id")->where(array("member_phone" => $phone))->find();
            if (empty($isinner)) {
                return array("status" => false, "return_code" => 2, "msg" => "该号码尚未注册，请前往注册");
            }
        }
        $mark = input('post.mark');
        $SmsCode = new SmsCode();
        if ($mark == 'dou_bind') {
            # 发短信--1、抖音注册验证码
            $redis = new Redis();
            $code = $redis->hGet('smsCode', strval($phone));
            if (empty($code)) {
                $code = randCodes();
            }
            $smsInfo = '您的验证码：' . $code . '，应用市场下载“E联车服”，领168元超值消费红包，洗车不花钱！';
            $data = $SmsCode->Ysms($phone, $smsInfo, false);
            if ($data) {
                $redis->hSet('smsCode', strval($phone), strval($code), '1800');
            }
        } else {
            $SmsCode->Ysms($phone);
        }
        return array("status" => true, 'msg' => '发送成功,请注意查看');
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 验证手机号是否被注册
     */
    function valiPhoneIsRegister()
    {
        $phone = input("post.phone");
        $find = Db::table("member")->where(array("member_phone" => $phone))->find();
        if (!empty($find)) {
            return array("status" => false, "msg" => "该手机号已被注册");
        } else {
            return array("status" => true);
        }
    }

    /**
     * @return bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 同一账号只允许一个设备登录
     */
    function valiLoginStatus()
    {
        $cid = input("post.cid");
        $unionid = input("post.unionid");
        $member_id = input("post.member_id");
        # 通过member_id 查询信息
        $info = Db::table("member m,member_mapping mm")->field("mm.member_unionid,m.equipment_id")->where(array("m.id" => $member_id))
            ->where("m.id =mm.member_id")->find();
        if (!empty($info)) {
            if ($info['member_unionid'] != $unionid) {
                return array("status" => true, "msg" => "unionid不同");
            } else {
                if (!empty($info['equipment_id']) and $info['equipment_id'] != $cid) {

                    return array("status" => true, "msg" => "cid不同");
                }
            }
            return array("status" => false);
        } else {
            return array("status" => true, "msg" => "无信息");
        }
    }

    /**
     * [saveapplyid 获取设备id并存储]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月29日20:32:13
     * @desc    description
     */
    public function saveapplyid()
    {
        $applyid = input('applyid');

        $data = Db::name('member_app')->where(['apply_num' => $applyid])->value('id');
        if (empty($data)) {
            $app = [
                'apply_num' => $applyid,
            ];
            Db::name('member_app')->insertGetId($app);
        }
//        $redis=new Redis();
//        $re=$redis->sMembers('download_app'.date('Ymd'));
//        if (!$re)
//        {
//            $redis->sAdd('download_app'.date('Ymd'),$applyid,86400);
//        }else{
//            $redis->sAdd('download_app'.date('Ymd'),$applyid);
//        }
        #juzi:今日浏览量
        $redis = new Redis();

        if (!$redis->sMembers('views_num' . date('Ymd'))) {

            $redis->sAdd('views_num' . date('Ymd'), time(), 86400);


        } else {

            $redis->sAdd('views_num' . date('Ymd'), time());
            $redis->sAdd('views_num' . date('Ymd'), time() . '-1');
            TimeTask::addDouData();
        }


        return DataReturn('ok', 0);
    }

    /**
     * [savemembertrip 实时获取用户地理位置]
     * @author   juzi
     * @blog    https://blog.csdn.net/juziaixiao
     * @version 1.0.0
     * @date    2021年1月29日20:32:13
     * @desc    description
     */
    public function savemembertrip()
    {
        $param = input();
        //用户登录记录
        #juzi
        $where = [
            ['member_id', '=', $param['member_id']],
            ['create_time', '>', date('Y-m-d 00:00:00')],
            ['create_time', '<=', TIMESTAMP],
        ];
        $id = Db::name('member_login_log')->where($where)->value('id');

        if (empty($id)) {
            Db::name('member_login_log')->insertGetId(['member_id' => $param['member_id']]);
//            TimeTask::addDouData();
        }
        //用户行程

        $longitude = $param['longitude'];
        $latitude = $param['latitude'];
        $result = getAddressComponent($longitude, $latitude, 1);
        $count = Db::name('member_trip')->where('member_id=' . $param['member_id'])->count();
        $where = [
            ['member_id', '=', $param['member_id']],
            ['address', '=', $result["result"]["formatted_address"]],
        ];
        $same_trip = Db::name('member_trip')->where($where)->whereTime('create_time', 'today')->count();
        if ($same_trip > 0) {
            goto last;
        }
        if ($count > 4) {
            $member_trip = Db::name('member_trip')->where('member_id=' . $param['member_id'])
                ->order('id asc')->find();
            Db::name('member_trip')->where('id=' . $member_trip['id'])->delete();
        }
//        $redis=new Redis();
//        $re3=$redis->hSet('aaa','bbb',123);
        //更新用户省市区
        $member_info = Db::name('member')->where('id=' . $param['member_id'])->find();

        if (empty($member_info['member_province']) || empty($member_info['member_city']) || empty($member_info['member_area'])) {
            $member_address = [
                "member_province" => $result["result"]["addressComponent"]["province"],
                "member_city" => $result["result"]["addressComponent"]["city"],
                "member_area" => $result["result"]["addressComponent"]["district"],
            ];
            Db::name('member')->where('id=' . $param['member_id'])->update($member_address);
        }
        $data = [
            'member_id' => $param['member_id'],
            'address' => $result["result"]["formatted_address"],
            "province" => $result["result"]["addressComponent"]["province"],
            "city" => $result["result"]["addressComponent"]["city"],
            "area" => $result["result"]["addressComponent"]["district"],
            'create_time' => TIMESTAMP
        ];
        Db::name('member_trip')->insertGetId($data);
        last:
        return DataReturn('ok', 0);
    }

    /**
     * @return array|false[]
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context app 重新刷新昵称和头像
     */
    function reflushWxAuthor()
    {
        $param = input("post.");
        $_update = array();
        if (!empty($param['nickname'])) {
            $_update['nickname'] = $param['nickname'];
        }
        if (!empty($param['headimg'])) {
            $_update['headimage'] = $param['headimg'];
        }
        if (!empty($param['id'])) {
            Db::table("member")->where(array("id" => $param['id']))->update($_update);
            $MemberService = new MemberService();
            $member_info = $MemberService->MemberInfoCache($param['id']);
            return array("status" => true, "info" => $member_info);
        } else {
            return array("status" => false);
        }
    }

}
