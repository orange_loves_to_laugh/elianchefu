<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/23
 * Time: 16:47
 */

namespace app\api\controller;


use app\api\ApiService\AdvanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\MemberPayCode;
use app\api\ApiService\PriceService;
use app\api\ApiService\VoucherService;
use app\service\IGeTuiService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class Index extends Common
{
    function homeInfo()
    {

        $register_time = $this->MemberInfo['register_time'];
        if (empty($register_time)) {
            if (!empty($this->MemberInfo['member_create'])) {
                $register_time = $this->MemberInfo['member_create'];
            } else {
                $register_time = date("Y-m-d H:i:s");
            }

        }
        # 用户未读消息数量
        $unread = Db::table("news")->where("is_del=2 and (news_accept=1 or news_accept=3 or news_accept=4) and news_sendtime>='" . $register_time . "'
        and news_sendtime<='" . date("Y-m-d H:i:s") . "' and id not in (select news_id from news_accepter where accepter_id = {$this->MemberId} and accepter_type=2)")->count();
        # 红包消息


        return array("status" => true, "unread" => $unread);
    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 红包弹窗
     */
    function redPacketPop()
    {
        $register_redpacket = input("post.register_redpacket");
        $_redis = new Redis();
        //$_redis->hDel("redpacket", "bigRedpacket");
        $redpacket = $_redis->hGetJson("redpacket", "bigRedpacket");
        if (empty($redpacket)) {
            // 查询当前红包是哪个
            $redpacket = Db::table("redpacket")->where("type = 1 and status = 1 and surplus_num > 0 and create_time<='" . date("Y-m-d H:i:s") . "' and end_time >='" . date("Y-m-d H:i:s") . "'")
                ->order("id desc")->find();
            if (!empty($redpacket)) {
                $time = strtotime($redpacket['end_time']) - time();
                $_redis->hSetJson("redpacket", "bigRedpacket", $redpacket, $time);
            }
        }
        if (!empty($redpacket)) {
            if ($redpacket['id'] == $this->MemberInfo['redpacket_id']) {
                return array("status" => false, "msg" => "已领取过当前红包");
            } else {
                if ($this->MemberLevel > 0) {
                    // 会员红包
                    $info = Db::table("redpacket_title")->where(array("redpacket_id" => $redpacket['id'], "person" => 3, "status" => 1))->find();
                } else {
                    if (empty($this->MemberInfo['member_code'])) {
                        $info = Db::table("redpacket_title")->where(array("redpacket_id" => $redpacket['id'], "person" => -2, "status" => 1))->find();
                    } else {
                        # 用户3天内注册 没服务 没消费 领取新人红包
                        if(!empty($this->MemberInfo['register_time']) and date("Y-m-d H:i:s",strtotime('-3 days'))<=$this->MemberInfo['register_time']){
                            # 查询是否有消费记录 和服务记录
                            $consump = Db::table("log_consump")->where(array("member_id"=>$this->MemberId))
                                ->where("log_consump_type=1 or (log_consump_type=2 and income_type not in (2,6,7))")->count();
                            if($consump==0){
                                $info = Db::table("redpacket_title")->where(array("redpacket_id" => $redpacket['id'], "person" => -2, "status" => 1))->find();
                            }else{
                                goto alreadyRegister;
                            }
                        }else{
                            // 已注册红包
                           alreadyRegister : $info = Db::table("redpacket_title")->where(array("redpacket_id" => $redpacket['id'], "person" => 2, "status" => 1))->find();
                        }

                    }

                }


                if (!empty($info)) {
                    $redpacket['info'] = $info;
                    return array("status" => true, "redpacket" => $redpacket);
                } else {
                    return array("status" => false, "msg" => "无对应的红包详情");
                }
            }
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 领取首页红包
     */
    function receivedRedPacket()
    {
        $bigRedpacket_id = input("post.bigRedPacket_id");
        $smallRedPacket_id = input("post.smallRedpacket_id");
        if ($this->MemberInfo['redpacket_id'] == $bigRedpacket_id) {
            return array("status" => false, "msg" => "已领取了该红包");
        } else {
            # 查询红包数量
            $big_info = Db::table("redpacket")->where("id = {$bigRedpacket_id}")->find();
            if ($big_info['surplus_num'] > 0) {
                $VoucherService = new VoucherService();
                $VoucherService->addRedpacket($this->MemberId, $smallRedPacket_id, 1, 1, array("voucher_source" => 12, 'paytype' => 0), $bigRedpacket_id);
                # 扣除红包数量
                Db::table("redpacket")->where("id = {$bigRedpacket_id}")->setDec("surplus_num", 1);
                $_redis = new Redis();
                $redpacket = $_redis->hGetJson("redpacket", "bigRedpacket");
                $redpacket['surplus_num'] -= 1;
                $_redis->hSetJson("redpacket", "bigRedpacket", $redpacket);
                # 用户领取红包记录
                $this->MemberInfo['redpacket_id'] = $bigRedpacket_id;
                $_redis->hSetJson("memberInfo", $this->MemberId, $this->MemberInfo);
                # 发送消息提醒
                $geTui = new IGeTuiService();
                $geTui->MessageNotification($this->MemberId, 10, ['title' => $redpacket['packet_title'], 'price' => $redpacket['price']]);
                return array("status" => true);
            } else {
                return array("status" => false, "msg" => "该红包已下架");
            }
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取红包种的卡券
     */
    function getsRedpacketVoucher()
    {
        $redpacket_id = input("post.redpacket_id");
        $type = input("post.type");
        $VoucherService = new VoucherService();
        $list = $VoucherService->redpacketVoucher($redpacket_id, $type);
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取领取红包记录
     */
    function getsRedpacketLog()
    {
        $VoucherService = new VoucherService();
        $where = " and date_format(rl.create_time,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
        $list = $VoucherService->redpacketLog(20, 1, $where);
        $shareRedPacket = config('share.shareRedPacket');
        $shareRedPacket['jumpUrl'] .= '?shareMemberId=' . $this->MemberId;
        return array("status" => true, "list" => $list, 'shareRedPacket' => $shareRedPacket);
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 活动弹窗
     */
    function activePop()
    {
        $_redis = new Redis();
        //$_redis->hDel("pop", "active");
        $pop = $_redis->hGetJson("pop", "active");
        if (empty($pop)) {
            $pop = Db::table("active_tips")->where(array("status" => 1))->where("active_start <= '" . date("Y-m-d H:i:s") . "' and active_end>='" . date("Y-m-d H:i:s") . "'")
                ->order("id desc")->find();
            if (!empty($pop)) {
                $time = strtotime($pop['active_end']) - time();
                $_redis->hSetJson("pop", "active", $pop, $time);
            }
        }
        if (!empty($pop)) {
            if ($pop['is_relation'] == 1 and $pop['is_type'] == 2) {
                $jump = AdvanceService::getsJumpUrl($pop['jump_type'], $pop['jump_id'],$pop['jump_uri']);
                $pop['url'] = $jump['url'];
                $pop['param'] = $jump['param'];
            }

            # 判断是否符合当前人群
            if ($pop['member_type'] == 3) {
                if ($this->MemberLevel > 0) {
                    return array("status" => true, "info" => $pop);
                } else {
                    return array("status" => false);
                }
            } else {
                if (!empty($this->MemberInfo['member_code'])) {
                    return array("status" => true, "info" => $pop);
                } else {
                    return array("status" => false);
                }
            }
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 生成支付码信息
     */
    function payCodeInfo()
    {
        return MemberPayCode::memberPayCode($this->MemberId);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 分享加积分
     */
    function addShareIntegral()
    {
        # 类型
        # look_article_integral=>查看文章
        $mark = input('post.mark');
        if (empty($mark)) {
            return array('status' => false, 'msg' => '分享成功啦');
        }
        $redis = new Redis();
//        $redis->hDel('addShare', $this->MemberId);
        $repeat = $redis->hGet('addShare', $this->MemberId);
        if (!empty($repeat)) {
            return array('status' => false, 'msg' => '已分享成功');
        }
        # 分享文章(加分享数量)
        if ($mark == 'is_share_club') {
            $clubId = input('post.pid');
            if (!empty($clubId)) {
                Db::table('club')->where(array('id' => $clubId))->setInc('club_transmit', 1);
            }
        }
        if ($mark == 'share_busines_integral') {
            $merchant_id = input("post.pid");
            if (!empty($merchant_id)) {
                Db::table("log_share")->insert(array("member_id" => $this->MemberId, "other_id" => $merchant_id, "type" => 9, "create" => date("Y-m-d H:i:s")));
                Db::table("merchants")->where(array("id" => $merchant_id))->setInc("forward_num", 1);
            }
//            return array('status' => false, 'msg' => '分享成功');
        }
        $isShare = $redis->hGet('isShare', $mark . $this->MemberId);
        if (!empty($isShare) and $mark != 'is_forward') {
            # 分享评论每次都加积分
            return array('status' => false, 'msg' => '分享成功');
        }
        $redis->hSet('addShare', $this->MemberId, 'addIng');
        $integralService = new IntegralService();
        $integral_num = $integralService->integralSetItem($mark);
        $integralService->addMemberIntegral($this->MemberId, integralSource($mark), $integral_num, 1);
        $this->MemberInfo['member_integral'] += intval($integral_num);
        if ($mark == 'share_car_integral') {
            # 完成任务=>'4'=> '分享二手车',
            TimeTask::finishTask($this->MemberId, 4);
        }
        if ($mark == 'is_share_club') {
            # 完成任务=>'6'=> '分享文章',
            TimeTask::finishTask($this->MemberId, 6);
        }
        if ($mark == 'share_busines_integral') {
            # 完成任务=>'5'=> '分享商家',
            TimeTask::finishTask($this->MemberId, 5);
        }
        $redis->hSetJson("memberInfo", $this->MemberId, $this->MemberInfo);
        $redis->hSet('isShare', $mark . $this->MemberId, 'SUCCESS', strtotime(date('Y-m-d 23:59:59')) - time());
        $redis->hDel('addShare', $this->MemberId);
        return array("status" => true, "msg" => "已入账+{$integral_num}积分");
    }

    /**
     * @return bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 大转轮开启状态
     */
    function wheelOpenStatus()
    {
        $active_status = Db::table("game_wheel")->field("id")->where("wheel_status = 1
         and wheel_start <= '" . date("Y-m-d H:i:s") . "' and wheel_end>='" . date("Y-m-d H:i:s") . "'")
            ->find();
        if (!empty($active_status)) {
            return array("status" => true);
        } else {
            return array("status" => false);
        }
    }

}
