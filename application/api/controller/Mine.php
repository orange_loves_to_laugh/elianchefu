<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/9
 * Time: 10:04
 */

namespace app\api\controller;


use app\api\ApiService\AdvanceService;
use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\MerchantService;
use app\api\ApiService\PriceService;
use app\api\ApiService\VoucherService;
use app\service\TaskService;
use Redis\Redis;
use think\Controller;
use think\Db;

class Mine extends Common
{
    /**
     * @return array
     * @context 用户账户信息
     */
    function mineAccessInfo()
    {
        # 判断 充值显示的文字
        if ($this->MemberInfo['logHandle'] == 2) {
            if ($this->MemberInfo['expiration_status'] != 2) {
                $recharge_text = '充值享优惠';
            } else {
                $recharge_text = '充值';
            }
        } else {
            $recharge_text = '充值享优惠';
        }
        # 会员升级信息
        $upgradeInfo = array();
        if ($this->MemberLevel == 1) {
            $upgradeInfo = LevelService::upgradeInfo(1, "up_level<=2");
            if(!empty($upgradeInfo)){
                $upgradeInfo = $upgradeInfo[0];
            }
        }
        return array("status" => true, "info" => $this->MemberInfo, "recharge_text" => $recharge_text, 'upgrade_info' => $upgradeInfo, 'recommendVip' => config('share.recommendVip'));
    }


    /**
     * @return array
     * @context 用户卡券 预约订单等信息
     */
    function memberAssetInfo()
    {
        # 用户可用卡券数量
        $voucherNum = Db::table("member_voucher")->where(array("status" => 1, "member_id" => $this->MemberId))->where("`create`>='" . date("Y-m-d H:i:s") . "'")->count();
        # 用户预约订单待待到店 order_appoin_time >'" . date("Y-m-d H:i:s", strtotime("-30 minutes")) . "'
        $appointNum = Db::table("orders")->where(array("order_type" => 3, "member_id" => $this->MemberId, "order_cancel" => 1))
            ->where("order_status = 1")->count();
        # 用户预约商家订单
        $appointMerchantsNum = Db::table("merchants_order")
            ->where(array("member_id" => $this->MemberId, "order_status" => 1))
            ->where("pay_type=1 or pay_type=6")
            ->count();
        # 用户收藏服务
        $collectionNum = Db::table("member_like")->where(array("type"=>2,"member_id"=>$this->MemberId))->group("club_id")->count();
        # 用户待评论订单
        $evaluationNum = Db::table("orders")->where(array("member_id" => $this->MemberId, "evaluation_status" => 1))
            ->where("order_status=5 or order_status=7")
            ->where("order_over", ">=", date("Y-m-d H:i:s", strtotime('-1 day')))
            ->where("order_type = 2 or order_type = 3")
            ->count();
        # 用户待评论商家订单
        $evaluationMerchantsNum = Db::table("merchants_evaluation me,merchants_order mo")
            ->where(array("me.default_mark" => 1, "mo.member_id" => $this->MemberId, "mo.order_status" => 2))
            ->where("me.merchants_order_id = mo.id and me.create_time >= '" . date("Y-m-d H:i:s", strtotime('-1 day')) . "'")
            ->count();
        # 客服电话
        $serviceTel = AdvanceService::customerServerTel();
        # 签到总开关(1 开启  2关闭)
        $switchSign = Db::table('switch')->field('switch_status')->where(array('switch_type' => 'sign'))->find()['switch_status'];
        # 头条会员信息
        $toutiaoMember = config('share.toutiaoMember');
        $toutiaoShow = config('share.toutiaoShow');
        return array("status" => true, "voucherNum" => $voucherNum, "appointNum" => $appointNum + $appointMerchantsNum, "collectionNum" => $collectionNum, "evaluationNum" => $evaluationNum + $evaluationMerchantsNum, 'serviceTel' => $serviceTel, 'switchSign' => $switchSign, 'toutiaoMember' => $toutiaoMember, 'toutiaoShow' => $toutiaoShow);
    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查看会员特权
     */
    function checkLevelPrivilege()
    {
        $level = input("post.level");
        $list = LevelService::getLevelPrivilege($level);
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取会员对应的充值金额
     */
    function getsLevelRecharge()
    {
        $level = input("post.level");
        $list = LevelService::getLevelRecharge($level);
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @context 充值成功
     */
    function rechargeSuccess()
    {
        $pay_price = input("post.pay_price");
        $pay_type = input("post.pay_type");
        $giving = input("post.giving");
        $order_number = input("post.order_number");
        $employee_id = input("post.employee_id");
        $assign_id = input("post.assign_id");
        $shareMemberId = input("post.shareMemberId");
        $formMark = input('post.formMark');
        $isContinue = true;
        if (!empty($order_number)) {
            $compion = Db::table("log_consump")->where(array("order_number" => $order_number))->find();
            if (!empty($compion)) {
                $isContinue = false;
            }
        } else {
            $compion = Db::table("log_consump")->field("consump_time")->where(array("member_id" => $this->MemberId, "log_consump_type" => 2, "income_type" => 7))->order("id desc")->find();
            if (!empty($compion) and $compion['consump_time'] > time() - 180) {
                $isContinue = false;
            }
        }
        if (!$isContinue) {
            return array("status" => false, "msg" => "订单操作频繁，请联系管理员处理");
        }
        $BalanceService = new BalanceService();
        $_redis = new Redis();
        if (empty($assign_id) and empty($employee_id) and empty($shareMemberId)) {
            $recommend = $_redis->hGetJson("relation_member", $this->MemberId);
            if (!empty($recommend)) {
                //{"share_member_id":"","assignId":"","employeeId":"113","member_id":1733,"formMark":"employeeSal"}
                if (array_key_exists('assignId', $recommend) and !empty($recommend['assignId'])) {
                    $assign_id = $recommend['assignId'];
                }
                if (array_key_exists('employeeId', $recommend) and !empty($recommend['employeeId'])) {
                    $employee_id = $recommend['employeeId'];
                }
                if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                    $shareMemberId = $recommend['share_member_id'];
                }
                if (array_key_exists('formMark', $recommend) and !empty($recommend['formMark'])) {
                    $formMark = $recommend['formMark'];
                }
            }
        }
        # 清除支付状态缓存
        $_redis->hDel('payRedis', $order_number);
        $BalanceService->memberRecharge($this->MemberId, $pay_price, $pay_type, 1, $giving, 1, $order_number, $employee_id, $assign_id, $shareMemberId, $formMark);
        return array("status" => true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员升级信息
     */
    function getsUpgradeInfo()
    {
        $level = input("post.level");
        if ($level == 0) {
            $level = 1;
        }
        return array("status" => true, "list" => LevelService::upgradeInfo($level, "up_level<=5"));
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员升级赠送
     */
    function getsUpgradeGiving()
    {
        $upgradeId = input("post.upgradeId");
        $list = LevelService::upgradeGiving($upgradeId);
//        $list = array_values(array_filter($list, function ($v) {
//            if ($v['giving_type'] != 1 and $v['giving_type'] != 2) return false;
//            return true;
//        }));
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['expira_time'] = date("Y-m-d", strtotime("+{$v['card_time']} days"));
            }
        }
        # 分享会员得积分
        $share_integral = IntegralService::integralSetItem("share_register_integra");
        return array("status" => true, "list" => $list, "share_integral" => $share_integral, 'shareInfo' => config('share.shareVip'));
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 会员升级成功
     */
    function upgradeSuccess()
    {
        $pay_price = input("post.pay_price");
        $pay_type = input("post.pay_type");
        $upgradeId = input("post.upgradeId");
        $up_level = input("post.up_level");
        $order_number = input("post.order_number");
        $employee_id = input("post.employee_id");
        $assign_id = input("post.assign_id");
        $formMark = input('post.formMark');
        $BalanceService = new BalanceService();
        # 查询用户信息
        $memberInfo = Db::table('member_mapping')->field('member_level_id,member_expiration')->where(array('member_id' => $this->MemberId))->find();
        if ($memberInfo['member_level_id'] == $up_level) {
            return array("status" => true);
        }
        $_redis = new Redis();
        # 清除支付状态缓存
        $_redis->hDel('payRedis', $order_number);
        if (empty($assign_id) and empty($employee_id) and empty($shareMemberId)) {
            $recommend = $_redis->hGetJson("relation_member", $this->MemberId);
            if (!empty($recommend)) {
                //{"share_member_id":"","assignId":"","employeeId":"113","member_id":1733,"formMark":"employeeSal"}
                if (array_key_exists('assignId', $recommend) and !empty($recommend['assignId'])) {
                    $assign_id = $recommend['assignId'];
                }
                if (array_key_exists('employeeId', $recommend) and !empty($recommend['employeeId'])) {
                    $employee_id = $recommend['employeeId'];
                }
                if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                    $shareMemberId = $recommend['share_member_id'];
                }
                if (array_key_exists('formMark', $recommend) and !empty($recommend['formMark'])) {
                    $formMark = $recommend['formMark'];
                }
            }
        }
        $BalanceService->memberUpgrade($this->MemberId, $pay_price, $pay_type, 1, 1, $upgradeId, $up_level, $order_number, $assign_id, $employee_id, '', $formMark);
        return array("status" => true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取用户卡券列表
     */
    function getsMemberVoucher()
    {
        $screen_type = input("post.screen_type");
        $VoucherService = new VoucherService();
        $memberId = $this->MemberId;
        $list = $VoucherService->memberVoucher($memberId, array("orderby" => "create asc","screen_type"=>$screen_type));
        $canUse = array();
        $beenUse = array();
        $pastUse = array();
        $nowDate = date("Y-m-d H:i:s");
        if (!empty($list['list'])) {
            foreach ($list['list'] as $k => $v) {
                $Desc = $VoucherService->voucherTypeDesc($v);
                $v['TypeTitle'] = $Desc['TypeTitle'];
                $v['voucherDesc'] = $Desc['voucherDesc'];
                $v['voucher_source_title'] = $Desc['voucherSource'];
                if ($v['status'] == 1) {

                    if ($v['create'] < $nowDate) {
                        # 已过期
                        array_push($pastUse, $v);
                    } else {
                        # 可使用
                        array_push($canUse, $v);
                    }
                } elseif ($v['status'] == 3) {
                    # 待核销也展示在可使用里 放在最上面
                    array_unshift($beenUse, $v);
                } else {
                    # 已使用
                    array_push($beenUse, $v);
                }
            }
        }
        $isHtml = true;
        if($this->MemberId==14285){
            $isHtml = false;
        }
        return array("status" => true, "canUse" => $canUse, "beenUse" => $beenUse, "pastUse" => $pastUse,'isHtml'=>$isHtml);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 我的收藏
     */
    function myCollection()
    {
        $pages = input("post.pages");
        if(empty($pages)){
            $pages = 0;
        }
        $pageNum = $pages*10;
        $list = Db::table("member_like ml")
            ->field("m.level,m.id merchants_id,m.title,m.thumb,m.bond_price")
            ->join("merchants m","m.id = ml.club_id")
            ->where(array("type"=>2,"member_id"=>$this->MemberId))
            ->where("m.area != '鲅鱼圈区' and m.status=1")
            ->group("ml.club_id")
            ->order("ml.id desc")
            ->limit($pageNum,10)
            ->select();
        if (!empty($list)) {
           $Merchants = new Merchants();
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $Merchants->merchantLevel($v['level']);
                $list[$k]['giving_score'] = 1;
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['merchants_id']);
            }
        }
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除收藏
     */
    function deleteMyCollection()
    {
        # 商家id
        $coll_id = input("post.coll_id");
        Db::table("member_like")->where(array("club_id" => $coll_id,"type"=>2,"member_id"=>$this->MemberId))->delete();
        return array("status" => true);
    }

    /**
     * @return array
     * @context 获取积分规则
     */
    function getsIntegralRules()
    {
        return array("status" => true, "rules" => IntegralService::integralRule());
    }

    function getsMemberShareTask(){
        return TaskService::shareTask($this->MemberId);
    }
    function receiveSharePrize(){
        $id = input("post.id");
        return TaskService::receivePrize($this->MemberId,$id);
    }

    /**
     * 获取用户积分返还计划列表
     */
    function getsMemberBackPlan(){
        if($this->MemberInfo['member_level_id']>0){
            if($this->MemberId==9405){
                $this->MemberId=14542;
            }
            # 查询用户办卡记录
            $log_handlecard = Db::table("log_handlecard lh")
                ->field("ml.extra_giving_integral,ml.extra_integral_time,date(lh.member_create) create_time")
                ->join("member_level ml","ml.id=lh.level_id")
                ->where(array("lh.member_id"=>$this->MemberId))
                ->order("lh.id desc")
                ->find();
            if(!empty($log_handlecard)){
                $list = array(array("back_plan_num"=>1000,
                    "dateStr"=>date("m/d",strtotime($log_handlecard['create_time'])),
                    "back_time"=>$log_handlecard['create_time'],"back_status"=>2
                ));
                $info = array();
                $canInfo = true;

                for($i=1;$i<=$log_handlecard['extra_integral_time'];$i++){
                    $back_time = date("Y-m-d",strtotime("+{$i} month",strtotime($log_handlecard['create_time'])));

                    $back_status = 1 ;
                    if($back_time<=date("Y-m-d")){
                        $back_status = 2;
                    }
                    $_array = array("back_plan_num"=>$log_handlecard['extra_giving_integral'],
                        "dateStr"=>date("m/d",strtotime("+{$i} month",strtotime($log_handlecard['create_time']))),
                        "back_time"=>$back_time,"back_status"=>$back_status
                        );
                    if($canInfo and $back_status==1 and $_array['back_time']>date("Y-m-d")){
                        $canInfo = false;
                        # 计算距离上次领取一共多少天
                        if($i==1){
                            $lastDate = $log_handlecard['create_time'];
                        }else{
                           $lastDate =  $list[$i-2]['back_time'];
                        }

                        $_array['diff_count_day'] = timediff($lastDate,$_array['back_time'],false)['day'];

                        $info = $_array;
                    }
                    array_push($list,$_array);
                }
                if(!empty($info)){
                    # 计算下一次反的日期到今天还有多少天
                    $timeArr=timediff(date("Y-m-d"),$info['back_time'],false);

                    $info['diff_day'] = $timeArr['day'];
                }
                return array("status"=>true,"list"=>$list,"info"=>$info,"countMonth"=>$log_handlecard['extra_integral_time'],
                    );
            }else{
                return array("status"=>false);
            }
        }else{
            return array("status"=>false);
        }

    }
}
