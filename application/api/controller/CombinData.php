<?php


namespace app\api\controller;



use app\api\ApiService\orderService;
use think\App;
use think\Controller;
use think\Db;

class CombinData extends Controller
{
    public $old_db;
    public $new_db;
    function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->old_db = Db::connect("ecars_old");
        $this->new_db = Db::connect("ecars");
    }


    function combinStart()
    {

        //$this->bizincome();
        $this->biztraffic();
        $this->logconsump();
        $this->loghandlecard();
        $this->logservice();
        $this->orders();
    }


    function memberLevel(){
        # 将所有用户的等级升1级
        Db::table("member_mapping")->where("member_level_id > 0")->setInc("member_level_id",1);
        return true;
    }
    function memberCode(){
        $i=0;
        while($i<=9){
            # 查询用户code
            $membercode = Db::table("member_mapping")->field("member_code")->where("member_code like '".$i."%'")->select();
            if(!empty($membercode)){
                $_array=array();
                foreach($membercode as $k=>$v){
                    $str= intval(substr($v['member_code'],1));
                    array_push($_array,$str);
                }
                $_code_array=implode(',',$_array);
                $codes = Db::table("codes_".$i)->where("codes_bind=0 and codes_status=0 and find_in_set('{$_code_array}',id)=0")->update(array("codes_bind"=>1));

            }
            $i++;

        }
    }

    function memberVoucher()
    {
        $oldList =  Db::connect("ecars_old")->table("member_voucher")->where("`create`>'".date("Y-m-d H:i:s")."'")->where("status=1")->select();

        $_add = array();
        foreach($oldList as $k=>$v){
            if ($v['voucher_source'] == 4) {
                # 卡券为现金红包,查询现金红包对应得详情
                $detailInfo =  Db::connect("ecars_old")->table('redpacket_detail')->where(array('id' => $v['voucher_id']))->find();

                $oldList[$k]['voucher_id'] = 0;
                $oldList[$k]['card_title'] = $detailInfo['voucher_title'];
                $oldList[$k]['voucher_cost'] = sprintf('%.2f', $detailInfo['voucher_price']);
                $oldList[$k]['card_time'] = $detailInfo['voucher_over'];
                $oldList[$k]['server_id'] = $detailInfo['voucher_id'];;
                $oldList[$k]['start_time'] = date("Y-m-d H:i:s", strtotime("- " . $detailInfo['voucher_over'] . " days", strtotime($v['create'])));
                $oldList[$k]['voucher_type'] = $detailInfo['voucher_type'];
                $oldList[$k]['money_off'] = sprintf('%.2f', $detailInfo['money_off']);
                $oldList[$k]['voucher_start'] = $detailInfo['voucher_start'];
            } else {
                # 查询卡券表信息
                $detailInfo = Db::connect("ecars_old")->table('card_voucher')->where(array('id' => $v['voucher_id']))->find();

                $oldList[$k]['card_title'] = $detailInfo['card_title'];
                $oldList[$k]['voucher_cost'] = sprintf('%.2f', $detailInfo['card_price']);
                if($detailInfo['card_type_id']==1){
                    # 服务卡券
                    $oldList[$k]['voucher_type'] = 4;
                }elseif($detailInfo['card_type_id']==3){
                    # 商品卡券
                    $oldList[$k]['voucher_type'] = 3;
                }else{
                    # 现金抵值券
                    $oldList[$k]['voucher_type'] = 6;
                }
                $oldList[$k]['card_time'] = $detailInfo['card_time'];
                $oldList[$k]['server_id'] =  $detailInfo['server_id'];
                $oldList[$k]['start_time'] = date("Y-m-d H:i:s", strtotime("- " . $detailInfo['card_time'] . " days", strtotime($v['create'])));
                $oldList[$k]['money_off'] = 0;
                $oldList[$k]['voucher_start'] = 1;

            }
            array_push($_add,array("voucher_id"=>$oldList[$k]['voucher_id'],"member_id"=>$v['member_id'],"create"=>$v['create'],"status"=>$v['status'],
                "payprice"=>$v['payprice'],"voucher_source"=>$v['voucher_source'],"paytype"=>$v['paytype'],"redpacket_id"=>$v['redpacket_id'],
                "voucher_cost"=>$oldList[$k]['voucher_cost'],"card_title"=>$oldList[$k]['card_title'],"card_time"=>$oldList[$k]['card_time']
            ,"server_id"=>$oldList[$k]['server_id'],"start_time"=>$oldList[$k]['start_time'],"voucher_type"=>$oldList[$k]['voucher_type'],
                "money_off"=>$oldList[$k]['money_off'],"voucher_start"=>$oldList[$k]['voucher_start']));
        }
        Db::table("member_voucher")->insertAll($_add);
        $this->changeVoucher();
        return true;
    }

    function changeVoucher(){
        $_array=array(
            array("old_server"=>102,"new_server"=>9,"card_title"=>'汽车普洗服务',"voucher_id"=>28),
            array("old_server"=>1,"new_server"=>10,"card_title"=>'汽车快捷精洗服务',"voucher_id"=>13),
            array("old_server"=>41,"new_server"=>94,"card_title"=>'全车精品机械打蜡（含蜡）',"voucher_id"=>102),
            array("old_server"=>76,"new_server"=>241,"card_title"=>'精准四轮定位服务',"voucher_id"=>249),
            array("old_server"=>91,"new_server"=>29,"card_title"=>'全车内饰深度清洁+养护',"voucher_id"=>37),
            array("old_server"=>38,"new_server"=>91,"card_title"=>'全车普通手工打蜡（含蜡）',"voucher_id"=>99),
            array("old_server"=>93,"new_server"=>95,"card_title"=>'全车极致手工打蜡（含蜡）',"voucher_id"=>103),
            array("old_server"=>133,"new_server"=>204,"card_title"=>'高端清洗进气管道',"voucher_id"=>212),
            array("old_server"=>44,"new_server"=>105,"card_title"=>'全车漆面普通封釉',"voucher_id"=>113),
            array("old_server"=>53,"new_server"=>99,"card_title"=>'全车极致抛光服务',"voucher_id"=>107),
            array("old_server"=>82,"new_server"=>83,"card_title"=>'全车犀牛皮（门板、底坎、手扣）',"voucher_id"=>91),
            array("old_server"=>78,"new_server"=>56,"card_title"=>'车辆补胎服务',"voucher_id"=>64),
            array("old_server"=>131,"new_server"=>201,"card_title"=>'高端清洗喷油嘴',"voucher_id"=>209),
            array("old_server"=>43,"new_server"=>106,"card_title"=>'全车漆面高端封釉',"voucher_id"=>114),
            array("old_server"=>75,"new_server"=>206,"card_title"=>'高端清洗三元催化',"voucher_id"=>214),
            array("old_server"=>87,"new_server"=>24,"card_title"=>'门板高端清洁+养护（4门）',"voucher_id"=>32),
            array("old_server"=>51,"new_server"=>87,"card_title"=>'全车漆面氧化层去除',"voucher_id"=>95),
            array("old_server"=>18,"new_server"=>245,"card_title"=>'空调管道可视清洗服务',"voucher_id"=>253),
            array("old_server"=>103,"new_server"=>107,"card_title"=>'全车漆面普通镀膜',"voucher_id"=>115),
            array("old_server"=>48,"new_server"=>111,"card_title"=>'全车漆面普通镀晶（3次含抛光）',"voucher_id"=>119),
            array("old_server"=>39,"new_server"=>92,"card_title"=>'全车普通机械打蜡（含蜡）',"voucher_id"=>100),
            array("old_server"=>17,"new_server"=>78,"card_title"=>'车辆消毒服务',"voucher_id"=>86),
            array("old_server"=>79,"new_server"=>56,"card_title"=>'车辆补胎服务',"voucher_id"=>64),
            array("old_server"=>208,"new_server"=>33,"card_title"=>'全车玻璃去油膜',"voucher_id"=>41),
            array("old_server"=>40,"new_server"=>96,"card_title"=>'全车极致机械打蜡（含蜡）',"voucher_id"=>104),
        );
        if(!empty($_array)){
            foreach($_array as $k=>$v){
                Db::table("member_voucher")->where("voucher_type=4 and redpacket_id>0 and server_id = {$v['old_server']}")
                    ->update(array("server_id"=>$v['new_server'],"card_title"=>$v['card_title']."抵用券"));
                Db::table("member_voucher")->where("voucher_type=4 and redpacket_id=0 and server_id = {$v['old_server']}")
                    ->update(array("server_id"=>$v['new_server'],"card_title"=>$v['card_title']."券","voucher_id"=>$v['voucher_id']));
            }
        }

    }

    function bizincome()
    {
        # 门店收入
        # 支付宝
        Db::table("biz_income")->where("pay_type=1")->update(array("pay_type"=>9));
        # 微信
        Db::table("biz_income")->where("pay_type=2")->update(array("pay_type"=>1));
        Db::table("biz_income")->where("pay_type=9")->update(array("pay_type"=>2));
        # 现金
        Db::table("biz_income")->where("pay_type=3")->update(array("pay_type"=>9));
        # 银联
        Db::table("biz_income")->where("pay_type=4")->update(array("pay_type"=>3));
        Db::table("biz_income")->where("pay_type=9")->update(array("pay_type"=>4));
        return true;
    }

    function logconsump()
    {
        //支付方式1：余额 2 微信3.支付宝 4 银联5现金
        //新 : 1微信 2支付宝  3银联  4现金  5余额  6卡券  7积分
        # 消费记录
        # 支付宝
        Db::table("log_consump")->where("consump_pay_type=3")->update(array("consump_pay_type"=>9));
        # 银联
        Db::table("log_consump")->where("consump_pay_type=4")->update(array("consump_pay_type"=>3));
        # 现金
        Db::table("log_consump")->where("consump_pay_type=5")->update(array("consump_pay_type"=>4));
        # 余额
        Db::table("log_consump")->where("consump_pay_type=1")->update(array("consump_pay_type"=>5));
        # 微信
        Db::table("log_consump")->where("consump_pay_type=2")->update(array("consump_pay_type"=>1));
        Db::table("log_consump")->where("consump_pay_type=9")->update(array("consump_pay_type"=>2));
        return true;
    }

    function loghandlecard()
    {
        # 办卡记录
        # 银联
        Db::table("log_handlecard")->where("pay_type=4")->update(array("pay_type"=>9));
        # 现金
        Db::table("log_handlecard")->where("pay_type=3")->update(array("pay_type"=>4));

        Db::table("log_handlecard")->where("pay_type=9")->update(array("pay_type"=>3));
        return true;
    }

    function logservice()
    {
        # 服务记录
         # 支付宝
        Db::table("log_service")->where("pay_type=3")->update(array("pay_type"=>9));
        # 银联
        Db::table("log_service")->where("pay_type=4")->update(array("pay_type"=>3));
        # 现金
        Db::table("log_service")->where("pay_type=5")->update(array("pay_type"=>4));
        # 余额
        Db::table("log_service")->where("pay_type=1")->update(array("pay_type"=>5));
        # 微信
        Db::table("log_service")->where("pay_type=2")->update(array("pay_type"=>1));
        Db::table("log_service")->where("pay_type=9")->update(array("pay_type"=>2));
        return true;
    }

    function orders()
    {
        // 1:微信 2 ：支付宝3：余额4：银联支付 5：现金支付 6：卡券 ,7:积分
        //新 : 1微信 2支付宝  3银联  4现金  5余额  6卡券  7积分
        # 订单

        # 银联
        Db::table("orders")->where("pay_type=4")->update(array("pay_type"=>9));
        # 现金
        Db::table("orders")->where("pay_type=5")->update(array("pay_type"=>4));
        # 余额
        Db::table("orders")->where("pay_type=3")->update(array("pay_type"=>5));

        Db::table("orders")->where("pay_type=9")->update(array("pay_type"=>3));
        return true;
    }

    function biztraffic()
    {
        # 到店记录
        # 银联
        Db::table("biz_traffic")->where("pay_type=4")->update(array("pay_type"=>9));
        # 现金
        Db::table("biz_traffic")->where("pay_type=3")->update(array("pay_type"=>4));

        Db::table("biz_traffic")->where("pay_type=9")->update(array("pay_type"=>3));
        return true;
    }

    function member(){
        $list=Db::connect("ecars_old")->table('member')->select();
        $_array=array();
        foreach($list as $k=>$v){
            array_push($_array,array("id"=>$v['id'],"member_phone"=>$v['member_phone']));
        }
        Db::table("member")->insertAll($_array);
        return true;
    }

    function employee()
    {
        $list=Db::connect("ecars_old")->table("employee e,employee_mapping em")->field("e.*,e.id eid,em.*,em.id emid")->where("e.id = em.employee_id")->select();
        $_array=array();
        foreach($list as $k=>$v){
            array_push($_array,array("id"=>$v['eid'],"employee_name"=>$v['employee_name'],"employee_sex"=>$v['employee_sex'],"employee_code"=>$v['employee_name'],
                "employee_phone"=>$v['employee_phone'],"employee_status"=>$v['employee_status'],"employee_create"=>$v['employee_create'],"regular_time"=>$v['employee_positive'],
                "employee_update"=>$v['employee_update'],"biz_id"=>$v['biz_id'],"employee_idcard"=>$v['employee_idcard'],"employee_openid"=>$v['employee_openid'],
                "employee_head"=>$v['employee_head'],"ranking_status"=>$v['ranking_status'],"satrap"=>$v['satrap'],"wx_authority"=>$v['wx_authority'],
                "employee_hugh"=>$v['employee_hugh'],"scores_ranking"=>$v['scores_ranking'],"employee_age"=>$v['employee_age'],"contract_code"=>$v['contract_code'],
                "is_del"=>$v['del_status'],"cid"=>$v['cid'],"address"=>$v['employee_address'],"nation"=>$v['employee_national'],
                "registered_cate"=>$v['register_type'],"political_landscape"=>$v['polictical'],"marriage_status"=>$v['employee_marry'],"education"=>$v['education'],
                "education_major"=>$v['professional'],"emergency_phone"=>$v['linkman_phone'],"job_remark"=>$v['jobs_info'],"social_security_code"=>$v['social_code'],
                "bank"=>$v['employee_bank'],"base_salary"=>$v['basic_salary'],"station_salary"=>$v['positive_salary'],"fullattendance_salary"=>$v['trial_full'],
                "regular_fullattendance_salary"=>$v['formal_full'],"regular_base_salary"=>$v['basic_salary'],"regular_station_salary"=>$v['positive_salary'],
                "bank_code"=>$v['employee_bank_card'],"jobs_info"=>$v['jobs_info'],"employee_mark"=>1,
                ));
        }
        Db::table("employee")->insertAll($_array);
        return true;
    }

    function membercar()
    {
        $list=Db::connect("ecars_old")->table("member_car")->select();
        $_array=array();
        foreach($list as $k=>$v){
            array_push($_array,array("id"=>$v['id'],"car_licens"=>$v['car_licens'],"car_recogin_code"=>$v['car_recogin_code'],"car_engine_number"=>$v['car_engine_number'],
                "register_time"=>$v['register_time'],"grant_register"=>$v['grant_register'],"car_type_id"=>$v['car_type_id'],"car_mileage"=>$v['car_mileage'],
                "member_id"=>0,"status"=>1,"queue_status"=>2));
        }
        Db::table("member_car")->insertAll($_array);
        return true;
    }

    function serviceCarDiscount()
    {
        $list = Db::table("service_car_mediscount")->where(array("biz_id"=>0))->select();
        $_array=array();
        foreach($list as $k=>$v){
            array_push($_array,array("service_id"=>$v['service_id'],"car_level_id"=>$v['car_level_id'],"discount"=>$v['discount'],
                "service_type"=>$v['service_type'],"biz_id"=>1,"settlement_amount"=>$v['settlement_amount']));
        }
        Db::table("service_car_mediscount")->insertAll($_array);
        return true;
    }

    function servicebiz()
    {
        $list = Db::table("service")->field("id")->where(array("is_del"=>2))->select();
        $_array=array();
        foreach($list as $k=>$v){
            array_push($_array,array("service_id"=>$v['id'],"biz_id"=>1));
        }
        Db::table("service_biz")->insertAll($_array);
        return true;
    }


    function weijingxi()
    {
        $list = Db::table("member_voucher")->where(array("card_title"=>"爱车微晶洗服务券"))->select();
        foreach($list as $k=>$v){
            if($v['redpacket_id']==0){
                # 全抵
                Db::table("member_voucher")->where(array("id"=>$v['id']))->update(array("card_title"=>"汽车微晶洗服务券","voucher_id"=>15,"server_id"=>12));
            }else{
                Db::table("member_voucher")->where(array("id"=>$v['id']))->update(array("card_title"=>"汽车微晶洗服务抵用券","server_id"=>12));
            }
        }
        return  true;
    }
    //array("old_server"=>210,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
    //array("old_server"=>212,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
    // array("old_server"=>214,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
    //array("old_server"=>239,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
    //array("old_server"=>160,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
    //array("old_server"=>83,"old_title"=>"全车手工打蜡（手工费）","new_server"=>96,"card_title"=>'全车极致机械打蜡（含蜡）'),
    //array("old_server"=>63,"old_title"=>"全车手工打蜡（手工费）","new_server"=>96,"card_title"=>'全车极致机械打蜡（含蜡）'),
    //array("old_server"=>68,"old_title"=>"局部抛光","new_server"=>90,"card_title"=>'全车机械打蜡手工费'),
    //array("old_server"=>58,"old_title"=>"全车手工极致打蜡（含蜡含手工费）","new_server"=>95,"card_title"=>'全车极致手工打蜡（含蜡）'),
    // array("old_server"=>69,"old_title"=>"全车漆面封釉（不含抛光）","new_server"=>95,"card_title"=>'全车极致手工打蜡（含蜡）'),
    //array("old_server"=>64,"old_title"=>"轮胎动平衡服务","new_server"=>53,"card_title"=>'轮胎动平衡服务'),
    //array("old_server"=>3,"old_title"=>"全车深度清洁套餐一（含发动机）","new_server"=>53,"card_title"=>'轮胎动平衡服务'),//不要了
    //array("old_server"=>19,"old_title"=>"汽车轮毂免拆卸清洁养护（单个价）","new_server"=>53,"card_title"=>'轮胎动平衡服务'),//不要了
    //array("old_server"=>138,"old_title"=>"##更换刹车片工时费","new_server"=>53,"card_title"=>'轮胎动平衡服务'),  // 不要了
    //array("old_server"=>150,"old_title"=>"##更换火花塞工时费","new_server"=>87,"card_title"=>'全车漆面氧化层去除'),// 不要了
    //array("old_server"=>145,"old_title"=>"##更换防冻液工时费","new_server"=>199,"card_title"=>'空调氟利昂加注工时费/次'),// 不要了
    //array("old_server"=>139,"old_title"=>"##更换刹车盘工时费","new_server"=>13,"card_title"=>'汽车座椅普通清洁养护（单座）'),// 不要了
    //array("old_server"=>34,"old_title"=>"发动机舱深度清洁+养护","new_server"=>25,"card_title"=>'发动机舱清洗服务（普通）'),// 不要了
    //array("old_server"=>144,"old_title"=>"##更换助力油工时费","new_server"=>13,"card_title"=>'汽车座椅普通清洁养护（单座）'),// 不要了
    //array("old_server"=>147,"old_title"=>"##更换变速箱油工时费","new_server"=>103,"card_title"=>'局部柏油去除服务'),// 不要了
    //array("old_server"=>148,"old_title"=>"##更换齿轮油工时费","new_server"=>206,"card_title"=>'高端清洗三元催化',),// 不要了
    //array("old_server"=>151,"old_title"=>"##更换单侧减震器工时费","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了
    //array("old_server"=>142,"old_title"=>"##水箱清洗工时费","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了
    //array("old_server"=>141,"old_title"=>"##更换汽油滤芯工时费","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了
    //array("old_server"=>146,"old_title"=>"##更换后桥油工时费","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了
    //array("old_server"=>152,"old_title"=>"#更换蓄电池工时费","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了
    //array("old_server"=>191,"old_title"=>"更换前后轴承（工时费）","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),// 不要了

    function changeServerArray(){
        return array(
            array("old_server"=>155,"old_title"=>"##底盘装甲液增加一瓶","new_server"=>247,"card_title"=>'补充底盘装甲液1瓶'),
            array("old_server"=>9,"old_title"=>"汽车仪表台深度清洁服务","new_server"=>155,"card_title"=>'仪表台高端清洁养护'),
            array("old_server"=>102,"old_title"=>"**爱车普洗服务","new_server"=>9,"card_title"=>'汽车普洗服务',"voucher_id"=>28),
            array("old_server"=>10,"old_title"=>"汽车仪表台上光养护服务","new_server"=>154,"card_title"=>'仪表台普通清洁养护'),
            array("old_server"=>1,"old_title"=>"爱车快捷精洗服务","new_server"=>10,"card_title"=>'汽车快捷精洗服务',"voucher_id"=>13),
            array("old_server"=>2,"old_title"=>"爱车深度精洗服务","new_server"=>11,"card_title"=>'汽车深度精洗服务'),
            array("old_server"=>92,"old_title"=>"全车高端清洁服务（含打蜡）","new_server"=>11,"card_title"=>'汽车深度精洗服务'),
            array("old_server"=>13,"old_title"=>"全车内饰清洁（含顶棚、仪表台、门板、座椅）","new_server"=>28,"card_title"=>'全车内饰清洁服务'),
            array("old_server"=>12,"old_title"=>"汽车真皮座椅清洁养护服务(单座价）","new_server"=>13,"card_title"=>'汽车座椅普通清洁养护（单座）'),
            array("old_server"=>156,"old_title"=>"爱车微晶洗服务","new_server"=>12,"card_title"=>'汽车微晶洗服务'),
            array("old_server"=>14,"old_title"=>"全车内饰深度高端清洁（含顶棚、仪表台、门板、座椅）","new_server"=>28,"card_title"=>'全车内饰清洁服务'),
            array("old_server"=>171,"old_title"=>"全车真皮座椅清洁（5-7座）","new_server"=>14,"card_title"=>'汽车座椅普通清洁养护（全车）'),
            array("old_server"=>89,"old_title"=>"真皮座椅高端清洁养护(单座价）","new_server"=>15,"card_title"=>'汽车座椅高端清洁养护（单座）'),
            array("old_server"=>78,"old_title"=>"轮胎漏洞修补 补胎（单次17寸以下）","new_server"=>64,"card_title"=>'车辆补胎服务'),
            array("old_server"=>17,"old_title"=>"全车内饰去甲醛消毒杀菌","new_server"=>78,"card_title"=>'车辆消毒服务',"voucher_id"=>86),
            array("old_server"=>170,"old_title"=>"全车坐垫（套）清洗","new_server"=>17,"card_title"=>'全车坐垫清洗（普通）'),
            array("old_server"=>18,"old_title"=>"汽车空调清洁服务","new_server"=>245,"card_title"=>'空调管道可视清洗服务',"voucher_id"=>253),
            array("old_server"=>97,"old_title"=>"全车坐垫套清洗","new_server"=>18,"card_title"=>'全车坐垫套清洗（高端）'),
            array("old_server"=>109,"old_title"=>"全车脚垫清洗","new_server"=>19,"card_title"=>'全车脚垫清洗（毛脚垫）'),
            array("old_server"=>90,"old_title"=>"顶棚清洁（含ABC柱）","new_server"=>21,"card_title"=>'顶棚普通清洗（含ABC柱）'),
            array("old_server"=>87,"old_title"=>"全车门板深度翻新清洁+养护（四门）","new_server"=>24,"card_title"=>'门板高端清洁+养护（4门）',"voucher_id"=>32),
            array("old_server"=>6,"old_title"=>"汽车门板深度清洁（单门）","new_server"=>24,"card_title"=>'门板高端清洁+养护（4门）'),
            array("old_server"=>33,"old_title"=>"汽车发动机舱清洁服务","new_server"=>25,"card_title"=>'发动机舱清洗服务（普通）'),
            array("old_server"=>29,"old_title"=>"汽车轮毂深度拆卸清洁养护（单个价）","new_server"=>35,"card_title"=>'全车轮毂拆卸清洁'),
            array("old_server"=>91,"old_title"=>"全车内饰深度高端清洁+养护（含顶棚、仪表台、门板、座椅）","new_server"=>29,"card_title"=>'全车内饰深度清洁+养护',"voucher_id"=>37),
            array("old_server"=>207,"old_title"=>"玻璃祛水印","new_server"=>32,"card_title"=>'全车玻璃去水印'),
            array("old_server"=>208,"old_title"=>"玻璃祛油膜","new_server"=>33,"card_title"=>'全车玻璃去油膜',"voucher_id"=>41),
            array("old_server"=>42,"old_title"=>"全车柏油去除服务","new_server"=>104,"card_title"=>'全车柏油去除服务'),
            array("old_server"=>115,"old_title"=>"##全车隐形车衣","new_server"=>42,"card_title"=>'全车隐形车衣工时费'),
            array("old_server"=>53,"old_title"=>"全车极致抛光服务","new_server"=>99,"card_title"=>'全车极致抛光服务'),
            array("old_server"=>77,"old_title"=>"轮胎动平衡服务","new_server"=>53,"card_title"=>'轮胎动平衡服务'),
            array("old_server"=>187,"old_title"=>"轮胎充氮气","new_server"=>55,"card_title"=>'单轮轮胎氮气'),
            array("old_server"=>56,"old_title"=>"顶棚鹿皮绒+ABC柱","new_server"=>138,"card_title"=>'顶棚鹿皮绒改装（含ABC柱）'),
            array("old_server"=>79,"old_title"=>"高端轮胎漏洞修补","new_server"=>56,"card_title"=>'车辆补胎服务',"voucher_id"=>64),
            array("old_server"=>161,"old_title"=>"汽车补胎（单轮）","new_server"=>56,"card_title"=>'车辆补胎服务'),
            array("old_server"=>81,"old_title"=>"全车门板普通清洁（四门）","new_server"=>156,"card_title"=>'门板普通清洁+养护（4门）'),
            array("old_server"=>108,"old_title"=>"大灯翻新修复（单个灯）","new_server"=>81,"card_title"=>'前大灯翻新服务（单个）'),
            array("old_server"=>82,"old_title"=>"全车犀牛皮","new_server"=>83,"card_title"=>'全车犀牛皮（门板、底坎、手扣）',"voucher_id"=>91),
            array("old_server"=>51,"old_title"=>"全车氧化层去除服务","new_server"=>87,"card_title"=>'全车漆面氧化层去除'),
            array("old_server"=>37,"old_title"=>"全车机械打蜡（手工费）","new_server"=>90,"card_title"=>'全车机械打蜡手工费'),
            array("old_server"=>38,"old_title"=>"全车手工打蜡（含蜡含手工费）","new_server"=>91,"card_title"=>'全车普通手工打蜡（含蜡）',"voucher_id"=>99),
            array("old_server"=>53,"old_title"=>"全车手工打蜡（手工费）","new_server"=>99,"card_title"=>'全车极致抛光服务',"voucher_id"=>107),
            array("old_server"=>39,"old_title"=>"全车机械打蜡（含蜡含工时费）","new_server"=>92,"card_title"=>'全车普通机械打蜡（含蜡）',"voucher_id"=>100),
            array("old_server"=>41,"old_title"=>"全车机械精致打蜡（含蜡含工费）","new_server"=>94,"card_title"=>'全车精品机械打蜡（含蜡）',"voucher_id"=>102),
            array("old_server"=>103,"old_title"=>"普通全车漆面镀膜（含抛光、去除氧化层）","new_server"=>107,"card_title"=>'全车漆面普通镀膜',"voucher_id"=>115),
            array("old_server"=>95,"old_title"=>"局部柏油去除","new_server"=>103,"card_title"=>'局部柏油去除服务'),
            array("old_server"=>93,"old_title"=>"全车手工极致打蜡（含蜡含手工费）","new_server"=>95,"card_title"=>'全车极致手工打蜡（含蜡）'),
            array("old_server"=>96,"old_title"=>"局部抛光","new_server"=>97,"card_title"=>'漆面局部抛光处理（单件）'),
            array("old_server"=>40,"old_title"=>"全车机械极致打蜡（含蜡含工费）","new_server"=>96,"card_title"=>'全车极致机械打蜡（含蜡）',"voucher_id"=>104),
            array("old_server"=>36,"old_title"=>"全车手工打蜡（手工费）","new_server"=>96,"card_title"=>'全车极致机械打蜡（含蜡）'),
            array("old_server"=>101,"old_title"=>"全车水渍去除","new_server"=>101,"card_title"=>'全车漆面水渍去除服务'),
            array("old_server"=>105,"old_title"=>"全车底盘装甲施工（含2瓶装甲液）","new_server"=>246,"card_title"=>'底盘装甲施工（含2瓶装甲液）'),
            array("old_server"=>44,"old_title"=>"精品全车漆面封釉（不含抛光）","new_server"=>105,"card_title"=>'全车漆面普通封釉',"voucher_id"=>113),
            array("old_server"=>106,"old_title"=>"##侧挡玻璃膜(单门)","new_server"=>166,"card_title"=>'侧挡玻璃太阳膜工时费（高端单块）'),
            array("old_server"=>43,"old_title"=>"全车漆面封釉（不含抛光）","new_server"=>106,"card_title"=>'全车漆面高端封釉',"voucher_id"=>114),
            array("old_server"=>45,"old_title"=>"普通全车漆面镀膜（不含抛光）","new_server"=>107,"card_title"=>'全车漆面普通镀膜'),
            array("old_server"=>46,"old_title"=>"精品全车漆面镀膜（不含抛光）","new_server"=>108,"card_title"=>'全车漆面高端镀膜'),
            array("old_server"=>47,"old_title"=>"全车中度漆面镀晶（含抛光、去除氧化层）","new_server"=>109,"card_title"=>' 全车漆面普通镀晶（不含氧化层单次）'),
            array("old_server"=>48,"old_title"=>"精品全车漆面镀晶（含抛光、去除氧化层）","new_server"=>109,"card_title"=>'全车漆面普通镀晶（不含氧化层单次）'),
            array("old_server"=>48,"old_title"=>"全车手工打蜡（手工费）","new_server"=>111,"card_title"=>'全车漆面普通镀晶（3次含抛光）',"voucher_id"=>119),
            array("old_server"=>59,"old_title"=>"全车门板皮质（PU）改装","new_server"=>124,"card_title"=>'门板仿真皮改装（单门）'),
            array("old_server"=>61,"old_title"=>"原车顶棚布翻新更换","new_server"=>136,"card_title"=>'原车顶棚布翻新更换'),
            array("old_server"=>114,"old_title"=>"##玻璃太阳膜手工费","new_server"=>152,"card_title"=>'全车太阳膜工时费（普通）'),
            array("old_server"=>165,"old_title"=>"汽车玻璃水加注","new_server"=>153,"card_title"=>'玻璃水加满'),
            array("old_server"=>8,"old_title"=>"汽车仪表台清洁服务","new_server"=>154,"card_title"=>'仪表台普通清洁养护'),
            array("old_server"=>5,"old_title"=>"汽车内饰门板普通清洁（单门）","new_server"=>156,"card_title"=>'门板普通清洁+养护（4门）'),
            array("old_server"=>7,"old_title"=>"汽车门板上光养护（单门）","new_server"=>156,"card_title"=>'门板普通清洁+养护（4门）'),
            array("old_server"=>158,"old_title"=>"##全车车身改色膜工时费","new_server"=>193,"card_title"=>'全车改色工时费'),
            array("old_server"=>157,"old_title"=>"空调氟利昂加注（高端）","new_server"=>197,"card_title"=>'空调氟利昂加注（高端）/瓶'),
            array("old_server"=>149,"old_title"=>"##空调氟利昂加注（工时费）","new_server"=>199,"card_title"=>'空调氟利昂加注工时费/次'),
            array("old_server"=>122,"old_title"=>"拆卸清洗喷油嘴（免工时费）","new_server"=>200,"card_title"=>' 普通清洗喷油嘴'),
            array("old_server"=>130,"old_title"=>"维护清洗喷油嘴（免工时费）","new_server"=>200,"card_title"=>'普通清洗喷油嘴'),
            array("old_server"=>131,"old_title"=>"高端深度清洗喷油嘴（免工时费）","new_server"=>201,"card_title"=>'高端清洗喷油嘴',"voucher_id"=>209),
            array("old_server"=>120,"old_title"=>"普通清洗节气门（含工时费）","new_server"=>202,"card_title"=>'清洗节气门工时费'),
            array("old_server"=>133,"old_title"=>"深度清洗进气管道（含工时费）","new_server"=>204,"card_title"=>'高端清洗进气管道',"voucher_id"=>212),
            array("old_server"=>74,"old_title"=>"节气门+喷油嘴+进气管道+三元催化清洗套餐","new_server"=>206,"card_title"=>'高端清洗三元催化'),
            array("old_server"=>75,"old_title"=>"节气门+喷油嘴+进气管道+三元催化高端清洗套餐","new_server"=>206,"card_title"=>'高端清洗三元催化',"voucher_id"=>214),
            array("old_server"=>98,"old_title"=>"深度清洗三元催化（免工时费）","new_server"=>206,"card_title"=>'高端清洗三元催化'),
            array("old_server"=>127,"old_title"=>"更换润滑油工时费","new_server"=>214,"card_title"=>' 更换润滑油工时费'),
            array("old_server"=>143,"old_title"=>"##更换刹车油工时费","new_server"=>220,"card_title"=>'更换刹车制动液'),
            array("old_server"=>162,"old_title"=>"补胎 轮胎修补（防爆胎单次价）","new_server"=>239,"card_title"=>'防爆胎补胎服务'),
            array("old_server"=>76,"old_title"=>"精准四轮定位检测服务","new_server"=>241,"card_title"=>'精准四轮定位服务',"voucher_id"=>249),
            array("old_server"=>164,"old_title"=>"##更换轮胎","new_server"=>242,"card_title"=>'单轮更换轮胎工时费'),
            array("old_server"=>159,"old_title"=>"空调氟利昂加注（普通）","new_server"=>252,"card_title"=>'空调氟利昂加注（普通）/瓶'),
            array("old_server"=>186,"old_title"=>"倒胎","new_server"=>255,"card_title"=>'两轮倒胎服务'),
        );
    }

    function changeStart()
    {
        $list = $this->changeServerArray();
        foreach($list as $k=>$v){
            Db::table("order_server")->where("id<=46069 and server_id = {$v['old_server']} and custommark =1")
                ->update(array("server_id"=>$v['new_server'],"service_title"=>$v['card_title']));
        }
        var_dump(123);
    }

    function changeAgain()
    {
        $list = $this->changeServerArray();
        array_push($list,array("old_title"=>"**车辆普通清洗","card_title"=>'汽车普洗服务'));
        array_push($list,array("old_title"=>"爱车精洗服务","card_title"=>'汽车快捷精洗服务'));
        array_push($list,array("old_title"=>"普通精洗服务","card_title"=>'汽车普洗服务'));
        foreach($list as $k=>$v){
            Db::table("log_service")->where("date(log_service_create)<='2021-01-13' and log_service_source = '".$v['old_title']."'")
                ->update(array("log_service_source"=>$v['card_title']));
        }
        var_dump(456);
    }

    function serviceDiscount()
    {
        $biz_id = 169 ;
        # 查询这个门店关联的服务
        $list = Db::table("service_car_mediscount")->field("service_id")->where(array("biz_id"=>$biz_id))->group("service_id")->select();
        foreach($list as $k=>$v){
            # 查询这个服务的平台价格
            $price = Db::table("service_car_mediscount")->where(array("service_id"=>$v['service_id'],"biz_id"=>0))->select();
            if(!empty($price)){
                foreach($price as $pk=>$pv){
                    Db::table("service_car_mediscount")->where(array("service_id"=>$v['service_id'],"biz_id"=>$biz_id,"car_level_id"=>$pv['car_level_id'],
                        "service_type"=>$pv['service_type']))->update(array("discount"=>$pv['discount']));
                }
            }
        }
        return true;
    }

    function demo(){
        $order_number = '1611626428303100003';
        $orderService = new orderService();
        $list = $orderService->formatEvaluationOrders($order_number);
        dump($list);
    }

    function serviceBizMapping()
    {
        $list = Db::table("service")->field("id")->where("id=255")->select();
        $_array=array();
        foreach($list as $k=>$v){
            $is_repeat=Db::table("service_car_mediscount")->where(array("service_id"=>$v['id'],"biz_id"=>1))->find();
            if(empty($is_repeat)){
                $discount=Db::table("service_car_mediscount")->where(array("service_id"=>$v['id'],"biz_id"=>0))->select();
                if(!empty($discount)){
                    foreach($discount as $dk=>$dv){
                        array_push($_array,array("service_id"=>$v['id'],"car_level_id"=>$dv['car_level_id'],
                            "discount"=>$dv['discount'],"service_type"=>$dv['service_type'],"biz_id"=>1));
                    }
                }
            }
        }
        if(!empty($_array)){
            Db::table("service_car_mediscount")->insertAll($_array);
            var_dump(true);
        }
    }
}





















