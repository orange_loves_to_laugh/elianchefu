<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/30
 * Time: 16:01
 */

namespace app\api\controller;


use think\App;
use think\Controller;
use think\Db;
use think\Log;
use Yansongda\Pay\Pay;

class PayMent extends Controller
{
    function __construct(App $app = null)
    {
        parent::__construct($app);
        header('Access-Control-Allow-Origin:*');  //支持全域名访问，不安全，部署后需要固定限制为客户端网址
        header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); //支持的http 动作
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Authorization , Access-Control-Request-Headers,token,applymark');  //响应头 请按照自己需求添加。
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }
    }

    protected $wx_config = [
        'appid' => 'wxc07909a6507b51e3', // APP APPID
        'app_id' => 'wx281132202d144816', // 公众号 APPID
        //'miniapp_id' => 'wxb3fxxxxxxxxxxx', // 小程序 APPID
        'mch_id' => '1573261001',
        'key' => 'youpilive13236999488youpilive488',
        'notify_url' => 'http://back.ecarfu.com/api/PayMent/notify',
        'cert_client' => ROOT_PATH . '../extend/cert/apiclient_cert.pem', // optional，退款等情况时用到
        'cert_key' => ROOT_PATH . '../extend/cert/apiclient_key.pem',// optional，退款等情况时用到
        'log' => [ // optional
            'file' => 'runtime/logs/wechat.log',
            'level' => 'info', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode' => 'dev', // optional, dev/hk;当为 `hk` 时，为香港 gateway。
    ];

    public function wxPayAPP($data)
    {
        $pay = Pay::wechat($this->wx_config)->app($data);
        return $pay;
    }

    public function wxPayMp($data)
    {
        $pay = Pay::wechat($this->wx_config)->mp($data);
        return $pay;
    }

    public function wxRefund($data)
    {
        $pay = Pay::wechat($this->wx_config)->refund($data);
        return $pay;
    }

    public function notify()
    {
        $pay = Pay::wechat($this->wx_config);
        try {
            $data = $pay->verify(); // 是的，验签就这么简单！
        } catch (\Exception $e) {
            // $e->getMessage();
        }
        return $pay->success()->send();// laravel 框架中请直接 `return $pay->success()`
    }

    /**
     * @param $out_trade_no
     * @return \Yansongda\Supports\Collection
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     * @context 查询订单状态
     */
    public function orderStatusQuery($out_trade_no, $type = 1)
    {
        if ($type == 1) {
            $result = Pay::wechat($this->wx_config)->find(array("out_trade_no" => $out_trade_no, "type" => "app"));
        } else {
            $result = Pay::alipay($this->ali_config)->find(array("out_trade_no" => $out_trade_no, "type" => "app"));
        }
        return $result;
    }
    public function findTest($out_trade_no, $type = 1,$mark='app')
    {
        if ($type == 1) {
            if($mark=='transfer'){
                $result = Pay::wechat($this->wx_config)->find(array("partner_trade_no" => $out_trade_no), $mark);
            }else {
                $result = Pay::wechat($this->wx_config)->find(array("out_trade_no" => $out_trade_no), $mark);
            }
        } else {
            $result = Pay::alipay($this->ali_config)->find(array("partner_trade_no" => $out_trade_no), $mark);
        }
        return $result;
    }

    protected $ali_config = [
        'app_id' => '2021002114641063',
        'notify_url' => 'http://back.ecarfu.com/api/PayMent/ali_notify.php',
        'return_url' => 'http://back.ecarfu.com/api/PayMent/ali_return.php',
        'ali_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAojSPnrmQl1GIIvNrRalo6QsnAYU3Wff3grKnHMoi0xZ6ftKMROB6tUY91AVa85CHld7MkrwZHlvDSjUrtzIfBnevpOSCUKIVgSEf4dO3WMbSlLDU7Pu5xz9gCM7WA6tbtvmszgwmk6piZDenSZcx08e4t1QIeBP5YxGjHM4lk8hPFgdSsP1frY7zVGw+fZxKl+QOcW6MtX89NqQOVe84IgSej4cWhTuSwBdKA5SoHLW2vuauvTwAF+qpETkUO4MzzPqYPTUe05Ap6NDbtVhctFerdbRudjfnYZe7Bgj4ukSz0Jc7eJ/BH/6jTZXYMQW7Ck7DwVShapp4pJoaLag11wIDAQAB',
        // 加密方式： **RSA2**
        'private_key' => 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHDBAl+Fg3wIa741xCYZR79mMZCqwYCJ1HjwO4XV0AsXF4X3RcT/q+IhJymJmJ6kvta7ilClctQRWoSnyKUy5gd+foi0w3xZe7dfAswRzN6QdmcimUeOVDYEjG8/oS57yJrLSaGdROoQRqde8O+rV6Hzt1efL+UIM90KmJ99+Pa4I3Emv7atbyulvsSVjrN/AOuAL2WGOS7rTL2Q88J72sgUIOJ6bk9TFYyd5ydedQurDagv1DXTohCAJtFIQDnOZn/1QX+vRX4d8uePmhjNQ6v02U3jp6/z+wp68718WzMRuO/FMtbxLu93Bo8aH6WP5QkNal9zuP4RfxDALtacLvAgMBAAECggEAJSKwBJV/x+8IdBdoCA5yiGogdPSTxOweg9I/zG7GwjFYzS9p4yscycpLjMvBoYRv4/y79zalqxDAfKRg9eGs/lyAjNcoQiOAOn43b/IbcKMwOUHFVij0BQz6pvuvxUUs3La2/yUgggbybvZPwoitwPoAutskpaPZyHecmdv//p6ogW6fLqWyAkItNcDt/6De3A8TR4t209MfXvKT0NHFucYMPQF2rFl973bb9STPncpzrcU0fRW4tlpOuuLoOlMX03rC9XV3hsmXP9vtwM2ydB9hvJy+5Uot72Cd92hnXE/FLvPqbEzSw8f9NkAbAK0jjGtr1wkCnxNtqSLx3g8h6QKBgQD6FqTbz4/RtqRqvNzlyYhNQMstvCYSAPFQtOv1Ikk1m6yCScdm1NaJE3ds7Lu9Vu709+BFCz3usOljff2IdovinKH2OjahLQSHLdShC+qjQ8NPFsEzdGR9R3J2zJHNdqYpU9V+WqLgoPwSwgUDslDM4ffNdwYmFXiQSgv8U+T5awKBgQDLwI21v4H7P6QjZO+17kRSfZG6T1k7tKm4t26wC6P6v6l4tHLu2ejiahz1+LLXSCmgtD22ebGA/nNk5EsyodRbPMUElcvs54yBEm9XSsCg8Op+VfITEGFzDAviEWICcFwZY7ndrOVh91PjhRqctzS40lf1GE/KVR41RfbQNF7pjQKBgQDT+HVrShZgbioVvlaM/bBlqlGMRjkOcXYYLgEBGTpsEbK6C8bTRftA3BsRW+YUqaaIQ8D3RHwN4C0xgwxtqe0O+ivdA6JiIkIAQbxDdFJAZ6MXWfYPosu50+EVCHdcawCTbtc57aC4PDrruz98zprlzXG32PaBORmjUt+t0330WQKBgBsNFINlyp8N4v/j/cNcv+tnyno+4K0Bnmrsx6BN21aGLbzd0EiP29B7oB60ByJ37Tbt1yer3nouzik9+hHd1HDMj6e9L5bjYw8b4HZpcUus0KU3Se1oBlgc6FL6VdXXDxM730hkFQw9pwCVAmR+GlzTaGQc9zL4vAr8n/kiittdAoGBALqZbC4st0K2H865QLTk/ZHWa7zl3a+iuTOo8/HOugpURULfdRP4sY3QyJpwwq1MMLq84k9UP1HjssmNr9Jpr1MQyf3yacXNzqv2j0aDppJW+VOPRugZ58W/B2BRfowRhLAKDyNlPoUQS3seV6kZjHpwftMKyBsBqbAEU4iAUrQI',
        //'app_cert_path'=>ROOT_PATH.'../extend/cert/appCertPublicKey_2021002114641063.crt', // 应用证书
        //'alipay_root_cert_path'=>ROOT_PATH.'../extend/cert/alipayRootCert.crt',// 支付宝根证书
        //'alipay_cert_path'=>ROOT_PATH.'../extend/cert/alipayCertPublicKey_RSA2.crt', // 支付宝公钥证书
        'log' => [ // optional
            'file' => 'runtime/logs/alipay.log',
            'level' => 'debug', // 建议生产环境等级调整为 info，开发环境为 debug
            'type' => 'single', // optional, 可选 daily.
            'max_file' => 30, // optional, 当 type 为 daily 时有效，默认 30 天
        ],
        'http' => [ // optional
            'timeout' => 5.0,
            'connect_timeout' => 5.0,
            // 更多配置项请参考 [Guzzle](https://guzzle-cn.readthedocs.io/zh_CN/latest/request-options.html)
        ],
        //'mode' => 'dev', // optional,设置此参数，将进入沙箱模式
    ];

    public function aliPayAPP($order)
    {
        $alipay = Pay::alipay($this->ali_config)->app($order);
        return $alipay;
    }

    public function aliRefund($order)
    {
        $alipay = Pay::alipay($this->ali_config)->refund($order);
        return $alipay;
    }

    public function ali_return()
    {
        $data = Pay::alipay($this->ali_config)->verify(); // 是的，验签就这么简单！

        // 订单号：$data->out_trade_no
        // 支付宝交易号：$data->trade_no
        // 订单总金额：$data->total_amount
    }

    public function ali_notify()
    {
        $alipay = Pay::alipay($this->ali_config);

        try {
            $data = $alipay->verify(); // 是的，验签就这么简单！

            // 请自行对 trade_status 进行判断及其它逻辑进行判断，在支付宝的业务通知中，只有交易通知状态为 TRADE_SUCCESS 或 TRADE_FINISHED 时，支付宝才会认定为买家付款成功。
            // 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号；
            // 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额）；
            // 3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）；
            // 4、验证app_id是否为该商户本身。
            // 5、其它业务逻辑情况

            \Yansongda\Pay\Log::debug('Alipay notify', $data->all());
        } catch (\Exception $e) {
            // $e->getMessage();
        }

        return $alipay->success()->send();// laravel 框架中请直接 `return $alipay->success()`
    }

    /**
     * @param $order
     * @return array
     * @content 企业转账
     */
    function transfers($order)
    {
        $result = Pay::wechat($this->wx_config)->transfer($order);
        if ($result->return_code == 'SUCCESS' and $result->result_code == 'SUCCESS') {
            return array('status' => true, 'isJump'=>false,'code' => 'SUCCESS', 'msg' => '转账成功','res'=>$result);
        } else {
            return array('status' => true,'isJump'=>false, 'code' => 'FAIL','res'=>$result, 'msg' => config('errCode.' . $result->return_code));
        }
    }


    function toutiaoNotify(){
        $resInfo = input();
        if(!empty($resInfo))
        Db::table('aaa')->insert(array('info'=>json_encode($resInfo),'type'=>'toutiao_wx'));
        echo json_encode(array('err_no'=>0,'err_tips'=>'success'));
    }
}
