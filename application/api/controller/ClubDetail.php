<?php


namespace app\api\controller;


use app\api\ApiService\AdvanceService;
use app\api\ApiService\IntegralService;
use app\service\TimeTask;
use Redis\Redis;
use think\Controller;
use think\Db;

class ClubDetail extends Controller
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取俱乐部轮播图
     */
    function getsClubBanner()
    {
        $info = Db::table("application")->field("application_image")->where(array("application_mark" => 'clubbanner'))->find();
        if (!empty($info)) {
            $list = explode(',', $info['application_image']);
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }
    /*新*/
    function getsClubBannerInfo()
    {
        $info=AdvanceService::getsAdvance("clubbanner");
        if (!empty($info)) {
            $list = $info['info'];
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 俱乐部列表
     */
    function clubList()
    {
        $page = input("post.page");
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $list = Db::table("club")->order("create_time desc")->limit($page_num, 10)->select();
        $flag = count($list) == 10 ? true : false;
        if (!empty($list)) {
            return array("status" => true, "list" => $list, "flag" => $flag);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 俱乐部详情
     */
    function clubDetail()
    {
        $id = input("post.id");
        $info = Db::table("club")->where(array("id" => $id))->find();
        # 增加查看数量
        Db::table("club")->where(array("id" => $id))->setInc("club_check", 1);
        $member_id = input("post.member_id");
        if (!empty($member_id)) {
            # 查看文章加积分
            $redis = new Redis();
            $mark = 'look_article_integral';
            $isShare = $redis->hGet('isShare', $mark . $member_id);
            if (empty($isShare)) {
                $integral_num = IntegralService::integralSetItem($mark);
                IntegralService::addMemberIntegral($member_id, integralSource($mark), $integral_num, 1);
                $memberInfo = $redis->hGetJson("memberInfo", $member_id);
                $memberInfo['member_integral'] += intval($integral_num);
                $redis->hSetJson("memberInfo", $member_id, $memberInfo);
                $redis->hSet('isShare', $mark . $member_id, 'SUCCESS', strtotime(date('Y-m-d 23:59:59')) - time());
                # 完成任务 '7'=> '查看文章',
                TimeTask::finishTask($member_id, 7);
            }
        }
        # 查询分享文章积分是多少
        $club_integral = IntegralService::integralSetItem("is_share_club");
        # 分享设置
        $shareClub = config('share.shareClub');
//        $shareClub['jumpUrl'] .= "?newsid=" . $id . '&shareMemberId=' . $this->MemberId;
        $shareClub['jumpUrl'] .= '?params=' . urlencode(json_encode(array('newsid' => $id, 'is_share' => 'share')));
        return array("status" => true, "info" => $info, "club_integral" => $club_integral, 'shareClub' => $shareClub);
    }
}
