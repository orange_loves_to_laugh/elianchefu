<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/13
 * Time: 10:04
 */

namespace app\api\controller;


use app\api\ApiService\MemberService;
use app\api\ApiService\orderService;
use app\service\AgentTask;
use app\service\TimeTask;
use think\App;
use think\Controller;
use think\facade\Request;

class Common extends Controller
{
    protected $MemberInfo;
    protected $MemberPhone;
    protected $MemberUnionId;
    protected $MemberLevel;
    protected $MemberId;

    function __construct(App $app = null)
    {
        parent::__construct($app);
        header('Access-Control-Allow-Origin:*');  //支持全域名访问，不安全，部署后需要固定限制为客户端网址
        header('Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE'); //支持的http 动作
        header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Authorization , Access-Control-Request-Headers,token,applymark');  //响应头 请按照自己需求添加。
        if (strtoupper($_SERVER['REQUEST_METHOD']) == 'OPTIONS') {
            exit;
        }
        $token = Request::header("token");
        $vali = $this->tokenVerification($token);
        if (!$vali['status']) {
            throw new \BaseException(['msg' => $vali['msg'], 'status' => false, 'debug' => false]);
        }
        # 预约订单超时提醒(只提醒一次)
        TimeTask::reservationExpired();
        # 会员到期提醒
        TimeTask::vipExpired();
        # 卡券过期提醒
        TimeTask::couponExpired();
        //\app\api\ApiService\orderService::ordersOverTime();
        orderService::ordersOverTime();
        // 支付状态处理
        TimeTask::payRedis();
        // 会员额外赠送积分
        TimeTask::bonusPoints();
        // 修改会员等级
//        TimeTask::clearLevel();
        // 完成代理任务奖励
        AgentTask::dayTaskCommission();
        // 未完成取消身份
        AgentTask::agentTaskFail();
        // 短信发送
        TimeTask::smsSend();
    }

    /**
     * @param $token
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context token 解析
     */
    function tokenVerification($token)
    {
        if (!empty($token)) {
            $MemberService = new MemberService();
            $member_id = $MemberService->authCode($token);
//            if($member_id==14175 or $member_id==  14285){
//                $member_id = 13347;
//            }
            if (!empty($member_id)) {
                $memberInfo = $MemberService->MemberInfoCache($member_id);
                $this->MemberInfo = $memberInfo;
                $this->MemberPhone = $memberInfo['member_phone'];
                $this->MemberUnionId = $memberInfo['member_unionid'];
                $this->MemberLevel = $memberInfo['member_level_id'];
                $this->MemberId = $member_id;
                return array("status" => true);
            } else {
                return array("status" => false, "msg" => "token解析失败");
            }
        } else {
            return array("status" => false, "msg" => "token验证失败");
        }
    }
}
