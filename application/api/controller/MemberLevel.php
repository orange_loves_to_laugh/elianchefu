<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/16
 * Time: 13:53
 */

namespace app\api\controller;


use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\VoucherService;
use Redis\Redis;
use think\Db;

class MemberLevel extends Common
{

    function newMemberInfo()
    {
        # 查询会员卡信息

    }


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员套餐
     */
    function memberLevelSetmeal()
    {
        $level = input("post.level");
        return array("status" => true, "list" => LevelService::levelPackage($level));
    }

    /**\
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 会员详情
     */
    function memberLevelDetail()
    {
        $level = input("post.level");
        $info = LevelService::memberLevelInfo($level);
        $info['level_content'] = LevelService::levelContentFormat($info['level_content']);
        $privilege = LevelService::getLevelPrivilege($level);
        $setmeal = LevelService::levelPackage($level);
        $info['price'] = array_sum(array_column($setmeal, 'price'));
        # 能否办理当前等级会员
        $canApply = $level <= $this->MemberLevel ? false : true;
        # 用户是否设置了支付密码
        $paypass = empty($this->MemberInfo['member_paypass']) ? true : false;
        # 获取分享积分
        $share_integral = IntegralService::integralSetItem("share_register_integra");
        return array("status" => true, "info" => $info, "privilege" => $privilege, "setmeal" => $setmeal, "paypass" => $paypass, "canApply" => $canApply, "share_integral" => $share_integral, 'shareInfo' => config('share.shareVip'));
    }

    function applyMemberCanUseVoucher()
    {
        $price = input("post.price");
        $VoucherService = new VoucherService();
        $voucher = $VoucherService->memberVoucher($this->MemberId, array("voucher_type" => 17, "price" => $price));
        if (!empty($voucher['list'])) {
            $nowDate = date("Y-m-d H:i:s");
            foreach ($voucher['list'] as $k => $v) {
                if ($v['create'] < $nowDate) {
                    # 已过期
                    unset($voucher['list'][$k]);
                    $voucher['num']--;
                    continue;
                }
                $Desc = $VoucherService->voucherTypeDesc($v);
                $voucher['list'][$k]['TypeTitle'] = $Desc['TypeTitle'];
                $voucher['list'][$k]['voucherDesc'] = $Desc['voucherDesc'];
                $voucher['list'][$k]['voucher_source_title'] = $Desc['voucherSource'];
            }
        }
        return array("status" => true, "list" => array_values($voucher['list']), "num" => $voucher['num']);
    }


    /**
     * @return array`
     * @context 办理会员成功
     */
    function applyMemberSuccess()
    {
        $level = input("post.level");
        $balance = input("post.balance");
        $setmeal_id = input("post.setmeal_id");
        $pay_type = input("post.pay_type");
        $order_number = input("post.order_number");
        $_redis = new Redis();
        $recommend = $_redis->hGetJson("relation_member", $this->MemberId);
        $assign_id = input('post.assign_id');
        $employee_id = input('post.employee_id');
        $shareMemberId = input('post.shareMemberId');
        # 来源  calling=>外呼平台  employeeSal=>平台员工
        $formMark = input('post.formMark');
        $voucherInfo = input('post.voucherInfo');
        /*if(empty($recommend)){
            $employee_id ='';
            $assign_id ='';
            $shareMemberId='';
        }else{
            $employee_id =$recommend["employeeId"];
            $assign_id = $recommend["assignId"];
            $shareMemberId = $recommend["shareMemberId"];
        }*/
        if (empty($assign_id) and empty($employee_id) and empty($shareMemberId)) {
            if (!empty($recommend)) {
                //{"share_member_id":"","assignId":"","employeeId":"113","member_id":1733,"formMark":"employeeSal"}
                if (array_key_exists('assignId', $recommend) and !empty($recommend['assignId'])) {
                    $assign_id = $recommend['assignId'];
                }
                if (array_key_exists('employeeId', $recommend) and !empty($recommend['employeeId'])) {
                    $employee_id = $recommend['employeeId'];
                }
                if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                    $shareMemberId = $recommend['share_member_id'];
                }
                if (array_key_exists('formMark', $recommend) and !empty($recommend['formMark'])) {
                    $formMark = $recommend['formMark'];
                }
            }
        }
//        if($this->MemberId==1733){
//            Db::table('aaa')->insert(array('info'=>json_encode($recommend),'type'=>'recommend'));
//            return array("status" => true);
//        }
//        if($this->MemberId == 9900){
//            Db::table('aaa')->insertGetId(array('info'=>json_encode($recommend)));
//            $a = json_encode(array("consump_price" => $balance, "pay_type" => $pay_type, "consump_type" => 1,
//                "order_number" => $order_number, "biz_id" => 1, 'employee_id' => $employee_id, 'assign_id' => $assign_id, 'shareMemberId' => $shareMemberId, 'formMark' => $formMark));
//            Db::table('aaa')->insertGetId(array('info'=>json_encode($a),'type'=>'a'));
//
////            return array("status" => true);
//
//        }

        # 查询用户信息
        $memberInfo = Db::table('member_mapping')->field('member_level_id,member_expiration')->where(array('member_id' => $this->MemberId))->find();
        if ($memberInfo['member_level_id'] == $level and $memberInfo['member_expiration'] > date('Y-m-d')) {
            return array("status" => true);
        }
        # 清除支付状态缓存
//        $_redis->hDel('payRedis',$order_number);
        LevelService::applyMember($this->MemberId, $level, $balance, $setmeal_id, array("consump_price" => $balance, "pay_type" => $pay_type, "consump_type" => 1,
            "order_number" => $order_number, "biz_id" => 1, 'employee_id' => $employee_id, 'assign_id' => $assign_id, 'shareMemberId' => $shareMemberId, 'formMark' => $formMark), $voucherInfo);
        return array("status" => true);
    }

    /**
     * @return array
     * @context 验证用户支付密码
     */
    function validatePassWord()
    {
        $password = md5(input("post.password"));
        if ($password == $this->MemberInfo['member_paypass']) {
            return array("status" => true);
        } else {
            return array("status" => false, "msg" => "密码错误");
        }
    }

    /**
     * @return array
     * @context 生成办理会员订单号
     */
    function getApplyMemberOrderNumber()
    {
        $type = input("post.type");
        $uplevel = input("post.uplevel");
        $order_number = LevelService::makeMemberLevelOrders($this->MemberId, $uplevel, $type, $this->MemberLevel);
        # 预约订单支付状态缓存
        $redis = new Redis();
        if ($type == 1) {
            # 会员办理
            $typeInfo = 'memberApply';
        } elseif ($type == 2) {
            # 会员升级
            $typeInfo = 'memberUpgrade';
        } else {
            # 会员充值
            $typeInfo = 'memberRecharge';
        }
        $payArr = array(
            'info' => array(
                'member_id' => $this->MemberId,
                'now_level' => $this->MemberLevel,
                'up_level' => $uplevel,
                'post_params' => input('post.pay_info')
            ),
            'pay_info'=>json_encode(input('post.pay_info')),
            'pay_time' => date('Y-m-d H:i:s'),
            'type' => $typeInfo
        );
        Db::table('aaa')->insert(array(
            'info' => json_encode($payArr),
            'type' => $order_number . '-payRedis-' . date('Y-m-d H:i:s') . '-orderNumber'
        ));
        $redis->hSetJson('payRedis', $order_number, $payArr);
        return array("status" => true, "order_number" => $order_number);
    }

    /**
     * @return bool[]
     * @context 是否弹出绑定车辆弹窗
     */
    function firstApplyMember()
    {
        # 查询办卡记录
        $num = Db::table("log_handlecard")->where(array("member_id" => $this->MemberId))->count();
        if ($num > 1) {
            return array("status" => false);
        } else {
            # 判断是否绑定了车辆
            $car_num = Db::table("member_car")->where(array("member_id" => $this->MemberId))->count();
            if ($car_num > 0) {
                return array("status" => false);
            } else {
                return array("status" => true);
            }
        }
    }

    /**
     * @return array
     * @context 获取会员协议
     */
    function getsLevelProtocol()
    {
        return array("status" => true, "protocol" => LevelService::levelProtocol());
    }

    /**
     * @return bool[]
     * @context 余额不足弹窗提醒
     */
    function balanceRunLow()
    {
        if ($this->MemberLevel > 1 and $this->MemberInfo['member_balance'] < 15) {
            return array("status" => true);//{$this->MemberInfo['member_balance']}
        } else {
            return array("status" => false);
        }
    }

}
