<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/20
 * Time: 15:30
 */

namespace app\api\controller;


use app\api\ApiService\MemberService;
use app\api\ApiService\PartnerService;
use app\api\ApiService\SubsidyService;
use app\service\IGeTuiService;
use Redis\Redis;
use think\Db;

class Partner extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 查询合伙人设置
     */
    function partnerGive()
    {
        # 合伙人赠送信息
        $partnerService = new PartnerService();
        $info = $partnerService->partnerGive();
        return $info;
    }

    /**
     * @return array
     * @content 订单编号
     */
    function getOrderNumber()
    {
        # 判断等级
        if ($this->MemberInfo['member_level_id'] < 2) {
            return array('status' => false, 'msg' => '只能体验卡以上用户可申请');
        }
        $orderNumber = date('YmdHis') . time() . $this->MemberId;
        return array('status' => true, 'order_number' => $orderNumber);
    }

    /**
     * @return int
     * @content 成为合伙人 返利
     */
    function rebate()
    {
        # 查询当前合伙人数量
        $num = Db::table('member_partner')->where(array('status' => 1))->count();
        $next_num = $num + 1;
        if ($next_num <= 5) {
            return 499;
        } elseif ($next_num >= 6 and $next_num <= 10) {
            return 399;
        } elseif ($next_num >= 11 and $next_num <= 15) {
            return 299;
        } elseif ($next_num >= 16 and $next_num <= 20) {
            return 199;
        } elseif ($next_num >= 21 and $next_num <= 30) {
            return 99;
        } else {
            return 0;
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 申请成为合伙人
     */
    function applyPartner()
    {
        $redis = new Redis();
        $isRunning = $redis->lock('applyPartner' . $this->MemberId);
        if ($isRunning) {
            # 接收上级id
            $pid = input('post.pid') ?? 0;
            /* $_redis = new Redis();
             $recommend = $_redis->hGetJson("relation_member", $this->MemberId);
             if (empty($pid)) {
                 if (!empty($recommend)) {
                     if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                         $pid = $recommend['share_member_id'];
                     }
                 }
             }*/
            # 上级为最后一次办理会员的推荐人
            $channel = Db::table('channel')->field('pid,action_type,channel')->where(array('member_id' => $this->MemberId))->order('id desc')->find();
            if (!empty($channel) and $channel['action_type'] == 2 and ($channel['channel'] == 3 or $channel['channel'] == 5) and $channel['pid'] != 0) {
                $pid = $channel['pid'];
            }
            # 接收支付方式 1 微信 2支付宝
            $payType = input('post.pay_type');
            # 查询赠送
            $partnerService = new PartnerService();
            $partnerSet = $partnerService->partnerGive()['info'];
            # 修改用户合伙人状态
            Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->update(array('member_type' => 2));
            # 平台返现
//        $rebate = $this->rebate();
            $rebate = 0;
            Db::table('member_partner')->insert(array(
                'member_id' => $this->MemberId,
                'partner_price' => $partnerSet['pay_price'],
                'partner_recommender_id' => $pid,
                'create_time' => date('Y-m-d H:i:s'),
                'phone' => $this->MemberInfo['member_phone'],
                'partner_balance' => $rebate
            ));
            if ($rebate > 0) {
                # 收益记录
                Db::table('log_consump')->insert(array(
                    'member_id' => $this->MemberId,
                    'consump_price' => $rebate,
                    'consump_pay_type' => 0,
                    'consump_time' => date('Y-m-d H:i:s'),
                    'log_consump_type' => 2,
                    'consump_type' => 1,
                    'income_type' => 18,
                    'biz_id' => 0
                ));
            }
            if (!empty($partnerSet['prise_info'])) {
                $memberVoucher = array();
                foreach ($partnerSet['prise_info'] as $k => $v) {
                    if ($v['type'] == 1 or $v['type'] == 2) {
                        # 1服务  2商品
                        for ($i = 0; $i < $v['num']; $i++) {
                            array_push($memberVoucher, array(
                                'voucher_id' => $v['id'],
                                'member_id' => $this->MemberId,
                                'create' => date('Y-m-d H:i:s', strtotime('+ ' . $v['card_time'] . ' days')),
                                'status' => 1,
                                'voucher_source' => 16,
                                'paytype' => $payType,
                                'redpacket_id' => 0,
                                'voucher_cost' => $v['detailInfo']['card_price'],
                                'card_title' => $v['detailInfo']['card_title'],
                                'card_time' => $v['card_time'],
                                'server_id' => $v['detailInfo']['server_id'],
                                'voucher_type' => $v['type'] == 2 ? 3 : 4,
                                'money_off' => 0,
                                'voucher_start' => 1
                            ));
                        }
                    } else {
                        # 赠送红包
                        # 查询红包详情
                        $redpacket = Db::table('redpacket_detail')->where(array('id' => $v['id']))->select();
                        if (!empty($redpacket)) {
                            for ($i = 0; $i < $v['num']; $i++) {
                                foreach ($redpacket as $rk => $rv) {
                                    array_push($memberVoucher, array(
                                        'voucher_id' => 0,
                                        'member_id' => $this->MemberId,
                                        'create' => date('Y-m-d H:i:s', strtotime('+ ' . $rv['voucher_over'] . ' days')),
                                        'status' => 1,
                                        'voucher_source' => 16,
                                        'paytype' => $payType,
                                        'redpacket_id' => $v['id'],
                                        'voucher_cost' => $rv['voucher_price'],
                                        'card_title' => $rv['voucher_title'],
                                        'card_time' => $rv['voucher_over'],
                                        'server_id' => $rv['voucher_id'],
                                        'voucher_type' => $rv['voucher_type'],
                                        'money_off' => $rv['money_off'],
                                        'voucher_start' => $rv['voucher_start']
                                    ));
                                }
                            }
                        }
                    }
                }
                if (!empty($memberVoucher)) {
                    Db::table('member_voucher')->insertAll($memberVoucher);
                }
            }
            # 消费记录
            Db::table('log_consump')->insert(array(
                'member_id' => $this->MemberId,
                'consump_price' => $partnerSet['pay_price'],
                'consump_pay_type' => 1,
                'consump_time' => date('Y-m-d H:i:s'),
                'log_consump_type' => 1,
                'consump_type' => 1,
                'income_type' => 13,
                'biz_id' => 0
            ));
            #赠送积分  900积分
            Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->setInc('member_integral', 900);
            # 积分记录
            Db::table('log_integral')->insert(array(
                'integral_number' => 900,
                'integral_source' => 23,
                'integral_type' => 1,
                'integral_time' => date('Y-m-d H:i:s'),
                'member_id' => $this->MemberId
            ));
            /* if (empty($pid)) {
                 # 查询合伙人关系
                 $pidInfo = Db::table('channel')->field('pid,channel')->where(array('member_id' => $this->MemberId))->order(array('id' => 'desc'))->find();
                 if (!empty($pidInfo) and $pidInfo['channel'] == 3) {
                     $pid = $pidInfo['pid'];
                 }
             }*/
            # 判断是否存在推荐人
            if (!empty($pid)) {
                # 判断推荐人是不是合伙人
                $memberType = Db::table('member_mapping')->field('member_type')->where(array('member_id' => $pid))->find()['member_type'];
                if ($memberType == 2) {
                    # 加推荐收益
                    Db::table('member_partner')->where(array('member_id' => $pid))->setInc('partner_balance', $partnerSet['prise_price']);
                    Db::table('member_partner')->where(array('member_id' => $pid))->setInc('partner_balance_total', $partnerSet['prise_price']);
                    $username = Db::table("member")->field("member_name")->where(array("id" => $pid))->find()['member_name'];
                    SubsidyService::addExpenses(array("type_id" => 14, "price" => $partnerSet['prise_price'], "username" => $username));
                    # 收益记录
                    Db::table('log_consump')->insert(array(
                        'member_id' => $pid,
                        'consump_price' => $partnerSet['prise_price'],
                        'consump_pay_type' => 5,
                        'consump_time' => date('Y-m-d H:i:s'),
                        'log_consump_type' => 2,
                        'consump_type' => 1,
                        'income_type' => 9,
                        'biz_id' => 0
                    ));
                    $getui = new IGeTuiService();
                    $partnerBalance = Db::table('member_partner')->field('partner_balance')->where(array('member_id' => $pid))->find()['partner_balance'];
                    $getui->MessageNotification($pid, 14, array('price' => $partnerSet['prise_price'], 'title' => '推荐推广员', 'balance' => $partnerBalance));
                }
            }
            # 更新用户信息
            $memberService = new MemberService();
            $memberService->changeMemberInfo($this->MemberId);
            return array('status' => true, 'msg' => '申请成功');
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 提现
     */
    function withdrawal()
    {
        $mark = input('post.mark');
//        if(empty($mark)){
//            return array('status' => false, 'msg' => '请前往微信公众号`E联车服智慧生活`,进行提现');
//        }
        # 接收提现金额
        $price = input('post.price');
        # 提现配置
        $partner_withdrawal = config('ratios.partner_withdrawal');
        if ($price < $partner_withdrawal['min']) {
            return array('status' => false, 'isJump' => false, 'msg' => '提现金额必须大于' . $partner_withdrawal['min'] . '元');
        }
        if ($price % $partner_withdrawal['divide'] != 0) {
            return array('status' => false, 'isJump' => false, 'msg' => '提现金额必须是' . $partner_withdrawal['divide'] . '的整数倍');
        }
        if ($price > $partner_withdrawal['max']) {
            return array('status' => false, 'isJump' => false, 'msg' => '单次提现金额上限时' . $partner_withdrawal['max'] . '元');
        }
        # 计算手续费
        $charge = $price * $partner_withdrawal['fee_ratio'];
        $amount = $price + $charge;
        $partnerInfo = Db::table('member_partner')->where(array('member_id' => $this->MemberId))->find();
        if ($partnerInfo['partner_balance'] < floatval($amount)) {
            return array('status' => false, 'isJump' => false, 'msg' => '可提现余额不足');
        }
        # 查询实名制认证状态
        if ($this->MemberInfo['member_status'] != 2) {
            return array('status' => false, 'isJump' => false, 'jumpType' => 'realName', 'msg' => '请先进行实名制');
        }
        # 查询用户openid
        $openid = $this->MemberInfo['member_openid'];
//        Db::table('aaa')->insert(array('info'=>$openid,'type'=>'redis'));
        if (empty($openid)) {
            $openid = Db::table('member_mapping')->field('member_openid')->where(array('member_id' => $this->MemberId))->find()['member_openid'];
//            Db::table('aaa')->insert(array('info'=>$openid));
            if (empty($openid)) {

                return array('status' => false, 'isJump' => true, 'jumpType' => 'weixin', 'msg' => '请前往微信公众号`E联车服智慧生活`,进行提现');
            }
        }
        if ($this->MemberId == 1733) {
//            return array('status' => false, 'msg' => '没毛病');
            $price = 1;
        }
        $order_number = $this->MemberId . date('YmdHis') . time();
        $redis = new Redis();
        $redis->hSet('tranfer'.$this->MemberId,'o',$order_number);
        $order = [
            'partner_trade_no' => $order_number,              //商户订单号
            'openid' => $openid,                        //收款人的openid
            'check_name' => 'NO_CHECK',            //NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名
            // 're_user_name'=>'张三',              //check_name为 FORCE_CHECK 校验实名的时候必须提交
            'amount' => $price * 100,                       //企业付款金额，单位为分
            'desc' => '帐户提现',                  //付款说明
        ];
        $payment = new PayMent();
        $res = $payment->transfers($order);
        $redis->hSet('tranfer'.$this->MemberId,'r',json_encode($res));
        if ($res['status'] and $res['code']=='SUCCESS') {
            # 扣除余额
            Db::table('member_partner')->where(array('member_id' => $this->MemberId))->setDec('partner_balance', $amount);
            # 提现记录
            Db::table('log_deposit')->insert(array(
                'create_time' => date('Y-m-d H:i:s'),
                'price' => $price,
                'account_time' => date('Y-m-d H:i:s'),
                'username' => $this->MemberInfo['nickname'],
                'phone' => $this->MemberInfo['member_phone'],
                'member_id' => $this->MemberId,
                'account_name' => $this->MemberInfo['member_name'] ? $this->MemberInfo['member_name'] : $this->MemberInfo['nickname'],
                'deposit_status' => 3,
                'deposit_type' => 1,
                'source_type' => 2
            ));
        }
        return $res;
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 爱车团
     */
    function partnerTeam()
    {
        # 查询我推荐得会员
        $pageNum = input('post.page_num') ?? 0;
        $size = input('post.size') ?? 10;
        $limit = $pageNum * $size;
        # 手机号搜索
        $phone = input('post.phone');
        $_where = "";
        if (!empty($phone)) {
            $_where = "member_phone = '" . $phone . "'";
        }
        $info = Db::table('channel c')
            ->field('nickname,headimage,member_type,member_balance,level_title')
            ->join('member m', 'm.id=c.member_id', 'left')
            ->join('member_mapping mm', 'mm.member_id=c.member_id', 'left')
            ->join('member_level ml', 'ml.id=mm.member_level_id', 'left')
            ->where(array('c.pid' => $this->MemberId, 'c.action_type' => 2, 'c.channel' => 3))
            ->where($_where)
            ->where("c.id = (select id from channel where member_id = c.member_id and action_type=2 order by id desc limit 1)")
//            ->limit($limit, $size)
            ->order('c.id desc')
            ->select();
        return array('status' => true, 'msg' => '查询成功', 'info' => $info);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 收益记录
     */
    function logPartner()
    {
        # 操作标识  1 今日  2 本周 3 本月  其他->指定日期   默认今日
        $mark = input("post.mark");
        # 搜索日期
        $start_time = input('post.start');
        $end_time = input('post.end');
        if ($mark == 1) {
            #今日
            $_where = "date_format(consump_time,'%Y-%m-%d') = '" . date('Y-m-d') . "'";
            $_time = date("Y-m-d") . '~' . date("Y-m-d");
        } elseif ($mark == 2) {
            #本周
            $begin = beginWeek(false);
            $end = endWeek(false);
            $_where = "date(consump_time) >= '" . $begin . "' and date(consump_time) <= '" . $end . "'";
            $_time = $begin . '~' . $end;
        } elseif ($mark == 3) {
            # 本月
            $_where = "date_format( consump_time,'%Y-%m') = '" . date('Y-m') . "'";
            $_time = date("Y-m-01") . '~' . date('Y-m-d');
        } else {
            # 指定日期搜索
            if (empty($mark)) {
                $start_time = date('Y-m-d');
                $end_time = date('Y-m-d');
            }
            $_where = "date(consump_time) >= '" . $start_time . "' and date(consump_time) <= '" . $end_time . "'";
            $_time = $start_time . '~' . $end_time;
        }
        $pageNum = input('post.page_num') ?? 0;
        $size = input('post.size') ?? 10;
        $limit = $pageNum * $size;
        $info = Db::table('log_consump')->field('income_type,if(income_type=10,"用户消费",if(income_type=8,"推荐用户","推荐推广员")) type,date_format(consump_time,"%Y-%m-%d %H:%i") time,consump_price price')
            ->where(array('member_id' => $this->MemberId))
            ->where("income_type in (8,9,10) and consump_price > 0")
            ->where($_where)
//            ->limit($limit, $size)
            ->order('consump_time desc')
            ->select();
        $count = 0;
        $member = 0;
        $consumption = 0;
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                $count += $v['price'];
                if ($v['income_type'] == 10) {
                    $consumption += $v['price'];
                } else {
                    $member += $v['price'];
                }
            }
        }
        return array('status' => true, 'logInfo' => $info, 'count' => $count, 'member' => $member, 'consumption' => $consumption, 'time' => $_time);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 提现记录
     */
    function log_deposit()
    {
        $pageNum = input('post.page_num') ?? 0;
        $size = input('post.size') ?? 10;
        $limit = $pageNum * $size;
        $info = Db::table('log_deposit')
            ->field('date_format(create_time,"%Y-%m-%d %H:%i") date,deposit_type,deposit_status,price')
            ->where(array('member_id' => $this->MemberId))
//            ->limit($limit, $size)
            ->order('create_time desc')
            ->select();
        return array('status' => true, 'info' => $info);
    }
}
