<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/25
 * Time: 16:21
 */

namespace app\api\controller;


use app\service\ResourceService;
use think\Db;

class News extends Common
{
    function news_list()
    {
        # 查询未读消息
        $register_time=$this->MemberInfo['register_time'];
        if(empty($register_time)){
            $register_time=$this->MemberInfo['member_create'];
        }
        # 用户未读消息数量
        $unread=Db::table("news n")
            ->field("n.id,n.news_title,n.news_author,date_format(n.news_sendtime,'%Y-%m-%d %H:%i') news_sendtime,n.news_thumb,1 as reader_status,1 as addstatus")
            ->where("is_del=2 and (news_accept=1 or news_accept=3 or news_accept=4) and news_sendtime>='".$register_time."'
        and news_sendtime<='".date("Y-m-d H:i:s")."' and id not in (select news_id from news_accepter where accepter_id = {$this->MemberId} and accepter_type=2)")
            ->order("n.news_sendtime desc")
            ->select();
        # 查询已读消息
        $read=Db::table("news_accepter na,news n")
            ->field("n.id,n.news_title,n.news_author,date_format(n.news_sendtime,'%Y-%m-%d %H:%i') news_sendtime,n.news_thumb,na.reader_status,2 as addstatus")
            ->where(array("na.accepter_id"=>$this->MemberId,"na.accepter_type"=>2,"na.is_del"=>2))
            ->where("na.news_id=n.id")
            ->order("na.id desc")
            ->select();
        # 合并消息
        $newsList=array_merge($unread,$read);
        if(empty($newsList)){
            return array("status"=>false);
        }else{
            return array("status"=>true,"list"=>$newsList);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 消息详情
     */
    function news_detail()
    {
        $id=input("post.id");
        $addstatus=input("post.addstatus");
        if($addstatus==1){
            # 查询这个消息是否添加过
            $is_repet=Db::table("news_accepter")->where(array("news_id"=>$id,"accepter_type"=>2,"accepter_id"=>$this->MemberId))->find();
            if(empty($is_repet)){
                # 未添加状态 先添加到已读消息
                Db::table("news_accepter")->insert(array("news_id"=>$id,"accepter_type"=>2,"accepter_id"=>$this->MemberId,"reader_status"=>2));
            }
        }
        # 查询消息详情
        $info=Db::table("news")->field("*,date_format(news_sendtime,'%Y-%m-%d %H:%i') news_sendtime")->where(array("id"=>$id))->find();
        return array("status"=>true,"info"=>$info);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 消息删除
     */
    function news_del()
    {
        $id=input("post.id");
        $addstatus=input("post.addstatus");
        if($addstatus==1){
            # 查询这个消息是否添加过
            $is_repet=Db::table("news_accepter")->where(array("news_id"=>$id))->find();
            if(empty($is_repet)){
                # 未添加状态 先添加到已读消息
                Db::table("news_accepter")->insert(array("news_id"=>$id,"accepter_type"=>2,"accepter_id"=>$this->MemberId,"reader_status"=>2,"is_del"=>1));
                return array("status"=>true);
            }
        }
        Db::table("news_accepter")->where(array("news_id"=>$id,"accepter_type"=>2,"accepter_id"=>$this->MemberId,))->update(array("is_del"=>1));
        return array("status"=>true);
    }
}