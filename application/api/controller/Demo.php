<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/24
 * Time: 9:40
 */

namespace app\api\controller;


use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\LevelService;
use app\api\ApiService\MemberService;
use app\api\ApiService\orderService;
use app\api\ApiService\PartnerService;
use app\api\ApiService\SubsidyService;
use app\api\ApiService\VoucherService;
use app\service\CommissionService;
use app\service\EalspellCommissionService;
use app\service\IGeTuiService;
use app\service\SmsCode;
use app\service\TimeTask;
use Protocol\Curl;
use Redis\Redis;
use think\App;
use think\Controller;
use think\Db;

class Demo extends Controller
{
    public $old_db;
    public $new_db;


    function dateDiff()
    {
        $date1 = date_create(date('Y-m-d H:i:s', strtotime('-2 day')));
        dump($date1);
        $date2 = Db::table('log_integral')
//            ->field('id')
            ->where(array('member_id' => 3630, 'integral_source' => 31))
            ->where("date(integral_time) = '" . date('Y-m-d') . "'")
            ->find();
//        $date2 = date_create($date2);
        dump($date2);
        return;
        dump(date_diff($date1, $date2));
        dump(date_diff($date1, $date2));
        dump(date_diff($date1, $date2)->format("%R%a days"));
        dump(date_diff($date1, $date2, true)->format("%R%a days"));
        dump(date_diff($date2, $date1)->format("%a"));
        dump(date_diff($date2, $date1, true)->format("%R%a days"));
    }

    function findOrder()
    {
        $redis = new Redis();
        $orderNumber = $redis->hget('tranfer25', 'o');
        $w = json_decode($redis->hget('tranfer1733', 'r'), true);
        $q = json_decode($redis->hget('tranfer25', 'r'), true);
        dump($orderNumber);
        dump($w);
        dump($q);
//        $orderNumber = '25'.date('YmdHis',strtotime('2021-10-11 19:27:33')).strtotime('2021-10-11 19:27:33');
        $PayMent = new PayMent();
        $res = $PayMent->findTest($orderNumber, 1, 'transfer');
        dump($res);
    }

    public function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->old_db = Db::connect("ecars_old", true);
        $this->new_db = Db::connect();
    }

    function checkBalance()
    {

    }

    function clearShareRedis()
    {
        $redis = new Redis();
//        $redis->hDel('addShare', $this->MemberId);
//        $repeat = $redis->hDel('isShare', 'share_busines_integral'.input('get.member_id'));
        $requestStatus = $redis->hGet('vipExpired', 'requestStatus');
        $time = date('Y-m-d');
        # 今天是否发送过会员卡过期提醒
        $todayRemind = $redis->hGet('vipExpired', $time);
        dump($requestStatus);
        dump($todayRemind);
        $member = Db::table('member_mapping mm')
            ->field('mm.id,member_expiration,member_level_id,member_id,member_name,nickname,level_title,member_type,equipment_id')
            ->join('member m', 'm.id=mm.member_id', 'left')
            ->join('member_level ml', 'ml.id=mm.member_level_id', 'left')
            ->where("member_expiration <= '" . $time . "'")
            ->where('member_level_id = 1 or member_level_id = 2')
            ->select();
        dump($member);
    }

    function demodemo()
    {
        $member_id = input("get.member_id");
        $pay_price = input("get.pay_price");
        $pay_type = input("get.pay_type");
        $nowLevel = input("get.now_level");
        $up_level = input("get.up_level");
        $employee_id = input("get.employee_id");
        $assign_id = input("get.assign_id");
        $formMark = input('get.formMark');
        return;
        if (empty($assign_id) and empty($employee_id) and empty($shareMemberId)) {
            $_redis = new Redis();
            $recommend = $_redis->hGetJson("relation_member", $member_id);
            if (!empty($recommend)) {
                //{"share_member_id":"","assignId":"","employeeId":"113","member_id":1733,"formMark":"employeeSal"}
                if (array_key_exists('assignId', $recommend) and !empty($recommend['assignId'])) {
                    $assign_id = $recommend['assignId'];
                }
                if (array_key_exists('employeeId', $recommend) and !empty($recommend['employeeId'])) {
                    $employee_id = $recommend['employeeId'];
                }
                if (array_key_exists('share_member_id', $recommend) and !empty($recommend['share_member_id'])) {
                    $shareMemberId = $recommend['share_member_id'];
                }
                if (array_key_exists('formMark', $recommend) and !empty($recommend['formMark'])) {
                    $formMark = $recommend['formMark'];
                }
            }
        }
        $commissionService = new CommissionService();
        $commissionService->cooperativeDeduct($assign_id, $employee_id, 'upgrade', $pay_price, $member_id, $nowLevel, $up_level, '', $formMark, $pay_type);
    }

    function employeeSal()
    {
        $assign = Db::table('assign_staff')->select();
        foreach ($assign as $k => $v) {
            Db::table('orders')->where(array('biz_id' => $v['biz_id']))->update(array('employee_sal' => $v['employee_id']));
            Db::table('biz_settlement')->where(array('biz_id' => $v['biz_id']))->update(array('employee_sal' => $v['employee_id']));
        }
        dump(123);
    }

    function bizsettlement()
    {
        $bizId = input('get.bizId');
        $order_number = input('get.orderNumber');
        $payType = input('get.payType');
        $order_price = input('get.order_price');
        $voucher_price = input('get.voucher_price');
        $is_online = input('get.is_online');
        $commissionService = new CommissionService();
        $commissionService->bizSettlement($order_number, $payType, $order_price, $voucher_price, $bizId, 'modify',$is_online);
    }


    function demo()
    {
        $_redis = new Redis();
        $data = ["share_member_id" => "0",
            "assignId" => "435",
            "employeeId" => "0",
            "member_id" => '11850',
            "formMark" => '0'];
//       $_redis->hSetJson("relation_member",input('get.member_ids'),$data);
//        $_redis->hDel('relation_member',1733);
//        $_redis->hDel('relation_member',9405);
        $recommend = $_redis->hGetJson("relation_member", input('get.member_id'));
        $recommends = $_redis->hGetJson("relation_member", input('get.member_ids'));
        dump($recommend);
        dump($recommends);
        die;
        $_redis->hDel('merchants_cate2', 'all');
        $_redis->hDel('merchants_cate1', 'all');
        dump($a = $_redis->hGet('memberPayCode', '9405'));
        die;

        $getui = new IGeTuiService();
////       $res =  $getui->pushMessageToSingle('这是内容啊', '红包来了', '推送内容的啊', 265);
////       $res =  $getui->pushMessageToApp('这是内容啊', '红包来了');
////       dump($res);
        $memberid = input('get.memberId');
        $a = $getui->MessageNotification($memberid, 8, array('expiration' => '2020-12-31', 'level_title' => '体验卡会员'), true, false);
        dump($a);
        $status = $getui->getUserStatus($memberid);
        dump($status);
        /*       $time = date('Y-m-d');
               $member = Db::table('member_mapping mm')
                   ->field('member_expiration,member_level_id,member_id,member_name,nickname,level_title,member_phone')
                   ->join('member m', 'm.id=mm.member_id','left')
                   ->join('member_level ml', 'ml.id=mm.member_level_id','left')
                   ->where("member_expiration <= '" . $time . "'")
                   ->where('member_level_id > 0')
                   ->where('equipment_id != ""')
                   ->order('member_id asc')
                   ->select();
               dump($member);*/

    }

    function transfers()
    {
        $order = [
            'partner_trade_no' => date('YmdHis') . time(),              //商户订单号
            'openid' => 'oafle6EJptAi33d2pAOVBqWoSoM8',  //unionid oxv3xs1sKJu05BbGMTgxGUeAmSrQ                      //收款人的openid
//            'openid' => 'ok5xFt4ZB5hYmnpm1OYgzEgUSOt8',                        //收款人的openid
            'check_name' => 'NO_CHECK',            //NO_CHECK：不校验真实姓名\FORCE_CHECK：强校验真实姓名
            // 're_user_name'=>'张三',              //check_name为 FORCE_CHECK 校验实名的时候必须提交
            'amount' => 1 * 100,                       //企业付款金额，单位为分
            'desc' => '帐户提现',                  //付款说明
        ];
        $p = new PayMent();
        $a = $p->transfers($order);
        dump($a);
    }

    function demos()
    {
        $redis = new Redis();
        $redisMark = $redis->hGet('extra_giving_integral', date('Y-m'));
//        $redis->delete('extra_giving_integral_mark');
        $redisMarks = $redis->get('extra_giving_integral_mark');
        dump($redisMark);
        dump($redisMarks);
        return;
        $mobile = input('get.phone');
        var_dump($mobile);
        $code = $redis->hGet('smsCode', strval($mobile));
        var_dump($code);

        $info = $redis->hGetJson("memberInfo", 639);
        dump($info);

        $a = Db::table('deduct')->where(array('employee_id' => 11))->order('id desc')->select();
        dump($a);
    }

    function sendCode()
    {
        /*$phone = input('get.phone');
        $SmsCode = new SmsCode();
        $code = input('get.code');
        $a = $SmsCode->Ysms($phone,$content = null, $mark = true,$code);
        dump($a);*/
        $phone = input('get.phone');
        $SmsCode = new SmsCode();
        $code = input('get.code');
        $a = $SmsCode->Ysms($phone, $code, true);
        dump($a);
    }

    function ecars_old()
    {
        $info = $this->new_db->table('member_voucher')
            ->where(array('status' => 1))
            ->where("`create` >= '" . date('Y-m-d H:i:s') . "'")
            ->select();
//        dump(Db::connect('database.ecars_old'));
//        dump(Db::connect('database.ecars_old')->table('member_voucher')->getLastSql());
        dump($info);
    }

    function demo_member()
    {
//        $list=Db::table("orders")->where("member_id=9840")->select();

//            ->where(array("member_id"=>'9840'))


        /*    $list=Db::query('UPDATE `ecars`.`orders` SET `evaluation_status`=1 WHERE `id`=38825');
            $list=Db::query('UPDATE `ecars`.`orders` SET `evaluation_status`=1 WHERE `id`=38822');*/
//        dump($list);

        $phone = input('get.phone');
        if (!empty($phone)) {
            $info = Db::table('member m')->join('member_mapping mm', 'mm.member_id = m.id', 'left')->where(array('member_phone' => $phone))->select();
            dump($info);
            die;
            $id = Db::table('member')->where(array('member_phone' => $phone))->value('id');


            $a = Db::table('member')->where(array('member_phone' => $phone))->delete();
            dump($a);
            $b = Db::table('member_mapping')->where(array('member_id' => $id))->delete();
            dump($b);
        } else {
            $unionid = input("get.unionid");
            $info = Db::table('member m')->join('member_mapping mm', 'mm.member_id = m.id', 'left')->where(array('member_unionid' => $unionid))->select();
            dump($info);


            dump('没有手机号');
        }

    }


    function deduct()
    {
        ignore_user_abort(true);     // 忽略客户端断开
        set_time_limit(0);           // 设置执行不超时
//        $size = 100;
//        $page = input('get.page') * $size;

        /*        $orderInfo = Db::table('orders')
                    ->field('order_number,car_level,member_id,biz_id')
                    ->where(array('biz_id' => 1))
                    ->where("order_create >= '2021-01-01 00:00:00' and order_create <= '2021-02-07 00:00:00'")
                    ->where(array('order_status' => 5))
                    ->order(array('id' => 'desc'))
                    ->limit($page, $size)
                    ->select();*/
//        die;
        $orderInfo = Db::table('orders')->where(array('order_number' => '20210508093216303100019'))->select();
        $commissionService = new CommissionService();
        foreach ($orderInfo as $k => $v) {
            # 服务工单
            $orderDetail = Db::table('order_server')->where(array('order_number' => $v['order_number']))->select();
            if (!empty($orderDetail)) {
                foreach ($orderDetail as $kk => $vv) {
                    if (!empty($vv['employee_id'])) {
                        $employee_con = explode(',', $vv['employee_id']);
                        foreach ($employee_con as $m => $n) {
                            # 查询评论
                            $score = Db::table('evaluation_score')->field('score')->where(array('orderserver_id' => $vv['id'], 'employee_id' => $n, 'type' => 1))->find()['score'];
                            if (empty($score)) {
                                $score = 3;
                            }
                            dump($n);
                            $commissionService->commission($v['biz_id'], $vv['id'], 1, $v['car_level'], 'comm', $n, $score);
                        }
                    }
                }
            }
            # 商品工单
            $orderBiz = Db::table('order_biz')->where(array('order_number' => $v['order_number']))->select();
            if (!empty($orderBiz)) {
                foreach ($orderBiz as $kk => $vv) {
                    if (!empty($vv['employee_id'])) {
                        $employee_con = explode(',', $vv['employee_id']);
                        foreach ($employee_con as $m => $n) {
                            # 查询评论
                            $score = Db::table('evaluation_score')->field('score')->where(array('orderserver_id' => $vv['id'], 'employee_id' => $n, 'type' => 2))->find()['score'];
                            if (empty($score)) {
                                $score = 3;
                            }
                            $commissionService->commission($v['biz_id'], $vv['id'], 2, $v['car_level'], 'comm', $n, $score);
                        }
                    }
                }
            }
        }
    }

    function refundAll()
    {
//        return;
        $payMent = new PayMent();
        $payStatus = $payMent->orderStatusQuery('a1m12591t1642482779994', 1);
        dump($payStatus);
        die;
//        return;
        $data = [
            'out_trade_no' => '202109062010533040800002',
            'out_refund_no' => time(),
            'total_fee' => floatval(22) * 100, //
            'refund_fee' => floatval(22) * 100,
            'refund_desc' => '预约超时退款',
        ];
        $status = $payMent->wxRefund($data);
        dump($status);
    }

    function aliRefund()
    {
        $payMent = new PayMent();
        $data = [
            'out_trade_no' => 'n1m11596u2t1635220430613',
            'refund_amount' => 259, // $v['pay_price']
        ];
        $status = $payMent->aliRefund($data);
        dump($status);
    }

    function payRedis()
    {
        $redis = new Redis();
        $info = $redis->hGet('payRedis' . input('get.biz_id'));
        $infos = $redis->hGet('payRedis', '1225571005758156');
        dump($info);
        dump($infos);
    }

    function refundDemo()
    {
        $a = orderService::ordersOverTime();
    }

    function bizCreation(){
        $redis = new Redis();
        $info = $redis->hGetJson('BizCreation', strval(input('get.id')));
        dump($info);
    }

    function rechargeSuccess()
    {
        $pay_price = input("post.pay_price");
        $pay_type = input("post.pay_type");
        $giving = input("post.giving");
        $order_number = input("post.order_number");
        $employee_id = input("post.employee_id");
        $assign_id = input("post.assign_id");
        $shareMemberId = input("post.shareMemberId");
        $member_id = input("post.member_id");
        $BalanceService = new BalanceService();
        $BalanceService->memberRecharge($member_id, $pay_price, $pay_type, 1, $giving, 1, $order_number, $employee_id, $assign_id, $shareMemberId);
        return array("status" => true);
    }


    function aliPayStatusQuery()
    {
        $order_number = input("get.order_number");
        $PayMent = new PayMent();
        $res = $PayMent->orderStatusQuery($order_number, 2);
        dump($res);
        return array("status" => true, "result" => $res);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查询服务可用卡券
     */
    function serviceCanUseVoucher()
    {
        $service_id = input("post.service_id");
        $price = input("post.price");
        $member_id = input("post.member_id");
        $VoucherService = new VoucherService();
        $voucher = $VoucherService->memberVoucher($member_id, array("service" => true, "service_id" => $service_id, "price" => $price));
        if (!empty($voucher['list'])) {
            foreach ($voucher['list'] as $k => $v) {
                $Desc = $VoucherService->voucherTypeDesc($v);
                $voucher['list'][$k]['TypeTitle'] = $Desc['TypeTitle'];
                $voucher['list'][$k]['voucherDesc'] = $Desc['voucherDesc'];
                $voucher['list'][$k]['voucher_source_title'] = $Desc['voucherSource'];
            }
        }
        return array("status" => true, "list" => $voucher['list'], "num" => $voucher['num']);
    }

    function recommendRelation()
    {
        $member_id = 9405;
        $_redis = new Redis();
        $a = $_redis->hGetJson("relation_member", $member_id);
        dump($a);
    }

    function MemberInfoCache(){
        $memberService = new MemberService();
        $shareMemberInfo = $memberService->MemberInfoCache(input('get.member_id'));
        dump($shareMemberInfo);
    }

    function partnerIncome()
    {
        return false;
        $member_id = 11221;
        $partner_id = 11143;
        $consump_price = 358;
        $level = 2;
        $pay_type = 1;
        $time = "2021-06-28 13:48:24";
        $integral = intval($consump_price * 0.3);
        $integralService = new IntegralService();
        $integralService->addMemberIntegral($partner_id, 19, $integral, 1);
        # 判断是不是合伙人
        $memberService = new MemberService();
        $shareMemberInfo = $memberService->MemberInfoCache($partner_id);
        if ($shareMemberInfo['member_type'] == 2) {
            if (($shareMemberInfo['member_level_id'] == 1 or $shareMemberInfo['member_level_id'] == 2) and $shareMemberInfo['member_expiration'] < date('Y-m-d')) {
                # 过期,不是合伙人
                Db::table('member_mapping')->where(array('member_id' => $partner_id))->update(array('member_type' => 1));
            } else {
                # 增加额外赠送
                LevelService::addLevelRecommend("shareMemberId", $level, $member_id);
                # 增加合伙人收益
                $partnerService = new PartnerService();
                $partnerSet = $partnerService->partnerGive();
                $partnerIncome = $partnerSet['info']['membership_ratio'] * $consump_price;
                Db::table('member_partner')->where(array('member_id' => $partner_id))->setInc('partner_balance', $partnerIncome);
                Db::table('member_partner')->where(array('member_id' => $partner_id))->setInc('partner_balance_total', $partnerIncome);
                # 加收益记录
                Db::table('log_consump')->insert(array(
                    'member_id' => $partner_id,
                    'consump_price' => $partnerIncome,
                    'consump_pay_type' => $pay_type,
                    'consump_time' => $time,
                    'log_consump_type' => 2,
                    'consump_type' => 1,
                    'income_type' => 8,
                    'biz_id' => 0
                ));
                SubsidyService::addExpenses(array("type_id" => 4, "price" => $partnerIncome));

            }
        }
        # 完成任务=>15 推荐会员
        TimeTask::finishTask($partner_id, 15);
        var_dump(123);
    }

    function recommendPartnerIncomeReward()
    {
        return false;
        $pid = 25;
        # 查询赠送
        $partnerService = new PartnerService();
        $partnerSet = $partnerService->partnerGive()['info'];
        # 加推荐收益
        Db::table('member_partner')->where(array('member_id' => $pid))->setInc('partner_balance', $partnerSet['prise_price']);
        Db::table('member_partner')->where(array('member_id' => $pid))->setInc('partner_balance_total', $partnerSet['prise_price']);
        $username = Db::table("member")->field("member_name")->where(array("id" => $pid))->find()['member_name'];
        SubsidyService::addExpenses(array("type_id" => 14, "price" => $partnerSet['prise_price'], "username" => $username));
        # 收益记录
        Db::table('log_consump')->insert(array(
            'member_id' => $pid,
            'consump_price' => $partnerSet['prise_price'],
            'consump_pay_type' => 5,
            'consump_time' => date('Y-m-d H:i:s'),
            'log_consump_type' => 2,
            'consump_type' => 1,
            'income_type' => 9,
            'biz_id' => 0
        ));
        var_dump(456);
    }

    function shaWanYi()
    {
        $phone = "13236999488";
        $memberService = new MemberService();
        $is_register = Db::table("member")->field("id")->where(array("member_phone" => $phone))->find();
        if (empty($is_register['id'])) {
            # 判断unionid 有没有被注册
            var_dump(123);

        } else {
            $member_id = $is_register['id'];
            var_dump(456);
        }
    }

    function changeUpgrade()
    {
        $info = Db::query("select *,
((select level_practical from member_level where `id` = up_level)-(select level_practical from member_level where `id` = now_level)) newPrice,
(select level_title from member_level where `id` = now_level) nowTitle,
(select level_title from member_level where `id` = up_level) upTitle
 FROM `upgrade`");
        dump($info);
//        foreach ($info as $k=>$v){
//            Db::table('upgrade')->where(array('id'=>$v['id']))->update(array('price'=>$v['newPrice']));
//        }
//        dump(132);
    }

    function orderAddress()
    {
        $info = Db::query("SELECT  
    *,  
    ROUND(  
        6371.393 * 2 * ASIN(  
            SQRT(  
                POW(  
                    SIN(  
                        (  
                            40.254056 * PI() / 180 - lat * PI() / 180  
                        ) / 2  
                    ),  
                    2  
                ) + COS(22.53462 * PI() / 180) * COS(lat * PI() / 180) * POW(  
                    SIN(  
                        (  
                            122.138126 * PI() / 180 - lon * PI() / 180  
                        ) / 2  
                    ),  
                    2  
                )  
            )  
        ) * 1000  
    ) AS distance_um  
FROM  
    merchants  
    where status=1 and start_time is not null and end_time is not null
ORDER BY  
    distance_um ASC limit 0,10");
        dump($info);
    }

    function jianjifen()
    {
        return;
        $info = Db::table('log_integral')
            ->field('id,integral_number,member_id')
            ->where('integral_source = 33')
            ->select();
        dump($info);
        foreach ($info as $k => $v) {
            Db::table('member_mapping')->where(array('member_id' => $v['member_id']))
                ->setDec('member_integral', $v['integral_number']);
            Db::table('log_integral')->where(array('id' => $v['id']))->delete();
        }
    }


    function applyMemberDemo()
    {
        return false;
        $level = 1;
        $balance = 99;
        $setmeal_id = 0;
        $pay_type = 1;
        $order_number = 'a1m11573t1640239928321';
        $employee_id = 0;
        $assign_id = 37;
        $shareMemberId = 0;
        $formMark = 'agent';
        $member_id = 11573;
        LevelService::applyMember($member_id, $level, $balance, $setmeal_id, array("consump_price" => $balance, "pay_type" => $pay_type, "consump_type" => 1,
            "order_number" => $order_number, "biz_id" => 1, 'employee_id' => $employee_id, 'assign_id' => $assign_id, 'shareMemberId' => $shareMemberId, 'formMark' => $formMark));
        var_dump(123);
    }

    function getTimeTaskDemo()
    {
        $redis = new Redis();
//        $redis->hDel('payRedis','a1m12876t1633848257876');
        $info = $redis->hGet('payRedis');
        $PayMent = new PayMent();
        $payRes = $PayMent->orderStatusQuery('a1m11573t1640911118829', 1);
        $payResInfo = json_decode($payRes, true);
        dump($info);
        dump($payResInfo);
    }

    function recoverLevel()
    {
        return false;
        ignore_user_abort(true);     // 忽略客户端断开
        set_time_limit(0);           // 设置执行不超时
        $info = '[{"member_id":3208,"member_create":"2019-06-30 16:28:32","member_level_id":3},{"member_id":500,"member_create":"2019-07-01 16:07:42","member_level_id":2},{"member_id":3220,"member_create":"2019-07-01 16:08:55","member_level_id":3},{"member_id":3022,"member_create":"2019-07-03 17:28:48","member_level_id":3},{"member_id":2665,"member_create":"2019-07-05 08:43:30","member_level_id":4},{"member_id":3123,"member_create":"2019-07-08 09:34:05","member_level_id":3},{"member_id":3305,"member_create":"2019-07-08 10:34:30","member_level_id":3},{"member_id":755,"member_create":"2019-07-09 11:02:02","member_level_id":2},{"member_id":1687,"member_create":"2019-07-10 12:02:00","member_level_id":3},{"member_id":3361,"member_create":"2019-07-12 10:18:29","member_level_id":3},{"member_id":2865,"member_create":"2019-07-12 17:20:43","member_level_id":3},{"member_id":3368,"member_create":"2019-07-12 17:36:02","member_level_id":2},{"member_id":1208,"member_create":"2019-07-13 13:50:23","member_level_id":3},{"member_id":3198,"member_create":"2019-07-14 14:35:44","member_level_id":3},{"member_id":3210,"member_create":"2019-07-16 10:17:54","member_level_id":3},{"member_id":3310,"member_create":"2019-07-17 15:00:55","member_level_id":3},{"member_id":2981,"member_create":"2019-07-17 17:23:48","member_level_id":2},{"member_id":3457,"member_create":"2019-07-18 08:18:06","member_level_id":3},{"member_id":3330,"member_create":"2019-07-18 16:00:42","member_level_id":3},{"member_id":3472,"member_create":"2019-07-18 16:51:31","member_level_id":3},{"member_id":2534,"member_create":"2019-07-19 11:51:05","member_level_id":2},{"member_id":3504,"member_create":"2019-07-20 09:15:52","member_level_id":2},{"member_id":3507,"member_create":"2019-07-20 09:31:14","member_level_id":3},{"member_id":3535,"member_create":"2019-07-21 10:43:10","member_level_id":4},{"member_id":2854,"member_create":"2019-07-24 16:50:56","member_level_id":3},{"member_id":3663,"member_create":"2019-07-26 15:58:17","member_level_id":3},{"member_id":3671,"member_create":"2019-07-27 10:01:13","member_level_id":4},{"member_id":3680,"member_create":"2019-07-27 13:45:18","member_level_id":4},{"member_id":3684,"member_create":"2019-07-27 14:27:17","member_level_id":2},{"member_id":3774,"member_create":"2019-08-01 12:39:13","member_level_id":3},{"member_id":3775,"member_create":"2019-08-01 14:37:07","member_level_id":2},{"member_id":3175,"member_create":"2019-08-05 11:49:35","member_level_id":2},{"member_id":3918,"member_create":"2019-08-10 08:52:13","member_level_id":3},{"member_id":789,"member_create":"2019-08-15 17:49:32","member_level_id":2},{"member_id":4104,"member_create":"2019-08-18 17:45:30","member_level_id":3},{"member_id":4116,"member_create":"2019-08-19 09:47:26","member_level_id":4},{"member_id":2096,"member_create":"2019-08-19 15:34:13","member_level_id":3},{"member_id":4126,"member_create":"2019-08-19 15:44:07","member_level_id":3},{"member_id":3023,"member_create":"2019-08-20 12:06:23","member_level_id":2},{"member_id":4189,"member_create":"2019-08-22 10:25:50","member_level_id":6},{"member_id":4210,"member_create":"2019-08-23 16:00:47","member_level_id":2},{"member_id":4214,"member_create":"2019-08-23 17:26:11","member_level_id":3},{"member_id":4224,"member_create":"2019-08-24 09:40:40","member_level_id":6},{"member_id":4228,"member_create":"2019-08-24 13:36:21","member_level_id":2},{"member_id":4309,"member_create":"2019-08-28 16:09:19","member_level_id":2},{"member_id":4326,"member_create":"2019-08-29 15:52:55","member_level_id":3},{"member_id":3617,"member_create":"2019-08-30 13:16:20","member_level_id":2},{"member_id":1758,"member_create":"2019-08-30 13:58:11","member_level_id":2},{"member_id":4344,"member_create":"2019-08-30 16:47:22","member_level_id":2},{"member_id":4348,"member_create":"2019-08-31 09:17:14","member_level_id":3},{"member_id":3679,"member_create":"2019-09-01 16:11:49","member_level_id":3},{"member_id":4394,"member_create":"2019-09-01 16:23:34","member_level_id":3},{"member_id":4409,"member_create":"2019-09-02 12:01:35","member_level_id":3},{"member_id":4165,"member_create":"2019-09-02 14:08:35","member_level_id":3},{"member_id":4238,"member_create":"2019-09-03 08:57:13","member_level_id":2},{"member_id":3001,"member_create":"2019-09-03 09:26:58","member_level_id":3},{"member_id":4435,"member_create":"2019-09-03 13:45:06","member_level_id":3},{"member_id":4445,"member_create":"2019-09-04 13:12:11","member_level_id":3},{"member_id":1489,"member_create":"2019-09-05 14:47:44","member_level_id":2},{"member_id":1473,"member_create":"2019-09-06 10:58:37","member_level_id":3},{"member_id":94,"member_create":"2019-09-06 11:14:59","member_level_id":2},{"member_id":2878,"member_create":"2019-09-08 13:50:06","member_level_id":2},{"member_id":4504,"member_create":"2019-09-08 14:01:56","member_level_id":3},{"member_id":4290,"member_create":"2019-09-08 16:00:22","member_level_id":3},{"member_id":2158,"member_create":"2019-09-08 16:44:00","member_level_id":2},{"member_id":4522,"member_create":"2019-09-09 11:05:09","member_level_id":3},{"member_id":4559,"member_create":"2019-09-12 09:56:48","member_level_id":3},{"member_id":2318,"member_create":"2019-09-14 07:44:03","member_level_id":2},{"member_id":4649,"member_create":"2019-09-15 14:28:13","member_level_id":2},{"member_id":2481,"member_create":"2019-09-16 08:59:09","member_level_id":4},{"member_id":4739,"member_create":"2019-09-17 17:10:54","member_level_id":3},{"member_id":4767,"member_create":"2019-09-18 18:12:23","member_level_id":2},{"member_id":491,"member_create":"2019-09-19 15:57:30","member_level_id":6},{"member_id":4875,"member_create":"2019-09-23 13:35:23","member_level_id":3},{"member_id":3528,"member_create":"2019-09-24 08:51:20","member_level_id":3},{"member_id":4888,"member_create":"2019-09-24 11:47:00","member_level_id":2},{"member_id":4914,"member_create":"2019-09-25 12:17:08","member_level_id":3},{"member_id":3705,"member_create":"2019-09-26 13:33:44","member_level_id":3},{"member_id":4989,"member_create":"2019-09-28 08:51:32","member_level_id":2},{"member_id":4994,"member_create":"2019-09-28 11:41:44","member_level_id":3},{"member_id":5004,"member_create":"2019-09-28 13:16:49","member_level_id":2},{"member_id":2991,"member_create":"2019-09-28 13:29:22","member_level_id":2},{"member_id":5014,"member_create":"2019-09-28 15:48:47","member_level_id":3},{"member_id":5058,"member_create":"2019-09-30 12:49:38","member_level_id":2},{"member_id":5164,"member_create":"2019-10-05 08:22:40","member_level_id":2},{"member_id":5186,"member_create":"2019-10-06 08:18:28","member_level_id":3},{"member_id":5202,"member_create":"2019-10-06 13:43:32","member_level_id":3},{"member_id":4653,"member_create":"2019-10-07 09:01:40","member_level_id":4},{"member_id":5231,"member_create":"2019-10-07 12:46:15","member_level_id":3},{"member_id":1013,"member_create":"2019-10-07 14:45:29","member_level_id":3},{"member_id":5248,"member_create":"2019-10-07 16:14:39","member_level_id":2},{"member_id":5263,"member_create":"2019-10-08 09:46:19","member_level_id":4},{"member_id":2256,"member_create":"2019-10-08 15:08:19","member_level_id":4},{"member_id":5274,"member_create":"2019-10-08 17:02:40","member_level_id":2},{"member_id":5284,"member_create":"2019-10-09 09:57:51","member_level_id":3},{"member_id":4903,"member_create":"2019-10-09 13:27:45","member_level_id":3},{"member_id":5297,"member_create":"2019-10-09 17:28:54","member_level_id":2},{"member_id":1362,"member_create":"2019-10-10 10:59:18","member_level_id":3},{"member_id":4458,"member_create":"2019-10-11 07:52:33","member_level_id":3},{"member_id":1000,"member_create":"2019-10-11 14:53:30","member_level_id":4},{"member_id":5342,"member_create":"2019-10-12 09:35:54","member_level_id":3},{"member_id":5354,"member_create":"2019-10-12 15:34:43","member_level_id":3},{"member_id":5366,"member_create":"2019-10-14 08:43:23","member_level_id":3},{"member_id":497,"member_create":"2019-10-14 10:16:02","member_level_id":3},{"member_id":5369,"member_create":"2019-10-14 12:32:23","member_level_id":2},{"member_id":4701,"member_create":"2019-10-16 16:28:40","member_level_id":4},{"member_id":5407,"member_create":"2019-10-17 09:23:11","member_level_id":4},{"member_id":5353,"member_create":"2019-10-17 10:52:02","member_level_id":3},{"member_id":4246,"member_create":"2019-10-19 10:39:41","member_level_id":3},{"member_id":5003,"member_create":"2019-10-19 12:14:47","member_level_id":2},{"member_id":534,"member_create":"2019-10-19 14:31:11","member_level_id":2},{"member_id":3177,"member_create":"2019-10-19 16:58:50","member_level_id":4},{"member_id":1181,"member_create":"2019-10-20 10:20:32","member_level_id":3},{"member_id":5474,"member_create":"2019-10-21 14:37:33","member_level_id":3},{"member_id":4355,"member_create":"2019-10-21 15:26:47","member_level_id":0},{"member_id":5500,"member_create":"2019-10-23 16:50:07","member_level_id":3},{"member_id":5506,"member_create":"2019-10-24 11:19:58","member_level_id":4},{"member_id":5518,"member_create":"2019-10-25 13:41:13","member_level_id":3},{"member_id":959,"member_create":"2019-10-27 11:00:00","member_level_id":3},{"member_id":297,"member_create":"2019-10-28 09:59:13","member_level_id":3},{"member_id":5558,"member_create":"2019-10-28 16:17:56","member_level_id":3},{"member_id":1861,"member_create":"2019-10-29 08:33:24","member_level_id":2},{"member_id":5564,"member_create":"2019-10-29 08:33:35","member_level_id":2},{"member_id":5572,"member_create":"2019-10-29 15:52:05","member_level_id":3},{"member_id":5590,"member_create":"2019-10-30 13:49:19","member_level_id":3},{"member_id":5381,"member_create":"2019-10-31 13:02:46","member_level_id":3},{"member_id":2734,"member_create":"2019-11-02 16:48:35","member_level_id":3},{"member_id":5651,"member_create":"2019-11-03 08:44:54","member_level_id":3},{"member_id":5667,"member_create":"2019-11-03 13:56:41","member_level_id":2},{"member_id":5703,"member_create":"2019-11-05 09:20:22","member_level_id":3},{"member_id":5730,"member_create":"2019-11-06 10:46:20","member_level_id":3},{"member_id":5745,"member_create":"2019-11-07 15:13:54","member_level_id":2},{"member_id":5756,"member_create":"2019-11-08 10:49:39","member_level_id":2},{"member_id":5776,"member_create":"2019-11-09 12:55:26","member_level_id":2},{"member_id":5812,"member_create":"2019-11-11 15:47:02","member_level_id":0},{"member_id":5842,"member_create":"2019-11-14 10:14:36","member_level_id":4},{"member_id":4763,"member_create":"2019-11-14 16:28:29","member_level_id":2},{"member_id":820,"member_create":"2019-11-15 15:03:08","member_level_id":2},{"member_id":1880,"member_create":"2019-11-15 15:59:11","member_level_id":3},{"member_id":5864,"member_create":"2019-11-16 09:15:45","member_level_id":3},{"member_id":3323,"member_create":"2019-11-16 15:11:52","member_level_id":2},{"member_id":3285,"member_create":"2019-11-18 11:22:49","member_level_id":2},{"member_id":595,"member_create":"2019-11-18 12:45:17","member_level_id":2},{"member_id":5567,"member_create":"2019-11-18 15:56:26","member_level_id":2},{"member_id":5760,"member_create":"2019-11-19 09:42:03","member_level_id":2},{"member_id":5898,"member_create":"2019-11-19 11:21:49","member_level_id":3},{"member_id":87,"member_create":"2019-11-19 12:30:22","member_level_id":2},{"member_id":3067,"member_create":"2019-11-20 08:18:28","member_level_id":3},{"member_id":5908,"member_create":"2019-11-20 11:50:49","member_level_id":3},{"member_id":5933,"member_create":"2019-11-21 12:22:05","member_level_id":2},{"member_id":5943,"member_create":"2019-11-22 08:37:07","member_level_id":2},{"member_id":5946,"member_create":"2019-11-22 09:43:35","member_level_id":2},{"member_id":5960,"member_create":"2019-11-23 10:36:00","member_level_id":3},{"member_id":5966,"member_create":"2019-11-24 10:18:57","member_level_id":3},{"member_id":248,"member_create":"2019-11-24 13:47:59","member_level_id":3},{"member_id":5971,"member_create":"2019-11-24 15:33:57","member_level_id":3},{"member_id":3054,"member_create":"2019-11-29 09:47:02","member_level_id":2},{"member_id":1257,"member_create":"2019-11-29 11:18:26","member_level_id":3},{"member_id":4236,"member_create":"2019-11-29 11:36:53","member_level_id":2},{"member_id":6043,"member_create":"2019-12-01 09:10:51","member_level_id":3},{"member_id":6073,"member_create":"2019-12-05 15:52:29","member_level_id":3},{"member_id":3609,"member_create":"2019-12-06 15:09:30","member_level_id":4},{"member_id":6085,"member_create":"2019-12-07 11:41:17","member_level_id":0},{"member_id":6108,"member_create":"2019-12-09 16:39:50","member_level_id":3},{"member_id":6113,"member_create":"2019-12-10 12:33:58","member_level_id":2},{"member_id":6122,"member_create":"2019-12-10 17:01:10","member_level_id":3},{"member_id":6127,"member_create":"2019-12-11 11:59:07","member_level_id":3},{"member_id":6136,"member_create":"2019-12-12 11:13:07","member_level_id":3},{"member_id":2678,"member_create":"2019-12-12 12:05:52","member_level_id":3},{"member_id":6147,"member_create":"2019-12-13 15:54:21","member_level_id":2},{"member_id":408,"member_create":"2019-12-14 08:08:50","member_level_id":6},{"member_id":6159,"member_create":"2019-12-14 16:09:55","member_level_id":2},{"member_id":2914,"member_create":"2019-12-16 16:23:14","member_level_id":3},{"member_id":4047,"member_create":"2019-12-18 08:26:13","member_level_id":2},{"member_id":6230,"member_create":"2019-12-23 10:08:54","member_level_id":3},{"member_id":6238,"member_create":"2019-12-24 12:39:12","member_level_id":3},{"member_id":6249,"member_create":"2019-12-26 11:17:02","member_level_id":2},{"member_id":6261,"member_create":"2019-12-26 14:40:13","member_level_id":2},{"member_id":6291,"member_create":"2019-12-28 12:51:21","member_level_id":4},{"member_id":6326,"member_create":"2019-12-30 17:20:47","member_level_id":2},{"member_id":6332,"member_create":"2019-12-31 08:46:07","member_level_id":2},{"member_id":6333,"member_create":"2019-12-31 09:03:30","member_level_id":2},{"member_id":6341,"member_create":"2019-12-31 11:39:22","member_level_id":2},{"member_id":6355,"member_create":"2019-12-31 16:51:01","member_level_id":2},{"member_id":1093,"member_create":"2019-12-31 17:33:40","member_level_id":3},{"member_id":384,"member_create":"2020-01-01 07:48:29","member_level_id":2},{"member_id":4554,"member_create":"2020-01-01 10:34:39","member_level_id":3},{"member_id":6382,"member_create":"2020-01-02 12:20:23","member_level_id":0},{"member_id":6416,"member_create":"2020-01-03 11:21:02","member_level_id":2},{"member_id":5685,"member_create":"2020-01-03 14:45:53","member_level_id":3},{"member_id":5897,"member_create":"2020-01-03 16:26:41","member_level_id":3},{"member_id":6436,"member_create":"2020-01-04 09:31:13","member_level_id":2},{"member_id":6448,"member_create":"2020-01-04 15:02:26","member_level_id":2},{"member_id":6465,"member_create":"2020-01-05 09:30:37","member_level_id":2},{"member_id":6216,"member_create":"2020-01-05 11:10:07","member_level_id":2},{"member_id":6090,"member_create":"2020-01-05 11:37:10","member_level_id":2},{"member_id":6472,"member_create":"2020-01-05 11:42:21","member_level_id":2},{"member_id":6493,"member_create":"2020-01-07 08:57:12","member_level_id":3},{"member_id":4792,"member_create":"2020-01-07 09:07:08","member_level_id":2},{"member_id":6502,"member_create":"2020-01-07 10:40:19","member_level_id":3},{"member_id":6505,"member_create":"2020-01-07 12:37:13","member_level_id":2},{"member_id":6509,"member_create":"2020-01-07 13:33:24","member_level_id":3},{"member_id":1931,"member_create":"2020-01-09 10:09:23","member_level_id":3},{"member_id":6542,"member_create":"2020-01-09 14:19:16","member_level_id":2},{"member_id":4282,"member_create":"2020-01-10 08:29:52","member_level_id":3},{"member_id":5817,"member_create":"2020-01-10 11:49:07","member_level_id":3},{"member_id":6330,"member_create":"2020-01-10 17:44:22","member_level_id":2},{"member_id":6580,"member_create":"2020-01-11 10:38:23","member_level_id":2},{"member_id":6592,"member_create":"2020-01-11 14:44:56","member_level_id":2},{"member_id":5616,"member_create":"2020-01-12 10:41:32","member_level_id":2},{"member_id":6582,"member_create":"2020-01-12 12:55:24","member_level_id":2},{"member_id":6631,"member_create":"2020-01-13 09:55:08","member_level_id":3},{"member_id":6647,"member_create":"2020-01-13 13:49:22","member_level_id":2},{"member_id":6651,"member_create":"2020-01-13 14:07:31","member_level_id":2},{"member_id":6667,"member_create":"2020-01-14 10:43:42","member_level_id":2},{"member_id":6752,"member_create":"2020-01-16 14:02:28","member_level_id":2},{"member_id":6768,"member_create":"2020-01-16 16:40:02","member_level_id":3},{"member_id":6795,"member_create":"2020-01-17 13:24:26","member_level_id":3},{"member_id":6820,"member_create":"2020-01-18 13:58:56","member_level_id":3},{"member_id":6822,"member_create":"2020-01-18 14:27:20","member_level_id":4},{"member_id":6846,"member_create":"2020-01-19 14:02:59","member_level_id":2},{"member_id":6848,"member_create":"2020-01-19 17:40:02","member_level_id":2},{"member_id":6855,"member_create":"2020-01-20 09:37:50","member_level_id":2},{"member_id":6861,"member_create":"2020-01-20 17:03:30","member_level_id":2},{"member_id":3465,"member_create":"2020-01-21 11:16:35","member_level_id":2},{"member_id":6870,"member_create":"2020-01-21 13:27:53","member_level_id":2},{"member_id":6871,"member_create":"2020-01-21 14:11:48","member_level_id":2},{"member_id":6567,"member_create":"2020-01-22 10:40:42","member_level_id":2},{"member_id":6419,"member_create":"2020-01-22 13:43:52","member_level_id":2},{"member_id":6951,"member_create":"2020-02-08 11:05:39","member_level_id":3},{"member_id":7024,"member_create":"2020-02-11 09:32:14","member_level_id":2},{"member_id":7045,"member_create":"2020-02-11 14:52:33","member_level_id":2},{"member_id":7040,"member_create":"2020-02-11 15:00:40","member_level_id":3},{"member_id":7064,"member_create":"2020-02-12 12:43:16","member_level_id":3},{"member_id":7066,"member_create":"2020-02-12 13:57:07","member_level_id":2},{"member_id":7082,"member_create":"2020-02-12 16:07:08","member_level_id":2},{"member_id":7046,"member_create":"2020-02-14 09:48:30","member_level_id":2},{"member_id":7119,"member_create":"2020-02-14 10:52:51","member_level_id":3},{"member_id":7136,"member_create":"2020-02-18 09:06:40","member_level_id":2},{"member_id":3077,"member_create":"2020-02-19 12:25:49","member_level_id":5},{"member_id":5819,"member_create":"2020-02-20 10:39:45","member_level_id":3},{"member_id":7176,"member_create":"2020-02-22 09:05:35","member_level_id":6},{"member_id":5628,"member_create":"2020-02-22 17:55:12","member_level_id":3},{"member_id":2534,"member_create":"2020-02-25 10:54:20","member_level_id":2},{"member_id":7003,"member_create":"2020-02-25 12:14:38","member_level_id":2},{"member_id":7211,"member_create":"2020-02-25 13:08:14","member_level_id":2},{"member_id":2904,"member_create":"2020-02-27 09:39:25","member_level_id":2},{"member_id":7229,"member_create":"2020-02-27 13:51:27","member_level_id":3},{"member_id":6954,"member_create":"2020-02-29 11:58:14","member_level_id":2},{"member_id":7251,"member_create":"2020-03-01 10:24:36","member_level_id":3},{"member_id":7257,"member_create":"2020-03-01 14:03:41","member_level_id":3},{"member_id":7267,"member_create":"2020-03-01 17:02:04","member_level_id":2},{"member_id":7276,"member_create":"2020-03-02 16:27:59","member_level_id":3},{"member_id":3571,"member_create":"2020-03-04 15:49:04","member_level_id":2},{"member_id":7289,"member_create":"2020-03-05 09:47:49","member_level_id":0},{"member_id":7061,"member_create":"2020-03-05 11:05:21","member_level_id":0},{"member_id":6288,"member_create":"2020-03-05 15:25:09","member_level_id":2},{"member_id":7295,"member_create":"2020-03-05 15:29:09","member_level_id":2},{"member_id":5544,"member_create":"2020-03-11 14:06:29","member_level_id":2},{"member_id":2199,"member_create":"2020-03-14 11:49:11","member_level_id":3},{"member_id":7369,"member_create":"2020-03-15 10:37:10","member_level_id":3},{"member_id":7380,"member_create":"2020-03-16 14:06:42","member_level_id":2},{"member_id":7383,"member_create":"2020-03-16 16:03:08","member_level_id":3},{"member_id":3109,"member_create":"2020-03-18 08:30:49","member_level_id":4},{"member_id":7448,"member_create":"2020-03-24 14:30:42","member_level_id":3},{"member_id":7467,"member_create":"2020-03-29 09:40:32","member_level_id":2},{"member_id":7497,"member_create":"2020-04-01 08:42:39","member_level_id":2},{"member_id":7498,"member_create":"2020-04-01 11:22:32","member_level_id":2},{"member_id":6316,"member_create":"2020-04-01 16:41:11","member_level_id":3},{"member_id":7506,"member_create":"2020-04-01 17:02:19","member_level_id":2},{"member_id":7511,"member_create":"2020-04-02 15:49:56","member_level_id":3},{"member_id":7309,"member_create":"2020-04-03 12:21:21","member_level_id":3},{"member_id":7522,"member_create":"2020-04-03 14:13:47","member_level_id":4},{"member_id":7559,"member_create":"2020-04-05 11:30:18","member_level_id":3},{"member_id":5251,"member_create":"2020-04-08 08:27:05","member_level_id":2},{"member_id":1789,"member_create":"2020-04-13 14:29:17","member_level_id":3},{"member_id":7656,"member_create":"2020-04-18 09:04:26","member_level_id":3},{"member_id":7659,"member_create":"2020-04-18 09:21:28","member_level_id":2},{"member_id":5448,"member_create":"2020-04-19 17:18:12","member_level_id":3},{"member_id":7687,"member_create":"2020-04-22 12:21:05","member_level_id":2},{"member_id":7690,"member_create":"2020-04-22 14:51:57","member_level_id":3},{"member_id":7698,"member_create":"2020-04-23 16:28:05","member_level_id":4},{"member_id":7487,"member_create":"2020-04-24 17:19:37","member_level_id":2},{"member_id":3385,"member_create":"2020-04-26 08:28:08","member_level_id":2},{"member_id":1488,"member_create":"2020-04-27 13:11:40","member_level_id":2},{"member_id":4169,"member_create":"2020-04-28 15:15:01","member_level_id":2},{"member_id":7771,"member_create":"2020-04-29 14:00:56","member_level_id":3},{"member_id":2267,"member_create":"2020-04-29 14:18:37","member_level_id":2},{"member_id":3774,"member_create":"2020-05-01 12:39:07","member_level_id":3},{"member_id":946,"member_create":"2020-05-03 13:59:44","member_level_id":2},{"member_id":7846,"member_create":"2020-05-06 17:46:42","member_level_id":0},{"member_id":7861,"member_create":"2020-05-10 09:50:49","member_level_id":3},{"member_id":5060,"member_create":"2020-05-11 12:46:38","member_level_id":3},{"member_id":7877,"member_create":"2020-05-11 14:21:20","member_level_id":4},{"member_id":7887,"member_create":"2020-05-12 08:40:55","member_level_id":0},{"member_id":7893,"member_create":"2020-05-13 09:07:42","member_level_id":3},{"member_id":7925,"member_create":"2020-05-20 09:24:28","member_level_id":3},{"member_id":1816,"member_create":"2020-05-20 10:54:04","member_level_id":3},{"member_id":7470,"member_create":"2020-05-25 10:09:02","member_level_id":2},{"member_id":7950,"member_create":"2020-05-25 10:31:36","member_level_id":3},{"member_id":7963,"member_create":"2020-05-26 15:20:09","member_level_id":2},{"member_id":7984,"member_create":"2020-05-30 08:52:31","member_level_id":3},{"member_id":3972,"member_create":"2020-05-30 18:21:49","member_level_id":0},{"member_id":7997,"member_create":"2020-05-31 09:47:02","member_level_id":2},{"member_id":6100,"member_create":"2020-06-04 15:36:23","member_level_id":5},{"member_id":8055,"member_create":"2020-06-07 18:08:12","member_level_id":2},{"member_id":7410,"member_create":"2020-06-08 12:31:10","member_level_id":2},{"member_id":8083,"member_create":"2020-06-12 10:00:03","member_level_id":2},{"member_id":8085,"member_create":"2020-06-12 13:35:52","member_level_id":2},{"member_id":7792,"member_create":"2020-06-16 12:06:07","member_level_id":2},{"member_id":8121,"member_create":"2020-06-17 10:29:55","member_level_id":3},{"member_id":4413,"member_create":"2020-06-19 14:42:34","member_level_id":2},{"member_id":8059,"member_create":"2020-06-20 12:25:08","member_level_id":2},{"member_id":5521,"member_create":"2020-06-20 15:55:35","member_level_id":2},{"member_id":8191,"member_create":"2020-06-27 08:55:29","member_level_id":3},{"member_id":8204,"member_create":"2020-06-27 17:28:20","member_level_id":3},{"member_id":8206,"member_create":"2020-06-28 14:28:17","member_level_id":3},{"member_id":5709,"member_create":"2020-06-30 16:22:32","member_level_id":2},{"member_id":8248,"member_create":"2020-07-03 12:34:21","member_level_id":3},{"member_id":8255,"member_create":"2020-07-04 09:35:26","member_level_id":3},{"member_id":8260,"member_create":"2020-07-04 16:04:12","member_level_id":2},{"member_id":8279,"member_create":"2020-07-07 08:56:22","member_level_id":3},{"member_id":7831,"member_create":"2020-07-09 13:50:49","member_level_id":2},{"member_id":7858,"member_create":"2020-07-14 12:15:12","member_level_id":2},{"member_id":950,"member_create":"2020-07-14 13:59:14","member_level_id":2},{"member_id":5938,"member_create":"2020-07-17 08:35:24","member_level_id":3},{"member_id":8353,"member_create":"2020-07-17 09:04:12","member_level_id":2},{"member_id":8369,"member_create":"2020-07-18 14:42:22","member_level_id":4},{"member_id":8407,"member_create":"2020-07-22 16:22:11","member_level_id":3},{"member_id":1147,"member_create":"2020-07-28 13:10:46","member_level_id":3},{"member_id":8040,"member_create":"2020-07-30 15:42:13","member_level_id":2},{"member_id":6701,"member_create":"2020-08-15 15:48:31","member_level_id":2},{"member_id":8591,"member_create":"2020-08-17 11:39:13","member_level_id":0},{"member_id":8613,"member_create":"2020-08-20 10:01:12","member_level_id":2},{"member_id":8633,"member_create":"2020-08-21 13:39:01","member_level_id":2},{"member_id":8653,"member_create":"2020-09-01 17:15:30","member_level_id":2},{"member_id":8692,"member_create":"2020-09-01 18:12:46","member_level_id":2},{"member_id":8734,"member_create":"2020-09-06 17:49:11","member_level_id":4},{"member_id":2747,"member_create":"2020-09-12 12:49:05","member_level_id":2},{"member_id":8785,"member_create":"2020-09-13 08:57:40","member_level_id":2},{"member_id":8791,"member_create":"2020-09-13 14:38:18","member_level_id":3},{"member_id":4207,"member_create":"2020-09-14 16:29:47","member_level_id":3},{"member_id":8317,"member_create":"2020-09-17 18:45:07","member_level_id":3},{"member_id":8640,"member_create":"2020-09-21 16:32:08","member_level_id":2},{"member_id":8864,"member_create":"2020-09-21 17:04:07","member_level_id":3},{"member_id":8878,"member_create":"2020-09-24 07:38:37","member_level_id":2},{"member_id":8893,"member_create":"2020-09-26 13:37:18","member_level_id":3},{"member_id":7281,"member_create":"2020-09-26 13:46:59","member_level_id":2},{"member_id":8901,"member_create":"2020-09-27 14:42:47","member_level_id":3},{"member_id":3367,"member_create":"2020-09-28 14:03:40","member_level_id":3},{"member_id":8687,"member_create":"2020-09-29 16:10:18","member_level_id":3},{"member_id":8934,"member_create":"2020-09-30 15:23:27","member_level_id":2},{"member_id":9006,"member_create":"2020-10-07 11:06:23","member_level_id":2},{"member_id":9019,"member_create":"2020-10-08 09:03:57","member_level_id":2},{"member_id":9038,"member_create":"2020-10-10 15:51:33","member_level_id":2},{"member_id":3970,"member_create":"2020-10-12 10:45:23","member_level_id":3},{"member_id":9058,"member_create":"2020-10-14 15:03:49","member_level_id":2},{"member_id":7271,"member_create":"2020-10-18 13:19:37","member_level_id":2},{"member_id":8840,"member_create":"2020-10-20 16:30:54","member_level_id":2},{"member_id":9170,"member_create":"2020-10-29 08:27:20","member_level_id":2},{"member_id":9062,"member_create":"2020-10-29 09:50:49","member_level_id":2},{"member_id":9092,"member_create":"2020-11-01 11:18:42","member_level_id":2},{"member_id":9198,"member_create":"2020-11-01 15:13:53","member_level_id":3},{"member_id":9160,"member_create":"2020-11-08 19:34:37","member_level_id":3},{"member_id":9143,"member_create":"2020-11-11 14:33:40","member_level_id":2},{"member_id":3620,"member_create":"2020-11-13 17:10:11","member_level_id":2},{"member_id":9296,"member_create":"2020-11-13 18:06:02","member_level_id":2},{"member_id":9341,"member_create":"2020-11-22 12:38:47","member_level_id":2},{"member_id":8602,"member_create":"2020-11-24 11:59:29","member_level_id":3},{"member_id":9371,"member_create":"2020-11-27 16:04:37","member_level_id":3},{"member_id":7032,"member_create":"2020-11-28 10:20:55","member_level_id":2},{"member_id":9395,"member_create":"2020-12-01 12:12:07","member_level_id":2},{"member_id":9452,"member_create":"2020-12-06 10:03:09","member_level_id":2},{"member_id":9457,"member_create":"2020-12-06 16:19:18","member_level_id":2},{"member_id":9477,"member_create":"2020-12-09 13:23:53","member_level_id":2},{"member_id":9525,"member_create":"2020-12-15 07:47:43","member_level_id":2},{"member_id":9541,"member_create":"2020-12-16 17:24:54","member_level_id":0},{"member_id":3430,"member_create":"2020-12-17 09:05:44","member_level_id":6},{"member_id":9562,"member_create":"2020-12-17 16:54:30","member_level_id":2},{"member_id":9607,"member_create":"2020-12-20 11:51:46","member_level_id":2},{"member_id":9619,"member_create":"2020-12-20 15:41:21","member_level_id":2},{"member_id":9674,"member_create":"2020-12-25 16:32:01","member_level_id":2},{"member_id":3571,"member_create":"2020-12-27 15:25:31","member_level_id":2},{"member_id":9737,"member_create":"2021-01-02 12:34:54","member_level_id":3},{"member_id":9203,"member_create":"2021-01-05 10:03:39","member_level_id":2},{"member_id":2934,"member_create":"2021-01-07 10:18:35","member_level_id":2},{"member_id":825,"member_create":"2021-01-08 10:41:14","member_level_id":2},{"member_id":9627,"member_create":"2021-01-08 13:13:50","member_level_id":2},{"member_id":9821,"member_create":"2021-01-12 13:42:00","member_level_id":2},{"member_id":9972,"member_create":"2021-01-30 16:15:26","member_level_id":0},{"member_id":9250,"member_create":"2021-02-03 09:37:47","member_level_id":3},{"member_id":10066,"member_create":"2021-02-22 11:38:44","member_level_id":2},{"member_id":10127,"member_create":"2021-03-04 10:35:58","member_level_id":0},{"member_id":10539,"member_create":"2021-04-11 00:28:08","member_level_id":0},{"member_id":10567,"member_create":"2021-04-12 18:25:58","member_level_id":0},{"member_id":10570,"member_create":"2021-04-13 10:21:43","member_level_id":0},{"member_id":10586,"member_create":"2021-04-13 21:09:11","member_level_id":0},{"member_id":10599,"member_create":"2021-04-14 12:27:21","member_level_id":0},{"member_id":10602,"member_create":"2021-04-14 13:48:59","member_level_id":0},{"member_id":10619,"member_create":"2021-04-15 15:37:23","member_level_id":0},{"member_id":10633,"member_create":"2021-04-16 14:18:58","member_level_id":0},{"member_id":10723,"member_create":"2021-04-24 17:44:32","member_level_id":0},{"member_id":10750,"member_create":"2021-04-28 13:49:30","member_level_id":0},{"member_id":10461,"member_create":"2021-05-04 15:09:07","member_level_id":0},{"member_id":3972,"member_create":"2021-05-08 14:23:02","member_level_id":0},{"member_id":10062,"member_create":"2021-05-08 15:11:47","member_level_id":0},{"member_id":10891,"member_create":"2021-05-13 16:46:34","member_level_id":0},{"member_id":10892,"member_create":"2021-05-13 17:48:53","member_level_id":0},{"member_id":11077,"member_create":"2021-06-08 17:20:33","member_level_id":0},{"member_id":11082,"member_create":"2021-06-09 13:07:17","member_level_id":0},{"member_id":11113,"member_create":"2021-06-15 16:52:47","member_level_id":0},{"member_id":11126,"member_create":"2021-06-18 08:06:20","member_level_id":0},{"member_id":11265,"member_create":"2021-07-02 10:54:36","member_level_id":0},{"member_id":11207,"member_create":"2021-07-06 13:00:49","member_level_id":0},{"member_id":11340,"member_create":"2021-07-07 15:28:18","member_level_id":3},{"member_id":11350,"member_create":"2021-07-08 12:12:03","member_level_id":0},{"member_id":11367,"member_create":"2021-07-09 09:48:47","member_level_id":0},{"member_id":11374,"member_create":"2021-07-10 08:43:02","member_level_id":0},{"member_id":11376,"member_create":"2021-07-10 09:11:33","member_level_id":0},{"member_id":11379,"member_create":"2021-07-10 11:01:23","member_level_id":0},{"member_id":11380,"member_create":"2021-07-10 11:31:54","member_level_id":0},{"member_id":11413,"member_create":"2021-07-11 11:20:31","member_level_id":0},{"member_id":11416,"member_create":"2021-07-11 12:51:04","member_level_id":0},{"member_id":11419,"member_create":"2021-07-11 14:04:25","member_level_id":0},{"member_id":11425,"member_create":"2021-07-11 16:51:32","member_level_id":0},{"member_id":11437,"member_create":"2021-07-13 10:13:34","member_level_id":0},{"member_id":11479,"member_create":"2021-07-16 09:13:54","member_level_id":0},{"member_id":11481,"member_create":"2021-07-16 10:29:31","member_level_id":0},{"member_id":11482,"member_create":"2021-07-16 12:31:05","member_level_id":0},{"member_id":11489,"member_create":"2021-07-16 15:58:15","member_level_id":0},{"member_id":11499,"member_create":"2021-07-17 10:48:55","member_level_id":0},{"member_id":11301,"member_create":"2021-07-19 09:13:30","member_level_id":0},{"member_id":11308,"member_create":"2021-07-19 14:55:26","member_level_id":0},{"member_id":11551,"member_create":"2021-07-20 16:05:00","member_level_id":0},{"member_id":11555,"member_create":"2021-07-21 08:34:22","member_level_id":0},{"member_id":11565,"member_create":"2021-07-21 12:42:54","member_level_id":0},{"member_id":11567,"member_create":"2021-07-21 15:49:21","member_level_id":0},{"member_id":11572,"member_create":"2021-07-22 08:03:34","member_level_id":0},{"member_id":11575,"member_create":"2021-07-22 09:59:34","member_level_id":0},{"member_id":11299,"member_create":"2021-07-23 13:13:57","member_level_id":0},{"member_id":11606,"member_create":"2021-07-24 13:13:49","member_level_id":0},{"member_id":11627,"member_create":"2021-07-25 15:23:01","member_level_id":0},{"member_id":11637,"member_create":"2021-07-26 08:48:30","member_level_id":0},{"member_id":11116,"member_create":"2021-07-26 12:03:52","member_level_id":0},{"member_id":11485,"member_create":"2021-07-26 17:01:57","member_level_id":0},{"member_id":10879,"member_create":"2021-07-27 16:40:03","member_level_id":0},{"member_id":11661,"member_create":"2021-07-28 08:20:06","member_level_id":0},{"member_id":11665,"member_create":"2021-07-28 10:14:29","member_level_id":2},{"member_id":11666,"member_create":"2021-07-28 10:43:07","member_level_id":0},{"member_id":11667,"member_create":"2021-07-28 14:08:29","member_level_id":0},{"member_id":11675,"member_create":"2021-07-29 11:12:31","member_level_id":0},{"member_id":11677,"member_create":"2021-07-29 11:39:01","member_level_id":0},{"member_id":11685,"member_create":"2021-07-31 15:23:35","member_level_id":0},{"member_id":11701,"member_create":"2021-08-02 16:48:42","member_level_id":0},{"member_id":11709,"member_create":"2021-08-03 14:21:21","member_level_id":0},{"member_id":11712,"member_create":"2021-08-03 16:22:46","member_level_id":0},{"member_id":11714,"member_create":"2021-08-03 17:12:05","member_level_id":0},{"member_id":11715,"member_create":"2021-08-03 17:14:43","member_level_id":0},{"member_id":11716,"member_create":"2021-08-03 17:25:29","member_level_id":0},{"member_id":11721,"member_create":"2021-08-04 11:35:42","member_level_id":0},{"member_id":11723,"member_create":"2021-08-04 12:21:33","member_level_id":0},{"member_id":11725,"member_create":"2021-08-04 13:11:45","member_level_id":3},{"member_id":11728,"member_create":"2021-08-04 15:17:28","member_level_id":0},{"member_id":11729,"member_create":"2021-08-04 15:36:09","member_level_id":0},{"member_id":11731,"member_create":"2021-08-04 16:24:43","member_level_id":0},{"member_id":11732,"member_create":"2021-08-04 16:38:41","member_level_id":0},{"member_id":11740,"member_create":"2021-08-05 12:55:07","member_level_id":0},{"member_id":11741,"member_create":"2021-08-05 13:00:41","member_level_id":0},{"member_id":11321,"member_create":"2021-08-06 08:43:09","member_level_id":2},{"member_id":11749,"member_create":"2021-08-06 09:46:41","member_level_id":0},{"member_id":11750,"member_create":"2021-08-06 09:49:40","member_level_id":0},{"member_id":11752,"member_create":"2021-08-06 09:59:51","member_level_id":0},{"member_id":11755,"member_create":"2021-08-06 11:19:15","member_level_id":3},{"member_id":11760,"member_create":"2021-08-06 13:09:08","member_level_id":0},{"member_id":11762,"member_create":"2021-08-06 14:13:05","member_level_id":0},{"member_id":11768,"member_create":"2021-08-06 16:39:51","member_level_id":0},{"member_id":11769,"member_create":"2021-08-06 16:54:18","member_level_id":0},{"member_id":11773,"member_create":"2021-08-07 07:48:13","member_level_id":0},{"member_id":11774,"member_create":"2021-08-07 08:42:22","member_level_id":0},{"member_id":11775,"member_create":"2021-08-07 09:18:50","member_level_id":0},{"member_id":11785,"member_create":"2021-08-07 14:55:04","member_level_id":0},{"member_id":11502,"member_create":"2021-08-08 08:39:30","member_level_id":0},{"member_id":11794,"member_create":"2021-08-08 10:07:41","member_level_id":0},{"member_id":11796,"member_create":"2021-08-08 10:42:28","member_level_id":0},{"member_id":11799,"member_create":"2021-08-08 12:38:35","member_level_id":0},{"member_id":11806,"member_create":"2021-08-08 16:53:58","member_level_id":0},{"member_id":11816,"member_create":"2021-08-09 10:46:56","member_level_id":0},{"member_id":11817,"member_create":"2021-08-09 10:53:53","member_level_id":0},{"member_id":11819,"member_create":"2021-08-09 14:13:47","member_level_id":0},{"member_id":11820,"member_create":"2021-08-09 14:27:41","member_level_id":0},{"member_id":11826,"member_create":"2021-08-09 17:02:46","member_level_id":0},{"member_id":11833,"member_create":"2021-08-10 11:41:44","member_level_id":0},{"member_id":11836,"member_create":"2021-08-10 12:50:06","member_level_id":0},{"member_id":11837,"member_create":"2021-08-10 13:51:22","member_level_id":0},{"member_id":11839,"member_create":"2021-08-10 15:31:15","member_level_id":0},{"member_id":9043,"member_create":"2021-08-10 16:43:03","member_level_id":0},{"member_id":11849,"member_create":"2021-08-11 08:54:46","member_level_id":0},{"member_id":11242,"member_create":"2021-08-11 13:31:19","member_level_id":0},{"member_id":11854,"member_create":"2021-08-11 14:41:19","member_level_id":0},{"member_id":11859,"member_create":"2021-08-11 19:39:48","member_level_id":0},{"member_id":11866,"member_create":"2021-08-12 09:52:55","member_level_id":0},{"member_id":11865,"member_create":"2021-08-12 09:54:14","member_level_id":0},{"member_id":11867,"member_create":"2021-08-12 10:02:02","member_level_id":0},{"member_id":11870,"member_create":"2021-08-12 10:38:31","member_level_id":0},{"member_id":11872,"member_create":"2021-08-12 10:55:44","member_level_id":0},{"member_id":11875,"member_create":"2021-08-12 11:26:14","member_level_id":0},{"member_id":11421,"member_create":"2021-08-12 12:29:26","member_level_id":0},{"member_id":11881,"member_create":"2021-08-12 13:40:11","member_level_id":0},{"member_id":11887,"member_create":"2021-08-12 15:52:37","member_level_id":0},{"member_id":11888,"member_create":"2021-08-12 17:09:36","member_level_id":0},{"member_id":11893,"member_create":"2021-08-13 09:21:59","member_level_id":0},{"member_id":11900,"member_create":"2021-08-13 12:25:07","member_level_id":0},{"member_id":11902,"member_create":"2021-08-13 13:30:29","member_level_id":0},{"member_id":11904,"member_create":"2021-08-13 14:12:24","member_level_id":0},{"member_id":11909,"member_create":"2021-08-13 15:56:07","member_level_id":0},{"member_id":11911,"member_create":"2021-08-13 17:26:13","member_level_id":0},{"member_id":11915,"member_create":"2021-08-13 17:39:39","member_level_id":0},{"member_id":11918,"member_create":"2021-08-13 19:13:47","member_level_id":0},{"member_id":11921,"member_create":"2021-08-14 08:07:31","member_level_id":0},{"member_id":11923,"member_create":"2021-08-14 10:27:21","member_level_id":0},{"member_id":11928,"member_create":"2021-08-14 12:30:17","member_level_id":0},{"member_id":11930,"member_create":"2021-08-14 13:04:48","member_level_id":0},{"member_id":11935,"member_create":"2021-08-14 14:44:44","member_level_id":0},{"member_id":11937,"member_create":"2021-08-14 15:28:59","member_level_id":5},{"member_id":11938,"member_create":"2021-08-14 15:58:41","member_level_id":0},{"member_id":11947,"member_create":"2021-08-15 10:16:11","member_level_id":0},{"member_id":11951,"member_create":"2021-08-15 12:40:15","member_level_id":0},{"member_id":11957,"member_create":"2021-08-15 14:20:43","member_level_id":0},{"member_id":11958,"member_create":"2021-08-15 14:25:15","member_level_id":0},{"member_id":11959,"member_create":"2021-08-15 14:39:02","member_level_id":0},{"member_id":11961,"member_create":"2021-08-15 15:45:09","member_level_id":0},{"member_id":11962,"member_create":"2021-08-15 15:51:15","member_level_id":0},{"member_id":11981,"member_create":"2021-08-16 15:52:43","member_level_id":0},{"member_id":11985,"member_create":"2021-08-17 08:18:52","member_level_id":0},{"member_id":11998,"member_create":"2021-08-18 10:43:09","member_level_id":0},{"member_id":11960,"member_create":"2021-08-18 15:08:42","member_level_id":0},{"member_id":12006,"member_create":"2021-08-18 15:51:41","member_level_id":0},{"member_id":12015,"member_create":"2021-08-19 14:17:12","member_level_id":0},{"member_id":12016,"member_create":"2021-08-19 14:40:04","member_level_id":0},{"member_id":12036,"member_create":"2021-08-21 08:50:13","member_level_id":0},{"member_id":12042,"member_create":"2021-08-21 13:03:51","member_level_id":0},{"member_id":11972,"member_create":"2021-08-21 14:13:42","member_level_id":0},{"member_id":12049,"member_create":"2021-08-21 15:18:03","member_level_id":0},{"member_id":11878,"member_create":"2021-08-21 16:27:37","member_level_id":0},{"member_id":12050,"member_create":"2021-08-21 16:29:40","member_level_id":0},{"member_id":12057,"member_create":"2021-08-21 17:21:18","member_level_id":0},{"member_id":12058,"member_create":"2021-08-21 17:44:33","member_level_id":0},{"member_id":12061,"member_create":"2021-08-21 18:31:04","member_level_id":0},{"member_id":12062,"member_create":"2021-08-22 09:01:54","member_level_id":0},{"member_id":12056,"member_create":"2021-08-22 10:02:09","member_level_id":0},{"member_id":12065,"member_create":"2021-08-22 10:37:43","member_level_id":0},{"member_id":12068,"member_create":"2021-08-22 11:27:17","member_level_id":2},{"member_id":12001,"member_create":"2021-08-22 17:42:42","member_level_id":0},{"member_id":12089,"member_create":"2021-08-23 12:24:15","member_level_id":0},{"member_id":12091,"member_create":"2021-08-23 13:06:50","member_level_id":0},{"member_id":12097,"member_create":"2021-08-23 14:00:12","member_level_id":0},{"member_id":12094,"member_create":"2021-08-23 14:02:42","member_level_id":0},{"member_id":12099,"member_create":"2021-08-23 14:17:51","member_level_id":0},{"member_id":12101,"member_create":"2021-08-23 15:11:28","member_level_id":0},{"member_id":12104,"member_create":"2021-08-23 16:08:10","member_level_id":0},{"member_id":12110,"member_create":"2021-08-24 10:56:24","member_level_id":0},{"member_id":12111,"member_create":"2021-08-24 11:08:14","member_level_id":0},{"member_id":12122,"member_create":"2021-08-25 09:17:02","member_level_id":0},{"member_id":12123,"member_create":"2021-08-25 09:30:16","member_level_id":0},{"member_id":12126,"member_create":"2021-08-25 09:53:19","member_level_id":0},{"member_id":12127,"member_create":"2021-08-25 09:55:55","member_level_id":0},{"member_id":12148,"member_create":"2021-08-26 08:58:57","member_level_id":0},{"member_id":12149,"member_create":"2021-08-26 09:50:09","member_level_id":0},{"member_id":12151,"member_create":"2021-08-26 09:59:43","member_level_id":0},{"member_id":12154,"member_create":"2021-08-26 10:37:09","member_level_id":0},{"member_id":12156,"member_create":"2021-08-26 10:39:49","member_level_id":0},{"member_id":12166,"member_create":"2021-08-26 13:23:33","member_level_id":0},{"member_id":12167,"member_create":"2021-08-26 13:35:28","member_level_id":0},{"member_id":12169,"member_create":"2021-08-26 14:16:50","member_level_id":0},{"member_id":12172,"member_create":"2021-08-26 14:31:44","member_level_id":0},{"member_id":12173,"member_create":"2021-08-26 15:01:48","member_level_id":0},{"member_id":12175,"member_create":"2021-08-26 15:19:17","member_level_id":0},{"member_id":12188,"member_create":"2021-08-27 08:20:37","member_level_id":0},{"member_id":12190,"member_create":"2021-08-27 08:56:15","member_level_id":0},{"member_id":12192,"member_create":"2021-08-27 09:00:44","member_level_id":0},{"member_id":12195,"member_create":"2021-08-27 09:19:17","member_level_id":0},{"member_id":12202,"member_create":"2021-08-27 09:46:37","member_level_id":0},{"member_id":12205,"member_create":"2021-08-27 10:26:35","member_level_id":0},{"member_id":12207,"member_create":"2021-08-27 10:44:16","member_level_id":0},{"member_id":12209,"member_create":"2021-08-27 10:49:09","member_level_id":0},{"member_id":12213,"member_create":"2021-08-27 10:57:36","member_level_id":0},{"member_id":12212,"member_create":"2021-08-27 11:13:59","member_level_id":0},{"member_id":12216,"member_create":"2021-08-27 11:39:32","member_level_id":0},{"member_id":12221,"member_create":"2021-08-27 12:52:18","member_level_id":0},{"member_id":11596,"member_create":"2021-08-27 14:07:09","member_level_id":2},{"member_id":12225,"member_create":"2021-08-27 14:29:07","member_level_id":0},{"member_id":12177,"member_create":"2021-08-27 16:23:57","member_level_id":0},{"member_id":12233,"member_create":"2021-08-27 16:25:41","member_level_id":0},{"member_id":11492,"member_create":"2021-08-27 16:34:00","member_level_id":0},{"member_id":12244,"member_create":"2021-08-28 08:25:52","member_level_id":0},{"member_id":12248,"member_create":"2021-08-28 08:54:35","member_level_id":0},{"member_id":12251,"member_create":"2021-08-28 10:32:35","member_level_id":0},{"member_id":12253,"member_create":"2021-08-28 10:51:17","member_level_id":0},{"member_id":12254,"member_create":"2021-08-28 11:08:02","member_level_id":0},{"member_id":12257,"member_create":"2021-08-28 11:27:15","member_level_id":0},{"member_id":12258,"member_create":"2021-08-28 11:48:45","member_level_id":0},{"member_id":12260,"member_create":"2021-08-28 11:54:01","member_level_id":0},{"member_id":12150,"member_create":"2021-08-28 12:34:46","member_level_id":0},{"member_id":12266,"member_create":"2021-08-28 13:27:13","member_level_id":0},{"member_id":12268,"member_create":"2021-08-28 14:07:53","member_level_id":0},{"member_id":12269,"member_create":"2021-08-28 14:08:32","member_level_id":0},{"member_id":12271,"member_create":"2021-08-28 15:34:59","member_level_id":0},{"member_id":12274,"member_create":"2021-08-29 07:54:33","member_level_id":0},{"member_id":12277,"member_create":"2021-08-29 09:16:16","member_level_id":0},{"member_id":12278,"member_create":"2021-08-29 09:18:47","member_level_id":0},{"member_id":12279,"member_create":"2021-08-29 09:27:17","member_level_id":0},{"member_id":12280,"member_create":"2021-08-29 09:35:26","member_level_id":0},{"member_id":12281,"member_create":"2021-08-29 10:37:11","member_level_id":0},{"member_id":12284,"member_create":"2021-08-29 11:23:03","member_level_id":0},{"member_id":12239,"member_create":"2021-08-29 11:29:43","member_level_id":0},{"member_id":12282,"member_create":"2021-08-29 12:24:48","member_level_id":0},{"member_id":12291,"member_create":"2021-08-29 12:35:51","member_level_id":0},{"member_id":12293,"member_create":"2021-08-29 13:04:19","member_level_id":0},{"member_id":12296,"member_create":"2021-08-29 13:55:26","member_level_id":0},{"member_id":12298,"member_create":"2021-08-29 14:11:28","member_level_id":0},{"member_id":12303,"member_create":"2021-08-29 14:51:50","member_level_id":0},{"member_id":12305,"member_create":"2021-08-29 15:06:58","member_level_id":0},{"member_id":12313,"member_create":"2021-08-29 18:31:54","member_level_id":0},{"member_id":12314,"member_create":"2021-08-29 19:18:42","member_level_id":0},{"member_id":12316,"member_create":"2021-08-30 08:45:39","member_level_id":0},{"member_id":12317,"member_create":"2021-08-30 08:58:31","member_level_id":0},{"member_id":12320,"member_create":"2021-08-30 10:11:04","member_level_id":0},{"member_id":11778,"member_create":"2021-08-30 14:00:24","member_level_id":0},{"member_id":12330,"member_create":"2021-08-30 14:09:34","member_level_id":0},{"member_id":12334,"member_create":"2021-08-30 15:08:53","member_level_id":0},{"member_id":12335,"member_create":"2021-08-30 15:17:02","member_level_id":0},{"member_id":12336,"member_create":"2021-08-30 15:24:17","member_level_id":0},{"member_id":12339,"member_create":"2021-08-30 15:55:01","member_level_id":2},{"member_id":12340,"member_create":"2021-08-30 16:10:56","member_level_id":0},{"member_id":12347,"member_create":"2021-08-31 11:26:10","member_level_id":0},{"member_id":12357,"member_create":"2021-08-31 14:23:15","member_level_id":0},{"member_id":12351,"member_create":"2021-08-31 14:37:45","member_level_id":0},{"member_id":12361,"member_create":"2021-08-31 15:45:22","member_level_id":0},{"member_id":12362,"member_create":"2021-08-31 16:03:01","member_level_id":0},{"member_id":12373,"member_create":"2021-09-01 09:10:56","member_level_id":0},{"member_id":12375,"member_create":"2021-09-01 09:13:43","member_level_id":0},{"member_id":11761,"member_create":"2021-09-01 11:57:32","member_level_id":0},{"member_id":12379,"member_create":"2021-09-01 11:58:45","member_level_id":0},{"member_id":12380,"member_create":"2021-09-01 12:26:04","member_level_id":0},{"member_id":12386,"member_create":"2021-09-01 14:25:10","member_level_id":0},{"member_id":12391,"member_create":"2021-09-01 15:47:03","member_level_id":0},{"member_id":11903,"member_create":"2021-09-01 16:44:46","member_level_id":0},{"member_id":12395,"member_create":"2021-09-01 17:30:50","member_level_id":0}]';
        $info = json_decode($info, true);
        $info = array_filter($info, function ($item) {
            if ($item['member_level_id'] == 0) {
                return false;
            } else {
                return true;
            }
        });
        dump($info);
        $member_id = implode(',', array_column($info, 'member_id'));
        dump($member_id);
        foreach ($info as $k => $v) {
            Db::table('member_mapping')->where(array('member_id' => $v['member_id']))->update(array('member_level_id' => $v['member_level_id']));
        }
        dump('执行成功了');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 会员升级成功
     */
    function upgradeSuccess()
    {
        $pay_price = input("post.pay_price");
        $pay_type = input("post.pay_type");
        $upgradeId = input("post.upgradeId");
        $up_level = input("post.up_level");
        $order_number = input("post.order_number");
        $employee_id = input("post.employee_id");
        $assign_id = input("post.assign_id");
        $formMark = input('post.formMark');
        $member_id = input("post.member_id");
        $BalanceService = new BalanceService();

        # 查询用户信息
        $memberInfo = Db::table('member_mapping')->field('member_level_id,member_expiration')->where(array('member_id' => $member_id))->find();

        if ($memberInfo['member_level_id'] == $up_level) {
            return array("status" => true);
        }

        $BalanceService->memberUpgrade($member_id, $pay_price, $pay_type, 1, 1, $upgradeId, $up_level, $order_number, $assign_id, $employee_id, '', $formMark);
        return array("status" => true);
    }

    function makeAppointOrders()
    {
        $data = array(
            'serviceInfo' =>
                array(
                    'id' => 9,
                    'service_title' => '汽车普洗服务',
                    'service_class_id' => 5,
                    'service_time' => 30,
                    'service_picurl' =>
                        array(
                            0 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348811896lbBvuhgkRW.jpg',
                            1 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/16173488175478r4kT3tB7r.jpg',
                        ),
                    'service_image' => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348789284cxOKidxffD.jpg',
                    'service_keyborder' =>
                        array(
                            0 => '洗车',
                            1 => '清洁',
                            2 => '刷车',
                        ),
                    'service_statement' => '预约门店服务需提前半小时操作，预约时间并非服务时间，预约成功后需到店根据实际情况进行服务！',
                    'team_id' => NULL,
                    'service_evaluation' => 99,
                    'service_status' => 1,
                    'service_top' => 1,
                    'service_order' => 0,
                    'is_active' => NULL,
                    'service_context' =>
                        array(
                            0 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348831182VkwpaR9Zgt.jpeg',
                            1 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348835818cY3xOfeIoe.jpg',
                            2 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348839532w1Cwnb4h0f.jpeg',
                            3 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210402/1617348841940pVe6UtiphS.jpg',
                        ),
                    'service_create' => '2021-01-05 08:57:35',
                    'service_update' => '2021-07-05 15:57:41',
                    'payment_mark' => NULL,
                    'update_status' => 1,
                    'service_click' => 0,
                    'service_refer' => 0,
                    'service_minprice' => 15,
                    'service_commission' => 0.15,
                    'service_wxpay' => 1,
                    'timer' => 1,
                    'iscut' => 0,
                    'shortcuts' => 2,
                    'appoint' => 1,
                    'settlement_min' => 0,
                    'is_del' => 2,
                    'is_relation' => 2,
                    'service_advert' => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210105/1609808190973sTdPcYzeQ2.jpg',
                    'service_pid' => NULL,
                    'class_pid' => 2,
                    'discount_type' => 4,
                    'real_num' => 3301,
                    'repeat_order' => 2,
                    'is_parts' => 1,
                    'is_maintain' => 2,
                    'is_repair' => 2,
                    'is_warranty' => 2,
                    'is_display' => 1,
                    'repeat_cycle' => 2,
                    'relation_eva' => 1,
                    'service_number' => '001',
                    'offline' => '28.00~48.00',
                    'online' => '22.00~38.00',
                    'member_price' => '8.80~30.40',
                    'original_price' => '22.00',
                    'pay_price' => '22.00',
                    'collection' => 1,
                ),
            'carInfo' =>
                array(
                    'id' => 11581,
                    'car_licens' => '辽HJJ865',
                    'car_recogin_code' => '',
                    'car_engine_number' => '',
                    'register_time' => NULL,
                    'grant_register' => NULL,
                    'car_type_id' => 319,
                    'car_mileage' => '0',
                    'member_id' => 11241,
                    'status' => 1,
                    'enginecode' => '',
                    'create_time' => '2021-07-06 14:40:04',
                    'queue_status' => 2,
                    'owner_name' => '姐',
                    'car_sort_id' => 319,
                    'car_logo_id' => 29,
                    'sort_title' => '锋范',
                    'car_level' => '1',
                    'logo_title' => '广汽本田',
                    'level_title' => '小型车',
                    'car_num' => '辽H·JJ865',
                ),
            'bizInfo' =>
                array(
                    'id' => 429,
                    'biz_title' => '铭赫汽车服务中心',
                    'address' => '营口市站前区市医院东门斜对面医药公司院内',
                    'biz_phone' => '15041728000',
                    'biz_img' => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210628/1624870326909bnm46PkEgV.jpg',
                    'biz_head' =>
                        array(
                            0 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210628/1624870352140oKzZk7eNrk.jpg',
                            1 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210628/1624870355517FyJKvXsDt4.jpg',
                            2 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210628/1624870363255PYR5ZKYmpm.jpg',
                            3 => 'http://ecars.oss-cn-beijing.aliyuncs.com/20210628/1624870367268nvMC6qOAo1.jpg',
                        ),
                    'biz_desc' => NULL,
                    'biz_lat' => 40.660662,
                    'biz_lon' => 122.233184,
                    'recommend_status' => 1,
                    'biz_tip' =>
                        array(
                            0 => '洗车美容',
                            1 => '维修养护',
                            2 => '装饰改装',
                            3 => '钣金喷漆',
                        ),
                    'biz_type' => '合作店',
                    'biz_opening' => '07:30',
                    'biz_closing' => '17:30',
                    'open_state' => 1,
                    'distance' => 0,
                    'open_state_title' => '营业中',
                    'opening_status' => 1,
                ),
            'pay_type' => 1,
            'pay_price' => '10.80',
            'voucherInfo' =>
                array(
                    'id' => 12689,
                    'voucher_id' => 0,
                    'member_id' => 11241,
                    'create' => '2021-09-29 12:47:05',
                    'status' => 1,
                    'payprice' => 0,
                    'voucher_source' => 12,
                    'paytype' => 0,
                    'car_type_id' => 0,
                    'redpacket_id' => 123,
                    'voucher_cost' => 11.2,
                    'card_title' => '汽车普洗服务抵用券',
                    'card_time' => 90,
                    'server_id' => 9,
                    'start_time' => '2021-07-01 12:47:05',
                    'voucher_type' => 4,
                    'money_off' => 10,
                    'voucher_start' => 1,
                    'merchants_id' => 0,
                    'order_id' => 0,
                    'order_server_id' => 0,
                    'workorder_type' => 1,
                    'is_type' => 0,
                    'cate_pid' => 0,
                    'settlement_price' => NULL,
                    'expiration_time' => '2021-09-29',
                    'TypeTitle' => '抵用券',
                    'voucherDesc' =>
                        array(
                            0 => '该卡券只能使用一次，过期后不可使用',
                            1 => '满10.00可使用',
                        ),
                    'voucher_source_title' => '平台赠送',
                ),
            'appoint_time' => '2021-08-05 17:50',
            'already' => 2,
            'already_order_number' => '',
        );
    }

    function voucherDemo()
    {
        Db::table("member_voucher")->where(array("voucher_type" => 9))->update(array("voucher_type" => 7));
        var_dump(123);
    }

    function share_id()
    {
        $share_id = '';
        $client_token_url = 'https://open.douyin.com/oauth/client_token/';
        $order = [
            'client_key' => 'awdb3q3lasnoi3qd',
            'client_secret' => '7b9f6931c256c73167d634c933b33551',
            'grant_type' => 'client_credential'
        ];
        $curl = new Curl();
        $res = $curl->post($client_token_url, $order, 'form');
        $resData = json_decode($res, true);
        if (!empty($resData)) {
            if ($resData['data']['error_code'] == 0) {
                $token = $resData['data']['access_token'];
                $url = 'https://open.douyin.com/share-id/?access_token=' . $token . '&need_callback=true';
                $re = $curl->get($url);
                $reData = json_decode($re, true);
                if ($reData['data']['error_code'] == 0) {
                    $share_id = $reData['data']['share_id'];
                }
            }
        }
        return $share_id;
    }

    function addDou()
    {
        TimeTask::addDouData();
    }

    function shareVideo()
    {
        $share_id = '1713576483648552';
        $client_token_url = 'https://open.douyin.com/oauth/client_token/';
        $order = [
            'client_key' => 'awdb3q3lasnoi3qd',
            'client_secret' => '7b9f6931c256c73167d634c933b33551',
            'grant_type' => 'client_credential'
        ];
        $curl = new Curl();
        $res = $curl->post($client_token_url, $order, 'form');
        $resData = json_decode($res, true);
        if (!empty($resData)) {
            if ($resData['data']['error_code'] == 0) {
                $token = $resData['data']['access_token'];
                $url = 'https://open.douyin.com//video/data/';
                $re = $curl->post($url, array(
                    'open_id' => '',
                    'access_token' => ''
                ));
                $reData = json_decode($re, true);
                if ($reData['data']['error_code'] == 0) {
                    $share_id = $reData['data']['share_id'];
                }
            }
        }
    }

    function merchantsDemo()
    {
        $car_orders =  Db::table('orders')->where(array("member_id"=>9405))
            ->where("order_status in (1,5,6)")
            ->where("(select count(id) from order_biz where order_biz.order_number = orders.order_number and order_biz.custommark=1)>0
            or (select count(id) from order_server where order_server.order_number = orders.order_number and order_server.custommark=1)>0")
            ->count();
        $merchants_orders = Db::table("merchants_order")->where(array("member_id"=>9405))
            ->where("(actual_price>50 and (select count(id) from merchants_voucher_order where order_id = merchants_order.id)=0)
                        or (actual_price<=50)")
            ->where("order_status in (1,2) and pay_type in (1,2,3,4)")->count();

        dump($car_orders);
        dump($merchants_orders);
    }

    function agentDeduct()
    {
        EalspellCommissionService::merchantsCharge(10, 188, 1336);
        EalspellCommissionService::merchantsCharge(10, 188, 1337);
        var_dump(123);
    }

}
