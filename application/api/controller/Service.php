<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/26
 * Time: 15:49
 */

namespace app\api\controller;


use app\api\ApiService\MemberService;
use app\api\ApiService\orderService;
use app\api\ApiService\PriceService;
use app\api\ApiService\VoucherService;
use app\service\AdminService;
use Redis\Redis;
use think\Db;

class Service extends Common
{


    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 重新计算服务价格
     */
    function recountPrice()
    {
        # 判断支付方式
        $pay_type = input("post.pay_type");
        // 点击预约时候出现的弹窗中 展示的提示信息
        $sure_pop_tips = "您将通过平台余额进行支付您的预约订单";
        # 车辆信息
        $carInfo = input('post.carInfo');
        $car_level = input("post.car_level");
        $id = input("post.id");
        $biz_id = input('post.biz_id');
        # 平台服务 进行 周期重复下单判断
        $serviceInfo = Db::table('service')->field('repeat_cycle,biz_pid')->where(array('id' => $id))->find();
        if ($serviceInfo['biz_pid'] == 0) {
            $serviceCycle = $serviceInfo['repeat_cycle'];
            if ($serviceCycle > 0) {
                $carLicense = $carInfo['car_licens'];
                # 查询该车辆周期内是否服务过
                $serviceNum = Db::table('order_server')
                    ->where(array('status' => 6, 'custommark' => 1, 'server_id' => $id))
                    ->where("order_number in (select order_number from orders where car_liences = '" . $carLicense . "'
                 and order_create <='" . date('Y-m-d H:i:s') . "' and order_create >= '" . date('Y-m-d H:i:s', strtotime('- ' . $serviceCycle . ' days')) . "')")
                    ->count();
                if ($serviceNum > 0) {
                    return array('status' => false, 'msg' => '此服务已完成，不可重复下单！', 'sure_pop_tips' => $sure_pop_tips);
                }
            }
        }
        $PriceService = new PriceService();
        $price = $PriceService->servicePrice($id, $biz_id);
        if (!empty($car_level)) {
            # 余额支付的金额
            $online_price = $price['online'][$car_level];
            $show_price = $price['online'][$car_level];
            # 平台服务 余额支付打折
            if ($price['biz_pid'] == 0) {
                # 平台服务 走正常余额支付 不是会员,折扣按 初级体验的折扣 计算
                if ($this->MemberLevel == 0) {
                    $member_level_id = 1;
                } else {
                    $member_level_id = $this->MemberLevel;
                }
                $discount = $price['member_discount'][$member_level_id];
//                $online_price = $online_price * $discount;//余额支付
            } else {
                # 合作端上架服务 线上下单打折=0.9 门店下单不打折
                $discount = 0.9;
//                $online_price = $online_price * 0.9;//余额支付

            }
            $priceB = getsPriceFormat($online_price * $discount);
            $online_price = $online_price * $discount;
            $balance_discount = $discount * 10;
            $b_discount = $discount * 10;
            $wx_discount = 9;
            $zfb_discount = 9;
            if (!empty($pay_type)) {
                if ($pay_type != 5) {
                    # 判断是平台服务/合作端上架的服务
                    if ($price['biz_pid'] == 0) {
                        # 平台服务 不是余额支付 也打折  0.9折扣
                        $discount = 0.9;
                        $online_price = $price['online'][$car_level] * $discount;
                        $priceB = getsPriceFormat($price['online'][$car_level] * $discount);
                    }
                }
            }
            $noFunds_text = [
                '账户余额不足',
                "使用账户余额支付更方便",
                "额外有优惠赠送",
                "点击下方按钮立即前往充值"
            ];
            # 判断打完折的价格 是否小于 余额 大于余额不打折
            if (getsPriceFormat($priceB) > getsPriceFormat($this->MemberInfo['member_balance'])) {
//                $balance_discount = 0;
                if ($pay_type == 5) {
                    $online_price = $show_price;
                    $noFunds_text = [
                        '账户余额不足',
                        "使用账户余额支付更方便",
                        "额外有优惠赠送",
                        "点击下方按钮立即前往充值",
//                        '本次使用余额支付将享受'.$b_discount.'折优惠'
                    ];
                }
            }
            $redis = new Redis();
            $redis->hSetJson('recountPriceDiscount','m'.$this->MemberId,array(
                '1'=>$wx_discount/10,
                '2'=>$zfb_discount/10,
                '5'=>$balance_discount/10
            ));
            return array("status" => true, "price" => getsPriceFormat($online_price), 'show_price' => $show_price, 'sure_pop_tips' => $sure_pop_tips,
                'balance_discount' => $balance_discount, 'noFunds_text' => $noFunds_text, 'wx_discount' => $wx_discount, 'zfb_discount' => $zfb_discount);
        } else {
            return array("status" => false, "msg" => "车辆信息错误", 'sure_pop_tips' => $sure_pop_tips);
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 收藏/取消收藏服务
     */
    function serviceCollection()
    {
        $collection = input("post.collection");
        $service_id = input("post.service_id");
        if ($collection == 2) {
            # 收藏状态
            Db::table("collection")->insert(array("member_id" => $this->MemberId, "service_id" => $service_id, "collection_create" => date("Y-m-d H:i:s")));
        } else {
            # 取消收藏状态
            Db::table("collection")->where(array("member_id" => $this->MemberId, "service_id" => $service_id))->delete();
        }
        return array("status" => true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查询服务可用卡券
     */
    function serviceCanUseVoucher()
    {
//        if($this->MemberId==14285){
//            $this->MemberId = 13541;
//        }
        $service_id = input("post.service_id");
        $price = input("post.price");
        # 车辆信息
        $carInfo = input('post.carInfo');
        # 门店信息
        $bizInfo = input('post.bizInfo');
        # 查询服务原价
        if (!empty($carInfo)) {
            $carLevel = $carInfo['car_level'];
        } else {
            $carLevel = 1;
        }
        # 查询服务的原价
        $price = $_discount = Db::table("service_car_mediscount")->field(array("discount"))
            ->where(array("service_id" => $service_id, 'car_level_id' => $carLevel, 'service_type' => 2, 'biz_id' => $bizInfo['id']))
            ->value('discount');
        # 查询该车牌号今天是否使用了卡券( 用户端   合作端   相同一个车牌号一个合作店每天只能使用一个卡券)
        $useNum = Db::table('member_voucher')
            ->where("order_id in (select id from orders where biz_id = " . $bizInfo['id'] . " 
            and car_liences = '" . $carInfo['car_licens'] . "' and date(order_create) = '" . date('Y-m-d') . "')")
            ->where(array('status' => 2))
            ->find();
        if (!empty($useNum)) {
            return array("status" => true, "list" => array(), "num" => 0,'msg'=>'每天只能使用一张卡券');
        }
        $VoucherService = new VoucherService();
        $voucher = $VoucherService->memberVoucher($this->MemberId, array("service" => true, "service_id" => $service_id, "price" => $price));
        if (!empty($voucher['list'])) {

            $voucher=$VoucherService->voucherFormat($voucher,$service_id,true,$carInfo);

        }
        return array("status" => true, "list" => array_values($voucher['list']), "num" => $voucher['num']);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 生成预约订单数据
     */
    function makeAppointOrders()
    {
        $uplevel = input("post.uplevel");
        if (empty($uplevel)) {
            # 小年判断
            if (!voucherDeadLine()) {
                return array("status" => false, 'msg' => '春节期间暂不提供预约服务');
            }
        }

        $serviceInfo = input("post.serviceInfo/a");
        $carInfo = input("post.carInfo");
        $bizInfo = input("post.bizInfo");
        $pay_type = input("post.pay_type");
        $pay_price = input("post.pay_price");
        $voucherInfo = input("post.voucherInfo");
        $appoint_time = input("post.appoint_time");
        $already = input("post.already");
        $already_order_number = input("post.already_order_number");
        # 查询相同的车 相同预约时间当天是否还有其他预约订单
//        $appoint=Db::table("orders")->where(array("member_id"=>$this->MemberId,"order_status"=>1,"order_type"=>3,"car_liences"=>$carInfo['car_licens']))
//            ->where("date(order_appoin_time) = '".date("Y-m-d",strtotime($appoint_time))."'")->find();
//        if(!empty($appoint)){
//            return array("status"=>false,"msg"=>"该车辆已预约了当天的服务,不可多次预约");
//        }
        # 2022-01-04 诚信洗车厂不让预约
        if ($bizInfo['id'] == 692) {
            return array("status" => false, "msg" => "暂不支持预约服务");
        }
        $order_number = orderService::getOrderNumber($bizInfo['id']);
        $_order_server = array("server_id" => $serviceInfo['id'], "price" => $pay_price, "custommark" => 1, "price_lock" => 2, 'coupon_mark' => 1, 'original_price' => 0);
        if (!empty($voucherInfo)) {
            if ($voucherInfo['voucher_cost'] > 0) {
                $_order_server['coupon_mark'] = 2;
            }
        }
        $_order_server['coupon_price'] = $voucherInfo['voucher_cost'];
        $_order_server['original_price'] = $serviceInfo['original_price'];
        # 生成对应订单格式传给微信、支付宝 并在成功时返回的数据
        $_array = array("server_id" => $serviceInfo['id'], "member_id" => $this->MemberId, "order_number" => $order_number, "pay_type" => $pay_type, "orders_type" => 3,
            "pay_price" => $pay_price, "appoint_time" => $appoint_time, "biz_id" => $bizInfo['id'], "member_car_id" => $carInfo['id'], "car_liences" => $carInfo['car_licens'],
            "car_level" => $carInfo['car_level'], "car_logo" => $carInfo['car_logo_id'], "car_sort" => $carInfo['car_sort_id'], "voucher_cost" => $voucherInfo['voucher_cost'],
            "voucher_id" => $voucherInfo['id'], "order_server" => array($_order_server), "already" => $already, "already_order_number" => $already_order_number);
        # 预约订单支付状态缓存
        $redis = new Redis();
        $payArr = array(
            'info' => $_array,
            'type' => 'Reservation',
            'pay_time' => date('Y-m-d H:i:s')
        );
        $redis->hSetJson('payRedis', $order_number, $payArr);
        return array("status" => true, "order_number" => $order_number, "info" => json_encode($_array));
    }


}
