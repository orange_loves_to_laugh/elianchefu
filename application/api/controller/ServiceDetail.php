<?php


namespace app\api\controller;


use app\api\ApiService\MemberService;
use app\api\ApiService\PriceService;
use Redis\Redis;
use think\Controller;
use think\Db;

class ServiceDetail extends Controller
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务详情
     */
    function serviceDetail()
    {
        $car_level = input("post.car_level");
        $member_id = input("post.member_id");
        $id = input("post.id");

        $PriceService = new PriceService();
        # 获取服务详情信息
        $info = Db::table("service")->where(array("id" => $id))->find();
        $price = $PriceService->servicePrice($id);

        if (!empty($price['offline'])) {
            if (min($price['offline']) == max($price['offline'])) {
                $info['offline'] = getsPriceFormat(min($price['offline']));
            } else {
                $info['offline'] = getsPriceFormat(min($price['offline'])) . "~" . getsPriceFormat(max($price['offline']));
            }
        } else {
            $info['offline'] = '0.00';
        }
        if (!empty($price['online'])) {
            if (min($price['online']) == max($price['online'])) {
                $info['online'] = getsPriceFormat(min($price['online']));
            } else {
                $info['online'] = getsPriceFormat(min($price['online'])) . "~" . getsPriceFormat(max($price['online']));
            }
        } else {
            $info['online'] = '0.00';
        }
        if (!empty($price['online'])) {

            if ((min($price['online']) * min($price['member_discount'])) == (max($price['online']) * max($price['member_discount']))) {
                $info['member_price'] = getsPriceFormat(min($price['online']) * min($price['member_discount']));
            } else {
                $info['member_price'] = getsPriceFormat(min($price['online']) * min($price['member_discount'])) . "~" . getsPriceFormat(max($price['online']) * max($price['member_discount']));
            }

        } else {
            $info['member_price'] = '0.00';
        }
        if (!empty($member_id)) {
            $MemberService = new MemberService();
            $memberInfo = $MemberService->MemberInfoCache($member_id);
            if (!empty($car_level)) {
                $online_price = $price['online'][$car_level];
                $info['original_price'] = $online_price;
                if ($memberInfo['member_level_id'] > 0) {
                    $online_price = $online_price * $price['member_discount'][$memberInfo['member_level_id']];
                }
                $info['pay_price'] = getsPriceFormat($online_price);
            } else {
                $info['pay_price'] = "--";
            }
            # 收藏状态
            $collection = Db::table("collection")->field("id")->where(array("member_id" => $member_id, "service_id" => $id))->find();
            $info['collection'] = empty($collection) ? 1 : 2;
        } else {
            $info['collection'] = 1;
            $info['pay_price'] = "--";
        }

        if (!empty($info['service_picurl'])) {
            $info['service_picurl'] = explode(',', $info['service_picurl']);
        }
        if (!empty($info['service_context'])) {
            $info['service_context'] = explode(',', $info['service_context']);
        } else {
            $info['service_context'] = array();
        }
        if (!empty($info['service_keyborder'])) {
            $info['service_keyborder'] = explode('，', $info['service_keyborder']);
        } else {
            $info['service_keyborder'] = array();
        }

        # 判断是否关联了其他服务以及自己的广告图
        if ($info['is_relation'] == 1) {
            # 关联了服务显示这个服务的广告图
            if (!empty($info['service_pid'])) {
                # 查询是否有广告图
                $relationAdvance = Db::table("service")->field("service_advert")->where(array("id" => $info['service_pid']))->find();
                if (!empty($relationAdvance)) {
                    $info['service_advert'] = $relationAdvance['service_advert'];
                } else {
                    $info['is_relation'] = 2;
                }
            } else {
                $info['is_relation'] = 2;
            }
        }
        # 判断积分兑换里面是否有这个服务
        $info['integral_exchange'] = 2;
        $integralInfo = Db::table("integral_exchange ie,integral_exchange_prize iep")
            ->field("ie.id")
            ->where("ie.status=1 and ie.surplus_num>0  and start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "' and iep.integral_exchange_id=ie.id ")
            ->where(array('iep.exchange_type' => 2, 'iep.voucher_type' => 2))
            ->where('iep.spoil_id in (select id from card_voucher where server_id = ' . $id . ')')
            ->select();
        if (!empty($integralInfo)) {
            $info['integral_exchange'] = 1;
        }
        $shareInfo = config('share.service');
        $shareInfo['jumpUrl'] .= '?params=' . urlencode(json_encode(array('id' => $id, 'type' => 2, 'is_share' => 'share')));
        $toutiaoShow = config('share.toutiaoShow');
        $exchange_i = imgUrl('static/img/exchange_i.gif', true);
        return array("status" => true, "info" => $info, 'shareInfo' => $shareInfo, 'shareOrder' => config('share.shareOrder'), 'toutiaoShow' => $toutiaoShow, 'exchange_i' => $exchange_i);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取服务分类推荐服务
     */
    function serviceRecommend()
    {
        $member_id = input("post.member_id");
        $class_pid = input("post.class_pid");
        $list = Db::table("service_recommend sr,service s")
            ->field("s.id,s.service_title,s.service_keyborder,s.service_image,s.service_time")
            ->where(array("service_class_pid" => $class_pid))
            ->where("sr.service_id=s.id and sr.start_time<='" . date("Y-m-d") . "' and sr.end_time>='" . date("Y-m-d") . "'")
            ->where('s.service_title', 'NOT REGEXP', '^\#\#')
            ->order("position asc")
            ->limit(3)
            ->select();
        if (!empty($list)) {
            if (!empty($member_id)) {
                $MemberService = new MemberService();
                $memberInfo = $MemberService->MemberInfoCache($member_id);
            }
            $PriceService = new PriceService();
            foreach ($list as $k => $v) {
                if (!empty($v['service_keyborder'])) {
                    $list[$k]['service_keyborder'] = explode('，', $v['service_keyborder']);
                }
                # 计算服务价格
                $price = $PriceService->servicePrice($v['id']);
                $min_price = 0;
                $max_price = 0;
                if (!empty($member_id)) {
                    if (!empty($price['online'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']);
                    }
                    if ($memberInfo['member_level_id'] > 0) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']) * max($price['member_discount']);
                    }
                } else {
                    if (!empty($price['offline'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['offline']);
                    }
                }

                if ($min_price == $max_price) {
                    $list[$k]['price'] = getsPriceFormat($min_price);
                } else {
                    $list[$k]['price'] = getsPriceFormat($min_price) . "~" . getsPriceFormat($max_price);
                }
            }
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 根据最高级服务分类获取服务列表
     */
    function getsServiceList()
    {
        $member_id = input("post.member_id");
        $class_pid = input("post.class_pid");
        $page = input("post.page");
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $where = "s.is_del = 2 and s.biz_pid != 408";
        $order = "s.biz_pid asc";
        $class_id = input("post.class_id");
        if (!empty($class_id)) {
            $where .= " and s.service_class_id = {$class_id}";
        }
        $search = input("post.search");
        if (!empty($search)) {
            $where .= " and (s.service_title like '%" . $search . "%' or s.service_keyborder like '%" . $search . "%')";
        }
        $price_line = input("post.price_line");
        if (!empty($price_line)) {
            if ($price_line == 'up') {
                $order = "max_price desc";
            } else {
                $order = "min_price asc";
            }
        }
        $biz_type = input("post.biz_type");
        if (!empty($biz_type)) {
            $where .= " and s.id in (select sb.service_id from biz b,service_biz sb where b.biz_type = {$biz_type} and b.biz_status = 1 and b.id = sb.biz_id)";
        }
        $list = Db::table("service s")
            ->field("s.id,s.service_title,s.service_keyborder,s.service_image,s.biz_pid,s.is_discount,s.add_type,
             if(s.biz_pid=0,
             (select min(discount) from service_car_mediscount where service_id=s.id and biz_id = 0 and service_type=1),
             (select min(discount) from service_car_mediscount where service_id=s.id and biz_id = s.biz_pid and service_type=1)
             ) min_price,
            if(s.biz_pid=0,
            (select max(discount) from service_car_mediscount where service_id=s.id and biz_id = 0 and service_type=1),
            (select max(discount) from service_car_mediscount where service_id=s.id and biz_id = s.biz_pid and service_type=1)
            )
             max_price")
            ->where("s.class_pid = {$class_pid}")
            ->where($where)
            ->where(array('service_status' => 1, 'audit_status' => 2,'is_display'=>1))
            ->where('s.service_title', 'NOT REGEXP', '^\#\#')
            ->order('s.biz_pid asc')
            ->order($order)
            ->limit($page_num, 10)
            ->select();
        //var_dump($list);
        if (!empty($list)) {
            $PriceService = new PriceService();
            if (!empty($member_id)) {
                $MemberService = new MemberService();
                $memberInfo = $MemberService->MemberInfoCache($member_id);
            }
            foreach ($list as $k => $v) {
                # 标签
                if ($v['biz_pid'] == 0) {
                    $label_before = '平台服务';
                } else {
                    if ($v['add_type'] == 1) {
                        $label_before = '门店商品';
                    } else {
                        $label_before = '门店服务';
                    }
                }
                if ($v['is_discount'] == 1) {
                    $list[$k]['service_label'] = $label_before . '可打折';
                } else {
                    $list[$k]['service_label'] = $label_before . '不打折';
                }
                if (!empty($v['service_keyborder'])) {
                    $list[$k]['service_keyborder'] = array_slice(explode('，', $v['service_keyborder']), 0, 2);
                }
                # 计算服务价格
                $price = $PriceService->servicePrice($v['id'], $v['biz_pid']);
                $min_price = 0;
                $max_price = 0;
                if (!empty($member_id)) {
                    if (!empty($price['online'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']);
                    }
                    if ($memberInfo['member_level_id'] > 0) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['online']) * max($price['member_discount']);
                    }
                } else {
                    if (!empty($price['offline'])) {
                        $min_price = min($price['online']) * min($price['member_discount']);
                        $max_price = max($price['offline']);
                    }
                }
                if ($min_price == $max_price) {
                    $list[$k]['price'] = getsPriceFormat($min_price);
                } else {
                    $list[$k]['price'] = getsPriceFormat($min_price) . "~" . getsPriceFormat($max_price);
                }
            }
            $flag = count($list) == 10 ? true : false;
            return array("status" => true, "list" => $list, "flag" => $flag);
        } else {
            return array("status" => false, "msg" => "暂无服务信息");
        }
    }
}
