<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/16
 * Time: 14:52
 */

namespace app\api\controller;


use app\api\ApiService\IntegralService;
use app\api\ApiService\MemberService;
use app\service\OcrService;
use app\service\ResourceService;
use app\service\SmsCode;
use Redis\Redis;
use think\Db;

class AccountSettings extends Common
{
    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 修改手机号
     */
    function modifyPhone()
    {
        # 接收手机号
        $phone = input('post.phone');
        # 接收验证码
        $code = input('post.code');
        # 手机号验证
        if (!CheckMobile($phone)) {
            return array('status' => false, 'msg' => '手机号填写不正确');
        }
        # 判断是否重复
        $memberInfo = Db::table('member')->field('id')
            ->where(array('member_phone' => $phone))
            ->where("id != " . $this->MemberId)
            ->find();
        if (!empty($memberInfo)) {
            return array('status' => false, 'msg' => '手机号已存在,请更换手机号');
        }
        # 验证码判断
        $MemberService = new MemberService();
        if (!$MemberService->valiPhoneCode($phone, $code) and $code != '000000') {
            return array('status' => false, 'msg' => '验证码填写错误');
        }
        # 手机号修改
        Db::table('member')->where(array('id' => $this->MemberId))->update(array('member_phone' => $phone));
        # 清空验证码redis
        $redis = new Redis();
        $redis->hDel('smsCode', strval($phone));
        # 修改用户redis
        $MemberService->changeMemberInfo($this->MemberId);
        return array('status' => true, 'msg' => '修改成功');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 修改支付密码/初次设置
     */
    function modifyPayPwd()
    {
        # 接收原密码
        $oldPwd = input('post.old_pwd');
        # 新密码
        $newPwd = input('post.new_pwd');
        # 确认密码
        $newConfirm = input('post.new_confirm');
        # 操作标识
        $mark = input('post.mark');
        # 判断原密码输入是否正确
        if($mark != 'first') {
            # 修改
            if ($this->MemberInfo['member_paypass'] != md5($oldPwd) and !empty($this->MemberInfo['member_paypass'])) {
                return array('status' => false, 'msg' => '原密码输入不正确');
            }
        }
        # 判断新密码和确认密码是否相同
        if ($newPwd != $newConfirm) {
            return array('status' => false, 'msg' => '两次密码输入不一致');
        }
        if($mark != 'first') {
            # 修改
            if (!empty($this->MemberInfo['member_paypass'] and $this->MemberInfo['member_paypass'] == md5($newPwd))) {
                return array('status' => false, 'msg' => '新密码不能与原密码相同');
            }
        }
        # 修改支付密码
        Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->update(array('member_paypass' => md5($newPwd)));
        Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->update(array('first_pay' => 2));
        # 更新redis
        $memberService = new MemberService();
        $memberService->changeMemberInfo($this->MemberId);
        return array('status' => true, 'msg' => '修改成功');
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @content 发送短信验证码
     */
    function sendSMSCode()
    {
        # 接收手机号
        $phone = input("post.phone");
        if (!CheckMobile($phone)) {
            return array('status' => false, 'msg' => '手机号不正确');
        }
        # 接收操作标识
        $mark = input('post.mark');
        if ($mark == 'modifyPhone') {
            # 修改手机号
            # 修改手机号 , 进行验证
            $memberInfo = Db::table('member')->field('id')
                ->where(array('member_phone' => $phone))
                ->where("id != " . $this->MemberId)
                ->find();
            if (!empty($memberInfo)) {
                return array('status' => false, 'msg' => '手机号已存在,请更换手机号');
            }
        }
        if ($mark == 'forgetPwd') {
            # 忘记密码
            $memberInfo = Db::table('member')->field('id')
                ->where(array('member_phone' => $phone, 'id' => $this->MemberId))
                ->find();
            if (empty($memberInfo)) {
                return array('status' => false, 'msg' => '手机号输入不正确');
            }
        }
        $SmsCode = new SmsCode();
        $SmsCode->Ysms($phone);
        return array("status" => true, 'msg' => '发送成功,请注意查看');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 忘记密码
     */
    function forgetPwd()
    {
        # 接收手机号
        $phone = input('post.phone');
        # 接收验证码
        $code = input('post.code');
        # 新密码
        $newPwd = input('post.new_pwd');
        # 确认密码
        $newConfirm = input('post.new_confirm');
        # 手机号验证
        if ($this->MemberPhone != $phone) {
            return array('status' => false, 'msg' => '手机号输入不正确');
        }
        # 验证码判断
        $MemberService = new MemberService();
        if (!$MemberService->valiPhoneCode($phone, $code) and $code != '000000') {
            return array('status' => false, 'msg' => '验证码填写错误');
        }
        # 判断新密码和确认密码是否相同
        if ($newPwd != $newConfirm) {
            return array('status' => false, 'msg' => '两次密码输入不一致');
        }
        if (!empty($this->MemberInfo['member_paypass'] and $this->MemberInfo['member_paypass'] == md5($newPwd))) {
            return array('status' => false, 'msg' => '新密码不能与原密码相同');
        }
        # 修改支付密码
        Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->update(array('member_paypass' => md5($newPwd)));
        # 清空验证码redis
        $redis = new Redis();
        $redis->hDel('smsCode', strval($phone));
        # 更新redis
        $memberService = new MemberService();
        $memberService->changeMemberInfo($this->MemberId);
        return array('status' => true, 'msg' => '修改成功');
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @content 实名认证
     */
    function realName()
    {
        # 接收姓名
        $name = input('post.name');
        # 接收性别
        $sex = input('post.sex')??0;
        # 接收身份证号
        $idCard = input('post.idCard');
        # 接收身份证图片--base64格式
        $imgInfo = input('post.imgInfo');
        # 验空
        if (empty($name) or is_null($sex) or empty($idCard) or empty($imgInfo)) {
            return array('status' => false, 'msg' => '请完善信息');
        }
        if (!preg_match('/^[\x{4e00}-\x{9fa5}]{2,4}$/u', $name)) {
            return array('status' => false, 'msg' => '姓名请输入2~4位中文汉字');
        }
        # 判断身份证号是否重复
        $memberInfo = Db::table('member')->field('id')
            ->where(array('member_idcard' => $idCard))
            ->where('id != ' . $this->MemberId)
            ->find();
        if (!empty($memberInfo)) {
            return array('status' => false, 'msg' => '身份证号重复');
        }
        # 图片识别验证
        $imgUrl = ResourceService::uploadBase($imgInfo,'idcard');
        if (is_array($imgUrl)) {
            return $imgUrl;
        }
        $ocr = new OcrService();
        $ocrRes = $ocr->OcrIdCard($imgUrl);
        # 删除 身份证照片
//        @unlink(ROOT_PATH . 'upload' . $imgUrl);
        if ($ocrRes['status']) {
            if ($idCard != $ocrRes['IDNumber']) {
                return array('status' => false, 'msg' => '身份证号填写有误');
            }
            if ($name != $ocrRes['name']) {
                return array('status' => false, 'msg' => '姓名填写有误');
            }
            # 姓名
            $name = $ocrRes['name'];
            if ($sex != getSexInfo($ocrRes['sex'])) {
                return array('status' => false, 'msg' => '性别填写有误');
            }
            # 性别
            $sex = getSexInfo($ocrRes['sex']);
        } else {
            return $ocrRes;
        }
        # 通过身份证号获取年龄/出生年月日
        $ageInfo = getAge($idCard);
        # 修改用户信息
        Db::table('member')->where(array('id' => $this->MemberId))
            ->update(array(
                'member_name' => $name,
                'member_age' => $ageInfo['age'],
                'member_sex' => $sex,
                'member_born_year' => $ageInfo['year'],
                'member_born_month' => $ageInfo['month'],
                'member_born_day' => $ageInfo['day'],
                'member_born' => $ageInfo['born'],
                'member_idcard'=>$idCard,
                'idcard_url'=>imgUrl($imgUrl)
            ));
        Db::table('member_mapping')->where(array('member_id' => $this->MemberId))->update(array('member_status' => 2));
        # 更新redis
        $memberService = new MemberService();
        $memberService->changeMemberInfo($this->MemberId);
        # 实名认证加积分
        $integral = IntegralService::integralSetItem('is_realname');
        $integralService = new IntegralService();
        $integralService->addMemberIntegral($this->MemberId, -1, $integral, 1);
        return array('status' => true, 'msg' => '认证成功');
    }

    function takeMember()
    {
        # 判断用户是否是会员
        $nextLevel = array();
        $privilege = array();
        if($this->MemberInfo['member_level_id']>0){
            if($this->MemberInfo['member_level_id']<9){
                # 查询升级后的会员等级 升级价格
                $upLevel = $this->MemberInfo['member_level_id']+1;
                if($upLevel==2){
                    $upLevel=3;
                }
                $nextLevel = Db::table('upgrade up')
                    ->field("convert(up.price,decimal(10,2)) price,ml.level_title")
                    ->join('member_level ml','up.up_level = ml.id','left')
                    ->where(array('now_level'=>$this->MemberInfo['member_level_id'],'up_level'=>$upLevel))
                    ->find();
                # 查询升级后的会员特权
                $privilege=Db::query("select title,privilege_icon from privilege where 
                          find_in_set(id,(select lc.level_privilege from level_content lc where lc.level_id=".($this->MemberInfo['member_level_id']+1).")) 
                ");
            }
            $downstate=2;
        }else{
            $downstate=1;
        }
        return array('status'=>true,'downstate'=>$downstate,'nextLevel'=>$nextLevel,'privilege'=>$privilege);
    }


}
