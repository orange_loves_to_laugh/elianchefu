<?php


namespace app\api\controller;


use app\api\ApiService\PropertyService;
use think\Controller;

class Property extends Controller
{
    function getPropertyList()
    {
        return PropertyService::getsPropertyList(input());
    }

    function propertyDetail()
    {
        return PropertyService::getsPropertyDetail(input());
    }

    function getPropertyBuild()
    {
        return PropertyService::getsPropertyBuild(input());
    }

    function propertyCanUseVoucher()
    {
        return PropertyService::getsPropertyCanUseVoucher(input());
    }

    function getPropertyOrder()
    {
        return PropertyService::makePropertyOrder(input());
    }

    function getMyHouse()
    {
        return PropertyService::getsMyHouse(input());
    }
    function getPayFeeLog()
    {
        return PropertyService::getsPayFeeLog(input());
    }

    function removeHouse()
    {
        return PropertyService::removeHouses(input());
    }
}