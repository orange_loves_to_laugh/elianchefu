<?php


namespace app\api\controller;


use app\api\ApiService\LevelService;
use think\Controller;
use think\Db;

class External extends Controller
{

    /**
     *
     * 获取用户会员信息
     */
    function memberInfo()
    {
        $phone=Input("post.phone");
        $info=Db::table("member m")
            ->field("m.id,mm.member_level_id,mm.member_expiration")
            ->join("member_mapping mm","mm.member_id=m.id")
            ->where(array("m.member_phone"=>$phone))
            ->find();
        if(!empty($info['member_level_id'])){
            $levelInfo=LevelService::memberLevelInfo($info['member_level_id']);
            if ($info['member_level_id'] == 1 or $info['member_level_id'] == 2) {
                # 判断是否过期
                if ($info['member_expiration'] < date("Y-m-d")) {
                    $info['member_level_id'] = 0;
                }
            }
            $info['img']=$levelInfo['level_img'];
            return array("status"=>true,"data"=>$info);
        }else{
            return array("status"=>false);
        }
    }
}