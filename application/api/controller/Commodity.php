<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/15
 * Time: 9:14
 */

namespace app\api\controller;


use app\api\ApiService\BalanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\SubsidyService;
use app\api\ApiService\VoucherService;
use app\service\EalspellCommissionService;
use app\service\IGeTuiService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class Commodity extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 积分兑换列表
     */
    function integralExchangeList()
    {
        $class_id = input("post.class_id") ?? 0;
        $priceOrder = input("post.priceOrder");
        $canExchange = input("post.canExchange");
        $show_mark = input("post.show_mark");
        $memberIntegral = $this->MemberInfo['member_integral'];
        $list = IntegralService::integralExchange($class_id, $priceOrder, $canExchange, $memberIntegral,'',$show_mark);
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 积分兑换详情
     */
    function integralExhchangeDetail()
    {
        $id = input("post.id");
        $info = IntegralService::integralExchange("", "", "", "", $id);
        # 分享积分
        $share = IntegralService::integralSetItem("share_service");
        return array("status" => true, "info" => $info[0], "memberLevel" => $this->MemberLevel, "share" => $share);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 立即兑换
     */
    function integralExchangeNow()
    {
        $_redis = new Redis();
        $is_repeat = $_redis->lock("integralExchangeNow" . $this->MemberId, 60);
        if (!$is_repeat) {
            return array("status" => false, "msg" => "请稍后再试~");
        }
        $id = input("post.id");
        $info = IntegralService::integralExchange("", "", "", "", $id);
        $info = $info[0];
        # 判断积分是否足够
        if ($this->MemberInfo['member_integral'] < $info['exchange_integral']) {
            return array("status" => false, "msg" => "积分不足");
        }
        # 判断商品数量
        if ($info['surplus_num'] < 1) {
            return array("status" => false, "msg" => "该商品库存不足");
        }
        # 判断是否是会员
        if ($this->MemberLevel == 0) {
            return array("status" => false, "msg" => "立即办理会员开启兑换权限");
        }
        if ($this->MemberLevel < 1) {
            return array("status" => false, "msg" => "暂无兑换权限");
        }
        # 判断限购数量
        if ($info['limit_num'] > 0) {
            # 查询用户是否兑换过该商品
            $already = Db::table("log_integral")->where(array("member_id" => $this->MemberId, "integral_source" => 5, "integral_type" => 0, "biz_pro_id" => $id))->count();
            if ($already >= $info['limit_num']) {
                return array("status" => false, "msg" => "超出兑换限制，请选择其他商品");
            }
        }
        # 减少商品数量
        Db::table("integral_exchange")->where(array("id" => $id))->setDec("surplus_num", 1);
        # 添加对应奖品
        if ($info['exchange_type'] == 1) {
            # 增加用户余额
            $BalanceService = new BalanceService();
            $BalanceService->operaMemberbalance($this->MemberId, $info['exchange_num'], 1, array("consump_price" => $info['exchange_num'],
                "pay_type" => 7, "log_consump_type" => 2, "consump_type" => 1, "income_type" => 12));
            $this->MemberInfo['member_balance'] = getsPriceFormat($this->MemberInfo['member_balance'] + $info['exchange_num']);
            # 增加补贴记录
            SubsidyService::addSubsidy(array("subsidy_type"=>9,"subsidy_number"=>$info['exchange_num'],"member_id"=>$this->MemberId));
        } elseif ($info['exchange_type'] == 2) {
            # 用户增加卡券
            $_voucher = array();
            # 查询服务id
            $server_id = Db::table("card_voucher")->field("server_id")->where(array("id" => $info['spoil_id']))->find();
            for ($i = 0; $i < $info['exchange_num']; $i++) {
                array_push($_voucher, array("voucher_id" => $info['spoil_id'], "card_time" => $info['exchange_indate'], "voucher_source" => 14,
                    "paytype" => 7, "voucher_cost" => $info['price'], "card_title" => $info['prize_title'] . "券", "voucher_type" => $info['voucher_type'] + 2,
                    "payprice" => $info['exchange_integral'], "server_id" => $server_id['server_id']));
            }
            $VoucherService = new VoucherService();
            $VoucherService->addMemberVoucher($this->MemberId, $_voucher);
        } else if (in_array($info['exchange_type'],array(7,8,9,10,11,12,13,14,15,16))) {
            $VoucherService = new VoucherService();
            $VoucherService->addMerchantVoucher($this->MemberId, $info['exchange_num'], $info['spoil_id'], 14);
        } else {
            # 增加红包卡券
            $VoucherService = new VoucherService();
            $VoucherService->addRedpacket($this->MemberId, $info['spoil_id'], 2, $info['exchange_num'], array("voucher_source" => 14, "paytype" => 7));
        }
        # 扣除用户积分
        $IntegralService = new IntegralService();
        $IntegralService->addMemberIntegral($this->MemberId, 5, $info['exchange_integral'], 0, array("biz_pro_id" => $id));
        $this->MemberInfo['member_integral'] = intval($this->MemberInfo['member_integral'] - $info['exchange_integral']);
        $_redis->hSetJson("memberInfo", $this->MemberId, $this->MemberInfo);
        # 完成任务=>10 积分兑换
        TimeTask::finishTask($this->MemberId, 10);
        # 积分兑换给代理提成
        # 查询该会员是否是代理推荐
        $channel = Db::table("channel")->where(array("member_id"=>$this->MemberId,"channel"=>10))->where("pid > 0")
            ->order("id desc")->find();
        if(!empty($channel)){
            EalspellCommissionService::memberIntegralExchange($this->MemberId,$channel['pid']);
        }

        # 发送消息提醒
        $IGeTuiService = new IGeTuiService();
        $IGeTuiService->MessageNotification($this->MemberId, 9, ['title' => $info['exchange_title'], 'order_number' => date('YmdHis') . time(), 'price' => $info['exchange_integral'], 'integral' => $this->MemberInfo['member_integral']]);
        $_redis->unlock('integralExchangeNow' . $this->MemberId);
        return array("status" => true);
    }
}
