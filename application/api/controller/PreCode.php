<?php


namespace app\api\controller;


use app\api\ApiService\VoucherService;
use app\service\SmsCode;
use think\Db;

class PreCode extends Common
{
    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 激活码是否有效
     */
    function canActive()
    {
        $id=input("post.id");
        $pre_code=strtoupper(trim(input("post.pre_code")));
        $member_id=$this->MemberId;
        # 查询这个码是否被绑定
        $isActive=Db::table("preferent_code")->where(array("id"=>$id,"pre_code"=>$pre_code))->find();
        if(empty($isActive) or $isActive['pre_status']!=1){
            return array("status"=>false,"msg"=>"无效激活码");
        }else{
            # 查询这个人是否是新用户
            $memberInfo=Db::table("member_mapping")->field("member_level_id,member_create,register_time,member_openid")->where(array("member_id"=>$member_id))->find();
            if($memberInfo['member_level_id']>0){
                return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
            }else{
                if(!empty($memberInfo['register_time'])){
                    if(date("Y-m-d",strtotime($memberInfo['register_time']))<date("Y-m-d",strtotime("-3 days"))){
                        return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                    }
                }else{
                    if(date("Y-m-d",strtotime($memberInfo['member_create']))<date("Y-m-d",strtotime("-3 days"))){
                        return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                    }
                }
            }
            return array("status"=>true);
        }
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 验证车牌号
     */
    function valiCarLicen()
    {
        $car_licens=input("post.car_licens");
        # 查询这个车牌号有没有被绑定过
        $isBind=Db::table("preferent_code")->where(array("pre_licens"=>$car_licens))->find();
        if(!empty($isBind)){
            return array("status"=>false,"msg"=>"无效的车牌号");
        }else{
            # 查询这个车是否有服务
            $isService=Db::table("log_service")->where(array("car_licens"=>$car_licens))->find();
            if(!empty($isService)){
                return array("status"=>false,"msg"=>"无效的车牌号");
            }
            return array("status"=>true);
        }
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 发送短信验证码
     */
    function sendActiveCode()
    {
        $phone=input("post.phone");
        $pre_code=strtoupper(trim(input("post.pre_code")));
        if(empty($phone) or empty($pre_code)){
            return array("status"=>false,"msg"=>"参数错误");
        }
        # 查询这个手机号是否被绑定
        $isBind=Db::table("preferent_code")->where(array("pre_phone"=>$phone))->find();
        if(!empty($isBind)){
            return array("status"=>false,"msg"=>"无效的手机号");
        }else{
            $SmsCode = new SmsCode();
            $content="您的激活码为：{$pre_code},请勿向他人泄露，以免造成您个人财产的损失。";
            $SmsCode->Ysms($phone,$content,false);
            return array("status"=>true);
        }
    }

    function getPreVoucher()
    {
        $id=input("post.id");
        $pre_code=strtoupper(trim(input("post.pre_code")));
        $phone=input("post.phone");
        $car_licens=input("post.car_licens");
        $member_id=$this->MemberId;
        $isActive=Db::table("preferent_code")->where(array("id"=>$id,"pre_code"=>$pre_code))->find();
        if(empty($isActive) or $isActive['pre_status']!=1){
            return array("status"=>false,"msg"=>"无效的激活码");
        }
        # 查询这个车牌号有没有被绑定过
        $isBind=Db::table("preferent_code")->where(array("pre_licens"=>$car_licens))->find();
        if(!empty($isBind)){
            return array("status"=>false,"msg"=>"无效的车牌号");
        }
        # 查询这个人是否是新用户
        $memberInfo=Db::table("member_mapping")->field("member_level_id,member_create,register_time,member_openid")->where(array("member_id"=>$member_id))->find();
        if($memberInfo['member_level_id']>0){
            return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
        }else{
            if(!empty($memberInfo['register_time'])){
                if(date("Y-m-d",strtotime($memberInfo['register_time']))<date("Y-m-d",strtotime("-3 days"))){
                    return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                }
            }else{
                if(date("Y-m-d",strtotime($memberInfo['member_create']))<date("Y-m-d",strtotime("-3 days"))){
                    return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                }
            }
        }
        # 更改该卡的绑定状态
        Db::table("preferent_code")->data(array("pre_status"=>2,"pre_licens"=>$car_licens,"pre_phone"=>$phone))->where(array("id"=>$id))->update();
        # 给用户增加卡券
        $this->addVoucher();

        return array("status"=>true);
    }

    /**
     * @return bool[]
     * @context 增加卡券
     */
    function addVoucher()
    {
        $_array=array(
            array("voucher_id"=>28,"card_time"=>90, "status"=>1,"voucher_source"=>13,"voucher_cost"=>'38',"voucher_type"=>4,"server_id"=>9,"card_title"=>"汽车普洗服务券"),
            array("voucher_id"=>28,"card_time"=>90, "status"=>1,"voucher_source"=>13,"voucher_cost"=>'38',"voucher_type"=>4,"server_id"=>9,"card_title"=>"汽车普洗服务券"),
            array("voucher_id"=>13,"card_time"=>90, "status"=>1,"voucher_source"=>13,"voucher_cost"=>'58',"voucher_type"=>4,"server_id"=>10,"card_title"=>"汽车快捷精洗服务券"),
            array("voucher_id"=>99,"card_time"=>90, "status"=>1,"voucher_source"=>13,"voucher_cost"=>'28',"voucher_type"=>4,"server_id"=>91,"card_title"=>"全车普通手工打蜡（含蜡）券"),
        );
        $VoucherService=new VoucherService();
        $VoucherService->addMemberVoucher($this->MemberId,$_array);
        return array("status"=>true);
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 发送验证码之前判断能否激活
     */
    function sendPreCodeVali()
    {
        $member_id=$this->MemberId;
        # 查询这个人是否是新用户
        $memberInfo=Db::table("member_mapping")->field("member_level_id,member_create,register_time,member_openid")->where(array("member_id"=>$member_id))->find();
        if($memberInfo['member_level_id']>0){
            return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
        }else{
            if(!empty($memberInfo['register_time'])){
                if(date("Y-m-d",strtotime($memberInfo['register_time']))<date("Y-m-d",strtotime("-3 days"))){
                    return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                }
            }else{
                if(date("Y-m-d",strtotime($memberInfo['member_create']))<date("Y-m-d",strtotime("-3 days"))){
                    return array("status"=>false,"msg"=>"该活动仅支持新注册的非会员用户参与");
                }
            }
        }
        return array("status"=>true);
    }
}