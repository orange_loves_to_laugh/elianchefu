<?php


namespace app\api\controller;


use app\service\RacGameService;
use think\App;
use think\Controller;
use think\Db;

class RacGameApi extends Controller
{
    public $MemberId;
    public $GameId;
    function __construct(App $app = null)
    {
        parent::__construct($app);
        $this->MemberId = input("post.member_id");
        $this->GameId = 1;   // 游戏id 为以后多个游戏做准备
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查询用户是否可以继续玩游戏
     */
    function memberCanPlay()
    {

        $game_log = Db::table("rac_log")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->order("id desc")->find();

        $prizeInfo=$this->PrizeInfo();

        if(!empty($game_log)){
            $game_log['can_play'] = 1;

            if($game_log['rac_status']==2){
                # 跳分享页
                $game_log['can_play'] = 2;
            }elseif($game_log['rac_status']==3){
                # 跳成功页
                $game_log['can_play'] = 3;
            }elseif($game_log['rac_status']==4){

                $memer_repeat_num= Db::table("rac_log")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->count();
                if($memer_repeat_num<$prizeInfo['repeat_num']){
                    $game_log['can_play'] = 1;
                    Db::table("rac_log")->insert(array("member_id"=>$this->MemberId,"log_time"=>date("Y-m-d H:i:s"),"member_repeat_num"=>$memer_repeat_num+1,"game_id"=>$this->GameId));
                }else{
                    $game_log['can_play'] = 4;  // 跳已领取奖励页
                }
            }
        }else{
            $game_log['can_play'] = 1;
           Db::table("rac_log")->insert(array("member_id"=>$this->MemberId,"log_time"=>date("Y-m-d H:i:s"),"member_repeat_num"=>1,"game_id"=>$this->GameId));
        }
        $game_log['member_id'] = $this->MemberId;
        return array("status"=>true,"info"=>$game_log,"prize"=>$prizeInfo);
    }
    function getsUserInfo()
    {
        $game_log = Db::table("rac_log rl,member m")
            ->field("rl.*,m.nickname,m.headimage,m.member_phone")
            ->where("rl.member_id = m.id")
            ->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))
            ->order("id desc")->find();
        $prizeInfo=$this->PrizeInfo();
        $game_log['winner_percent'] = $prizeInfo['winner_percent'];
        $game_log['adv'] = $prizeInfo['advance'];
        $game_log['share_title'] = $prizeInfo['share_title'];
        return array("status"=>true,"info"=>$game_log,"prize_info"=>$prizeInfo);
    }

    function getPrizeInfo()
    {
        $prizeInfo = Db::table("rac_prize")->where(array("id"=>$this->GameId))->find();
        if(empty($prizeInfo)){
            $game_log['winner_percent'] = '';
            $game_log['adv'] = '';
            $game_log['share_title'] = '';
        }else {
            $game_log['winner_percent'] = $prizeInfo['winner_percent'];
            $game_log['adv'] = $prizeInfo['advance'];
            $game_log['share_title'] = $prizeInfo['share_title'];
        }
        return array("status"=>true,"info"=>$game_log);
    }

    function PrizeInfo()
    {
        $prize = Db::table("rac_prize")->where(array("id"=>$this->GameId))->find();
        return $prize;
    }
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 计算需要多少名好友助力
     */
    function computerGame()
    {
        $score=input("post.score");
        $prizeInfo=$this->PrizeInfo();
        # 计算助力总人数
        $help_person=ceil(($prizeInfo['winner_percent']-$score)/$prizeInfo['share_percent']);
        return array("help_person"=>$help_person);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 完成游戏之后
     */
    function finishGame()
    {
        $score=input("post.score");
        $prizeInfo=$this->PrizeInfo();;
        # 计算助力总人数
        $help_person=ceil(($prizeInfo['winner_percent']-$score)/$prizeInfo['share_percent']);
        $_return=array("percent_now"=>$score,"help_person"=>$help_person,"plurs_person"=>$help_person,"rac_status"=>2,"member_game_percent"=>$score,"log_time"=>date("Y-m-d H:i:s"));
        # 剩余助力总人数
        $log_id =  Db::table("rac_log")->field("id")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->order("id desc")->find();
        Db::table("rac_log")->where(array("id"=>$log_id['id']))->update($_return);
        return $_return;
    }


    /**
     * @return array
     * @throws \think\Exception
     * @context 好友助力
     */
    function friendHelp()
    {
        $share_member_id=input("post.share_member_id");
        $unionId=input("post.unionId");
        $nickname=input("post.nickname");
        $headpic=input("post.headpic");
        $repeat_num = input("post.repeat_num");
        $game_id = input("post.game_id");
        if($game_id != $this->GameId){
            return array("status"=>false,"msg"=>"该活动已结束");
        }
        # 获取用户信息及选择的奖品
        $share_memberinfo= Db::table("rac_log rl,member_mapping mm")
            ->field("rl.*,mm.member_unionid")
            ->where("rl.member_id = mm.member_id")
            ->where(array("rl.member_id"=>$share_member_id,"rl.game_id"=>$this->GameId,"rl.member_repeat_num"=>$repeat_num))
            ->order("rl.id desc")->find();
        if($unionId==$share_memberinfo['member_unionid']){
            return array("status"=>false,"msg"=>"快邀请好友助力吧！！");
        }
        if(empty($share_memberinfo['plurs_person'])){
            return array("status"=>false,"msg"=>"好友已领取了奖品，无需助力");
        }else{
            # 判断是否帮助力过
            $helper=Db::table("rac_loghelp")->where(array("friend_unionid"=>$unionId,"member_id"=>$share_member_id,"game_id"=>$this->GameId,"help_repeat_num"=>$repeat_num))->find();
            if(!empty($helper)){
                return array("status"=>false,"msg"=>"已帮好友助力过，不可多次助力");
            }
            $prizeInfo=$this->PrizeInfo();
            $help_score=$this->getsHelpScore($share_memberinfo['plurs_person'],($prizeInfo['winner_percent']-$share_memberinfo['percent_now']),
                $prizeInfo['share_percent']);
            # 修改用户记录
            Db::table("rac_log")->where(array("id"=>$share_memberinfo['id']))->setInc("percent_now",$help_score);
            Db::table("rac_log")->where(array("id"=>$share_memberinfo['id']))->setDec("plurs_person",1);
            # 增加分享记录
            Db::table("rac_loghelp")->insert(array("friend_name"=>$nickname,"friend_headpic"=>$headpic,"help_percent"=>$help_score,
                "member_id"=>$share_member_id,"create_time"=>date("Y-m-d H:i:s"),"friend_unionid"=>$unionId,"help_repeat_num"=>$repeat_num,"game_id"=>$this->GameId));
            return array("status"=>true,"help_percent"=>$help_score,"msg"=>"助力成功");
        }
    }

    /**
     * @param $plurs_person
     * @param $plurs_score
     * @param $aveage
     * @return int|mixed
     * @context 好友助力分数
     */
    function getsHelpScore($plurs_person,$plurs_score,$aveage){
        if($plurs_person==1){
            $_return_score=$plurs_score;
        }else{
            # 选择区间
            $_opera=rand(0,2); // 随机区间取值 0 取平均数  1 取小于平均数 2取大于平均数
            if($_opera==0){
                $_return_score=$aveage;
            }else{
                if($plurs_score<=0){
                    $_return_score = 1;
                    return $_return_score;
                }
                # 判断当前平均数
                $_num=[1,2,3,4,5];
                $_rand=rand(0,4);
                $rand_num=$_num[$_rand];
                if($plurs_score/$plurs_person>=$aveage){
                    $_return_score=$aveage+$rand_num;
                }else{
                    if($rand_num>=$aveage){
                        $_return_score=1;
                    }else{
                        $_return_score=$aveage-$rand_num;
                    }
                }
            }
        }
        return $_return_score;
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取是否助力成功 助力好友列表
     */
    function helpList()
    {
        # 获取用户信息
        $memberInfo=Db::table("rac_log")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->order("id desc")->find();
        # 获取好友助力列表
        $log_help=Db::table("rac_loghelp")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId,"help_repeat_num"=>$memberInfo['member_repeat_num']))->order("id desc")->select();
        if($memberInfo['rac_status']==4){
            $status=3; // 已领取奖励
        }else{
            # 判断用户是否已经助力成功
            $prizeInfo=$this->PrizeInfo();
            if($memberInfo['percent_now']>=$prizeInfo['winner_percent'] and $memberInfo['plurs_person']==0){
                $status=2;  // 可领取奖励
            }else{
                $status=1;  // 不可领取奖励
            }
        }
        return array("status"=>true,"memberInfo"=>$memberInfo,"list"=>$log_help);
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\DbException
     * @context 领取奖品
     */
    function getsPrize()
    {
        # 判断是否已经领取了奖励
        $memberInfo=Db::table("rac_log")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->order("id desc")->find();
        if($memberInfo['rac_status']==4){
            return array("status"=>false,"msg"=>"不可重复领取奖品");
        }else{
            # 将用户改为已完成
            Db::table("rac_log")->where(array("id"=>$memberInfo['id']))->update(array("rac_status"=>3));
            return array("status"=>true);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取轮播消息
     */
    function getsBannerNews(){
        $list=Db::table("rac_log rl,member m")
            ->field("rl.*,m.nickname")
            ->where("rl.member_id = m.id and rl.rac_status=4 and rl.game_id = {$this->GameId} and date(rl.log_time)='".date("Y-m-d")."'")
            ->order("rl.id desc")->limit(20)->select();
        if(!empty($list)){
            return array("status"=>true,"list"=>$list);
        }else{
            return array("status"=>false);
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 游戏重新开始
     */
    function reGame()
    {
        $info= Db::table("rac_log")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId))->order("id desc")->find();
        # 将用户信息删除
        Db::table("rac_log")->where(array("id"=>$info['id']))->update(array("percent_now"=>0,"help_person"=>0,"plurs_person"=>0,"rac_status"=>1));
        # 删除助力信息
        Db::table("rac_loghelp")->where(array("member_id"=>$this->MemberId,"game_id"=>$this->GameId,"help_repeat_num"=>$info['member_repeat_num']))->delete();
        return array("status"=>true);
    }

    function bizList()
    {
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $RacGameService = new RacGameService();
        $list=$RacGameService->racStore(false,20,array("province"=>$province,"city"=>$city,"lon"=>$lon,"lat"=>$lat));
        return array("data"=>$list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 核销登陆
     */
    function login()
    {
        $account=input("post.account");
        $password=input("post.password");
        $info=Db::table("rac_store rs,biz b")->where("rs.biz_id=b.id")->where(array("b.biz_phone"=>$account,"b.biz_phone"=>$password,"b.biz_status"=>1))->find();
        if(empty($info)){
            return array("status"=>false,"msg"=>"账号或密码错误");
        }else{
            return array("status"=>true,'biz_id'=>$info['id']);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 核销记录
     */
    function logCancel()
    {
        $where="";
        $biz_id=input("post.biz_id");
        if(!empty($biz_id)){
            $where="rl.biz_id={$biz_id}";
        }
        $prizeInfo=$this->PrizeInfo();
        # 查询这个门店的用户并且是已到店的
        $list=Db::table("rac_log rl,member m")
            ->field(array("m.nickname name,m.member_phone phone,rl.log_time time"))
            ->where(array("rl.rac_status"=>4,"rl.game_id"=>$this->GameId))
            ->where("rl.member_id = m.id")
            ->where($where)
            ->order("rl.log_time desc")
            ->select();
        if(!empty($list)){
            return array("status"=>true,"list"=>$list);
        }else{
            return array("status"=>false,"msg"=>"暂无更多信息");
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 核销信息
     */
    function cancelInfo()
    {
        $biz_id=input("post.biz_id");
        $phone = input("post.phone");
        # 查询是否有这个用户 并且游戏是否完成
        $info= Db::table("rac_log rl,member m")
            ->field("rl.*,m.member_phone")
            ->where("m.id = rl.member_id")
            ->where(array("m.member_phone"=>$phone,"rl.game_id"=>$this->GameId))->order("id desc")->find();
        if(empty($info)){
            return array("status"=>false,"msg"=>"未找到这个用户");
        }
        if($info['rac_status']==4){
            return array("status"=>false,"msg"=>"该号码已核销");
        }
        $prizeInfo=$this->PrizeInfo();
        if($prizeInfo['winner_percent']>$info['percent_now']){
            return array("status"=>false,"msg"=>"游戏尚未完成");
        }
        # 核销该用户
        Db::table("rac_log")->where(array("id"=>$info['id']))->update(array("rac_status"=>4,"log_time"=>date("Y-m-d H:i:s"),"biz_id"=>$biz_id));
        return array("status"=>true,"prize_title"=>$prizeInfo['prize_title'],"phone"=>$info['member_phone']);
    }



}