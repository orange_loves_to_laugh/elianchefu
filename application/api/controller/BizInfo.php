<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/27
 * Time: 16:25
 */

namespace app\api\controller;


use app\api\ApiService\ClassifyService;
use app\api\ApiService\MemberService;
use app\api\ApiService\PriceService;
use app\api\ApiService\VoucherService;
use Redis\Redis;
use think\Controller;
use think\Db;

class BizInfo extends Controller
{

    /**
     * @return array
     * @context 门店信息
     */
    function getBizInfo()
    {
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $service_id = input("post.service_id");
        $member_id = input("post.member_id");
        $where = " ";
        if (!empty($service_id)) {
//            $where.="  and ((b.biz_type!=1 and (select count(sb.id) from service_biz sb where sb.biz_id=b.id and sb.service_id={$service_id})>0) or b.biz_type =1)";
            $where .= "  and ((b.biz_type!=1 and (b.id in (select DISTINCT biz_id from service_biz where service_id = {$service_id}))) or b.biz_type =1)";
        }
        if ($service_id != 422 && $member_id != 14285 && $member_id != 9405) {
            $where .= " and b.id !=408 ";
        }
        # 判断鲅鱼圈区看不到所有
        $detailAddress = getAddressResolution($lon, $lat);
        $detail_add_a = json_decode($detailAddress, true);
        if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

            $_district = $detail_add_a['regeocode']['addressComponent']['district'];
            if ($_district == '鲅鱼圈区' && $member_id != 14285 && $member_id != 9405) {
                return array("status" => false);
            }
        }
        // 不查询鲅鱼圈的门店
        if ($member_id != 14285 && $member_id != 9405)
            $where .= " and (area != '鲅鱼圈区' or b.id = 692)";
        # 门店名称搜索
        $biz_title = input("post.biz_title");
        if (!empty($biz_title)) {
            # 按门店名称
            $biz_title = implode("%", mb_str_splits($biz_title));
            $where .= " and (b.biz_title like '%" . $biz_title . "%'";
            # 按服务名称
            $serviceTitle = Db::table("service s")
                ->field("sb.biz_id")
                ->join(array("service_biz" => "sb"), "sb.service_id = s.id")
                ->where("s.service_title like '%" . $biz_title . "%'")
                ->group("sb.biz_id")
                ->select();
            if (!empty($serviceTitle)) {
                $biztitle_id_str = implode(",", array_column($serviceTitle, 'biz_id'));
                $where .= " or b.id in ({$biztitle_id_str})";
            }
            # 按服务分类名称

            $cateTitle = Db::table("service_class sc")
                ->field("sb.biz_id")
                ->join(array("service" => 's'), "s.service_class_id = sc.id")
                ->join(array("service_biz" => "sb"), "sb.service_id = s.id")
                ->where(" sc.service_class_pid in (select scs.id from service_class scs where scs.service_class_pid=0 and 
                 scs.service_class_title like '%" . $biz_title . "%') or sc.service_class_title like '%" . $biz_title . "%'")
                ->group("sb.biz_id")
                ->select();
//or sc.service_class_title like '%".$biz_title."%'
            if (!empty($cateTitle)) {
                $catetitle_id_str = implode(",", array_column($cateTitle, 'biz_id'));
                $where .= " or b.id in ({$catetitle_id_str}) ";
            }
            $where .= " )";

        }
        $class_id = input("post.class_id");
        if (!empty($class_id)) {
            # 按服务分类搜索
            $cateInfo = Db::table("service_class sc")
                ->field("sb.biz_id")
                ->join(array("service" => 's'), "s.service_class_id = sc.id")
                ->join(array("service_biz" => "sb"), "sb.service_id = s.id")
                ->where(array("sc.service_class_pid" => $class_id))
                ->group("sb.biz_id")
                ->select();

            if (!empty($cateInfo)) {
                $biz_id_str = implode(",", array_column($cateInfo, 'biz_id'));
                $where .= " and b.id in ({$biz_id_str}) ";
            }
            //$where.=" and b.biz_type = {$class_id}";
        }
        $road_help = input("post.road_help");
        if (!empty($road_help)) {
            // 道路救援按维修养护和钣金喷漆类型查
            $roadList = Db::table("service_class sc")
                ->field("sb.biz_id")
                ->join(array("service" => 's'), "s.service_class_id = sc.id")
                ->join(array("service_biz" => "sb"), "sb.service_id = s.id")
                ->where(" sc.service_class_pid in (1,4)")
                ->group("sb.biz_id")
                ->select();
            if (!empty($roadList)) {
                $biz_id_str = implode(",", array_column($roadList, 'biz_id'));
                $where .= " and b.id in ({$biz_id_str}) ";
            }
        }
        if (empty($lon) or empty($lat)) {
            return array("status" => false, "msg" => "暂无门店信息");
        }
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and b.biz_create<'2021-07-01 00:00:00' ";
            }
        }
        # 按省市查询门店 并按经纬度进行排序
        $list = Db::query("SELECT b.id, b.biz_title,concat(b.city,b.area,b.biz_address) address,b.biz_phone,b.biz_img,b.biz_head,b.biz_desc,b.biz_lat,b.biz_lon,b.recommend_status,
        if(b.biz_type=1, (select group_concat(distinct sc.service_class_title) from service s,service_class sc where s.class_pid = sc.id),
        (select group_concat(distinct sc.service_class_title) from service_biz sb,service s,service_class sc where sb.biz_id=b.id and sb.service_id=s.id
         and s.class_pid = sc.id)) biz_tip,
         ( case b.biz_type when 1 then '直营店' when 2 then '加盟店' when 3 then '合作店' end) biz_type,
         date_format(b.biz_opening,'%H:%i') biz_opening,date_format(b.biz_closing,'%H:%i') biz_closing,b.open_state,
        ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$lat}*PI()/180-b.biz_lat*PI()/180)/2),2)+COS({$lat}*PI()/180)*COS(b.biz_lat*PI()/180)*POW(SIN((({$lon}*PI()/180)-(b.biz_lon*PI()/180))/2),2))),2) AS distance  
        FROM biz b where b.biz_status=1 and b.province like '%" . $province . "%' and b.city like '%" . $city . "%'  and b.id !=437 and b.biz_opening is not null 
        {$where}
         order by recommend_status asc,distance asc limit 15");
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                if ($k == 0) {
                    $list[$k]['distance_order'] = -2;
                } elseif ($k == 1) {
                    $list[$k]['distance_order'] = -1;
                } else {
                    $list[$k]['distance_order'] = $list[$k]['distance'];
                }
                if (!empty($v['biz_tip'])) {
                    $list[$k]['biz_tip'] = explode(',', $v['biz_tip']);
                }
                if (!empty($v['biz_head'])) {
                    $list[$k]['biz_head'] = explode(',', $v['biz_head']);
                }
                if ($v['open_state'] == 1) {
                    if ($v['biz_opening'] <= date("H:i:s") and $v['biz_closing'] >= date("H:i:s")) {
                        $list[$k]['open_state_title'] = "营业中";
                        $list[$k]['opening_status'] = 1;
                    } else {
                        $list[$k]['open_state_title'] = "已打烊";
                        $list[$k]['opening_status'] = 2;
                    }
                } else {
                    $list[$k]['open_state_title'] = "暂停营业";
                    $list[$k]['opening_status'] = 2;
                }
                $list[$k]['serviceInfo'] = $this->bizRecommendService($v['id'], $member_id);
            }
            array_multisort(array_column($list, 'distance_order'), SORT_ASC, $list);
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false, "msg" => "暂无门店信息");
        }
    }

    /**
     * @param $biz_id
     * @return array|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 查询门店推荐商品或服务
     */
    function bizRecommendService($biz_id, $member_id)
    {
        $info = Db::table("service")->where(array("biz_pid" => $biz_id, "show_recommend" => 1, "audit_status" => 2, "service_status" => 1))
            ->find();
        if (!empty($info)) {
            # 查询服务小型车价格
            $discount = Db::table("service_car_mediscount")->where(array("service_id" => $info['id'], "car_level_id" => 1, "service_type" => 1))->value("discount");
            $discount = $discount * 0.9;
            # 查询是否有可用卡券 并取即将过期的一张
            $VoucherService = new VoucherService();
            $voucher = $VoucherService->memberVoucher($member_id, array("service" => true, "service_id" => $info['id'], "price" => $discount, "orderby" => "expiration_time asc"));
            if (!empty($voucher['list'])) {
                $voucher = $VoucherService->voucherFormat($voucher, $info['id'], false);
                if ($voucher['num'] > 0) {
                    $voucher_list = array_values($voucher['list']);
                    $discount -= $voucher_list[0]['voucher_cost'];
                    if ($discount <= 0) {
                        $discount = 0;
                    }
                }
            }
            $info['discount'] = getsPriceFormat($discount, 1);
            return $info;
        } else {
            return [];
        }
    }

    /**
     * @return array|bool|mixed|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取最高级服务分类
     */
    function getSeviceMaxClass()
    {
        $type = ClassifyService::getsServiceClass("max");
        array_unshift($type, array("id" => 0,
            "service_class_title" => "全部分类"));
        return array('status' => true, 'list' => $type);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取服务二级分类
     */
    function getServiceSecondLevelClass()
    {
        return array("status" => true, "list" => ClassifyService::getsServiceClass("second"));
    }

    function getServiceMoreClass()
    {
        $list = ClassifyService::getsServiceMoreClass();
        array_unshift($list, array("id" => 0, "name" => "全部分类", "child" => array(array("id" => 0, "name" => "全部分类"))));
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取某一最高级分类的下级分类
     */
    function getServiceSecondLevelItemClass()
    {
        return array("status" => true, "list" => ClassifyService::getsServiceClass("second_item", input("post.class_pid")));
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 门店服务列表
     */
    function bizServiceList()
    {
        $car_level = input('post.car_level');
        $member_id = input("post.member_id");
        $biz_id = input("post.biz_id");
        $order = "s.show_recommend asc ,s.id asc";
        $page = input("post.page");
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $where = "s.is_del = 2 ";
        $class_id = input("post.class_id");
        if (!empty($class_id)) {
            $where .= " and s.service_class_id = {$class_id}";
        }
        $lineup = input("post.lineup");
        if (!empty($lineup)) {
            if ($lineup == 'up') {
                $order = 'num desc';
            } else {
                $order = "num asc";
            }
        }
        $priceline = input("post.priceline");
        if (!empty($priceline)) {
            if ($priceline == 'up') {
                $order = "max_price desc";
            } else {
                $order = "min_price asc";
            }
        }
        # 获取门店营业时间
        $open_time = Db::table("biz")->field("biz_opening,biz_closing,biz_type")->where(array("id" => $biz_id))->find();
        if ($open_time['biz_type'] == 1) {
            # 自营门店查所有服务
            $list = Db::table("service s")
                ->field("s.id,s.service_title,s.service_keyborder,s.service_image,s.service_time,s.biz_pid,s.is_discount,s.add_type,
            (select min(discount) from service_car_mediscount where service_id=s.id and biz_id = 0 and service_type=1) min_price,
            (select max(discount) from service_car_mediscount where service_id=s.id and biz_id = 0 and service_type=1) max_price,
            (select count(os.id) from orders o,order_server os where os.status=1 and o.biz_id={$biz_id}  and os.custommark = 1 and os.order_number=o.order_number
            and os.server_id=s.id and (o.order_status=1 or o.order_status=2 or o.order_status=3 or o.order_status=6) 
            and ((o.order_type=2 and o.order_create<='" . date("Y-m-d H:i:s") . "') or (o.order_type=3 ))) num")
                ->where("s.is_del = 2 and service_status = 1 and audit_status = 2 and is_display = 1")
                ->where($where)
                ->where('s.service_title', 'NOT REGEXP', '^\#\#')
                ->order($order)
                ->limit($page_num, 10)
                ->select();
        } else {
            $list = Db::table("service_biz sb,service s")
                ->field("s.id,s.service_title,s.service_keyborder,s.service_image,s.service_time,s.biz_pid,s.is_discount,s.add_type,
            (select min(discount) from service_car_mediscount where service_id=s.id and biz_id = {$biz_id} and service_type=1) min_price,
            (select max(discount) from service_car_mediscount where service_id=s.id and biz_id = {$biz_id} and service_type=1) max_price,
            (select count(os.id) from orders o,order_server os where os.status=1 and o.biz_id={$biz_id}  and os.custommark = 1 and os.order_number=o.order_number
            and os.server_id=s.id and (o.order_status=1 or o.order_status=2 or o.order_status=3 or o.order_status=6) 
            and ((o.order_type=2 and o.order_create<='" . date("Y-m-d H:i:s") . "') or (o.order_type=3 ))) num")
                ->where("sb.service_id=s.id and sb.biz_id = {$biz_id} and s.is_del = 2 and service_status = 1 and audit_status = 2 and is_display = 1")
                ->where($where)
                ->where('s.service_title', 'NOT REGEXP', '^\#\#')
                ->order($order)
                ->limit($page_num, 10)
                ->select();
        }

        if (!empty($list)) {

            $canOrder = true;
//            if(date("H:i:s")<$open_time['biz_opening'] or date("H:i:s")>$open_time['biz_closing']){
//                $canOrder = false;
//            }
            # 查询该门店有没有正在等待中的服务,有-需要排队,没有-无需排队
            $orderInfo = Db::table('order_server os')
                ->join('orders o', 'o.order_number=os.order_number', 'left')
                ->where(array('member_id' => $member_id, 'biz_id' => $biz_id))
                ->where(array('os.status' => 1))
                ->count();
            if (!empty($orderInfo)) {
                $canOrder = false;
            }
            if (!empty($member_id)) {
                $MemberService = new MemberService();
                $memberInfo = $MemberService->MemberInfoCache($member_id);
            }
            $PriceService = new PriceService();
            $VoucherService = new VoucherService();
            foreach ($list as $k => $v) {
                # 标签
                if ($v['biz_pid'] == 0) {
                    $label_before = '平台服务';
                    $list[$k]['service_label'] = $label_before . '可打折';
                } else {
//                    if ($v['add_type'] == 1) {
//                        $label_before = '门店商品';
//                    } else {
//                        $label_before = '门店服务';
//                    }
                    $list[$k]['service_label'] = '线上支付9折';
                }
//                if ($v['is_discount'] == 1) {
//                    $list[$k]['service_label'] = $label_before . '可打折';
//                } else {
//                    $list[$k]['service_label'] = $label_before . '不打折';
//                }
//                $list[$k]['service_label'] = $label_before . '可打折';
                # 计算是否需要显示排队数量
                if (!$canOrder) {
                    $list[$k]['num'] = 0;
                }
                if (!empty($v['service_keyborder'])) {
                    $list[$k]['service_keyborder'] = explode('，', $v['service_keyborder']);
                }
                # 计算服务价格 没注册 显示会员最高折扣 到 门店大型车价格    注册了  显示会员最高折扣 到线上价格大型车价格   成为了会员显示：最大会员折扣小型车 到 最小会员折扣 大型车
                $price = $PriceService->servicePrice($v['id'], $biz_id, true);
                $min_price = 0;
                $max_price = 0;
                // Y:2022  合作端上架服务商品  设置折扣(所有会员折扣一样)   线上下单打折(不分是不是会员)  门店下单不打折
                // 平台服务  余额支付打折  不是余额支付不打折
                if ($v['biz_pid'] == 0) {
                    if (!empty($memberInfo)) {
                        if ($memberInfo['member_level_id'] == 0) {
                            $level = 1;
                        } else {
                            $level = $memberInfo['member_level_id'];
                        }
                    } else {
                        $level = 1;
                    }
                    # 系统服务折扣
                    $minDiscount = ($price['member_discount'][$level]);
                    $maxDiscount = ($price['member_discount'][$level]);
                } else {
                    # 合作端上架服务商品  折扣固定
                    $minDiscount = 0.9;
                    $maxDiscount = 0.9;
                }
                if (!empty($member_id)) {
                    if (!empty($car_level)) {
                        $price_min = $price['online'][$car_level];
                        $price_max = $price['online'][$car_level];
                    } else {
                        $price_min = min($price['online']);
                        $price_max = max($price['online']);
                    }
                    if (!empty($price['online'])) {
                        $min_price = $price_min * $minDiscount;
                        $max_price = $price_max * $maxDiscount;
                    }
                    if ($memberInfo['member_level_id'] > 0) {
                        $min_price = $price_min * $minDiscount;
                        $max_price = $price_max * $maxDiscount;
                    }
                } else {
                    if (!empty($car_level)) {
                        $price_min = $price['online'][$car_level];
                        $price_max = $price['online'][$car_level];
                    } else {
                        $price_min = min($price['online']);
                        $price_max = max($price['online']);
                    }
                    if (!empty($price['offline'])) {
                        $min_price = $price_min * $minDiscount;
                        $max_price = $price_max * $maxDiscount;
                    }
                }
                if ($min_price == $max_price) {
                    $list[$k]['price'] = getsPriceFormat($min_price);
                    # 查询是否有可用卡券 并取即将过期的一张

                    $voucher = $VoucherService->memberVoucher($member_id, array("service" => true, "service_id" => $v['id'], "price" => $min_price, "orderby" => "expiration_time asc"));
                    if (!empty($voucher['list'])) {
                        $voucher = $VoucherService->voucherFormat($voucher, $v['id'], false);
                        if ($voucher['num'] > 0) {
                            $voucher_list = array_values($voucher['list']);
                            $list[$k]['price'] -= $voucher_list[0]['voucher_cost'];
                            if ($list[$k]['price'] <= 0) {
                                $list[$k]['price'] = 0;
                            }
                            $list[$k]['price'] = getsPriceFormat($list[$k]['price']);
                        }
                    }
                } else {
                    $list[$k]['price'] = getsPriceFormat($min_price) . "~" . getsPriceFormat($max_price);
                }
            }
            $flag = count($list) == 10 ? true : false;
            return array("status" => true, "list" => $list, "flag" => $flag, "page" => $page);
        } else {
            return array("status" => false);
        }
    }

}
