<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/12
 * Time: 13:28
 */

namespace app\api\controller;


use app\api\ApiService\orderService;
use app\service\IGeTuiService;
use app\service\TimeTask;
use Protocol\Curl;
use Redis\Redis;
use think\Db;
use think\facade\Request;

class Orders extends Common
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 我的预约订单
     */
    function memberAppoint()
    {
        $status = input("post.status");
        $page = input("post.page");
        if ($this->MemberId == 13574) {
            $this->MemberId = 12776;
        }
        $list = orderService::appointOrders($this->MemberId, $status, $page);
        $flag = count($list) == 10 ? true : false;
        if (!empty($list)) {
            return array("status" => true, "list" => $list, "flag" => $flag);
        } else {
            return array("status" => false, "msg" => "暂无信息");
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 订单预约成功
     */
    function appointSuccess()
    {
        Db::table('aaa')->insert(array(
            'info' => (input("post.info")),
            'type' =>  'appointSuccess-payRedis-' . date('Y-m-d H:i:s') . '-orderNumber'
        ));
        $info = json_decode(input("post.info"), true);
        $wx_app_pay = input("post.wx_app_pay");
        if (empty($wx_app_pay)) {
            $wx_app_pay = 1;
        }
        $info['wx_app_pay'] = $wx_app_pay;
        # 清除支付状态缓存
        $_redis = new Redis();
        $_redis->hDel('payRedis', $info['order_number']);
        if ($info['pay_type'] == 5 and $info['pay_price'] > 0) {
            if ($this->MemberInfo['member_balance'] < $info['pay_price']) return array("status" => false, "msg" => "账户余额不足，请充值后再试");
            $this->MemberInfo['member_balance'] = getsPriceFormat($this->MemberInfo['member_balance'] - $info['pay_price']);
            $_redis->hSetJson("memberInfo", $this->MemberId, $this->MemberInfo);
        }
        $orderService = new orderService();
        $orderService->makeOrderServer($info);
        # 发送预约成功通知
        // $IGeTuiService = new IGeTuiService();
        //$IGeTuiService->MessageNotification($this->MemberId, 1, ['appoint_time' => $info['appoint_time'], 'biz_id' => $info['biz_id'], 'server_id' => $info['server_id']]);
        return array("status" => true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户待评论信息
     */
    function memberEvaluation()
    {
        $list = orderService::evaluationOrders($this->MemberId);
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['pay_type'] = getpay_type($v['pay_type']);
            }
        }
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户评论详情
     */
    function memberEvaluationDetail()
    {
        $order_number = input("post.order_number");
        $evaluation_order_type = input("post.evaluation_order_type");
        return array("status" => true, "list" => orderService::formatEvaluationOrders($order_number, $evaluation_order_type), 'shareOrder' => config('share.shareOrder'));
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     * @context 用户评论提交
     */
    function commitComment()
    {
        $order_number = input("post.order_number");
        $feedback = input("post.feedback");
        $list = json_decode(input("post.list"), true);
        $orderService = new orderService();
        $orderService->orderCommentCommit($order_number, $list, $feedback);
        # 完成任务+.'9'=> '服务评论'
        TimeTask::finishTask($this->MemberId, 9);
        return array("status" => true);
    }

    function commitMerchantsComment()
    {
        $order_number = input("post.order_number");
        $feedback = input("post.feedback");
        $list = json_decode(input("post.list"), true);
        $orderService = new orderService();
        $orderService->orderMerchantsCommentCommit($order_number, $list, $feedback);
        return array("status" => true);
    }

    /**
     * @return array
     * @context 微信支付 APP
     */
    function wxPayMent()
    {
        $pay_price = floatval(input("post.pay_price")) * 100;
        if ( $this->MemberId == 9405 or $this->MemberId == 14285 or $this->MemberId == 14392 or $this->MemberId == 13498) {
            $pay_price = 1;
        }
        $orderNumber = input("post.order_number");
        $body = input("post.body");
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        if ($applymark != 3) {
            $order = [
                'out_trade_no' => $orderNumber,
                'total_fee' => $pay_price, // **单位：分**
                'body' => $body,
                //'openid' => $this->MemberInfo['member_openid'],
            ];
            # 发起微信APP支付 , 判断支付状态缓存里面是否有该订单 , 存在将支付方式写进缓存
            $redis = new Redis();
            $payRedis = $redis->hGetJson('payRedis', $orderNumber);
            if (!empty($payRedis)) {
                $payRedis['pay_type'] = 1;
                $payRedis['pay_price'] = input("post.pay_price");
                $payRedis['pay_time'] = date('Y-m-d H:i:s');
                $redis->hSetJson('payRedis', $orderNumber, $payRedis);
            }
            Db::table('aaa')->insert(array(
                'info'=>json_encode($payRedis),
                'type'=>$orderNumber.'-payRedis-'.date('Y-m-d H:i:s')
            ));
            $PayMent = new PayMent();
            $res = $PayMent->wxPayAPP($order)->send();
        } else {
            # 抖音小程序支付
            $postUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/create_order';
            $order = [
                'app_id' => config('ecars.toutiao_Appid'),
                'out_order_no' => $orderNumber,
                'total_amount' => $pay_price,
                'subject' => $body,
                'body' => $body,
                'valid_time' => 180,
                'notify_url' => 'http://back.elianchefu.com/api/PayMent/toutiaoNotify'
            ];
            $order['sign'] = $this->toutiaoSign($order);
            $curl = new Curl();
            $res = $curl->post($postUrl, $order, 'json');
            $resData = json_decode($res, true);
            return array("status" => true, "data" => $resData['data']);
        }
        return array("status" => true, "data" => json_decode($res, true));
    }

    /**
     * @return array
     * @content 头条支付
     */
    function toutiaoPay()
    {
        $pay_price = floatval(input("post.pay_price")) * 100;
        if ( $this->MemberId == 9405) {
            $pay_price = 1;
        }
        $orderNumber = input("post.order_number");
        $body = input("post.body");
        # 抖音小程序支付
        $postUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/create_order';
        $order = [
            'app_id' => config('ecars.toutiao_Appid'),
            'out_order_no' => $orderNumber,
            'total_amount' => $pay_price,
            'subject' => $body,
            'body' => $body,
            'valid_time' => 180,
            'notify_url' => 'http://back.elianchefu.com/api/PayMent/toutiaoNotify'
        ];
        $order['sign'] = $this->toutiaoSign($order);
        $curl = new Curl();
        $res = $curl->post($postUrl, $order, 'json');
        $resData = json_decode($res, true);
        return array("status" => true, "data" => $resData['data']);

    }

    /**
     * @return array
     * @content 抖音支付结果查询
     */
    function toutiaoPayRes()
    {
        $order_number = input("post.order_number");
        $postUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/query_order';
        $order = [
            'app_id' => config('ecars.toutiao_Appid'),
            'out_order_no' => $order_number,
        ];
        $order['sign'] = $this->toutiaoSign($order);
        $curl = new Curl();
        $res = $curl->post($postUrl, $order, 'json');
        $resData = json_decode($res, true);
        return array("status" => true, "data" => $resData['payment_info']);
    }

    /**
     * @param $map
     * @return string
     * @content 抖音  签名 sign
     */
    function toutiaoSign($map)
    {
        $rList = array();
        foreach ($map as $k => $v) {
            if ($k == "other_settle_params" || $k == "app_id" || $k == "sign" || $k == "thirdparty_id")
                continue;
            $value = trim(strval($v));
            $len = strlen($value);
            if ($len > 1 && substr($value, 0, 1) == "\"" && substr($value, $len, $len - 1) == "\"")
                $value = substr($value, 1, $len - 1);
            $value = trim($value);
            if ($value == "" || $value == "null")
                continue;
            array_push($rList, $value);
        }
        array_push($rList, "gAdqhXQldqFXvyeDhZWedo26MjvtzjXEKg1UwItz");
        sort($rList, 2);
        return md5(implode('&', $rList));
    }


    //H5 微信支付
    function wxPayMp()
    {
        $pay_price = floatval(input("post.pay_price")) * 100;
        #查询手机号
        $phone = Db::table('member')->where(array('id' => $this->MemberId))->value('member_phone');
//        if ($phone == '13154199291' or $phone == '13124243401' or $phone == '15840083826' or $phone == '13940482595' or $this->MemberId == 9405) {
//            $pay_price = 1;
//        }
        if($phone=='13066700403' or $phone == '15754242415'){
            $pay_price = 1;
        }
        $body = input("post.body");
        $order = [
            'out_trade_no' => input("post.order_number"),
            'total_fee' => $pay_price, // **单位：分**
            'body' => $body,
            'openid' => input('post.openid'),
        ];
        $redis = new Redis();
        $payRedis = $redis->hGetJson('payRedis', input("post.order_number"));
        $other_info = input('post.other_info');
        if (!empty($payRedis)) {
            $payRedis['pay_type'] = 1;//微信公众号支付
            $payRedis['pay_price'] = $pay_price;
            $payRedis['pay_time'] = date('Y-m-d H:i:s');
            $payRedis['voucherInfo'] = $other_info['voucherInfo'];
            $re = $redis->hSetJson('payRedis', input("post.order_number"), $payRedis);
            Db::table('aaa')->insert(array('info'=>json_encode($payRedis),'type'=>$this->MemberId.'开始支付'.date('Y-m-d H:i:s').'-'.$re));
        }
        if($body=='付款给商家'){
            $payRedis = array();
            $payRedis['pay_type'] = 1;//微信公众号支付
            $payRedis['pay_price'] = $pay_price;
            $payRedis['pay_time'] = date('Y-m-d H:i:s');
            $payRedis['type'] = 'businessOrder';
            $payRedis['info'] = json_encode($other_info);
            $re = $redis->hSetJson('payRedis', input("post.order_number"), $payRedis);
            Db::table('aaa')->insert(array('info'=>json_encode($other_info),'type'=>$this->MemberId.'付款给商家'.date('Y-m-d H:i:s').$re));
            Db::table('aaa')->insert(array('info'=>json_encode($redis->hGet('payRedis')),'type'=>$this->MemberId.'付款给商家-redis-'.date('Y-m-d H:i:s').$re));
        }
        $PayMent = new PayMent();
        $res = $PayMent->wxPayMp($order);
        return array("status" => true, "data" => json_decode($res, true));
    }

    /**
     * @return array
     * @throws \Yansongda\Pay\Exceptions\GatewayException
     * @throws \Yansongda\Pay\Exceptions\InvalidArgumentException
     * @throws \Yansongda\Pay\Exceptions\InvalidSignException
     * @context 订单状态查询
     */
    function wxPayStatusQuery()
    {
        $order_number = input("post.order_number");
        $PayMent = new PayMent();
        $res = $PayMent->orderStatusQuery($order_number, 1);
        return array("status" => true, "result" => $res);
    }

    function aliPayMent()
    {
        $pay_price = floatval(input("post.pay_price"));
        #查询手机号
        $phone = Db::table('member')->where(array('id' => $this->MemberId))->value('member_phone');
        if($phone=='13940482595' or $phone=='13066700403' or $phone == '15840083826' or $phone == '15754242415'){
            $pay_price=0.01;
        }
        $orderNumber = input("post.order_number");
        $body = input("post.body");
        $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
        if ($applymark != 3) {
            $order = [
                'out_trade_no' => $orderNumber,
                'total_amount' => $pay_price,
                'subject' => $body,
            ];
            # 发起微信APP支付 , 判断支付状态缓存里面是否有该订单 , 存在将支付方式写进缓存
            $redis = new Redis();
            $payRedis = $redis->hGetJson('payRedis', $orderNumber);
            if (!empty($payRedis)) {
                $payRedis['pay_type'] = 2;
                $payRedis['pay_time'] = date('Y-m-d H:i:s');
                $redis->hSetJson('payRedis', $orderNumber, $payRedis);
            }
            $PayMent = new PayMent();
            $res = $PayMent->aliPayAPP($order)->send();
            return array("status" => true, "data" => json_decode($res, true));
        } else {
            # 抖音小程序支付
            $pay_price = $pay_price * 100;
            $postUrl = 'https://developer.toutiao.com/api/apps/ecpay/v1/create_order';
            $order = [
                'app_id' => config('ecars.toutiao_Appid'),
                'out_order_no' => $orderNumber,
                'total_amount' => $pay_price,
                'subject' => $body,
                'body' => $body,
                'valid_time' => 180,
                'notify_url' => 'http://back.elianchefu.com/api/PayMent/toutiaoNotify'
            ];
            $order['sign'] = $this->toutiaoSign($order);
            $curl = new Curl();
            $res = $curl->post($postUrl, $order, 'json');
            $resData = json_decode($res, true);
            return array("status" => true, "data" => $resData['data'], 'res' => $res);
        }
    }

    function aliPayStatusQuery()
    {
        $order_number = input("post.order_number");
        $PayMent = new PayMent();
        $res = $PayMent->orderStatusQuery($order_number, 2);
        return array("status" => true, "result" => $res);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 服务预约时间
     */
    function serviceAppointTime()
    {
        $service_id = input("post.service_id");
        $service_time = input("post.service_time");
        $biz_id = input("post.biz_id");
        $biz_start = date("H:i:s", strtotime(input("post.biz_start")));
        $biz_end = date("H:i:s", strtotime(input("post.biz_end")));
        $car_lience = input("post.car_lience");
        $section_start = NULL;
        $nowTime = date("Y-m-d H:i:s");
        $add_days = 2;
        if (empty($biz_id) or empty($service_time)) {
            return array("status" => false);
        }
        $bizState = Db::table('biz')->where(array('id'=>$biz_id))->value('open_state');
        if($bizState==2) {
            return array("status" => false);
        }
        if (!empty($car_lience)) {
            # 通过车牌号、门店、当前时间、查询是否有预约订单
            $appoint_order = Db::table("orders")->field("order_number,order_appoin_time")
                ->where(array("member_id" => $this->MemberId, "order_type" => 3, "order_status" => 1, "biz_id" => $biz_id,
                    "car_liences" => $car_lience))->where("order_appoin_time>'" . date("Y-m-d H:i:s") . "'")->find();
            if (!empty($appoint_order)) {
                $_date = date("Y-m-d", strtotime($appoint_order['order_appoin_time']));
                if ($_date == date("Y-m-d")) {
                    $title = "今天";
                } elseif ($_date == date("Y-m-d", strtotime("+1 day"))) {
                    $title = '明天';
                } elseif ($_date == date("Y-m-d", strtotime("+2 day"))) {
                    $title = '后天';
                } else {
                    $title = '大后天';
                }
                $_hour = date("H", strtotime($appoint_order['order_appoin_time']));
                $_minute = date("i", strtotime($appoint_order['order_appoin_time']));
                $_return[$_date] = array("title" => $title, "date" => $_date, "hours" => array(array("num" => $_hour, "mins" => array($_minute))));
                return array("status" => true, "list" => array_values($_return), "already" => 1, "already_order_number" => $appoint_order['order_number']);
            }
        }
        # 判断当前时间是否在营业时间里
        if (date("H:i:s") < $biz_start) {
            # 在开始营业之前  开始时间是当天营业时间 并查看距离营业时间开始是否大于30分钟
            if (date("Y-m-d {$biz_start}", strtotime("-30 minutes")) >= $nowTime) {
                # 预约开始时间为营业开始时间
                $section_start = date("Y-m-d {$biz_start}");
            }
        } elseif (date("H:i:s") > $biz_end) {
            # 只可预约第二天的 营业时间
            $section_start = date("Y-m-d {$biz_start}", strtotime("+1 day"));
            $add_days = 1;
        } else {
            # 在营业时间内 判断距离关闭时间是否大于半小时并且大于服务时长
            if (date("Y-m-d {$biz_end}", strtotime("-30 minutes")) >= $nowTime and date("Y-m-d {$biz_end}", strtotime("-{$service_time} minutes")) >= $nowTime) {
                # 可以预约   预约开始时间为当前时间之后的半小时并且无其他排队订单
                $section_start = date("Y-m-d H:i:00", strtotime("+30 minutes"));
            } else {
                $section_start = date("Y-m-d {$biz_start}:00", strtotime("+1 day"));
            }
        }
        # 判断开始时间是否是被10整除的数
        if (date("i", strtotime($section_start)) % 10 != 0) {
            $start_minutes = (intval(date("i", strtotime($section_start)) / 10) + 1) * 10 - date("i", strtotime($section_start));

            $section_start = date("Y-m-d H:i:00", strtotime("+{$start_minutes} minutes", strtotime($section_start)));
        }
        # 计算结束时间 为开始时间加3天的关闭时间之前服务时长时间或半小时
        if ($service_time < 30) {
            # 结束时间为关闭前半小时
            $section_end = date("Y-m-d {$biz_end}", strtotime("+{$add_days} days", strtotime($section_start)));
            $section_end = date("Y-m-d H:i:s", strtotime("-30 minutes", strtotime($section_end)));
        } else {
            # 结束时间为关闭前的服务时长
            $section_end = date("Y-m-d {$biz_end}", strtotime("+{$add_days} days", strtotime($section_start)));
            $section_end = date("Y-m-d H:i:s", strtotime("-{$service_time} minutes", strtotime($section_end)));
        }

        # 罗列从开始时间 到结束时间 间隔为5分钟服务时长  并去除营业时间之外时间的  时间数组
        $current_time = $section_start;
        $_Time_array = array();
        while ($current_time < $section_end) {
            array_push($_Time_array, $current_time);
            a:
            $current_time = date("Y-m-d H:i:00", strtotime("+5 minutes", strtotime($current_time)));//{$service_time}
            if (date("H:i:00", strtotime($current_time)) < $biz_start or date("H:i:00", strtotime($current_time)) > date("H:i:00", strtotime("-{$service_time} minutes", strtotime($biz_end)))
                or date("H:i:00", strtotime($current_time)) > date("H:i:00", strtotime($section_end))) {
                goto a;
            }
        }
        # 查询从开始到结束时间是否有排队订单 是否有预约订单
        $lineupOrders = orderService::serverLineUp($service_id, $biz_id, $section_start, $section_end);
        $_line_biz_order = array();
        $_line_biz_time = NULL;
        $_line_up = array();
        $_all_line_up = array();
        if (!empty($lineupOrders)) {
            foreach ($lineupOrders as $k => $v) {
                $_line_up[$v['order_type']][] = $v;
            }
            # 首先确定预约订单的服务时间范围
            if (!empty($_line_up[3])) {
                foreach ($_line_up[3] as $k => $v) {
                    array_push($_all_line_up, array(date('Y-m-d H:i:00', strtotime("-{$service_time} minutes +5 minutes", strtotime($v['order_appoin_time']))), date("Y-m-d H:i:00", strtotime("+{$service_time} minutes", strtotime($v['order_appoin_time'])))));

                }
//                if($this->MemberId==9405){
//                    //dump($_Time_array);
//                    dump("====================================");
//                    dump($_all_line_up);
//                }
                # 筛选时间段 使用正常订单填充空缺时间
                foreach ($_Time_array as $k => $v) {
                    $_screen = orderService::screenTimeSection($v, $_all_line_up);
                    if ($_screen) {
                        unset($_Time_array[$k]);
                        array_push($_line_biz_order, $v);
                    }
                }

                if (!empty($_line_up[2])) {
                    $num = count($_line_up[2]);
                    //$_current = 0;
                    $_current_screen_time = $_Time_array[0];
                    for ($i = 0; $i < $num; $i++) {
                        $_end_screen_time = date("Y-m-d H:i:00", strtotime("+{$service_time} minutes", strtotime($_current_screen_time)));
                        foreach ($_Time_array as $tk => $tv) {
                            # 超出部分停止循环
                            if ($tv >= $_end_screen_time) {
                                break;
                            }
                            if ($tv >= $_current_screen_time and $tv < $_end_screen_time) {
                                unset($_Time_array[$tk]);
                            }
                        }
                        $_current_screen_time = $_end_screen_time;
                    }
//                    foreach ($_line_up[2] as $k => $v) {
//
//                        if ($num > 0) {
//                            array_push($_all_line_up, array($_line_biz_order[$_current],
//                                date("Y-m-d H:i:00", strtotime("+{$service_time} minutes", strtotime($_line_biz_order[$_current])))));
//                            $_current++;
//                            $num--;
//                        } else {
//                            $_current_end_time = date("Y-m-d H:i:00", strtotime("+{$service_time} minutes", strtotime($_current_screen_time)));
//                            array_push($_all_line_up, array($_current_screen_time, $_current_end_time));
//                            $_current_screen_time = $_current_end_time;
//                        }
//                    }
                }

            } else {
                # 无预约订单  从开始时间开始往后排
                $_all_time = $service_time * count($_line_up[2]);
                $_current_screen_time = date("Y-m-d H:i:00", strtotime("+{$_all_time} minutes", strtotime($section_start)));
                foreach ($_Time_array as $tk => $tv) {
                    # 超出部分停止循环
                    if ($tv >= $_current_screen_time) {
                        break;
                    }
                    if ($tv < $_current_screen_time) {
                        unset($_Time_array[$tk]);
                    }
                }
                //array_push($_all_line_up, array($section_start, date("Y-m-d H:i:00", strtotime("+{$_all_time} minutes", strtotime($section_start)))));
            }
        }
        $_Time_array = array_values($_Time_array);
        # 最后筛选时间段 并更改时间段格式
        $_return = array();
        foreach ($_Time_array as $k => $v) {
//            if (!empty($_all_line_up)) {
//                $_continue = false;
//                foreach ($_all_line_up as $ak => $av) {
//                    if ($v >= $av[0] and $v < $av[1]) {
//                        unset($_Time_array[$k]);
//                        $_continue = true;
//                        break;
//                    }
//                }
//                if ($_continue) {
//                    $_continue = false;
//                    continue;
//                }
//            }
            $_date = date("Y-m-d", strtotime($v));
            $_hour = date("H", strtotime($v));
            $_minute = date("i", strtotime($v));
            if (!key_exists($_date, $_return)) {
                if ($_date == date("Y-m-d")) {
                    $title = "今天";
                } elseif ($_date == date("Y-m-d", strtotime("+1 day"))) {
                    $title = '明天';
                } elseif ($_date == date("Y-m-d", strtotime("+2 day"))) {
                    $title = '后天';
                } else {
                    $title = '大后天';
                }
                $_return[$_date] = array("title" => $title, "date" => $_date, "hours" => array($_hour => array("num" => $_hour, "mins" => array($_minute))));
            } else {
                if (key_exists($_hour, $_return[$_date]['hours'])) {
                    array_push($_return[$_date]['hours'][$_hour]['mins'], $_minute);
                } else {
                    $_return[$_date]['hours'][$_hour] = array("num" => $_hour, "mins" => array($_minute));
                }
            }
        }
        if ($biz_id == 1 and ($service_id == 9 or $service_id == 10)) {
            $mapping_date = date("Y-m-d");
            $mapping_start = date("Y-m-d H:i:s");
            $mapping_end = date("Y-m-d 23:59:59");
            $order_num = orderService::serverLineUp($service_id, $biz_id, $mapping_start, $mapping_end);
            if (count($order_num) > 0) {
                if (!empty($_return[$mapping_date]['hours'])) {
                    for ($i = 0; $i < count($order_num); $i++) {
                        if (empty($_return[$mapping_date]["hours"])) {
                            unset($_return[$mapping_date]);
                            break;
                        }
                        foreach ($_return[$mapping_date]["hours"] as $k => $v) {
                            if (empty($v['mins'])) {
                                unset($_return[$mapping_date]["hours"][$k]);
                            } else {
                                $_return[$mapping_date]["hours"][$k]['mins'] = array_values($_return[$mapping_date]["hours"][$k]['mins']);
                                unset($_return[$mapping_date]["hours"][$k]['mins'][0]);
                                $_return[$mapping_date]["hours"][$k]['mins'] = array_values($_return[$mapping_date]["hours"][$k]['mins']);
                                break;
                            }

                        }
                    }
                }
            }
        }
        if (!empty($_return)) {
            foreach ($_return as $k => $v) {
                if (!empty($v['hours'])) {
                    $_return[$k]['hours'] = array_values($v['hours']);
                }
            }
        }

        return array("status" => true, "list" => array_values($_return), "already" => 2);
    }
}
