<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/8
 * Time: 15:36
 */

namespace app\api\controller;


use app\api\ApiService\IntegralService;
use app\service\TimeTask;
use Redis\Redis;
use think\Db;

class Club extends Common
{


    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 分享文章加积分
     */
    function shareArticle()
    {
        # 查询用户今日是否是第一次分享文章
        $already_share = Db::table("log_integral")->where(array("member_id" => $this->MemberId))->where("date(integral_time)='" . date("Y-m-d") . "' and type=1")->find();
        if (empty($already_share)) {
            # 增加用户分享文章积分
            $integral_num = IntegralService::integralSetItem("is_share_club");
            $add = IntegralService::addMemberIntegral($this->MemberId, 8, $integral_num, 1);
            $this->MemberInfo['member_integral'] += intval($integral_num);
            $_redis = new Redis();
            $_redis->hSetJson("memberInfo", $this->MemberId, $this->MemberInfo);
            # 完成任务 '6'=> '分享文章',
            TimeTask::finishTask($this->MemberId, 6);
            return array("status" => true, "msg" => "已入账+{$integral_num}积分");
        } else {
            return array("status" => true, "msg" => "今日已分享");
        }
    }
}
