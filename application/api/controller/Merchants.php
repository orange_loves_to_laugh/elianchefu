<?php


namespace app\api\controller;

use app\api\ApiService\AdvanceService;
use app\api\ApiService\IntegralService;
use app\api\ApiService\MemberService;
use app\api\ApiService\MerchantService;
use app\api\ApiService\orderService;
use app\api\ApiService\PropertyService;
use app\api\ApiService\VoucherService;
use app\businessapi\ApiService\TaskService;
use app\businessapi\controller\Task;
use Protocol\Curl;
use Redis\Redis;
use think\Controller;
use think\Db;
use think\validate\ValidateRule;
use think\facade\Request;

class Merchants extends Controller
{
    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 首页推荐商户
     */
    function recommendMerchant()
    {
        $member_id = input("post.member_id");
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if (empty($province) or empty($city)) {
            return array("status" => false);
        }
        # 判断鲅鱼圈区看不到所有
        $detailAddress = getAddressResolution($lon, $lat);
        $detail_add_a = json_decode($detailAddress, true);
        if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

            $_district = $detail_add_a['regeocode']['addressComponent']['district'];
            if ($_district == '鲅鱼圈区') {
                return array("status" => false);
            }
        }
        if (!config('share.toutiaoShow') && Request::header("applymark") == 3) {
            return array("status" => false, "msg" => "暂无商户信息");
        }
        $where = "province like '%" . $province . "%' and city like '%" . $city . "%'";
        // 2021-12-23 加判断 不展示鲅鱼圈的商家
        $where .= " and area != '鲅鱼圈区'";
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and create_time<'2021-07-01 00:00:00' ";
            }
        }
        $where .= " and (
                (start_time!='00:00' or end_time != '00:00') and 
                (
                    (UNIX_TIMESTAMP(start_time)<=UNIX_TIMESTAMP(end_time) and start_time<='" . date("H:i:s") . "' and end_time>='" . date("H:i:s") . "') 
                    or 
                    (UNIX_TIMESTAMP(start_time)>UNIX_TIMESTAMP(end_time) and 
                        (
                            (end_time>'" . date("H:i:s") . "') or (start_time<='" . date("H:i:s") . "')
                         )
                    )
                )
            )
            or
            (start_time='00:00' and end_time = '00:00')";
        $where .= " and (temporary_close<=0 or (temporary_close>0 and (close_time>'" . date("Y-m-d H:i:s") . "' 
            or date_add(close_time,interval temporary_close hour))<'" . date("Y-m-d H:i:s") . "'))";
        $where_toutiao = '';
        if (Request::header("applymark") == 3) {
            $where .= " and open_toutiao = 1";
            $where_toutiao = ' open_toutiao = 1';
        }

        $where .= " and m.id != 1387 and m.id != 1388";
        $MerchantService = new MerchantService();
        $list = array();
        # 优质商家6个
        $num = 6;
        $repeat_id_array = array();
        for ($i = 1; $i <= 4; $i++) {
            if (!empty($repeat_id_array)) {
                $where .= " and m.id not in (" . implode(',', $repeat_id_array) . ") ";
            }
            $res = $MerchantService->LevelMerchant($i, $lat, $lon, 2, $num, true, $where, $where_toutiao);
            if (!empty($res)) {
                $list = array_merge($list, $res);
                array_push($repeat_id_array, $res[0]['id']);
                $num -= count($res);
                if ($num <= 0) {
                    break;
                }
            }
        }
        # 4个推荐商家
//        $recommend_num = 2;
//        $recommend_list =array();
//        $repeat_array=NULL;
//        if(!empty($list)){
//            $repeat_array=implode(',',array_column($list,'id'));
//            $where.=" and merchants.id not in (".$repeat_array.")";
//        }
//        for($i=2;$i<=4;$i++){
//            $res = $MerchantService->LevelMerchant($i,$lat,$lon,2,$recommend_num,true,$where);
//            if(!empty($res)){
//                $recommend_list = array_merge($recommend_list,$res);
//                $recommend_num-=count($res);
//                if($recommend_num<=0){
//                    break;
//                }
//            }
//        }
//        $list = $this->array_insert($list,2,$recommend_list);
        $_member_like = array();
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2))->select();
            if (!empty($support)) {
                $_member_like = array_column($support, 'club_id');
            }
        }
        foreach ($list as $k => $v) {
            $list[$k]['level_title'] = $this->merchantLevel($v['level']);
            $list[$k]['userSupport'] = 0;
            if (!empty($_member_like) and in_array($v['id'], $_member_like)) {
                $list[$k]['userSupport'] = 1;
            }
            $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
            $list[$k]['recommend_pro'] = MerchantService::merchantsRecommendPro($v['id'], 1);
            $list[$k]['giving_score'] = 1;
//            if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                $list[$k]['giving_score'] = 1;
//            } else {
//                $list[$k]['giving_score'] = 2;
//            }
        }
        return array("status" => true, "list" => $list);
    }

    function recommendMerchantsPro()
    {
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if (empty($province) or empty($city)) {
            return array("status" => false);
        }
        # 判断鲅鱼圈区看不到所有
        $detailAddress = getAddressResolution($lon, $lat);
        $detail_add_a = json_decode($detailAddress, true);
        if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

            $_district = $detail_add_a['regeocode']['addressComponent']['district'];
            if ($_district == '鲅鱼圈区') {
                return array("status" => false);
            }
        }
        $member_id = input("post.member_id");
        $where = " mp.id>0 ";
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and m.create_time<'2021-07-01 00:00:00' ";
            }
        }
        # 随机取6个 每个商户只展示一个商品 不足6个再用相同商户的补充
        $list = Db::table("merchants_pro mp")
            ->field("distinct mp.merchants_id,mp.*,m.title merchants_title,
            ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$lat}*PI()/180-lat*PI()/180)/2),2)+COS({$lat}*PI()/180)*COS(lat*PI()/180)*POW(SIN((({$lon}*PI()/180)-(lon*PI()/180))/2),2))),2) AS distance")
            ->join("merchants m", "m.id=mp.merchants_id  and m.level = 1 
            and m.province like '%" . $province . "%' and m.city like '%" . $city . "%' and m.status=1
            ")
            ->where("m.id!=1 and m.id!=2")
            ->where(array("mp.status" => 1))
            ->where("mp.recommend_status=1 or mp.id = (select mps.id from merchants_pro mps where mps.merchants_id = mp.merchants_id order by id asc limit 1)")
            ->where("mp.start_time<='" . date("Y-m-d") . "' and mp.end_time >='" . date("Y-m-d") . "' and (mp.num - mp.sold_num)>0")
            ->where($where)
            ->orderRaw("rand()")
            ->order("distance asc")
            ->limit(6)
            ->select();
        if (count($list) < 6) {
            if (!empty($list)) {
                $id_con = implode(',', array_column($list, 'id'));
                $where .= " and mp.id not in (" . $id_con . ")";
            }
            $news = Db::table("merchants_pro mp")
                ->field("mp.*,m.title merchants_title,
            ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$lat}*PI()/180-lat*PI()/180)/2),2)+COS({$lat}*PI()/180)*COS(lat*PI()/180)*POW(SIN((({$lon}*PI()/180)-(lon*PI()/180))/2),2))),2) AS distance")
                ->join("merchants m", "m.id=mp.merchants_id 
            and m.province like '%" . $province . "%' and m.city like '%" . $city . "%' and m.status=1
            ")
                ->where("m.id!=1 and m.id!=2")
                ->where(array("mp.status" => 1))
                ->where("mp.start_time<='" . date("Y-m-d") . "' and mp.end_time >='" . date("Y-m-d") . "' and (mp.num - mp.sold_num)>0")
                ->where($where)
                ->orderRaw("rand()")
                ->order("distance asc")
                ->limit(6 - count($list))
                ->select();
            if (!empty($news)) {
                $list = array_merge($list, $news);
            }
        }
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['price'] = getsPriceFormat($v['price']);
                $list[$k]['original_price'] = getsPriceFormat($v['original_price']);
                $list[$k]['discount'] = round(($v['price'] / $v['original_price']) * 10, 2);
//                if($v['price']==$v['original_price']){
//                    $list[$k]['discount']=0;
//                }else{
//                    $list[$k]['discount'] = getsPriceFormat($v['price'] / $v['original_price']) * 10;
//                }

            }
        }
        return array("status" => true, "list" => $list);

    }

    /**
     * @param $array
     * @param $position
     * @param $insert_array
     * @return array
     * @context 将一个数组插入到另一个数组的指定位置
     */
    function array_insert(&$array, $position, $insert_array)
    {
        $first_array = array_splice($array, 0, $position);
        $array = array_merge($first_array, $insert_array, $array);
        return $array;
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 用户点赞商家
     */
    function merchantSupport()
    {
        $id = input("post.id");
        $member_id = input("post.member_id");
        $opera = input("post.opera");
//        if($opera>0){
//            $opera = 0;
//            # 取消点赞 扣除点赞数量  删除点赞记录
//            Db::table("merchants")->where(array("id"=>$id))->setDec("thumbup_num",1);
//            Db::table("member_like")->where(array("type"=>2,"member_id"=>$member_id,"club_id"=>$id))->delete();
//        }else{
        $opera = 1;
        Db::table("merchants")->where(array("id" => $id))->setInc("thumbup_num", 1);
        # 查询是否点赞过
        $isSupport = Db::table("member_like")->where(array("type" => 2, "member_id" => $member_id, "club_id" => $id))->count();
        if ($isSupport == 0) {
            Db::table("member_like")->insert(array("type" => 2, "member_id" => $member_id, "club_id" => $id, "like_time" => date("Y-m-d H:i:s")));
        }

        //}
        #添加任务记录 点赞记录类型
        $params['merchants_id'] = $id;
        TaskService::getTaskPaySucess($params, 8);
        return array("status" => true, "opera" => $opera);
    }

    /**
     * @param $level
     * @return string
     * @context 商户等级
     */
    function merchantLevel($level)
    {
        switch ($level) {
            case 1 :
                return "优质商家";
                break;
            case 2 :
                return "推荐商家";
                break;
            case 3 :
                return "诚信商家";
                break;
            default:
                return "普通商家";
                break;
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取商户分类
     */
    function getsMerchantCate()
    {
        $is_type = input("post.is_type");
        $mark = 'more';
        $isHead = input("post.is_head");
        $MerchantService = new MerchantService();
        $list = $MerchantService->merchantCate($mark, $is_type);
        if (empty($isHead)) {
            array_unshift($list, array("id" => 0, "name" => "全部", "child" => array(array("id" => 0, "name" => "全部"))));
        }
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商户列表
     */
    function getsMerchantListes()
    {
        $member_id = input("post.member_id");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $province = input("post.province");
        $city = input("post.city");
        $page = input("post.page");
        $is_type = input("post.is_type");
        $show_mark = input("post.show_mark");
        $search_merchants_id = input("post.search_merchants_id");
        if (!config('share.toutiaoShow') && Request::header("applymark") == 3) {
            return array("status" => false, "msg" => "暂无商户信息");
        }
        $where = "m.id >0 and m.is_type = {$is_type} ";
        $MerchantService = new MerchantService();
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $_member_like = array();
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2))->select();
            if (!empty($support)) {
                $_member_like = array_column($support, 'club_id');
            }
        }
        $search = input("post.search");
        if (!empty($search)) {
            $search = implode("%", mb_str_splits($search));
            $where .= " and ((m.title like '%" . $search . "%') or m.keywords like '%" . $search . "%')";
        }
        $class_a = input("post.class_a");
        if (!empty($class_a)) {
            # 获取这个分类等级下的所有分类
            $child = $MerchantService->getsCateChild(2, $class_a);
            if (!empty($child)) {
                $where .= " and mcr.cate_pid = {$class_a}";
                $child_str = implode(',', array_column($child, 'id'));
                $where .= " and mcr.cate_id in (" . $child_str . ")";
            }
        }
        $class_b = input("post.class_b");
        if (!empty($class_b)) {
            if ($is_type == 1) {
                $where .= " and mcr.cate_pid = {$class_b}";
            } else {
                $where .= " and mcr.cate_id = {$class_b}";
            }

        }
        $merchant_level = input("post.merchant_level");
        if (!empty($merchant_level)) {
            $where .= " and m.level = {$merchant_level}";
        }
        $order = "distance asc";
        $order_line = input("post.order_line");
        if (!empty($order_line)) {
            if ($order_line == 'up') {
                $order = "m.thumbup_num desc";
            } else {
                $order = "m.thumbup_num asc";
            }
        }
        if (!empty($search_merchants_id)) {
            $where .= " and m.id = {$search_merchants_id}";
        }
        $length = 10;
        $first_array = array();
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if ($page == 0 and empty($search) and empty($class_a) and empty($class_b) and empty($order_line) and empty($merchant_level) and empty($search_merchants_id)) {

            $firstwhere = "province like '%" . $province . "%' and city like '%" . $city . "%'";
            if (!empty($class_b)) {
                $firstwhere .= " and mcr.cate_pid = {$class_b}";
            }
            if (Request::header("applymark") == 3) {
                $firstwhere .= " and open_toutiao = 1";
            }
            $_repeat = array();
            # 查询最高级的3个
            $n = 1;
            for ($j = 0; $j < 3; $j++) {
                if (count($first_array) >= 3 or $n > 3) {
                    break;
                }
                for ($i = 1; $i < 2; $i++) {
                    if (!empty($_repeat)) {
                        $_repeat_str = implode(',', $_repeat);
                        $firstwhere .= " and merchants.id not in (" . $_repeat_str . ")";
                    }
                    $res = $MerchantService->LevelMerchant($n, $lat, $lon, $is_type, 1, true, $firstwhere);
                    if (!empty($res)) {
                        $first_array = array_merge($first_array, $res);
                        array_push($_repeat, $res[0]['id']);
                        if (count($first_array) >= 3) {
                            break;
                        }
                    } else {
                        $n++;
                    }
                }
            }
        }
        $repeat_array = null;
        if (!empty($first_array)) {
            $repeat_array = implode(',', array_column($first_array, 'id'));
            $length = $length - count($first_array);
            $where .= " and m.id not in (" . $repeat_array . ")";
        }
        $receive_repeat = input("post.repeat");
        if (!empty($receive_repeat) and $page != 0) {
            $where .= " and m.id not in ({$receive_repeat})";
        }
        $where_toutiao = "";
        if (Request::header("applymark") == 3) {
            $where_toutiao = "open_toutiao = 1";
        }
        if (!empty($show_mark)) {
            $giving_score_where = array('m.giving_score' => 1);
        } else {
            $giving_score_where = array();
        }
        //&& $member_id != 12085
        if ($member_id != 3630) {
            $list = Db::table("merchants m")
//            ->field("distinct m.*,ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$lat}*PI()/180-m.lat*PI()/180)/2),2)+COS({$lat}*PI()/180)*COS(m.lat*PI()/180)*POW(SIN((({$lon}*PI()/180)-(m.lon*PI()/180))/2),2))),2) AS distance,
//            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->field("distinct m.id mtid, m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id ")
                ->where(array("m.status" => 1, "m.is_type" => $is_type))
                ->where($giving_score_where)
                ->where($where_toutiao)
                ->where("m.start_time is not null and m.end_time is not null")
                ->where("m.province like '%" . $province . "%' and m.city like '%" . $city . "%'")
//            ->where("(
//                (m.start_time!='00:00' and m.end_time != '00:00') and
//                (
//                    (UNIX_TIMESTAMP(m.start_time)>UNIX_TIMESTAMP(m.end_time) and m.start_time<='".date("H:i:s")."' and m.end_time>='".date("H:i:s")."')
//                    or
//                    (UNIX_TIMESTAMP(m.start_time)<=UNIX_TIMESTAMP(m.end_time) and
//                        (
//                            (m.end_time>'".date("H:i:s")."') or (m.start_time<='".date("H:i:s")."')
//                         )
//                    )
//                )
//            )
//            or
//            (m.start_time='00:00' and m.end_time = '00:00')
//            ")
//            ->where("m.temporary_close<=0 or (m.temporary_close>0 and (m.close_time>'".date("Y-m-d H:i:s")."'
//            or date_add(m.close_time,interval m.temporary_close hour))<'".date("Y-m-d H:i:s")."')")
                ->where($where)
                ->where("m.id != 1 and m.id != 2 and m.id != 1387 and m.id != 1388")
                ->order($order)
                ->limit($page_num, $length)->select();
        } else {
            $list = Db::table("merchants m")
//            ->field("distinct m.*,ROUND(6378.138*2*ASIN(SQRT(POW(SIN(({$lat}*PI()/180-m.lat*PI()/180)/2),2)+COS({$lat}*PI()/180)*COS(m.lat*PI()/180)*POW(SIN((({$lon}*PI()/180)-(m.lon*PI()/180))/2),2))),2) AS distance,
//            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->field("distinct m.id mtid, m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id ")
                ->where(array("m.status" => 1, "m.is_type" => $is_type))
                ->where($giving_score_where)
                ->where($where_toutiao)
                ->where("m.start_time is not null and m.end_time is not null")
                ->where("m.id = 1 or m.id = 2")
                ->order($order)
                ->limit($page_num, $length)->select();
        }
        $list = array_merge($first_array, $list);
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                $list[$k]['userSupport'] = 0;
                if (!empty($_member_like) and in_array($v['id'], $_member_like)) {
                    $list[$k]['userSupport'] = 1;
                }
                $list[$k]['closeStore'] = 1;
                if ($v['start_time'] != '00:00' and $v['end_time'] != "00:00") {
                    if (date("H:i:s", strtotime($v['start_time'])) > date("H:i:s", strtotime($v['end_time']))) {
                        if ($v['start_time'] > date("H:i:s") and $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    } else {
                        if ($v['start_time'] > date("H:i:s") or $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    }
                }
                if ($v['temporary_close'] > 0 and $v['close_time'] <= date("Y-m-d H:i:s")
                    and $v['close_time'] >= date("Y-m-d H:i:s", strtotime("-{$v['temporary_close']} hours"))) {
                    $list[$k]['closeStore'] = 2;
                }
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                $list[$k]['plurs_star'] = 5 - $v['start'];
                $list[$k]['giving_score'] = 1;
//                if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                    $list[$k]['giving_score'] = 1;
//                } else {
//                    $list[$k]['giving_score'] = 2;
//                }
            }
            $flag = count($list) == 10 ? true : false;
            return array("status" => true, "list" => $list, "flag" => $flag, "repeat" => $repeat_array);
        } else {
            return array("status" => false, "msg" => "暂无商户信息");
        }

    }

    function getsMerchantListssssssssss()
    {
        $member_id = input("post.member_id");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $province = input("post.province");
        $city = input("post.city");
        $page = input("post.page");
        $is_type = input("post.is_type");
        $show_mark = input("post.show_mark");
        $search_merchants_id = input("post.search_merchants_id");

        if (!config('share.toutiaoShow') && Request::header("applymark") == 3) {
            return array("status" => false, "msg" => "暂无商户信息");
        }
        $where = "m.id >0 and m.is_type = {$is_type} ";
        // 2021-12-23 加判断 不展示鲅鱼圈的商家
        $where .= " and m.area != '鲅鱼圈区'";
        $MerchantService = new MerchantService();
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $_member_like = array();
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2))->select();
            if (!empty($support)) {
                $_member_like = array_column($support, 'club_id');
            }
        }
        $search = input("post.search");
        if (!empty($search)) {
            $search = implode("%", mb_str_splits($search));
            $where .= " and ((m.title like '%" . $search . "%') or m.keywords like '%" . $search . "%')";
        }
        $class_a = input("post.class_a");
        if (!empty($class_a)) {
            # 获取这个分类等级下的所有分类
            $child = $MerchantService->getsCateChild(2, $class_a);
            if (!empty($child)) {
                $where .= " and mcr.cate_pid = {$class_a}";
                $child_str = implode(',', array_column($child, 'id'));
                $where .= " and mcr.cate_id in (" . $child_str . ")";
            }
        }
        $class_b = input("post.class_b");
        if (!empty($class_b)) {
            if ($is_type == 1) {
                $where .= " and mcr.cate_pid = {$class_b}";
            } else {
                $where .= " and mcr.cate_id = {$class_b}";
            }

        }
        $merchant_level = input("post.merchant_level");
        if (!empty($merchant_level)) {
            $where .= " and m.level = {$merchant_level}";
        }
        $order = "distance asc";
        $order_line = input("post.order_line");
        if (!empty($order_line)) {
            if ($order_line == 'up') {
                $order = "m.thumbup_num desc";
            } else {
                $order = "m.thumbup_num asc";
            }
        }
        if (!empty($search_merchants_id)) {
            $where .= " and m.id = {$search_merchants_id}";
        }

        if (Request::header("applymark") == 3) {
            $where .= " and open_toutiao = 1";
        }
        if (!empty($show_mark)) {
//            $where .= " and m.giving_score = 1";
            # 查优质商家
            $where .= " and m.level = 1 and expire_time > '" . date('Y-m-d H:i:s') . "'";

        }
        # 是否只筛选优质商家
        $active_status = input("post.active_status");
        if (!empty($active_status) and $active_status == 1) {
            $where .= " and level=1";
        }
        $length = 10;
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and create_time<'2021-07-01 00:00:00' ";
            }
        }
        $where .= " and province like '%" . $province . "%' and city like '%" . $city . "%'";
        # 优质商家从第几个查
        $youzhi_num = input("post.youzhi_num");
        # 推荐商家从第几个查
        $tuijian_num = input("post.tuijian_num");
        # 诚信商家从第几个查
        $chengxin_num = input("post.chengxin_num");
        # 普通商家从第几个查
        $putong_num = input("post.putong_num");
        if (empty($search) and empty($order_line) and empty($merchant_level) and empty($search_merchants_id) and (empty($active_status) or $active_status == 0)) {

            # 各级需查询的数量
            $num_array = [3, 3, 0, 4];
            $limit_array = [3, 3, 0, 4];
            $list = array();
            for ($i = 1; $i <= 4; $i++) {
                # 这里要修改
                if ($i == 1) {
                    $pageNum = $youzhi_num;
                } elseif ($i == 2) {
                    $pageNum = $tuijian_num;
                } elseif ($i == 3) {
                    $pageNum = $chengxin_num;
                } else {
                    $pageNum = $putong_num;
                }
                //$pageNum = ($page * $limit_array[$i - 1]);
                if ($i == 3 and $num_array[$i - 1] == 0) {
                    continue;
                }
                if ($i == 3) {
                    $pageNum = ($page * $num_array[$i - 1]);
                }

                $res = $MerchantService->LevelMerchant($i, $lat, $lon, $is_type, $num_array[$i - 1], false, $where, '', $pageNum);
                if (!empty($res)) {
                    if ($i == 1) {
                        # 优质商家直接放入
                        $list = $res;
                        $youzhi_num = $youzhi_num + count($res) + 1;
                    } else {
                        if ($i == 2) {
                            $tuijian_num = $tuijian_num + count($res) + 1;
                        } else {
                            $putong_num = $putong_num + count($res) + 1;

                        }
                        # 推荐商家放入中间
                        $list = $this->array_insert($list, ceil(count($list) / 2), $res);

                    }
                }
                # 高级不足 低级补充
                if (count($res) < $num_array[$i - 1]) {
                    if ($i < 4) {
                        $num_array[$i] += $num_array[$i - 1] - count($res);
                    }
                }
            }

        } else {
            # 有条件的情况
            $list = Db::table("merchants m")
                ->field("distinct m.id mtid, m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id ")
                ->where(array("m.status" => 1, "m.is_type" => $is_type))
                ->where("m.start_time is not null or m.end_time is not null")
                ->where($where)
                ->where("m.id != 1 and m.id != 2 and m.id != 1387 and m.id != 1388")
                ->order($order)
                ->limit($page_num, $length)->select();
        }
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                $list[$k]['userSupport'] = 0;
                if (!empty($_member_like) and in_array($v['id'], $_member_like)) {
                    $list[$k]['userSupport'] = 1;
                }
                $list[$k]['closeStore'] = 1;
                if ($v['start_time'] != '00:00' and $v['end_time'] != "00:00") {
                    if (date("H:i:s", strtotime($v['start_time'])) > date("H:i:s", strtotime($v['end_time']))) {
                        if ($v['start_time'] > date("H:i:s") and $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    } else {
                        if ($v['start_time'] > date("H:i:s") or $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    }
                }
                if ($v['temporary_close'] > 0 and $v['close_time'] <= date("Y-m-d H:i:s")
                    and $v['close_time'] >= date("Y-m-d H:i:s", strtotime("-{$v['temporary_close']} hours"))) {
                    $list[$k]['closeStore'] = 2;
                }
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                $list[$k]['plurs_star'] = 5 - $v['start'];
                $list[$k]['recommend_pro'] = MerchantService::merchantsRecommendPro($v['id'], 1);
                $list[$k]['giving_score'] = 1;
//                if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                    $list[$k]['giving_score'] = 1;
//                } else {
//                    $list[$k]['giving_score'] = 2;
//                }
            }
            $flag = count($list) == 10 ? false : true;
            return array("status" => true, "list" => $list, "flag" => $flag, "repeat" => '',
                "youzhi_num" => $youzhi_num, "tuijian_num" => $tuijian_num, "putong_num" => $putong_num);
        } else {
            return array("status" => false, "msg" => "暂无商户信息");
        }
    }

    function getsMerchantList()
    {
        $member_id = input("post.member_id");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $province = input("post.province");
        $city = input("post.city");
        $page = input("post.page");
        $is_type = input("post.is_type");
        $show_mark = input("post.show_mark");
        $search_merchants_id = input("post.search_merchants_id");

        if (!config('share.toutiaoShow') && Request::header("applymark") == 3) {
            return array("status" => false, "msg" => "暂无商户信息");
        }
        # 判断鲅鱼圈区看不到所有
        $detailAddress = getAddressResolution($lon, $lat);
        $detail_add_a = json_decode($detailAddress, true);
        if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

            $_district = $detail_add_a['regeocode']['addressComponent']['district'];
            if ($_district == '鲅鱼圈区') {
                return array("status" => false);
            }
        }
        $where = "m.id >0 and m.is_type = {$is_type} ";
        // 2021-12-23 加判断 不展示鲅鱼圈的商家
        $where .= " and m.area != '鲅鱼圈区'";
        $MerchantService = new MerchantService();
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $_member_like = array();
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2))->select();
            if (!empty($support)) {
                $_member_like = array_column($support, 'club_id');
            }
        }
        $search = input("post.search");
        if (!empty($search)) {
            $search = implode("%", mb_str_splits($search));
            $where .= " and ((m.title like '%" . $search . "%') or m.keywords like '%" . $search . "%')";
        }
        $class_a = input("post.class_a");
        if (!empty($class_a)) {
            # 获取这个分类等级下的所有分类
            $child = $MerchantService->getsCateChild(2, $class_a);
            if (!empty($child)) {
                $where .= " and mcr.cate_pid = {$class_a}";
                $child_str = implode(',', array_column($child, 'id'));
                $where .= " and mcr.cate_id in (" . $child_str . ")";
            }
        }
        $class_b = input("post.class_b");
        if (!empty($class_b)) {
            if ($is_type == 1) {
                $where .= " and mcr.cate_pid = {$class_b}";
            } else {
                $where .= " and mcr.cate_id = {$class_b}";
            }

        }
        $merchant_level = input("post.merchant_level");
        if (!empty($merchant_level)) {
            $where .= " and m.level = {$merchant_level}";
        }
        $order = "distance asc";
        $order_line = input("post.order_line");
        if (!empty($order_line)) {
            if ($order_line == 'up') {
                $order = "m.thumbup_num desc";
            } else {
                $order = "m.thumbup_num asc";
            }
        }
        if (!empty($search_merchants_id)) {
            $where .= " and m.id = {$search_merchants_id}";
        }

        if (Request::header("applymark") == 3) {
            $where .= " and open_toutiao = 1";
        }
        if (!empty($show_mark)) {
            $where .= " and m.giving_score = 1";
        }
        # 是否只筛选优质商家
        $active_status = input("post.active_status");
        if (!empty($active_status) and $active_status == 1) {
            $where .= " and level=1";
        }
        $length = 10;
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and create_time<'2021-07-01 00:00:00' ";
            }
        }
        $where .= " and province like '%" . $province . "%' and city like '%" . $city . "%'";
        $receive_repeat = input("post.repeat");
        if (!empty($receive_repeat)) {
            $where .= " and m.id not in ({$receive_repeat})";
        }
        if($member_id != 14285){
            $where .= " and m.id != 1 and m.id != 2 and m.id != 1388 and m.id != 1387";
        }
        # 优质商家从第几个查
        $youzhi_num = input("post.youzhi_num");
        # 推荐商家从第几个查
        $tuijian_num = input("post.tuijian_num");
        # 诚信商家从第几个查
        $chengxin_num = input("post.chengxin_num");
        # 普通商家从第几个查
        $putong_num = input("post.putong_num");
        if (empty($search) and empty($order_line) and empty($merchant_level) and empty($search_merchants_id) and (empty($active_status) or $active_status == 0)) {

            # 各级需查询的数量
            $num_array = [3, 3, 0, 4];
            $limit_array = [3, 3, 0, 4];
            $list = array();
            $count_num = 10;
            # 首先查3个优质
            $check_youzhi = 3;
            $youzhi_array = array();
            for ($i = 1; $i <= 4; $i++) {
                if ($i == 1) {
                    $pageNum = $youzhi_num;
                } elseif ($i == 2) {
                    $pageNum = $tuijian_num;
                } elseif ($i == 3) {
                    $pageNum = $chengxin_num;
                } else {
                    $pageNum = $putong_num;
                }
                $res = $MerchantService->LevelMerchant($i, $lat, $lon, $is_type, $check_youzhi, false, $where, '', $pageNum);
                if (!empty($res)) {
                    if ($i == 1) {
                        # 优质商家直接放入
                        $youzhi_array = $res;
                        $youzhi_num = $youzhi_num + count($res) + 1;
                    } else {
                        if ($i == 2) {
                            $tuijian_num = $tuijian_num + count($res) + 1;
                        } elseif ($i == 3) {
                            $chengxin_num = $chengxin_num + count($res) + 1;
                        } else {
                            $putong_num = $putong_num + count($res) + 1;
                        }
                        $youzhi_array = $this->array_insert($youzhi_array, ceil(count($youzhi_array) / 2), $res);
                    }
                }
                # 高级不足 低级补充
                if (count($res) < $check_youzhi) {
                    if ($i < 4) {
                        $check_youzhi -= count($res);
                    }
                } else {
                    break;
                }
            }
            if (!empty($youzhi_array)) {
                $list = array_merge($list, $youzhi_array);
                $repeat_array = implode(',', array_column($youzhi_array, 'id'));
                $where .= " and m.id not in (" . $repeat_array . ")";
                $count_num -= count($youzhi_array);
            }
            # 再查一个推荐
            $check_tuijian = 1;
            $tuijian_array = array();
            $res = $MerchantService->LevelMerchant(2, $lat, $lon, $is_type, $check_youzhi, false, $where, '', $tuijian_num);
            if (empty($res)) {
                for ($i = 1; $i <= 4; $i++) {
                    if ($i == 1) {
                        $pageNum = $youzhi_num;
                    } elseif ($i == 2) {
                        $pageNum = $tuijian_num;
                    } elseif ($i == 3) {
                        $pageNum = $chengxin_num;
                    } else {
                        $pageNum = $putong_num;
                    }
                    $res = $MerchantService->LevelMerchant($i, $lat, $lon, $is_type, $check_youzhi, false, $where, '', $pageNum);
                    if (!empty($res)) {
                        $tuijian_array = $res;
                        if ($i == 1) {
                            # 优质商家直接放入
                            $youzhi_num = $youzhi_num + count($res) + 1;
                        } else {
                            if ($i == 2) {
                                $tuijian_num = $tuijian_num + count($res) + 1;
                            } elseif ($i == 3) {
                                $chengxin_num = $chengxin_num + count($res) + 1;
                            } else {
                                $putong_num = $putong_num + count($res) + 1;
                            }
                        }
                    }
                    # 高级不足 低级补充
                    if (count($res) < $check_tuijian) {
                        if ($i < 4) {
                            $check_tuijian -= count($res);
                        }
                    } else {
                        break;
                    }
                }
            } else {
                $tuijian_array = $res;
                $tuijian_num = $tuijian_num + count($res) + 1;
            }
            if (!empty($tuijian_array)) {
                $list = array_merge($list, $tuijian_array);
                $repeat_array = implode(',', array_column($tuijian_array, 'id'));
                $where .= " and m.id not in (" . $repeat_array . ")";
                $count_num -= count($tuijian_array);
            }
            $page_num = $page * 10;
            # 其余按距离正常排序
            $qita_array = Db::table("merchants m")
                ->field("distinct m.id mtid, m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id ")
                ->where(array("m.status" => 1, "m.is_type" => $is_type))
                ->where("m.start_time is not null or m.end_time is not null")
                ->where($where)
//                ->where("m.id != 1 and m.id != 2")
                ->order($order)
                ->limit($page_num, $count_num)->select();
            if (!empty($qita_array)) {
                if (count($list) >= 4) {
                    $start = 3;

                } else {
                    $start = ceil(count($list) / 2);
                }
                $list = $this->array_insert($list, $start, $qita_array);
            }

        } else {
            # 有条件的情况
            $list = Db::table("merchants m")
                ->field("distinct m.id mtid, m.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.start_time,m.end_time,m.temporary_close,m.close_time")
                ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id ")
                ->where(array("m.status" => 1, "m.is_type" => $is_type))
                ->where("m.start_time is not null or m.end_time is not null")
                ->where($where)
//                ->where()
                ->order($order)
                ->limit($page_num, $length)->select();
        }
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                $list[$k]['userSupport'] = 0;
                if (!empty($_member_like) and in_array($v['id'], $_member_like)) {
                    $list[$k]['userSupport'] = 1;
                }
                $list[$k]['closeStore'] = 1;
                if ($v['start_time'] != '00:00' and $v['end_time'] != "00:00") {
                    if (date("H:i:s", strtotime($v['start_time'])) > date("H:i:s", strtotime($v['end_time']))) {
                        if ($v['start_time'] > date("H:i:s") and $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    } else {
                        if ($v['start_time'] > date("H:i:s") or $v['end_time'] <= date("H:i:s")) {
                            $list[$k]['closeStore'] = 2;
                        }
                    }
                }
                if ($v['temporary_close'] > 0 and $v['close_time'] <= date("Y-m-d H:i:s")
                    and $v['close_time'] >= date("Y-m-d H:i:s", strtotime("-{$v['temporary_close']} hours"))) {
                    $list[$k]['closeStore'] = 2;
                }
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                $list[$k]['plurs_star'] = 5 - $v['start'];
                $list[$k]['recommend_pro'] = MerchantService::merchantsRecommendPro($v['id'], 1);
                $list[$k]['giving_score'] = 1;
//                if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                    $list[$k]['giving_score'] = 1;
//                } else {
//                    $list[$k]['giving_score'] = 2;
//                }
            }
            $flag = count($list) == 10 ? false : true;
            //$flag = false;
            $repeat_return = implode(',', array_column($list, 'id'));
            if (empty($receive_repeat)) {
                $receive_repeat = $repeat_return;
            } else {
                $receive_repeat .= ',' . $repeat_return;
            }
            return array("status" => true, "list" => $list, "flag" => $flag, "repeat" => $receive_repeat,
                "youzhi_num" => $youzhi_num, "tuijian_num" => $tuijian_num, "putong_num" => $putong_num, "chengxin_num" => $chengxin_num);
        } else {
            return array("status" => false, "msg" => "暂无商户信息");
        }
    }

    /**
     * @return array
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商家详情
     */
    function merchantDetail()
    {
        $id = input("post.id");
        $member_id = input("post.member_id");
        $info = Db::table("merchants m")->where(array("id" => $id))->find();
        if (!empty($info['remarks'])) {
            $info['remarks'] = explode("*", $info['remarks']);
        } else {
            $info['remarks'] = array();
        }
        $info['giving_score'] = 1;
//        if ($info['level'] == 1 and $info['expire_time'] > date('Y-m-d H:i:s')) {
//            $info['giving_score'] = 1;
//        } else {
//            $info['giving_score'] = 2;
//        }
        if ((($info['level'] ==1 or $info['level']==3)  and $info['expire_time'] > date('Y-m-d H:i:s')) and $info['take_video']== 1) {
            $info['take_video'] = 1;
        } else {
            $info['take_video'] = 2;
        }
        $info['userSupport'] = 0;
        $info['share_num'] = 0;
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2, "club_id" => $id))->count();
            if ($support > 0) {
                $info['userSupport'] = 1;
            }
            # 查询这个用户分享这个商家分享了几次
            $log_integral = Db::table("log_share")->where(array("member_id" => $member_id, "type" => 9, "other_id" => $id))->count();
            $info['share_num'] = $log_integral + 1;
        }
        $info['level_title'] = $this->merchantLevel($info['level']);
        if ($info['level'] < 4) {
            $info['bao'] = '本商家为' . $info['level_title'] . '，消费过程中产生争议致电客服，平台给予处理。';
        }
        $info['plurs_star'] = 5 - $info['start'];
        if (!empty($info['keywords'])) {
            $info['keywords'] = explode('，', $info['keywords']);
        } else {
            $info['keywords'] = array();
        }
        # 是否营业状态判断
        $info['closeStore'] = 1;
        if ($info['start_time'] != '00:00' and $info['end_time'] != "00:00") {
            if (date("H:i:s", strtotime($info['start_time'])) > date("H:i:s", strtotime($info['end_time']))) {
                if ($info['start_time'] > date("H:i:s") and $info['end_time'] <= date("H:i:s")) {
                    $info['closeStore'] = 2;
                }
            } else {
                if ($info['start_time'] > date("H:i:s") or $info['end_time'] <= date("H:i:s")) {
                    $info['closeStore'] = 2;
                }
            }
        }
        if ($info['temporary_close'] > 0 and $info['close_time'] <= date("Y-m-d H:i:s")
            and $info['close_time'] >= date("Y-m-d H:i:s", strtotime("-{$info['temporary_close']} hours"))) {
            $info['closeStore'] = 2;
        }
        # 查看数量加1
        Db::table("merchants")->where(array("id" => $id))->setInc("look_num", 1);
        $info['look_num'] += 1;
        $share_member_id = input("post.share_member_id");

        if (!empty($share_member_id)) {
            # 给这个用户加积分
            $share_num = input("post.share_num");
            $this->shareMerchantIntegral($share_member_id, $share_num, $id);
        }
        # 数量格式化
        if ($info['thumbup_num'] > 99999) {
            $info['thumbup_num'] = '99999+';
        }
        if ($info['look_num'] > 99999) {
            $info['look_num'] = '99999+';
        }
        if ($info['forward_num'] > 99999) {
            $info['forward_num'] = '99999+';
        }
        # 商家分类查询
        # 一级分类
        $cate_info = Db::table("merchants_cate_relation")->where(array("merchants_id" => $info['id']))->find();
        if (!empty($cate_info)) {
            $info['cate_p_title'] = Db::table("merchants_cate")->where(array("id" => $cate_info['cate_pid']))->value('title');
            $info['cate_title'] = Db::table("merchants_cate")->where(array("id" => $cate_info['cate_id']))->value('title');
        }

        #添加任务记录 查看人数记录类型
        $params['merchants_id'] = $id;
        TaskService::getTaskPaySucess($params, 7);
        # 分享积分
        $integralService = new IntegralService();
        $info['integralNum'] = $integralService->integralSetItem('share_busines_integral');
        $imgInfo = array(
            'icon' => imgUrl('static/img/toutiao.gif', true) . '?1',
            'video_btn_bg' => imgUrl('static/img/video_btn_bg.png', true),
            'video_popup' => imgUrl('static/img/video_popup.png', true),
            'video_popup_new' => imgUrl('static/img/video_popup_new.png', true),
            'video_share_btn_bg' => imgUrl('static/img/video_share_btn_bg.png', true),
            'video_take_btn_bg' => imgUrl('static/img/video_take_btn_bg.png', true),
        );
        # 话题
        $topic = array_filter(explode('，', $info['douyin_topic']));
        if (empty($topic)) {
            $topic = ['E联车服'];
            # 查询话题
            $topicInfo = Db::table('merchants_topic')->where(array('m_id' => $id, 'status' => 1))->column('topics');
            if (!empty($topicInfo)) {
                if (count($topicInfo) > 2) {
                    $randInfo = array_rand($topicInfo, 2);
                    $topicInfo_arr = array($topicInfo[$randInfo[0]], $topicInfo[$randInfo[1]]);
                } else {
                    $topicInfo_arr = $topicInfo;
                }
                $topic = $topicInfo_arr;
            }
        }
        # title
        $title = $info['title'];
        if (empty($info['ad_words'])) {
            $info['ad_words'] = array();
        }
        # 查询该商家时候上传了转发视频
        $videoNum = Db::table('merchants_video')->where(array('m_id' => $id))->count();
        return array("status" => true, "info" => $info, 'imgInfo' => $imgInfo, 'topic' => $topic, 'title' => $title, 'videoNum' => $videoNum);
    }

    function shareVideo()
    {
        ignore_user_abort(true);     // 忽略客户端断开
        set_time_limit(0);           // 设置执行不超时
        //商户id
        $m_id = input('post.m_id');
        //用户id
        $member_id = input('post.member_id');
        $source = input('post.source');
        // 用户授权码  用来获取token
        $code = input('post.code');
        $t_openid = input('post.t_openid');
        if (!empty($t_openid) and empty($member_id)) {
            $member_idInfo = Db::table('member')
                ->where(array('toutiao_openid' => $t_openid))
                ->whereOr(array('dou_openid' => $t_openid))
                ->value('id');
            if (!empty($member_idInfo)) {
                $member_id = $member_idInfo;
            }
        }
        // 抖音openid
        $openid = Db::table('member')->where(array('id' => $member_id))->value('dou_openid');
        $redis = new Redis();
        $curl = new Curl();
        # access_token
        $token = $redis->hGet('douAccessToken', 'token');
//        if (empty($token)) {
        $client_token_url = 'https://open.douyin.com/oauth/access_token/';
        $order = [
            'client_key' => 'awdb3q3lasnoi3qd',
            'client_secret' => '7b9f6931c256c73167d634c933b33551',
            'grant_type' => 'authorization_code',
            'code' => $code
        ];
        if (Request::header("applymark") == 3) {
            $order = [
                'client_key' => 'tt4c163aabd32fa84501',
                'client_secret' => '5a29be0565c104fef54c995050121423304fd552',
                'grant_type' => 'authorization_code',
                'code' => $code
            ];
        }
        $res = $curl->post($client_token_url, $order, 'form');
        Db::table('aaa')->insert(array(
            'info' => json_encode($res),
            'type' => '抖音' . $member_id
        ));
        $resData = json_decode($res, true);
        if (!empty($resData)) {
            if ($resData['data']['error_code'] == 0) {
                $token = $resData['data']['access_token'];
                if (empty($openid)) {
                    $openid = $resData['data']['open_id'];
                    Db::table('aaa')->insert(array(
                        'info' => $openid,
                        'type' => '$openid' . $member_id
                    ));
                    if (empty($member_id)) {
                        $member_idInfo = Db::table('member')
                            ->where(array('toutiao_openid' => $openid))
                            ->whereOr(array('dou_openid' => $openid))
                            ->value('id');
                        if (!empty($member_idInfo)) {
                            $member_id = $member_idInfo;
                        }
                    }
                    Db::table('member')->where(array('id' => $member_id))->update(array('dou_openid' => $openid));
                }
                $redis->hSet('douAccessToken', 'token', $token, 86300);
//                    $url = 'https://open.douyin.com/share-id/?access_token=' . $token . '&need_callback=true';
//                    $re = $curl->get($url);
//                    $reData = json_decode($re, true);
//                    if ($reData['data']['error_code'] == 0) {
//                        $share_id = $reData['data']['share_id'];
//                    }
            }
        }
//        }
        // 上传视频
        $uploadVideo_url = 'https://open.douyin.com/video/upload/?open_id=' . $openid . '&access_token=' . $token;
        # 查询该商户是否上传了视频
        if($source != 3) {
            $merchantsVideoInfo = Db::table('merchants_video')
                ->where(array('m_id' => $m_id))->column('video_url');
        }else{
            $merchantsVideoInfo = Db::table('biz_video')
                ->where(array('b_id' => $m_id))->column('video_url');
        }
        Db::table('aaa')->insert(array(
            'info' => json_encode(input('post.')),
            'type' => '$postinfo' . $member_id
        ));
//        $uploadVideo_data = 'https://www.youpilive.com/merchants/20211204/16386116080746XtB3k9Eo1.mp4';
        if (!empty($merchantsVideoInfo)) {
            $uploadVideo_data = $merchantsVideoInfo[array_rand($merchantsVideoInfo, 1)];
        } else {
            return array("status" => false, 'msg' => '商家暂未上传视频');
        }
        $uploadVideo_data_name = substr($uploadVideo_data, strripos($uploadVideo_data, "/") + 1);
        Db::table('aaa')->insert(array(
            'info' => $uploadVideo_data_name,
            'type' => '$uploadVideo_data_name' . $member_id
        ));
        $uploadVideo_resData = $this->curl_upload_file($uploadVideo_url, $uploadVideo_data, $uploadVideo_data_name);
        Db::table('aaa')->insert(array(
            'info' => $uploadVideo_resData,
            'type' => '$uploadVideo_resData' . $member_id
        ));
        if (!is_array($uploadVideo_resData)) {
            // 发布视频
            $createVideo_url = 'https://open.douyin.com/video/create/?open_id=' . $openid . '&access_token=' . $token;
            # 查询该商家的文案
            $title = '';
            if($source != 3) {
                $copywritingInfo = Db::table('merchants_copywriting')->where(array('m_id' => $m_id))->column('context');
            }else{
                $copywritingInfo = Db::table('biz_copywriting')->where(array('b_id' => $m_id))->column('context');
            }
            if (!empty($copywritingInfo)) {
                $title = mb_substr($copywritingInfo[array_rand($copywritingInfo, 1)], 0, 30);
            }
            # 查询话题
            if($source != 3) {
                $topicInfo = Db::table('merchants_topic')->where(array('m_id' => $m_id, 'status' => 1))->column('topics');
            }else{
                $topicInfo = Db::table('biz_topic')->where(array('b_id' => $m_id, 'status' => 1))->column('topics');
            }
            $topic = '#E联车服';
            if (!empty($topicInfo)) {
                if (count($topicInfo) > 2) {
                    $randInfo = array_rand($topicInfo, 2);
                    $topicInfo_arr = array($topicInfo[$randInfo[0]], $topicInfo[$randInfo[1]]);
                } else {
                    $topicInfo_arr = $topicInfo;
                }
                $topic .= '#' . implode('#', $topicInfo_arr);
            }
            $text = $title . $topic;
            $createData = [
                'video_id' => $uploadVideo_resData,
                'text' => $text
            ];
            # 获取poi信息
            if($source != 3) {
                $merchantsInfo = Db::table('merchants')->field('city,title')->where(array('id' => $m_id))->find();
            }else{
                $merchantsInfo = Db::table('biz')->field('city,biz_title title')->where(array('id' => $m_id))->find();
            }
            $poiInfo = $this->getPoiInfo($merchantsInfo['city'], $merchantsInfo['title'], $m_id);
            if (!empty($poiInfo)) {
                $createData['poi_id'] = $poiInfo['poi_id'];
                $createData['poi_name'] = $poiInfo['poi_name'];
            }
            $createVideo_res = $this->curl_create_file($createVideo_url, $createData);
            Db::table('aaa')->insert(array(
                'info' => json_encode($createVideo_res),
                'type' => '$createVideo_res' . $member_id
            ));
            # 领取卡券
            if($source != 3) {
                $voucher = Db::table('merchants_voucher')
                    ->where(array('merchants_id' => $m_id, 'dou_mark' => 1))
                    ->find();
            }else{
                $voucher = array();
            }
            if (!empty($voucher)) {
                # 判断是否领取过
                $canReceive = true;
                $logStatus = Db::table('member_voucher')
                    ->where(array('merchants_id' => $m_id, 'member_id' => $member_id, 'voucher_id' => $voucher['id']))
                    ->where("date(start_time) = '" . date('Y-m-d') . "'")
                    ->find();
                if (!empty($logStatus)) {
                    if ($voucher['is_repeat'] == 2) {
                        $canReceive = false;
//                        return array('status'=>false,'msg'=>'该卡券奖励不可重复领取','info' => $createVideo_res);
                    }
                    if ($voucher['surplus_num'] <= 0) {
                        $canReceive = false;
//                        return array('status'=>false,'msg'=>'该卡券奖励领没了','info' => $createVideo_res);
                    }
                }
                if ($canReceive) {
                    $_voucher = array(array("voucher_id" => $voucher['id'], 'merchants_id' => $m_id, "member_id" => $member_id, "card_time" => $voucher['validity_day'],
                        "create" => date("Y-m-d H:i:s", strtotime("+{$voucher['validity_day']} days")),
                        "voucher_source" => 15, "paytype" => 0, "voucher_cost" => $voucher['price'], "card_title" => $voucher['title'],
                        "voucher_type" => 7, "money_off" => $voucher['min_consume'], "cate_pid" => $voucher['cate_pid'], 't_openid' => $t_openid
                    ));
                    $voucherService = new VoucherService();
                    $voucherService->addMemberVoucher($member_id, $_voucher);
                    Db::table("merchants_voucher")->where(array("id" => $voucher['id']))->setDec('surplus_num', 1);
                }
            }
            # 加积分
            $integral_num = 0;
            # 先查最后一次 赠送积分的时间
            $lastTime = Db::table('log_integral')
                ->field('id')
                ->where(array('member_id' => $member_id, 'integral_source' => 32, 'biz_id' => $m_id))
                ->where("date(integral_time) = '" . date('Y-m-d') . "'")
                ->find();
            if($source != 3){
                $source_mark=1;
            }else{
                $source_mark=2;
            }
            if (empty($lastTime)) {
                # 判断今天领取过几次了
                $countNum = Db::table('log_integral')
                    ->field('id')
                    ->where(array('member_id' => $member_id, 'integral_source' => 32))
                    ->where("date(integral_time) = '" . date('Y-m-d') . "'")
                    ->count();
                if ($countNum < 3) {
                    $integral_num = 50;
                    $redis = new Redis();
                    $IntegralService = new IntegralService();
                    $IntegralService->addMemberIntegral($member_id, 32, $integral_num, 1, array("biz_id" => $m_id, "biz_pro_id" => 0,'source_mark'=>$source_mark));
                    $memberInfo = $redis->hGetJson("memberInfo", $member_id);
                    $memberInfo['member_integral'] += intval($integral_num);
                    $redis->hSetJson("memberInfo", $member_id, $memberInfo);
                }
            }
            if($source != 3) {
                Db::table('share_video')
                    ->insert(array(
                        'member_id' => $member_id,
                        'm_id' => $m_id,
                        'share_id' => $createVideo_res['data']['item_id'],
                        'add_time' => date('Y-m-d H:i:s'),
                        'integral_num' => $integral_num
                    ));
            }else{
                Db::table('share_video')
                    ->insert(array(
                        'member_id' => $member_id,
                        'b_id' => $m_id,
                        'share_id' => $createVideo_res['data']['item_id'],
                        'add_time' => date('Y-m-d H:i:s'),
                        'integral_num' => $integral_num
                    ));
            }
            return array("status" => true, 'msg' => '发布视频', 'info' => $createVideo_res);
        } else {
            return array("status" => false, 'msg' => '上传视频错误', 'info' => $uploadVideo_resData);
        }
    }

    function getPoiID()
    {
        $address = '';
        $city = '营口市';
        $title = '富祥海鲜炭火烤鱼自烤羊腿';
        $redis = new Redis();
        $token = $redis->hGet('douClientToken', 'token');
        if (empty($token)) {
            $token = $this->getClientToken();
        }
        $url = "https://open.douyin.com/poi/search/keyword/?access_token=$token&cursor=0&count=20&keyword=$title&city=$city";
        dump($url);
        $curl = new Curl();
        $re = $curl->get($url);
        $reData = json_decode($re, true);
        dump($reData);
    }

    function getPoiInfo($city, $title, $m_id,$source)
    {
        if($source != 3){
            $source=1;
        }else{
            $source = 2;
        }
        # 先查询数据库
        $poiInfo = Db::table('merchants_poi_info')->where(array('m_id' => $m_id,'source_mark'=>$source))->order(array('id' => 'desc'))->find();
        if (empty($poiInfo)) {
            $redis = new Redis();
            $token = $redis->hGet('douClientToken', 'token');
            if (empty($token)) {
                $token = $this->getClientToken();
            }
//        $city = '营口市';
//        $title = '俄罗斯进口食品';
            $url = "https://open.douyin.com/poi/search/keyword/?access_token=$token&cursor=0&count=20&keyword=$title&city=$city";
//            dump($url);
            $curl = new Curl();
            $re = $curl->get($url);
            $reData = json_decode($re, true);
//            dump($reData);
            if (!empty($reData) and $reData['data']['error_code'] == 0) {
                if (array_key_exists('pois', $reData['data'])) {
                    $poiInfo = $reData['data']['pois'][0];
                    $poiInfo['m_id'] = $m_id;
                    $poiInfo['source_mark'] = $source;
                    Db::table('merchants_poi_info')->insert($poiInfo);
                }
            }
        }
        return $poiInfo;

    }

    function getVideosVoucher()
    {
        //商户id
        $m_id = input('post.m_id');
        $source=input('post.source');
        if($source != 3) {
            # 查询该商户设置的卡券信息
            $voucher = Db::table('merchants_voucher')
                ->where(array('merchants_id' => $m_id, 'dou_mark' => 1))
                ->find();
            $merchantsInfo = Db::table('merchants')
                ->field('id,title,province,city,area,address,thumb')
                ->where(array('id' => $m_id))
                ->find();
        }else{
            $voucher = array();
            $merchantsInfo = Db::table('biz')
                ->field('id,biz_title title,province,city,area,address,biz_img thumb')
                ->where(array('id' => $m_id))
                ->find();
        }
        return array('status' => true, 'voucherInfo' => $voucher, 'merchantsInfo' => $merchantsInfo, 'integralNum' => 50);
    }

    function receiveVideoVoucher()
    {
        $m_id = input('post.m_id');
        $member_id = input('post.member_id');
        $voucher = Db::table('merchants_voucher')
            ->where(array('merchants_id' => $m_id, 'dou_mark' => 1))
            ->find();
        if (!empty($voucher)) {
            # 判断是否领取过
            $logStatus = Db::table('member_voucher')->where(array('merchants_id' => $m_id, 'voucher_id' => $voucher['id']))->find();
            if (!empty($logStatus)) {
                if ($voucher['is_repeat'] == 2) {
                    return array('status' => false, 'msg' => '该卡券奖励不可重复领取');
                }
                if ($voucher['surplus_num'] <= 0) {
                    return array('status' => false, 'msg' => '该卡券奖励领没了');
                }
            }
            $_voucher = array(array("voucher_id" => $voucher['id'], 'merchants_id' => $m_id, "member_id" => $member_id, "card_time" => $voucher['validity_day'],
                "create" => date("Y-m-d H:i:s", strtotime("+{$voucher['validity_day']} days")),
                "voucher_source" => 15, "paytype" => 0, "voucher_cost" => $voucher['price'], "card_title" => $voucher['title'],
                "voucher_type" => $voucher['is_type'], "money_off" => $voucher['min_consume'], "cate_pid" => $voucher['cate_pid']
            ));
            $voucherService = new VoucherService();
            $voucherService->addMemberVoucher($member_id, $_voucher);
            Db::table("merchants_voucher")->where(array("id" => $voucher['id']))->setDec('surplus_num', 1);
        }
        return array('status' => true);
    }

    /**上传视频
     * @param $url //请求url(https://open.douyin.com/video/create?open_id={$open_id}&access_token={$access_token})
     * @param $file //文件路径 (http://www.xxx.com/uploads/video/test.mp4)
     * @param $video_name //文件名称(test.mp4)
     */
    function curl_upload_file($url, $file, $video_name)
    {
        ignore_user_abort(true);     // 忽略客户端断开
        set_time_limit(0);           // 设置执行不超时
        @ini_set('memory_limit', '512M');
        $payload = '';
        $params = "--ABC1234\r\n"
            . "Content-Type: application/x-www-form-urlencoded\r\n"
            // . "Accept:application/json\r\n"
            . "\r\n"
            . $payload . "\r\n"
            . "--ABC1234\r\n"
            . "Content-Type: video/mp4\r\n"
            . "Content-Disposition: form-data; name=\"video\"; filename=\"" . $video_name . "\"\r\n"
            . "\r\n"
            . file_get_contents($file) . "\r\n"
            . "--ABC1234--";

        $first_newline = strpos($params, "\r\n");
        $multipart_boundary = substr($params, 2, $first_newline - 2);
        $request_headers = array();
        $request_headers[] = 'Content-Length: ' . strlen($params);
        $request_headers[] = 'Content-Type: multipart/form-data; boundary=' . $multipart_boundary;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $reply = curl_exec($ch);
        Db::table('aaa')->insert(array(
            'info' => $reply,
            'type' => '$reply' . date('Y-m-d H:i:s')
        ));
        $reply_data = json_decode($reply, true);
        if ($reply_data['data']['error_code'] == 0) {
            return $reply_data['data']['video']['video_id'];
        }
        return $reply_data;
//        return json_decode($reply,true);
    }

    /**发布视频
     * @param $url //请求url (https://open.douyin.com/video/create?open_id={$open_id}&access_token={$access_token})
     * @param $video_id //文件id(上传视频返回的video_id)
     * @param $video_title //视频title (非必填)
     */
    function curl_create_file($url, $data)
    {
        $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 60); //设置超时
        // if(0 === strpos(strtolower($url), 'https')) {
        // 　　curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //对认证证书来源的检查
        // 　　curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); //从证书中检查SSL加密算法是否存在
        // }
        curl_setopt($ch, CURLOPT_POST, TRUE);
//        $data = [
//            'video_id' => $video_id,
//            'text' => $video_title
//        ];
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($data)))
        );
        $reply = curl_exec($ch);//CURLOPT_RETURNTRANSFER 不设置  curl_exec返回TRUE 设置  curl_exec返回json(此处) 失败都返回FALSE
        curl_close($ch);
        return json_decode($reply, true);
    }


    function share_id()
    {
        $share_id = '';
        $redis = new Redis();
        $token = $redis->hGet('douClientToken', 'token');
        if (empty($token)) {
            $token = $this->getClientToken();
        }
        if (!empty($token)) {
            $url = 'https://open.douyin.com/share-id/?access_token=' . $token . '&need_callback=true';
            $curl = new Curl();
            $re = $curl->get($url);
            $reData = json_decode($re, true);
            if ($reData['data']['error_code'] == 0) {
                $share_id = $reData['data']['share_id'];
            }
        }
        return array("status" => true, 'share_id' => $share_id);;
    }

    function getClientToken()
    {
        $token = '';
        $client_token_url = 'https://open.douyin.com/oauth/client_token/';
        $order = [
            'client_key' => 'awdb3q3lasnoi3qd',
            'client_secret' => '7b9f6931c256c73167d634c933b33551',
            'grant_type' => 'client_credential'
        ];
        $curl = new Curl();
        $res = $curl->post($client_token_url, $order, 'form');
        $resData = json_decode($res, true);
        if (!empty($resData)) {
            if ($resData['data']['error_code'] == 0) {
                $token = $resData['data']['access_token'];
                $redis = new Redis();
                $redis->hSet('douClientToken', 'token', $token, 7180);
            }
        }
        return $token;
    }

    /**
     * @param $member_id
     * @param $num
     * @param $merchant_id
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 分享点进来才加积分  分享一次 第一个点进来的才加1次
     */
    function shareMerchantIntegral($member_id, $num, $merchant_id)
    {
        if (empty($num)) return false;
        # 查询是否加过这个商户的积分
        $repeat = Db::table("log_integral")->where(array("integral_source" => 26, "integral_type" => 1, "member_id" => $member_id, "biz_id" => $merchant_id, "biz_pro_id" => $num))->count();
        if ($repeat <= 0) {
            # 加积分  加积分记录
            $redis = new Redis();
            $mark = "share_busines_integral";
            $integral_num = IntegralService::integralSetItem($mark);
            IntegralService::addMemberIntegral($member_id, integralSource($mark), $integral_num, 1, array("biz_id" => $merchant_id, "biz_pro_id" => $num));
            $memberInfo = $redis->hGetJson("memberInfo", $member_id);
            $memberInfo['member_integral'] += intval($integral_num);
            $redis->hSetJson("memberInfo", $member_id, $memberInfo);
        }
    }

    function addVideoIntegral()
    {
        $merchant_id = input('post.id');
        $member_id = input('post.member_id');
        $share_id = input('post.share_id');
        $integral_num = 0;
        # 先查最后一次 赠送积分的时间
        $lastTime = Db::table('log_integral')
            ->field('id')
            ->where(array('member_id' => $member_id, 'integral_source' => 31, 'biz_id' => $merchant_id))
            ->where("date(integral_time) = '" . date('Y-m-d') . "'")
            ->find();
        if (empty($lastTime)) {
            # 判断加了几次
            $countNum = Db::table('log_integral')
                ->where(array('member_id' => $member_id, 'integral_source' => 31))
                ->where("date(integral_time) = '" . date('Y-m-d') . "'")
                ->count();
            if ($countNum < 3) {
                $integral_num = 20;
                $redis = new Redis();
                IntegralService::addMemberIntegral($member_id, 31, $integral_num, 1, array("biz_id" => $merchant_id, "biz_pro_id" => 0));
                $memberInfo = $redis->hGetJson("memberInfo", $member_id);
                $memberInfo['member_integral'] += intval($integral_num);
                $redis->hSetJson("memberInfo", $member_id, $memberInfo);
            }
        }
        Db::table('share_video')
            ->insert(array(
                'member_id' => $member_id,
                'm_id' => $merchant_id,
                'share_id' => $share_id,
                'add_time' => date('Y-m-d H:i:s'),
                'integral_num' => $integral_num
            ));
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商家推荐商品
     */
    function recommendPro()
    {
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $page = input("post.page");
        if(empty($page)){
            $page = 0 ;
        }
        $pageNum = $page*10;
        $order = "mp.sold_num desc";
        $num_line = input("post.num_line");
        if(!empty($num_line)){
            if($num_line=='up'){
                $order = "mp.sold_num asc";
            }else{
                $order = "mp.sold_num desc";
            }
        }
        $price_line = input("post.price_line");
        if(!empty($price_line)){
            if($price_line=='up'){
                $order = "mp.price asc";
            }else {
                $order = "mp.price desc";
            }
        }
        $distince_line = input("post.distince_line");
        if(!empty($distince_line)){
            if($distince_line=='up'){
                $order = "distance asc";
            }else {
                $order = "distance desc";
            }
        }
        $list = Db::table("merchants_pro mp")
            ->field("mp.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance")
            ->join("merchants m","mp.merchants_id = m.id")
            ->where("province like '%" . $province . "%' and city like '%" . $city . "%' and area != '鲅鱼圈区'
                           and m.id != 1387 and m.id != 1388 and m.status=1")
            ->where(array("mp.status" => 1))
            ->where("mp.start_time<='" . date("Y-m-d") . "' and mp.end_time >='" . date("Y-m-d") . "' and (mp.num - mp.sold_num)>0")
            ->order($order)
            ->limit($pageNum, 10)
            ->select();
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                if (!empty($v['tips'])) {
                    $list[$k]['tips'] = explode(',', $v['tips']);
                } else {
                    $list[$k]['tips'] = [];
                }
                if (!empty($v['detailimage'])) {
                    $list[$k]['detailimage'] = explode(',', $v['detailimage']);
                } else {
                    $list[$k]['detailimage'] = [];
                }
                if (!empty($v['banner'])) {
                    $list[$k]['banner'] = explode(',', $v['banner']);
                } else {
                    $list[$k]['banner'] = [];
                }
                $list[$k]['describes'] = json_decode($v['describes'], true);
                $list[$k]['notice'] = json_decode($v['notice'], true);
                $list[$k]['discount'] = round(($v['price'] / $v['original_price']) * 10, 2);
                $list[$k]['surplus_num'] = $v['num'] - $v['sold_num'];
            }
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false, "msg" => "暂无商品信息", 'list' => array());
        }
    }

    function recommendPros()
    {
        $merchants_id = input("post.id");
        $page = input("post.page");
        $limit = input("post.limit");
        $list = Db::table("merchants_pro")->where(array("status" => 1, "merchants_id" => $merchants_id))
            ->where("start_time<='" . date("Y-m-d") . "' and end_time >='" . date("Y-m-d") . "' and (num - sold_num)>0")
            ->order("recommend_status asc,id desc");
        if (!empty($limit)) {
            $list = $list->limit($limit);
        }
        if (!empty($page) or $page == 0) {
            $pageNum = $page * 10;
            $list = $list->limit($pageNum, 10);
        }
        $list = $list->select();
        $flag = count($list) == 10 ? true : false;
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                if (!empty($v['tips'])) {
                    $list[$k]['tips'] = explode(',', $v['tips']);
                } else {
                    $list[$k]['tips'] = [];
                }
                if (!empty($v['detailimage'])) {
                    $list[$k]['detailimage'] = explode(',', $v['detailimage']);
                } else {
                    $list[$k]['detailimage'] = [];
                }
                if (!empty($v['banner'])) {
                    $list[$k]['banner'] = explode(',', $v['banner']);
                } else {
                    $list[$k]['banner'] = [];
                }
                $list[$k]['end_time'] = date("Y-m-d", strtotime("+1 day", strtotime($v['end_time'])));
                $list[$k]['describes'] = json_decode($v['describes'], true);
                $list[$k]['notice'] = json_decode($v['notice'], true);
                $list[$k]['discount'] = round(($v['price'] / $v['original_price']) * 10, 2);
                $list[$k]['end_time1'] = array("d" => '00', "h" => "00", "m" => '00', "s" => "00");
                $list[$k]['surplus_num'] = $v['num'] - $v['sold_num'];
            }
            return array("status" => true, "list" => $list, "flag" => $flag, "page" => $page);
        } else {
            return array("status" => false, "msg" => "暂无商品信息");
        }
    }

    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 商品详情
     */
    function recommendProDetail()
    {
        $id = input("post.id");
        $info = Db::table("merchants_pro")->where(array("id" => $id))->find();
        if (!empty($info)) {
            $info['detail_context'] = strip_tags(preg_replace('/<img[^>]+>/i', '', $info['detailimage']));
            return array("status" => true, "info" => $info);
        } else {
            return array("status" => false);
        }

    }

    function recommendProDetails()
    {
        $id = input("post.id");
        $info = Db::table("merchants_pro")->where(array("id" => $id))->find();
        if (!empty($info)) {
            $info['price'] = getsPriceFormat($info['price']);
            $info['original_price'] = getsPriceFormat($info['original_price']);
            if (!empty($info['tips'])) {
                $info['tips'] = explode(',', $info['tips']);
            } else {
                $info['tips'] = [];
            }
            if (!empty($info['detailimage'])) {
                $info['detailimage'] = explode(',', $info['detailimage']);
            } else {
                $info['detailimage'] = [];
            }
            if (!empty($info['banner'])) {
                $info['banner'] = explode(',', $info['banner']);
            } else {
                $info['banner'] = [];
            }
            $info['end_time'] = date("Y-m-d", strtotime("+1 day", strtotime($info['end_time'])));
            $info['describes'] = json_decode($info['describes'], true);
            $info['notice'] = json_decode($info['notice'], true);
            $info['discount'] = round(($info['price'] / $info['original_price']) * 10, 2);
            $info['end_time1'] = array("d" => '00', "h" => "00", "m" => '00', "s" => "00");
            $info['surplus_num'] = $info['num'] - $info['sold_num'];
            return array("status" => true, "info" => $info);
        } else {
            return array("status" => false);
        }

    }


    /**
     * @return array|bool[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context  商家优惠券
     */
    function merchantVoucher()
    {
        $id = input("post.id");
        $member_id = input("post.member_id");

        $list = Db::table("merchants_voucher")
            ->field("merchants_voucher.*,date_format(date_add(now(),INTERVAL validity_day day),'%Y-%m-%d') validity_time")
            ->where(array("merchants_id" => $id, "status" => 1, 'dou_mark' => 2))->where("surplus_num", ">", 0)
            ->where("start_time<='" . date("Y-m-d H:i:s") . "' and end_time>='" . date("Y-m-d H:i:s") . "'")->select();
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['canReceive'] = 1; # 可以领取状态
                if (!empty($member_id)) {
                    if ($v['is_repeat'] == 2) {
                        # 查询用户是否领取了这个卡券
                        $is_receive = Db::table("member_voucher")
                            ->where(array("merchants_id" => $id, "member_id" => $member_id, "voucher_id" => $v['id'], "voucher_source" => 15))
                            ->count();

                    } else {
                        # 查询用户是否有未使用的这个卡券
                        $is_receive = Db::table("member_voucher")
                            ->where(array("merchants_id" => $id, "member_id" => $member_id, "voucher_id" => $v['id'], "voucher_source" => 15, "status" => 1))
                            ->where("`create`>'" . date("Y-m-d H:i:s") . "'")
                            ->count();
                    }
                    if ($is_receive > 0) {
                        $list[$k]['canReceive'] = 2; # 不可领取状态
                    }
                }
            }
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 领取卡券
     */
    function receiveVoucher()
    {
        $merchant_id = input("post.merchant_id");
        $voucher_id = input("post.voucher_id");
        $member_id = input("post.member_id");

        # 查询是否还有卡券
        if (empty($merchant_id)) {
            // 平台投放卡券
            $info = Db::table("merchants_voucher mv")->where(array("mv.id" => $voucher_id))->find();
        } else {
            $info = Db::table("merchants_voucher mv")
                ->field("mv.*,m.is_type")
                ->join("merchants m", "m.id = mv.merchants_id")
                ->where(array("mv.id" => $voucher_id))->find();
        }
        if ($info['surplus_num'] > 0) {
            # 给用户增加卡券

            # 判断商家是否是物业
            $merchantInfo = Db::table("merchants")->field("nh_id")->where(array("id" => $info['merchants_id']))->find();
            if (!empty($merchantInfo['nh_id'])) {
                $voucher_type = 9;
            } else {
                $voucher_type = $info['is_type'] == 1 ? 8 : 7;
            }
            $_voucher = array(array("voucher_id" => $info['id'], "member_id" => $member_id, "card_time" => $info['validity_day'],
                "create" => date("Y-m-d H:i:s", strtotime("+{$info['validity_day']} days")),
                "voucher_source" => 15, "paytype" => 0, "voucher_cost" => $info['price'], "card_title" => $info['title'],
                "voucher_type" => $voucher_type, "money_off" => $info['min_consume'], "cate_pid" => $info['cate_pid']
            ));
            if (!empty($info['merchants_id'])) {
                $_voucher[0]['merchants_id'] = $info['merchants_id'];
            }
            $voucherService = new VoucherService();
            $voucherService->addMemberVoucher($member_id, $_voucher);
            $canReceive = 2;
            Db::table("merchants_voucher")->where(array("id" => $voucher_id))->setDec('surplus_num', 1);
            return array("status" => true, "canReceive" => $canReceive);
        } else {
            return array("status" => false, "msg" => "这个优惠券已经被领光了~");
        }
    }

    /**
     * @return array|bool[]
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 帮商家上热门
     */
    function helpMerchantHot()
    {
        $merchant_id = input("post.merchant_id");
        $member_id = input("post.member_id");
        $_redis = new Redis();
        $hot_status = $_redis->hGet("merchant_hot", $merchant_id);
        if (!empty($hot_status)) {
            sleep(1);
        }
        # 判断用户积分是否足够
        $MemberService = new MemberService();
        $memberInfo = $MemberService->MemberInfoCache($member_id);
        if ($memberInfo['member_integral'] >= 5) {
            $_redis->hSet("merchant_hot", $merchant_id, 1);
            # 扣除用户5积分
            $IntegralService = new IntegralService();
            $IntegralService->addMemberIntegral($member_id, 25, 5, 0);
            $memberInfo['member_integral'] -= 5;
            $_redis->hSetJson("memberInfo", $member_id, $memberInfo);
            # 判断商家分数是否满20分
            $merchantInfo = Db::table("merchants")->field("hot_score,start")->where(array("id" => $merchant_id))->find();
            $need_update = false;
            $star = $merchantInfo['start'];
            if ($merchantInfo['hot_score'] + 1 >= 20) {
                if ($merchantInfo['start'] < 5) {
                    # 给商家加1颗星 并清空热门得分
                    Db::table("merchants")->where(array("id" => $merchant_id))->setInc('start', 1);
                    $need_update = true;
                    $star = $star + 1;
                }
            } else {
                # 增加商家分数
                Db::table("merchants")->where(array("id" => $merchant_id))->setInc("hot_score", 1);
            }
            $plurs_star = 5 - $star;
            # 增加助力记录
            Db::table("merchants_hot_log")->insert(array("merchants_id" => $merchant_id, "member_id" => $member_id, "create_time" => date("Y-m-d H:i:s")));
            $_redis->hDel("merchant_hot", $merchant_id);
            $_redis->hDel("merchants", 'merchantsInfo' . $merchant_id);
            #调下 任务 热门等级
            $params['merchants_id'] = $merchant_id;
            $params['task_type'] = 12;
            $res = TaskService::getTaskPaySucess($params);
            return array("status" => true, "need_update" => $need_update, "star" => $star, "plurs_star" => $plurs_star);
        } else {
            return array("status" => false, "msg" => "积分不足");
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户可用商户卡券
     */
    function merchantCanUseVoucher()
    {
        $member_id = input("post.member_id");

        $merchant_id = input("post.merchant_id");
        $price = input("post.price");
        # 卡券一天一家只能用一次，(用户端   商家版    一个用户一天也不可重复使用平台赠送的卡券,商户类型得卡券不限制)
        $logInfo = Db::table('merchants_voucher_order')
            ->where(array('member_id' => $member_id, 'voucher_type' => 1))
            ->where("(select merchants_id from merchants_order where id = order_id) = " . $merchant_id)
            ->whereTime('create_time', 'd')
            ->find();
//        if (!empty($logInfo)) {
//            return array("status" => true, "list" => array(), "num" => 0);
//        }
        $merchantInfo = Db::table("merchants m")->field("m.is_type,group_concat(mcr.cate_pid) cate_coll")
            ->leftJoin("merchants_cate_relation mcr", "mcr.merchants_id = m.id")
            ->where(array("m.id" => $merchant_id))->find();

        $is_type = $merchantInfo['is_type'];
        $voucher_type = $is_type == 1 ? 8 : ($is_type == 3 ? 9 : 7);
        $where = null;
        if (!empty($merchantInfo['cate_coll'])) {
            $where .= "cate_pid in (" . $merchantInfo['cate_coll'] . ") or cate_pid is null or cate_pid = 0";
        }
        if (!empty($logInfo)) {
            $where .= " voucher_source in (3,11,15) ";
        }
        $list = Db::table("member_voucher")
            ->field("*,date(`create`) expiration_time")
            ->where(array("member_id" => $member_id, "status" => 1, "voucher_type" => $voucher_type))
            ->where("money_off<=$price")
            ->where("`create` > '" . date('Y-m-d H:i:s') . "'")
            ->where("(merchants_id>0 and merchants_id = {$merchant_id}) or (merchants_id=0)")
            ->where($where)
            ->select();

        $merchantVoucher = array();
        $pingtai = array();
        if (!empty($list)) {
            $VoucherService = new VoucherService();
            foreach ($list as $k => $v) {
                $Desc = $VoucherService->voucherTypeDesc($v);
                $list[$k]['TypeTitle'] = $Desc['TypeTitle'];
                $list[$k]['voucherDesc'] = $Desc['voucherDesc'];
                $list[$k]['voucher_source_title'] = $Desc['voucherSource'];
                if($v['merchants_id']>0){
                    array_push($merchantVoucher,$list[$k]);
                }else{
                    array_push($pingtai,$list[$k]);
                }
            }
        }
        $mvnum = count($merchantVoucher);
        //$mvnum = 0;
        return array("status" => true, "list" => $pingtai, "num" => count($pingtai),"mvlist"=>$merchantVoucher,"mvnum"=>$mvnum);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 详情页推荐商家
     */
    function showRecommendMerchant()
    {
        $merchant_id = input("post.merchant_id");
        $member_id = input("post.member_id");
        $province = input("post.province");
        $city = input("post.city");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $num = 1;
        # 查询多少条
        $receiveNum = input("post.num");
        if (!empty($receiveNum)) {
            $num = $receiveNum;
        }
        if (empty($lon) or empty($lat)) {
            return array("status" => false);
        }
        $where = "province like '%" . $province . "%' and city like '%" . $city . "%'";
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and create_time<'2021-07-01 00:00:00' ";
            }
        }
        if (!empty($merchant_id)) {
            $where .= " and m.id != {$merchant_id} ";
        }
        $where .= " and (
                (start_time!='' and end_time != '') and 
                (
                    (UNIX_TIMESTAMP(start_time)<=UNIX_TIMESTAMP(end_time) and start_time<='" . date("H:i:s") . "' and end_time>='" . date("H:i:s") . "') 
                    or 
                    (UNIX_TIMESTAMP(start_time)>UNIX_TIMESTAMP(end_time) and 
                        (
                             (end_time>'" . date("H:i:s") . "') or (start_time<='" . date("H:i:s") . "')
                         )
                    )
                )
            )
            or
            (start_time='00:00' and end_time = '00:00')";
        $where .= " and (temporary_close<=0 or (temporary_close>0 and (close_time>'" . date("Y-m-d H:i:s") . "' 
            or date_add(close_time,interval temporary_close hour))<'" . date("Y-m-d H:i:s") . "'))";
        $where_toutiao = '';
        if (Request::header("applymark") == 3) {
            $where .= " and open_toutiao = 1";
            $where_toutiao = ' open_toutiao = 1';
        }
        $MerchantService = new MerchantService();
        $list = $MerchantService->LevelMerchant(1, $lat, $lon, 2, $num, true, $where, $where_toutiao);
        $_member_like = array();
        if (!empty($member_id)) {
            # 查询所有用户点赞
            $support = Db::table("member_like")->field("club_id")->where(array("member_id" => $member_id, "type" => 2))->select();
            if (!empty($support)) {
                $_member_like = array_column($support, 'club_id');
            }
        }
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                $list[$k]['userSupport'] = 0;
                if (!empty($_member_like) and in_array($v['id'], $_member_like)) {
                    $list[$k]['userSupport'] = 1;
                }
                if (!empty($v['keywords'])) {
                    $list[$k]['keywords'] = explode('，', $v['keywords']);
                } else {
                    $list[$k]['keywords'] = array();
                }
                if (!empty($v['remarks'])) {
                    $list[$k]['remarks'] = explode("*", $v['remarks']);
                } else {
                    $list[$k]['remarks'] = array();
                }
                if (empty($v['ad_words'])) {
                    $list[$k]['ad_words'] = array();
                }
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                $list[$k]['recommend_pro'] = MerchantService::merchantsRecommendPro($v['id'], 1);
                $list[$k]['giving_score'] = 1;
//                if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                    $list[$k]['giving_score'] = 1;
//                } else {
//                    $list[$k]['giving_score'] = 2;
//                }
            }
        }
        return array("status" => true, "list" => $list);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 周边商户优惠券
     */
    function merchantsVoucherPacket()
    {
        $member_id = input("post.member_id");
        $province = input("post.province");
        $city = input("post.city");
        $is_type = input("post.is_type");
        $voucher_type = $is_type == 1 ? 8 : 7;
        if (!empty($member_id)) {
            # 查询附近有卡券的商家 (没领取过或者可重复领取没有未使用的)
            $list = Db::table("merchants_voucher mev")
                ->field("m.title merchants_title,mev.*,date_format(date_add(now(),INTERVAL validity_day day),'%Y.%m.%d') validity_time,1 canReceive,mev.merchants_id")
                ->join("merchants m", "m.id=mev.merchants_id and m.is_type={$is_type} and m.province like '%" . $province . "%' and m.city like '%" . $city . "%'
                and m.id !=1 and m.id !=2")
                ->where(array("mev.status" => 1, 'mev.dou_mark' => 2))->where("surplus_num", ">", 0)
                ->where("mev.start_time<='" . date("Y-m-d H:i:s") . "' and mev.end_time>='" . date("Y-m-d H:i:s") . "'")
                ->where("mev.id = (select id from merchants_voucher mvo where mvo.merchants_id = mev.merchants_id and status=1
                 and mvo.start_time<='" . date("Y-m-d H:i:s") . "' and mvo.end_time>='" . date("Y-m-d H:i:s") . "' order by mvo.price desc limit 1)")
                ->where("(select count(mv.id) from member_voucher mv where mv.member_id = {$member_id} and mv.voucher_type ={$voucher_type} and mv.voucher_id = mev.id
                 and mv.status=1 and mv.merchants_id = mev.merchants_id)=0")
                ->orderRaw("rand()")
                ->limit(3)
                ->select();
            if (count($list) < 3) {
                $num = 3 - count($list);
                $launch = Db::table("merchants_voucher mev")
                    ->field("mev.*,date_format(date_add(now(),INTERVAL validity_day day),'%Y.%m.%d') validity_time,1 canReceive")
                    ->where("(select count(mv.id) from member_voucher mv where mv.member_id = {$member_id} and mv.voucher_type ={$voucher_type} and mv.voucher_id = mev.id
                 and mv.status=1)=0")
                    ->where(array("mev.status" => 1, "launch_status" => 2, 'mev.dou_mark' => 2))->where("surplus_num", ">", 0)
                    ->where("mev.start_time<='" . date("Y-m-d H:i:s") . "' and mev.end_time>='" . date("Y-m-d H:i:s") . "'")
                    ->orderRaw("rand()")
                    ->limit($num)
                    ->select();
                if (empty($launch)) {
                    $list = array_merge($list, $launch);
                }
            }
            if (!empty($list)) {
                return array("status" => true, "list" => $list);
            } else {
                return array("status" => false, "msg" => "暂无商家优惠券");
            }
        } else {
            return array("status" => false, "msg" => "暂无商家");
        }

    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户预约的商户订单
     */
    function memberMerchantOrders()
    {
        $member_id = input("post.member_id");
        if (empty($member_id)) {
            return array("status" => false);
        }
        $list = MerchantService::memberMerchantsOrderService($member_id, 1);
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                if ($v['pay_type'] == 6) {
                    $list[$k]['product_title'] = "物业费缴纳";
                    $list[$k]['duration_title'] = lang("duration")[$v['duration']];
                    $info = PropertyService::getsPayFeeLogOrder($v['order_number']);
                    $list[$k]['house_info'] = $info['build_title'] . $info['unit_title'] . $info['floor_title'] . $info['house_title'] . "号";
                }
            }
            return array("status" => true, "list" => $list);
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取本地生活banner
     */
    function localLifeBanner()
    {
        $mark = input("post.mark");
        $_redis = new Redis();
        # 首页广告管理
        $banner = AdvanceService::getsAdvance("locallifebanner");
        if (!empty($banner)) {
            if (empty($mark)) {
                return array("status" => true, "list" => array_column($banner['info'], "application_image"));
            } else {
                return array("status" => true, "list" => $banner['info']);
            }

        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 用户我的足迹
     */
    function merchantsConsumpList()
    {
        $member_id = input("post.member_id");
        $page = input("post.page");
        if (empty($page)) {
            $page = 0;
        }
        $page_num = $page * 10;
        $list = Db::table("merchants_logconsump ml")
            ->field("ml.*,m.title merchants_title,m.is_type,m.nh_id")
            ->leftJoin("merchants m", "m.id = ml.merchants_id")
            ->where(array("ml.member_id" => $member_id, "ml.status" => 1))
            ->order("ml.create_time desc")
            ->limit($page_num, 10)->select();
        if (!empty($list)) {
            foreach ($list as $k => $v) {
                $list[$k]['pay_type_title'] = getpay_type($v['pay_type']);
            }
        }
        $flag = count($list) == 10 ? true : false;
        return array("status" => true, "list" => $list, "flag" => $flag);

    }

    /**
     * @return bool[]
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 删除足迹记录
     */
    function deleteFootLog()
    {
        $id = input("post.id");
        $merchantService = new MerchantService();
        $merchantService->delLogConsump($id);
        return array("status" => true);
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 获取商家等级分类
     */
    function getsMerchantsType()
    {
        $param = input("post.");
        $type_array = array(array("name" => "全部商家", "id" => 0));
        if (empty($param['province']) or empty($param['city'])) {
            return array("status" => true, "type_array" => $type_array);
        } else {

            # 查询商户有哪些类型
            $list = Db::table("merchants m")
                ->field("count(m.id) num,m.level")
                ->where(array("m.status" => 1, "m.is_type" => $param['is_type']))
                ->where("m.start_time<='" . date("H:i:s") . "' and m.end_time>='" . date("H:i:s") . "' and m.province like '%" . $param['province'] . "%'
                 and m.city like '%" . $param['city'] . "%'")
                ->where("m.temporary_close<=0 or (m.temporary_close>0 and (m.close_time>'" . date("Y-m-d H:i:s") . "'
            or date_add(m.close_time,interval m.temporary_close hour))<'" . date("Y-m-d H:i:s") . "')")
                ->where("m.id != 1 and m.id != 2 and m.id != 1387 and m.id != 1388")
                ->group("m.level")
                ->select();
            if (!empty($list)) {
                $_type = array_column($list, 'level');
                if (in_array(1, $_type)) {
                    array_push($type_array, array("name" => "优质商家", "id" => 1));
                }
                if (in_array(2, $_type)) {
                    array_push($type_array, array("name" => "推荐商家", "id" => 2));
                }
                if (in_array(3, $_type)) {
                    array_push($type_array, array("name" => "诚信商家", "id" => 3));
                }
                if (in_array(4, $_type)) {
                    array_push($type_array, array("name" => "普通商家", "id" => 4));
                }
            }
            return array("status" => true, "type_array" => $type_array);
        }
    }

    function merchantsVideoList()
    {
        return array("status" => false);
        $m_id = input('post.id');
        # 查询商家视频
        $info = Db::table('merchants_video')
            ->field('video_url')
            ->where(array('m_id' => $m_id))->select();
        if (!empty($info)) {
            foreach ($info as $k => $v) {
                $info[$k]['video_path'] = $v['video_url'];
                $info[$k]['like'] = 0;
                $info[$k]['like_num'] = 0;
            }
            return array("status" => true, 'list' => $info);
        }
        return array("status" => false);
    }

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 吃喝玩乐推荐商家
     */
    function getsRecommendMerchantsList()
    {
        $lon = input("post.lon");
        $lat = input("post.lat");
        $province = input("post.province");
        $city = input("post.city");
        $member_id = input("post.member_id");
        if (!config('share.toutiaoShow') && Request::header("applymark") == 3) {
            return array("status" => false, "msg" => "暂无商户信息");
        }
        # 判断鲅鱼圈区看不到所有
        $detailAddress = getAddressResolution($lon, $lat);
        $detail_add_a = json_decode($detailAddress, true);
        if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

            $_district = $detail_add_a['regeocode']['addressComponent']['district'];
            if ($_district == '鲅鱼圈区') {
                return array("status" => false);
            }
        }
        $where = "m.id >0 and m.is_type = 2 ";
        $where .= " and area != '鲅鱼圈区'";
        $MerchantService = new MerchantService();
        if (Request::header("applymark") == 3) {
            $where .= " and open_toutiao = 1";
        }
        if (!empty($province) and empty($city)) {
            $city = $province;
            $province = '';
        }
        if (!empty($member_id)) {
            # 查询用户是否是2021-07-01之后注册的用户或会员
            $is_old = Db::table("member")->where(array("id" => $member_id))->value("register_time");
            if (empty($is_old) or date("Y-m-d", strtotime($is_old)) < '2021-07-01') {
                # 此类会员看不到11月1日上架的商家
                $where .= " and create_time<'2021-07-01 00:00:00' ";
            }
        }
        $where .= " and province like '%" . $province . "%' and city like '%" . $city . "%'";
        $where .= " and ((
                (start_time!='00:00' or end_time != '00:00') and
                (
                    (UNIX_TIMESTAMP(start_time)<=UNIX_TIMESTAMP(end_time) and start_time<='" . date("H:i:s") . "' and end_time>='" . date("H:i:s") . "')
                    or
                    (UNIX_TIMESTAMP(start_time)>UNIX_TIMESTAMP(end_time) and
                        (
                            (end_time>'" . date("H:i:s") . "') or (start_time<='" . date("H:i:s") . "')
                         )
                    )
                )
            )
            or
            (start_time='00:00' and end_time = '00:00'))";
        $where .= " and ((temporary_close<=0 or (temporary_close>0 and (close_time>'" . date("Y-m-d H:i:s") . "'
            or date_add(close_time,interval temporary_close hour))<'" . date("Y-m-d H:i:s") . "')))";
        # 优质商家推荐
        $youzhi_array = array();
        $youzhi_res = $MerchantService->LevelMerchant(1, $lat, $lon, 2, 6, true, $where, '', 0);
        if (!empty($youzhi_res)) {
            foreach ($youzhi_res as $k => $v) {
                $youzhi_res[$k]['level_title'] = $this->merchantLevel($v['level']);
                $youzhi_res[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                $youzhi_res[$k]['giving_score'] = 1;
//                if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                    $youzhi_res[$k]['giving_score'] = 1;
//                } else {
//                    $youzhi_res[$k]['giving_score'] = 2;
//                }
            }
            $youzhi_array = $youzhi_res;
        }
        $return_array = array();
        $class_list = $MerchantService->merchantCate('more', 2);
        if (!empty($class_list)) {
            foreach ($class_list as $ck => $cv) {
                $new_where = "";
                $child = $MerchantService->getsCateChild(2, $cv['id']);
                if (!empty($child)) {
                    $child_str = implode(',', array_column($child, 'id'));
                    $new_where .= " and mcr.cate_id in (" . $child_str . ")";
                }
                $new_where = $where . $new_where;
                $list = array();
                $num = 6; // 查询6个
                for ($i = 1; $i <= 4; $i++) {
                    if (count($list) >= 6) {
                        break;
                    }
                    $res = $MerchantService->LevelMerchant($i, $lat, $lon, 2, $num, true, $new_where, '', 0);
                    if (!empty($res)) {
                        $list = array_merge($list, $res);
                    }

                    # 高级不足 低级补充
                    if (count($res) < $num) {
                        $num = $num - count($res);
                    }
                }
                foreach ($list as $k => $v) {
                    $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                    $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['id']);
                    $list[$k]['giving_score'] = 1;
//                    if ($v['level'] == 1 and $v['expire_time'] > date('Y-m-d H:i:s')) {
//                        $list[$k]['giving_score'] = 1;
//                    } else {
//                        $list[$k]['giving_score'] = 2;
//                    }
                }
                if ($cv['id'] == 25) {
                    # 餐饮美食
                    $return_array[0] = $list;
                } elseif ($cv['id'] == 33) {
                    # 便利生活
                    $return_array[1] = $list;
                } elseif ($cv['id'] == 26) {
                    # 休闲娱乐
                    $return_array[2] = $list;
                } elseif ($cv['id'] == 28) {
                    # 丽人美发
                    $return_array[3] = $list;
                } elseif ($cv['id'] == 31) {
                    # 运动健身
                    $return_array[4] = $list;
                } elseif ($cv['id'] == 27) {
                    # 酒店
                    $return_array[5] = $list;
                } elseif ($cv['id'] == 32) {
                    # 景点
                    $return_array[6] = $list;
                } elseif ($cv['id'] == 30) {
                    # 亲子教育
                    $return_array[7] = $list;
                } elseif ($cv['id'] == 29) {
                    # 保健
                    $return_array[8] = $list;
                } elseif ($cv['id'] == 110) {
                    # 宠物生活
                    $return_array[9] = $list;
                }

            }
        }
        return array("status" => true, "recommendList" => $youzhi_array, "list" => $return_array);
    }

    function getsAdvanceBanner()
    {
        $mark = input("post.mark");
        //$banner=AdvanceService::getsAdvance("banner");
        $banner = array("info" => array(array("application_image" => imgUrl('static/imgs/chwl/advance1.png', true),
            "is_jump" => 2)));
        if (!empty($banner['info'])) {
            $applymark = Request::header("applymark");// 1 app 2 公众号 3抖音小程序
            if($applymark != 3) {
                return array("status" => true, "list" => $banner['info']);
            }else{
                return array("status" => false, "list" => array());
            }
        } else {
            return array("status" => false);
        }
    }

    /**
     * @return array|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 支付后获取商家红包卡券
     */
    function getsPayRedPage(){
        $merchants_id = input("post.merchants_id");
        $member_id = input("post.member_id");
        $province = input("post.province");
        $city = input("post.city");
        $where = "";
        if(!empty($merchants_id)){
            $where = "mev.merchants_id = {$merchants_id}";
        }else{
            $where = "m.province like '%" . $province . "%' and m.city like '%" . $city . "%'";
        }
        $info= Db::table("merchants_voucher mev")
            ->field("m.title merchants_title,mev.*,date_format(date_add(now(),INTERVAL validity_day day),'%Y.%m.%d') validity_time,1 canReceive,mev.merchants_id")
            ->join("merchants m", "m.id=mev.merchants_id and m.id !=1 and m.id !=2")
            ->where(array("mev.status" => 1,'mev.dou_mark'=>2))
            ->where("surplus_num", ">", 0)
            ->where("mev.start_time<='" . date("Y-m-d H:i:s") . "' and mev.end_time>='" . date("Y-m-d H:i:s") . "'")
            ->where($where)
            ->orderRaw("rand()")
            ->find();
        //->where("(select count(mv.id) from member_voucher mv where mv.member_id = {$member_id} and (mv.voucher_type =7 or mv.voucher_type=8) and mv.voucher_id = mev.id
        //                 and mv.status=1 and mv.merchants_id = mev.merchants_id)=0")
        if(!empty($info)){
            return array("status"=>true,"info"=>$info);
        }else{
            return array("status"=>false);
        }

    }

    /**
     * @return bool[]|false[]
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @context 是否显示首页上门洗车图片
     */
    function showDoorWashCar(){
        $lon = input("post.lon");
        $lat = input("post.lat");
        $province = input("post.province");
        $city = input("post.city");
        $member_id = input("post.member_id");
        # 显示会员图片或者上门洗车图片 1 会员 2洗车
        $showMark = 1;
        # 开启或关闭状态
        $openStatus = 1;
        if($openStatus==1){
            if($showMark==1){
                # 判断是否是会员用户
                $MemberService = new MemberService();
                $memberInfo = $MemberService->MemberInfoCache($member_id);
                if($memberInfo['member_level_id']>0){
                    # 已经是会员跳转上门洗车
                    goto b;
                }else{
                    $jumpInfo = AdvanceService::getsJumpUrl(12,0);
                    $return_info = array(
                        "imgurl"=>"https://back.elianchefu.com/static/imgs/index/door_service2.gif?1",
                        "url"=>$jumpInfo['url'],
                        "param" => $jumpInfo['param'],
                        "is_jump"=>1,
                        "jump_type"=>12,

                    );
                    return array("status"=>true,"info"=>$return_info);
                }
            }else{
                # 判断用户是否是新注册
                # 查询用户是否是2021-10-01日之后注册
             b: $memberInfo=Db::table("member")->field("id")->where(array("id"=>$member_id))->where("register_time >='2021-10-01'")->find();
                if(!empty($memberInfo)){
                    # 判断是否有上门洗车的商家
                    $info = Db::table("merchants m")
                        ->Join("merchants_cate_relation mcr", "mcr.merchants_id = m.id and mcr.cate_pid = 121")
                        ->where(array("m.status" => 1, "m.is_type" => 1))
                        ->where("m.province like '%" . $province . "%' and m.city like '%" . $city . "%'")
                        ->find();
                    if(!empty($info)){
                        $jumpInfo = AdvanceService::getsJumpUrl(11,0,'',121);
                        $return_info = array(
                            "imgurl"=>"https://back.elianchefu.com/static/imgs/index/door_service1.gif",
                            "url"=>$jumpInfo['url'],
                            "param" => $jumpInfo['param'],
                            "is_jump"=>1,
                            "jump_type"=>11,
                        );
                        return array("status"=>true,"info"=>$return_info);
                    }else{
                        return array("status"=>false);
                    }
                }else{
                    return array("status"=>false);
                }
            }
        }else{
            return array("status"=>false);
        }


    }


    function getsVideoList(){
        $member_id = input("post.member_id");
        $lon = input("post.lon");
        $lat = input("post.lat");
        $pages = input("post.pages");
        if(empty($pages)){
            $pages = 0;
        }
        $pageNum = $pages*3;
        $province = input("post.province");
        $city = input("post.city");
        $where = "";
        if(empty($province) or empty($city)){
            $detailAddress = getAddressResolution($lon, $lat);
            $detail_add_a = json_decode($detailAddress, true);

            if ($detail_add_a['status'] == 1 and $detail_add_a['info'] == 'OK') {

                $_district = $detail_add_a['regeocode']['addressComponent']['district'];
                if ($_district == '鲅鱼圈区') {
                    return array("status" => false);
                }

                $province = $detail_add_a['regeocode']['addressComponent']['province'];
                $city = $detail_add_a['regeocode']['addressComponent']['city'];
                if($member_id == 9405  and $city == '沈阳市'){
                    $city = '营口市';
                }
                $where = "m.province like '".$province."' and m.city like '".$city."'";
            }
        }else {
            if($member_id == 9405  and $city == '沈阳市'){
                $city = '营口市';
            }
            $where = "m.province like '".$province."' and m.city like '".$city."'";
        }

        $list = Db::table("merchants_video mv")
            ->field("mv.*,round(st_distance_sphere(point({$lon},{$lat}),point(m.lon,m.lat))/1000,2) AS distance,
            m.level,m.id merchants_id,m.title,m.thumb,m.bond_price")
            ->join("merchants m","mv.m_id = m.id")
            ->where("area != '鲅鱼圈区' and m.status=1 and m.level<=3 
            and m.id !=164 and m.id !=1388")
            ->where($where)
            ->limit($pageNum,3)
            ->orderRand()
            ->select();
        //and m.id != 1387 and m.id != 1388
        if(!empty($list)){
            foreach($list as $k=>$v){
                $list[$k]['level_title'] = $this->merchantLevel($v['level']);
                $list[$k]['giving_score'] = 1;
                $list[$k]['voucher_list'] = MerchantService::merchantsFullDiscount($v['merchants_id']);
                $list[$k]['recommend_pro'] = Db::table("merchants_pro")
                    ->field("id,title,convert(price,decimal(10,2)) price,convert(original_price,decimal(10,2)) original_price")
                    ->where(array("merchants_id"=>$v['merchants_id'],"status"=>1))
                    ->where("start_time<='" . date("Y-m-d") . "' and end_time >='" . date("Y-m-d") . "' and (num - sold_num)>0")
                    ->orderRaw("rand()")->find();
                $list[$k]['collect_count_num'] = $v['collect_num'];
                # 收藏数量是收藏商家的数量
                if(empty($member_id)){
                    $list[$k]['collect_num'] = 0;
                }else{
                    $list[$k]['collect_num'] = Db::table("member_like")
                        ->where(array("member_id" => $member_id, "type" => 4,"club_id"=>$v['id']))->count();
                }

                $list[$k]['support_status'] =  Db::table("member_like")->where(array("type"=>3,"club_id"=>$v['id'],"member_id"=>$member_id))->count();

            }
            return array("status"=>true,"list"=>$list);
        }else{
            return array("status"=>false);
        }

    }

    /**
     * @return bool
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @context 点赞视频收藏商家
     */
    function operationVideo(){
        $type = input("post.type");
        $operation_type = input("post.operation_type");
        $id = input("post.id");
        $member_id = input("post.member_id");
        if($type==1){
            # 点赞视频
            if($operation_type==1){
                # 增加
                Db::table("merchants_video")->where(array("id"=>$id))->setInc("support_num",1);
               $a = Db::table("member_like")->insertGetId(array("type"=>3,"club_id"=>$id,"member_id"=>$member_id,"like_time"=>date("Y-m-d H:i:s")));

            }else{
                # 减少
                Db::table("merchants_video")->where(array("id"=>$id))->where("support_num > 0")->setDec("support_num",1);
                Db::table("member_like")->where(array("type"=>3,"club_id"=>$id,"member_id"=>$member_id))->delete();

            }

        }elseif($type==2){
            $video_id = input("post.video_id");
            # 收藏视频商家
            if($operation_type==1){
                # 增加
                Db::table("member_like")->insert(array("type"=>2,"club_id"=>$id,"member_id"=>$member_id,"like_time"=>date("Y-m-d H:i:s")));
                Db::table("member_like")->insert(array("type"=>4,"club_id"=>$video_id,"member_id"=>$member_id,"like_time"=>date("Y-m-d H:i:s")));
                Db::table("merchants_video")->where(array("id"=>$video_id))->setInc("collect_num",1);
            }else{
                # 减少
                Db::table("member_like")->where(array("type"=>2,"club_id"=>$id,"member_id"=>$member_id))->delete();
                Db::table("member_like")->where(array("type"=>4,"club_id"=>$video_id,"member_id"=>$member_id))->delete();
                Db::table("merchants_video")->where(array("id"=>$video_id))->where("collect_num > 0")->setDec("collect_num",1);
            }
        }else{
            Db::table("merchants_video")->where(array("id"=>$id))->setInc("player_num",1);
            Db::table("merchants_video")->where(array("id"=>$id))->setInc("read_num",1);
        }
        return true;
    }

    function getsIntegralList(){
        return array(
            array("time_length"=>2,"integral_num"=>5,"timestr"=>120),
            array("time_length"=>5,"integral_num"=>10,"timestr"=>300),
            array("time_length"=>8,"integral_num"=>15,"timestr"=>480),
            array("time_length"=>12,"integral_num"=>20,"timestr"=>720),
            array("time_length"=>16,"integral_num"=>25,"timestr"=>960),
            array("time_length"=>22,"integral_num"=>30,"timestr"=>1320),
            array("time_length"=>30,"integral_num"=>35,"timestr"=>1800),
            array("time_length"=>34,"integral_num"=>40,"timestr"=>2040),
            array("time_length"=>40,"integral_num"=>50,"timestr"=>2400),
            array("time_length"=>45,"integral_num"=>60,"timestr"=>2700),
        );
    }

    function getsMembervideoList(){
        $member_id = input("post.member_id");
        $today = date("Y-m-d");
        $redis = new Redis();
        $minutes = 0; //
        $now_time = 0; // 当前用户看了多长时间
        $show_status = false; // 是否显示积分图标
        if(!empty($member_id)){
            # 查询用户今日是否已经添加了
            $list = Db::table("log_video_integral")->where(array("member_id"=>$member_id))
                ->where("date(create_time)='".$today."'")->select();
            if(empty($list)){
                $list = array();
                $integralList = $this->getsIntegralList();
                foreach($integralList as $v){

                    array_push($list,array("member_id"=>$member_id,"integral_num"=>$v['integral_num'],
                        "time_length"=>$v['time_length'],"status"=>1,"create_time"=>date("Y-m-d H:i:s")));
                }
                Db::table("log_video_integral")->insertAll($list);
                $_time = strtotime(date("Y-m-d 23:59:59"))-time();
                $redis->hSet("memberVideoIntegral",strval($member_id),0,$_time);
                $show_status = true;
                $minutes = $list[0]['time_length'];
            }else {
               $now_time=$redis->hGet("memberVideoIntegral",strval($member_id));
                if($now_time>0){
                    $now_time_length = intval($now_time/60);
                }
                foreach($list as $k=>$v){
                    if($now_time_length>0){
                        if($v['status']==1 and $v['time_length']<=$now_time_length){
                            $list[$k]['status'] = 2 ;
                            Db::table("log_video_integral")->where(array("id"=>$v['id']))->update(array("status"=>2));
                        }
                    }

                    if($v['status']==1 or $v['status']==2){
                        $show_status = true;
                    }
                    if($minutes==0 and $list[$k]['status']==1){
                        $minutes = $list[$k]['time_length'];
                    }
                }

            }
            return array("status"=>$show_status,"list"=>$list,"minutes"=>$minutes,"now_time"=>$now_time,"count_integral"=>array_sum(array_column($list, 'integral_num')));
        }else{
            return array("status"=>false);
        }
    }
    
    function updateMemberVideoTime(){
        $num = input("post.num");
        $member_id = input("post.member_id");
        if(!empty($member_id)){
            $redis = new Redis();
            $redis->hSet("memberVideoIntegral",strval($member_id),$num);
            if($num>0){
                # 计算一共多少时长
                $now_time_length = intval($num/60);
                Db::table("log_video_integral")
                    ->where("time_length < {$now_time_length} and date(create_time)='".date("Y-m-d")."'
                     and member_id = {$member_id} and status=1")
                    ->update(array("status"=>2));
            }
            return array("status"=>true);
        }
    }

    function receiveVideoIntegral(){
         $num = input("post.num");
         $time_length = intval($num/60);
         $member_id = input("post.member_id");
         if(!empty($member_id)){
             $redis = new Redis();
             $lock = $redis->lock('receiveVideoIntegral'.$member_id,5);

             if($lock){
                 $integralService = new IntegralService();
                 $list=Db::table("log_video_integral")
                     ->where("time_length <= {$time_length} and date(create_time)='".date("Y-m-d")."'
                     and member_id = {$member_id} and (status=1 or status=2)")->select();
                 if(!empty($list)){
                     foreach ($list as $k=>$v){
                         $integralService->addMemberIntegral($member_id, 34, $v['integral_num'], 1);
                     }
                     Db::table("log_video_integral")
                         ->where("time_length <= {$time_length} and date(create_time)='".date("Y-m-d")."'
                     and member_id = {$member_id} and (status=1 or status=2)")->update(array("status"=>3));
                 }
                 return array("status"=>true);
             }else {
                 return array("status"=>false);
             }
         }else{
             return array("status"=>false);
         }

    }

    function demo()
    {
        $_redis = new Redis();
        $list = $_redis->hGetJson("merchants_cate1", "all");
        $list2 = $_redis->hGetJson("merchants_cate2", "all");
        dump($list);
        dump($list2);
    }
}
